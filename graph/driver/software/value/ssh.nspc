%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								 SSH.NSPC
%
%	nSpace SSH value driver.  The current implementation
%	relays on port forwarding on the remote system to
%	its local namespace.  This provides an authenticated, 
%	encrypted link to another namespace.
%
%	TODO: Does not currently contain a server, assumes
%	'sshd' running on target system for now, sshd connects
%	to the TCP server directly.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications
	$	OnStart				Misc.Dist {}		% Start I/o

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire
		!	Uninitialize/OnFire					Debug/Fire
	$	DebugConn Misc.Debug {}
	$	DebugDisC Misc.Debug {}

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: This state
	#	State											State/Driver/Software/Value/Ssh/ {}

	% Subgraph: Outgoing connections
	#	Connect										Driver/Software/Comm/Network/Ip/Tcp/Ssh/Connect/ {}

	% Subgraph: Read management
	#	Reads											Driver/Software/Comm/Network/Ip/Tcp/Ssh/Reads/ {}

	% MISCN: Run states
	$	ValueRunIdle Misc.Dist { Value Idle }
		!	Initialize/OnFire						ValueRunIdle/Fire
		!	ValueRunIdle/OnFire					State/Transport/Run/Fire
	$	ValueRunRun Misc.Dist { Value Run }
		!	ValueRunRun/OnFire					State/Transport/Run/Fire

	% MISCN: Run on/off
	$	ValueRunT Misc.Dist { Value true:bool }
		!	ValueRunT/OnFire						Connect/State/Run/Fire
	$	ValueRunF Misc.Dist { Value false:bool }
		!	ValueRunF/OnFire						Connect/State/Run/Fire

	% MISCN: Create parser for values
	$	CreatePrsr Misc.Create { Id "Io.StmPrsBin" }
		!	Initialize/OnFire						CreatePrsr/Fire

	%%%%%%%%%%%
	% Run/Stop
	%%%%%%%%%%%

	% MISCN: Demux status changes
	$	DecodeRun Misc.Decode { Values ( Server Client Stop ) }
		!	State/Transport/Run/OnFire			DecodeRun/Select
		!	State/Transport/Run/OnFire			DecodeRun/Fire

	% Signal start up
	!	DecodeRun/OnClient						ValueRunT/Fire
	!	DecodeRun/OnServer						ValueRunT/Fire
	!	ValueRunT/OnFire							ValueRunRun/Fire

	% MISCN: Create a dictionary to keep track of connections
	$	CreateConnsDct Misc.Create { Id Adt.Dictionary }
		!	ValueRunT/OnFire						CreateConnsDct/Fire

	% Shutdown
	!	DecodeRun/OnStop							ValueRunF/Fire

	%%%%%%%%%%%%%%
	% Connections
	%%%%%%%%%%%%%%

	%% Established connections

	% MISCN: Connection established
	$	OnConnect Misc.Dist {}
		!	Connect/State/OnConnect/OnFire	OnConnect/Fire
		!	OnConnect/OnFire						DebugConn/Fire
%		!	OnConnect/OnFire						Debug/Fire

	%% For value I/O, we want a predictable buffer size.  Too much buffer
	%% and values can get backlogged.

	% ADTN: Connection will be identified on the remote address
	$	LoadConnRem Adt.Load { Key Destination }
		!	OnConnect/OnFire						LoadConnRem/Dictionary

	% MATHN: Prepend the transport for an official connection string
	$	AppendSsh Math.Binary { Left "ssh://" }
		!	LoadConnRem/OnFire					AppendSsh/Right
		!	LoadConnRem/OnFire					AppendSsh/Fire

	% ADTN: Store connection under address if new
	$	StoreConn Adt.Store {}
		!	CreateConnsDct/OnFire				StoreConn/Dictionary
		!	OnConnect/OnFire						StoreConn/Value
		!	AppendSsh/OnFire						StoreConn/Key
		!	AppendSsh/OnFire						StoreConn/Fire
%		!	StoreConn/OnFire						Debug/Fire

	% Connection established
	!	AppendSsh/OnFire							Debug/Fire
	!	AppendSsh/OnFire							State/Transport/OnConnect/Fire

	% Latent connections
	!	OnConnect/OnFire							LoadConnRem/Fire

	% ADTN: Load the channel context for read
	$	LoadChConn Adt.Load { Key Channel }
		!	OnConnect/OnFire						LoadChConn/Dictionary
		!	OnConnect/OnFire						LoadChConn/Fire

	% When a connection is made, begin reading values
	!	LoadChConn/OnFire							Reads/State/Add/Fire

	%% Request an outgoing connection.

	% MISCN: Extract protocol from string
	$	ProtSubConn Misc.StringOp { From 0:int To 6:int }
		!	State/Transport/Connect/OnFire	ProtSubConn/Source

	% MISCN: Extract address/port from transport
	$	AddrSubConn Misc.StringOp { From 6:int }
		!	State/Transport/Connect/OnFire	AddrSubConn/Source

	% MISCN: Match this protocol ?
	$	IsSshConn Misc.Compare { Left "ssh://" }
		!	ProtSubConn/OnFire					IsSshConn/Fire
%		!	IsSshConn/OnEqual						Debug/Fire

	% Add a new client connection
	!	AddrSubConn/OnFire						Debug/Fire
	!	AddrSubConn/OnFire						Connect/State/Add/Fire
	!	IsSshConn/OnEqual							AddrSubConn/Substring
	!	State/Transport/Connect/OnFire		ProtSubConn/Substring

	%%%%%%%%%%%
	% Transmit
	%%%%%%%%%%%

	%% TODO: Asynchronous transmissions ?
%	!	State/Transport/Value/OnFire			Debug/Fire

	% MISCN: Transmit value
	$	OnTransmit Misc.Dist {}
		!	State/Transport/Value/OnFire		OnTransmit/Fire

	% MISCN: Demux value specifications
	$	LoadConnTx Adt.Load { Key Connection }
		!	OnTransmit/OnFire						LoadConnTx/Dictionary
	$	LoadValTx Adt.Load { Key Value }
		!	OnTransmit/OnFire						LoadValTx/Dictionary
	$	StoreValErr Adt.Store { Key Error Value true:bool }
		!	OnTransmit/OnFire						StoreValErr/Dictionary

	% MISCN: Extract protocol for this transport
	$	ProtSubVal Misc.StringOp { From 0:int To 6:int }
		!	LoadConnTx/OnFire						ProtSubVal/Source

	% MISCN: Match ?
	$	IsSshVal Misc.Compare { Left "ssh://" }
		!	ProtSubVal/OnFire						IsSshVal/Fire

	% ADTN: Is connection string valid ?  Load its dictionary.
	$	LoadConnDctTx Adt.Load {}
		!	CreateConnsDct/OnFire				LoadConnDctTx/Dictionary
		!	LoadConnTx/OnFire						LoadConnDctTx/Key
		!	IsSshVal/OnEqual						LoadConnDctTx/Fire

	% MISCN: Write error
	$	ValueWriteErr Misc.Dist {}
		!	LoadConnDctTx/OnFire					ValueWriteErr/Value
		!	StoreValErr/OnFire					ValueWriteErr/Fire

	% ADTN: Debug
%	$	LoadDebugTx Adt.Load { Key Value }
%		!	LoadValTx/OnFire						Debug/Fire
%		!	LoadValTx/OnFire						LoadDebugTx/Dictionary
%		!	LoadValTx/OnFire						LoadDebugTx/Fire
%		!	LoadDebugTx/OnFire					Debug/Fire

	%% Save value to channel directly

	% ADTN: Load channel context
	$	LoadChTx Adt.Load { Key Channel }
		!	LoadConnDctTx/OnFire					LoadChTx/Dictionary
		!	LoadConnDctTx/OnFire					LoadChTx/Fire

	% SSHN: Obtain a stream for persistence
	$	ChStm Ssh.Channel {}
		!	LoadChTx/OnFire						ChStm/Channel
		!	LoadChTx/OnFire						ChStm/Stream

	% ION: Persist value to channel
	$	Save Io.Persist {}
		!	CreatePrsr/OnFire						Save/Parser
		!	ChStm/OnStream							Save/Stream
		!	LoadValTx/OnFire						Save/Value
		!	LoadValTx/OnFire						Save/Save
%		!	Save/OnSave								Debug/Fire
%		!	Save/OnError							Debug/Fire

	% Error conditions
	!	LoadConnTx/OnNotFound					StoreValErr/Fire
	!	LoadConnDctTx/OnNotFound				StoreValErr/Fire
	!	Save/OnError								StoreValErr/Fire

	% Latent connections
	!	LoadConnDctTx/OnFire						LoadValTx/Fire
	!	LoadConnTx/OnFire							ProtSubVal/Substring
	!	OnTransmit/OnFire							LoadConnTx/Fire

	%%%%%%%%
	% Reads
	%%%%%%%%

	%% Label the incoming values with the connection string from whence they came.
%	!	Reads/State/OnChannel/OnFire			Debug/Fire
%	!	Reads/State/OnRead/OnFire				Debug/Fire

	%% Parse out incoming values

	% ION: Persist value from channel
	$	Load Io.Persist {}
		!	CreatePrsr/OnFire						Load/Parser
		!	Reads/State/OnRead/OnFire			Load/Stream
		!	Reads/State/OnRead/OnFire			Load/Load
%		!	Load/OnError							Debug/Fire

	% ADTN: Debug
%	$	LoadDebugRx Adt.Load { Key Value }
%		!	Load/OnLoad								Debug/Fire
%		!	Load/OnLoad								LoadDebugRx/Dictionary
%		!	Load/OnLoad								LoadDebugRx/Fire
%		!	LoadDebugRx/OnFire					Debug/Fire

	%% Deliver value to transport layer

	% MISCN: Create a dictionary for the value descriptor
	$	CreateDctRx Misc.Create { Id Adt.Dictionary }
		!	Load/OnLoad								CreateDctRx/Fire

	% ADTN: Store loaded value in descriptor
	$	StoreDctRx Adt.Keys { Keys ( Connection Value ) }
		!	CreateDctRx/OnFire					StoreDctRx/Dictionary
		!	Load/OnLoad								StoreDctRx/Value

	% ADTN: Load address from whence the value came
	$	LoadConnRx Adt.Load { Key Destination }
		!	Reads/State/OnChannel/OnFire		LoadConnRx/Dictionary
		!	Load/OnLoad								LoadConnRx/Fire

	% MISCN: Prepend source transport
	$	AppendSshRx Math.Binary { Left "ssh://" }
		!	LoadConnRx/OnFire						AppendSshRx/Right
		!	LoadConnRx/OnFire						AppendSshRx/Fire

	% Store connection and notify parent
	!	AppendSshRx/OnFire						StoreDctRx/Connection
	!	AppendSshRx/OnFire						StoreDctRx/Store
	!	StoreDctRx/OnStore						State/Transport/OnValue/Fire

	% MISCN: Socket read error
	$	ValueReadErr Misc.Dist {}
		!	Reads/State/OnChannel/OnFire		ValueReadErr/Value
		!	Reads/State/OnError/OnFire			ValueReadErr/Fire
		!	Load/OnError							ValueReadErr/Fire

	%%%%%%%%
	% Close
	%%%%%%%%

	%% Disconnects from the other side will result in errors
	%% from the socket reader.

	% MISCN: Socket closure
	$	OnClose Misc.Dist {}
		!	OnClose/OnFire							DebugDisC/Fire
		!	ValueReadErr/OnFire					OnClose/Fire
		!	ValueWriteErr/OnFire					OnClose/Fire

	% Tell reader in case someome else generated error
	!	OnClose/OnFire								Reads/State/Remove/Fire

	% ADTN: Load address associated with remote end
	$	LoadCloseAddr Adt.Load { Key Destination }
		!	OnClose/OnFire							LoadCloseAddr/Dictionary
		!	OnClose/OnFire							LoadCloseAddr/Fire

	% Let connection handler know in case it was an outgoing connection
%	!	LoadCloseAddr/OnFire						DebugDisC/Fire
	!	LoadCloseAddr/OnFire						Connect/State/Disconnected/Fire
%	!	LoadCloseAddr/OnFire						DebugDisC/Fire

	% MISCN: Prepend source transport
	$	AppendSshDis Math.Binary { Left "ssh://" }
		!	LoadCloseAddr/OnFire					AppendSshDis/Right
		!	LoadCloseAddr/OnFire					AppendSshDis/Fire
%		!	AppendSshDis/OnFire					Debug/Fire

	% ADTN: Remove connection from internal dictionary
	$	RemoveConn Adt.Remove {}
		!	CreateConnsDct/OnFire				RemoveConn/Container
		!	AppendSshDis/OnFire					RemoveConn/Key
		!	AppendSshDis/OnFire					RemoveConn/Fire
%		!	RemoveConn/OnFire						Debug/Fire

	% Announce closure to transport
	!	AppendSshDis/OnFire						DebugDisC/Fire
	!	AppendSshDis/OnFire						State/Transport/OnDisconnect/Fire

