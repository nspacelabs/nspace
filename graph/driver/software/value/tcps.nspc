%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								 TCPS.NSPC
%
%						nSpace SSL TCP/IP value driver
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications
	$	OnStart				Misc.Dist {}		% Start I/o

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire
		!	Uninitialize/OnFire					Debug/Fire
	$	DebugConn Misc.Debug {}
	$	DebugDisC Misc.Debug {}

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: This state
	#	State											State/Driver/Software/Value/Tcp/ {}
%		!	State/Port/OnFire						Debug/Fire

	% Subgraph: SSL server
%	#	Ssl											Driver/Software/Comm/Network/Ip/Tcp/Server/ {}
	#	Ssl											Driver/Software/Comm/Network/Ip/Tcp/SslServer/ {}
		!	State/Port/OnFire						Ssl/State/Port/Fire

	% MISCN: Run states
	$	ValueRunIdle Misc.Dist { Value Idle }
		!	Initialize/OnFire						ValueRunIdle/Fire
		!	ValueRunIdle/OnFire					State/Transport/Run/Fire
	$	ValueRunRun Misc.Dist { Value Run }
		!	ValueRunRun/OnFire					State/Transport/Run/Fire

	% MISCN: Run on/off
	$	ValueRunT Misc.Dist { Value true:bool }
		!	ValueRunT/OnFire						Ssl/State/Run/Fire
	$	ValueRunF Misc.Dist { Value false:bool }
		!	ValueRunF/OnFire						Ssl/State/Run/Fire

	% MATHN: Append transport postfix to instance name	
	$	AppendPost Math.Binary { Right "://" }
		!	Initialize/OnFire						AppendPost/Left
		!	Initialize/OnFire						AppendPost/Fire

	% MATHN: Length of transport prefix
	$	TransLen Misc.StringOp {}
		!	AppendPost/OnFire						TransLen/Source
		!	AppendPost/OnFire						TransLen/Length

	% MISCN: Create parser for values
	$	CreatePrsr Misc.Create { Id "Io.StmPrsBin" }
		!	State/Parser/OnFire					CreatePrsr/Id
		!	State/Parser/OnFire					CreatePrsr/Fire

	%%%%%%
	% Ssl
	%%%%%%

	%% Create SSL security context for server.

	% MISCN: Security context
	$	ValueSecDct Misc.Dist { Value {
			Certificate	"C:\\sdks\\cygwin64\\home\\jhoy\\ssl\\camera.crt"
			PrivateKey	"C:\\sdks\\cygwin64\\home\\jhoy\\ssl\\camera.priv"
			} }
		!	Initialize/OnFire						ValueSecDct/Fire

	% OPENSSL: Initialize the security context
	$	SslCtx OpenSsl.SocketOp {}
		!	ValueSecDct/OnFire					SslCtx/Context
		!	SslCtx/OnContext						Ssl/State/Secure/Fire

	%%%%%%%%%%%
	% Run/Stop
	%%%%%%%%%%%

	% MISCN: Demux status changes
	$	DecodeRun Misc.Decode { Values ( Server Client Stop ) }
		!	State/Transport/Run/OnFire			DecodeRun/Select
		!	State/Transport/Run/OnFire			DecodeRun/Fire

	% Signal start up
	!	DecodeRun/OnClient						ValueRunT/Fire
	!	DecodeRun/OnServer						ValueRunT/Fire
	!	ValueRunT/OnFire							ValueRunRun/Fire

	% MISCN: Create a dictionary to keep track of connections
	$	CreateConnsDct Misc.Create { Id Adt.Dictionary }
		!	ValueRunT/OnFire						CreateConnsDct/Fire

	% MISCN: Create a dictionary to keep track of aliases (hostnames)
	$	CreateAliasDct Misc.Create { Id Adt.Dictionary }
		!	ValueRunT/OnFire						CreateAliasDct/Fire

	% Shutdown
	!	DecodeRun/OnStop							ValueRunF/Fire

	%%%%%%%%%%%%%%
	% Connections
	%%%%%%%%%%%%%%

	%% Established connections

	% MISCN: Connection established
	$	OnConnect Misc.Dist {}
		!	Ssl/State/OnConnect/OnFire			OnConnect/Fire

	% ADTN: Connection will be identified on the remote address
	$	LoadConnRem Adt.Load { Key Remote }
		!	OnConnect/OnFire						LoadConnRem/Dictionary
		!	LoadConnRem/OnFire					DebugConn/Fire

	% MATHN: Prepend the transport for an official connection string
	$	AppendTcp Math.Binary {}
		!	AppendPost/OnFire						AppendTcp/Left
		!	LoadConnRem/OnFire					AppendTcp/Right

	% ADTN: Load the original alias for this connection
	$	LoadAliasCn Adt.Load {}
		!	CreateAliasDct/OnFire				LoadAliasCn/Dictionary
		!	LoadConnRem/OnFire					LoadAliasCn/Key
		!	LoadConnRem/OnFire					LoadAliasCn/Fire

	% Generate connection string
	!	LoadAliasCn/OnFire						AppendTcp/Right
	!	LoadConnRem/OnFire						AppendTcp/Fire

	% ADTN: Store connection under address if new
	$	StoreConn Adt.Store {}
		!	CreateConnsDct/OnFire				StoreConn/Dictionary
		!	OnConnect/OnFire						StoreConn/Value
		!	AppendTcp/OnFire						StoreConn/Key
		!	AppendTcp/OnFire						StoreConn/Fire
%		!	StoreConn/OnFire						Debug/Fire

	% Connection established
	!	AppendTcp/OnFire							Debug/Fire
	!	AppendTcp/OnFire							State/Transport/OnConnect/Fire

	% Latent connections
	!	OnConnect/OnFire							LoadConnRem/Fire

	%% Check if connection request applies to this instance.

	% MISCN: Extract protocol from string
	$	ProtSubConn Misc.StringOp {}
		!	State/Transport/Connect/OnFire	ProtSubConn/Source
		!	TransLen/OnFire						ProtSubConn/To

	% MISCN: Match this transport ?
	$	IsTcpConn Misc.Compare {}
		!	AppendPost/OnFire						IsTcpConn/Left
		!	ProtSubConn/OnFire					IsTcpConn/Fire

	% MISCN: Extract address/port
	$	AddrSubConn Misc.StringOp {}
		!	State/Transport/Connect/OnFire	AddrSubConn/Source
		!	TransLen/OnFire						AddrSubConn/From
		!	IsTcpConn/OnEqual						AddrSubConn/Substring

	%% Aliases.  The requestor can specify a hostname instead of a direct
	%% IP address under which the connection name is known.  Store
	%% original hostname for reverse look-up later for outgoing dictionaries.

	% ADTN: Store the resolved IP address vs. original connection name
	$	StoreAliasDct Adt.Store {}
		!	CreateAliasDct/OnFire				StoreAliasDct/Dictionary
		!	AddrSubConn/OnFire					StoreAliasDct/Value

	% NET: Resolve provided IP address into ip number
	$	ResolveConn Net.Address {}

	% Resolve and "stringify" the address/port for usage in connection dictionaries
	!	AddrSubConn/OnFire						ResolveConn/Address
	!	AddrSubConn/OnFire						ResolveConn/String

	% Store resolved address in dictionary
	!	ResolveConn/OnString						StoreAliasDct/Key
	!	ResolveConn/OnString						StoreAliasDct/Fire

	% Add a new client connection
	!	ResolveConn/OnString						Debug/Fire
	!	ResolveConn/OnString						Ssl/State/Connect/Fire
	!	State/Transport/Connect/OnFire		ProtSubConn/Substring

	%%%%%%%%%%%%%
	% Disconnect
	%%%%%%%%%%%%%

	% MISCN: Lost connection
	$	OnDisconnect Misc.Dist {}
		!	Ssl/State/OnDisconnect/OnFire		OnDisconnect/Fire

	% ADTN: Connection identified by the remote address
	$	LoadConnDis Adt.Load { Key Remote }
		!	OnDisconnect/OnFire					LoadConnDis/Dictionary
		!	OnDisconnect/OnFire					LoadConnDis/Fire

	% MISCN: Prepend source transport
	$	AppendTcpDis Math.Binary {}
		!	AppendPost/OnFire						AppendTcpDis/Left
		!	LoadConnDis/OnFire					AppendTcpDis/Right
		!	LoadConnDis/OnFire					AppendTcpDis/Fire

	% ADTN: Remove connection from internal dictionary
	$	RemoveConn Adt.Remove {}
		!	CreateConnsDct/OnFire				RemoveConn/Container
		!	AppendTcpDis/OnFire					RemoveConn/Key
		!	AppendTcpDis/OnFire					RemoveConn/Fire
%		!	RemoveConn/OnFire						Debug/Fire

	% Announce closure to transport
	!	AppendTcpDis/OnFire						DebugDisC/Fire
	!	AppendTcpDis/OnFire						State/Transport/OnDisconnect/Fire

	%%%%%%%%%%%
	% Transmit
	%%%%%%%%%%%

	%% TODO: Asynchronous transmissions ?
%	!	State/Transport/Value/OnFire			Debug/Fire

	% MISCN: Transmit value
	$	OnTransmit Misc.Dist {}
		!	State/Transport/Value/OnFire		OnTransmit/Fire

	% MISCN: Demux value specifications
	$	LoadConnTx Adt.Load { Key Connection }
		!	OnTransmit/OnFire						LoadConnTx/Dictionary
	$	LoadValTx Adt.Load { Key Value }
		!	OnTransmit/OnFire						LoadValTx/Dictionary
%		!	LoadValTx/OnFire						Debug/Fire
	$	StoreValErr Adt.Store { Key Error Value true:bool }
		!	OnTransmit/OnFire						StoreValErr/Dictionary

	% MISCN: Extract protocol for this transport
	$	ProtSubVal Misc.StringOp {}
		!	LoadConnTx/OnFire						ProtSubVal/Source
		!	TransLen/OnFire						ProtSubVal/To

	% MISCN: Match ?
	$	IsTcpVal Misc.Compare {}
		!	AppendPost/OnFire						IsTcpVal/Left
		!	ProtSubVal/OnFire						IsTcpVal/Fire

	% ADTN: Is connection string valid ?  Load its dictionary.
	$	LoadConnDctTx Adt.Load {}
		!	CreateConnsDct/OnFire				LoadConnDctTx/Dictionary
		!	LoadConnTx/OnFire						LoadConnDctTx/Key
		!	IsTcpVal/OnEqual						LoadConnDctTx/Fire

	% MISCN: Socket read error
	$	ValueWriteErr Misc.Dist {}
		!	LoadConnDctTx/OnFire					ValueWriteErr/Value
		!	StoreValErr/OnFire					ValueWriteErr/Fire

	% ADTN: Debug
%	$	LoadDebugTx Adt.Load { Key Value }
%		!	LoadValTx/OnFire						Debug/Fire
%		!	LoadValTx/OnFire						LoadDebugTx/Dictionary
%		!	LoadValTx/OnFire						LoadDebugTx/Fire
%		!	LoadDebugTx/OnFire					Debug/Fire

	%% Save value to socket directly

	% ADTN: Load the socket stream
	$	LoadStmTx Adt.Load { Key Stream }
		!	LoadConnDctTx/OnFire					LoadStmTx/Dictionary
		!	LoadConnDctTx/OnFire					LoadStmTx/Fire

	% ION: Persist value to the stream
	$	Save Io.Persist {}
		!	CreatePrsr/OnFire						Save/Parser
		!	LoadStmTx/OnFire						Save/Stream
		!	LoadValTx/OnFire						Save/Value
		!	LoadValTx/OnFire						Save/Save
		!	Save/OnError							Debug/Fire
%		!	Save/OnSave								Debug/Fire

	% ION: Complete write of value
	$	FlushTx Io.StreamOp {}
		!	LoadStmTx/OnFire						FlushTx/Stream
		!	LoadValTx/OnFire						FlushTx/Flush

	% Error conditions
	!	LoadConnTx/OnNotFound					StoreValErr/Fire
%	!	LoadConnDctTx/OnNotFound				StoreValErr/Fire
	!	Save/OnError								StoreValErr/Fire

	% Transmit value for valid client
	!	LoadConnDctTx/OnFire						LoadValTx/Fire

	% Latent connections
	!	LoadValTx/OnFire							LoadStmTx/Fire
	!	LoadConnTx/OnFire							ProtSubVal/Substring
	!	OnTransmit/OnFire							LoadConnTx/Fire

	%%%%%%%%
	% Reads
	%%%%%%%%

	%% Label the incoming values with the connection string from whence they came.
%	!	Ssl/State/OnRead/OnFire					Debug/Fire

	% MISCN: Read context
	$	OnRead Misc.Dist {}
		!	Ssl/State/OnRead/OnFire				OnRead/Fire

	% MISCN: On an error
	$	ValueErrRd Misc.Dist {}
		!	OnRead/OnFire							ValueErrRd/Value
		!	ValueErrRd/OnFire						Ssl/State/Error/Fire

	%% Parse out incoming values

	% ADTN: Load the socket stream
	$	LoadStmRx Adt.Load { Key Stream }
		!	OnRead/OnFire							LoadStmRx/Dictionary

	% ION: Load value from stream
	$	Load Io.Persist {}
		!	CreatePrsr/OnFire						Load/Parser
		!	LoadStmRx/OnFire						Load/Stream
		!	LoadStmRx/OnFire						Load/Load
%		!	Load/OnLoad								Debug/Fire
		!	Load/OnError							Debug/Fire

	% MISCN: Fix this, sometimes OpenSSL generates an SSL_ERROR_SYSCALL error
	% on link startup.  Seems harmless as future reads work.  Ignore this
	% for now until fixed.
	$	IsRdErr Misc.Compare { Left 5:int }
		!	Load/OnError							IsRdErr/Fire

	% Read error
	!	IsRdErr/OnNotEqual						ValueErrRd/Fire

	% ADTN: Debug
%	$	LoadDebugRx Adt.Load { Key Value }
%		!	Load/OnLoad								Debug/Fire
%		!	Load/OnLoad								LoadDebugRx/Dictionary
%		!	Load/OnLoad								LoadDebugRx/Fire
%		!	LoadDebugRx/OnFire					Debug/Fire

	%% Deliver value to transport layer

	% MISCN: Create a dictionary for the value descriptor
	$	CreateDctRx Misc.Create { Id Adt.Dictionary }
		!	Load/OnLoad								CreateDctRx/Fire

	% ADTN: Store loaded value in descriptor
	$	StoreDctRx Adt.Keys { Keys ( Connection Value ) }
		!	CreateDctRx/OnFire					StoreDctRx/Dictionary
		!	Load/OnLoad								StoreDctRx/Value

	% ADTN: Load the address
	$	LoadRemRx Adt.Load { Key Remote }
		!	OnRead/OnFire							LoadRemRx/Dictionary
		!	Load/OnLoad								LoadRemRx/Fire

	% MISCN: Prepend source transport
	$	AppendTcpRx Math.Binary {}
		!	AppendPost/OnFire						AppendTcpRx/Left
		!	LoadRemRx/OnFire						AppendTcpRx/Right
		!	LoadRemRx/OnFire						AppendTcpRx/Fire
		!	AppendTcpRx/OnFire					StoreDctRx/Connection

	% ADTN: Load the original alias for this connection if it exists 
	% (only would exist for outgoing connections)
	$	LoadAliasRx Adt.Load {}
		!	CreateAliasDct/OnFire				LoadAliasRx/Dictionary
		!	OnRead/OnFire							LoadAliasRx/Key
		!	Load/OnLoad								LoadAliasRx/Fire

	% Use un-aliased name
	!	LoadAliasRx/OnFire						AppendTcpRx/Right
	!	LoadAliasRx/OnFire						AppendTcpRx/Fire

	% Store connection and notify parent
%	!	AppendTcpRx/OnFire						Debug/Fire
	!	Load/OnLoad									StoreDctRx/Store
%	!	StoreDctRx/OnStore						Debug/Fire
	!	StoreDctRx/OnStore						State/Transport/OnValue/Fire

	% Latent connections
	!	OnRead/OnFire								LoadStmRx/Fire


