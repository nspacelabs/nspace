%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								Date.NSPC
%
%			Temporal context location by date driver 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: State for this instance
	#	State											State/Driver/Software/Temporal/Value/Date/ {}

	% Subgraph: Temporal context
	#	Context										State/Driver/Software/Temporal/Value/Context/ {}

	% MISCN: Set temporal with to one
	$	ValueWidth1 Misc.Dist { Value 1:int }
		!	ValueWidth1/OnFire					Context/Width/Fire

	%%%%%%%%%%%
	% Temporal
	%%%%%%%%%%%

	% NSPCN: Temporal operations
	$	TemporalOp Nspc.TemporalOp { ReadOnly true:bool }
		!	Context/Temporal/OnFire				TemporalOp/Temporal
		!	Context/Temporal/OnFire				TemporalOp/Open
		!	State/Index/OnFire					TemporalOp/Location
		!	State/Index/OnFire					TemporalOp/Open
%		!	TemporalOp/OnDate						Debug/Fire

	% Sequence start
	!	State/OnSeqB/OnFire						Context/Sequence/Fire

	% At beginning
	!	State/OnSeqB/OnFire						Context/At/Fire

	% MATHN: Compute width of context window
	$	SeqSub Math.Binary { Op Sub }
		!	State/OnSeqB/OnFire					SeqSub/Right
		!	State/OnSeqE/OnFire					SeqSub/Left
		!	State/OnSeqE/OnFire					SeqSub/Fire
	$	SeqAdd Math.Binary { Op Add Right 1:int }
		!	SeqSub/OnFire							SeqAdd/Left
		!	SeqSub/OnFire							SeqAdd/Fire

	% Width
	!	SeqAdd/OnFire								Context/Width/Fire

	% At end
	!	State/OnSeqE/OnFire						Context/At/Fire

	%%%%%%
	% End
	%%%%%%

	%% Given an ending sequence, find start sequence using the configured delta T.
	%% TODO: In future, 'GoTo' date can be used once temporal database has efficient
	%% way of indexing dates.

	% MISCN: Ending sequence
	$	OnEnd Misc.Dist {}

	% MISCN: Create a dictionary for statistics
	$	CreateStatDct Misc.Create { Id Adt.Dictionary }
		!	OnEnd/OnFire							CreateStatDct/Fire

	% ADTN: Collect statistics
	$	StatDct Adt.Keys { Keys ( DateBeg DateEnd SeqBeg SeqEnd ) }
		!	CreateStatDct/OnFire					StatDct/Dictionary
%		!	StatDct/OnStore						Debug/Fire

	% Default beginning sequence number
	!	OnEnd/OnFire								StatDct/SeqBeg

	% MATHN: One less for default beginning sequence # to indiciate 'zero'
	$	EndSeqSub Math.Binary { Op Sub Right 1:int }
		!	OnEnd/OnFire							EndSeqSub/Left
		!	OnEnd/OnFire							EndSeqSub/Fire
		!	EndSeqSub/OnFire						StatDct/SeqEnd

	% Load date of whatever value is at sequence number
	!	OnEnd/OnFire								Debug/Fire
	!	OnEnd/OnFire								TemporalOp/Load
	!	TemporalOp/OnDate							Debug/Fire
	!	OnEnd/OnFire								Debug/Fire

	% MISCN: End date
	$	ValueEndDate Misc.Dist {}
		!	TemporalOp/OnDate						ValueEndDate/Value
		!	OnEnd/OnFire							ValueEndDate/Fire
		!	ValueEndDate/OnFire					StatDct/DateEnd

	% MATHN: Subtract delta T from the ending data to get ideal start
	$	EndSub Math.Binary { Op Sub }
		!	State/Delta/OnFire					EndSub/Right
		!	TemporalOp/OnDate						EndSub/Left
		!	OnEnd/OnFire							EndSub/Fire
		!	EndSub/OnFire							Debug/Fire

	% Initialize defaults
	!	EndSub/OnFire								StatDct/DateBeg
	!	EndSub/OnFire								StatDct/Store

	% ADTN: Search for index value that is later than start
	$	GoToEnd Adt.Iterate {}
		!	TemporalOp/OnOpen						GoToEnd/Container
		!	OnEnd/OnFire							GoToEnd/Goto
		!	OnEnd/OnFire							GoToEnd/Next
%		!	GoToEnd/OnKey							Debug/Fire
%		!	GoToEnd/OnNext							Debug/Fire

	% Possible beginning values
	!	TemporalOp/OnDate							StatDct/DateBeg
	!	GoToEnd/OnKey								StatDct/SeqBeg

	% Obtain date for sequence number
	!	GoToEnd/OnKey								TemporalOp/Load

	% MISCN: Date still valid ?
	$	IsDateEnd Misc.Compare {}
		!	EndSub/OnFire							IsDateEnd/Left
		!	TemporalOp/OnDate						IsDateEnd/Right
		!	GoToEnd/OnKey							IsDateEnd/Fire

	% Date still okay, continue
	!	IsDateEnd/OnLess							StatDct/Store
	!	IsDateEnd/OnLess							GoToEnd/Next

	% End condition, date reached or end of data.
	!	IsDateEnd/OnEqual							StatDct/Load
	!	IsDateEnd/OnGreater						StatDct/Load
	!	GoToEnd/OnLast								StatDct/Load

	% Update state
	!	StatDct/OnDateBeg							State/OnDateB/Fire
	!	StatDct/OnDateEnd							State/OnDateE/Fire
	!	StatDct/OnSeqBeg							State/OnSeqB/Fire
	!	StatDct/OnSeqEnd							State/OnSeqE/Fire

	%%%%%%%%
	% Begin
	%%%%%%%%

	%% Given a beginning sequence, find ending sequence using the configured delta T.

	% MISCN: Beginning sequence
	$	OnBegin Misc.Dist {}

	% Create dictionary
	!	OnBegin/OnFire								CreateStatDct/Fire

	% New beginning sequence number
	!	OnBegin/OnFire								StatDct/SeqBeg

	% MATHN: One less for default beginning sequence # to indiciate 'zero'
	$	BegSeqSub Math.Binary { Op Sub Right 1:int }
		!	OnBegin/OnFire							BegSeqSub/Left
		!	OnBegin/OnFire							BegSeqSub/Fire
		!	BegSeqSub/OnFire						StatDct/SeqEnd

	% Load whatever value is at sequence number
	!	OnBegin/OnFire								TemporalOp/Load

	% MISCN: End date
	$	ValueBegDate Misc.Dist {}
		!	TemporalOp/OnDate						ValueBegDate/Value
		!	OnBegin/OnFire							ValueBegDate/Fire
		!	ValueBegDate/OnFire					StatDct/DateBeg

	% MATHN: Add delta T to the beginning data to get ideal end
	$	BegAdd Math.Binary { Op Add }
		!	State/Delta/OnFire					BegAdd/Right
		!	TemporalOp/OnDate						BegAdd/Left
		!	OnBegin/OnFire							BegAdd/Fire
		!	BegAdd/OnFire							Debug/Fire

	% Initialize defaults
	!	BegAdd/OnFire								StatDct/DateEnd
	!	BegAdd/OnFire								StatDct/Store

	% ADTN: Search for index value that is later than start
	$	GoToBeg Adt.Iterate {}
		!	TemporalOp/OnOpen						GoToBeg/Container
		!	OnBegin/OnFire							GoToBeg/Goto
		!	OnBegin/OnFire							GoToBeg/Previous
%		!	GoToBeg/OnKey							Debug/Fire
%		!	GoToBeg/OnNext							Debug/Fire

	% Possible ending values
	!	TemporalOp/OnDate							StatDct/DateEnd
	!	GoToBeg/OnKey								StatDct/SeqEnd

	% Obtain date for sequence number
	!	GoToBeg/OnKey								TemporalOp/Load

	% MISCN: Date still valid ?
	$	IsDateBeg Misc.Compare {}
		!	BegAdd/OnFire							IsDateBeg/Left
		!	TemporalOp/OnDate						IsDateBeg/Right
		!	GoToBeg/OnKey							IsDateBeg/Fire

	% Date still okay, continue
	!	IsDateBeg/OnGreater						StatDct/Store
	!	IsDateBeg/OnGreater						GoToBeg/Previous

	% End condition, date reached or end of data.
	!	IsDateBeg/OnEqual							StatDct/Load
	!	IsDateBeg/OnLess							StatDct/Load
	!	GoToBeg/OnFirst							StatDct/Load

	%%%%%%%%%
	% Latest
	%%%%%%%%%

	%% Begin will set the context to the newest values of specified interval.

	% Shrink with to one while limits are changing
	!	State/Latest/OnFire						ValueWidth1/Fire

	% Retrieve the lastest limits
	!	State/Latest/OnFire						TemporalOp/Limits

	% MISCN: Sequence number of latest entry
	$	ValueLatestSeq Misc.Dist {}
		!	TemporalOp/OnLast						ValueLatestSeq/Value
		!	State/Latest/OnFire					ValueLatestSeq/Fire
		!	ValueLatestSeq/OnFire				OnEnd/Fire

	%%%%%%%%%%%
	% Previous
	%%%%%%%%%%%

	% MISCN: Move to previous sequence number as new end
	$	PrevSub Math.Binary { Op Sub Right 1:long }
		!	State/OnSeqB/OnFire					PrevSub/Left
		!	State/Prev/OnFire						PrevSub/Fire

	% Set as new end sequence
	!	PrevSub/OnFire								OnEnd/Fire

	%%%%%%%
	% Next
	%%%%%%%

	% MISCN: Move to next sequence number as new beginning
	$	NextAdd Math.Binary { Op Add Right 1:long }
		!	State/OnSeqE/OnFire					NextAdd/Left
		!	State/Next/OnFire						NextAdd/Fire

	% Set as new end sequence
	!	NextAdd/OnFire								OnBegin/Fire
