%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								FinderLocal.NSPC
%
%					Local namespace finder driver.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: State for this instance
	#	State											State/Driver/Software/nSpace/Finder/ {}
%		!	State/OnResult/OnFire				Debug/Fire

	% NSPC: Enable/disable controls
	$	StoreRunEn Adt.Store { Key Enable }
		!	State/Search/OnDescriptor			StoreRunEn/Dictionary
		!	StoreRunEn/OnFire						State/Search/Descriptor
	$	RunEnT Misc.Dist { Value true:bool }
		!	RunEnT/OnFire							StoreRunEn/Fire
		!	Initialize/OnFire						RunEnT/Fire
	$	RunEnF Misc.Dist { Value false:bool }
		!	RunEnF/OnFire							StoreRunEn/Fire
	$	StoreStopEn Adt.Store { Key Enable }
		!	State/Cancel/OnDescriptor			StoreStopEn/Dictionary
		!	StoreStopEn/OnFire					State/Cancel/Descriptor
	$	StopEnT Misc.Dist { Value true:bool }
		!	StopEnT/OnFire							StoreStopEn/Fire
	$	StopEnF Misc.Dist { Value false:bool }
		!	StopEnF/OnFire							StoreStopEn/Fire
		!	Initialize/OnFire						StopEnF/Fire

	%%%%%%%%%
	% Search
	%%%%%%%%%

	%% Recurisvely search the specified namespace for the specified criteria.

	% MISCN: Perform search synchronously because local
	$	SearchL Misc.Dist {}

	% Flip controls
	!	SearchL/OnFire								RunEnF/Fire
	!	SearchL/OnFire								StopEnT/Fire

	% MISCN: Create dictionary for results
	$	CreateResLst Misc.Create { Id Adt.List }
		!	SearchL/OnFire							CreateResLst/Fire

	% MISCN: Send results to state when complete
	$	ValueResLst Misc.Dist {}
		!	CreateResLst/OnFire					ValueResLst/Value
		!	ValueResLst/OnFire					State/OnResult/Fire

	% MISCN: Create a queue for remaining locations to search
	$	CreateLocQ Misc.Create { Id Adt.Queue }
		!	SearchL/OnFire							CreateLocQ/Fire

	% MISCN: Add location to queue
	$	WriteLocQ Adt.Write {}
		!	CreateLocQ/OnFire						WriteLocQ/List

	% MISCN: Search root
	$	ValueRootSrch Misc.Dist {}
		!	State/Root/OnFire						ValueRootSrch/Value
		!	SearchL/OnFire							ValueRootSrch/Fire
		!	ValueRootSrch/OnFire					WriteLocQ/Fire

	% ADTN: Iterate and search next location
	$	LocQIt Adt.Iterate {}
		!	CreateLocQ/OnFire						LocQIt/Container
		!	SearchL/OnFire							LocQIt/First
%		!	LocQIt/OnNext							Debug/Fire

	% NSPC: Load the location from the local namespace
	$	LoadFrom Nspc.This {}
		!	LocQIt/OnNext							LoadFrom/Location

	%%%%%%%%%
	% Loaded
	%%%%%%%%%

	%% Certain locations will be added to the queue for further querying

	% MATHN: Append the sublocation to the current location and queue it
	$	AppendLdLoc Math.Binary {}
		!	LocQIt/OnNext							AppendLdLoc/Left
	$	AppendLdLoc_ Math.Binary { Right "/" }
		!	AppendLdLoc/OnFire					AppendLdLoc_/Left
		!	AppendLdLoc/OnFire					AppendLdLoc_/Fire
%		!	AppendLdLoc_/OnFire					Debug/Fire

	% MISCN: No need to traverse the 'ref' branch
	$	IsRef Misc.Compare { Left "/ref/" }
		!	AppendLdLoc_/OnFire					IsRef/Fire

	% MISCN: Operation cancelled ?
	$	CancelBool Misc.Toggle {}
		!	State/Search/OnFire					CancelBool/False
		!	Initialize/OnFire						CancelBool/True
		!	IsRef/OnNotEqual						CancelBool/Fire
		!	CancelBool/OnFalse					WriteLocQ/Fire

	% ADTN: Iterate the entries at the location
	$	LdValIt Adt.Iterate {}
		!	LoadFrom/OnLoad						LdValIt/Container
		!	LoadFrom/OnLoad						LdValIt/First
%		!	LdValIt/OnKey							Debug/Fire
%		!	LdValIt/OnNext							Debug/Fire

	% Possible location to queue
	!	LdValIt/OnKey								AppendLdLoc/Right

	% MISCN: Skip own parent
	$	IsParent Misc.Compare { Left ".Parent" }
		!	LdValIt/OnKey							IsParent/Right

	% MISCN: Is item a dictionary ?
	$	IsDict Misc.Type { Type Dictionary }
		!	LdValIt/OnNext							IsDict/Value
		!	IsParent/OnNotEqual					IsDict/Fire

	%%
	%% nSpace value.
	%%

	% ADTN: Check for a descriptor dictionary (node)
	$	LoadDescChk Adt.Load { Key ".Descriptor" }
		!	IsDict/OnEqual							LoadDescChk/Dictionary
		
	% ADTN: Load the behaviour for the item
	$	LoadBehave Adt.Load { Key ".Behaviour" }
		!	LoadDescChk/OnFire					LoadBehave/Dictionary
		!	LoadDescChk/OnFire					LoadBehave/Fire

	% MISCN: nSpace value ?
	$	IsValn Misc.Compare { Left "nspc.value" }
		!	LoadBehave/OnFire						IsValn/Fire

	% MATHN: Append name to generate full path
	$	AppendValName Math.Binary {}
		!	LocQIt/OnNext							AppendValName/Left
		!	LdValIt/OnKey							AppendValName/Right
		!	IsValn/OnEqual							AppendValName/Fire
%		!	AppendValName/OnFire					Debug/Fire

	% ADTN: Store full path of item in results
	$	StoreResLst Adt.Write {}
		!	CreateResLst/OnFire					StoreResLst/List
		!	AppendValName/OnFire					StoreResLst/Fire
%	$	StoreResDct Adt.Store { Value 0:int }
%		!	CreateResDct/OnFire					StoreResDct/Dictionary
%		!	AppendValName/OnFire					StoreResDct/Key
%		!	AppendValName/OnFire					StoreResDct/Fire
%		!	StoreResDct/OnFire					Debug/Fire

	%%
	%% Folder.
	%%

	% ADTN: Check for a parent (folder)
	$	LoadParent Adt.Load { Key ".Parent" }
		!	IsDict/OnEqual							LoadParent/Dictionary
		!	LoadDescChk/OnNotFound				LoadParent/Fire
		!	LoadBehave/OnNotFound				LoadParent/Fire
		!	IsValn/OnNotEqual						LoadParent/Fire

	% Folder, continue processing
	!	LoadParent/OnFire							AppendLdLoc/Fire

	% Latent connections
	!	IsDict/OnEqual								LoadDescChk/Fire
	!	LdValIt/OnNext								IsParent/Fire

	% Proceed to next sub-location
	!	LdValIt/OnNext								LdValIt/Next

	% Location loaded, proceed to next location on queue
	!	LoadFrom/OnLoad							LocQIt/Next

	% Latent connections
	!	LocQIt/OnNext								LoadFrom/Load

	%%%%%%%%%%%
	% Complete
	%%%%%%%%%%%

	% MISCN: Search complete
	$	OnDone Misc.Dist {}
		!	LocQIt/OnLast							OnDone/Fire
		!	State/Cancel/OnFire					OnDone/Fire

	% Flip controls
	!	OnDone/OnFire								RunEnT/Fire
	!	OnDone/OnFire								StopEnF/Fire

	% Results
%	!	OnDone/OnFire								ValueResDct/Fire
	!	OnDone/OnFire								ValueResLst/Fire

	% When queue is empty, no more locations
%	!	LocQIt/OnLast								Debug/Fire
%	!	LocQIt/OnLast								Debug/Fire

	% Initiate search
	!	State/Search/OnFire						SearchL/Fire



