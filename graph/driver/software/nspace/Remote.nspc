%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								REMOTE.NSPC
%
%			Remote connection management for nSpace drivers.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: State for this instance
	#	State											State/Driver/Software/nSpace/Remote/ {}
		!	State/Discover/OnConnection/OnFire	
														Debug/Fire
		!	State/Discover/Names/OnFire			
														Debug/Fire

	% Subgraph: Remote driver state
	#	Remote										State/Driver/Software/Remote/State/ {}

	% NSPCN: Link to default remote server
	$	LinkRemote Nspc.Link	{	Source		"/Driver/Software/Remote/Inst/Default/State/"
										Destination	"./Remote/" }
		!	Initialize/OnFire						LinkRemote/Link

	% NSPC: Link to system default discovery driver
	$	LinkDis Nspc.Link {	Source			"/Driver/Software/nSpace/Discover/Default/State/"
									Destination		"./State/Discover/" }
		!	Initialize/OnFire						LinkDis/Link

	% MISCN: Create dictionary to keep track of loads
	$	CreateLoadDct Misc.Create { Id Adt.Dictionary }
		!	Initialize/OnFire						CreateLoadDct/Fire

	%%%%%%%%%%%%%
	% Connection
	%%%%%%%%%%%%%

	% Connection strings has been selected
	!	State/Discover/OnConnection/OnFire	State/OnConnection/Fire

	%%%%%%%
	% Verb
	%%%%%%%

	%% Namespace requests.  

	% MISCN: Demux the request
	$	DemuxVerb Misc.Demux { Key Verb Values ( Load ) }
		!	State/Verb/OnFire						DemuxVerb/Dictionary
		!	State/Verb/OnFire						DemuxVerb/Fire

	%% Requestors can specify a path to place the loaded values.
	%% This logic assumes the 'To' location is only waiting for one load at a time.

	% ADTN: Remove the 'to' specification as it only applies to this logic.
	$	RemoveTo Adt.Remove { Key To }
		!	DemuxVerb/OnLoad						RemoveTo/Container

	% ADTN: Load request info
	$	LoadKeys Adt.Keys { Keys ( To From ) }
		!	DemuxVerb/OnLoad						LoadKeys/Dictionary
		!	DemuxVerb/OnLoad						LoadKeys/Load
		!	LoadKeys/OnTo							RemoveTo/Fire

	% ADTN: Store to/from into dictionary
	$	StoreToFrom Adt.Store {}
		!	CreateLoadDct/OnFire					StoreToFrom/Dictionary
		!	LoadKeys/OnTo							StoreToFrom/Key
		!	LoadKeys/OnFrom						StoreToFrom/Fire

	% Send request
	!	State/Verb/OnFire							Debug/Fire
	!	State/Verb/OnFire							Remote/Value/Fire

	%%%%%%%%%
	% Loaded
	%%%%%%%%%

	%% Distribute loaded values to waiting clients

	% MISCN: Loaded response
	$	OnLoaded Misc.Dist {}
		!	Remote/OnLoad/OnFire					OnLoaded/Fire
%		!	OnLoaded/OnFire						Debug/Fire

	% ADTN: Load loaded info.
	$	LoadedKeys Adt.Keys { Keys ( Connection From Value ) }
		!	OnLoaded/OnFire						LoadedKeys/Dictionary
		!	OnLoaded/OnFire						LoadedKeys/Load

	% MATHN: Generate full 'from' path for comparison
	$	AppendLdConn Math.Binary {}
		!	LoadedKeys/OnConnection				AppendLdConn/Left
		!	LoadedKeys/OnFrom						AppendLdConn/Right
		!	LoadedKeys/OnFrom						AppendLdConn/Fire
%		!	AppendLdConn/OnFire					Debug/Fire

	% ADTN: Iterate and search for load requests for received value
	$	LdIt Adt.Iterate {}
		!	CreateLoadDct/OnFire					LdIt/Container

	% NSPC: Receive value into receptor
	$	LdRx Nspc.This {}
		!	LdIt/OnKey								LdRx/Location
		!	LoadedKeys/OnValue					LdRx/Value

	% MISCN: Comparison ?
	$	IsFrom Misc.Compare {}
		!	AppendLdConn/OnFire					IsFrom/Left
		!	LdIt/OnNext								IsFrom/Fire

	% Send value
%	!	IsFrom/OnEqual								Debug/Fire
	!	IsFrom/OnEqual								LdRx/Store

	% Iterate entries
	!	LoadedKeys/OnValue						LdIt/First
	!	LdIt/OnNext									LdIt/Next
