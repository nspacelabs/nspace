%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								Client.NSPC
%
%							MQTT client driver
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications
	$	OnSocket				Misc.Dist {}		% Connected socket
	$	OnTransmit			Misc.Dist {}		% Transmit packet
	$	OnReceive			Misc.Dist {}		% Packet received

	% DEBUG
	$	Debug			Misc.Debug {}
%		!	Initialize/OnFire						Debug/Fire
		!	OnTransmit/OnFire						Debug/Fire
		!	OnReceive/OnFire						Debug/Fire
	$	DebugConn	Misc.Debug {}
	$	DebugDisC	Misc.Debug {}

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: State for this driver
	#	State											State/Driver/Software/Comm/Network/Ip/Tcp/Mqtt/Client/ {}
		!	OnReceive/OnFire						State/OnReceive/Fire

	% Subgraph: TCP/IP client
	#	Tcp											Driver/Software/Comm/Network/Ip/Tcp/Ssl/Client/ {}
		!	State/Secure/OnFire					Tcp/State/Secure/Fire
%		!	Tcp/State/OnSocket/OnFire			Debug/Fire
		!	Tcp/State/OnConnect/OnFire			Debug/Fire
		!	Tcp/State/OnConnect/OnFire			State/OnConnect/Fire
		!	Tcp/State/OnDisconnect/OnFire		Debug/Fire
		!	Tcp/State/OnDisconnect/OnFire		State/OnDisconnect/Fire
		!	Tcp/State/OnRead/OnFire				Debug/Fire

	% Subgraph: Communications format
	#	Format										Driver/Software/Comm/Network/Ip/Tcp/Mqtt/Format/ {}

	% MISCN: Big endian flag for transceiver
	$	ValueEndBig Misc.Dist { Value Big }b
		!	Initialize/OnFire						ValueEndBig/Fire

	%%%%%%%%%%
	% Address
	%%%%%%%%%%

	% MISCN: Valid address ?
%	$	IsAddrNull Misc.Compare { Left "" }
%		!	State/Address/OnFire					IsAddrNull/Fire

	% ADTN: See if security context exists
%	$	LoadSslChk Adt.Load { Key SslCtx }
%		!	State/Secure/OnFire					LoadSslChk/Dictionary
%		!	IsAddrNull/OnNotEqual				LoadSslChk/Fire

	% MISCN: Select port based on secure or not
%	$	PortDef Misc.Dist { Value ":1883" }
%		!	LoadSslChk/OnNotFound				PortDef/Fire
%	$	PortSsl Misc.Dist { Value ":8883" }
%		!	LoadSslChk/OnFire						PortSsl/Fire

	% MATHN: Append the port of an MQTT server.
%	$	AppendPort Math.Binary {}
%		!	PortDef/OnFire							AppendPort/Right
%		!	PortSsl/OnFire							AppendPort/Right
%		!	IsAddrNull/OnNotEqual				AppendPort/Left
%		!	IsAddrNull/OnNotEqual				AppendPort/Fire
%		!	AppendPort/OnFire						Debug/Fire

	% Set new address
%	!	IsAddrNull/OnEqual						Tcp/State/Address/Fire
%	!	AppendPort/OnFire							Tcp/State/Address/Fire
	!	State/Address/OnFire						Tcp/State/Address/Fire

	%%%%%%%%%%%
	% Transmit
	%%%%%%%%%%%

	% Subgraph: Asynchronous transmission queue
	#	TxQ											Lib/Sync_Value_QA/ {}
		!	Initialize/OnFire						TxQ/Clone/Fire
		!	State/Write/OnFire					TxQ/Value/Fire
		!	State/OnConnect/OnFire				TxQ/Reset/Fire
		!	State/OnDisconnect/OnFire			TxQ/Reset/Fire
		!	TxQ/OnValue/OnFire					OnTransmit/Fire

	% Subgraph: Convert dictionaries to streams
	#	DctToStm										Lib/Comm/DctStm/ {}
		!	Format/ValueFmtOut/OnFire			DctToStm/Format/Fire
		!	ValueEndBig/OnFire					DctToStm/Endian/Fire
		!	DctToStm/OnStream/OnFire			Debug/Fire
		!	DctToStm/OnSize/OnFire				Debug/Fire

	% ADTN: Load the socket stream
	$	LoadSktStm Adt.Load { Key Stream }
		!	Tcp/State/OnSocket/OnFire			LoadSktStm/Dictionary
		!	OnTransmit/OnFire						LoadSktStm/Fire

	% MISCN: Copy stream bytes to socket stream
	$	CopyStmTx Io.StreamCopy {}
		!	LoadSktStm/OnFire						CopyStmTx/Destination
		!	DctToStm/OnStream/OnFire			CopyStmTx/Source
		!	DctToStm/OnSize/OnFire				CopyStmTx/Size

	% ION: Ensure all data is written
	$	FlushTx Io.StreamOp {}
		!	CopyStmTx/OnFire						FlushTx/Flush

	%% Pre-process type specific commands.

	% ADTN: Store various needed fields in command
	$	StoreTxKeys Adt.Keys { Keys ( Length1 Clean IdLen PacketId
												UserFlag PassFlag UserLen PassLen TopicLen ) }
		!	OnTransmit/OnFire						StoreTxKeys/Dictionary
		!	StoreTxKeys/OnStore					Debug/Fire

	% Immediate clearing of updated values
	!	StoreTxKeys/OnStore						StoreTxKeys/Clear

	% MISCN: Demux the transmitted type
	$	DemuxTypeTx Misc.Demux { Key Type Values ( Connect Publish ) }
		!	OnTransmit/OnFire						DemuxTypeTx/Dictionary
		!	OnTransmit/OnFire						DemuxTypeTx/Fire

	%%
	%% Connect.
	%%

	%% Default is a clean connection

	% ADTN: Check presence
	$	LoadCleanChk Adt.Load { Key Clean }
		!	DemuxTypeTx/OnConnect				LoadCleanChk/Dictionary
		!	DemuxTypeTx/OnConnect				LoadCleanChk/Fire

	% MISCN: Default to clean connection
	$	CleanT Misc.Dist { Value true:bool }
		!	CleanT/OnFire							StoreTxKeys/Clean
		!	LoadCleanChk/OnNotFound				CleanT/Fire

	%% Client Id

	% ADTN: Check if protocol required client Id is present
	$	LoadIdChk Adt.Load { Key Id }
		!	DemuxTypeTx/OnConnect				LoadIdChk/Dictionary
		!	DemuxTypeTx/OnConnect				LoadIdChk/Fire

	% MISCN: Default to no client Id (length = 0)
	$	ValueIdZ Misc.Dist { Value 0:int }
		!	ValueIdZ/OnFire						StoreTxKeys/IdLen
		!	LoadIdChk/OnNotFound					ValueIdZ/Fire

	% MISCN: Otherwise, compute length of string for header
	$	IdLen Misc.StringOp {}
		!	LoadIdChk/OnFire						IdLen/Source
		!	LoadIdChk/OnFire						IdLen/Length
		!	IdLen/OnFire							StoreTxKeys/IdLen

	%% Username

	% ADTN: Check if present
	$	LoadUserChk Adt.Load { Key Username }
		!	DemuxTypeTx/OnConnect				LoadUserChk/Dictionary
		!	DemuxTypeTx/OnConnect				LoadUserChk/Fire

	% MISCN: Otherwise, compute length of string for header
	$	UserLen Misc.StringOp {}
		!	LoadUserChk/OnFire					UserLen/Source
		!	LoadUserChk/OnFire					UserLen/Length
		!	UserLen/OnFire							StoreTxKeys/UserLen

	% MISCN: Set the user flag to true
	$	UserT Misc.Dist { Value true:bool }
		!	UserT/OnFire							StoreTxKeys/UserFlag
		!	UserLen/OnFire							UserT/Fire

	%% Password

	% ADTN: Check if present
	$	LoadPassChk Adt.Load { Key Password }
		!	DemuxTypeTx/OnConnect				LoadPassChk/Dictionary
		!	DemuxTypeTx/OnConnect				LoadPassChk/Fire

	% MISCN: Otherwise, compute length of string for header
	$	PassLen Misc.StringOp {}
		!	LoadPassChk/OnFire					PassLen/Source
		!	LoadPassChk/OnFire					PassLen/Length
		!	PassLen/OnFire							StoreTxKeys/PassLen

	% MISCN: Set the Pass flag to true
	$	PassT Misc.Dist { Value true:bool }
		!	PassT/OnFire							StoreTxKeys/PassFlag
		!	PassLen/OnFire							PassT/Fire

	%%
	%% Publish
	%%

	%% Required Topic length

	% ADTN: Check if protocol required client Topic is present
	$	LoadTopicChk Adt.Load { Key Topic }
		!	DemuxTypeTx/OnPublish				LoadTopicChk/Dictionary
		!	DemuxTypeTx/OnPublish				LoadTopicChk/Fire

	% MISCN: Default to no client Topic (length = 0) (should not happen)
	$	ValueTopicZ Misc.Dist { Value 0:int }
		!	ValueTopicZ/OnFire					StoreTxKeys/TopicLen
		!	LoadTopicChk/OnNotFound				ValueTopicZ/Fire

	% MISCN: Compute length of string for header
	$	TopicLen Misc.StringOp {}
		!	LoadTopicChk/OnFire					TopicLen/Source
		!	LoadTopicChk/OnFire					TopicLen/Length
		!	TopicLen/OnFire						StoreTxKeys/TopicLen

	%% Packet Id

	% MATHN: Packet Id
	$	PktIdCnt Math.Counter { Reset 0:int }
		!	PktIdCnt/OnFire						StoreTxKeys/PacketId
		!	Initialize/OnFire						PktIdCnt/Reset
		!	DemuxTypeTx/OnPublish				PktIdCnt/Increment

	%%
	%% Length.  The total length of the packet must be computed to
	%% place in the header.  Generate stream with zero length then compute and
	%% do it again.
	%%

	% MISCN: Zero out lenght and ensure any values are up-to-date
	$	ValueLenZ Misc.Dist { Value 0:int }
		!	OnTransmit/OnFire						ValueLenZ/Fire
		!	ValueLenZ/OnFire						StoreTxKeys/Length1
		!	ValueLenZ/OnFire						StoreTxKeys/Store

	% Format into stream
	!	OnTransmit/OnFire							DctToStm/Dictionary/Fire

	% MATHN: Length is full size minus the fixed 2 byte header
	$	LenSub Math.Binary { Op Sub Right 2:int }
		!	DctToStm/OnSize/OnFire				LenSub/Left

	%% Encode.  The length size is variable depending on the size.
	%% Using algorithm provided by standard to figure out each byte.

	% MATHN: Byte counter
	$	LenByteCnt Math.Counter { Reset 1:int }

	% MATHN: Append count for label
	$	AppendLenNum Math.Binary { Left "Length" }
		!	LenByteCnt/OnFire						AppendLenNum/Right
		!	LenByteCnt/OnFire						AppendLenNum/Fire

	% ADTN: Store next byte under its label
	$	StoreLenByte Adt.Store {}
		!	OnTransmit/OnFire						StoreLenByte/Dictionary
		!	AppendLenNum/OnFire					StoreLenByte/Key

	% MATHN: Mod / div
	$	LenMod Math.Binary { Op Mod Right 128:int }
		!	LenSub/OnFire							LenMod/Left
		!	LenByteCnt/OnFire						LenMod/Fire
	$	LenDiv Math.Binary { Op Div Right 128:int }
		!	LenSub/OnFire							LenDiv/Left
		!	LenByteCnt/OnFire						LenDiv/Fire

	% Possible encoded byte
	!	LenMod/OnFire								StoreLenByte/Value

	% Update for next time
	!	LenDiv/OnFire								LenMod/Left
	!	LenDiv/OnFire								LenDiv/Left

	% MISCN: More data to encode ?
	$	IsDivZ Misc.Compare { Left 0:int }
		!	LenDiv/OnFire							IsDivZ/Fire

	% MATHN: Set top bit of the current byte
	$	LenOr Math.Binary { Op Or Right 128:int }
		!	LenMod/OnFire							LenOr/Left
		!	IsDivZ/OnLess							LenOr/Fire

	% Use top bit value instead
	!	LenOr/OnFire								StoreLenByte/Value

	% Save resulting byte
	!	LenDiv/OnFire								StoreLenByte/Fire

	% Continue processing until div is zero
	!	IsDivZ/OnNotEqual							LenByteCnt/Increment
	!	LenSub/OnFIre								LenByteCnt/Reset

	% Trigger encoding
	!	OnTransmit/OnFire							LenSub/Fire

	% Transmit after saving changes
	!	OnTransmit/OnFire							StoreTxKeys/Store
	!	OnTransmit/OnFire							DctToStm/Dictionary/Fire
	!	OnTransmit/OnFire							CopyStmTx/Fire

	%%%%%%%%%%
	% Receive
	%%%%%%%%%%

	% Subgraph: Stream to dictionary
	#	StmDct										Lib/Comm/StmDct/ {}
		!	Format/ValueFmtIn/OnFire			StmDct/Format/Fire
		!	ValueEndBig/OnFire					StmDct/Endian/Fire
%		!	Tcp/State/OnRead/OnFire				Debug/Fire
		!	Tcp/State/OnRead/OnFire				StmDct/Stream/Fire

	% Received
	!	StmDct/OnDict/OnFire						OnReceive/Fire

	% MISCN: Demux the received type
	$	DemuxTypeRx Misc.Demux { Key Type Values ( ConnectAck PingResp PubAck ) }
		!	OnReceive/OnFire						DemuxTypeRx/Dictionary
		!	OnReceive/OnFire						DemuxTypeRx/Fire

	% Responses meannext command okay
	!	DemuxTypeRx/OnConnectAck				TxQ/Next/Fire
	!	DemuxTypeRx/OnPingResp					TxQ/Next/Fire
	!	DemuxTypeRx/OnPubAck						TxQ/Next/Fire

