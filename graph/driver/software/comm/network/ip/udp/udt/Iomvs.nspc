%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%							Iomvs.NSPC
%
%	Value based multi-threaded secure UDT socket handler
%	Works with 'Iom.nspc'
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context
	$	Socket				Misc.Dist {}		% Socket to manage
	$	Transmit				Misc.Toggle {}		% Transmit value

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications
	$	OnRead				Misc.Toggle {}		% Read complete
	$	OnError				Misc.Dist {}		% Error state

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire
		!	Socket/OnFire							Debug/Fire
%		!	Transmit/OnTrue						Debug/Fire

	%%%%%%%%
	% Setup
	%%%%%%%%

	% MISCN: Create value parser
	$	CreatePrsr Misc.Create { Id "Io.StmPrsBin" }
		!	Initialize/OnFire						CreatePrsr/Fire

	% ADTN: Load raw TCP stream for socket
	$	LoadStm Adt.Load { Key Stream }
		!	Socket/OnFire							LoadStm/Dictionary
		!	Socket/OnFire							LoadStm/Fire

	%%%%%%%%%
	% Parent
	%%%%%%%%%

	%% Signals are forwaded to parent as long as socket is in 'run' state

	% Forward signals to parent
	!	OnRead/OnTrue								../../OnRead/Fire
	!	OnError/OnFire								../../OnError/Fire

	% Enable on startup
	!	Socket/OnFire								Transmit/True
	!	Socket/OnFire								OnRead/True

	%%%%%%%%%%%%%%%
	% Availability
	%%%%%%%%%%%%%%%

	%% TCP raw socket data destined for BIO

	% NETN: Wait for socket availability
	$	AvailReadA Udt.Avail { Timeout 250:int Read true:bool Write false:bool }
		!	Initialize/OnFire						AvailReadA/Start
		!	Socket/OnFire							AvailReadA/Socket
		!	Uninitialize/OnFire					AvailReadA/Remove
		!	Uninitialize/OnFire					AvailReadA/Stop
%		!	AvailReadA/OnRead						Debug/Fire
%		!	AvailReadA/OnError					Debug/Fire

	%%%%%%%%%%%
	% Transmit
	%%%%%%%%%%%

	%% To transmit async means the incoming value has to be cloned 
	%% otherwise the value could be stale by the time tranmission takes
	%% place.  If a client is slow, it will simply lose values, the idea
	%% is not to hold up everyone else.

	% MISCN: Clone incoming value
	$	CloneTx Misc.Clone {}
		!	Transmit/OnTrue						CloneTx/Fire

	% Subgraph: Cache verb dictionary
%	#	CacheTx										Driver/Software/Remote/VerbCache/ {}
	#	CacheTx										Driver/Software/Remote/VerbCacheOrd/ {}
		!	CloneTx/OnFire							CacheTx/Verb/Fire
		!	CloneTx/OnFire							CacheTx/Cache/Fire

	% MISCN: Has handshake been completed yet ?
	$	HandBool Misc.Toggle {}
		!	Socket/OnFire							HandBool/False
		!	CloneTx/OnFire							HandBool/Fire

	% MISCN: Signal transmitter that there is work
	$	TxQ Misc.AsyncEmit {}
		!	HandBool/OnTrue						TxQ/Fire

	% Obtain latest cache
	!	TxQ/OnFire									CacheTx/Next/Fire

	% MISCN: Still running ?
	$	CacheTxBool Misc.Toggle {}
		!	Socket/OnFire							CacheTxBool/True
		!	Uninitialize/OnFire					CacheTxBool/False
		!	CacheTx/OnNext/OnFire				CacheTxBool/Fire

	% ION: Persist value directly to socket
	$	ValToStm Io.Persist {}
		!	CreatePrsr/OnFire						ValToStm/Parser
		!	LoadSslStm/OnFire						ValToStm/Stream
%		!	CacheTxBool/OnTrue					Debug/Fire
		!	CacheTxBool/OnTrue					ValToStm/Save

	% Next cached value
	!	CacheTxBool/OnTrue						CacheTx/Next/Fire

	% ION: Ensure all data has been written
	$	FlushTx Io.StreamOp {}
		!	LoadSslStm/OnFire						FlushTx/Stream
		!	CacheTx/OnEmpty/OnFire				FlushTx/Flush

	%%%%%%%%%
	% Secure
	%%%%%%%%%

	%% Access the proper security key pair from the provided dictionary
	%% based on the connection type and string.

	% ADTN: Load the connection type
	$	LoadType Adt.Load { Key Type }
		!	Socket/OnFire							LoadType/Dictionary
		!	Socket/OnFire							LoadType/Fire

	% MISCN: No type specified
	$	ValueTypeNone Misc.Dist { Value None }
		!	LoadType/OnNotFound					ValueTypeNone/Fire

	% ADTN: Load the key pairs
	$	LoadSecure Adt.Load { Key Secure }
		!	Socket/OnFire							LoadSecure/Dictionary

	% MISCN: Demux if client or server end
	$	DecodeConnType Misc.Decode { Values ( Connect Accept ) }
		!	LoadType/OnFire						DecodeConnType/Select
		!	Socket/OnFire							LoadSecure/Fire
		!	LoadSecure/OnFire						DecodeConnType/Fire

	% ADTN: Load the server or client key pairs
	$	LoadServerPair Adt.Load { Key server }
		!	DecodeConnType/OnAccept				LoadServerPair/Dictionary
		!	DecodeConnType/OnAccept				LoadServerPair/Fire
	$	LoadClientPair Adt.Load { Key client }
		!	DecodeConnType/OnConnect			LoadClientPair/Dictionary
		!	DecodeConnType/OnConnect			LoadClientPair/Fire

	% MISCN: Create blank security context if not present
	$	CreateBlankPair Misc.Create { Id Adt.Dictionary }
		!	LoadServerPair/OnNotFound			CreateBlankPair/Fire
		!	LoadClientPair/OnNotFound			CreateBlankPair/Fire

	% MISCN: Clone context for local use
	$	ClonePair Misc.Clone {}
		!	LoadServerPair/OnFire				ClonePair/Fire
		!	LoadClientPair/OnFire				ClonePair/Fire
		!	CreateBlankPair/OnFire				ClonePair/Fire

	% OPENSSL: Open/close new context
	$	Ssl OpenSsl.Context { PeerCert true:bool }
		!	ClonePair/OnFire						Ssl/Dictionary
		!	ClonePair/OnFire						Ssl/Open
%		!	Ssl/OnOpen								Debug/Fire

	% OPENSSL: Open up bios for the channel
	$	Bio OpenSsl.Bio {}
		!	Ssl/OnOpen								Bio/Context
		!	Socket/OnFire							Bio/Dictionary
%		!	Bio/OnOpen								Debug/Fire

	% ADTN: Load the app/SSL side byte stream
	$	LoadSslStm Adt.Load { Key StreamSsl }
		!	Bio/OnOpen								LoadSslStm/Dictionary
		!	Bio/OnOpen								LoadSslStm/Fire

	% ADTN: Access the BIO byte stream for channel I/O
	$	LoadBioStm Adt.Load { Key StreamBio }
		!	Bio/OnOpen								LoadBioStm/Dictionary
		!	Bio/OnOpen								LoadBioStm/Fire

	% MISCN: Demux if client or server end
	$	DecodeType Misc.Decode { Values ( Connect Accept ) }
		!	LoadType/OnFire						DecodeType/Select
		!	Bio/OnOpen								DecodeType/Fire

	% Tell BIO which direction to expect handshake
	!	DecodeType/OnConnect						Bio/Connect
	!	DecodeType/OnAccept						Bio/Accept

	% Start listening for raw TCP reads
	!	Bio/OnOpen									AvailReadA/Add

	%%
	%% BIO Writes.  The SSL layer can request writes, e.g. during handshake.
	%%
%	!	Bio/OnWrite									Debug/Fire

	% ION: Copy between the BIO stream into the TCP stream
	$	CopyTx Io.StreamCopy {}
		!	Bio/OnWrite								CopyTx/Source
		!	LoadStm/OnFire							CopyTx/Destination

	% ION: Flush the TCP stream to force write out network
	$	FlushBioTx Io.StreamOp {}
		!	LoadStm/OnFire							FlushBioTx/Stream
		!	CopyTx/OnFire							FlushBioTx/Flush

	% ION: Retrieve the number of bytes available to read from BIO
	$	BioTxOp Io.StreamOp {}
		!	Bio/OnWrite								BioTxOp/Available

	% Send
%	!	BioTxOp/OnAvailable						Debug/Fire
	!	BioTxOp/OnAvailable						CopyTx/Size
	!	BioTxOp/OnAvailable						CopyTx/Fire

	%%
	%% BIO Reads
	%% TCP -> SSL/Bio
	%% TCP/encrypted data is available.  Transfer whatever is 
	%% available into SSL/Bio.
	%%
%	!	AvailReadA/OnRead							Debug/Fire

	% MISCN: Create byte stream to receive data
	$	CreateStmRd Misc.Create { Id Io.StmMemory }
		!	AvailReadA/OnRead						CreateStmRd/Fire

	% NETN: Receive whatever data might be available
	$	NetRx Udt.Recv {}
		!	Socket/OnFire							NetRx/Socket
		!	CreateStmRd/OnFire					NetRx/Stream
		!	CreateStmRd/OnFire					NetRx/Fire
		!	NetRx/OnError							Debug/Fire

	% ION: Reset position of read stream for use
	$	StmRdOp Io.StreamOp {}
		!	NetRx/OnFire							StmRdOp/Seek
		!	NetRx/OnFire							StmRdOp/Available
%		!	NetRx/OnFire							Debug/Fire

	% ION: Copy bytes from read TCP to SSL (proper size ?)
	$	CopyRx Io.StreamCopy {}
		!	StmRdOp/OnAvailable					CopyRx/Size
		!	LoadBioStm/OnFire						CopyRx/Destination
		!	NetRx/OnFire							CopyRx/Source
		!	NetRx/OnFire							CopyRx/Fire
%		!	CopyRx/OnFire							Debug/Fire

	%%%%%%%%%%%%%%%%%
	% Authentication
	%%%%%%%%%%%%%%%%%

	%% When handshake is complete, ensure it is allowed to be connected to
	%% the system.  Check provided list for public key.

	% MISCN: Server ?
	$	IsConnSrvr Misc.Compare { Left Accept }
		!	LoadType/OnFire						IsConnSrvr/Right
		!	Bio/OnConnect							IsConnSrvr/Fire

	% Query the certificate information
	!	IsConnSrvr/OnEqual						Bio/Query
	!	Bio/OnQuery									Debug/Fire

	% ADTN: Load the public key information
	$	LoadPeerKey Adt.Load { Key X509PublicKey }
		!	Bio/OnQuery								LoadPeerKey/Dictionary
		!	Bio/OnQuery								LoadPeerKey/Fire
		!	LoadPeerKey/OnFire					Debug/Fire

	% MISCN: Create context for conversion
	$	CreateKeyDct Misc.Create { Id Adt.Dictionary }
		!	Initialize/OnFire						CreateKeyDct/Fire

	% ADTN: Store key stream in dictionary
	$	StoreKeyStm Adt.Store { Key Stream }
		!	CreateKeyDct/OnFire					StoreKeyStm/Dictionary
		!	LoadPeerKey/OnFire					StoreKeyStm/Fire

	% MISCN: Format stream into string
	$	FmtKey Misc.StringFormat { Format ( { Name Stream Encode Base64 } ) }
		!	StoreKeyStm/OnFire					FmtKey/Dictionary
		!	StoreKeyStm/OnFire					FmtKey/Fire
		!	FmtKey/OnFire							Debug/Fire

	% ADTN: Is an authenticated list specified ?
	$	LoadPeerAuth Adt.Load { Key Authorized }
		!	Socket/OnFire							LoadPeerAuth/Dictionary

	% ADTN: Is the public key allowed to connect ?
	$	LoadPeerChk Adt.Load {}
		!	FmtKey/OnFire							LoadPeerChk/Key
		!	LoadPeerAuth/OnFire					LoadPeerChk/Dictionary
		!	LoadPeerAuth/OnFire					LoadPeerChk/Fire

	% Allowed, everything can continue
%	!	LoadPeerChk/OnFire						Debug/Fire

	% Latent connections
	!	FmtKey/OnFire								LoadPeerAuth/Fire

	% Handshake has completed, check for immediately for outgoing data
	!	Bio/OnConnect								HandBool/True
	!	Bio/OnConnect								HandBool/Fire

	%%%%%%%%
	% Reads
	%%%%%%%%

	%%
	%% Reads must happen asynchronously since
	%% additional incoming encrypted data will need to be
	%% processed at the same time as any value reads.
	%%

	% MISCN: Read available signal
	$	ReadA Misc.AsyncEmit {}

	% ION: Persist value from stream
	$	PersistFrom Io.Persist {}
		!	CreatePrsr/OnFire						PersistFrom/Parser
		!	LoadSslStm/OnFire						PersistFrom/Stream
		!	ReadA/OnFire							PersistFrom/Load
%		!	PersistFrom/OnLoad					Debug/Fire
		!	PersistFrom/OnError					Debug/Fire

	%% Send read result, parent needs to know which socket generated the value

	% MISCN: Create dictionary for result
	$	CreateDctRx Misc.Create { Id Adt.Dictionary }
		!	PersistFrom/OnLoad					CreateDctRx/Fire

	% ADTN: Store info
	$	StoreDctRx Adt.Keys { Keys ( Socket Value ) }
		!	CreateDctRx/OnFire					StoreDctRx/Dictionary
		!	Socket/OnFire							StoreDctRx/Socket
		!	PersistFrom/OnLoad					StoreDctRx/Value
		!	PersistFrom/OnLoad					StoreDctRx/Store

	% Result
	!	StoreDctRx/OnStore						OnRead/Fire

	%% Read signal generation

	% MISCN: Synchronize access to read signal
	$	ReadSync Misc.Pass { Keys ( Pre Post ) }
		!	Bio/OnRead								ReadSync/Pre

	% MISCN: Read signal asserted ?
	$	ReadBool Misc.Toggle {}
		!	ReadSync/OnPre							ReadBool/Fire

	% If read signal was not asserted when a new read
	% came in, assert and fire
	!	ReadBool/OnFalse							ReadBool/True
	!	ReadBool/OnFalse							ReadA/Fire

	% After read signal is complete, another read assertion
	% will be necessary if there is more data available.
	!	ReadA/OnFire								ReadSync/Post

	% ION: Number of bytes still available from the SSL side
	$	SslAv Io.StreamOp {}
		!	LoadSslStm/OnFire						SslAv/Stream
		!	ReadSync/OnPost						SslAv/Available
%		!	SslAv/OnAvailable						Debug/Fire

	% MISCN: Anything ?
	$	IsAv Misc.Compare { Left 0:int }
		!	SslAv/OnAvailable						IsAv/Fire

	% Re-assert
	!	IsAv/OnNotEqual							ReadA/Fire

	% Read signal is no longer asserted
	!	IsAv/OnEqual								ReadBool/False

	%%%%%%%%
	% Error
	%%%%%%%%

	% MISCN: Error state
	$	OnErrorInt Misc.Dist {}
		!	Socket/OnFire							OnErrorInt/Value
		!	PersistFrom/OnError					OnErrorInt/Fire
		!	AvailReadA/OnError					OnErrorInt/Fire
		!	ValToStm/OnError						OnErrorInt/Fire
		!	NetRx/OnError							OnErrorInt/Fire

	% Unauthorized clients are dumped
	!	LoadPeerChk/OnNotFound					OnErrorInt/Fire

	% Always do this
	!	OnErrorInt/OnFire							AvailReadA/Remove

	%% This client graph "owns" soket to disable and close all logic here
	%% before notifying parent to avoid any conflict when this handler is shutdown.

	% ADTN: Remove transmit receptor from socket to disable future transmissions
	% This also filters out any double error processing
	$	RemTx Adt.Remove { Key Transmit }
		!	Socket/OnFire							RemTx/Container
		!	OnErrorInt/OnFire						RemTx/Fire
	
	% No more activity
	!	RemTx/OnFire								OnRead/False
	!	RemTx/OnFire								Transmit/False
	!	RemTx/OnFire								CacheTxBool/False
	!	RemTx/OnFire								TxQ/Stop

	% NETN: Close socket here to stop any local activity
	$	SocketCl Udt.SocketOp {}
		!	Socket/OnFire							SocketCl/Socket
		!	RemTx/OnFire							SocketCl/Close
		!	Uninitialize/OnFire					SocketCl/Close

	% OPENSSL: Close socket Bios
	$	BioCl OpenSsl.Bio {}
		!	Socket/OnFire							BioCl/Dictionary
		!	RemTx/OnFire							BioCl/Close
		!	Uninitialize/OnFire					BioCl/Close

	% Close context
	!	OnErrorInt/OnFire							Debug/Fire
	!	RemTx/OnFire								Ssl/Close

	% Notify parent of error with socket
	!	Socket/OnFire								OnError/Value
	!	RemTx/OnFire								OnError/Fire

	% Start listening for raw TCP reads
	!	Socket/OnFire								Bio/Open
