%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								Inst.NSPC
%
%				nSpace visual "circle list" driver
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire
		!	Initialize/OnFire						Debug/Fire
		!	Initialize/OnFire						Debug/Fire
		!	Initialize/OnFire						Debug/Fire

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: State for this instance
	#	State											State/Driver/Visual/List/Circle/ {}
		!	State/Values/List/OnFire			Debug/Fire
		!	State/Values/List/OnFire			Debug/Fire

	% Subgraph: Circle center / root
	#	Center										Driver/Visual/List/Circle/Center/ {}

	%%%%%%%%
	% Batch
	%%%%%%%%

	%% Logic to accumulate a set of properties to set for items
	%% and store them.

	% Subgraph: Value playback
	#	Snap											Lib/State/Snapshot/ {}

	% MISCN: Create a dictionary to receive relative path vs. value
	$	CreatePrpDct Misc.Create { Id Adt.Dictionary }

	% ADTN: Store key/value in dictionary
	$	StorePrpDct Adt.Store {}
		!	CreatePrpDct/OnFire					StorePrpDct/Dictionary
%		!	StorePrpDct/OnFire					Debug/Fire

	% NSPC: Resolve full root location of target
	$	ResolveThis Nspc.This { Location "./" }
		!	Initialize/OnFire						ResolveThis/Resolve

	% MATHN: Append location of items
	$	AppendItems Math.Binary { Right "Center/Item/" }
		!	ResolveThis/OnResolve				AppendItems/Left
		!	ResolveThis/OnResolve				AppendItems/Fire

	% MISCN: Store item properties
	$	ValuePrpSt Misc.Dist {}
		!	CreatePrpDct/OnFire					ValuePrpSt/Value
		!	ValuePrpSt/OnFire						Debug/Fire
		!	ValuePrpSt/OnFire						Snap/Store/Fire

	% MISCN: Add true/false property value
	$	ValuePrpT Misc.Dist { Value true:bool }
		!	ValuePrpT/OnFire						StorePrpDct/Fire
	$	ValuePrpF Misc.Dist { Value false:bool }
		!	ValuePrpF/OnFire						StorePrpDct/Fire

	%%%%%%
	% Use
	%%%%%%

	%% Activate/use the specified number of items, unused items will be hidden.

	% MISCN: Activate specified item count
	$	OnUse Misc.Dist {}
		!	OnUse/OnFire							Debug/Fire
		!	OnUse/OnFire							Debug/Fire
		!	OnUse/OnFire							Debug/Fire
		!	OnUse/OnFire							Debug/Fire

	% MISCN: Any items ?
	$	IsUseZ Misc.Compare { Left 0:int }
		!	OnUse/OnFire							IsUseZ/Fire

	% Reset properties
	!	OnUse/OnFire								CreatePrpDct/Fire

	% MISCN: Add item counter
	$	UseCnt Math.Counter { Reset 1:int }
%		!	UseCnt/OnFire							Debug/Fire

	% MISCN: Need more ?
	$	MoreItems Misc.Compare {}
		!	OnUse/OnFire							MoreItems/Left

	% MATHN: Append count to path
	$	AddUseCnt_ Math.Binary { Left "" }
		!	AppendItems/OnFire					AddUseCnt_/Left
		!	MoreItems/OnGreater					AddUseCnt_/Right
		!	MoreItems/OnGreater					AddUseCnt_/Fire
		!	MoreItems/OnEqual						AddUseCnt_/Right
		!	MoreItems/OnEqual						AddUseCnt_/Fire
		!	AddUseCnt_/OnFire						Debug/Fire
	$	AddUseCnt Math.Binary { Right "/" }
		!	AddUseCnt_/OnFire						AddUseCnt/Left
		!	AddUseCnt_/OnFire						AddUseCnt/Fire
		!	AddUseCnt/OnFire						Debug/Fire

	% NSPC: Ensure item exists
	$	LoadItm Nspc.This { Definition "Driver/Visual/List/Circle/Item/" }
		!	AddUseCnt/OnFire						LoadItm/Location
%		!	LoadItm/OnLoad							Debug/Fire

	% MISCN: Set properties
	$	OnUsePrp Misc.Dist {}
		!	LoadItm/OnLoad							OnUsePrp/Fire

	%%%%%%%%%%
	% Arrange
	%%%%%%%%%%

	%% Items will be arranged in a circle
	%% Size of circle will scale with number of items (option ?).
	%% Items will be placed equidistant on circle.

	% MISCN: Circle step size for count/placement
	$	ValueStep Misc.Dist { Value 25.0:float }
		!	Initialize/OnFire						ValueStep/Fire

	% MATHN: Multiply item count times radius scale
	$	MulRad Math.Binary { Op Mul Left 1.0:float }
		!	ValueStep/OnFire						MulRad/Left
		!	OnUse/OnFire							MulRad/Right
		!	OnUse/OnFire							MulRad/Fire
		!	MulRad/OnFire							Debug/Fire

	% MATHN: Compute angle step based on number of items
	$	ThetaStep Math.Binary { Op Div Left 6.283185307:float }
		!	IsUseZ/OnNotEqual						ThetaStep/Right
		!	IsUseZ/OnNotEqual						ThetaStep/Fire
		!	ThetaStep/OnFire						Debug/Fire
		!	ThetaStep/OnFire						Debug/Fire

	% MATHN: Start at zero so first item is always is same position
	$	ThetaSub Math.Binary { Op Sub Right 1:int }
		!	UseCnt/OnFire							ThetaSub/Left
		!	UseCnt/OnFire							ThetaSub/Fire

	% MATHN: Calculate angle for current item
	$	ThetaMul Math.Binary { Op Mul Right 1.0:float }
		!	ThetaStep/OnFire						ThetaMul/Right
		!	ThetaSub/OnFire						ThetaMul/Left
		!	OnUsePrp/OnFire						ThetaMul/Fire
		!	ThetaMul/OnFire						Debug/Fire

	% MATHN: Start at top
	$	ThetaAdd Math.Binary { Op Add Right 1.570796:float }
		!	ThetaMul/OnFire						ThetaAdd/Left
		!	ThetaMul/OnFire						ThetaAdd/Fire

	% MATHN: Sin/cos of current angle
	$	CosT Math.Unary { Op Cos }
		!	ThetaAdd/OnFire						CosT/Fire
		!	CosT/OnFire								Debug/Fire
	$	SinT Math.Unary { Op Sin }
		!	ThetaAdd/OnFire						SinT/Fire
		!	SinT/OnFire								Debug/Fire

	% MATHN: Multiply radius to get item position (x = r*cos(theta))
	$	XMult Math.Binary { Op Mul }
		!	MulRad/OnFire							XMult/Right
		!	CosT/OnFire								XMult/Left
		!	CosT/OnFire								XMult/Fire
	$	YMult Math.Binary { Op Mul }
		!	MulRad/OnFire							YMult/Right
		!	SinT/OnFire								YMult/Left
		!	SinT/OnFire								YMult/Fire

	% MATHN: Append property
	$	AppendX Math.Binary { Right "Shape/Element/Transform/Translate/A1" }
		!	AddUseCnt/OnFire						AppendX/Left
		!	AppendX/OnFire							StorePrpDct/Key
		!	XMult/OnFire							AppendX/Fire
		!	XMult/OnFire							StorePrpDct/Fire
	$	AppendY Math.Binary { Right "Shape/Element/Transform/Translate/A2" }
		!	AddUseCnt/OnFire						AppendY/Left
		!	AppendY/OnFire							StorePrpDct/Key
		!	YMult/OnFire							AppendY/Fire
		!	YMult/OnFire							StorePrpDct/Fire

	% MATHN: Append property
	$	AppendVis Math.Binary { Right "Shape/Element/Visible" }
		!	AddUseCnt/OnFire						AppendVis/Left
		!	OnUsePrp/OnFire						AppendVis/Fire

	% Make item visible
	!	AppendVis/OnFire							StorePrpDct/Key
	!	OnUsePrp/OnFire							ValuePrpT/Fire

	%% Label.  Label's go just outside the radius with proper
	%% left/center/right alignment based on current angle.

	% MATHN: Increase radius to label radius
	$	AddRadLbl Math.Binary { Op Add Right 50.0:float }
%		!	ValueStep/OnFire						AddRadLbl/Right
		!	MulRad/OnFire							AddRadLbl/Left
		!	OnUsePrp/OnFire						AddRadLbl/Fire

	% MATHN: Multiply radius to get item position (x = r*cos(theta))
	$	XMultLbl Math.Binary { Op Mul }
		!	CosT/OnFire								XMultLbl/Left
		!	AddRadLbl/OnFire						XMultLbl/Right
		!	AddRadLbl/OnFire						XMultLbl/Fire
	$	YMultLbl Math.Binary { Op Mul }
		!	SinT/OnFire								YMultLbl/Left
		!	AddRadLbl/OnFire						YMultLbl/Right
		!	AddRadLbl/OnFire						YMultLbl/Fire

	% MATHN: Append property
	$	AppendLblX Math.Binary { Right "Label/Element/Transform/Translate/A1" }
		!	AddUseCnt/OnFire						AppendLblX/Left
		!	AppendLblX/OnFire						StorePrpDct/Key
		!	XMultLbl/OnFire						AppendLblX/Fire
		!	XMultLbl/OnFire						StorePrpDct/Fire
	$	AppendLblY Math.Binary { Right "Label/Element/Transform/Translate/A2" }
		!	AddUseCnt/OnFire						AppendLblY/Left
		!	AppendLblY/OnFire						StorePrpDct/Key
		!	YMultLbl/OnFire						AppendLblY/Fire
		!	YMultLbl/OnFire						StorePrpDct/Fire

	% MATHN: Append property
	$	AppendLblA Math.Binary { Right "Label/AlignHorz" }
		!	AddUseCnt/OnFire						AppendLblA/Left
		!	AppendLblA/OnFire						StorePrpDct/Key

	% MISCN: Label alignments
	$	ValueLeft Misc.Dist { Value Left }
		!	ValueLeft/OnFire						AppendLblA/Fire
		!	ValueLeft/OnFire						StorePrpDct/Fire
	$	ValueCenter Misc.Dist { Value Center }
		!	ValueCenter/OnFire					AppendLblA/Fire
		!	ValueCenter/OnFire					StorePrpDct/Fire
		!	OnUsePrp/OnFire						ValueCenter/Fire
	$	ValueRight Misc.Dist { Value Right }
		!	ValueRight/OnFire						AppendLblA/Fire
		!	ValueRight/OnFire						StorePrpDct/Fire

	% MISCN: Side of circle
	$	IsRight Misc.Compare { Left 3.141592653:float }
		!	ThetaMul/OnFire						IsRight/Right
		!	OnUsePrp/OnFire						IsRight/Fire

	% Left side will be right aligned, right side left aligned, middle is centered
	!	IsRight/OnGreater							ValueRight/Fire
	!	IsRight/OnLess								ValueLeft/Fire

	% Latent connections
	!	MoreItems/OnGreater						LoadItm/Load
	!	MoreItems/OnEqual							LoadItm/Load
	!	UseCnt/OnFire								MoreItems/Fire

	% First/next item
	!	LoadItm/OnLoad								UseCnt/Increment
	!	OnUse/OnFire								UseCnt/Reset

	%% Center.  Offset the center by the radius so the top spot
	%% stays anchored.  Currently doing this for navigation since '..' is always
	%% the first entry.  TOOD: Option ?

	% MATHN: Negative center radius
	$	NegRadC Math.Binary { Op Mul Right -1.0:float }
		!	MulRad/OnFire							NegRadC/Left
		!	OnUse/OnFire							NegRadC/Fire

	% MATHN: Append property
	$	AppendCenY Math.Binary { Right "Center/Shape/Element/Transform/Translate/A2" }
		!	ResolveThis/OnResolve				AppendCenY/Left
		!	AppendCenY/OnFire						StorePrpDct/Key
		!	NegRadC/OnFire							AppendCenY/Fire
		!	NegRadC/OnFire							StorePrpDct/Fire

	% MATHN: Offset Y by radius to ensure top spot stays anchored while the circle
	% below changes.
%	$	YSub Math.Binary { Op Sub }
%		!	MulRad/OnFire							YSub/Right
%		!	YMult/OnFire							YSub/Left
%		!	YMult/OnFire							YSub/Fire
%		!	MulRad/OnFire							YMult/Right

	% MATHN: Offset Y by radius to ensure top spot stays anchored while the circle
	% below changes.
%	$	YSubLbl Math.Binary { Op Sub }
%		!	MulRad/OnFire							YSubLbl/Right
%		!	YMultLbl/OnFire						YSubLbl/Left
%		!	YMultLbl/OnFire						YSubLbl/Fire

	%% End of list, this means any remaining items will need to hidden/unused.

	% MATHN: Unused count
	$	UnuseCnt Math.Counter {}
		!	MoreItems/OnLess						UnuseCnt/Set
		!	MoreItems/OnLess						UnuseCnt/Nocrement

	% Generate path to potential next item
	!	UnuseCnt/OnFire							AddUseCnt_/Right
	!	UnuseCnt/OnFire							AddUseCnt_/Fire

	% NSPC: Does item exist ?
	$	TestItm Nspc.This {}
		!	AddUseCnt/OnFire						TestItm/Location
		!	UnuseCnt/OnFire						TestItm/Test

	% If exists, hide
	!	TestItm/OnTest								AppendVis/Fire
	!	TestItm/OnTest								ValuePrpF/Fire

	% Continue searching
	!	TestItm/OnTest								UnuseCnt/Increment

	% Apply generated properties
	!	OnUse/OnFire								ValuePrpSt/Fire

	%%%%%%%%%
	% Values
	%%%%%%%%%

	%% A new value list has been provided, update.

	% MISCN: New list.  Item connect to this to get latest list.
	$	OnList Misc.Dist {}
		!	State/Values/List/OnFire			OnList/Fire

	% ADTN: Count number of entries
	$	CountLst Adt.Stat {}
		!	OnList/OnFire							CountLst/Container
		!	OnList/OnFire							CountLst/Fire

	% MISCN: Hide existing blocks
	$	ValueCntZ Misc.Dist { Value 0:int }
		!	CountLst/OnCount						ValueCntZ/Fire
%		!	ValueCntZ/onFire						OnUse/Fire

	% Use that many
	!	CountLst/OnCount							OnUse/Fire

	% MISCN: Selection request, blocks send index to this
	% Make this asynchronous to de-couple from visual logic
	$	Select Misc.AsyncEmit {}
%		!	Select/OnFire							Debug/Fire

	% Select index in list
	!	Select/OnFire								State/Values/Element/Default/Fire

	%% Debug

	% MISCN: Block counts
%	$	ValueBlks Misc.Dist { Value 10:int }
%		!	Initialize/OnFire						ValueBlks/Fire
%		!	ValueBlks/OnFire						OnUse/Fire
%	$	ValueBlksTwo Misc.Dist { Value 5:int }
%		!	Initialize/OnFire						ValueBlksTwo/Fire
%		!	ValueBlksTwo/OnFire					OnUse/Fire

