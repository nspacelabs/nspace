%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								Block.NSPC
%
%		Driver for an invidual block, keep light as possible
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize

	% Notifications

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire

	%%%%%%%%
	% Setup
	%%%%%%%%

	% MISCN: The graph instance name is the block index
	$	ToInt Misc.Type { Type Integer }
		!	Initialize/OnFire						ToInt/Convert

	%%%%%%%%%%
	% Visuals
	%%%%%%%%%%

	% Subgraph: Block and text
	#	Shape											State/Visual/Shape/ {}
	#	Label											State/Visual/Label/ {}
	#	InputRay										State/Visual/Input/Ray/ {}
%		!	InputRay/Enter/OnFire				Debug/Fire
		!	InputRay/Activate/OnFire			Debug/Fire

	% Track same position as shape
	!	Shape/Element/Transform/Translate/A2/OnFire
			Label/Element/Transform/Translate/A2/Fire

	% Label has same visibility as shape
	!	Shape/Element/Visible/OnFire			Label/Element/Visible/Fire

	% MATHN: Place label slightly in front of block to ensure visibility
	$	AddZ Math.Binary { Right +10.0:float }
		!	Shape/Element/Transform/Translate/A3/OnFire
														AddZ/Left
		!	Shape/Element/Transform/Translate/A3/OnFire
														AddZ/Fire
		!	AddZ/OnFire								Label/Element/Transform/Translate/A3/Fire

	% MISCN: Shape
%	$	ValueShape Misc.Dist { Value Cube }
	$	ValueShape Misc.Dist { Value Cylinder }
		!	Initialize/OnFire						ValueShape/Fire
		!	ValueShape/OnFire						Shape/Name/Fire
%		!	ValueShape/OnFire						Label/Interface/Element/Default/Fire

	% MISCN: "Skinny" block
	$	ValueZ Misc.Dist { Value 0.1:float }
		!	Initialize/OnFire						ValueZ/Fire
		!	ValueZ/OnFire							Shape/Element/Transform/Scale/A3/Fire

	% MISCN: Default width
	$	ValueW Misc.Dist { Value 2.0:float }
		!	Initialize/OnFire						ValueW/Fire
		!	ValueW/OnFire							Shape/Element/Transform/Scale/A1/Fire

	% MISCN: Default height
	$	ValueH Misc.Dist { Value 0.5:float }
		!	Initialize/OnFire						ValueH/Fire
		!	ValueH/OnFire							Shape/Element/Transform/Scale/A2/Fire

	% MISCN: Colors
	$	ValueClrDef Misc.Dist { Value 0xff0000ff:int }
		!	Initialize/OnFire						ValueClrDef/Fire
		!	ValueClrDef/OnFire					Shape/Element/Color/Fire
	$	ValueClrEn Misc.Dist { Value 0xffff0000:int }
		!	ValueClrEn/OnFire						Shape/Element/Color/Fire
	$	ValueClrAct Misc.Dist { Value 0xffffffff:int }
		!	ValueClrAct/OnFire					Shape/Element/Color/Fire
	$	ValueClrLbl Misc.Dist { Value 0xffffffff:int }
		!	Initialize/OnFire						ValueClrLbl/Fire
		!	ValueClrLbl/OnFire					Label/Element/Color/Fire

	% MISCN: Euler rotation mode
	$	ValueEuler Misc.Dist { Value Euler }
		!	Initialize/OnFire						ValueEuler/Fire
		!	ValueEuler/OnFire						Label/Element/Transform/ModeRotate/Fire

	% MISCN: Temporary until labels are fixed
	$	ValueRotX Misc.Dist { Value 90:int }
		!	Initialize/OnFire						ValueRotX/Fire
		!	ValueRotX/OnFire						Label/Element/Transform/Rotate/A1/Fire
	$	ValueRotY Misc.Dist { Value -90:int }
		!	Initialize/OnFire						ValueRotY/Fire
		!	ValueRotY/OnFire						Label/Element/Transform/Rotate/A2/Fire

	%%%%%%%%
	% Enter
	%%%%%%%%

	% MISCN: Input ray entered ?
	$	IsEnter Misc.Compare { Left true:bool }
		!	InputRay/Enter/OnFire				IsEnter/Right
		!	InputRay/Enter/OnFire				IsEnter/Fire
		!	IsEnter/OnEqual						ValueClrEn/Fire
		!	IsEnter/OnNotEqual					ValueClrDef/Fire

	%%%%%%%%%%%%%
	% Activation
	%%%%%%%%%%%%%

	% Treat ray activation as a 'selection'.

	% MISCN: Input ray activated ?
	$	IsAct Misc.Compare { Left true:bool }
		!	InputRay/Activate/OnFire			IsAct/Fire

	% MISCN: Send selection index
	$	ValueSel Misc.Dist {}
		!	ToInt/OnFire							ValueSel/Value
		!	IsAct/OnEqual							ValueSel/Fire

	% Send to parent
	!	ValueSel/OnFire							../../Select/Fire

	%%%%%%%
	% List
	%%%%%%%

	%% Receive the latest value list and update own label
	%% based on index of this block.

	% MISCN: Value list
	$	OnList Misc.Dist {}
		!	../../OnList/OnFire					OnList/Fire
%		!	OnList/OnFire							Debug/Fire

	% ADTN: Load onw value from list
	$	LoadVal Adt.Load {}
		!	ToInt/OnFire							LoadVal/Key
		!	OnList/OnFire							LoadVal/Dictionary
		!	OnList/OnFire							LoadVal/Fire
		!	Initialize/OnFire						LoadVal/Fire

	% Set as label
	!	LoadVal/OnFire								Label/Interface/Element/Default/Fire

