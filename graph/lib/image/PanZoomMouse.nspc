%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%						PanZoomMouse.NSPC
%
%		Perform panning and zooming via mouse input state.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize
	$	Reset					Misc.Dist {}		% Reset state

	% Notifications
	$	OnRectX				Misc.Dist {}		% Rectangle position
	$	OnRectY				Misc.Dist {}		% Rectangle position
	$	OnW					Misc.Dist {}		% Current width
	$	OnH					Misc.Dist {}		% Current height
	$	OnL					Misc.Dist {}		% Upper-left coordinates
	$	OnT					Misc.Dist {}		% Upper-left coordinates
	$	OnR					Misc.Dist {}		% Lower-right coordinates
	$	OnB					Misc.Dist {}		% Lower-right coordinates
	$	OnZoom				Misc.Dist {}		% New zoom level
	$	OnUpdate				Misc.Dist {}		% Coordinates updated
	$	OnOver				Misc.Dist {}		% Mouse over pixel T/F

	% DEBUG
	$	Debug Misc.Debug {}

 	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: Mouse input
	#	Mouse											State/Interface/Mouse/ {}
%		!	Mouse/XY/OnFire						Debug/Fire
%		!	Mouse/Wheel/OnFire					Debug/Fire
%		!	Mouse/Left/OnFire						Debug/Fire
%		!	Mouse/Over/OnFire						Debug/Fire
		!	Mouse/Over/OnFire						OnOver/Fire

	%%%%%%%%
	% Reset
	%%%%%%%%

	%% Reset working area back to full 100%

	% MISCN: Initial upper left hand coordinates
	$	ValueUL Misc.Dist { Value 0.0:float }
		!	Reset/OnFire							ValueUL/Fire
		!	ValueUL/OnFire							OnL/Fire
		!	ValueUL/OnFire							OnT/Fire

	% MISCN: Zoom level (not zoomed)
	$	ValueZoomZ Misc.Dist { Value 1:int }
		!	Reset/OnFire							ValueZoomZ/Fire
		!	ValueZoomZ/OnFire						OnZoom/Fire

	%%%%%%%%%%%%%%
	% Coordinates
	%%%%%%%%%%%%%%

	%% Compute current mouse coordinates in rectangle space.

	% ADTN: Load coordinates
	$	LoadXY Adt.Keys { Keys ( X Y ) }
		!	Mouse/XY/OnFire						LoadXY/Dictionary
		!	Mouse/XY/OnFire						LoadXY/Load

	% MATHN: Current width and height with zoom level
	$	ZoomW Math.Binary { Op Div Left 1.0:float }
		!	OnZoom/OnFire							ZoomW/Right
		!	OnZoom/OnFire							ZoomW/Fire
%		!	ZoomW/OnFire							Debug/Fire
	$	ZoomH Math.Binary { Op Div Left 1.0:float }
		!	OnZoom/OnFire							ZoomH/Right
		!	OnZoom/OnFire							ZoomH/Fire
%		!	ZoomH/OnFire							Debug/Fire

	% Notify
	!	ZoomW/OnFire								OnW/Fire
	!	ZoomH/OnFire								OnH/Fire

	% MATHN: Scale coordinates by current size
	$	RectX Math.Binary { Op Mul Right 1:float }
		!	ZoomW/OnFire							RectX/Right
		!	LoadXY/OnX								RectX/Left
		!	LoadXY/OnX								RectX/Fire
	$	RectY Math.Binary { Op Mul Right 1:float }
		!	ZoomH/OnFire							RectY/Right
		!	LoadXY/OnY								RectY/Left
		!	LoadXY/OnY								RectY/Fire

	% MATHN: Offset by current upper left hand corner
	$	RectOffX Math.Binary { Op Add }
		!	OnL/OnFire								RectOffX/Left
		!	RectX/OnFire							RectOffX/Right
		!	RectX/OnFire							RectOffX/Fire
	$	RectOffY Math.Binary { Op Add }
		!	OnT/OnFire								RectOffY/Left
		!	RectY/OnFire							RectOffY/Right
		!	RectY/OnFire							RectOffY/Fire

	% Current mouse position within rectangle
	!	RectOffX/OnFire							OnRectX/Fire
	!	RectOffY/OnFire							OnRectY/Fire

	%%%%%%%%%%%%
	% Rectangle
	%%%%%%%%%%%%

	%% Allow a new upper left corner to be set, limit values and compute
	%% bounding rectangle using zoomed width and height.

	% NSPC: Upper left hand corner, limit to valid ranges
	$	SetLeft	Nspc.Value { Type Float Minimum 0.0:float Maximum 1.0:float }
	$	SetTop	Nspc.Value { Type Float Minimum 0.0:float Maximum 1.0:float }

	% New left/top
	!	SetLeft/OnFire								OnL/Fire
	!	SetTop/OnFire								OnT/Fire

	% MATHN: Compute the new right/bottom
	$	AddR Math.Binary { Op Add }
		!	OnW/OnFire								AddR/Left
		!	SetLeft/OnFire							AddR/Right
		!	SetLeft/OnFire							AddR/Fire
	$	AddB Math.Binary { Op Add }
		!	OnH/OnFire								AddB/Left
		!	SetTop/OnFire							AddB/Right
		!	SetTop/OnFire							AddB/Fire

	% New right/bottom
	!	AddR/OnFire									OnR/Fire
	!	AddB/OnFire									OnB/Fire

	%% It is possible the new upper left corner results in
	%% a rectangle that is too big, a new upper left corner need to be set.

	% MISCN: Is new corner too large ?
	$	IsOverR Misc.Compare { Left 1.0:float }
		!	AddR/OnFire								IsOverR/Fire
	$	IsOverB Misc.Compare { Left 1.0:float }
		!	AddB/OnFire								IsOverB/Fire

	% MATHN: Calculate how much it is over
	$	SubR Math.Binary { Op Sub Right 1.0:float }
		!	IsOverR/OnLess							SubR/Left
		!	IsOverR/OnLess							SubR/Fire
	$	SubB Math.Binary { Op Sub Right 1.0:float }
		!	IsOverB/OnLess							SubB/Left
		!	IsOverB/OnLess							SubB/Fire

	% MATHN: Top and left now need to be backed off by that amount
	% to that rectangle goes right up to the edge.
	$	SubL Math.Binary { Op Sub }
		!	OnL/OnFire								SubL/Left
		!	SubR/OnFire								SubL/Right
		!	SubR/OnFire								SubL/Fire
	$	SubT Math.Binary { Op Sub }
		!	OnT/OnFire								SubT/Left
		!	SubB/OnFire								SubT/Right
		!	SubB/OnFire								SubT/Fire

	% Try again with new corner.  Theoretically this could result
	% in a continuous loop but proper logic will prevent that.
	!	SubL/OnFire									SetLeft/Fire
	!	SubT/OnFire									SetTop/Fire

	%%%%%%%
	% Zoom
	%%%%%%%

	% MISCN: Zoom direction (based on sign)
	$	IsDeltaN Misc.Compare { Left 0:int }
		!	Mouse/Wheel/OnFire					IsDeltaN/Fire

	% MISCN: Zoom too high/too low ?
	$	IsZoomL Misc.Compare { Left 1:int }
		!	OnZoom/OnFire							IsZoomL/Right
		!	IsDeltaN/OnLess						IsZoomL/Fire
	$	IsZoomH Misc.Compare { Left 16:int }
		!	OnZoom/OnFire							IsZoomH/Right
		!	IsDeltaN/OnGreater					IsZoomH/Fire

	% MATHN: Adjust zoom level
	$	ZoomOut Math.Binary { Op Div Right 2:int }
		!	OnZoom/OnFire							ZoomOut/Left
		!	IsZoomL/OnLess							ZoomOut/Fire
	$	ZoomIn Math.Binary { Op Mul Right 2:int }
		!	OnZoom/OnFire							ZoomIn/Left
		!	IsZoomH/OnGreater						ZoomIn/Fire

	% MISCN: New zoom level
	$	ValueZoom Misc.Dist {}
		!	OnZoom/OnFire							ValueZoom/Value
		!	ZoomOut/OnFire							ValueZoom/Value
		!	ZoomIn/OnFire							ValueZoom/Value
		!	Mouse/Wheel/OnFire					ValueZoom/Fire

	% MISCN: New zoom level off ?
	$	IsZoomOne Misc.Compare { Left 1:int }
		!	ValueZoom/OnFire						IsZoomOne/Fire

	% Reset logic for original zoomed image to head of round-offs
	!	IsZoomOne/OnEqual							Reset/Fire

	% New zoom level
	!	IsZoomOne/OnNotEqual						OnZoom/Fire

	% MATHN: Scale current offset with the new zoom
	$	SclOffX Math.Binary { Op Div Left 0.0:float }
		!	OnRectX/OnFire							SclOffX/Left
		!	OnZoom/OnFire							SclOffX/Right
		!	OnZoom/OnFire							SclOffX/Fire
	$	SclOffY Math.Binary { Op Div Left 0.0:float }
		!	OnRectY/OnFire							SclOffY/Left
		!	OnZoom/OnFire							SclOffY/Right
		!	OnZoom/OnFire							SclOffY/Fire

	% MATHN: Compute a new upper left corner
	$	ZoomL Math.Binary { Op Sub Left 0.0:float }
		!	OnRectX/OnFire							ZoomL/Left
		!	SclOffX/OnFire							ZoomL/Right
		!	SclOffX/OnFire							ZoomL/Fire
	$	ZoomT Math.Binary { Op Sub Left 0.0:float }
		!	OnRectY/OnFire							ZoomT/Left
		!	SclOffY/OnFire							ZoomT/Right
		!	SclOffY/OnFire							ZoomT/Fire

	% Set a new corner
	!	ZoomL/OnFire								SetLeft/Fire
	!	ZoomT/OnFire								SetTop/Fire
	!	ZoomT/OnFire								OnUpdate/Fire

	%%%%%%
	% Pan
	%%%%%%

	%% Ensure the point under the mouse stays under the mouse by
	%% adjust the corner during movement.

	% MISCN: Down/up
	$	IsLftDn Misc.Compare { Left true:bool }
		!	Mouse/Left/OnFire						IsLftDn/Fire
%		!	IsLftDn/OnEqual						Debug/Fire

	% MISCN: Cancel pan if mouse not over
	$	IsOver Misc.Compare { Left true:bool }
		!	Mouse/Over/OnFire						IsOver/Fire

	% MISCN: True if panning image
	$	PanBool Misc.Toggle {}
		!	Mouse/Left/OnFire						PanBool/Value
		!	IsOver/OnNotEqual						PanBool/False
		!	Mouse/XY/OnFire						PanBool/Fire

	% MISCN: Remember where the mouse was before pan
	$	ValuePanX Misc.Dist { Value 0:int }
		!	OnRectX/OnFire							ValuePanX/Value
		!	IsLftDn/OnEqual						ValuePanX/Fire
	$	ValuePanY Misc.Dist { Value 0:int }
		!	OnRectY/OnFire							ValuePanY/Value
		!	IsLftDn/OnEqual						ValuePanY/Fire

	% MATHN: Difference between mouse positions
	$	PanDx Math.Binary { Op Sub }
		!	ValuePanX/OnFire						PanDx/Left
		!	OnRectX/OnFire							PanDx/Right
		!	PanBool/OnTrue							PanDx/Fire
	$	PanDy Math.Binary { Op Sub }
		!	ValuePanY/OnFire						PanDy/Left
		!	OnRectY/OnFire							PanDy/Right
		!	PanBool/OnTrue							PanDy/Fire

	% MATHN: Adjust original corner by same delta
	$	PanDl Math.Binary { Op Add }
		!	OnL/OnFire								PanDl/Left
		!	PanDx/OnFire							PanDl/Right
		!	PanDx/OnFire							PanDl/Fire
	$	PanDt Math.Binary { Op Add }
		!	OnT/OnFire								PanDt/Left
		!	PanDy/OnFire							PanDt/Right
		!	PanDy/OnFire							PanDt/Fire

	% New corner
	!	PanDl/OnFire								SetLeft/Fire
	!	PanDt/OnFire								SetTop/Fire
	!	PanDt/OnFire								OnUpdate/Fire

	%%

	% Initial reset
	!	Initialize/OnFire							Reset/Fire
