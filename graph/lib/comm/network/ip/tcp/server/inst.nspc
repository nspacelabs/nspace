%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%								INST.NSPC
%
%							TCP/IP server.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize

	% Notifications

	% DEBUG
	$	Debug Misc.Debug {}

 	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: Server state
	#	State											State/Comm/Network/Ip/Tcp/Server/State/ {}

	% Subgraph: Shared run-time connections state
	#	Shared										State/Comm/Network/Ip/Tcp/Server/Connections/ {}

	% Subgraph: Server interface
	#	Interface									State/Comm/Network/Ip/Tcp/Server/Interface/ {}

	% Subgraph: Connections interface
	#	Connections									State/Comm/Connections/Interface/ {}

	% Subgraph: Interface management
	#	IntfConntns									Lib/Interface/Dict/ {}
		!	IntfConntns/OnStoreKey/OnFire		Debug/Fire
		!	IntfConntns/OnStore/OnFire			Debug/Fire

	% MISCN: Location of dictionary to manage
	$	ValueIntfConntns Misc.Dist { Value "./Connections/Connections/" }
		!	Initialize/OnFire						ValueIntfConntns/Fire
		!	ValueIntfConntns/OnFire				IntfConntns/Location/Fire

	%%%%%%%%%%%
	% Handlers
	%%%%%%%%%%%

	%% Each connection is handled by a graph instance.

	% Subgraph: Connection handler list
	#	Handlers										State/List/ {}

	% Subgraph: List management
	#	ListConntn									Lib/Graph/Dict/ {}
		!	ListConntn/OnStore/OnFire			Debug/Fire

	% MISCN: Location of state to manage
	$	ValueListConntn Misc.Dist { Value "./Handlers/" }
		!	Initialize/OnFire						ValueListConntn/Fire
		!	ValueListConntn/OnFire				ListConntn/Location/Fire

	% MISCN: Definition of connection handler
	$	ValueDefConntn Misc.Dist { Value "Lib/Comm/Network/Ip/Tcp/Server/Connection/" }
		!	Initialize/OnFire						ValueDefConntn/Fire
		!	ValueDefConntn/OnFire				ListConntn/Definition/Fire

	%% Link new connection handlers to run-time state

	% MATNN: Generate location to run-time state
	$	AppendConntnState_ Math.Binary { Left "./Handlers/" }
		!	ListConntn/OnStore/OnFire			AppendConntnState_/Right
		!	ListConntn/OnStore/OnFire			AppendConntnState_/Fire
		!	ListConntn/OnRemove/OnFire			AppendConntnState_/Right
		!	ListConntn/OnRemove/OnFire			AppendConntnState_/Fire
	$	AppendConntnState Math.Binary { Right "/Shared/" }
		!	AppendConntnState_/OnFire			AppendConntnState/Left
		!	AppendConntnState_/OnFire			AppendConntnState/Fire
		!	AppendConntnState/OnFire			Debug/Fire

	% NSPC: Perform link
	$	LinkConntn Nspc.Link { Source "./Shared/" }
		!	AppendConntnState/OnFire			LinkConntn/Destination
		!	ListConntn/OnStore/OnFire			LinkConntn/Link
		!	ListConntn/OnRemove/OnFire			LinkConntn/UnLink

	%% Update location of I/O on new handlers

	% Use logic below to update the 'Io' location of the connection
	!	ListConntn/OnStore/OnFire				IntfConntns/Key/Fire

	% MATHN: Append location of transfer state
	$	AppendIo Math.Binary { Right "/State/Io/" }
		!	AppendConntnState_/OnFire			AppendIo/Left
		!	ListConntn/OnStore/OnFire			AppendIo/Fire

	% Store path in list
	!	AppendIo/OnFire							IntfConntns/Value/Fire
	!	AppendIo/OnFire							IntfConntns/Store/Fire

	%%%%%%%%%
	% TCP/IP
	%%%%%%%%%

	% Subgraph: Server/client socket usage.
	#	Server										Lib/Comm/Network/Ip/Tcp/ServerClients/ {}

   % Reflect in use port
   !	Server/OnPortSrvr/OnFire				State/PortSrvr/Fire
%	!	State/PortReq/OnFire						Server/PortSrvr/Fire

	% MISCN: Decode requested state
	$	DecodeState Misc.Decode { Values ( Start Stop ) }
		!	Interface/State/Element/Default/Value/OnFire	
														DecodeState/Select
		!	Interface/State/Element/Default/Value/OnFire	
														DecodeState/Fire

	% Start/stop
	!	DecodeState/OnStart						Server/Start/Fire
	!	DecodeState/OnStop						Server/Stop/Fire

	% MISCN: Server idle on stop
	$	ValueIdle Misc.Dist { Value Idle }
		!	DecodeState/OnStop					ValueIdle/Fire
		!	ValueIdle/OnFire						Interface/State/Element/Default/Value/Fire

   % Debug
   !  Initialize/OnFire                   Server/Start/Fire

	%%%%%%%%%%%%%%
	% Connections
	%%%%%%%%%%%%%%

	%% External entities can request a connection.
	%% Create and store a connection handler to the specified address.

	% MISCN: Connection value null ?
	$	IsConnNull Misc.Compare { Left "" }
		!	IntfConntns/OnStore/OnFire			IsConnNull/Fire

	% MISCN: Connection request
	$	OnConnection Misc.Dist {}
		!	IntfConntns/OnStoreKey/OnFire		OnConnection/Value
		!	IsConnNull/OnEqual					OnConnection/Fire
		!	OnConnection/OnFire					Debug/Fire

	% ADTN: Store connection in dictionaries with no active socket
	$	StoreConn Adt.Store { Value {} }
		!	Shared/Connections/OnFire			StoreConn/Dictionary
		!	OnConnection/OnFire					StoreConn/Key
		!	OnConnection/OnFire					StoreConn/Fire
		!	StoreConn/OnFire						Debug/Fire

	% The connection string will be used to identify the handler as well.
	!	OnConnection/OnFire						ListConntn/Key/Fire
	!	OnConnection/OnFire						ListConntn/Store/Fire

	%%%%%%%%%%
	% Connect
	%%%%%%%%%%

	% Make a request for connection on behalf of handler
%	!	Shared/OnConnect/OnFire					Debug/Fire
	!	Shared/OnConnect/OnFire					Server/Address/Fire
	!	Shared/OnConnect/OnFire					Server/Connect/Fire

	%%%%%%%%%%%%
	% Connected
	%%%%%%%%%%%%

	%% Ensure handler for every connection.

	% MISCN: Client connected
	$	OnConnected Misc.Dist {}
		!	Server/OnConnect/OnFire				OnConnected/Fire

	% ADTN: Load remote address of connection
	$	LoadConntdRem Adt.Load { Key Remote }
		!	OnConnected/OnFire					LoadConntdRem/Dictionary
		!	OnConnected/OnFire					LoadConntdRem/Fire

	% Key for connection information
	!	LoadConntdRem/OnFire						IntfConntns/Key/Fire

	% ADTN: Store information about connection
	$	StoreConntd Adt.Store {}
		!	Shared/Connections/OnFire			StoreConntd/Dictionary
		!	LoadConntdRem/OnFire					StoreConntd/Key
		!	OnConnected/OnFire					StoreConntd/Fire

	% Ensure handler exists for connection
	!	LoadConntdRem/OnFire						ListConntn/Key/Fire
	!	OnConnected/OnFire						ListConntn/Store/Fire

	% MISCN: Signal connection
	$	ValueConntd Misc.Dist {}
		!	LoadConntdRem/OnFire					ValueConntd/Value
		!	OnConnected/OnFire					ValueConntd/Fire
		!	ValueConntd/OnFire					Shared/Connected/Fire
	
	%%%%%%%
	% Read
	%%%%%%%

	% MISCN: Socket readable
	$	OnRead Misc.Dist {}
		!	Server/OnRead/OnFire					OnRead/Fire

	% ADTN: Load connection for socket
	$	LoadRemRd Adt.Load { Key Remote }
		!	OnRead/OnFire							LoadRemRd/Dictionary
		!	OnRead/OnFire							LoadRemRd/Fire

	% Notify connection
	!	LoadRemRd/OnFire							Shared/Read/Fire

	%%%%%%%%
	% Write
	%%%%%%%%

	%% Connection can request notification when it becomes 'writable'

	% MISCN: Request write
	$	OnWrite Misc.Dist {}
		!	Shared/OnWrite/OnFire				OnWrite/Fire

	% ADTN: Load connection descriptor
	$	LoadConnWr Adt.Load {}
		!	Shared/Connections/OnFire			LoadConnWr/Dictionary
		!	OnWrite/OnFire							LoadConnWr/Key
		!	OnWrite/OnFire							LoadConnWr/Fire

	% Request writable detection
	!	LoadConnWr/OnFire							Server/Socket/Fire
	!	LoadConnWr/OnFire							Server/Write/Fire

	%% Connection has become writable.

	% MISCN: Socket writable
	$	OnWritable Misc.Dist {}
		!	Server/OnWrite/OnFire				Debug/Fire
		!	Server/OnWrite/OnFire				OnWritable/Fire

	% ADTN: Load address of connection
	$	LoadWriteRem Adt.Load { Key Remote }
		!	OnWritable/OnFire						LoadWriteRem/Dictionary
		!	OnWritable/OnFire						LoadWriteRem/Fire

	% Notify
	!	LoadWriteRem/OnFire						Shared/Write/Fire
