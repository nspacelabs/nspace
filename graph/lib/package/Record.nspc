%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%						Record.NSPC
%
%		Read in and process the dependencies for a 
%		recording file.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context
	$	Locations			Misc.Dist {}		% Locations dictionary
	$	Location				Misc.Dist {}		% Recording file
	$	Modules				Misc.Dist {}		% Modules used
	$	Source				Misc.Dist {}		% Location for source files

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize
	$	Scan					Misc.Dist {}		% Perform scan

	% DEBUG
	$	Debug Misc.Debug {}

 	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph: Graph dependencies
	#	Depends										Lib/Package/Depends/ {}
		!	Source/OnFire							Depends/Source/Fire
		!	Locations/OnFire						Depends/Locations/Fire
		!	Modules/OnFire							Depends/Modules/Fire

	% MISCN: Create file stream source
	$	CreateFileSrc Misc.Create { Id Io.StmSrcFile }
		!	Initialize/OnFire						CreateFileSrc/Fire

	% NSPC: Create nSpace parser
	$	CreatePrsr Misc.Create { Id Nspc.PersistTxt }
		!	Initialize/OnFire						CreatePrsr/Fire

	%%%%%%%
	% Load
	%%%%%%%

	% MATHN: Append source root
	$	AppendSrc Math.Binary { Left "" }
		!	Source/OnFire							AppendSrc/Left
		!	Location/OnFire						AppendSrc/Right
		!	Scan/OnFire								AppendSrc/Fire
%		!	AppendSrc/OnFire						Debug/Fire

	% ION: Open recording file
	$	RecOpen Io.StreamSource { Options { ReadOnly true:bool } }
		!	CreateFileSrc/OnFire					RecOpen/Source
		!	AppendSrc/OnFire						RecOpen/Location
		!	AppendSrc/OnFire						RecOpen/Open

	% ION: Load definition from stream
	$	LoadRec Io.Persist {}
		!	CreatePrsr/OnFire						LoadRec/Parser
		!	RecOpen/OnOpen							LoadRec/Stream
		!	RecOpen/OnOpen							LoadRec/Load
%		!	LoadRec/OnLoad							Debug/Fire

	% Done w/stream
	!	RecOpen/OnOpen								RecOpen/Close

	%%%%%%%%%
	% Search
	%%%%%%%%%

	% ADTN: Iterate entries
	$	RecIt Adt.Iterate {}
		!	LoadRec/OnLoad							RecIt/Container
		!	LoadRec/OnLoad							RecIt/First
%		!	RecIt/OnKey								Debug/Fire
%		!	RecIt/OnNext							Debug/Fire

	% MISCN: Demux the operation
	$	DemuxOp Misc.Demux { Key Command Values ( put ) }
		!	RecIt/OnNext							DemuxOp/Dictionary
		!	RecIt/OnNext							DemuxOp/Fire

	% ADTN: Load the put information
	$	LoadPutKeys Adt.Keys { Keys ( Value Path ) }
		!	DemuxOp/Onput							LoadPutKeys/Dictionary
		!	DemuxOp/Onput							LoadPutKeys/Load
%		!	LoadPutKeys/OnValue					Debug/Fire
%		!	LoadPutKeys/OnPath					Debug/Fire

	% Process connection path
	!	LoadPutKeys/OnPath						Depends/Location/Fire
	!	LoadPutKeys/OnPath						Depends/Scan/Fire

	%%
	%% Auto-start
	%%

	% MISCN: Special case, nSpace shell auto startup graphs
	$	IsAuto Misc.Compare { Left "/App/Shell/Default/State/Auto/OnFire/" }
		!	LoadPutKeys/OnPath					IsAuto/Fire

	% ADTN: Scan the auto-start graphs
	$	AutoIt Adt.Iterate {}
		!	LoadPutKeys/OnValue					AutoIt/Container
		!	IsAuto/OnEqual							AutoIt/First
		!	AutoIt/OnNext							Depends/Location/Fire
		!	AutoIt/OnNext							Depends/Scan/Fire
		!	AutoIt/OnNext							AutoIt/Next

	%%
	%% Remote transports
	%%

	% MISCN: Special case, remote driver specifies transport drivers
	$	IsRem Misc.Compare { Left "/Driver/Software/Value/Inst/Remote/State/Transports/OnFire/" }
		!	LoadPutKeys/OnPath					IsRem/Fire
%		!	IsRem/OnEqual							Debug/Fire

	% ADTN: Scan the transports
	$	RemIt Adt.Iterate {}
		!	LoadPutKeys/OnValue					RemIt/Container
		!	IsRem/OnEqual							RemIt/First

	% Transports are stored as <definition>/<instance name>
	% Isolate just the definition for scanning.

	% MISCN: Substring definition
	$	TransDef Misc.StringOp {}
		!	RemIt/OnNext							TransDef/Source

	% MISCN: Location of last slash
	$	TransIdx Misc.StringOp { Source "/" }
		!	RemIt/OnNext							TransIdx/Destination
		!	RemIt/OnNext							TransIdx/LastIndexOf

	% MATHN: Keep the slash in the definition since it a slocation
	$	TransAdd Math.Binary { Op Add Right 1:int }
		!	TransIdx/OnFire						TransAdd/Left
		!	TransIdx/OnFire						TransAdd/Fire

	% Generate definition
	!	TransAdd/OnFire							TransDef/To
	!	TransAdd/OnFire							TransDef/Substring

	% Add to dependencies
%	!	TransDef/OnFire							Debug/Fire
	!	TransDef/OnFire							Depends/Location/Fire
	!	TransDef/OnFire							Depends/Scan/Fire

	% Next transport
	!	RemIt/OnNext								RemIt/Next

	% Continue search
	!	RecIt/OnNext								RecIt/Next

