%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%							CopyMapped.NSPC
%
%		Copy to/from mapped memory (i.e. Linux "/dev/mem").
%		For infrequent use (memory opened/closed each time)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context
	$	Address				Misc.Dist {}		% Address of region
	$	Size					Misc.Dist {}		% Size of copy (and region)
	$	Data					Misc.Dist {}		% Byte stream or memory block to copy

	% Actions
	$	Initialize			Misc.Dist {}		% Initialize
	$	Uninitialize		Misc.Dist {}		% Uninitialize
	$	CopyTo				Misc.Dist {}		% Copy to mapped memory

	% Notifications
	$	OnError				Misc.Dist {}		% Copy unsuccessful

	% DEBUG
	$	Debug Misc.Debug {}
		!	Initialize/OnFire						Debug/Fire
		!	Uninitialize/OnFire					Debug/Fire
		!	OnError/OnFire							Debug/Fire

 	%%%%%%%%
	% Setup
	%%%%%%%%

	% MISCN: File system stream source
	$	CreateFileSrc Misc.Create { Id Io.StmSrcFile }
		!	Initialize/OnFire						CreateFileSrc/Fire

	%%%%%%
	% Map
	%%%%%%

	% ION: Access memory device
	$	OpenDevMem Io.StreamSource { Location "/dev/mem"
			Options { ReadOnly false:bool Sync true:bool } }
		!	CreateFileSrc/OnFire					OpenDevMem/Source
		!	CopyTo/OnFire							OpenDevMem/Open
		!	OpenDevMem/OnError					Debug/Fire

	% MISCN: Memory region options
	$	ValueMemMapOpts Misc.Dist { Value { Offset 0x0:int Size 0x0:int } }
		!	Initialize/OnFire						ValueMemMapOpts/Fire

	% ADTN: Store "file" that will be used for mapping
	$	StoreMapFile Adt.Store { Key File }
		!	ValueMemMapOpts/OnFire				StoreMapFile/Dictionary
		!	OpenDevMem/OnOpen						StoreMapFile/Fire

	% ADTN: Store address/offset in memory options
	$	StoreMapAddr Adt.Store { Key Offset }
		!	ValueMemMapOpts/OnFire				StoreMapAddr/Dictionary
		!	Address/OnFire							StoreMapAddr/Fire

	% ADTN: Store image size in memory options
	$	StoreMapSize Adt.Store { Key Size }
		!	ValueMemMapOpts/OnFire				StoreMapSize/Dictionary
		!	Size/OnFire								StoreMapSize/Fire

	%%%%%%%
	% Copy
	%%%%%%%

	% MISCN: Create a memory mapped file object
	$	CreateMemMap Misc.Create { Id Io.MappedFile }
		!	OpenDevMem/OnOpen						CreateMemMap/Fire

	% ION: Open memory window
	$	OpenMemMap Io.Resource {}
		!	ValueMemMapOpts/OnFire				OpenMemMap/Options
		!	CreateMemMap/OnFire					OpenMemMap/Resource
		!	CreateMemMap/OnFire					OpenMemMap/Open

	% MISCN: Copy image data to own memory block
	$	CopyBits Io.StreamCopy {}
		!	Data/OnFire								CopyBits/Source
%		!	Size/OnFire								CopyBits/Size
		!	OpenMemMap/OnOpen						CopyBits/Destination
		!	OpenMemMap/OnOpen						CopyBits/Fire
%		!	CopyBits/OnFire						Debug/Fire

	% Error conditions
	!	OpenDevMem/OnError						OnError/Fire
	!	OpenMemMap/OnError						OnError/Fire
	!	CopyBits/OnError							Debug/Fire

	% Done w/mapping
	!	OpenMemMap/OnOpen							OpenMemMap/Close
	!	OpenDevMem/OnOpen							OpenDevMem/Close
