%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%							DELETE_RECURSE.NSPC
%
%			Recursively delete a specified URL from storage.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context
	$	Source			Misc.Dist {}			% Stream source
	$	Location			Misc.Dist {}			% Location within source to delete

	% Actions
	$	Initialize		Misc.Dist {}			% Initialize
	$	Uninitialize	Misc.Dist {}			% Uninitialize
	$	Delete			Misc.Dist {}			% Perform delete

	% Notifications
	$	OnItem			Misc.Dist {}			% Item being deleted
	$	OnEnd				Misc.Dist {}			% Deletion complete
	$	OnError			Misc.Dist {}			% Error detected

	% DEBUG
	$	Debug Misc.Debug { Message "DELETE IT RECURSE" }

	%%%%%%%%
	% Setup
	%%%%%%%%

	% Subgraph : File iteration
	#	FileIt										Lib/Stream/File_it_recurse/ {}
		!	Source/OnFire							FileIt/Source/Fire
		!	FileIt/OnError/OnFire				OnError/Fire

	% CREATE: Create stack for deletions
	$	CreateDeleteStk Misc.Create { Id Adt.Stack }
		!	Delete/OnFire							CreateDeleteStk/Fire

	% ADTN: Write locations to stack
	$	WriteDeleteStk Adt.Write {}
		!	CreateDeleteStk/OnFire				WriteDeleteStk/List

	% VALUE: Place initial location on stack so it will be removed at end
	$	ValueDeleteLoc Misc.Dist {}
		!	Location/OnFire						ValueDeleteLoc/Value
		!	Delete/OnFire							ValueDeleteLoc/Fire
		!	ValueDeleteLoc/OnFire				WriteDeleteStk/Fire

	% Iterate all locations
	!	Location/OnFire							FileIt/Location/Fire
	!	Delete/OnFire								FileIt/First/Fire

	%%%%%%%%%%%
	% Deletion
	%%%%%%%%%%%

	% Write locations and URLs to stack and proceed to next item
	!	FileIt/OnLocation/OnFire				WriteDeleteStk/Fire
	!	FileIt/OnItem/OnFire						WriteDeleteStk/Fire
	!	FileIt/OnItem/OnFire						FileIt/Next/Fire

	% ADTN: Iterate locations to be deleted
	$	DeleteIt Adt.Iterate {}
		!	CreateDeleteStk/OnFire				DeleteIt/Container
		!	FileIt/OnEnd/OnFire					DeleteIt/First
%		!	DeleteIt/OnNext						Debug/Fire

	% ION: File deletion
	$	SourceDel Io.StreamSource {}
		!	Source/OnFire							SourceDel/Source
		!	DeleteIt/OnNext						SourceDel/Location
		!	DeleteIt/OnNext						SourceDel/Remove
	
	% Next location
	!	DeleteIt/OnNext							DeleteIt/Next

	% Done
	!	DeleteIt/OnLast							OnEnd/Fire

