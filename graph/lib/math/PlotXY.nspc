%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%							PlotXY.NSPC
%
%		Plot data from a data image to an XY plot image.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@

	% Context
	$	Image					Misc.Dist {}		% Image block to save
	$	Width					Misc.Dist {}		% Size of plot area
	$	Height				Misc.Dist {}		% Size of plot area
	$	Range					Misc.Dist {}		% Range dictionary (Left,Top,Right,Bottom)
	$	Label					Misc.Dist {}		% Label dictionary
	$	Table					Misc.Dist {}		% Table configuration

	% Actions
	$	Initialize			Misc.Dist {}		% Initalize graph
	$	Update				Misc.Dist {}		% Update plot

	% Notifications
	$	OnImage				Misc.Dist {}		% Plot image
		
	% DEBUG
	$	Debug Misc.Debug {}

	%%%%%%%%
	% Setup
	%%%%%%%%

	% NOTE: Keeping initialization and rendering on same thread
	% Implementation dependent (VTK), fix this in the nodes.

	% MISCN: Async update
	$	UpdateA Misc.AsyncEmit {}
		!	Update/OnFire							UpdateA/Fire

	% MISCN: True for first time initialization
	$	FirstBool Misc.Toggle {}
		!	Initialize/OnFire						FirstBool/True
		!	FirstBool/OnTrue						FirstBool/False
		!	UpdateA/OnFire							FirstBool/Fire

	% MISCN: Create render context
	$	CreateRenderDct Misc.Create { Id Adt.Dictionary }
		!	FirstBool/OnTrue						CreateRenderDct/Fire

	% MISCN: Create plot context
	$	CreatePlotDct Misc.Create { Id Adt.Dictionary }
		!	FirstBool/OnTrue						CreatePlotDct/Fire

	%%%%%%%
	% Plot
	%%%%%%%

	% VISUALIZEN: XY plot
	$	XyPlot Visualize.XYPlot {}
		!	CreatePlotDct/OnFire					XyPlot/Dictionary

	% MISCN: Default table configuration is 1 XY plot from column 0, 1
	$	ValueTblDef Misc.Dist { Value ( { X 0:int Y 1:int Color 0x00ff00:int } ) }
		!	Initialize/OnFire						ValueTblDef/Fire
		!	ValueTblDef/OnFire					XyPlot/Table
		!	Table/OnFire							XyPlot/Table

	% Source data
	!	Image/OnFire								XyPlot/Image
	!	Range/OnFire								XyPlot/Range
	!	Label/OnFire								XyPlot/Label

	% VISUALIZEN: Renderer
	$	Render Visualize.Render {}
		!	Width/OnFire							Render/Width
		!	Height/OnFire							Render/Height

	% Open a renderer on context
	!	CreateRenderDct/OnFire					Render/Dictionary
	!	CreateRenderDct/OnFire					Render/Open

	% Update plot
	!	UpdateA/OnFire								XyPlot/Fire
	!	XyPlot/OnFire								Render/Item
	!	XyPlot/OnFire								Render/Add
	!	XyPlot/OnFire								Render/Fire
	!	XyPlot/OnFire								Render/Remove

	% Result
	!	Render/OnFire								OnImage/Fire
