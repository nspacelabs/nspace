////////////////////////////////////////////////////////////////////////
//
//									UDTN.CPP
//
//					Main file for the UDT networking library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"Udt"

// Library implementations
#include "../../lib/udtl/udtl_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()

	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Avail)
	CCL_OBJLIST_ENTRY	(Client)
	CCL_OBJLIST_ENTRY	(PersistSkt)
	CCL_OBJLIST_ENTRY	(Recv)
	CCL_OBJLIST_ENTRY	(SocketOp)

CCL_OBJLIST_END()
