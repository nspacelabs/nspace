////////////////////////////////////////////////////////////////////////
//
//									SCARDN.CPP
//
//				Main file for the media node library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"SCard"
#include "../../lib/scardl/scardl_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()

	// Nodes
	CCL_OBJLIST_ENTRY	(Card)
	CCL_OBJLIST_ENTRY	(Enum)
	CCL_OBJLIST_ENTRY	(Status)

CCL_OBJLIST_END()
