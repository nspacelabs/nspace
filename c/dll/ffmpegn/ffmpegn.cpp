////////////////////////////////////////////////////////////////////////
//
//									FFMPEGN.CPP
//
//				Main file for the FFMpeg library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"FFmpeg"

// Library implementations
#include "../../lib/ffmpegl/ffmpegl_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()
	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Decode)
	CCL_OBJLIST_ENTRY	(Encode)
	CCL_OBJLIST_ENTRY	(File)
	
CCL_OBJLIST_END()
