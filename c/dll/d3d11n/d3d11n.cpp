////////////////////////////////////////////////////////////////////////
//
//									D3D11N.CPP
//
//				Main file for the Direct3D11 library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"D3D11"

// Library implementations
#include "../../lib/d3d11l/d3d11l_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()
	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Buffer)
	CCL_OBJLIST_ENTRY	(Device)
	CCL_OBJLIST_ENTRY	(Shader)
	CCL_OBJLIST_ENTRY	(Texture)

CCL_OBJLIST_END()

