////////////////////////////////////////////////////////////////////////
//
//									NSHX.CPP
//
//		Main file to provide a "C" external interface for the nSpace
//		client.  This is written as a "polling" (ugh) interface since
//		it is originally being written for environments such as Unity,
//		that have issues with callback functions from multi-threaded
//		systems.
//
////////////////////////////////////////////////////////////////////////

// Library implementations
#define	INITGUID
#include "nshx.h"
#include <stdio.h>

static bool bCoInitd = false;

extern "C" 
HRESULT WINAPI n_Close ( U64 *pdwCli )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Closes any open client connection.  Client ID is invalid
	//			after this call.
	//
	//	PARAMETERS
	//		-	pdwCli is the client Id returned in 'open'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;

	// Debug
	lprintf ( LOG_INFO, L"pdwCli %p", pdwCli );

	// Setup
	CCLTRYE ( pdwCli != NULL, E_INVALIDARG );
	CCLTRYE ( (pnCli = (nSpaceClientAsync *)(*pdwCli)) != NULL, E_INVALIDARG );

	// Close connection
	CCLOK ( pnCli->close(); )

	// Clean up
	if (hr == S_OK)
		{
		delete pnCli;
		*pdwCli = 0;
		}	// if
	if (bCoInitd)
		{
		cclUninitialize();
		bCoInitd = false;
		}	// if
	
	return hr;
	}	// n_Close

extern "C"
HRESULT WINAPI n_Listen (	U64 dwCli, const char *szPath, U16 bL )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Listen/unlisten to an emitter in the namespace.
	//
	//	PARAMETERS
	//		-	dwCli is the Id returned during 'n_Open'.
	//		-	szPath is the namespace path
	//		-	bL is TRUE to listen, FALSE to unlisten
	//		-	pCB is the callback function for the listen
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;
	adtString			strPath	= szPath;

	// Debug
	lprintf ( LOG_INFO, L"dwCli 0x%x szPath %S bL %d", 
								dwCli, szPath, bL );

	// Setup
	CCLTRYE ( (pnCli = (nSpaceClientAsync *)dwCli) != NULL, E_INVALIDARG );

	// Perform listen/unlisten at location
	CCLTRY ( pnCli->listen ( strPath, bL ) );

	return hr;
	}	// n_Listen

extern "C"
HRESULT WINAPI n_Load ( U64 dwCli, const char *szPath )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Loads a value from the namespace.  Must call 'n_Received'
	//			to obtain value.
	//
	//	PARAMETERS
	//		-	dwCli is the Id returned during 'n_Open'.
	//		-	szPath is the namespace path
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;
	adtString 			strPath	= szPath;
	
	// Debug
	lprintf ( LOG_INFO, L"dwCli 0x%x szPath %S", dwCli, szPath );

	// Setup
	CCLTRYE ( (pnCli = (nSpaceClientAsync *)dwCli) != NULL, E_INVALIDARG );

	// Perform load
	CCLTRY ( pnCli->load ( strPath ) );

	return hr;
	}	// n_Load

extern "C" 
HRESULT WINAPI n_Open (	U64 *pdwCli, const char *pCmdLine, U16 bShared,
								U16 bDirect )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Opens a new client connection to nSpace.
	//
	//	PARAMETERS
	//		-	dwCli will receive the client Id to pass to future calls.
	//		-	pCmdLine is the command line to use for the namespace
	//		-	bShare is true to open an existing or create a new service or 
	//			false to create a private one.
	//		-	bDirect is true to create namespace directly, false to do
	//			it over the COM control.
	//		-	pCB is the callback client for notifications.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;
	adtString 			strCmd	= pCmdLine;
	adtInt				iAddr(-1),iPort(62831);
	
	// Debug
	logFile ( L"/tmp/log.txt" );

	// Initialize COM
	bCoInitd = (cclInitialize ( NULL, COINIT_MULTITHREADED ) == S_OK);
	
	// Create new object and open namespace
	CCLTRYE ( (pnCli = new nSpaceClientAsync()) != NULL, E_OUTOFMEMORY );

	// Attempt to resolve command line as an IP address to auto-switch
	// to TCP.
	CCLOK ( NetSkt_Resolve ( strCmd, iAddr, iPort ); )

	// Open with flags.  Detect IP address for TCP/IP "open".
	if (hr == S_OK && iAddr != -1)
		hr = pnCli->open ( strCmd, NULL );
	else if (hr == S_OK)
		hr = pnCli->open ( strCmd, bShared, bDirect );

	// Results
	if (hr != S_OK && pnCli != NULL)
		{
		delete pnCli;
		pnCli = NULL;
		}	// if

	// Store ptr. as Id
	(*pdwCli) = (U64)pnCli;

	return hr;
	}	// n_Open

#ifdef		_WIN32

extern "C"
HRESULT WINAPI n_Received ( U64 dwCli, char *pRoot, char *pLoc,
										U32 szStr, void **ppvData, U32 *pszData )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve the next received value from the queue.
	//
	//	PARAMETERS
	//		-	dwCli is the Id returned during 'n_Open'.
	//		-	pwRoot will receive the root path
	//		-	pwLoc will receie the location path
	//		-	szStr is the size allocated for the strings
	//		-	ppvData will receive the byte array
	//		-	pszData will receive the number of bytes in the array
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;
	adtVariant			var;
	adtValue				vL;
	adtString			strRoot,strLoc;

	// Debug
//	lprintf ( LOG_INFO, L"dwCli 0x%x", dwCli );

	// Setup
	CCLTRYE ( (pnCli = (nSpaceClientAsync *)dwCli) != NULL, E_INVALIDARG );

	// Read the next value
	CCLTRY ( pnCli->received ( strRoot, strLoc, (BYTE **)ppvData, pszData ) );

	// Convert to caller parameters
	if (hr == S_OK)
		{
		char *pcStr = NULL;

		// Root
//		WCSCPY ( pwRoot, szStr, strRoot );
		if (strRoot.toAscii(&pcStr) == S_OK)
			{
			strcpy_s ( pRoot, 1024, pcStr );
			_FREEMEM(pcStr);
			}	// if

		// Location
//		WCSCPY ( pwLoc, szStr, strLoc );
		if (strLoc.toAscii(&pcStr) == S_OK)
			{
			strcpy_s ( pLoc, 1024, pcStr );
			_FREEMEM(pcStr);
			}	// if

		// Value to variant
//		var = vL;
//		VariantCopy ( pv, &var );
		}	// if

	return hr;
	}	// n_Received

/*
extern "C"
HRESULT WINAPI n_Received ( U64 dwCli, char *pRoot, char *pLoc,
										U32 szStr, VARIANT *pv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve the next received value from the queue.
	//
	//	PARAMETERS
	//		-	dwCli is the Id returned during 'n_Open'.
	//		-	pwRoot will receive the root path
	//		-	pwLoc will receie the location path
	//		-	szStr is the size allocated for the strings
	//		-	pv will receive the value
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;
	adtVariant			var;
	adtValue				vL;
	adtString			strRoot,strLoc;

	// Debug
//	lprintf ( LOG_INFO, L"dwCli 0x%x", dwCli );

	// Setup
	CCLTRYE ( (pnCli = (nSpaceClientAsync *)dwCli) != NULL, E_INVALIDARG );

	// Read the next value
	CCLTRY ( pnCli->received ( strRoot, strLoc, vL ) );

	// Convert to caller parameters
	if (hr == S_OK)
		{
		char *pcStr = NULL;

		// Root
//		WCSCPY ( pwRoot, szStr, strRoot );
		if (strRoot.toAscii(&pcStr) == S_OK)
			{
			strcpy_s ( pRoot, 1024, pcStr );
			_FREEMEM(pcStr);
			}	// if

		// Location
//		WCSCPY ( pwLoc, szStr, strLoc );
		if (strLoc.toAscii(&pcStr) == S_OK)
			{
			strcpy_s ( pLoc, 1024, pcStr );
			_FREEMEM(pcStr);
			}	// if

		// Value to variant
		var = vL;
		VariantCopy ( pv, &var );
		}	// if

	return hr;
	}	// n_Received
*/
extern "C"
HRESULT WINAPI n_Resolve ( char *path, char *pathAbs, U32 size )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Resolve path to a valid absolute path.
	//
	//	PARAMETERS
	//		-	path is the input path
	//		-	pathAbs will receive the full path
	//		-	size is the allocated size of the return buffer
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	char			*pcAsc	= NULL;
	adtString	strPath(path),strPathAbs;

	// Since there is no default 'root', incoming paths must already be rooted.
	CCLTRYE ( strPath[0] == '/', E_INVALIDARG );

	// Expand/collapse internal path specifiers ("../",etc)
	CCLTRY ( nspcPathTo(NULL,strPath,strPathAbs) );

	// Result
	if (	pathAbs != NULL && 
			size > (int)strPathAbs.length() &&
			strPathAbs.toAscii(&pcAsc) == S_OK)
		{
		// Use result
		strcpy_s ( pathAbs, size, pcAsc );
		_FREEMEM(pcAsc);
		}	// if

	return hr;
	}	// n_Resolve

extern "C"
HRESULT WINAPI n_Store ( U64 dwCli, const char *szPath, VARIANT *pv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Store a value into the namespace.
	//
	//	PARAMETERS
	//		-	dwCli is the Id returned during 'n_Open'.
	//		-	szPath is the namespace path
	//		-	pv contains the value to put into the namespace.
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	nSpaceClientAsync	*pnCli	= NULL;
	adtVariant			varS;
	adtValue				vS;
	adtString			strPath	= szPath;
	
	// Debug
//	lprintf ( LOG_INFO, L"dwCli 0x%x szPath %S", dwCli, szPath );

	// Setup
	CCLTRYE ( (pnCli = (nSpaceClientAsync *)dwCli) != NULL, E_INVALIDARG );

	// Special case, since strings are possibly coming from another
	// environment (i.e. C#), ensure any strings to be stored are
	// 'owned' by nSpace.
	if (hr == S_OK && pv->vt == VT_BSTR)
		{
		adtString strValue(pv->bstrVal);

		// Own string and copy to value
		CCLOK(strValue.at();)
		CCLTRY(adtValue::copy(strValue,vS));
		}	// if
	else
		{
		// Conversion from variant
		CCLOK (varS = pv; )
		CCLTRY(varS.toValue(vS));
		}	// else

	// Perform store
	CCLTRY ( pnCli->store ( strPath, vS ) );

	return hr;
	}	// n_Store

#endif
