////////////////////////////////////////////////////////////////////////
//
//								SHM.CPP
//
//				Portable shared memory layer for C#
//
////////////////////////////////////////////////////////////////////////

#include "nshx.h"

// Globals

extern "C" 
U64 WINAPI n_shm_close ( U64 fd )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Closes a mapping to a shared memory location
	//
	//	PARAMETERS
	//		-	fd is the file descriptor from 'open'.
	//
	//	RETURN VALUE
	//		State of file descriptor
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	HANDLE	hMap	= (HANDLE)fd;

	// Debug
	lprintf ( LOG_INFO, L"fd 0X%x", fd );

	// Clean up
	if (hMap != NULL)
		CloseHandle(hMap);

	return fd;
	}	// n_shm_close

extern "C" 
U32 WINAPI n_shm_copy_to ( U64 fd, const BYTE *pcFrom, U32 length )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Copies a block of memory to the shared memory.
	//
	//	PARAMETERS
	//		-	fd is the file descriptor of the shared memory location
	//		-	pcFrom is the local memory ptr from which to copy
	//		-	length is the number of bytes to copy
	//
	//	RETURN VALUE
	//		Number of bytes copied
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	HANDLE	hMap	= (HANDLE)fd;
	U8			*pcTo	= NULL;

	// State check
	CCLTRYE ( hMap != NULL, E_INVALIDARG );
	CCLTRYE ( pcFrom != NULL, E_INVALIDARG );

	// Obtain a ptr. to the location
	CCLTRYE ( (pcTo = (U8 *) MapViewOfFile ( hMap,
					FILE_MAP_READ|FILE_MAP_WRITE, 0, 0, 0 )) != NULL, GetLastError() );

	// Truncate, vald length ?
	CCLTRYE ( length > 0, E_INVALIDARG );
	if (hr == S_OK && length > *((U32 *)pcTo))
		length = *((U32 *)pcTo);

	// Debug
//	lprintf ( LOG_INFO, L"fd 0X%x Size %d bytes pcTo 0x%x (0x%x) pcFrom 0x%x length %d hr 0x%x", 
//										fd, *((U32 *)pcTo), pcTo, pcTo+4, pcFrom, length, hr );

	// Copy
	CCLOK ( memcpy ( pcTo+4, pcFrom, length ); )

	// Clean up
	if (hr != S_OK)
		length = 0;
	if (pcTo != NULL)
		UnmapViewOfFile(pcTo);

	return length;
	}	// n_shm_copy_to

extern "C" 
U64 WINAPI n_shm_open ( const WCHAR *pwName, U32 length )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Opens a global shared memory mapped location.
	//
	//	PARAMETERS
	//		-	pwName is the name to use for the resource
	//		-	length is the size of the location
	//
	//	RETURN VALUE
	//		Index of shared memory location, -1 on error
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	HANDLE	hMap		= NULL;
	U32		*puBlk	= NULL;

	// Create a new file mapping backed by memory, leave room for size
	CCLTRYE ( (hMap = CreateFileMapping ( INVALID_HANDLE_VALUE,
					NULL, PAGE_READWRITE, 0, (length+4), pwName ))
					!= NULL, GetLastError() );

	// Obtain a ptr. to the location
	CCLTRYE ( (puBlk = (U32 *) MapViewOfFile ( hMap,
					FILE_MAP_READ|FILE_MAP_WRITE, 0, 0, 0 )) != NULL, GetLastError() );

	// Write size
	CCLOK ( *puBlk = length; )

	// Clean up
	if (puBlk != NULL)
		UnmapViewOfFile(puBlk);
	if (hr != S_OK && hMap != NULL)
		{
		CloseHandle(hMap);
		hMap = NULL;
		}	// if

	// Debug
	lprintf ( LOG_INFO, L"pwName %s, length %d hMap 0x%x",
						pwName, length, hMap );

	return (U64)hMap;
	}	// n_shm_open

