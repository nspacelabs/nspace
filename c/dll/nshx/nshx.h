////////////////////////////////////////////////////////////////////////
//
//										NSHX.H
//
//			Include file for the external "C" nSpace interface.
//
////////////////////////////////////////////////////////////////////////

#ifndef	NSHX_H
#define	NSHX_H

// nSpace
#include "../../lib/nshxl/nshxl.h"

// Prototypes
extern "C"
	{
	// nSpace client
	DLLEXPORT 	HRESULT WINAPI n_Close		( U64 * );
	DLLEXPORT 	HRESULT WINAPI n_Listen		( U64, const char *, U16 );
	DLLEXPORT	HRESULT WINAPI n_Load		( U64, const char * );
	DLLEXPORT 	HRESULT WINAPI n_Open		( U64 *, const char *, U16, U16 );
	DLLEXPORT	HRESULT WINAPI n_Resolve	( char *, char *, U32 );

	// Until it is figured out how VARIANTs are handled in Linux
	DLLEXPORT	HRESULT WINAPI n_Received	( U64, char *, char *, U32, void **, U32 * );
	#ifdef		_WIN32
//	DLLEXPORT	HRESULT WINAPI n_Received	( U64, char *, char *, U32, VARIANT * );
	DLLEXPORT 	HRESULT WINAPI n_Store		( U64, const char *, VARIANT * );
	#endif
	// Portable shared memory routines for C#
//	U64 WINAPI n_shm_close		( U64 );
//	U32 WINAPI n_shm_copy_to	( U64, const BYTE *, U32 );
//	U64 WINAPI n_shm_open		( const WCHAR *, U32 );
	}

#endif
