////////////////////////////////////////////////////////////////////////
//
//									PYTHONN.CPP
//
//					Main file for the python node library
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#define	CCL_OBJ_PREFIX		L"nSpace"
#define	CCL_OBJ_MODULE		L"Python"

// Library implementations
#include "../../lib/pythonl/pythonl_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()

	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(PythonOp)

CCL_OBJLIST_END()
