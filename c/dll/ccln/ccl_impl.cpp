////////////////////////////////////////////////////////////////////////
//
//									CCL_IMPL.CPP
//
//				An implementation of the COM Compatibility Layer.  
//
////////////////////////////////////////////////////////////////////////

// Includes
#include "ccl_impl.h"
#if defined(__unix__) || defined(__APPLE__)
#include <libgen.h>
#include <dlfcn.h>
#endif
#if defined(__APPLE__)
#include <libproc.h>
#endif
#include <stdio.h>

// Globals
static  	char			cDirProc[1024]		= "";
static 	IDictionary	*pDctLib				= NULL;
static 	IDictionary	*pLstLib				= NULL;
static 	IDictionary	*pDctFct				= NULL;
static 	U32 			coRefCnt 			= 0;

// Prototypes
HRESULT cclLoadFactoryInt	( const wchar_t *pwLib, const wchar_t *pwId,
										void **ppvLib, IClassFactory **ppFact );

HRESULT cclAddLibrary	( const wchar_t *pwLib, U64 iHandle )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Add a library to internal catalog.
	//
	//	PARAMETERS
	//		-	pwLib is the path to use for the library
	//		-	iHandle is the OS specific, already loaded library handle
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	U32			sz = 0;
	adtString	sLib(pwLib);
	adtLong		lHandle(iHandle);

	// Add to dictionary
	CCLTRY ( pDctLib->store ( sLib, lHandle ) );

	// Add to list
	CCLTRY ( pLstLib->size ( &sz ) );
	CCLTRY ( pLstLib->store ( adtInt(sz+1), lHandle ) );

	return hr;
	}	// cclAddLibrary

extern "C" DLLEXPORT
HRESULT cclCreateObject ( const wchar_t *pId, IUnknown *pOuter,
									REFIID iid, void **ppv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Creates the specified object.
	//
	//	PARAMETERS
	//		-	pId is the class Id
	//		-	pOuter is the outer unknown
	//		-	iid is the interface to query for
	//		-	ppv will receive the object
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr				= S_OK;
	IClassFactory	*pFact		= NULL;

	// Class factory for object
	CCLTRY ( cclGetFactory ( pId, IID_IClassFactory, (void **) &pFact ) );
   
	// Create object from factory
	CCLTRY(pFact->CreateInstance ( pOuter, iid, ppv ) );

	// Clean up
	_RELEASE(pFact);

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_ERR, L"cclCreateObject : Fail : %s : 0x%x", pId, hr );

	return hr;
	}	// cclCreateObject

extern "C" DLLEXPORT
HRESULT cclGetFactory ( const wchar_t *pId, REFIID iid, void **ppv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Obtain class factory for specified object.
	//
	//	PARAMETERS
	//		-	pId is the class Id
	//		-	iid is the factory interface to query for
	//		-	ppv will receive the factory object
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	IClassFactory	*pFact	= NULL;
	U32				sz			= 0;
	adtValue			vL;

	// State check
	*ppv = NULL;
	CCLTRYE ( pDctFct != NULL, ERROR_INVALID_STATE );
	CCLTRY  ( pDctFct->size ( &sz ) );

	// Check cache for pre-existing factory
	if (hr == S_OK && pDctFct->load ( adtString(pId), vL ) == S_OK)
		{
		adtIUnknown	unkV(vL);

		// Factory object
		CCLTRY ( _QISAFE(unkV,IID_IClassFactory,&pFact) );
//		dbgprintf ( L"cclCreateObject:Factory cached:pFact:%p:0x%x\r\n", pFact, hr );
		}	// if

	// New factory required
	else if (hr == S_OK)
		{
		void				*pvLib	= NULL;
		adtString		strLib,strDir;
		const wchar_t	*wDot;

		// Name of library
		#ifdef	_WIN32
		CCLOK ( strLib = L""; )							// No prefix for Windows
		#else
		CCLOK ( strLib = L"lib"; )
		#endif
		CCLTRY( strLib.append ( pId ) );
			
		// Find dot separating module and object name and finish path
		CCLOK ( strLib.toLower(); )
		CCLTRYE ( (wDot = wcsrchr ( &strLib.at(), '.' )) != NULL, E_UNEXPECTED );
		CCLOK   ( strLib.at(wDot-(LPCWSTR)strLib) = '\0'; )
		#ifdef	_WIN32
		CCLTRY  ( strLib.append ( L"n.dll" ) );
		#elif		__APPLE__
		CCLTRY  ( strLib.append ( L"n.dylib" ) );
		#else
		CCLTRY  ( strLib.append ( L"n.so" ) );
		#endif

		// Generate full path to library
		CCLOK   ( strDir = cDirProc; )
		CCLTRY  ( strLib.prepend ( (LPCWSTR) strDir ) );

		// Obtain factory
		CCLTRY ( cclLoadFactoryInt ( strLib, pId, &pvLib, &pFact ) );

		// Cache factory under Id
		CCLTRY ( pDctFct->store ( adtString(pId), adtIUnknown(pFact) ) );
//		dbgprintf ( L"cclCreateObject:New factory cached:pFact:%p:%s:0x%x\r\n", pFact, pId, hr );
		}	// else if

	// Result
	if (hr == S_OK)
		{
		(*ppv) = pFact;
		pFact->AddRef();
		}	// if
	
	// Clean up
	_RELEASE(pFact);

	// Debug
//	dbgprintf ( L"cclGetFactory:%s:0x%x:0x%x\r\n", pId, (int)(pDctFct), *((int *)ppv) );
	if (hr != S_OK)
		lprintf ( LOG_ERR, L"cclGetFactory : Fail : %s : 0x%x", pId, hr );

	return hr;
	}	// cclGetFactory

extern "C" DLLEXPORT
HRESULT	cclInitialize ( void *pv, DWORD dwCoInit )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Initializes the CCL implementation.
	//
	//	PARAMETERS
	//		-	pv is not used.
	//		-	dwCoInit on Windows specifies initialization flags
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 			= S_OK;

	// For Windows, allow normal COM to work as well
	#ifdef	_WIN32
	CoInitializeEx ( NULL, COINIT_MULTITHREADED );
	#endif

	// Reference count
//	lprintf ( LOG_DBG, L"Reference count %d", coRefCnt );
	if (InterlockedIncrement(&coRefCnt) > 1)
		return S_OK;

	// Currently it is assumed internal shared libraries are in the same
	// directory as the main process.  This can be overrideen by the
	// use of environment variables
   if (cDirProc[0] == '\0')
      {
		char *ls,*env	= NULL;
		size_t	len	= 0;

		// Environment variable override ?
		#ifdef	_WIN32
		if (_dupenv_s(&env,&len,"NSPACE_BIN") == 0 && len > 0)
			strcpy_s ( cDirProc, env );
		#else
		if ((env = getenv("NSPACE_BIN")) != NULL)
			strcpy ( cDirProc, env );
		#endif
		else
			{
			// Attempt to get the path to the process EXE
			#if defined(_WIN32)
			WCHAR	dllname[MAX_PATH];
			GetModuleFileName ( NULL, dllname, sizeof(dllname)/sizeof(WCHAR) );
			sprintf_s ( cDirProc, "%S", dllname );
			#elif defined(__unix__)
			CCLTRYE ( readlink ( "/proc/self/exe", cDirProc, sizeof(cDirProc) ) != -1, errno );
			#elif defined(__APPLE__)
			CCLTRYE ( proc_pidpath ( getpid(), cDirProc, sizeof(cDirProc) )
							> 0, errno );
			#endif
			
			// Truncate to path
			if (hr == S_OK && 
				(	((ls = strrchr(cDirProc,'/')) != NULL) || 
					((ls = strrchr(cDirProc,'\\')) != NULL) ) )
				*(ls+1) = '\0';
			}	// else

		// Ensure trailing slash
		len = strlen(cDirProc);
		if (	cDirProc[len-1] != '/' &&
				cDirProc[len-1] != '\\')
			#if defined(_WIN32)
			strcat_s ( cDirProc, "\\" );
			#else
			strcat ( cDirProc, "/" );
			#endif

		// Clean up
		#ifdef	_WIN32
		if (env != NULL)
			free(env);
		#endif
		}  // if

	// A dictionary is used to keep track of loaded modules, ensure dictionary
	// (and required library) exists
	if (hr == S_OK && pLstLib == NULL)
		{
		void				*pvLib	= NULL;
		IClassFactory	*pFact	= NULL;
		adtString		strLib;
		#ifdef	_WIN32
		adtString		strAdt(L"adtn.dll");
		#elif		__APPLE__
		adtString		strAdt(L"libadtn.dylib");
		#else
		adtString		strAdt(L"libadtn.so");
		#endif
		adtString		strDct(L"Adt.Dictionary");

		// Location of required library
		CCLOK ( strLib = cDirProc; )
		CCLTRY( strLib.append ( strAdt ) );

		// Attempt to get class factory for dictionary
		CCLTRY ( cclLoadFactoryInt ( strLib, strDct, &pvLib, &pFact ) );

		// Create a dictionary for the libraries and factories
		CCLTRY ( pFact->CreateInstance ( NULL, IID_IDictionary, (void **) &pDctLib ) );
		CCLTRY ( pFact->CreateInstance ( NULL, IID_IDictionary, (void **) &pLstLib ) );
		CCLTRY ( pFact->CreateInstance ( NULL, IID_IDictionary, (void **) &pDctFct ) );

		// Store own library in dictionary
		CCLTRY ( pDctLib->store ( strLib, adtLong((U64)pvLib) ) );
		CCLTRY ( pLstLib->store ( adtInt(1), adtLong((U64)pvLib) ) );

		// Store factory in cache
		CCLTRY ( pDctFct->store ( strDct, adtIUnknown(pFact) ) );

		// Clean up
		_RELEASE(pFact);
		}	// if

	// Debug
//	dbgprintf ( L"CoInitialize : %S:0x%x\r\n", cDirProc, hr );
		
	return hr;
	}	// cclInitialize

HRESULT cclLoadFactoryInt	( const wchar_t *pwLib, const wchar_t *pwId,
										void **ppvLib, IClassFactory **ppFact )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Loads/creates a factory for the specified object from the
	//			specified shared library.
	//
	//	PARAMETERS
	//		-	pwLib is the path to the library
	//		-	pwId is the object Id
	//		-	ppvLib will receive the handle to the loaded library
	//		-	ppFact will receive the factory
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr 															= S_OK;
	HRESULT 		(*cclgco) ( const wchar_t *, REFIID, void ** ) 	= NULL;
	char			*pcLib														= NULL;
	adtLong		iHandle;
	adtString	strErr,sLib(pwLib);
	
	// Setup
	(*ppvLib) 	= NULL;
	(*ppFact)	= NULL;
	
	// Load library
	if (hr == S_OK)
		{
		// Already loaded ?
		if (pDctLib == NULL || pDctLib->load ( sLib, iHandle ) != S_OK)
			{
			// Debug
			CCLOK 	( lprintf ( LOG_DBG, L"Loading '%s'", (LPCWSTR)sLib ); )

			#ifdef	_WIN32

			// Load DLL manually
			CCLTRYE ( ((*ppvLib) = LoadLibrary ( sLib )) != NULL, GetLastError() );
			if (hr != S_OK)
				lprintf ( LOG_ERR, L"Load failed : %s,0x%x", (LPCWSTR)sLib, hr );
			#else
			// Ascii version for API
			CCLTRY ( sLib.toAscii ( &pcLib ) );

			// Load
			CCLTRYE 	( ((*ppvLib) = dlopen ( pcLib, RTLD_NOW|RTLD_LOCAL )) != NULL, E_UNEXPECTED );
			if (hr != S_OK)
				{
				strErr = dlerror();
				lprintf ( LOG_ERR, L"Load failed : %s", (LPCWSTR) strErr );
				}	// if
			#endif

			// Cache
			if (hr == S_OK && pDctLib != NULL)
				hr = cclAddLibrary ( sLib, (U64) (*ppvLib) );
			/*
			CCLOK  ( iHandle = (U64) (*ppvLib); )
			if (hr == S_OK && pDctLib != NULL)
				{
				U32	sz = 0;

				// Add to dictionary
				CCLTRY ( pDctLib->store ( sLib, iHandle ) );

				// Add to list
				CCLTRY ( pLstLib->size ( &sz ) );
				CCLTRY ( pLstLib->store ( adtInt(sz+1), iHandle ) );
				}	// if
			*/

			// Clean up
			_FREEMEM(pcLib);
			}	// if
		else
			(*ppvLib) = (void *)(U64)iHandle;
		}	// if

	// Entry point
	if (hr == S_OK)
		{
		#ifdef	_WIN32
		CCLTRYE ( (cclgco = (HRESULT (*) (const wchar_t *, REFIID, void **)) 
						GetProcAddress ( (HMODULE) (*ppvLib), "cclGetClassObject" )) != NULL, GetLastError () );
		#else
		cclgco = (HRESULT (*) (const wchar_t *, REFIID, void **)) dlsym ( (*ppvLib), "cclGetClassObject" );
		#endif
		hr = (cclgco != NULL) ? S_OK : E_UNEXPECTED;
		if (hr != S_OK)
			{
			#ifdef _WIN32
			lprintf ( LOG_ERR, L"Symbol failed : 0x%x", hr );
			#else
			strErr = dlerror();
			lprintf ( LOG_ERR, L"Symbol failed : %s", (LPCWSTR)strErr );
			#endif
			}	// if
		}	// if

	// Create the factory
	CCLTRY ( (cclgco) ( pwId, IID_IClassFactory, (void **) ppFact ) );

	// Clean up
	if (hr != S_OK)
		{
		_RELEASE((*ppFact));
		#ifdef _WIN32
		if ((*ppvLib) != NULL) FreeLibrary((HMODULE)(*ppvLib));
		#else
		if ((*ppvLib) != NULL) dlclose((*ppvLib));
		#endif
		*ppvLib = NULL;
		}	// if

	return hr;
	}	// cclLoadFactoryInt

extern "C" DLLEXPORT
void cclUninitialize ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Uninitializes the CCL implementation.
	//
	////////////////////////////////////////////////////////////////////////

	// For Windows...
	#ifdef	_WIN32
	CoUninitialize();
	#endif

	// Reference count
	lprintf ( LOG_DBG, L"Reference count %d, pLstLib %p", coRefCnt, pLstLib );
	if (InterlockedDecrement(&coRefCnt) > 0)
		return;

	// Free previously loaded libraries
	if (pLstLib != NULL)
		{
		HRESULT	hr		= S_OK;
		IIt		*pIt	= NULL;
		U64		*phs	= NULL;
		S32		i		= 0;
		U32		sz		= 0;
		adtValue	vL;

		// Debug, list libraries that will be freed along with their ptr.
		CCLTRY ( pDctLib->keys ( &pIt ) );
		while (hr == S_OK && pIt->read ( vL ) == S_OK)
			{
			adtString 	strLib(vL);
			adtValue		vH;
			if (pDctLib->load ( strLib, vH ) == S_OK)
				lprintf ( LOG_DBG, L"strLib %s Handle 0x%lx\r\n",
								(LPCWSTR)strLib, (U64)adtLong(vH) );
			pIt->next();
			}	// while
		_RELEASE(pIt);

		// NOTE: Does it make sense to manually unload the libraries or let the OS do it.
		// Given the complexities of shared libraries, for now assume program shutdown and
		// let the OS handle it (Linux).
		/*
		// NOTE: Even the library that contains dictionary implementation
		// will be freed, so first, generate a list of libraries to be
		// freed before actually unloading libraries.
		CCLTRY ( pLstLib->size(&sz) );
		CCLOK ( lprintf ( LOG_DBG, L"Library list size : %d\r\n", sz ); )
		CCLTRYE( (phs = (U64 *) _ALLOCMEM ( sz*sizeof(U64) )) != NULL,
					E_OUTOFMEMORY );

		//
		// Iterate and store handles in array
		//
		CCLTRY ( pLstLib->keys ( &pIt ) );
		while (hr == S_OK && pIt->read ( vL ) == S_OK)
			{
			adtInt		vIdx(vL);
			adtValue		vH;
			if (pLstLib->load ( vIdx, vH ) == S_OK)
				{
				U64	tst = (U64)adtLong(vH);
				lprintf ( LOG_DBG, L"Index %d,%d Handle 0x%lx\r\n",
								(U32)vIdx, i, tst );
				phs[i++] = (U64)adtLong(vH);
				}	// if
			pIt->next();
			}	// while


		// Iterate and populate list in the reverse order that
		// libraries were loaded
		CCLTRY ( pLstLib->iterate ( &pIt ) );
		while (hr == S_OK && pIt->read ( vL ) == S_OK)
			{
			lprintf ( LOG_DBG, "vL %d,0x%lx\r\n", vL.vtype, vL.vlong );

			// Add and move to next library
			lprintf ( LOG_DBG, L"Library handle 0x%lx\r\n", (U64)adtLong(vL) );
			phs[sz-i-1] = (U64)adtLong(vL);
			++i;
			pIt->next();
			}	// while
		*/

		// Free dictionaries before releasing shared libraries
		_RELEASE(pIt);
		_RELEASE(pDctFct); 
		_RELEASE(pLstLib);
		_RELEASE(pDctLib);
		/*
		// Free libraries in reverse load order
		for (i = sz-1;i >= 0;--i)
			{
			lprintf ( LOG_DBG, L"Free handle %d, 0x%lx\r\n", i, phs[i] );
			#ifdef	_WIN32
//			FreeLibrary ( (HMODULE)phs[i] );
			#else
//			dlclose ( (void *)phs[i] );
			#endif
			}	// for
		*/

		// Clean up
		_FREEMEM(phs);
		}	// if

	}	// cclUninitialize

#ifndef _WIN32
extern "C"
DLLEXPORT
HRESULT StringFromCLSID ( REFCLSID clsid, WCHAR **ppwStr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts a CLSID to a string.
	//
	//	PARAMETERS
	//		-	clsid is the class Id to use
	//		-	vStr will receive the string
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Allocate memory for string (caller must free)
	CCLTRYE( ((*ppwStr) = (WCHAR *) _ALLOCMEM ( 40*sizeof(wchar_t) ))
					!= NULL, E_OUTOFMEMORY );


	// Format string
	if (hr == S_OK)
		swprintf ( SWPF((*ppwStr),39),
						L"{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}",
						clsid.Data1, clsid.Data2, clsid.Data3,
						clsid.Data4[0], clsid.Data4[1], clsid.Data4[2],
						clsid.Data4[3], clsid.Data4[4], clsid.Data4[5],
						clsid.Data4[6], clsid.Data4[7] );

	return S_OK;
	}	// StringFromCLSID
#endif
