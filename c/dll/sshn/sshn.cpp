////////////////////////////////////////////////////////////////////////
//
//									SSHN.CPP
//
//					Main file for the SSH node library
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#define	CCL_OBJ_MODULE		L"Ssh"

// Library implementations
#include "../../lib/sshl/sshl_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()

	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Avail)
	CCL_OBJLIST_ENTRY	(Channel)
	CCL_OBJLIST_ENTRY	(Key)
	CCL_OBJLIST_ENTRY	(Scp)
	CCL_OBJLIST_ENTRY	(Session)

CCL_OBJLIST_END()
