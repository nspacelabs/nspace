cmake_minimum_required(VERSION 3.0.2 FATAL_ERROR)

project("drmn")

set	(	
		SOURCES
		drmn.cpp
		)

set	(
		HEADERS
		)

add_library (drmn SHARED ${SOURCES} ${HEADERS})

target_link_libraries(drmn drml adtl nspcl ccln sysl drm kms)

set_property(TARGET ${PROJECT_NAME} PROPERTY FOLDER dll)

# Installation
install	(
			TARGETS drmn 
			RUNTIME DESTINATION bin 
			LIBRARY DESTINATION bin
			ARCHIVE DESTINATION bin
			)


