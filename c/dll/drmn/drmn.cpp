////////////////////////////////////////////////////////////////////////
//
//									DRMN.CPP
//
//				Main file for the DRM interface library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"Drm"

// Library implementations
#include "../../lib/drml/drml_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()
	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Buffer)
	CCL_OBJLIST_ENTRY	(Device)
	CCL_OBJLIST_ENTRY	(Mode)
	CCL_OBJLIST_ENTRY	(Property)

CCL_OBJLIST_END()

