////////////////////////////////////////////////////////////////////////
//
//									DRMN.CPP
//
//				Main file for the Libtorch interface library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"Torch"

// Library implementations
#include "../../lib/torchl/torchl_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()
	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Activation)
	CCL_OBJLIST_ENTRY	(DataLoader)
	CCL_OBJLIST_ENTRY	(Dropout)
	CCL_OBJLIST_ENTRY	(Linear)
	CCL_OBJLIST_ENTRY	(Loss)
	CCL_OBJLIST_ENTRY	(Net)
	CCL_OBJLIST_ENTRY	(Optimize)
	CCL_OBJLIST_ENTRY	(Reshape)
	CCL_OBJLIST_ENTRY	(Size)
	CCL_OBJLIST_ENTRY	(TensorOp)

CCL_OBJLIST_END()
