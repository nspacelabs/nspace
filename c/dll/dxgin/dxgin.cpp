////////////////////////////////////////////////////////////////////////
//
//									DXGIN.CPP
//
//	Main file for the DirectX Graphics Infrastructure (DXGI) library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"Dxgi"

// Library implementations
#include "../../lib/dxgil/dxgil_.h"

// Objects in this module
CCL_OBJLIST_BEGIN()
	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Adapter)
	CCL_OBJLIST_ENTRY	(Factory)
	CCL_OBJLIST_ENTRY	(Output)
	CCL_OBJLIST_ENTRY	(Share)
	CCL_OBJLIST_ENTRY	(SwapChain)

CCL_OBJLIST_END()

