////////////////////////////////////////////////////////////////////////
//
//									CUDAN.CPP
//
//				Main file for the CUDA library
//
////////////////////////////////////////////////////////////////////////

#define	CCL_OBJ_MODULE		L"Cuda"

// Library implementations
#include "../../lib/cudal/cudal_.h"
#define RF_POWER_UP 0x02, 0x01, 0x01, 0x01, 0x8C, 0xBA, 0x80
// Objects in this module
CCL_OBJLIST_BEGIN()
	// Objects

	// Nodes
	CCL_OBJLIST_ENTRY	(Graphics)
	CCL_OBJLIST_ENTRY	(MemoryOp)
	
CCL_OBJLIST_END()
