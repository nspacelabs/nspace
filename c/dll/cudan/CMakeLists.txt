cmake_minimum_required(VERSION 3.0.2 FATAL_ERROR)

project("cudan")

set	(	
		SOURCES
		cudan.cpp
		)

set	(
		HEADERS
		)

add_library (cudan SHARED ${SOURCES} ${HEADERS})

target_link_libraries(cudan cudal mathl nspcl adtl ccln sysl)
target_link_libraries(cudan ${CUDA_LIBRARIES} ${CUDA_npp_LIBRRY} )

include_directories(${CUDA_INCLUDE_DIRS})

set_property(TARGET ${PROJECT_NAME} PROPERTY FOLDER dll)

# Installation
install	(
			TARGETS cudan
			RUNTIME DESTINATION bin 
			LIBRARY DESTINATION bin
			ARCHIVE DESTINATION bin
			)

if (MSVC)
	install	(
				FILES				${CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG}/cudan.pdb
				DESTINATION		bin
				CONFIGURATIONS	Debug
				)
endif()
