////////////////////////////////////////////////////////////////////////
//
//									DEVICE.CPP
//
//				Implementation of the USB device node.
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#include "usbl_.h"
#include <stdio.h>

// Globals

Device :: Device ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	pCtx		= NULL;
	ppDevs	= NULL;
	iSet		= 0;
	iCnf		= 1;
	iIntf		= 0;
	iIdx		= 0;
	iCnt		= 0;
	}	// Device

#if 0 // #ifdef		_WIN32
HRESULT Device :: getString ( WINUSB_INTERFACE_HANDLE hIntf, S32 iIdx, 
										adtString &strV )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve a string descriptor
	//
	//	PARAMETERS
	//		-	hIntf is the interface handle
	//		-	iIdx is the string index
	//		-	strV will receive the string
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	ULONG		nx;
	WCHAR		wStr[200];

	// Setup
	strV = L"";

	// State check
	CCLTRYE ( iIdx > 0, E_UNEXPECTED );

	// Retrieve descriptor
	CCLTRYE ( WinUsb_GetDescriptor ( hIntf, USB_STRING_DESCRIPTOR_TYPE, iIdx,
					0, (PUCHAR) wStr, sizeof(wStr), &nx ) == TRUE, GetLastError() );

	// Terminate string using descriptor length in first byte
	CCLTRYE	( (nx = ((U8 *)wStr)[0]) > 2, E_UNEXPECTED );
	CCLOK		( wStr[nx/2] = '\0'; )

	// Result
	CCLOK		( strV = &wStr[1]; )
	CCLOK		( strV.at(); )

	// First 2 bytes are langage Id, terminate string
	CCLOK ( lprintf ( LOG_INFO, L"String index %d : (%d,%d) %s\r\n", iIdx, 
							((char *)(wStr))[0],
							((char *)(wStr))[1],
							&(wStr[1]) ); )

	return hr;
	}	// getString
#endif

HRESULT Device :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Setting"), vL ) == S_OK)
			iSet = vL;
		if (pnDesc->load ( adtString(L"Interface"), vL ) == S_OK)
			iIntf = vL;
		if (pnDesc->load ( adtString(L"Configuration"), vL ) == S_OK)
			iCnf = vL;

		// Context for own use
		int 
		ret = libusb_init ( &pCtx );
		if (ret < 0)
			lprintf ( LOG_DBG, L"Unable to initialize USB system API ret %d\r\n", ret );
		}	// if

	// Detach
	else
		{
		// Shutdown
		if (ppDevs != NULL)
			{
			libusb_free_device_list(ppDevs,1);
			ppDevs = NULL;
			}	// if
		if (pCtx != NULL)
			{
			libusb_exit(pCtx);
			pCtx = NULL;
			}	// if
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Device :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	//
	// Enumeration
	//
	if (_RCP(First))
		{
		int ret = 0;

		// State check
		CCLTRYE ( pDct != NULL && pCtx != NULL, ERROR_INVALID_STATE );

		// Previous iteration
		iIdx = 0;
		if (ppDevs != NULL)
			{
			libusb_free_device_list(ppDevs,1);
			ppDevs = NULL;
			}	// if

		// Obtain count
		if (hr == S_OK && (iCnt = (S32)libusb_get_device_list(pCtx,&ppDevs)) < 0)
			{
			lprintf ( LOG_WARN, L"Unable to get device list %d:%S\r\n",
										iCnt, libusb_error_name(iCnt) );
			hr = E_UNEXPECTED;
			}	// if

		// Emit next device
		CCLOK ( onReceive ( prNext, v ); )
		}	// if

	// Next device
	else if (_RCP(Next))
		{
		int										ret = 0;
		struct libusb_device_descriptor 	desc;

		// State check
		CCLTRYE ( (iIdx < iCnt), ERROR_INVALID_STATE );

		// Next device
		CCLTRYE ( (ret = libusb_get_device_descriptor ( ppDevs[iIdx], &desc )) >= 0,
						E_UNEXPECTED );

		// Device information
		if (hr == S_OK)
			{
			// Info
			CCLTRY ( pDct->clear() );
			CCLTRY ( pDct->store ( adtString(L"IdVendor"), adtInt(desc.idVendor) ) );
			CCLTRY ( pDct->store ( adtString(L"IdProduct"), adtInt(desc.idProduct) ) );
			CCLTRY ( pDct->store ( adtString(L"MaxPacketSize"), 
						adtInt(desc.bMaxPacketSize0) ) );
			CCLTRY ( pDct->store ( adtString(L"iSerialNumber"), 
						adtInt(desc.iSerialNumber) ) );
			CCLTRY ( pDct->store ( adtString(L"iManufacturer"), 
						adtInt(desc.iManufacturer) ) );
			CCLTRY ( pDct->store ( adtString(L"iProduct"), 
						adtInt(desc.iProduct) ) );

			// Next device
			++iIdx;
			}	// if

		// Emit next device or signal end
		if (hr == S_OK)
			_EMT(Next,adtIUnknown(pDct));
		else
			_EMT(End,adtInt(0));
		}	// else if

	// Open active device
	else if (_RCP(Open))
		{
		int 										ret 		= 0;
		struct libusb_device_handle		*pDevH	= NULL;
		IList										*pLstCnf	= NULL;
		struct libusb_device_descriptor	desc;
		int										val;

		// State check
		CCLTRYE ( pDct != NULL && ppDevs != NULL && iIdx > 0 && iIdx <= iCnt,
	 					ERROR_INVALID_STATE );

		// Attempt to open the currently enumerated device 
		// Index is incremented after enumeration so the 'active' one is one less
		// that the current index
		if (hr == S_OK && (ret = libusb_open ( ppDevs[iIdx-1], &pDevH )) < 0)
			{
			lprintf ( LOG_WARN, L"Unable to open USB device at index %d,%S\r\n", 
										iIdx, libusb_error_name(ret) );
			hr = E_UNEXPECTED;
			}	// if

		// Descriptor
		CCLTRYE ( (ret = libusb_get_device_descriptor ( ppDevs[iIdx-1], &desc ))
						== 0, ret );

		// Create list for configurations and store in device dictionary
		CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLstCnf ) );
		CCLTRY ( pDct->store ( adtString(L"Config"), adtIUnknown(pLstCnf) ) );

		// Configurations
		for (U32 i = 0;hr == S_OK && i < desc.bNumConfigurations;++i)
			{
			struct libusb_config_descriptor	*pCnf		= NULL;
			IDictionary								*pDctCnf	= NULL;
			IList										*pLstInt	= NULL;

			// Retrieve configuration information
			if (libusb_get_config_descriptor ( ppDevs[iIdx-1], i, &pCnf ) 
					!= LIBUSB_SUCCESS)
				continue;

			// Create dictionary for configuration
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctCnf ) );
			CCLTRY ( pLstCnf->write ( adtIUnknown(pDctCnf) ) );
			CCLTRY ( pDctCnf->store ( adtString(L"NumInterfaces"), adtInt(pCnf->bNumInterfaces) ) );
			CCLTRY ( pDctCnf->store ( adtString(L"MaxPower"), adtInt(pCnf->MaxPower) ) );

			// Create and store interface list
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLstInt ) );
			CCLTRY ( pDctCnf->store ( adtString(L"Interface"), adtIUnknown(pLstInt) ) );

			// Interfaces
			for (U32 j = 0;hr == S_OK && j < pCnf->bNumInterfaces;++j)
				{
				IDictionary *pDctInt = NULL;
				IList			*pLstSet	= NULL;
				const struct libusb_interface *
				pInt						= &(pCnf->interface[j]);

				// Create dictionary for interface
				CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctInt ) );
				CCLTRY ( pLstInt->write ( adtIUnknown(pDctInt) ) );
				CCLTRY ( pDctInt->store ( adtString(L"NumSettings"), adtInt(pInt->num_altsetting) ) );

				// Create and store settings list
				CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLstSet ) );
				CCLTRY ( pDctInt->store ( adtString(L"Setting"), adtIUnknown(pLstSet) ) );

				// Alternate settings
				for (int k = 0;hr == S_OK && k < pInt->num_altsetting;++k)
					{
					const struct libusb_interface_descriptor *
					pSet						= &(pInt->altsetting[k]);
					IDictionary *pDctSet = NULL;
					IList			*pLstEnd	= NULL;

					// Create dictionary for setting
					CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctSet ) );
					CCLTRY ( pLstSet->write ( adtIUnknown(pDctSet) ) );
					CCLTRY ( pDctSet->store ( adtString(L"NumEndpoints"), adtInt(pSet->bNumEndpoints) ) );
					CCLTRY ( pDctSet->store ( adtString(L"Class"), adtInt(pSet->bInterfaceClass) ) );
					CCLTRY ( pDctSet->store ( adtString(L"SubClass"), adtInt(pSet->bInterfaceSubClass) ) );
					CCLTRY ( pDctSet->store ( adtString(L"Protocol"), adtInt(pSet->bInterfaceProtocol) ) );

					// Create and store settings list
					CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLstEnd ) );
					CCLTRY ( pDctSet->store ( adtString(L"Endpoint"), adtIUnknown(pLstEnd) ) );

					// Endpoints
					for (U32 l = 0;hr == S_OK && l < pSet->bNumEndpoints;++l)
						{
						const struct libusb_endpoint_descriptor *
						pEnd						= &(pSet->endpoint[l]);
						IDictionary *pDctEnd = NULL;

						// Create dictionary for endpoint
						CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctEnd ) );
						CCLTRY ( pLstEnd->write ( adtIUnknown(pDctEnd) ) );
						CCLTRY ( pDctEnd->store ( adtString(L"Address"), adtInt(pEnd->bEndpointAddress)));
						CCLTRY ( pDctEnd->store ( adtString(L"Attributes"), adtInt(pEnd->bmAttributes)));
						CCLTRY ( pDctEnd->store ( adtString(L"MaxPacketSize"), adtInt(pEnd->wMaxPacketSize)));
						CCLTRY ( pDctEnd->store ( adtString(L"Interval"), adtInt(pEnd->bInterval)));
						CCLTRY ( pDctEnd->store ( adtString(L"Refresh"), adtInt(pEnd->bRefresh)));
						CCLTRY ( pDctEnd->store ( adtString(L"SyncAddress"), adtInt(pEnd->bSynchAddress)));

						// Clean up
						_RELEASE(pDctEnd);
						}	// for

					// Clean up
					_RELEASE(pLstEnd);
					_RELEASE(pDctSet);
					}	// for

				// Clean up
				_RELEASE(pLstSet);
				_RELEASE(pDctInt);
				}	// for

			// Clean up
			_RELEASE(pLstInt);
			_RELEASE(pDctCnf);
			libusb_free_config_descriptor(pCnf);
			}	// for

		// Active configuration
		CCLTRYE ( (ret = libusb_get_configuration ( pDevH, &val )) == 0, ret );
		if (hr == S_OK && iCnf != val)
			{
			// Set desired configuration
			CCLTRYE ( (ret = libusb_set_configuration ( pDevH, iCnf )) == 0, ret );
			}	// if			

		// Alternate settings
		if (hr == S_OK && iSet != 0)
			{
			CCLTRYE ( (ret = libusb_set_interface_alt_setting ( pDevH, iIntf, iSet )) == 0, ret );
			}	// if

		// Activate desired interface
		CCLTRYE ( (ret = libusb_claim_interface ( pDevH, iIntf )) == 0, ret );

		// Result
		CCLTRY ( pDct->store ( adtString(L"Device"), adtLong((U64)pDevH) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Close active device
	else if (_RCP(Close))
		{
		adtValue vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Closing
		CCLOK(_EMT(Close,adtIUnknown(pDct));)

		// Valid device ?
		if (hr == S_OK && pDct->load ( adtString(L"Device"), vL ) == S_OK)
			{
			struct libusb_device_handle *pDev =
				(struct libusb_device_handle *)(U64)adtLong(vL);

			// Clean up
			if (pDev != NULL)
				libusb_close(pDev);
			pDct->remove ( adtString(L"Device") );
			}	// if

		}	// else if

	// State
	else if (_RCP(Device))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Setting))
		iSet = adtInt(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

