////////////////////////////////////////////////////////////////////////
//
//									Devices.CPP
//
//			Implementation of the USB devices node
//
////////////////////////////////////////////////////////////////////////

#include "usbl_.h"

#if 	__unix__

Devices :: Devices ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	////////////////////////////////////////////////////////////////////////

	// Setup
	ppDevs	= NULL;
	pCtx		= NULL;
	pDct		= NULL;
	iCnt		= 0;
	iIdx		= 0;
	}	// Devices

HRESULT Devices :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{

		// Context for own use
		#if 	__unix__
		int 
		ret = libusb_init ( &pCtx );
		if (ret < 0)
			lprintf ( LOG_DBG, L"Unable to initialize USB system API ret %d\r\n", ret );
		#endif

		}	// if

	// Detach
	else
		{
		// Clean up
		if (ppDevs != NULL)
			{
			libusb_free_device_list(ppDevs,1);
			ppDevs = NULL;
			}	// if
		if (pCtx != NULL)
			{
			libusb_exit(pCtx);
			pCtx = NULL;
			}	// if
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Devices :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Enumeration
	if (_RCP(First))
		{
		int ret = 0;

		// State check
		CCLTRYE ( pDct != NULL && pCtx != NULL, ERROR_INVALID_STATE );

		// Previous iteration
		if (ppDevs != NULL)
			{
			libusb_free_device_list(ppDevs,1);
			ppDevs = NULL;
			}	// if

		// Obtain count
		if ((iCnt = libusb_get_device_list(pCtx,&ppDevs)) < 0)
			{
			lprintf ( LOG_WARN, L"Unable to get device list %d:%S\r\n",
										iCnt, libusb_error_name(iCnt) );
			hr = E_UNEXPECTED;
			}	// if

		// Emit next device
		CCLOK ( onReceive ( prNext, v ); )
		}	// if

	// Next device
	else if (_RCP(Next))
		{
		int										ret = 0;
		struct libusb_device_descriptor 	desc;

		// State check
		CCLTRYE ( (iIdx < iCnt), ERROR_INVALID_STATE );

		// Next device
		CCLTRYE ( (ret = libusb_get_device_descriptor ( ppDevs[iIdx], &desc )) >= 0,
						E_UNEXPECTED );

		// Device information
		if (hr == S_OK)
			{
			// Info
			CCLTRY ( pDct->clear() );
			CCLTRY ( pDct->store ( adtString(L"IdVendor"), adtInt(desc.idVendor) ) );
			CCLTRY ( pDct->store ( adtString(L"IdProduct"), adtInt(desc.idProduct) ) );
			CCLTRY ( pDct->store ( adtString(L"MaximumPacketSize"), 
						adtInt(desc.bMaxPacketSize0) ) );
			CCLTRY ( pDct->store ( adtString(L"iSerialNumber"), 
						adtInt(desc.iSerialNumber) ) );
			CCLTRY ( pDct->store ( adtString(L"iManufacturer"), 
						adtInt(desc.iManufacturer) ) );
			CCLTRY ( pDct->store ( adtString(L"iProduct"), 
						adtInt(desc.iProduct) ) );

			// Next device
			++iIdx;
			}	// if

		// Emit next device or signal end
		if (hr == S_OK)
			_EMT(Next,adtIUnknown(pDct));
		else
			_EMT(End,adtInt(0));
		}	// else if

	// Open active device
	else if (_RCP(Open))
		{
		struct libusb_device_handle	*pDev = NULL;
		int 									ret 	= 0;

		// State check
		CCLTRYE ( pDct != NULL && ppDevs != NULL && iIdx > 0 && iIdx <= iCnt,
	 					ERROR_INVALID_STATE );

		// Attempt to open the currently enumerated device 
		// Index is incremented after enumeration so the 'active' one is one less
		// that the current index
		if (hr == S_OK && (ret = libusb_open ( ppDevs[iIdx-1], &pDev )) < 0)
			{
			lprintf ( LOG_WARN, L"Unable to open USB device at index %d,%s\r\n", 
										iIdx, libusb_error_name(ret) );
			hr = E_UNEXPECTED;
			}	// if

		// Result
		CCLTRY ( pDct->store ( adtString(L"Device"), adtLong((U64)pDev) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Close active device
	else if (_RCP(Close))
		{
		adtValue vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Closing
		CCLOK(_EMT(Close,adtIUnknown(pDct));)

		// Valid device ?
		if (hr == S_OK && pDct->load ( adtString(L"Device"), vL ) == S_OK)
			{
			struct libusb_device_handle *pDev =
				(struct libusb_device_handle *)(U64)adtLong(vL);

			// Clean up
			if (pDev != NULL)
				libusb_close(pDev);
			pDct->remove ( adtString(L"Device") );
			}	// if

		}	// else if

	// State
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

#endif

