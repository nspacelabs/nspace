////////////////////////////////////////////////////////////////////////
//
//										USBL_.H
//
//				Implementation include file for USB library
//
////////////////////////////////////////////////////////////////////////

#ifndef	USBL__H
#define	USBL__H

// Includes
#include "usbl.h"
#include "../../lib/nspcl/nspcl.h"

// External API
#ifdef	_WIN32
#include <libusb.h>
#else
#include <libusb-1.0/libusb.h>
#endif

///////////
// Objects
///////////


/////////
// Nodes
/////////

//
// Class - Device.  USB device node.
//

class Device :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Device ( void );										// Constructor

	// Run-time data
	IDictionary					*pDct;					// Active context
	struct libusb_context	*pCtx;					// USB context
	struct libusb_device		**ppDevs;				// Enumerated device list 
	adtInt						iIntf;					// Interace number
	adtInt						iCnf;						// Configuration number
	adtInt						iSet;						// Setting number
	S32							iIdx,iCnt;				// Enumeration index and count

	// CCL
	CCL_OBJECT_BEGIN(Device)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)

	DECLARE_RCP(First)
	DECLARE_CON(Next)
	DECLARE_EMT(End)

	DECLARE_CON(Close)
	DECLARE_CON(Open)

	DECLARE_EMT(Error)
	DECLARE_RCP(Setting)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)

		DEFINE_RCP(First)
		DEFINE_CON(Next)
		DEFINE_EMT(End)

		DEFINE_CON(Close)
		DEFINE_CON(Open)

		DEFINE_EMT(Error)
		DEFINE_RCP(Setting)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
//	#ifdef	_WIN32
//	HRESULT getString ( WINUSB_INTERFACE_HANDLE, S32, adtString & );
//	#endif

	};

//
// Class - Endpoint.  WinUSB Endpoint node.
//

class Endpoint :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Endpoint ( void );									// Constructor

	// Run-time data
	IDictionary		*pDctDev;							// Device dictionary
	IDictionary		*pDctEnd;							// End point dictionary
	U8					*pcBfr;								// Internal buffer
	U32				szBfr;								// Internal buffer size
	IByteStream		*pStmIo;								// I/O stream
	adtInt			iSzIo;								// I/O size
	adtInt			iSzPkt;								// Packet size
	adtInt			iPipe;								// Pipe/endpoint Id
	adtInt			iAttr;								// Endpoint attributes
	adtBool			bDbg;									// Debug enabled ?

	/*
	adtInt		iPipe;									// Pipe Id
	adtBool		bAsync;									// Asynchronous reads ?
	#if 0 // #ifdef		_WIN32
	HANDLE		hevWr,hevRd;							// I/O events
	HANDLE		hevStop;									// Stop event for read thread
	OVERLAPPED	ovIo;										// I/O transfer
	#endif

	// For control transfers
	adtInt		iCtlType;								// Request type
	*/

	// CCL
	CCL_OBJECT_BEGIN(Endpoint)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)
	DECLARE_RCP(Endpoint)
	DECLARE_CON(Control)
	DECLARE_EMT(Error)
	DECLARE_CON(Read)
	DECLARE_RCP(Stream)
	DECLARE_RCP(Size)
	DECLARE_CON(Write)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)
		DEFINE_RCP(Endpoint)
		DEFINE_CON(Control)
		DEFINE_EMT(Error)
		DEFINE_CON(Read)
		DEFINE_RCP(Stream)
		DEFINE_RCP(Size)
		DEFINE_CON(Write)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	HRESULT	pktIo			( BOOL, DWORD, DWORD, DWORD * );
	void		update		( void );

	};

#endif
