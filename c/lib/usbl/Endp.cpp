////////////////////////////////////////////////////////////////////////
//
//									ENDP.CPP
//
//				Implementation of the USB endpoint node.
//
////////////////////////////////////////////////////////////////////////

#include "usbl_.h"
#include <stdio.h>

// Globals

Endpoint :: Endpoint ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctDev	= NULL;
	pDctEnd	= NULL;
	pStmIo	= NULL;
	pcBfr		= NULL;
	szBfr		= 0;
	iSzIo		= 0;
	iPipe		= -1;
	/*
	#if 0 // #ifdef		_WIN32
	hIntf		= INVALID_HANDLE_VALUE;
	hevWr		= NULL;
	hevRd		= NULL;
	hevStop	= NULL;
	#endif
	bAsync	= false;
	pThrd		= NULL;
	*/
	}	// Endpoint

HRESULT Endpoint :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"Id"), vL ) == S_OK)
//			iPipe = vL;
//		if (pnDesc->load ( adtString(L"AsyncRead"), vL ) == S_OK)
//			bAsync = vL;
		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			iSzIo = adtInt(vL);
		if (pnDesc->load ( adtString(L"Debug"), vL ) == S_OK)
			bDbg = vL;
//		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
//			iCtlType = adtInt(vL);
//		if (pnDesc->load ( adtString(L"Request"), vL ) == S_OK)
//			iCtlReq = adtInt(vL);
/*
		// I/O events
		#if 0 // #ifdef		_WIN32
		CCLTRYE ( (hevWr = CreateEvent ( NULL, FALSE, FALSE, NULL )) != NULL,
						GetLastError() );
		CCLTRYE ( (hevRd = CreateEvent ( NULL, FALSE, FALSE, NULL )) != NULL,
						GetLastError() );
		CCLTRYE ( (hevStop = CreateEvent ( NULL, TRUE, FALSE, NULL )) != NULL,
						GetLastError() );
		#endif
*/
		}	// if

	// Detach
	else
		{
		// Clean up
		#if 0 // #ifdef		_WIN32
		if (hevWr != NULL)
			{
			CloseHandle ( hevWr );
			hevWr = NULL;
			}	// if
		if (hevRd != NULL)
			{
			CloseHandle ( hevRd );
			hevRd = NULL;
			}	// if
		if (hevStop != NULL)
			{
			CloseHandle ( hevStop );
			hevStop = NULL;
			}	// if
		#endif
		szBfr = 0;
		_FREEMEM(pcBfr);
		_RELEASE(pDctEnd);
		_RELEASE(pDctDev);
		_RELEASE(pStmIo);
		}	// else

	return hr;
	}	// onAttach

HRESULT Endpoint :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Write
	if (_RCP(Write))
		{
		U32			uLeft	= 0;
		U64			uXferd;
		ULONG			uXfer;

		// State check
		CCLTRYE ( pDctDev != NULL,	ERROR_INVALID_STATE );
		CCLTRYE ( pStmIo != NULL,	ERROR_INVALID_STATE );

		// Size of transfer, size of 0 means entire stream
		if (hr == S_OK && iSzIo == 0)
			{
			U64	uAv = 0;
			CCLTRY ( pStmIo->available ( &uAv ) );
			CCLOK  ( uLeft = (U32) uAv; )
			}	// if
		else if (hr == S_OK)
			uLeft = iSzIo;

		// Write packet size data from stream
		while (hr == S_OK && uLeft > 0)
			{
			// Amount to write on next transaction
//			uXfer = (uLeft < (U32)iSzPkt) ? uLeft : (U32)iSzPkt;
			uXfer = uLeft;

			// Buffer size
			if (hr == S_OK && uXfer > szBfr)
				{
				_FREEMEM(pcBfr);
				szBfr = 0;
				CCLTRYE ( (pcBfr = (U8 *) _ALLOCMEM(uXfer+1)) != NULL, E_OUTOFMEMORY );
				CCLOK ( szBfr = uXfer+1; )
				}	// if

			// Read from source stream
			CCLTRY ( pStmIo->read ( pcBfr, uXfer, &uXferd ) );

			// DEBUG
			if (bDbg)
				{
				for (U32 i = 0;i < uXferd;++i)
					lprintf ( LOG_DBG, L"W%d) 0x%x\r\n", i, pcBfr[i] );
				}	// if

			// Write out endpoint
			CCLTRY ( pktIo ( TRUE, (DWORD)uXferd, 2000, &uXfer ) );
					
			// Next block
			CCLOK ( uLeft -= (U32)uXferd; )
			}	// while

		// Result
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"Write:hr 0x%x, I/O %d/%d\r\n", hr, uLeft, (U32)iSzIo );
		if (hr == S_OK)
			_EMT(Write,adtIUnknown(pStmIo));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Read
	else if (_RCP(Read))
		{
		U32			uLeft	= 0;
		ULONG			uXfer;

		// State check
		CCLTRYE ( pDctDev != NULL,	ERROR_INVALID_STATE );
		CCLTRYE ( pStmIo != NULL,	ERROR_INVALID_STATE );

		// The size of the transfer depends on if it is control transfer or not
		// and if an explicit size is specified
		uLeft = iSzIo;
		if (uLeft == 0 || iPipe == -1)
			uLeft = iSzPkt;

		// Buffer size

//	CCLTRYE ( (iSzPkt = vL) > 0, E_UNEXPECTED );
//	CCLTRYE ( (pcBfrPkt = (U8 *) _ALLOCMEM(iSzPkt+1)) != NULL, E_OUTOFMEMORY );

		// Read packet size data from endpoint
		while (hr == S_OK && uLeft > 0)
			{
			// Amount to read on next transaction
//			lprintf ( LOG_DBG, L"uLeft %d\r\n", uLeft );
//			uXfer = (uLeft < iSzPkt) ? uLeft : iSzPkt;
			uXfer = uLeft;
//			uXfer = (uLeft < (U32)iSzPkt) ? uLeft : (U32)iSzPkt;

			// Buffer size
			if (hr == S_OK && uXfer > szBfr)
				{
				_FREEMEM(pcBfr);
				szBfr = 0;
				CCLTRYE ( (pcBfr = (U8 *) _ALLOCMEM(uXfer+1)) != NULL, E_OUTOFMEMORY );
				CCLOK ( szBfr = uXfer+1; )
				}	// if

			// If a control transfer, request is in first byte
			if (hr == S_OK && iPipe == -1)
				hr = pStmIo->read ( pcBfr, 1, NULL );

			// Read from endpoint.
			CCLTRY ( pktIo ( FALSE, uXfer, 2000, &uXfer ) );

			// DEBUG
			if (bDbg)
				{
				for (U32 i = 0;i < uXfer;++i)
					lprintf ( LOG_DBG, L"R%d) 0x%x\r\n", i, (pcBfr != NULL) ? pcBfr[i] : 0 );
				}	// if

			// Debug
//			if (uXfer > 60)
//				lprintf ( LOG_DBG, L"Hi!\r\n" );

			// Write to destination stream
			CCLTRY ( pStmIo->write ( pcBfr, uXfer, NULL ) );

			// Next block
			if (uLeft >= uXfer)
				uLeft -= uXfer;
			else
				uLeft = 0;
//			CCLOK ( uLeft -= uXfer; )

			// A short packet or default endpoint means end of transfer
			if (hr == S_OK && (iPipe == 0 || uXfer < iSzPkt))
				break;
			}	// while

		// Result
//		if (hr != S_OK)
//			dbgprintf ( L"Endpoint::Read:hr 0x%x, I/O %d/%d\r\n", hr, uLeft, (U32)iSzIo );
		if (hr == S_OK)
			_EMT(Read,adtIUnknown(pStmIo));
		else
			_EMT(Error,adtInt(hr));
		}	// if
	else if (_RCP(Device))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctDev);
		_QISAFE(unkV,IID_IDictionary,&pDctDev);
		update();
		}	// else if
	else if (_RCP(Endpoint))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctEnd);
		_QISAFE(unkV,IID_IDictionary,&pDctEnd);
		update();
		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pStmIo);
		_QISAFE(unkV,IID_IByteStream,&pStmIo);
		}	// else if
	else if (_RCP(Size))
		iSzIo = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Endpoint :: pktIo  ( BOOL bWr, DWORD uIo, DWORD uTo, DWORD *puIo )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Unified I/O for packet buffer.
	//
	//	PARAMETERS
	//		-	bWr is TRUE to write, FALSE to read.
	//		-	uIo is the size of the requested transfer
	//		-	uTo is the timeout
	//		-	puIo will receive the amount transfered
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT								hr		= S_OK;
	struct libusb_device_handle	*pDev = NULL;
	int									ret	= 0;
	adtValue								vL;

	// State check
	CCLTRYE ( pDctDev != NULL, ERROR_INVALID_STATE );

	// Open device handle
	CCLTRY ( pDctDev->load ( adtString(L"Device"), vL ) );
	CCLTRYE ( (pDev = (struct libusb_device_handle *)(U64)adtLong(vL))
					!= NULL, ERROR_INVALID_STATE );

	// No pipe is assumed to mean control transfer
	if (hr == S_OK && iPipe == -1)
		{
		// Request type
		uint8_t bmRequestType = (bWr ? LIBUSB_ENDPOINT_OUT : LIBUSB_ENDPOINT_IN) 
							| LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE;

		// Amount to transfer
		uint16_t ntodo = (uint16_t)((uIo > (U32)iSzPkt) ? (U32)iSzPkt : uIo);

		// Transfer data, the request byte is the first byte in the buffer
		ret = libusb_control_transfer ( pDev, bmRequestType, pcBfr[0], 0, 0, 
													&pcBfr[1], ntodo, uTo );

		// Result
		if (ret < 0)
			{
			lprintf ( LOG_DBG, L"Control transfer failed %d\r\n", ret );
			hr = S_FALSE;
			}	// if
		else
			*puIo = ret;

		// Debug
		lprintf ( LOG_DBG, L"Control : %c : Request 0x%x : ret %d\r\n", (bWr) ? WCHAR('W') : WCHAR('R'),
						pcBfr[0], ret );
		for (int i = 0;i < ntodo;++i)
			lprintf ( LOG_DBG, L"%d ) %d : 0x%x : %c\r\n", i, pcBfr[i], pcBfr[i], pcBfr[i] );
		}	// if

	// Endpoint
	else if (hr == S_OK && iPipe >= 0)
		{
		int	nx = 0;

		// Bulk transfer
		if (iAttr & 0x02)
			{
			// Perform transfer
			ret = libusb_bulk_transfer ( pDev, iPipe, pcBfr, uIo, &nx, uTo );

			// Result
//			if (bDbg)
//				lprintf ( LOG_DBG, L"bulk %s iPipe 0x%x iAttr 0x%x ret %d nx %d\r\n", 
//								(iPipe & 0x80) ? L"In" : L"Out", (S32)iPipe, (S32)iAttr, ret, nx );
			if (ret != 0)
				{
				if (bDbg)
					lprintf ( LOG_DBG, L"Bulk transfer failed %d,%d,%d bytes\r\n", ret, errno, uIo );
				*puIo = 0;
				hr = S_FALSE;
				}	// if
			else 
				*puIo = nx;
			}	// if

		}	// else if

	// Debug
//	dbgprintf ( L"Endpoint::pktIo:bWr %d:uIo %d {\r\n", bWr, uIo );

	// I/O is always overlapped since that it the way USB devices are opened.
	#if 0 // #ifdef		_WIN32
	memset ( &ovIo, 0, sizeof(ovIo) );
	ovIo.hEvent = (bWr) ? hevWr : hevRd;
	if (hr == S_OK && bWr)
		{
		// If pipe Id is zero, assume control transfer
		if (hr == S_OK && iPipe == 0)
			{
			WINUSB_SETUP_PACKET	pkt;

			// Prepare packet information
			pkt.RequestType	= iCtlType;
			pkt.Request			= iCtlReq;
			pkt.Value			= 0;
			pkt.Index			= 0;
			pkt.Length			= (USHORT)uIo;

			// Begin transfer
			CCLTRYE ( WinUsb_ControlTransfer ( hIntf, pkt, 
							pcBfrPkt, uIo, NULL, &ovIo ) == TRUE, GetLastError() );
			}	// if

		// Endpoint
		else
			{
			// Begin a write
			CCLTRYE ( WinUsb_WritePipe (	hIntf, iPipe, pcBfrPkt, uIo,
													NULL, &ovIo ) == TRUE, GetLastError() );
			}	// else

		// Debug
		if (hr != S_OK && hr != ERROR_IO_PENDING)
			dbgprintf ( L"Endpoint::pktIo:hIntf %d:iPipe %d:bWr %d:hr 0x%x\r\n", 
							hIntf, (U32)iPipe, bWr, hr );
		}	// if
	else if (hr == S_OK)
		{
		// If pipe Id is zero, assume control transfer
		if (hr == S_OK && iPipe == 0)
			{
			WINUSB_SETUP_PACKET	pkt;

			// Prepare packet information
			pkt.RequestType	= iCtlType;
			pkt.Request			= iCtlReq;
			pkt.Value			= 0;
			pkt.Index			= 0;
			pkt.Length			= (USHORT)uIo;

			// Begin transfer
			CCLTRYE ( WinUsb_ControlTransfer ( hIntf, pkt, 
							pcBfrPkt, uIo, NULL, &ovIo ) == TRUE, GetLastError() );
			}	// if

		// Endpoint
		else
			{
			// Begin a read
			CCLTRYE ( WinUsb_ReadPipe (	hIntf, iPipe, pcBfrPkt, uIo,
													NULL, &ovIo ) == TRUE, GetLastError() );
			}	// else

		}	// else if
	#endif

	return hr;
	}	// pktIo
/*
HRESULT Endpoint :: pktIoWait  ( DWORD uTo, DWORD *puIo )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Wait for a previously initiated I/O to complete.
	//
	//	PARAMETERS
	//		-	uTo is the timeout in milliseconds
	//		-	puIo will receive the actual amount transferred.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	#if 0 // #ifdef		_WIN32
	HANDLE	hevIo[2];
	DWORD		dwRet;

	// Event handles for waiting
	hevIo[0] = ovIo.hEvent;
	hevIo[1] = hevStop;

	// Wait for completion or signal to stop
	dwRet = WaitForMultipleObjects ( 2, hevIo, FALSE, uTo );

	// Success ?
	if (dwRet != WAIT_OBJECT_0)
		{
		// Stop event detected
		if (dwRet == WAIT_OBJECT_0+1)
			hr = S_FALSE;

		// Timeout
		else if (dwRet == WAIT_TIMEOUT)
			hr = ERROR_TIMEOUT;

		// ??
		else
			hr = GetLastError();
		}	// if

	// Amount transfered
	CCLTRYE ( WinUsb_GetOverlappedResult ( hIntf, &ovIo, puIo, TRUE ) == TRUE,
					GetLastError() );
	#endif

	return hr;
	}	// pktIoWait
*/

void Endpoint :: update ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Update internal state of node to handle new configuration.
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtValue	vL;

	// Previous state
	iPipe  = -1;
//	_FREEMEM(pcBfrPkt);

	// Obtain the maximum packet size for the endpoint (control or other)
	if (pDctEnd != NULL)
		hr = pDctEnd->load ( adtString(L"MaxPacketSize"), vL );
	else if (pDctDev != NULL)
		hr = pDctDev->load ( adtString(L"MaxPacketSize"), vL );
	else
		hr = ERROR_INVALID_STATE;

	// Allocate enough space for a full packet, extra byte for control requests
	CCLTRYE ( (iSzPkt = vL) > 0, E_UNEXPECTED );
//	CCLTRYE ( (pcBfrPkt = (U8 *) _ALLOCMEM(iSzPkt+1)) != NULL, E_OUTOFMEMORY );

	// Endpoint address ?
	if (pDctEnd != NULL && pDctEnd->load ( adtString(L"Address"), vL ) == S_OK)
		iPipe = vL;
	if (pDctEnd != NULL && pDctEnd->load ( adtString(L"Attributes"), vL ) == S_OK)
		iAttr = vL;

	}	// update

