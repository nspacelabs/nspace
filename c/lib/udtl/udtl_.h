////////////////////////////////////////////////////////////////////////
//
//										UDTL_.H
//
//			Implementation include file for the UDT networking library
//
////////////////////////////////////////////////////////////////////////

#ifndef	UDTL__H
#define	UDTL__H

// Includes
#include	"udtl.h"

// Winsock
#if	defined(_WIN32)
#include <winsock2.h>
#include <ws2tcpip.h>
#include <IPHlpApi.h>
#endif

// Library
#include "../ext/udt4/api.h"
//#include <src/api.h>

#define	SIZE_PERSIST_CACHE		0x10000

//
// Class - Avail.  Avails for readability on sockets.
//

class Avail :
	public CCLObject,										// Base class
	public Behaviour,										// Interface
	public ITickable										// Interface
	{
	public :
	Avail ( void );										// Constructor

	// Run-time data
	IThread		*pThrd;									// Worker thread
	IDictionary	*pAvails;								// Avail dictionary
	IIt			*pInSkt;									// Socket iterator
	IDictionary	*pSkt;									// Active socket
	bool			bAvail;									// Running ?
	adtBool		bRead;									// Readability ?
	adtBool		bWrite;									// Writeability ?
	adtInt		iTo;										// Configured timeout
	UDTSOCKET	sktNotify;								// Notification socket

	// 'ITickable' members
	STDMETHOD(tick)		( void );
	STDMETHOD(tickAbort)	( void );
	STDMETHOD(tickBegin)	( void );
	STDMETHOD(tickEnd)	( void );

	// CCL
	CCL_OBJECT_BEGIN(Avail)
		CCL_INTF(IBehaviour)
		CCL_INTF(ITickable)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Add)
	DECLARE_RCP(Remove)
	DECLARE_RCP(Socket)
	DECLARE_RCP(Start)
	DECLARE_RCP(Stop)
	DECLARE_RCP(Timeout)
	DECLARE_EMT(Error)
	DECLARE_EMT(Read)
	DECLARE_EMT(Write)
	DECLARE_EMT(Exception)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Add)
		DEFINE_RCP(Remove)
		DEFINE_RCP(Socket)
		DEFINE_RCP(Start)
		DEFINE_RCP(Stop)
		DEFINE_RCP(Timeout)

		DEFINE_EMT(Error)
		DEFINE_EMT(Read)
		DEFINE_EMT(Write)
		DEFINE_EMT(Exception)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	void notify ( void );
	};

//
// Class - Client.  Socket client node.
//

class Client :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Client ( void );										// Constructor

	// Run-time data
	IDictionary		*pSkt;								// Socket context
	adtInt			iAddr;								// Address
	adtInt			iPort;								// Port
	adtInt			iTimeout;							// Timeout

	// CCL
	CCL_OBJECT_BEGIN(Client)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Accept)
	DECLARE_CON(Connect)
	DECLARE_CON(Connected)
	DECLARE_RCP(Address)
	DECLARE_RCP(Port)
	DECLARE_RCP(Socket)
	DECLARE_RCP(Timeout)
	DECLARE_EMT(Error)
	DECLARE_EMT(Pending)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Accept)
		DEFINE_CON(Connect)
		DEFINE_CON(Connected)

		DEFINE_RCP(Address)
		DEFINE_RCP(Port)
		DEFINE_RCP(Socket)
		DEFINE_RCP(Timeout)

		DEFINE_EMT(Error)
		DEFINE_EMT(Pending)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - PersistSktStm.  Stream portion of socket persist node.
//
class PersistSkt;
class PersistSktStm :
	public CCLObject,										// Base class
	public IByteStream									// Interface
	{
	public :
	PersistSktStm ( UDTSOCKET _skt );				// Constructor

	// Run-time data
	UDTSOCKET		skt;									// Active socket
	U8					bWr[SIZE_PERSIST_CACHE];		// Write cache
	S32				iWrIdx;								// Write cache position
	
	// 'IByteStream' members
	STDMETHOD(available)	( U64 * );
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * );
	STDMETHOD(flush)		( void );
	STDMETHOD(read)		( void *, U64, U64 * );
	STDMETHOD(seek)		( S64, U32, U64 * );
	STDMETHOD(setSize)	( U64 );
	STDMETHOD(write)		( void const *, U64, U64 * );

	// CCL
	CCL_OBJECT_BEGIN_INT(PersistSktStm)
		CCL_INTF(IByteStream)
	CCL_OBJECT_END()
	};

//
// Class - PersistSkt.  Node to save/load values to/from sockets.
//

class PersistSkt :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	PersistSkt ( void );									// Constructor

	// Run-time data
	adtValue			vSave;								// Value to save
	IStreamPersist	*pPrs;								// Persistence parser
	IDictionary		*pSkt;								// Socket context

	// CCL
	CCL_OBJECT_BEGIN(PersistSkt)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()
	virtual void		destruct	( void );			// Destruct object

	// Connections
	DECLARE_CON(Load)
	DECLARE_CON(Save)
	DECLARE_RCP(Parser)
	DECLARE_RCP(Socket)
	DECLARE_RCP(Value)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Load)
		DEFINE_CON(Save)
		DEFINE_RCP(Parser)
		DEFINE_RCP(Socket)
		DEFINE_RCP(Value)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Recv.  Receive stream node.
//

class Recv :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Recv ( void );											// Constructor

	// Run-time data
	IDictionary		*pSkt;								// Socket context
	IByteStream		*pStm;								// Stream
	U8					*pcFrame;							// Internal buffer
	adtInt			iTo;									// Timeout
	
	// CCL
	CCL_OBJECT_BEGIN(Recv)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Fire)
	DECLARE_RCP(Socket)
	DECLARE_RCP(Stream)
	DECLARE_RCP(Timeout)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Fire)
		DEFINE_RCP(Socket)
		DEFINE_RCP(Stream)
		DEFINE_RCP(Timeout)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - SocketOp.  Socket context node.
//

class SocketOp :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	SocketOp ( void );									// Constructor

	// Run-time data
	IDictionary		*pSkt;								// Socket context
	adtInt			iAddr;								// Address
	adtInt			iPort;								// Port
	adtInt			iBfrSz;								// Buffer sizes

	// Utilities
	static HRESULT sktStm ( IDictionary *, const WCHAR *, IByteStream ** );

	// CCL
	CCL_OBJECT_BEGIN(SocketOp)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Address)
	DECLARE_CON(Close)
	DECLARE_CON(Open)
	DECLARE_CON(Port)
	DECLARE_CON(Stream)
	DECLARE_RCP(Query)
	DECLARE_RCP(Size)
	DECLARE_RCP(Socket)
	DECLARE_EMT(Error)
	DECLARE_EMT(PeerAddress)
	DECLARE_EMT(PeerPort)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Address)
		DEFINE_CON(Close)
		DEFINE_CON(Open)
		DEFINE_CON(Port)
		DEFINE_CON(Stream)

		DEFINE_RCP(Query)
		DEFINE_RCP(Size)
		DEFINE_RCP(Socket)

		DEFINE_EMT(Error)
		DEFINE_EMT(PeerAddress)
		DEFINE_EMT(PeerPort)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

// Protoypes
HRESULT	Udt_AddRef	( void );
HRESULT	Udt_Block	( UDTSOCKET, bool );
HRESULT	Udt_Error	( const WCHAR * );
HRESULT	Udt_Receive ( UDTSOCKET, void *, U32, U32 *, U32 );
HRESULT	Udt_Release	( void );
HRESULT	Udt_Send		( UDTSOCKET, void const *, U32, U32 *, U32 );

#endif
