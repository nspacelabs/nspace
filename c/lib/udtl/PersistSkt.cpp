////////////////////////////////////////////////////////////////////////
//
//									Persis.CPP
//
//					Implementation of the socket persistence node
//
////////////////////////////////////////////////////////////////////////

#include "udtl_.h"
#include <stdio.h>

// Definitions

PersistSkt :: PersistSkt ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pSkt		= NULL;
	pPrs		= NULL;
	}	// PersistSkt

void PersistSkt :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pSkt);
	_RELEASE(pPrs);
	}	// destruct

HRESULT PersistSkt :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (!bAttach)
		{
		_RELEASE(pSkt);
		_RELEASE(pPrs);
		adtValue::clear(vSave);
		}	// if

	return S_OK;
	}	// onAttach

HRESULT PersistSkt :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Save
	if (_RCP(Save))
		{
		IByteStream		*pStmPer	= NULL;
		UDTSOCKET		skt		= UDT::INVALID_SOCK;
		adtValue			vSkt;

		// Value to use
		const ADTVALUE	*pUseV	= (!adtValue::empty(vSave)) ? &vSave : &v;

		// State check
		CCLTRYE	( pPrs != NULL, ERROR_INVALID_STATE );
		CCLTRYE	( pSkt != NULL, ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (UDTSOCKET) (U64) adtLong(vSkt); )
		CCLTRYE	( !adtValue::empty(*pUseV), ERROR_INVALID_STATE );

		// Persistence stream object
		CCLTRYE ( (pStmPer = new PersistSktStm ( skt )) != NULL, E_OUTOFMEMORY );
		_ADDREF(pStmPer);

		// Save value to stream
		CCLTRY ( pPrs->save ( pStmPer, *pUseV ) );

		// Flush any cached data
		CCLOK ( pStmPer->flush(); )

		// Result
		if (hr == S_OK)
			_EMT(Save,adtIUnknown(*pUseV) );
		else
			{
			lprintf ( LOG_DBG, L"Save failed:hr 0x%x\r\n", hr );
			_EMT(Error,adtInt(hr) );
			}	// else

		// Clean up
		_RELEASE(pStmPer);
		}	// if

	// Load
	else if (_RCP(Load))
		{
		IByteStream	*pStmPer	= NULL;
		UDTSOCKET	skt		= UDT::INVALID_SOCK;
		adtValue		vSkt;
		adtValue		vL;

		// State check
		CCLTRYE	( pPrs != NULL, ERROR_INVALID_STATE );
		CCLTRYE	( pSkt != NULL, ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (UDTSOCKET) (U64) adtLong(vSkt); )

		// Persistence stream object
		CCLTRYE ( (pStmPer = new PersistSktStm ( skt )) != NULL, E_OUTOFMEMORY );
		_ADDREF(pStmPer);

		// Load value from stream
		CCLTRY ( pPrs->load ( pStmPer, vL ) );

		// Result
		if (hr == S_OK)
			_EMT(Load,vL);
		else
			{
			lprintf ( LOG_DBG, L"Load failed:hr 0x%x\r\n", hr );
			_EMT(Error,adtInt(hr) );
			}	// else

		// Clean up
		_RELEASE(pStmPer);

//		dbgprintf ( L"} PersistSkt::Load:hr 0x%x\r\n", hr );
		}	// else if

	// State
	else if (_RCP(Socket))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSkt);
		hr = _QISAFE(unkV,IID_IDictionary,&pSkt);
		}	// else if
	else if (_RCP(Parser))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pPrs);
		hr = _QISAFE(unkV,IID_IStreamPersist,&pPrs);
		}	// else if
	else if (_RCP(Value))
		hr = adtValue::copy ( v, vSave );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

/////////////////
// PersistStkStm
/////////////////

PersistSktStm :: PersistSktStm ( UDTSOCKET _skt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	skt is the socket upon which to perform operations
	//
	////////////////////////////////////////////////////////////////////////
	skt		= _skt;
	iWrIdx	= 0;
	}	// PersistSktStm

HRESULT PersistSktStm :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr					= S_OK;
	struct		timeval	to		= { 0, 0 };
	int			ret				= 0;
	UDT::UDSET	rfds;

	// Initialize descriptor
	CCLOK ( UD_ZERO ( &rfds ); )
	CCLOK ( UD_SET ( skt, &rfds ); )

	// Socket readable ?
	CCLTRYE	( (ret = UDT::select ( (int)skt+1, &rfds, NULL, NULL, &to )) 
					!= UDT::ERROR, Udt_Error ( L"PersistSktStm::available::select" ) );
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( UD_ISSET ( skt, &rfds ), ERROR_TIMEOUT );

	// If socket is readbale, assume at least one byte is available.
	(*puAv) = (hr == S_OK) ? 1 : 0;

	return hr;
	}	// available

HRESULT PersistSktStm :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
	U8			cpybufr[4096];
	U64		nio,nw,nr;

	// State check
	CCLTRYE ( skt != UDT::INVALID_SOCK, ERROR_INVALID_STATE );
	CCLTRYE ( uSz != 0, ERROR_INVALID_STATE );
 
	// Setup
	if (puSz != NULL)
		*puSz	= 0;

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK ( nio = (sizeof(cpybufr) < uSz) ? sizeof(cpybufr) : uSz; )
		CCLTRY( read ( cpybufr, nio, &nr ) );

		// Any bytes available ?
		if (hr == S_OK && nr == 0)
			{
			lprintf ( LOG_DBG, L"Successful read of 0 bytes uSz %ld nio %d\r\n", uSz, nio );
			break;
			}	// if

		// Write full block to stream
		CCLOK ( fp		= cpybufr; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;

		// If at end of file before request has been satisfied, stop
//		if (uSz && (nr < nio))
//			{
//			lprintf ( LOG_DBG, L"Breaking uSz %ld nr %d nio %d\r\n", uSz, nr, nio );
//			break;
//			}	// if
		}	// while

	// Debug
//	if (hr != S_OK)
//		dbgprintf ( L"PersistSktStm::copyTo:Failed:0x%x\r\n", hr );

	return hr;
	}	// copyTo

HRESULT PersistSktStm :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U32			nleft,nw;

	// Flush will send out anything in the write cache
	if (iWrIdx > 0)
		{
		// Debug
//		adtDate then;
//		then.now();
//		lprintf ( LOG_DBG, L"skt 0x%x iWrIdx %d \r\n", skt, iWrIdx );

		// Write all the data
		CCLOK ( nleft 	= iWrIdx; )
		CCLOK ( iWrIdx = 0; )
		while (hr == S_OK && nleft > 0)
			{
			// Next write
			CCLTRY ( Udt_Send ( skt, &bWr[iWrIdx], nleft, &nw, 10000 ) );
//			if (nw > 1024)
//				lprintf ( LOG_DBG, L"skt 0x%x nleft %d nw %d hr 0x%x\r\n", skt, nleft, nw, hr );

			// Result
			CCLOK ( nleft	-= nw; )
			CCLOK ( iWrIdx	+= nw; )
			}	// while

		// Debug
//		adtDate now;
//		now.now();
//		lprintf ( LOG_DBG, L"skt 0x%x iWrIdx %d hr 0x%x time %g\r\n", 
//									skt, iWrIdx, hr, ((now-then)*SECINDAY) );

		// Cache empty
		iWrIdx = 0;
		}	// if

	return hr;
	}	// flush

HRESULT PersistSktStm :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	U32		nr;

	// State check
	CCLTRYE ( (skt != UDT::INVALID_SOCK), ERROR_INVALID_STATE );

	// Read next block of data
	CCLTRY ( Udt_Receive ( skt, pvBfr, (U32)nio, &nr, 5000 ) );

	// Results
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// read

HRESULT PersistSktStm :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Not supported on a socket
	return E_NOTIMPL;
	}	// seek

HRESULT PersistSktStm :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// A socket can be thought of has having any amount of data
	return S_OK;
	}	// setSize

HRESULT PersistSktStm :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	const char	*pc	= (const char *)pcvBfr;
	U32			ntot,nw;

	// State check
	CCLTRYE ( (skt != UDT::INVALID_SOCK), ERROR_INVALID_STATE );

	// Fill cache before sending to minimize calls
	CCLOK ( ntot	= (U32)nio; )
	CCLOK ( nio		= 0; )
	while (hr == S_OK && ntot > 0)
		{
		// Amount to copy into cache
		nw = (ntot < (U32)(SIZE_PERSIST_CACHE-iWrIdx)) ? ntot :
				(U32)(SIZE_PERSIST_CACHE-iWrIdx);
//		lprintf ( LOG_DBG, L"Cache write : %d / %d / %d bytes\r\n", nw, ntot, nio );

		// Copy into cache
		if (nw > 0)
			{
			// Write to cache
			memcpy ( &bWr[iWrIdx], pc, nw );

			// Next block
			iWrIdx	+= nw;
			pc 		+= nw;
			nio		+= nw;
			ntot	 	-= nw;
			}	// if

		// Is cache full ? 
		if (iWrIdx >= SIZE_PERSIST_CACHE)
			flush();
		}	// while

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nio;

	// Debug
//	if (hr != S_OK)
//		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// write
