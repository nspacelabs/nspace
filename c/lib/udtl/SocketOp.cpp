////////////////////////////////////////////////////////////////////////
//
//									SOCKET.CPP
//
//					Implementation of the socket node
//
////////////////////////////////////////////////////////////////////////

#include "udtl_.h"

SocketOp :: SocketOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pSkt		= NULL;
	iAddr		= INADDR_ANY;
	iPort		= 0;
	}	// Socket

HRESULT SocketOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		Udt_AddRef();

	// Detach
	else
		{
		_RELEASE(pSkt);
		Udt_Release();
		}	// else

	return S_OK;
	}	// onAttach

HRESULT SocketOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		UDTSOCKET	skt			= UDT::INVALID_SOCK;
		BOOL			bListen		= FALSE;
		BOOL			bDatagram	= FALSE;
		adtValue		vSkt,vL;
		struct 
		sockaddr_in	sockaddr;

		// State check
		CCLTRYE	( (pSkt != NULL), ERROR_INVALID_STATE );
		CCLTRYE	( pSkt->load ( adtString(L"Socket"), vSkt ) != S_OK, ERROR_INVALID_STATE );

		// Socket options
		if (hr == S_OK)
			{
			// Datagram / stream
			if (	pSkt->load ( adtString ( L"Datagram" ), vL ) == S_OK &&
					vL.vtype == VTYPE_BOOL && vL.vbool == TRUE )
				bDatagram = TRUE;

			// Bind port
			if ( pSkt->load ( adtString ( L"Port" ), vL ) == S_OK )
				adtValue::copy ( iPort, vL );

			// Bind address
			if (	pSkt->load ( adtString ( L"Address" ), vL ) == S_OK )
				NetSkt_Resolve ( vL, iAddr, iPort );

			// Listen (server)
			if (	pSkt->load ( adtString ( L"Listen" ), vL ) == S_OK &&
					vL.vtype == VTYPE_BOOL && vL.vbool == TRUE )
				bListen = TRUE;

			}	// if
 
		// Create a socket of the specified type
		CCLTRYE ( (skt = UDT::socket ( AF_INET, 
						(bDatagram) ? SOCK_DGRAM : SOCK_STREAM, 0 ))
						!= UDT::INVALID_SOCK, Udt_Error ( L"SocketOp::open::socket" ) );

		// Allow re-use/multiple servers on same port. (multicast support)
		// When processes crash (like on Linux) it keeps ports as 'used' 
		// for a while.
		if (hr == S_OK)
			{
			int	reuse = 1;
			CCLTRYE(UDT::setsockopt(skt, 0, UDT_REUSEADDR, (char*)&reuse,
				sizeof(reuse)) != -1, Udt_Error(L"SocketOp::open::setsockopt::UDT_REUSEADDR"));
			}	// if

		// Send/receive buffer
		if (hr == S_OK && iBfrSz > 0)
			{
			int			bfr = iBfrSz;

			// Send/receive buffer
			CCLTRYE	( UDT::setsockopt ( skt, 0, UDT::SOCKOPT::UDT_RCVBUF, (char *) &bfr,
							sizeof(bfr) ) != -1, Udt_Error ( L"SocketOp::open::setsockopt::UDT_RCVBUF" ) );
			CCLTRYE	( UDT::setsockopt ( skt, 0, UDT::SOCKOPT::UDT_SNDBUF, (char *) &bfr,
							sizeof(bfr) ) != -1, Udt_Error ( L"SocketOp::open::setsockopt::UDT_SNDBUF" ) );
			}	// if

		// Always a non-blocking socket
		CCLTRY ( Udt_Block ( skt, FALSE ) );

		// Bind to optionally specified address / port
		if (hr == S_OK)
			{
			memset ( &sockaddr, 0, sizeof(sockaddr) );
			sockaddr.sin_family			= AF_INET;
			sockaddr.sin_port				= htons ( iPort.vint );
			sockaddr.sin_addr.s_addr	= (iAddr != 0) ? htonl ( iAddr.vint ) : INADDR_ANY;
			memset ( &(sockaddr.sin_zero), 0, 8 );
			hr = ((UDT::bind ( skt, (struct sockaddr *) &sockaddr,
					sizeof(sockaddr) ) != UDT::ERROR) ? S_OK : 
					Udt_Error ( L"SocketOp::open::bind" ));
			}	// if

		// Listen ?
		if (hr == S_OK && bListen)
			{
			CCLTRYE ( (UDT::listen ( skt, SOMAXCONN ) == 0), 
							Udt_Error ( L"SocketOp::open::listen" ) );
			}	// if

		// Store socket in context
		if (hr == S_OK)
			hr = pSkt->store ( adtString(L"Socket"), adtLong(skt) );
		else if (skt != UDT::INVALID_SOCK)
			UDT::close(skt);

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pSkt) );
		else
			_EMT(Error,adtInt(hr) );
		}	// if

	// Close
	else if (_RCP(Close))
		{
		UDTSOCKET	skt = UDT::INVALID_SOCK;
		adtValue		vSkt;
		adtLong		lSkt;

		// State check
		CCLTRYE	( (pSkt != NULL), ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (UDTSOCKET) (lSkt = vSkt); )

		// Socket closing
		CCLOK ( _EMT(Close,adtIUnknown(pSkt) ); )

		// Socket
		if (hr == S_OK && skt != UDT::INVALID_SOCK)
			UDT::close ( skt );

		// Clean up
		if (pSkt != NULL)
			pSkt->remove ( adtString(L"Socket") );
		}	// else if

	// Query
	else if (_RCP(Query))
		{
		adtValue					vSkt;
		adtLong					lSkt;
		UDTSOCKET				skt = UDT::INVALID_SOCK;
		struct sockaddr_in	sockaddr;
		int						alen;

		// State check
		CCLTRYE	( (pSkt != NULL), ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (UDTSOCKET) (lSkt = vSkt); )

		// Obtain/emit socket information
		CCLOK		( alen = sizeof(sockaddr); )
		CCLTRYE	( UDT::getsockname ( skt, (struct sockaddr *) &sockaddr, &alen )
						!= UDT::ERROR, Udt_Error ( L"SocketOp::query::getsockname" ) );

		// Result
//		CCLOK ( lprintf ( LOG_DBG, L"Query::Address 0x%x:%d\r\n", 
//					ntohl ( sockaddr.sin_addr.s_addr ), ntohs ( sockaddr.sin_port ) ); )
		CCLOK	( _EMT(Address,adtInt ( ntohl ( sockaddr.sin_addr.s_addr ) ) ); )
		CCLOK	( _EMT(Port,adtInt ( ntohs ( sockaddr.sin_port ) ) ); )

		// Peer information
		CCLOK		( alen = sizeof(sockaddr); )
		if (hr == S_OK && UDT::getpeername ( skt, 
				(struct sockaddr *) &sockaddr, &alen ) != UDT::ERROR )
			{
			// Result
//			lprintf ( LOG_DBG, L"Query:Peer 0x%x:%d\r\n", 
//					ntohl ( sockaddr.sin_addr.s_addr ), ntohs ( sockaddr.sin_port ) );
			CCLOK	( _EMT(PeerAddress,adtInt ( ntohl ( sockaddr.sin_addr.s_addr ) ) ); )
			CCLOK	( _EMT(PeerPort,adtInt ( ntohs ( sockaddr.sin_port ) ) ); )
			}	// if

		}	// else if

	// Socket buffer size
	else if (_RCP(Size))
		{
		// Cache default buffer size
		iBfrSz = v;

		// Apply to current socket if present
		if (pSkt != NULL && iBfrSz > 0)
			{
			UDTSOCKET	skt = UDT::INVALID_SOCK;
			int			bfr = iBfrSz;
			adtValue		vSkt;
			adtLong		lSkt;

			// State check
			CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
			CCLOK		( skt = (UDTSOCKET) (lSkt = vSkt); )

			// Send/receive buffer
			CCLTRYE	( UDT::setsockopt ( skt, 0, UDT::SOCKOPT::UDT_RCVBUF, (char *) &bfr,
							sizeof(bfr) ) != -1, Udt_Error ( L"SocketOp::size::setsockopt::UDT_RCVBUF" ) );
			CCLTRYE	( UDT::setsockopt ( skt, 0, UDT::SOCKOPT::UDT_SNDBUF, (char *) &bfr,
							sizeof(bfr) ) != -1, Udt_Error ( L"SocketOp::size::setsockopt::UDT_SNDBUF" ) );
			}	// if

		}	// else if

	// Attach byte stream to socket
	else if (_RCP(Stream))
		{
		UDTSOCKET		skt		= UDT::INVALID_SOCK;
		IByteStream		*pStm		= NULL;

		// Ensure stream exists for socket
		CCLTRY ( sktStm ( pSkt, strnName, &pStm ) );

		// Result
		if (hr == S_OK)
			_EMT(Stream,adtIUnknown(pStm));
		else 
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pStm);
		}	// else if

	// State
	else if (_RCP(Socket))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSkt);
		hr = _QISAFE(unkV,IID_IDictionary,&pSkt);
		}	// else if
	else if (_RCP(Address))
		hr = NetSkt_Resolve ( v, iAddr, iPort );
	else if (_RCP(Port))
		iPort = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT SocketOp :: sktStm ( 	IDictionary *pSkt, const WCHAR *pwName,
										IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve the persistence socket stream for the socket 
	//			dictionary.
	//
	//	PARAMETERS
	//		-	pSkt is the socket dictionary
	//		-	pwName is the name of the requestor (for debug)
	//		-	ppStm will receive the stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	UDTSOCKET	skt	= UDT::INVALID_SOCK;
	adtIUnknown	unkV;
	adtValue		vL;

	// State check
	CCLTRYE	( pSkt != NULL, ERROR_INVALID_STATE );
	CCLTRY	( pSkt->load ( adtString(L"Socket"), vL ) );
	CCLOK		( skt = (UDTSOCKET) (U64) adtLong(vL); )

	// Does socket have a persistence stream yet ?
	if (hr == S_OK)
		{
		// Need a new stream
		if (pSkt->load ( adtString(L"Stream"), vL ) != S_OK)
			{
//			lprintf ( LOG_DBG, L"New stream for socket %d\r\n", skt );

			// Persistence stream object
			CCLTRYE	( (*ppStm = new PersistSktStm ( skt )) != NULL, E_OUTOFMEMORY );
			(*ppStm)->AddRef();

			// Store in dictionary
			CCLTRY ( pSkt->store ( adtString(L"Stream"), adtIUnknown(*ppStm) ) );
			}	// if

		// Existing stream
		else
			{
			CCLTRY ( _QISAFE((unkV=vL),IID_IByteStream,ppStm) );
			}	// else
		}	// if

	return hr;
	}	// sktStm
