////////////////////////////////////////////////////////////////////////
//
//									UDTL.CPP
//
//						General UDT utilities
//
////////////////////////////////////////////////////////////////////////

#include "udtl_.h"

// Globals
static	U32		UdtRefCnt	= 0;
#define	TIMING
//#undef	TIMING

HRESULT Udt_AddRef ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Increments socket use for this DLL.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Already initialized ?
	if (++UdtRefCnt > 1) return S_OK;

	return hr;
	}	// Udt_AddRef

HRESULT Udt_Block ( UDTSOCKET skt, bool bBlk )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Change the non-blocking state of a socket.
	//
	//	PARAMETERS
	//		-	skt is the socket to transfer
	//		-	bBlk is true to set to blocking socket, false for non-blocking.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	int		val	= 0;

	// Set non-blocking I/O (false)
	CCLTRYE	( UDT::setsockopt ( skt, 0, UDT::SOCKOPT::UDT_SNDSYN, (char *) &val,
					sizeof(val) ) != -1, Udt_Error ( L"Udt_Block::UDT_SNDSYN" ) );
	CCLTRYE	( UDT::setsockopt ( skt, 0, UDT::SOCKOPT::UDT_RCVSYN, (char *) &val,
					sizeof(val) ) != -1, Udt_Error ( L"Udt_Block::UDT_RCVSYN" ) );

	return hr;
	}	// Udt_Block

HRESULT Udt_Error ( const WCHAR *pwCtx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Process the last error in the UDT stack
	//
	//	PARMAETERS
	//		-	pwCtx is the context string
	//
	//	RETURN VALUE
	//		S_OK if successful, otherwise error code
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr;
	UDT::ERRORINFO	err;

	// Last error information
	err = UDT::getlasterror();

	// Error ?
	hr = (err.getErrorCode() == UDT::ERRORINFO::SUCCESS) ? S_OK : 
				err.getErrorCode();

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"%s : %S (0x%x)\r\n",
						(pwCtx != NULL) ? pwCtx : L"Error", 
						err.getErrorMessage(), hr );

	return hr;
	}	// Udt_Error

HRESULT Udt_Receive ( UDTSOCKET skt, void *pvBfr, U32 nio, U32 *pnio, U32 toms )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the socket.
	//
	//	PARAMETERS
	//		-	skt is the socket to transfer
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//		-	toms is the timeout in milliseconds for the read
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	struct timeval	to		= { 0, 0 };
	int				len	= 0;
	int				ret	= 0;
	UDT::UDSET		rfds,efds;

	// Setup
	CCLTRYE	( (skt != UDT::INVALID_SOCK), E_UNEXPECTED );

	// Debug
	#ifdef	TIMING
	adtDate dThen,dNow;
	dThen.now();
	#endif

	// Initialize descriptors
	CCLOK ( UD_ZERO ( &rfds ); )
	CCLOK ( UD_SET ( skt, &rfds ); )
	CCLOK ( UD_ZERO ( &efds ); )
	CCLOK ( UD_SET ( skt, &efds ); )

	// Timeout
	CCLOK ( to.tv_sec		= (toms/1000); )
	CCLOK ( to.tv_usec	= (toms % 1000) * 1000; )

	// Wait for readability.  This makes sure the recv does not hang forever.
//	lprintf ( LOG_DBG, L"select 0x%x\r\n", skt );
	CCLTRYE	( (ret = UDT::select ( (int)skt+1, &rfds, NULL, &efds, &to )) 
						!= UDT::ERROR, Udt_Error ( L"Udt_Receive::select" ) );
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( UD_ISSET ( skt, &rfds ), ERROR_TIMEOUT );

	// Debug
	#ifdef	TIMING
	dNow.now ();
	double dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow select %g s\r\n", dt );
	#endif

	// Read data
//	lprintf ( LOG_DBG, L"hr 0x%x ret %d nio %d\r\n", hr, ret, nio );
	CCLTRYE	( (len = UDT::recv ( skt, (char *) pvBfr, nio, 0 )) >= 0, 
					Udt_Error ( L"Udt_Receive::recv" ) );
//	lprintf ( LOG_DBG, L"hr 0x%x len %d\r\n",  hr, len );

	// Debug
	#ifdef	TIMING
	dNow.now ();
	dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow read %g s\r\n", dt );
	#endif

	// A readable socket that returns 0 bytes means socket has been closed gracefully
	if (hr == S_OK && len == 0)
		hr = WSAECONNRESET;

	// Debug
//	dbgprintf ( L"CommSktLoad::read:pvBfr %p nio %d len %d\r\n", pvBfr, nio, len );

	// Result
	if (pnio != NULL)		*pnio = len;

	// Debug
//	dbgprintf ( L"Net_Receive:skt 0x%x hr 0x%x len %d nio %d\n", skt, hr, len, nio );
//	#if	defined(_DEBUG) || defined(DEBUG)
	if (hr != S_OK)
		{
		lprintf ( LOG_DBG, L"Udt_Receive:skt 0x%x hr 0x%x len %d nio %d\n", skt, hr, len, nio );
		}	// if
//	#endif

	// Debug
//	CommSkt_Log ( skt, L"Net_Receive", pvBfr, nio, len, hr );

	return hr;
	}	// Udt_Receive

HRESULT Udt_Release ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decrements socket use for this DLL.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Last one ?
	if (--UdtRefCnt > 0)
		return S_OK;

	return S_OK;
	}	// Udt_Release

HRESULT Udt_Send ( UDTSOCKET skt, void const *pcvBfr, U32 nio, 
							U32 *pnio, U32 toms )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	skt is the socket
	//		-	pcvBfr contains the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//		-	toms is the timeout in milliseconds for the write
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	struct timeval	to		= { 0, 0 };
	int				len	= 0;
	int				ret	= 0;
	UDT::UDSET		wfds,efds;

	// Setup
	CCLTRYE	( (skt != UDT::INVALID_SOCK), E_UNEXPECTED );
	if (pnio != NULL) *pnio = 0;

	// Debug
	#ifdef	TIMING
	adtDate dThen,dNow;
	dThen.now();
	#endif

	// Timeout
	CCLOK ( to.tv_sec		= (toms/1000); )
	CCLOK ( to.tv_usec	= (toms % 1000) * 1000; )

	// Initialize descriptors
	CCLOK ( UD_ZERO ( &wfds ); )
	CCLOK ( UD_SET ( skt, &wfds ); )
	CCLOK ( UD_ZERO ( &efds ); )
	CCLOK ( UD_SET ( skt, &efds ); )

	// Wait for writeability.  This is how the timeout is used.
	if (hr == S_OK)
		{
		CCLTRYE	( (ret = UDT::select ( (int)skt+1, NULL, &wfds, &efds, &to )) != UDT::ERROR, 
							Udt_Error ( L"Udt_Send::select" ) );

		// If error on select, socket is bad
		if (hr != S_OK) skt = UDT::INVALID_SOCK;
		}	// if
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( UD_ISSET ( skt, &wfds ), ERROR_TIMEOUT );

	// Debug
	#ifdef	TIMING
	dNow.now ();
	double dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow select %g s\r\n", dt );
	#endif

	// Write data
	if (hr == S_OK)
		{
		// Attempt write
		CCLTRYE	( (len = UDT::send ( skt, (const char *) pcvBfr, (int)nio, 0 )) >= 0, 
						Udt_Error ( L"Udt_Send::send" ) );
		}	// if

	// Result
	if (pnio != NULL && hr == S_OK)	*pnio = len;

	// Debug
	#ifdef	TIMING
	dNow.now ();
	dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow read %g s\r\n", dt );
	#endif

	// If the write gets a timeout, do best to detect a reset connection to avoid continual retry
	if (hr == ERROR_TIMEOUT)
		{
		UDT::UDSET	rfds;

		// Initialize descriptors
		UD_ZERO ( &rfds );
		UD_SET ( skt, &rfds );
		memset ( &to, 0, sizeof(to) );

		// See if data is available on socket, a 'reset' connection will have data waiting
		ret = UDT::select ( (int)skt+1, &rfds, NULL, NULL, &to );

		// Socket marked ?
		if (ret > 0 && UD_ISSET ( skt, &rfds ))
			{
			char	byte;

			// Peek at incoming data just to see if 'recv' returns an error
			ret = UDT::recv ( skt, &byte, 1, MSG_PEEK );

			// If connection is bad, read returns immediately with 0 bytes read
			if (ret == 0)
				{
				// Close socket ourselves, this will cause future access to error out
				UDT::close ( skt );
				hr = E_UNEXPECTED;
				}	// if

			}	// if
		}	// if

	// Debug
//	dbgprintf ( L"NetSkt_Send:hr 0x%x skt 0x%x len %d nio %d\n", hr, skt, len, nio );
//	#if	defined(_DEBUG) || defined(DEBUG)
	if (hr != S_OK && skt != UDT::INVALID_SOCK)
		{
		lprintf ( LOG_DBG, L"Udt_Send:hr 0x%x skt 0x%x len %d nio %d\n", hr, skt, len, nio );
		}	// if
//	#endif

	// Debug
//	Net_Log ( skt, L"NetSkt_Send", pcvBfr, nio, len, hr );
//	dNow.now();
//	lprintf ( LOG_DBG, L"hr 0x%x pnio %d Time %g\r\n", 
//					hr, (pnio != NULL) ? *pnio : 0, (dNow-dThen)*SECINDAY );

	return hr;
	}	// Udt_Send
