////////////////////////////////////////////////////////////////////////
//
//									SwapChain.CPP
//
//				Implementation of the DXGI SwapChain node.
//
////////////////////////////////////////////////////////////////////////

#include "dxgil_.h"
#include <stdio.h>

// Globals

SwapChain :: SwapChain ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	pWnd	= NULL;
	pDev	= NULL;
	pSwp	= NULL;
	}	// SwapChain

HRESULT SwapChain :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		_RELEASE(pSwp);
		_RELEASE(pWnd);
		_RELEASE(pDct);
		_RELEASE(pDev);
		}	// else

	return hr;
	}	// onAttach

HRESULT SwapChain :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open/Create
	if (_RCP(Open))
		{
		IDXGISwapChain	*pSwpOp	= NULL;
		IUnknown			*punkDev = NULL;
		IDXGIFactory	*pFact	= NULL;
		adtIUnknown		unkV;
		adtValue			vL;

		// State check
		CCLTRYE ( pDct != NULL && pDev != NULL, ERROR_INVALID_STATE );

		// Create needed factory
		CCLTRY ( CreateDXGIFactory ( IID_IDXGIFactory, (void **) &pFact ) );

		// Extract device interface
		CCLTRY ( pDev->load ( adtString(L"Device"), vL ) );
		CCLTRYE( (IUnknown *)(NULL) != (unkV=vL), ERROR_INVALID_STATE );
		if (hr == S_OK)
			{
			punkDev = unkV;
			_ADDREF(punkDev);
			}	// if

		// Create swap chain
		if (hr == S_OK)
			{
			DXGI_SWAP_CHAIN_DESC	desc;
			adtString				strV;

			// Descriptor
			memset ( &desc, 0, sizeof(desc) );
			desc.BufferCount			= 1;
			desc.BufferUsage			= (DXGI_USAGE_SHADER_INPUT | DXGI_USAGE_RENDER_TARGET_OUTPUT);
			desc.SampleDesc.Count	= 1;
			desc.SampleDesc.Quality	= 0;
			desc.Windowed				= TRUE;			// TODO: Make property

			//
			// Buffer descriptor
			//

			// Format, add more as necessary
			CCLTRY ( pDct->load ( adtString(L"Format"), vL ) );
			CCLTRYE( (strV = vL).length() > 0, E_INVALIDARG );
			CCLOK  ( desc.BufferDesc.Format	= (!WCASECMP(strV,L"R16G16B16A16f")) ?	DXGI_FORMAT_R16G16B16A16_FLOAT :
																											DXGI_FORMAT_R8G8B8A8_UNORM_SRGB; )

			// Size
			CCLTRY ( pDct->load ( adtString(L"Width"), vL ) );
			CCLOK  ( desc.BufferDesc.Width = adtInt(vL); )
			CCLTRY ( pDct->load ( adtString(L"Height"), vL ) );
			CCLOK  ( desc.BufferDesc.Height = adtInt(vL); )

			// Window output
			if (hr == S_OK && pWnd != NULL && pWnd->load ( adtString(L"Window"), vL ) == S_OK)
				desc.OutputWindow = (HWND)(U64)adtLong(vL);

			// Create the swap chain
			CCLTRY ( pFact->CreateSwapChain ( punkDev, &desc, &pSwpOp ) );
			}	// if

		// Context
		CCLTRY ( pDct->store ( adtString(L"SwapChain"), adtIUnknown(pSwpOp) ) );

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pSwpOp);
		_RELEASE(punkDev);
		_RELEASE(pFact);
		}	// if

	// Close/Destroy
	else if (_RCP(Close))
		{
		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Ensure swap chain is removed dictionary, this will release the interface
		CCLOK ( pDct->remove ( adtString(L"SwapChain") ); )
		}	// else if

	// Present image
	else if (_RCP(Present))
		{
		IDXGISwapChain	*pSwap	= NULL;
		adtValue			vL;
		adtIUnknown		unkV;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Swap chain
		if (hr == S_OK && pSwp == NULL)
			{
			CCLTRY ( pDct->load ( adtString(L"SwapChain"), vL ) );
			CCLTRY ( _QISAFE((unkV=vL), IID_IDXGISwapChain, &pSwp) );
			}	// if

		// Present
		CCLTRY ( pSwp->Present ( 0, 0 ) );
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pDct);
		_RELEASE(pSwp);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Device))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pDev);
		_QISAFE(unkV,IID_IDictionary,&pDev);
		}	// else if
	else if (_RCP(Window))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pWnd);
		_QISAFE(unkV,IID_IDictionary,&pWnd);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
