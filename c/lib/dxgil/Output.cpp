////////////////////////////////////////////////////////////////////////
//
//									Output.CPP
//
//				Implementation of the DXGI Output node.
//
////////////////////////////////////////////////////////////////////////

#include "dxgil_.h"
#include <stdio.h>

// Globals

Output :: Output ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pOut = NULL;
	}	// Output

HRESULT Output :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		_RELEASE(pOut);
		}	// else

	return hr;
	}	// onAttach

HRESULT Output :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Describe output
	if (_RCP(Describe))
		{
		IDictionary			*pDct = NULL;
		DXGI_OUTPUT_DESC	desc;

		// State check
		CCLTRYE ( (pOut != NULL), ERROR_INVALID_STATE );

		// Describe
		CCLTRY ( pOut->GetDesc ( &desc ) );
		if (hr == S_OK)
			{
			// Result dictionary
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );
			CCLTRY ( pDct->store ( adtString(L"Device"), adtString(desc.DeviceName) ) );
			CCLTRY ( pDct->store ( adtString(L"DesktopLeft"), adtInt(desc.DesktopCoordinates.left) ) );
			CCLTRY ( pDct->store ( adtString(L"DesktopRight"), adtInt(desc.DesktopCoordinates.right) ) );
			CCLTRY ( pDct->store ( adtString(L"DesktopTop"), adtInt(desc.DesktopCoordinates.top) ) );
			CCLTRY ( pDct->store ( adtString(L"DesktopBottom"), adtInt(desc.DesktopCoordinates.bottom) ) );
			CCLTRY ( pDct->store ( adtString(L"DesktopAttached"), adtBool(desc.AttachedToDesktop == TRUE) ) );
			CCLTRY ( pDct->store ( adtString(L"Rotation"), adtInt( 
											(desc.Rotation == DXGI_MODE_ROTATION_ROTATE90) ? 90 :
											(desc.Rotation == DXGI_MODE_ROTATION_ROTATE180) ? 180 :
											(desc.Rotation == DXGI_MODE_ROTATION_ROTATE270) ? 270 : 0 ) ) );
			CCLTRY ( pDct->store ( adtString(L"Monitor"), adtLong((U64)desc.Monitor) ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Describe,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pDct);
		}	// if

	// State
	else if (_RCP(Output))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pOut);
		_QISAFE(unkV,IID_IDXGIOutput,&pOut);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
