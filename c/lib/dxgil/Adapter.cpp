////////////////////////////////////////////////////////////////////////
//
//									Adapter.CPP
//
//				Implementation of the DXGI Adapter node.
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#include "dxgil_.h"
#include <stdio.h>

// Globals

Adapter :: Adapter ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pAd		= NULL;
	idxOut	= 0;
	}	// Adapter

HRESULT Adapter :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		_RELEASE(pAd);
		}	// else

	return hr;
	}	// onAttach

HRESULT Adapter :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Output enumeration
	if (_RCP(FirstOutput) || _RCP(NextOutput))
		{
		IDXGIOutput	*pOutput = NULL;

		// State check
		CCLTRYE ( (pAd != NULL), ERROR_INVALID_STATE );

		// Reset count
		if (_RCP(FirstOutput))
			idxOut = 0;

		// Next Output
		CCLTRY ( pAd->EnumOutputs ( idxOut++, &pOutput ) );
	
		// Result
		if (hr == S_OK)
			_EMT(NextOutput,adtIUnknown(pOutput));
		else
			_EMT(EndOutput,adtInt(hr));	

		// Clean up
		_RELEASE(pOutput);
		}	// if

	// Describe output
	else if (_RCP(Describe))
		{
		IDictionary			*pDct = NULL;
		DXGI_ADAPTER_DESC	desc;

		// State check
		CCLTRYE ( (pAd != NULL), ERROR_INVALID_STATE );

		// Describe
		CCLTRY ( pAd->GetDesc ( &desc ) );
		if (hr == S_OK)
			{
			// Result dictionary
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );
			CCLTRY ( pDct->store ( adtString(L"Description"), adtString(desc.Description) ) );
			CCLTRY ( pDct->store ( adtString(L"VendorId"), adtInt(desc.VendorId) ) );
			CCLTRY ( pDct->store ( adtString(L"DeviceId"), adtInt(desc.DeviceId) ) );
			CCLTRY ( pDct->store ( adtString(L"SubSysId"), adtInt(desc.SubSysId) ) );
			CCLTRY ( pDct->store ( adtString(L"Revision"), adtInt(desc.Revision) ) );
			CCLTRY ( pDct->store ( adtString(L"DedicatedVideoMemory"), adtLong(desc.DedicatedVideoMemory) ) );
			CCLTRY ( pDct->store ( adtString(L"DedicatedSystemMemory"), adtLong(desc.DedicatedSystemMemory) ) );
			CCLTRY ( pDct->store ( adtString(L"SharedSystemMemory"), adtLong(desc.SharedSystemMemory) ) );
			CCLTRY ( pDct->store ( adtString(L"LuidLow"), adtInt(desc.AdapterLuid.LowPart) ) );
			CCLTRY ( pDct->store ( adtString(L"LuidHigh"), adtInt(desc.AdapterLuid.HighPart) ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Describe,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pDct);
		}	// if

	// State
	else if (_RCP(Adapter))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pAd);
		_QISAFE(unkV,IID_IDXGIAdapter,&pAd);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
