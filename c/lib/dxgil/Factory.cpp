////////////////////////////////////////////////////////////////////////
//
//									Factory.CPP
//
//				Implementation of the DXGI factory node.
//
////////////////////////////////////////////////////////////////////////

#include "dxgil_.h"
#include <stdio.h>

// Globals

Factory :: Factory ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pFact = NULL;
	idxAd = 0;
	}	// Factory

HRESULT Factory :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		// Create object TODO Node properties for interface ?
		CreateDXGIFactory ( IID_IDXGIFactory, (void **) &pFact );
		}	// if

	// Detach
	else
		{
		_RELEASE(pFact);
		}	// else

	return hr;
	}	// onAttach

HRESULT Factory :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Adapter enumeration
	if (_RCP(FirstAdapter) || _RCP(NextAdapter))
		{
		IDXGIAdapter	*pAdapter = NULL;

		// State check
		CCLTRYE ( (pFact != NULL), ERROR_INVALID_STATE );

		// Reset count
		if (_RCP(FirstAdapter))
			idxAd = 0;

		// Next adapter
		CCLTRY ( pFact->EnumAdapters ( idxAd++, &pAdapter ) );
	
		// Result
		if (hr == S_OK)
			_EMT(NextAdapter,adtIUnknown(pAdapter));
		else
			_EMT(EndAdapter,adtInt(hr));	

		// Clean up
		_RELEASE(pAdapter);
		}	// if

	// State
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

//
// DxgiRef
//

DxgiRef::DxgiRef(HANDLE _hRef)
	{
	hRef = _hRef;
	}	// DxgiRef

void DxgiRef::destruct(void)
	{
	if (hRef != NULL)
		CloseHandle(hRef);
	}	// destruct
