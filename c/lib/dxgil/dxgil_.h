////////////////////////////////////////////////////////////////////////
//
//										DXGIL_.H
//
//				Implementation include file for 
//				DirectX Graphics Infrastructure (DXGI) library
//
////////////////////////////////////////////////////////////////////////

#ifndef	DXGIL__H
#define	DXGIL__H

// Includes
#include "dxgil.h"
#include "../../lib/nspcl/nspcl.h"

// External API
#include <dxgi1_2.h>
#include <d3d11_1.h>

///////////
// Objects
///////////

//
// Class - Reference counted objects.
//

class DxgiRef :
	public CCLObject										// Base class
	{
	public :
	DxgiRef ( HANDLE );									// Constructor

	// Run-time data
	HANDLE	hRef;											// Win32 handle

	// CCL
	CCL_OBJECT_BEGIN_INT(DxgiRef)
	CCL_OBJECT_END()
	virtual void		destruct	( void );			// Destruct object
	};

/////////
// Nodes
/////////

//
// Class - Adapter.  DXGI Adapter node.
//

class Adapter :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Adapter ( void );										// Constructor

	// Run-time data
	IDXGIAdapter	*pAd;									// Adapter interface
	U32				idxOut;								// Output enumeration

	// CCL
	CCL_OBJECT_BEGIN(Adapter)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Adapter)
	DECLARE_CON(Describe)
	DECLARE_RCP(FirstOutput)
	DECLARE_CON(NextOutput)
	DECLARE_EMT(EndOutput)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Adapter)
		DEFINE_CON(Describe)
		DEFINE_RCP(FirstOutput)
		DEFINE_CON(NextOutput)
		DEFINE_EMT(EndOutput)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :
	};

//
// Class - Factory.  DXGI factory node.
//

class Factory :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Factory ( void );										// Constructor

	// Run-time data
	IDXGIFactory	*pFact;								// Factory interface
	U32				idxAd;								// Adapter enumeration

	// CCL
	CCL_OBJECT_BEGIN(Factory)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(FirstAdapter)
	DECLARE_CON(NextAdapter)
	DECLARE_EMT(EndAdapter)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(FirstAdapter)
		DEFINE_CON(NextAdapter)
		DEFINE_EMT(EndAdapter)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :
	};

//
// Class - Output.  DXGI Output node.
//

class Output :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Output ( void );										// Constructor

	// Run-time data
	IDXGIOutput		*pOut;								// Output interface

	// CCL
	CCL_OBJECT_BEGIN(Output)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Describe)
	DECLARE_EMT(Error)
	DECLARE_RCP(Output)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Describe)
		DEFINE_EMT(Error)
		DEFINE_RCP(Output)
	END_BEHAVIOUR_NOTIFY()

	private :
	};

//
// Class - Share.  DXGI Sharing node.
//

class Share :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Share ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context dictionary
	IDictionary		*pDev;								// Device dictionary
	adtString		strName;								// Resource to share
	adtString		strShare;							// Share name
	adtBool			bCr,bRo;								// Open flags

	// CCL
	CCL_OBJECT_BEGIN(Share)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Close)
	DECLARE_RCP(Device)
	DECLARE_RCP(Dictionary)
	DECLARE_EMT(Error)
	DECLARE_RCP(Name)
	DECLARE_CON(Open)
	DECLARE_RCP(Share)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Close)
		DEFINE_RCP(Device)
		DEFINE_RCP(Dictionary)
		DEFINE_EMT(Error)
		DEFINE_RCP(Name)
		DEFINE_CON(Open)
		DEFINE_RCP(Share)
	END_BEHAVIOUR_NOTIFY()

	private :
	};

//
// Class - SwapChain.  DXGI SwapChain node.
//

class SwapChain :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	SwapChain ( void );									// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context dictionary
	IDictionary		*pDev;								// Device dictionary
	IDictionary		*pWnd;								// Window dictionary
	IDXGISwapChain	*pSwp;								// Swap chain

	// CCL
	CCL_OBJECT_BEGIN(SwapChain)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Close)
	DECLARE_RCP(Device)
	DECLARE_RCP(Dictionary)
	DECLARE_EMT(Error)
	DECLARE_CON(Open)
	DECLARE_RCP(Present)
	DECLARE_RCP(Window)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Close)
		DEFINE_RCP(Device)
		DEFINE_RCP(Dictionary)
		DEFINE_EMT(Error)
		DEFINE_CON(Open)
		DEFINE_RCP(Present)
		DEFINE_RCP(Window)
	END_BEHAVIOUR_NOTIFY()

	private :
	};

#endif
