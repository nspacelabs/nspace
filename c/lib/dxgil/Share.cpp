////////////////////////////////////////////////////////////////////////
//
//									Share.CPP
//
//				Implementation of the DXGI share node.
//
////////////////////////////////////////////////////////////////////////

#include "dxgil_.h"
#include <stdio.h>

// Globals

Share :: Share ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	pDev	= NULL;
	bRo	= true;
	bCr	= false;
	}	// Share

HRESULT Share :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Name"), vL ) == S_OK)
			hr = adtValue::toString(vL,strName);
		if (pnDesc->load ( adtString(L"Share"), vL ) == S_OK)
			hr = adtValue::toString(vL,strShare);
		if (pnDesc->load ( adtString(L"ReadOnly"), vL ) == S_OK)
			bRo = vL;
		if (pnDesc->load ( adtString(L"Create"), vL ) == S_OK)
			bCr = vL;
		}	// if

	// Detach
	else
		{
		_RELEASE(pDct);
		_RELEASE(pDev);
		}	// else

	return hr;
	}	// onAttach

HRESULT Share :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open/Create
	if (_RCP(Open))
		{
		IUnknown			*punkDev = NULL;
		IDXGIResource1	*pRes		= NULL;
		ID3D11Device1	*pDev1	= NULL;
		HANDLE			hShare	= NULL;
		DWORD				dwAccess	= DXGI_SHARED_RESOURCE_READ;
		adtValue			vL;
		adtIUnknown		unkV;

		// State check
		CCLTRYE ( pDct != NULL && pDev != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0 && strShare.length() > 0, ERROR_INVALID_STATE );

		// Extract device interface
		CCLTRY ( pDev->load ( adtString(L"Device"), vL ) );
		CCLTRYE( (IUnknown *)(NULL) != (unkV=vL), ERROR_INVALID_STATE );
		if (hr == S_OK)
			{
			punkDev = unkV;
			_ADDREF(punkDev);
			}	// if

		// Access flags
		if (hr == S_OK && !bRo)
			dwAccess |= DXGI_SHARED_RESOURCE_WRITE;

		// Create a shared handle for the resource, must have been created to support sharing.
		if (hr == S_OK && bCr)
			{
			// Access resource by name that is to be shared
			CCLTRY ( pDct->load ( strName, vL ) );
			CCLTRYE( (IUnknown *)(NULL) != (unkV=vL), ERROR_INVALID_STATE );
			CCLTRY(_QISAFE(unkV,IID_IDXGIResource1,&pRes));

			// Create a new share
			CCLTRY ( pRes->CreateSharedHandle ( NULL, dwAccess, strShare, &hShare ) );

			// Result
			CCLTRY ( pDct->store ( adtString(L"Share"), adtIUnknown(new DxgiRef(hShare)) ) );
			}	// if

		// Open an existing shared resource
		else if (hr == S_OK)
			{
			// Existing device
			CCLTRY(_QI(punkDev,IID_ID3D11Device1,&pDev1));

			// Open existing share by name
			CCLTRY ( pDev1->OpenSharedResourceByName ( strName, dwAccess, IID_IDXGIResource1, 
																		(void **) &pRes ) );

			// Update resource by name
			CCLTRY ( pDct->store ( strName, adtIUnknown(pRes) ) );
			}	// else if

		// Results
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pDev1);
		_RELEASE(pRes);
		}	// if

	// Close/Destroy
	else if (_RCP(Close))
		{
		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Ensure objects are removed dictionary, this will release the interface
		if (hr == S_OK && bCr)
			pDct->remove ( adtString(L"Share") );
		else if (hr == S_OK && !bCr)
			pDct->remove ( strName );
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Device))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pDev);
		_QISAFE(unkV,IID_IDictionary,&pDev);
		}	// else if
	else if (_RCP(Name))
		hr = adtValue::toString(v,strName);
	else if (_RCP(Share))
		hr = adtValue::toString(v,strShare);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
