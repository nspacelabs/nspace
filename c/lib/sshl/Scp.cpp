////////////////////////////////////////////////////////////////////////
//
//									SCP.CPP
//
//				Implementation of the SSH SCP node
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"

Scp :: Scp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pSess		= NULL;
	pCh		= NULL;
	pStm		= NULL;
	strRoot	= ".";
	bWr		= false;
	mode		= perms::owner_read;
	}	// Scp

HRESULT Scp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load(adtString("Write"),vL) == S_OK)
			bWr = adtBool(vL);
		if (pnDesc->load(adtString("Root"),vL) == S_OK)
			onReceive(prRoot,vL);
		if (pnDesc->load(adtString("Location"),vL) == S_OK)
			onReceive(prLocation,vL);
		if (pnDesc->load(adtString("Name"),vL) == S_OK)
			onReceive(prName,vL);
		if (pnDesc->load(adtString("Mode"),vL) == S_OK)
			onReceive(prMode,vL);
		}	// if

	// Detach
	else 
		{
		// Close current Scp
		onReceive(prClose,adtInt());

		// Clean up
		_RELEASE(pCh);
		_RELEASE(pSess);
		_RELEASE(pStm);
		}	// if

	return hr;
	}	// onAttach

HRESULT Scp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open a Scp session on top of an existing Ssh session
	if (_RCP(Open))
		{
		ssh_session	ps		= NULL;
		ssh_scp		ch		= NULL;
		char			*pcR	= NULL;
		int			rc		= 0;
		adtValue		vL;

		// State check
		CCLTRYE ( pSess != NULL && pCh != NULL, ERROR_INVALID_STATE );

		// Session
		CCLTRY ( pSess->load ( adtString(L"Session"), vL ) );
		CCLTRYE( (ps = (ssh_session)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// ASCII version of root
		CCLTRY ( strRoot.toAscii ( &pcR ) );

		// Thread safety
		sshEnter();

		// Create and open a new scp channel
		CCLTRYE ( (ch = ssh_scp_new(ps,(bWr) ? SSH_SCP_WRITE|SSH_SCP_RECURSIVE : 
						SSH_SCP_READ, pcR)) != NULL, E_OUTOFMEMORY );
		CCLTRYE ( (rc = ssh_scp_init(ch)) == SSH_OK, rc );

		// Thread safety
		sshLeave();

		// Result
		CCLTRY ( pCh->store ( adtString(L"Channel"), adtLong((U64)ch) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pCh));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		if (hr != S_OK && ch != NULL)
			{
			ssh_scp_close(ch);
			ssh_scp_free(ch);
			}	// if
		_FREEMEM(pcR);
		}	// if

	// Close Scp
	else if (_RCP(Close))
		{
		ssh_scp	ch	= NULL;
		adtValue	vL;

		// State check
		CCLTRYE ( pCh != NULL, ERROR_INVALID_STATE );

		// Now closed
		CCLOK ( _EMT(Close,adtIUnknown(pCh)); )

		// Clean up
		if (hr == S_OK && pCh->load ( adtString(L"Channel"), vL ) == S_OK)
			{
			// Thread safety
			sshEnter();

			// Scp context
			ch = (ssh_scp)(U64)adtLong(vL);
			ssh_scp_close(ch);
			ssh_scp_free(ch);

			// Thread safety
			sshLeave();

			// Closed
			pCh->remove ( adtString(L"Channel") );
			}	// if

		}	// else if

	// Enter/create a location
	else if (_RCP(Enter))
		{
		ssh_scp	ch		= NULL;
		int		rc		= 0;
		char		*pcL	= NULL;
		adtValue	vL;

		// State check
		CCLTRYE ( pCh != NULL,				ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0,	ERROR_INVALID_STATE );

		// ASCII version of name
		CCLTRY ( strLoc.toAscii ( &pcL ) );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_scp)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Enter/create the directory
		CCLTRYE ( (rc = ssh_scp_push_directory ( ch, pcL, mode )) == SSH_OK, rc );

		// Thread safety
		sshLeave();

		// Result
		if (hr == S_OK)
			_EMT(Enter,strLoc);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcL);
		}	// else if

	// Write file
	else if (_RCP(Write))
		{
		ssh_scp	ch		= NULL;
		int		rc		= 0;
		char		*pcN	= NULL;
		adtValue	vL;

		// State check
		CCLTRYE ( pCh != NULL,				ERROR_INVALID_STATE );
		CCLTRYE ( pStm != NULL,				ERROR_INVALID_STATE );
		CCLTRYE ( bWr == true,				ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0,	ERROR_INVALID_STATE );
		CCLTRYE ( iSz > 0,					ERROR_INVALID_STATE );

		// ASCII version of name
		CCLTRY ( strName.toAscii ( &pcN ) );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_scp)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Request a write
		CCLTRYE ( (rc = ssh_scp_push_file64 ( ch, pcN, iSz, mode )) == SSH_OK, rc );

		// Thread safety
		sshLeave();

		// Copy bytes to file
		for (U64 nleft = iSz;hr == S_OK && nleft > 0;)
			{
			U64 nr;

			// Read next block
			CCLTRY (pStm->read ( bBfr, sizeof(bBfr), &nr ));

			// Thread safety
			sshEnter();

			// Send to remote system
			CCLTRYE ( (rc = ssh_scp_write ( ch, bBfr, nr )) == SSH_OK, rc );

			// Thread safety
			sshLeave();

			// Done w/block
			CCLOK ( nleft -= nr; )
			}	// for

		// Result
		if (hr == S_OK)
			_EMT(Write,adtIUnknown(pStm));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcN);
		}	// else if

	// Read file
	else if (_RCP(Read))
		{
		ssh_scp		ch		= NULL;
		int			rc		= 0;
		char			*pcN	= NULL;
		adtValue		vL;
		adtLong		iSzRd;
		int			iMode;
		adtString	strNameRd;

		// State check
		CCLTRYE ( pCh != NULL,				ERROR_INVALID_STATE );
		CCLTRYE ( pStm != NULL,				ERROR_INVALID_STATE );
		CCLTRYE ( bWr == false,				ERROR_INVALID_STATE );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_scp)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Request the transfer (file(s) specified in root at open time
		CCLTRYE ( (rc = ssh_scp_pull_request ( ch )) == SSH_SCP_REQUEST_NEWFILE, rc );

		// Retrieve information about the transfer
		CCLTRYE ( (iSzRd = ssh_scp_request_get_size ( ch )) > 0, ERROR_NOT_FOUND );
		CCLOK   ( _EMT(Size,iSzRd); )

		// Filename
		CCLOK	( strNameRd = ssh_scp_request_get_filename ( ch ); )
		CCLOK	( _EMT(Name,strNameRd); )

		// Permissions
		CCLTRYE ( (iMode = ssh_scp_request_get_permissions ( ch )) != -1, E_UNEXPECTED );

		// Accept the transfer
		CCLTRYE ( (rc = ssh_scp_accept_request ( ch )) == SSH_OK, rc );

		// Thread safety
		sshLeave();

		// Copy bytes to stream
		for (U64 nleftRd = iSzRd;hr == S_OK && nleftRd > 0;)
			{
			U64 nr;

			// Amount to read
			nr = (nleftRd < sizeof(bBfr)) ? nleftRd : sizeof(bBfr);

			// Thread safety
			sshEnter();

			// Read next block
			CCLTRYE ( (nr = ssh_scp_read ( ch, bBfr, nr )) != SSH_ERROR, rc );

			// Thread safety
			sshLeave();

			// Write to provided stream
			CCLTRY (pStm->write ( bBfr, nr, NULL ));

			// Done w/block
			CCLOK ( nleftRd -= nr; )
			}	// for

		// Result
		if (hr == S_OK)
			_EMT(Read,adtIUnknown(pStm));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcN);
		}	// else if

	// Mode permissions
	else if (_RCP(Mode))
		{
		adtString	strMode(v);
		int			len = strMode.length();

		// Generate mode flags from string
		mode = 0;
		if (len > 0 && strMode[0] == 'r')
			mode |= perms::owner_read;
		if (len > 1 && strMode[1] == 'w')
			mode |= perms::owner_write;
		if (len > 2 && strMode[2] == 'x')
			mode |= perms::owner_exec;

		// Mode bits - group
		if (len > 3 && strMode[3] == 'r')
			mode |= perms::group_read;
		if (len > 4 && strMode[4] == 'w')
			mode |= perms::group_write;
		if (len > 5 && strMode[5] == 'x')
			mode |= perms::group_exec;

		// Mode bits - other
		if (len > 6 && strMode[6] == 'r')
			mode |= perms::others_read;
		if (len > 7 && strMode[7] == 'w')
			mode |= perms::others_write;
		if (len > 8 && strMode[8] == 'x')
			mode |= perms::others_exec;

		}	// else if

	// State
	else if (_RCP(Session))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSess);
		_QISAFE(unkV,IID_IDictionary,&pSess);
		}	// else if
	else if (_RCP(Channel))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pCh);
		_QISAFE(unkV,IID_IDictionary,&pCh);
		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStm);
		_QISAFE(unkV,IID_IByteStream,&pStm);
		}	// else if
	else if (_RCP(Size))
		iSz = adtInt(v);
	else if (_RCP(Root))
		hr = adtValue::toString(v,strRoot);
	else if (_RCP(Location))
		hr = adtValue::toString(v,strLoc);
	else if (_RCP(Name))
		hr = adtValue::toString(v,strName);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

