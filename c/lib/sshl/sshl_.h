////////////////////////////////////////////////////////////////////////
//
//										SSHL_.H
//
//			Implementaiton include file for the SSH library
//
////////////////////////////////////////////////////////////////////////

#ifndef	SSHL__H
#define	SSHL__H

// SSH (this include must come first)
#include	<libssh/libssh.h>
#include <libssh/callbacks.h>

// Includes
#include	"sshl.h"

// From C++ <filesystem> so it is reusable in Windows and Unix

// ENUM perms
enum perms { // names for permissions
    none             = 0,
    owner_read       = 0400, // S_IRUSR
    owner_write      = 0200, // S_IWUSR
    owner_exec       = 0100, // S_IXUSR
    owner_all        = 0700, // S_IRWXU
    group_read       = 040, // S_IRGRP
    group_write      = 020, // S_IWGRP
    group_exec       = 010, // S_IXGRP
    group_all        = 070, // S_IRWXG
    others_read      = 04, // S_IROTH
    others_write     = 02, // S_IWOTH
    others_exec      = 01, // S_IXOTH
    others_all       = 07, // S_IRWXO
    all              = 0777,
    set_uid          = 04000, // S_ISUID
    set_gid          = 02000, // S_ISGID
    sticky_bit       = 01000, // S_ISVTX
    mask             = 07777,
    unknown          = 0xFFFF,
    add_perms        = 0x10000,
    remove_perms     = 0x20000,
    resolve_symlinks = 0x40000
};

//
// Objects
//

//
// Class - ChannelStream.  Stream object on top of a channel.
//

class Channel;
class ChannelStream :
	public CCLObject,										// Base class
	public IByteStream									// Interface
	{
	public :
	ChannelStream ( IDictionary * );					// Constructor

	// Run-time data
	IDictionary		*pDctCh;								// Channel dictionary

	// 'IByteStream' members
	STDMETHOD(available)	( U64 * );
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * );
	STDMETHOD(flush)		( void );
	STDMETHOD(read)		( void *, U64, U64 * );
	STDMETHOD(seek)		( S64, U32, U64 * );
	STDMETHOD(setSize)	( U64 );
	STDMETHOD(write)		( void const *, U64, U64 * );

	// CCL
	CCL_OBJECT_BEGIN_INT(ChannelStream)
		CCL_INTF(IByteStream)
	CCL_OBJECT_END()
	virtual void		destruct	( void );			// Destruct object

	private :

	// Internal utilities
	HRESULT getCh ( ssh_channel * );

	};

//
// Class - SshRef.  Reference counted SSH resource.
//

class SshRef :
	public CCLObject,										// Base class
	public IThis											// Interface
	{
	public :
	SshRef ( void );										// Constructor
	SshRef ( ssh_session );								// Constructor
	SshRef ( ssh_channel );								// Constructor
	SshRef ( ssh_key );									// Constructor

	// Run-time data
	ssh_session		pSess;								// Session
	ssh_channel		pCh;									// Channel
	ssh_key			pKey;									// Key

	// 'IThis' members
	STDMETHOD(getThis)	( void **ppv ) { *ppv = this; return S_OK; }

	// CCL
	CCL_OBJECT_BEGIN_INT(SshRef)
		CCL_INTF(IThis)
	CCL_OBJECT_END()
	virtual void		destruct	( void );			// Destruct object
	};

//
// Nodes
//

//
// Class - Avail.  Read availability for channels.
//

class Avail :
	public CCLObject,										// Base class
	public Behaviour,										// Interface
	public ITickable										// Interface
	{
	public :
	Avail ( void );										// Constructor

	// Run-time data
	IDictionary	*pDctCh;									// Channel dictionary
	IThread		*pThrd;									// Worker thread
	IDictionary	*pDctAv;									// Available dictionaries
	IDictionary	*pDctMap;								// Map channels to dictionaries
	IIt			*pInAv;									// Iterator
	bool			bAvail;									// Running ?
	adtBool		bRead;									// Readability ?
	adtBool		bWrite;									// Writeability ?
	adtBool		bExcep;									// Execptions ?
	adtInt		iTo;										// Configured timeout
	ssh_channel	*pChsR,*pChsW,*pChsX;				// Channel lists
	U32			nChs;										// Size of channel list

	// 'ITickable' members
	STDMETHOD(tick)		( void );
	STDMETHOD(tickAbort)	( void );
	STDMETHOD(tickBegin)	( void );
	STDMETHOD(tickEnd)	( void );

	// CCL
	CCL_OBJECT_BEGIN(Avail)
		CCL_INTF(IBehaviour)
		CCL_INTF(ITickable)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Add)
	DECLARE_RCP(Channel)
	DECLARE_RCP(Remove)
	DECLARE_RCP(Start)
	DECLARE_RCP(Stop)
	DECLARE_RCP(Timeout)
	DECLARE_EMT(Error)
	DECLARE_EMT(Read)
	DECLARE_EMT(Write)
	DECLARE_EMT(Exception)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Add)
		DEFINE_RCP(Channel)
		DEFINE_RCP(Remove)
		DEFINE_RCP(Start)
		DEFINE_RCP(Stop)
		DEFINE_RCP(Timeout)

		DEFINE_EMT(Error)
		DEFINE_EMT(Read)
		DEFINE_EMT(Write)
		DEFINE_EMT(Exception)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Channel.  SSH channel node.
//

class Channel :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Channel ( void );										// Constructor

	// Run-time data
	IDictionary	*pSess;									// Session context
	IDictionary	*pCh;										// Channel context
	adtBool		bBlocking;								// Blocking ?

	// CCL
	CCL_OBJECT_BEGIN(Channel)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Channel)
	DECLARE_CON(Close)
	DECLARE_CON(Execute)
	DECLARE_CON(Open)
	DECLARE_EMT(Pending)
	DECLARE_RCP(Session)
	DECLARE_CON(Stream)
	DECLARE_CON(Eof)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Channel)
		DEFINE_CON(Close)
		DEFINE_CON(Execute)
		DEFINE_CON(Open)
		DEFINE_EMT(Pending)
		DEFINE_RCP(Session)
		DEFINE_CON(Stream)
		DEFINE_CON(Eof)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Key.  SSH key node.
//

class Key :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Key ( void );											// Constructor

	// Run-time data
	IDictionary	*pDct;									// Key dictionary
	adtString	strAlgor;								// Algorithm
	adtInt		iSz;										// Key size
	adtString	strLoc;									// Location

	// CCL
	CCL_OBJECT_BEGIN(Key)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Create)
	DECLARE_CON(Export)
	DECLARE_CON(Import)
	DECLARE_RCP(Location)
	DECLARE_CON(Public)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Create)
		DEFINE_CON(Export)
		DEFINE_CON(Import)
		DEFINE_RCP(Location)
		DEFINE_CON(Public)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Scp.  SSH secure copy node.
//

class Scp :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Scp ( void );											// Constructor

	// Run-time data
	IDictionary	*pSess;									// Session context
	IDictionary	*pCh;										// Scp context
	adtString	strRoot;									// Destination root location
	adtBool		bWr;										// Write/read mode
	adtString	strLoc;									// Active location
	adtString	strName;									// Active filename
	adtLong		iSz;										// Size of outgoing file
	IByteStream	*pStm;									// Read/write file stream
	int			mode;										// Permissions mode
	BYTE			bBfr[0x10000];							// Copy buffer

	// CCL
	CCL_OBJECT_BEGIN(Scp)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Session)
	DECLARE_RCP(Channel)
	DECLARE_RCP(Root)
	DECLARE_RCP(Location)
	DECLARE_CON(Close)
	DECLARE_CON(Enter)
	DECLARE_RCP(Mode)
	DECLARE_CON(Name)
	DECLARE_CON(Open)
	DECLARE_CON(Read)
	DECLARE_CON(Write)
	DECLARE_CON(Size)
	DECLARE_RCP(Stream)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Session)
		DEFINE_RCP(Channel)
		DEFINE_RCP(Root)
		DEFINE_RCP(Location)
		DEFINE_RCP(Mode)
		DEFINE_CON(Name)
		DEFINE_CON(Close)
		DEFINE_CON(Enter)
		DEFINE_CON(Open)
		DEFINE_CON(Read)
		DEFINE_CON(Write)
		DEFINE_CON(Size)
		DEFINE_RCP(Stream)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Session.  SSH session node.
//

class Session :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Session ( void );										// Constructor

	// Run-time data
	IDictionary	*pDct;									// Session dictionary
	IDictionary *pDctPrv;								// Key dictionary
	adtString	strAddr;									// Address
	adtString	strUser,strPass;						// User/password
	adtInt		iTo;										// Session timeout
	adtBool		bBlocking;								// Blocking ?

	// CCL
	CCL_OBJECT_BEGIN(Session)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Address)
	DECLARE_CON(Connect)
	DECLARE_CON(Disconnect)
	DECLARE_RCP(Private)
	DECLARE_RCP(Password)
	DECLARE_RCP(Session)
	DECLARE_RCP(Timeout)
	DECLARE_EMT(Error)
	DECLARE_EMT(Pending)
	DECLARE_RCP(Username)
//	DECLARE_CON(Verify)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Address)
		DEFINE_CON(Connect)
		DEFINE_CON(Disconnect)
		DEFINE_RCP(Private)
		DEFINE_RCP(Password)
		DEFINE_RCP(Session)
		DEFINE_RCP(Timeout)
		DEFINE_EMT(Error)
		DEFINE_EMT(Pending)
		DEFINE_RCP(Username)
//		DEFINE_CON(Verify)
	END_BEHAVIOUR_NOTIFY()
	};

// Internal utilities
HRESULT sshEnter ( void );
HRESULT sshLeave ( void );

#endif


