////////////////////////////////////////////////////////////////////////
//
//									Avail.CPP
//
//					Implementation of the ssh availability node
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"
#include <stdio.h>

Avail :: Avail ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the availability node
	//
	////////////////////////////////////////////////////////////////////////
	pDctAv		= NULL;
	pDctCh		= NULL;
	pInAv			= NULL;
	pChsR			= NULL;
	pChsW			= NULL;
	pChsX			= NULL;
	nChs			= 0;
	pThrd			= NULL;
	bAvail		= false;
	iTo			= 1000;
	bRead			= true;
	bWrite		= false;
	bExcep		= false;
	}	// Avail

HRESULT Avail :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue	v;

		// Objects
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctAv ) );
		CCLTRY ( pDctAv->keys ( &pInAv ));
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctMap ) );

		// Attributes
		if (hr == S_OK && pnDesc->load ( adtString(L"Timeout"), v ) == S_OK)
			iTo = v;
		if (hr == S_OK && pnDesc->load ( adtString(L"Read"), v ) == S_OK)
			bRead = v;
		if (hr == S_OK && pnDesc->load ( adtString(L"Write"), v ) == S_OK)
			bWrite = v;
		if (hr == S_OK && pnDesc->load ( adtString(L"Exception"), v ) == S_OK)
			bExcep = v;
		}	// if

	// Detach
	else
		{
		// Shutdown thread
		if (pThrd != NULL)
			{
			pThrd->threadStop(5000);
			pThrd->Release();
			pThrd = NULL;
			}	// if

		// Clean up
		nChs = 0;
		_FREEMEM(pChsR);
		_FREEMEM(pChsW);
		_FREEMEM(pChsX);
		_RELEASE(pDctCh);
		_RELEASE(pDctMap);
		_RELEASE(pInAv);
		_RELEASE(pDctAv);
		}	// if

	return hr;
	}	// onAttach

HRESULT Avail :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Start
	if (_RCP(Start))
		{
		// State check
		CCLTRYE ( pThrd == NULL, ERROR_INVALID_STATE );
//		dbgprintf ( L"%s:Avail::receive:Start\r\n", (LPCWSTR)strnName );

		// Debug
//		lprintf ( LOG_DBG, L"%s(%p):Start:pAvails %p:%d\n", 
//						(LPCWSTR)strnName, this, pAvails, hr );

		// Start server thread
		CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
		CCLOK (bAvail = true;)
		CCLTRY(pThrd->threadStart ( this, 5000 ));
		}	// if

	// Stop
	else if (_RCP(Stop))
		{
		// State check
		CCLTRYE ( pThrd != NULL, ERROR_INVALID_STATE );
//		dbgprintf ( L"%s:Avail::receive:Stop\r\n", (LPCWSTR)strnName );

		// Debug
//		lprintf ( LOG_DBG, L"%s(%p):Stop:pAvails %p:%d\n",
//						(LPCWSTR)strnName, this, pAvails, hr );

		// Shutdown worker thread
		if (pThrd != NULL)
			pThrd->threadStop(10000);
		_RELEASE(pThrd);
		}	// if

	// Add
	else if (_RCP(Add))
		{
		adtValue	vL;

		// State check
		CCLTRYE	(	pDctCh != NULL, ERROR_INVALID_STATE );
		CCLTRYE	(	pDctCh->load ( adtString(L"Channel"), vL ) == S_OK,
						ERROR_INVALID_STATE );

		// Store in list with dictionary as key and notify
//		lprintf ( LOG_DBG, L"%s:Add:%p\r\n", (LPCWSTR)strnName, pDct );
		CCLTRY	( pDctAv->store ( adtIUnknown(pDctCh), adtInt(0) ) );
		}	// else if

	// Remove
	else if (_RCP(Remove))
		{
		// State check
		CCLTRYE	( pDctCh != NULL, ERROR_INVALID_STATE );

		// Attempt to remove dictionary from list
//		lprintf ( LOG_DBG, L"%s:Remove:%p\r\n", (LPCWSTR)strnName, pDct );
		CCLTRY	( pDctAv->remove ( adtIUnknown(pDctCh) ) );
		}	// else if

	// State
	else if (_RCP(Channel))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctCh);
		hr = _QISAFE(unkV,IID_IDictionary,&pDctCh);
		}	// else if
	else if (_RCP(Timeout))
		iTo = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Avail :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Turn off ticking
//	lprintf ( LOG_DBG, L"%s(%p)\r\n", (LPCWSTR)strnName, this );
	bAvail = false;

	return S_OK;
	}	// tickAbort

HRESULT Avail :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	int				ret		= 0;
	adtValue			vL;
	struct timeval	to;
	U32				sz;

	// Debug
//	lprintf ( LOG_DBG, L"Avail::tick { %s\r\n", (LPCWSTR)strnName );

	// Sanity check
	CCLTRYE ( (bAvail == true), S_FALSE );

	// Wait for activity on sockets
	if (hr == S_OK)
		{
		// Ensure enough room in channel list
		CCLTRY ( pDctAv->size ( &sz ) );
		if (hr == S_OK && sz+1 > nChs)
			{
			nChs = sz+1;									// +1 is for NULL termination
			CCLTRYE ( (pChsR = (ssh_channel *) _REALLOCMEM ( pChsR,
							nChs*sizeof(ssh_channel) )) != NULL, E_OUTOFMEMORY );
			CCLTRYE ( (pChsW = (ssh_channel *) _REALLOCMEM ( pChsW,
							nChs*sizeof(ssh_channel) )) != NULL, E_OUTOFMEMORY );
			CCLTRYE ( (pChsX = (ssh_channel *) _REALLOCMEM ( pChsX,
							nChs*sizeof(ssh_channel) )) != NULL, E_OUTOFMEMORY );
			}	// if

		// Run down list adding to channel lists.
		CCLOK ( sz = 0; )
		CCLOK ( pDctMap->clear(); )
//		lprintf ( L"%s:1\r\n", (LPCWSTR)strnName );
		CCLTRY( pInAv->begin() );
		for (sz = 0;hr == S_OK && pInAv->read ( vL ) == S_OK;++sz)
			{
			IDictionary	*pDctAdd = NULL;
			ssh_channel ch			= NULL;
			adtIUnknown unkV(vL);

			// Channel state
			CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctAdd) );
			CCLTRY ( pDctAdd->load ( adtString(L"Channel"), vL ) );
			CCLTRYE ( (ch = (ssh_channel)(U64)adtLong(vL)) != NULL, E_UNEXPECTED );

			// Add to lists
			CCLOK ( pChsR[sz]	= ch; )
			CCLOK ( pChsW[sz]	= ch; )
			CCLOK ( pChsX[sz]	= ch; )

			// Associate the channel with the dictionary for later mapping
			CCLTRY ( pDctMap->store ( adtLong((U64)ch), adtIUnknown(pDctAdd) ) );

			// No need to fail if non-channel dictionary ?
			if (hr != S_OK)
				hr = S_OK;

			// Clean up and continue scan
			_RELEASE(pDctAdd);
			pInAv->next();
			}	// for

		// Terminate slots
		CCLOK ( pChsR[sz]	= NULL; )
		CCLOK ( pChsW[sz]	= NULL; )
		CCLOK ( pChsX[sz]	= NULL; )

		// Wait for activity or just sleep if nothing to do
		if (hr == S_OK)
			{
			// Execte
			if (sz > 0)
				{
				// Thread safety
				sshEnter();

				// libssh is apparently not thread save so only 'poll' the channels for availability
				// and if there is nothing going on, the delay will be done outside the call.
				// TODO: Fix this, slow if activity comes in during sleep.
				memset ( &to, 0, sizeof(to) );

				// Wait for channel activity.  
				// SSH_OK(0) if operation successful.
				// SSH_AGAIN(-2) on timeout
//				lprintf ( LOG_DBG, L"%s:hr 0x%x sktMax %d\r\n", (LPCWSTR)strnName, hr, sktMax );
				ret = ssh_channel_select ( (bRead) ? pChsR : NULL, (bWrite) ? pChsW : NULL, 
													(bExcep) ? pChsX : NULL, &to );
//							S_OK : ret;
//				lprintf ( LOG_DBG, L"%s:ret %d (%p,%p,%p) (to %d %d)\r\n", 
//								(LPCWSTR)strnName, ret, pChsR[0], pChsW[0], pChsX[0],
//								to.tv_sec, to.tv_usec );
//				if (ret != 0)
//					lprintf ( LOG_DBG, L"Return %d\r\n", ret );

				// Check for shutdown immediately to avoid unncessary emissions
				if (!bAvail)
					{
//					lprintf ( LOG_DBG, L"%s(%p):Aborting after select\n", (LPCWSTR)strnName, this );
					hr = S_FALSE;
					}	// if

				// Thread safety
				sshLeave();
				}	// if

			// Debug
//			lprintf ( LOG_DBG, L"hr 0x%x sz %d ret %d\r\n", hr, sz, ret );

			// Need to delay ?
			if (sz == 0 || ret == SSH_AGAIN)
				{
				#ifdef	_WIN32
				Sleep ( (DWORD)(S32)iTo );
				#else
				usleep( ((S32)(iTo))*1000 );
				#endif
				}	// if

			}	// if
//		lprintf ( LOG_DBG, L"%s:s %d:hr 0x%x\r\n", (LPCWSTR) strnName, s, hr );
		}	// if

	// Still Availing ?
	CCLTRYE ( (bAvail == true), S_FALSE );
//	if (!bAvail)
//		lprintf ( LOG_DBG, L"%s(%p):Aborting after select\n", (LPCWSTR)strnName, this );

	// Loop through the channel lists looking for active channels
	if (hr == S_OK && ret == 0)
		{
		// Check all lists
		for (int i = 0;i < 3 && hr == S_OK;++i)
			{
			// Channel list
			ssh_channel		*pChs =	(i == 0 && bRead)		? pChsR :
											(i == 1 && bWrite)	? pChsW :
											(i == 2 && bExcep)	? pChsX : NULL;
			if (pChs == NULL) continue;

			// Active channels
			for (int j = 0;pChs[j] != NULL;++j)
				{
				adtValue vMap;

				// Dictionary for channel
				pDctMap->load ( adtLong((U64)pChs[j]), vMap );

				// Channel emissions
				if			(i == 0)
					_EMT(Read,vMap);
				else if	(i == 1)
					_EMT(Write,vMap);
				else
					_EMT(Exception,vMap);
				}	// for

			}	// for

		}	// if

	// Debug
	#ifdef	_WIN32
	if (hr != S_OK && bAvail == true)
		{
		dbgprintf ( L"Avail::tick:Returning error! 0x%x\n", hr );
		}	// if
	#endif
//	lprintf ( LOG_DBG, L"} Avail::tick, hr 0x%x\n", hr );

	return hr;
	}	// tick

HRESULT Avail :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;
	return hr;
	}	// tickBegin

HRESULT Avail :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"%s(%p)\r\n", (LPCWSTR)strnName, this );

	// Clean up
	pDctMap->clear();

	return S_OK;
	}	// tickEnd


