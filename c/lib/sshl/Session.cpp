////////////////////////////////////////////////////////////////////////
//
//									SESSION.CPP
//
//				Implementation of the SSH session node
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"

Session :: Session ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct			= NULL;
	pDctPrv		= NULL;
	iTo			= 1000;
	bBlocking	= true;
	}	// Session

HRESULT Session :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load(adtString("Timeout"),vL) == S_OK)
			onReceive(prTimeout,vL);
		if (pnDesc->load(adtString("Blocking"),vL) == S_OK)
			bBlocking = vL;
		if (pnDesc->load(adtString("Username"),vL) == S_OK)
			onReceive(prUsername,vL);
		if (pnDesc->load(adtString("Password"),vL) == S_OK)
			onReceive(prPassword,vL);
		}	// if

	// Detach
	else
		{
		onReceive(prDisconnect,adtInt());
		_RELEASE(pDctPrv);
		_RELEASE(pDct);
		}	// if

	return S_OK;
	}	// onAttach

HRESULT Session :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Connect to system.  NOTE: In non-blocking mode the 'connect'
	// function may need to be called repeatadly (SSH_AGAIN) so this signal
	// will only create a new session object if necessary.
	if (_RCP(Connect))
		{
		ssh_session	ps			= NULL;
		int			state		= 0;
		int			rc			= 0;
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
//		lprintf ( LOG_DBG, L"pDct %p, hr 0x%x\r\n", pDct, hr );

		// Already connected/authorized ?
		CCLTRYE ( (pDct->load ( adtString(L"Authorized"), vL ) != S_OK),
						ERROR_INVALID_STATE );

		// Does context already have a session object ?
		if (hr == S_OK && pDct->load ( adtString(L"Session"), vL ) != S_OK)
			{
			int	verbosity	= SSH_LOG_PROTOCOL;
			long	to				= 0;
			char	*pcAddr		= NULL;
			char	*pcUser		= NULL;

			// ASCII versions of strings
			CCLTRYE ( strAddr.length() > 0, ERROR_INVALID_STATE );
			CCLTRY  ( strAddr.toAscii ( &pcAddr ) );
			if (hr == S_OK && strUser.length() > 0)
				hr = strUser.toAscii ( &pcUser );

			// Thread safety
			sshEnter();

			// New session object
			CCLTRYE ( (ps = ssh_new()) != NULL, E_OUTOFMEMORY );

			// Store in context
			CCLTRY ( pDct->store ( adtString(L"Session"), adtLong((U64)ps) ) );

			// Options
			CCLOK ( ssh_options_set ( ps, SSH_OPTIONS_HOST, pcAddr ); )
			if (hr == S_OK && pcUser != NULL)
			ssh_options_set ( ps, SSH_OPTIONS_USER, (pcUser != NULL) ? pcUser : "root" );
//			ssh_options_set ( ps, SSH_OPTIONS_USER, "root" );

			// Timeout for connection
			CCLOK ( ssh_options_set ( ps, SSH_OPTIONS_TIMEOUT, &(to = (iTo/1000)) ); )
			CCLOK ( ssh_options_set ( ps, SSH_OPTIONS_TIMEOUT_USEC, &(to = (iTo % 1000)*1000) ); )

			// Thread safety
			sshLeave();

			// Debug
//			CCLOK ( ssh_options_set(ps, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);)

			// Put the session into selected blocking mode
//			CCLOK ( ssh_set_blocking ( ps, (bBlocking) ? 1 : 0 ); )
//			lprintf ( LOG_DBG, L"pcUser %p pcAddr %p hr 0x%x\r\n", pcUser, pcAddr, hr );

			// Clean up
			_FREEMEM(pcUser);
			_FREEMEM(pcAddr);
			}	// if
		else if (hr == S_OK)
			{
			// Use existing session object
			CCLTRYE( (ps = (ssh_session)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );
			}	// else if

		// Connect ?
		if (hr == S_OK && pDct->load ( adtString(L"Connected"), vL ) != S_OK)
			{
			// Thread safety
			sshEnter();

			// Connect to other end
			// In non-blocking mode, will return EAGAIN when it needs to be called again.
//			lprintf ( LOG_DBG, L"ssh_connect ps %p hr 0x%x {\r\n", ps, hr );
			rc = ssh_connect(ps);
//			lprintf ( LOG_DBG, L"} ssh_connect rc %d\r\n", rc );
			if (rc != SSH_OK)
				lprintf ( LOG_DBG, L"%s:ssh_connect %d\r\n", (LPCWSTR)strnName, rc );

			// Thread safety
			sshLeave();

			// Store the socket file descriptor in dictionary for external use
//			CCLTRY ( pDct->store ( adtString(L"Socket"), adtLong(ssh_get_fd(ps)) ) );
//			lprintf ( LOG_DBG, L"%s:ssh_get_fd %d\r\n", (LPCWSTR)strnName, ssh_get_fd(ps) );

			// Success ?
			hr =	(rc == SSH_OK)		?	S_OK			:
					(rc == SSH_AGAIN) ?	SSH_AGAIN	: 
												E_UNEXPECTED;

			// Connected
			CCLTRY ( pDct->store ( adtString(L"Connected"), adtBool(true) ) );

			// Verify TODO: External input ?
//			if (hr == S_OK && (state = ssh_is_server_known(ps)) != SSH_SERVER_KNOWN_OK)
//				{
//				// Error ?
//				CCLTRYE ( (state != SSH_SERVER_ERROR), E_UNEXPECTED );
//
//				// Add server to known hosts
//				CCLOK ( ssh_write_knownhost(ps); )
//				}	// if

			}	// if

		// Authentication
//		lprintf ( LOG_DBG, L"hr 0x%x, pDctPrv %p\r\n", hr, pDctPrv );
		if (hr == S_OK && pDct->load ( adtString(L"Authorized"), vL ) != S_OK)
			{
			// Public key authentication ?
			if (pDctPrv != NULL)
				{
				IThis			*pKeyThis	= NULL;
				SshRef		*pKeyRef		= NULL;
				adtIUnknown unkV;
				adtValue		vL;

				// Extract private key from dictionary
				CCLTRY ( pDctPrv->load ( adtString(L"Key"), vL ) );
				CCLTRY ( _QISAFE((unkV = vL),IID_IThis,&pKeyThis) );
				CCLTRY ( pKeyThis->getThis ( (void **) &pKeyRef ) );
				CCLTRYE( pKeyRef->pKey != NULL, ERROR_INVALID_STATE );

				// Thread safety
				sshEnter();

				// Key pair authentication
				CCLTRYE ( (rc = ssh_userauth_publickey ( ps, NULL, pKeyRef->pKey )) 
								== SSH_AUTH_SUCCESS, rc);
				if (hr != S_OK)
					lprintf ( LOG_DBG, L"ssh_userauth_publickey failed %d\r\n", rc );

				// Thread safety
				sshLeave();

				// Automatic keypair authentications
//				CCLTRYE ( (rc = ssh_userauth_publickey_auto ( ps, NULL, NULL )) 
//								== SSH_AUTH_SUCCESS, rc);
				}	// if

			// If a username is specified, assume username/password
			else if (strUser.length() > 0)
				{
				char	*pcPass	= NULL;

				// ASCII versions
				CCLOK ( strPass.toAscii ( &pcPass ); )

				// Thread safety
				sshEnter();

				// Authenticate
				CCLTRYE ( (rc = ssh_userauth_password ( ps, NULL, pcPass ))
								== SSH_AUTH_SUCCESS, rc );
				if (hr != S_OK)
					lprintf ( LOG_DBG, L"ssh_userauth_password failed %d\r\n", rc );

				// Thread safety
				sshLeave();

				// Clean up
				_FREEMEM(pcPass);
				}	// if

			// Authorized
			CCLTRY ( pDct->store ( adtString(L"Authorized"), adtBool(true) ) );
//			if (rc != SSH_OK)
//				lprintf ( LOG_DBG, L"%s:ssh_user_auth_xxx %d\r\n", (LPCWSTR)strnName, rc );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Connect,adtIUnknown(pDct));
		else if (hr == SSH_AGAIN || hr == SSH_AUTH_AGAIN)
			_EMT(Pending,adtIUnknown(pDct));
		else
			{
			// Result
			_EMT(Error,adtInt(hr));

			// Clean up
			if (ps != NULL)
				onReceive(prDisconnect,v);
			}	// else if

		}	// if

	// Disconnect
	else if (_RCP(Disconnect))
		{
		ssh_session	ps			= NULL;
		char			*pcAddr	= NULL;
		adtValue		vL;

		// State check
//		lprintf ( LOG_DBG, L"Disconnect:pDct %p\r\n", pDct );
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Session
		CCLTRY ( pDct->load ( adtString(L"Session"), vL ) );
		CCLTRYE( (ps = (ssh_session)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Disconnect from other side
		CCLOK ( ssh_disconnect ( ps ); )

		// Thread safety
		sshLeave();

		// Clean up
		if (pDct != NULL)
			{
			pDct->remove ( adtString(L"Connected") );
			pDct->remove ( adtString(L"Authorized") );
			pDct->remove ( adtString(L"Session") );
			}	// if
		if (ps != NULL)
			ssh_free(ps);
//		lprintf ( LOG_DBG, L"Disconnect:pDct %p hr 0x%x\r\n", pDct, hr );
		}	// else if

	// State
	else if (_RCP(Session))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Private))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctPrv);
		_QISAFE(unkV,IID_IDictionary,&pDctPrv);
		}	// else if
	else if (_RCP(Address))
		hr = adtValue::toString(v,strAddr);
	else if (_RCP(Username))
		hr = adtValue::toString(v,strUser);
	else if (_RCP(Password))
		hr = adtValue::toString(v,strPass);
	else if (_RCP(Timeout))
		iTo = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

/*
	// Verify
	else if (_RCP(Verify))
		{
		ssh_session	ps	= NULL;
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Session
		CCLTRY ( pDct->load ( adtString(L"Session"), vL ) );
		CCLTRYE( (ps = (ssh_session)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Verify the host
		CCLOK ( ssh_write_knownhost(ps); )
		}	// else if
			// Need authentication ?
			if (state != SSH_SERVER_KNOWN_OK)
				{
				unsigned char	*hash	= NULL;
				char				*hex	= NULL;
				int				len	= 0;
				adtString		strHash;

				// Get the public key
				len = ssh_get_pubkey_hash ( ps, &hash );
				if (len > 0)
					strHash = ssh_get_hexa ( hash, len );

				// Verify public key
				_EMT(Verify,strHash);

				// Clean up
				_FREEMEM(hash);
				}	// if
*/
