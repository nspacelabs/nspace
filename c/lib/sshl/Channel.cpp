////////////////////////////////////////////////////////////////////////
//
//									CHANNEL.CPP
//
//				Implementation of the SSH channel node
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"

Channel :: Channel ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pSess			= NULL;
//	pStm			= NULL;
	pCh			= NULL;
//	iSz			= 0;
//	pcBfrRd		= NULL;
//	iBfrRd		= 0;
//	pcBfrWr		= NULL;
//	iBfrWr		= 0;
	bBlocking	= true;
	}	// Channel

HRESULT Channel :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load(adtString("Blocking"),vL) == S_OK)
			bBlocking = vL;
		}	// if

	// Detach
	else 
		{
		// Close current channel
		onReceive(prClose,adtInt());

		// Clean up
		_RELEASE(pCh);
		_RELEASE(pSess);
//		_RELEASE(pStm);
//		_FREEMEM(pcBfrRd);
//		_FREEMEM(pcBfrWr);
		}	// if

	return hr;
	}	// onAttach

HRESULT Channel :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open a channel session
	if (_RCP(Open))
		{
		ssh_session	ps	= NULL;
		ssh_channel	ch	= NULL;
		int			rc	= 0;
		adtValue		vL;

		// State check
		CCLTRYE ( pSess != NULL && pCh != NULL, ERROR_INVALID_STATE );

		// Session
		CCLTRY ( pSess->load ( adtString(L"Session"), vL ) );
		CCLTRYE( (ps = (ssh_session)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Does context already have a channel object ?
		if (hr == S_OK && pCh->load ( adtString(L"Channel"), vL ) != S_OK)
			{
			// Thread safety
			sshEnter();

			// Create and open a new channel
			CCLTRYE ( (ch = ssh_channel_new(ps)) != NULL, E_OUTOFMEMORY );

			// Store in context
			CCLTRY ( pCh->store ( adtString(L"Channel"), adtLong((U64)ch) ) );

			// Put the channel into the correct blocking mode
//			CCLOK ( ssh_channel_set_blocking ( ch, (bBlocking) ? 1 : 0 ); )

			// Thread safety
			sshLeave();
			}	// if

		// Is a remote address specified for port forwarding ?
		if (hr == S_OK && pCh->load ( adtString(L"AddressRemote"), vL ) == S_OK)
			{
			WCHAR			*pw			= NULL;
			char			*pcAddr		= NULL;
			char			*pcAddrLcl	= NULL;
			adtString	strAddr(vL);
			adtInt		iPort(0),iPortLcl(5555);

			// Address has the format <address>:<port>
			CCLTRYE( ((pw = wcschr ( &strAddr.at(), ':' )) != NULL), E_INVALIDARG );
			CCLOK  ( *pw = '\0'; )
			CCLTRY ( strAddr.toAscii ( &pcAddr ) );

			// Port
			CCLTRYE ( (iPort = adtString(pw+1)) > 0, E_INVALIDARG );

			// API claims the 'sourcehost' and 'localport' are "mostly"
			// for logging.  Allow it to be specified just in case.
			if (pSess->load ( adtString(L"AddressLocal"), vL ) == S_OK)
				{
				// Valid ?
				CCLTRYE ( (strAddr = vL).length() > 0, E_INVALIDARG );

				// Address has the format <address>:<port>
				CCLTRYE( ((pw = wcschr ( &strAddr.at(), ':' )) != NULL), E_INVALIDARG );
				CCLOK  ( *pw = '\0'; )
				CCLTRY ( strAddr.toAscii ( &pcAddrLcl ) );

				// Port
				CCLTRYE ( (iPortLcl = adtString(pw+1)) > 0, E_INVALIDARG );
				}	// if

			// Open
			if (hr == S_OK)
				{
				// Thread safety
				sshEnter();

				// Open port forward
				CCLTRYE ( (rc = ssh_channel_open_forward(ch,pcAddr,iPort,
								(pcAddrLcl != NULL) ? pcAddrLcl : "localhost", iPortLcl )) 
								== SSH_OK, rc );
//				if (rc != SSH_OK)
//					lprintf ( LOG_DBG, L"%s:ssh_channel_open_forward %d\r\n", (LPCWSTR)strnName, rc );

				// Thread safety
				sshLeave();

				// Success ?  Handle non-blocking results
				hr =	(rc == SSH_OK)		?	S_OK			:
						(rc == SSH_AGAIN) ?	SSH_AGAIN	: 
													E_UNEXPECTED;
				}	// if

			// Clean up
			_FREEMEM(pcAddrLcl);
			_FREEMEM(pcAddr);
			}	// if

		// The default behaviour is to open a sesstion when not port forwarding
		else if (hr == S_OK)
			{
			// Thread safety
			sshEnter();

			// Open a session suited for shell commands
			CCLTRYE ( (rc = ssh_channel_open_session(ch)) == SSH_OK, rc );

			// Thread safety
			sshLeave();

			// Success ?  Handle non-blocking results
			hr =	(rc == SSH_OK)		?	S_OK			:
					(rc == SSH_AGAIN) ?	SSH_AGAIN	: 
												E_UNEXPECTED;
			}	// else if

/*
		// DEBUG
		if (hr == S_OK)
			{
			struct		timeval	to	= { 1, 0 };
			for (int i = 0;i < 10;++i)
{
			ssh_channel	chs[2]		= { ch, NULL };
			int ret = ssh_channel_select ( chs, NULL, NULL, &to );
			lprintf ( LOG_DBG, L"ret %d chs[0] %p\r\n", ret, chs[0] );
}
			}	// if
*/
		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pCh));
		else if (hr == SSH_AGAIN)
			_EMT(Pending,adtIUnknown(pCh));
		else
			{
			// Result
			_EMT(Error,adtInt(hr));

			// Clean up
			if (ch != NULL)
				onReceive(prClose,v);
			}	// else

		}	// if

	// Close channel
	else if (_RCP(Close))
		{
		ssh_channel	ch	= NULL;
		adtValue		vL;

		// State check
//		lprintf ( LOG_DBG, L"Close:pCh %p\r\n", pCh );
		CCLTRYE ( pCh != NULL, ERROR_INVALID_STATE );

		// Now closed
		CCLOK ( _EMT(Close,adtIUnknown(pCh)); )

		// Clean up
		if (hr == S_OK && pCh->load ( adtString(L"Channel"), vL ) == S_OK)
			{
			// Thread safety
			sshEnter();

			// Channel context
			ch = (ssh_channel)(U64)adtLong(vL);
			ssh_channel_send_eof(ch);
			ssh_channel_close(ch);
			ssh_channel_free(ch);

			// Thread safety
			sshLeave();

			// Closed
			pCh->remove ( adtString(L"Channel") );
			}	// if
//		lprintf ( LOG_DBG, L"Close:pCh %p hr 0x%x\r\n", pCh, hr );
		}	// else if

	// Execute a single shell command
	else if (_RCP(Execute))
		{
		ssh_channel	ch			= NULL;
		char			*pcExec	= NULL;
		int			rc			= 0;
		adtValue		vL;
		adtString	strExec(v),strBfr,strRd;

		// State check
		CCLTRYE ( pCh != NULL, ERROR_INVALID_STATE );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_channel)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Execute the command
		CCLTRY ( strExec.toAscii(&pcExec) );
		CCLOK ( lprintf ( LOG_DBG, L"%s", (LPCWSTR)strExec ); )
		CCLTRYE ( (rc = ssh_channel_request_exec ( ch, pcExec )) == SSH_OK, rc );

		// Thread safety
		sshLeave();

		// Result
		if (hr == S_OK)
			_EMT(Execute,strExec);
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Create and attach a stream object to the channel for external use
	else if (_RCP(Stream))
		{
		ChannelStream	*pStm = NULL;
		IByteStream		*ps	= NULL;

		// State check
		CCLTRYE ( pCh != NULL, ERROR_INVALID_STATE );

		// Stream wrapper object
		CCLTRYE ( (pStm = new ChannelStream ( pCh )) != NULL, E_OUTOFMEMORY );
		_ADDREF(pStm);

		// Result
		if (hr == S_OK)
			_EMT(Stream,adtIUnknown((ps = pStm)));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pStm);
		}	// else if

	// Check if remote has sent an EOF
	else if (_RCP(Eof))
		{
		ssh_channel	ch			= NULL;
		adtValue		vL;

		// State check
		CCLTRYE ( pCh != NULL, ERROR_INVALID_STATE );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_channel)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Result
		if (hr == S_OK)
			_EMT(Eof,adtBool((ssh_channel_is_eof(ch)) ? true : false));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

/*
	// Read data from a channel
	else if (_RCP(Read))
		{
		ssh_channel	ch			= NULL;
		char			*pcExec	= NULL;
		int			rc			= 0;
		U32			szIo		= iSz;
		int			num;
		adtValue		vL;

		// State check
		CCLTRYE ( pCh != NULL && pStm != NULL, ERROR_INVALID_STATE );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_channel)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Ensure large enough read buffer
		if (hr == S_OK && szIo == 0)	szIo = 1024;
		if (hr == S_OK && szIo > iBfrRd)
			{
			// Resize
			CCLTRYE ( (pcBfrRd = (U8 *) _REALLOCMEM ( pcBfrRd, szIo )) != NULL, E_OUTOFMEMORY );
			CCLOK ( iBfrRd = szIo; )
			}	// if

		// Perform a non-blocking read on the channel to check for data
		if (hr == S_OK && 
				(num = ssh_channel_read_timeout ( ch, pcBfrRd, szIo, 0, 1000 )))
			{
			// Copy data into stream and send out result
			CCLTRY ( pStm->write ( pcBfrRd, num, 0 ) );
			CCLOK ( _EMT(Read,adtIUnknown(pStm)); )
			}	// if

		// Has remote side sent an EOF ?
		if (hr == S_OK)
			_EMT(Eof,adtBool((ssh_channel_is_eof(ch)) ? true : false));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Write data to channel
	else if (_RCP(Write))
		{
		ssh_channel	ch			= NULL;
		char			*pcExec	= NULL;
		int			rc			= 0;
		int			num;
		adtValue		vL;
		U64			nr;

		// State check
		CCLTRYE ( pCh != NULL && pStm != NULL, ERROR_INVALID_STATE );

		// For now size must be specified
		CCLTRYE ( iSz > 0, ERROR_INVALID_STATE );

		// Channel
		CCLTRY ( pCh->load ( adtString(L"Channel"), vL ) );
		CCLTRYE( (ch = (ssh_channel)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Ensure large enough write buffer
		if (hr == S_OK && iSz > iBfrWr)
			{
			// Resize
			CCLTRYE ( (pcBfrWr = (U8 *) _REALLOCMEM ( pcBfrWr, iSz )) != NULL, E_OUTOFMEMORY );
			CCLOK ( iBfrWr = iSz; )
			}	// if

		// Perform a non-blocking write on the channel 
		CCLTRY ( pStm->read ( pcBfrWr, iSz, &nr ) );
		CCLTRYE( (num = ssh_channel_write ( ch, pcBfrWr, iSz )) > 0, num );

		// Result
		if (hr == S_OK)
			_EMT(Write,adtInt(num));
		else 
			_EMT(Error,adtInt(hr));
		}	// else if
*/

	// State
	else if (_RCP(Session))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSess);
		_QISAFE(unkV,IID_IDictionary,&pSess);
		}	// else if
	else if (_RCP(Channel))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pCh);
		_QISAFE(unkV,IID_IDictionary,&pCh);
		}	// else if
/*
	else if (_RCP(Stream))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStm);
		_QISAFE(unkV,IID_IByteStream,&pStm);
		}	// else if
	else if (_RCP(Size))
		iSz = adtInt(v);
*/
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

