////////////////////////////////////////////////////////////////////////
//
//									KEY.CPP
//
//				Implementation of the SSH key node
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"

Key :: Key ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct			= NULL;
	}	// Key

HRESULT Key :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load(adtString("Algorithm"),vL) == S_OK)
			adtValue::toString ( vL, strAlgor );
		if (pnDesc->load(adtString("Size"),vL) == S_OK)
			iSz = vL;
		if (pnDesc->load(adtString("Location"),vL) == S_OK)
			onReceive(prLocation,vL);
		}	// if

	// Detach
	else
		{
		_RELEASE(pDct);
		}	// if

	return S_OK;
	}	// onAttach

HRESULT Key :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Create a private/public key pair
	if (_RCP(Create))
		{
		SshRef			*pRefPrv	= NULL;
		IThis				*pThis	= NULL;
		ssh_keytypes_e type		=
			(!WCASECMP(strAlgor,L"RSA"))						?	SSH_KEYTYPE_RSA :
			(!WCASECMP(strAlgor,L"DSA"))						?	SSH_KEYTYPE_ECDSA : // Is this right ?
			(!WCASECMP(strAlgor,L"ECDSA") && iSz == 256)	?	SSH_KEYTYPE_ECDSA_P256 :
			(!WCASECMP(strAlgor,L"ECDSA") && iSz == 384)	?	SSH_KEYTYPE_ECDSA_P384 :
			(!WCASECMP(strAlgor,L"ECDSA") && iSz == 521)	?	SSH_KEYTYPE_ECDSA_P521 :
			(!WCASECMP(strAlgor,L"ED25519"))					?	SSH_KEYTYPE_ED25519 : 
																			SSH_KEYTYPE_UNKNOWN;
		int				rc;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( type != SSH_KEYTYPE_UNKNOWN, ERROR_INVALID_STATE );
		CCLTRYE ( iSz > 0, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Create the private key
		CCLTRYE ( (pRefPrv = new SshRef()) != NULL, E_OUTOFMEMORY );
		CCLTRYE ( (rc = ssh_pki_generate ( type, iSz, &(pRefPrv->pKey) ))
						== SSH_OK, E_UNEXPECTED );
		CCLTRY ( pDct->store ( adtString("Key"), adtIUnknown((pThis = pRefPrv)) ) );

		// Thread safety
		sshLeave();

		// Result
		if (hr == S_OK)
			_EMT(Create,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Generate public key from private
	else if (_RCP(Public))
		{
		IDictionary	*pDctPub		= NULL;
		SshRef		*pRefPub		= NULL;
		SshRef		*pRefPrv		= NULL;
		IThis			*pThisPrv	= NULL;
		adtIUnknown unkV(v);
		adtValue		vL;
		int			rc;

		// Public key dictionary
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctPub) );

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pDct->load ( adtString(L"Key"), vL ) );
		CCLTRY ( _QISAFE((unkV = vL),IID_IThis,&pThisPrv) );
		CCLTRY ( pThisPrv->getThis ( (void **) &pRefPrv ) );
		CCLTRYE( pRefPrv->pKey != NULL, ERROR_INVALID_STATE );

		// Thread safety
		sshEnter();

		// Generate public key from the private key
		CCLTRYE ( (pRefPub = new SshRef()) != NULL, E_OUTOFMEMORY );
		CCLTRYE ( (rc = ssh_pki_export_privkey_to_pubkey ( pRefPrv->pKey,
						&(pRefPub->pKey) ) == SSH_OK), E_UNEXPECTED );
		CCLTRY ( pDctPub->store ( adtString("Key"), adtIUnknown((IThis *)pRefPub) ) );

		// Thread safety
		sshLeave();

		// Result
		if (hr == S_OK)
			_EMT(Public,v);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRefPub);
		_RELEASE(pThisPrv);
		_RELEASE(pDctPub);
		}	// else if

	// Export key
	else if (_RCP(Export))
		{
		IThis			*pKeyThis	= NULL;
		SshRef		*pKeyRef		= NULL;
		char			*pcLoc		= NULL;
		adtIUnknown unkV;
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );
		CCLTRY ( pDct->load ( adtString(L"Key"), vL ) );
		CCLTRY ( _QISAFE((unkV = vL),IID_IThis,&pKeyThis) );
		CCLTRY ( pKeyThis->getThis ( (void **) &pKeyRef ) );
		CCLTRYE( pKeyRef->pKey != NULL, ERROR_INVALID_STATE );

		// ASCII version of location for API
		CCLTRY ( strLoc.toAscii ( &pcLoc ) );

		// Thread safety
		sshEnter();

		// Different export functions based on key type
		if (hr == S_OK && ssh_key_is_private ( pKeyRef->pKey ))
			{
			// Save key to file
			CCLTRYE ( ssh_pki_export_privkey_file ( pKeyRef->pKey, NULL,
							NULL, NULL, pcLoc ) == SSH_OK, E_UNEXPECTED );
			}	// if
		else if (hr == S_OK && ssh_key_is_public ( pKeyRef->pKey ))
			{
			// Save key to file
			CCLTRYE ( ssh_pki_export_pubkey_file ( pKeyRef->pKey, 
							pcLoc ) == SSH_OK, E_UNEXPECTED );
			}	// else if

		// Thread safety
		sshLeave();

		// Result
		if (hr == S_OK)
			_EMT(Export,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcLoc);
		_RELEASE(pKeyThis);
		}	// else if

	// Import key
	else if (_RCP(Import))
		{
		SshRef		*pKeyRef		= NULL;
		char			*pcLoc		= NULL;
		adtIUnknown unkV;
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// Create reference holder for key
		CCLTRYE ( (pKeyRef = new SshRef()) != NULL, E_OUTOFMEMORY );
		CCLTRY ( pDct->store ( adtString("Key"), adtIUnknown((IThis *)pKeyRef)) );

		// ASCII version of location for API
		CCLTRY ( strLoc.toAscii ( &pcLoc ) );

		// Thread safety
		sshEnter();

		// Import key
		if (hr == S_OK)
			{
			// First, attempt to import as private key
			CCLTRYE ( ssh_pki_import_privkey_file ( pcLoc, NULL,
							NULL, NULL, &(pKeyRef->pKey) ) == SSH_OK, E_UNEXPECTED );

			// Failed as private ?
			if (hr != S_OK)
				{
				// If it failed, try as a public key
				hr = S_OK;
				CCLTRYE ( ssh_pki_import_pubkey_file ( pcLoc, &(pKeyRef->pKey) )
								== SSH_OK, E_UNEXPECTED );
				}	// if

			}	// if

		// Thread safety
		sshLeave();

		// Result
		if (hr == S_OK)
			_EMT(Import,adtIUnknown(pDct));
		else
			{
			if (pDct != NULL)
				pDct->remove ( adtString(L"Key") );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_FREEMEM(pcLoc);
		_RELEASE(pKeyRef);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Location))
		hr = adtValue::toString ( v, strLoc );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
