////////////////////////////////////////////////////////////////////////
//
//									ChannelStream.CPP
//
//					Byte stream wrapper for an SSH channel.
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"
#include <stdio.h>

// Definitions

ChannelStream :: ChannelStream ( IDictionary *_pDctCh )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	_pDctCh is the channel context
	//
	////////////////////////////////////////////////////////////////////////
	pDctCh	= _pDctCh; _ADDREF(pDctCh);
	}	// ChannelStream

void ChannelStream :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pDctCh);
	}	// destruct

HRESULT ChannelStream :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr				= S_OK;
	ssh_channel	chs[2]		= { NULL, NULL };
	struct		timeval	to	= { 0, 0 };
	int			ret			= 0;
	adtValue		vL;

	// Channel context
	CCLTRY ( getCh ( &(chs[0]) ) );

	// Thread safety
	sshEnter();

	// Is basically a socket so perform a 'select' and if reading is available
	// there is at least one byte available
	CCLTRYE ( (ret = ssh_channel_select ( chs, NULL, NULL, &to )) == SSH_OK,
					E_UNEXPECTED );

	// Thread safety
	sshLeave();

	// Is channel readable ?
	(*puAv) = (hr == S_OK && chs[0] != NULL) ? 1 : 0;

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"available failed ret %d hr 0x%x\r\n", ret, hr );

	return S_OK;
	}	// available

HRESULT ChannelStream :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U8				*fp	= NULL;
	U64			nleft	= 0;
	U8				cpybufr[4096];
	U64			nio,nw,nr;

	// State check
	CCLTRYE	( uSz != 0, ERROR_INVALID_STATE );
 
	// Setup
	if (puSz != NULL)
		*puSz	= 0;

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK	( nio = (sizeof(cpybufr) < uSz) ? sizeof(cpybufr) : uSz; )
		CCLTRY( read ( cpybufr, nio, &nr ) );

		// Write full block to stream
		CCLOK ( fp		= cpybufr; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;

		// If at end of file before request has been satisfied, stop
		if (uSz && (nr < nio))
			break;
		}	// while

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"copyTo:Failed:(%p,%d):0x%x\r\n", pStmDst, uSz, hr );

	return hr;
	}	// copyTo

HRESULT ChannelStream :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr	= S_OK;
	ssh_channel		ch;

	// Channel context
	CCLTRY ( getCh ( &ch ) );

	// Thread safety
	sshEnter();

	// Flush
	CCLTRYE( ssh_blocking_flush ( ssh_channel_get_session ( ch ), 1000 ) !=
				SSH_ERROR, E_UNEXPECTED );

	// Thread safety
	sshLeave();

	return hr;
	}	// flush

HRESULT ChannelStream :: getCh ( ssh_channel *pch )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Obtain the embedded channel structure.
	//
	//	PARAMETERS
	//		-	pch will receive the chanenl
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtValue	vL;

	// State check
	CCLTRYE ( pDctCh != NULL, ERROR_INVALID_STATE );

	// Embedded state
	CCLTRY ( pDctCh->load ( adtString(L"Channel"), vL ) );

	// Resulting channel
	CCLOK ( (*pch) = (ssh_channel) (U64) adtLong (vL); )

	return hr;
	}	// getCh

HRESULT ChannelStream :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr	= S_OK;
	U32				nr	= 0;
	ssh_channel		ch;
//	U64				av;

	// DEBUG
//	lprintf ( LOG_DBG, L"pvBfr %p nio %d pnio %p\r\n", pvBfr, nio, pnio );

	// Channel context
	CCLTRY ( getCh ( &ch ) );

	// Ensure there is at least 1 byte available
//	CCLTRY ( available ( &av ) );
//	CCLTRYE( av > 0, ERROR_TIMEOUT );

	// Thread safety
	sshEnter();

	// Read next block of data
//	CCLTRYE	( (nr = ssh_channel_read ( ch, pvBfr, (uint32_t) nio, 0 )) 
	CCLTRYE	( (nr = ssh_channel_read_timeout ( ch, pvBfr, (uint32_t) nio, 0, 1000 )) 
						!= SSH_ERROR, E_UNEXPECTED );

	// Merge stderr with stdout ?  This is necessary because the call to
	// 'ssh_channel_is_eof' will return false if there is any data left
	// in either buffer even if the other side has EOF'd.
	// Anything from the stderr buffer ?
	if (hr == S_OK && nr == 0)
		{
		// Read from the stderr buffer.
		CCLTRYE	( (nr = ssh_channel_read_timeout ( ch, pvBfr, (uint32_t) nio, 1, 1000 )) 
						!= SSH_ERROR, E_UNEXPECTED );
		if (hr == S_OK && nr > 0)
			lprintf ( LOG_DBG, L"stderr buffer had %d bytes\r\n", nr );
		}	// if

	// Thread safety
	sshLeave();

	// Results
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	// Using socket logic now since it underlies libssh
	// A readable socket that returns 0 bytes means socket has been closed gracefully
	if (hr == S_OK && nr == 0)
		hr = WSAECONNRESET;

	// Debug
//	if (hr != S_OK)
//		lprintf ( LOG_DBG, L"Result:%d,0x%x\r\n", nr, hr );

	// DEBUG
//	lprintf ( LOG_DBG, L"hr 0x%x pnio %d\r\n", hr, nr );

	return hr;
	}	// read

HRESULT ChannelStream :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Not supported on a socket
	return E_NOTIMPL;
	}	// seek

HRESULT ChannelStream :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// A socket can be thought of has having any amount of data
	return S_OK;
	}	// setSize

HRESULT ChannelStream :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U32			nw;
	ssh_channel	ch;

	// DEBUG
//	lprintf ( LOG_DBG, L"pcvBfr %p nio %d pnio %p\r\n", pcvBfr, nio, pnio );

	// Channel context
	CCLTRY ( getCh ( &ch ) );

	// Thread safety
	sshEnter();

	// Attempt write
	CCLTRYE	( (nw = ssh_channel_write ( ch, pcvBfr, (uint32_t) nio )) 
						!= SSH_ERROR, E_UNEXPECTED );

	// Thread safety
	sshLeave();

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nw;

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	// DEBUG
//	lprintf ( LOG_DBG, L"hr 0x%x pnio %d\r\n", hr, nw );

	return hr;
	}	// write

