////////////////////////////////////////////////////////////////////////
//
//									SshRef.CPP
//
//				Implementation of the ssh reference node
//
////////////////////////////////////////////////////////////////////////

#include "sshl_.h"

SshRef :: SshRef ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pSess		= NULL;
	pCh		= NULL;
	pKey		= NULL;

	// So creators do not have to do the initial AddRef
	AddRef();
	}	// SshRef

SshRef :: SshRef ( ssh_key _pKey ) : SshRef()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	_pKey is the object to reference
	//
	////////////////////////////////////////////////////////////////////////
	pKey	= _pKey;
	}	// SshRef

SshRef :: SshRef ( ssh_session _pSess ) : SshRef()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	_pSess is the object to reference
	//
	////////////////////////////////////////////////////////////////////////
	pSess	= _pSess;
	}	// SshRef

SshRef :: SshRef ( ssh_channel _pCh ) : SshRef()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	_pCh is the object to reference
	//
	////////////////////////////////////////////////////////////////////////
	pCh = _pCh;
	}	// SshRef

void SshRef :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////

	// Thread safety
	sshEnter();

	// Clean up
	if (pKey != NULL)
		{
		ssh_key_free ( pKey );
		pKey = NULL;
		}	// if
	if (pCh != NULL)
		{
		ssh_channel_send_eof ( pCh );
		ssh_channel_close ( pCh );
		ssh_channel_free ( pCh );
		pCh = NULL;
		}	// if
	if (pSess != NULL)
		{
		ssh_disconnect ( pSess );
		ssh_free ( pSess );
		pSess = NULL;
		}	// if

	// Thread safety
	sshLeave();

	}	// destruct

//
// Utility functions
//

static sysCS csSsh;

HRESULT sshEnter ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Enter an SSH calling sequence.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return (csSsh.enter()) ? S_OK : S_FALSE;
	}	// sshEnter

HRESULT sshLeave ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Leave an SSH calling sequence.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return (csSsh.leave()) ? S_OK : S_FALSE;
	}	// sshLeave


