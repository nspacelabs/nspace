////////////////////////////////////////////////////////////////////////
//
//									TemporalSql.CPP
//
//					Implementation of the temporal SQL node
//
////////////////////////////////////////////////////////////////////////

#include "sqll_.h"
#include <stdio.h>

// SQLite DLL
SQLiteDll	sqliteDll ( L"sqlite3.dll" );

// Table names
#define	TABLE_LOCATIONS	"nLocations"
#define	TABLE_VALUES		"nValues"

TemporalSql :: TemporalSql ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pDb		= NULL;
	pPerWr	= NULL;
	pMemWr	= NULL;
	pStmWr	= NULL;
	pPerRd	= NULL;
	pMemRd	= NULL;
	pStmRd	= NULL;
	nLocs		= 0;
	nSeqNum	= 0;

	// Statements
	stmtIns	= NULL;
	stmtLoc	= NULL;
	stmtLd	= NULL;
	}	// TemporalSql

int TemporalSql :: cbCount ( void *pv, int nc, char **pstr, char **pnames )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Query callback function for counting records in a table.
	//
	//	PARAMETERS
	//		-	pv is a ptr. to an integer
	//		-	nc is the number of columns in results
	//		-	pstr is the list of column values
	//		-	pnames is the list of column names
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Slow but only returning counts infrequently
	adtString	strCnt	= pstr[0];
	*(U32 *)(pv)			= adtInt(strCnt);

	// Continue
	return 0;
	}	// cbTables

int TemporalSql :: cbTables ( void *pv, int nc, char **pstr, char **pnames )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Query callback function for retrieving the list of tables.
	//
	//	PARAMETERS
	//		-	pv is a dictionary interface
	//		-	nc is the number of columns in results
	//		-	pstr is the list of column values
	//		-	pnames is the list of column names
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	IDictionary *pTbls	= (IDictionary *)pv;
	adtString	strName	= pstr[0];

	// Add to dictionary
	if (pTbls != NULL)
		pTbls->store ( strName, adtInt(0) );

	// Continue
	return 0;
	}	// cbTables

HRESULT TemporalSql :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Close the resource.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (stmtIns != NULL)
		{
		sqliteDll.sqlite3_finalize(stmtIns);
		stmtIns = NULL;
		}	// if
	if (stmtLd != NULL)
		{
		sqliteDll.sqlite3_finalize(stmtLd);
		stmtLd = NULL;
		}	// if
	if (stmtLoc != NULL)
		{
		sqliteDll.sqlite3_finalize(stmtLoc);
		stmtLoc = NULL;
		}	// if
	if (pDb != NULL)
		{
		sqliteDll.sqlite3_close(pDb);
		pDb = NULL;
		}	// if

	return S_OK;
	}	// close

HRESULT TemporalSql :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;

	// Parser and stream for values
	CCLTRY ( COCREATE(L"Io.StmPrsBin",IID_IStreamPersist,&pPerWr) );
	CCLTRY ( COCREATE(L"Io.MemoryBlock",IID_IMemoryMapped,&pMemWr) );
	CCLTRY ( pMemWr->stream ( &pStmWr ) );

	CCLTRY ( COCREATE(L"Io.StmPrsBin",IID_IStreamPersist,&pPerRd) );
	CCLTRY ( COCREATE(L"Io.MemoryBlock",IID_IMemoryMapped,&pMemRd) );
	CCLTRY ( pMemRd->stream ( &pStmRd ) );

	return hr;
	}	// construct

void TemporalSql :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	close();
	_RELEASE(pStmRd);
	_RELEASE(pMemRd);
	_RELEASE(pPerRd);
	_RELEASE(pStmWr);
	_RELEASE(pMemWr);
	_RELEASE(pPerWr);
	}	// destruct

HRESULT TemporalSql :: getResId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Return an identifier for the resource.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return adtValue::copy ( adtLong((U64)pDb), vId );
	}	// getResId

HRESULT TemporalSql :: load ( const ADTVALUE &vKey, ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IDictionary
	//
	//	PURPOSE
	//		-	Loads a value from the dictionary with the given key.
	//
	//	PARAMETERS
	//		-	vKey is the key
	//		-	vValue will receive the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	void				*pvMem	= NULL;
	U32				sz			= 0;
	U64				id;
	int				ret;
	adtString		strLoc(vKey);

	// First step is to get the Id for the specified key/location
	CCLTRY ( locId ( strLoc, false, &id ) );

	// Precompiled statement
	if (hr == S_OK && stmtLd == NULL)
		{
		// Generate the query command for the value, for now always take the latest value
		strcpy_s ( cQryBfr, "SELECT Value FROM " TABLE_VALUES " WHERE IdPath = ? "
									"ORDER BY Sequence DESC" );

		// Prepare query
		if (hr == S_OK)
			hr = ((ret = sqliteDll.sqlite3_prepare_v2 ( pDb, cQryBfr, -1, 
					&stmtLd, NULL )) == SQLITE_OK) ? S_OK : E_UNEXPECTED;
		}	// if

	// Bind Id to query
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_bind_int ( stmtLd, 1, (U32)id )
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	// Found ?
	if (hr == S_OK)
		{
		// Attempt stepping to first record
		hr = ((ret = sqliteDll.sqlite3_step(stmtLd)) == SQLITE_ROW) ? S_OK : E_UNEXPECTED;

		// If 'done' there were no records
		if (hr != S_OK) 
			hr = (ret == SQLITE_DONE) ? ERROR_NOT_FOUND : E_UNEXPECTED;
		}	// if

	// There should be a single column of type blob
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_column_count(stmtLd) == 1 &&
				sqliteDll.sqlite3_column_type(stmtLd,0) == SQLITE_BLOB) ? S_OK : E_UNEXPECTED;

	// Obtain the size of blob
	CCLTRYE ( (sz = sqliteDll.sqlite3_column_bytes(stmtLd,0)) > 0, E_UNEXPECTED );

	// Read into buffer
	CCLTRY ( pMemRd->setSize ( sz ) );
	CCLTRY ( pMemRd->getInfo ( &pvMem, NULL ) );
	CCLOK  ( memcpy ( pvMem, sqliteDll.sqlite3_column_blob(stmtLd,0), sz ); )

	// Parse the nSpace value from the blob
	CCLTRY ( pStmRd->seek ( 0, STREAM_SEEK_SET, NULL ) );
	CCLTRY ( pPerRd->load ( pStmRd, vValue ) );

	// Clean up
	if (stmtLd != NULL)
		{
		sqliteDll.sqlite3_clear_bindings(stmtLd);
		sqliteDll.sqlite3_reset(stmtLd);
		}	// if

	return hr;
	}	// load

HRESULT TemporalSql :: locId ( const WCHAR *pwLoc, bool bCreate, U64 *pId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Load/create an id for the specified location.
	//
	//	PARAMETERS
	//		-	pwLoc is the location
	//		-	bCreate is true to create the Id if it missing
	//		-	pId will receive the Id
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	char				*errmsg	= NULL;
	int				ret;

	// Precompiled statement
	if (hr == S_OK && stmtLoc == NULL)
		{
		adtString		strQry;

		// Generate query string to retrieve the Id for the path
		strcpy_s ( cQryBfr, "SELECT Id FROM " TABLE_LOCATIONS " WHERE Path = ? COLLATE NOCASE;" );

		// Prepare query
		if (hr == S_OK)
			hr = ((ret = sqliteDll.sqlite3_prepare_v2 ( pDb, cQryBfr, -1, 
					&stmtLoc, NULL )) == SQLITE_OK) ? S_OK : E_UNEXPECTED;
		}	// if

	// Bind location to query
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_bind_text16 ( stmtLoc, 1, pwLoc, -1, NULL )
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	// Found ?
	if (hr == S_OK)
		{
		// Attempt stepping to first record
		hr = ((ret = sqliteDll.sqlite3_step(stmtLoc)) == SQLITE_ROW) ? S_OK : E_UNEXPECTED;

		// If 'done' there were no records
		if (hr != S_OK) 
			hr = (ret == SQLITE_DONE) ? ERROR_NOT_FOUND : E_UNEXPECTED;
		}	// if

	// There should be a single column of type integer (for the Id)
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_column_count(stmtLoc) == 1 &&
				sqliteDll.sqlite3_column_type(stmtLoc,0) == SQLITE_INTEGER) ? S_OK : E_UNEXPECTED;

	// Found, grab Id
	if (hr == S_OK)
		(*pId) = sqliteDll.sqlite3_column_int64(stmtLoc,0);

	// Create if not found and requested 
	else if (hr == ERROR_NOT_FOUND && bCreate)
		{
		char			*pcQry	= NULL;

		// Generate insertion command
		sprintf_s ( cQryBfr, "INSERT INTO " TABLE_LOCATIONS " VALUES ( '%d' , '%S' );",
						nLocs, pwLoc );

		// Associate path with new Id
		hr = ((ret = sqliteDll.sqlite3_exec ( pDb, cQryBfr, NULL, NULL, &errmsg ))
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

		// Insert successful, next Id
		if (hr == S_OK)
			*pId = (U32)nLocs++;
		}	// if

	// Clean up
	if (stmtLoc != NULL)
		{
		sqliteDll.sqlite3_clear_bindings(stmtLoc);
		sqliteDll.sqlite3_reset(stmtLoc);
		}	// if

	return hr;
	}	// locId

HRESULT TemporalSql :: open ( IDictionary *pOpts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Open a byte stream on top of a file.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the file.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IDictionary	*pTbls	= NULL;
	char			*pcLoc	= NULL;
	int			flags		= 0;
	char			*errmsg	= NULL;
	adtBool		bRO(true),bCr(false);
	adtString	strFile;
	adtValue		vL;

	// State check
	CCLTRYE ( pDb == NULL, ERROR_INVALID_STATE );

	// Options
	CCLTRY ( pOpts->load(adtString(L"Location"),vL) );
	CCLTRYE( (strFile = vL).length() > 0, E_INVALIDARG );

	// Need ASCII version of location
	CCLTRY ( strFile.toAscii ( &pcLoc ) );

	// Read only access ?
	if (hr == S_OK && pOpts->load ( adtString(L"ReadOnly"), vL ) == S_OK)
		bRO = vL;

	// Create if it does not exist ?
	if (hr == S_OK && pOpts->load ( adtString(L"Create"), vL ) == S_OK)
		bCr = vL;

	// Flags
	if (hr == S_OK)
		{
		flags = (bRO) ? SQLITE_OPEN_READONLY : SQLITE_OPEN_READWRITE;
		if (flags && !bRO)
			flags |= SQLITE_OPEN_CREATE;
		}	// if

	// Access database
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_open_v2 ( pcLoc, &pDb, flags, NULL ) 
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	//
	// Verify required tables exist
	//

	// Create dictionary to receive tables
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pTbls ) );

	// Retrieve the list of tables in the database
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_exec ( pDb,
				"SELECT name FROM sqlite_master WHERE type='table' ORDER BY NAME;",
				cbTables, pTbls, &errmsg )) == SQLITE_OK ? S_OK : E_UNEXPECTED;

	// Table for mapping locations to index/Id
	if (hr == S_OK && pTbls->load ( adtString(TABLE_LOCATIONS), vL ) != S_OK)
		{
		// Create new table
		lprintf ( LOG_INFO, L"Creating table %S\r\n", TABLE_LOCATIONS );
		hr = (sqliteDll.sqlite3_exec ( pDb,
				"CREATE TABLE " TABLE_LOCATIONS " ( Id INTEGER, Path TEXT UNIQUE );",
				NULL, NULL, &errmsg )) == SQLITE_OK ? S_OK : E_UNEXPECTED;
		}	// if

	// Table for storing values
	if (hr == S_OK && pTbls->load ( adtString(TABLE_VALUES), vL ) != S_OK)
		{
		// Create new table
		lprintf ( LOG_INFO, L"Creating table %S\r\n", TABLE_VALUES );
		hr = (sqliteDll.sqlite3_exec ( pDb,
				"CREATE TABLE " TABLE_VALUES " ( Sequence INTEGER UNIQUE, IdPath INTEGER, Date REAL, Value BLOB );",
				NULL, NULL, &errmsg )) == SQLITE_OK ? S_OK : E_UNEXPECTED;
		}	// if

	//
	// Obtain current information from Db for run-time operation.
	//

	// Retrieve count of records in both tables to know what the next Id should be
	// if needed.  Our counts are 1-based

	// Locations
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_exec ( pDb,
				"SELECT Count(*) FROM " TABLE_LOCATIONS, cbCount, &nLocs, &errmsg ))
				== SQLITE_OK ? S_OK : E_UNEXPECTED;
	if (hr == S_OK) ++nLocs;
		
	// Values
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_exec ( pDb,
				"SELECT Count(*) FROM " TABLE_VALUES, cbCount, &nSeqNum, &errmsg ))
				== SQLITE_OK ? S_OK : E_UNEXPECTED;
	if (hr == S_OK) ++nSeqNum;

	// Necessary for any reasonable amount of speed.
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_exec ( pDb, "PRAGMA synchronous = OFF", NULL, NULL, &errmsg )) 
				== SQLITE_OK ? S_OK : E_UNEXPECTED;
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_exec ( pDb, "PRAGMA journal_mode = MEMORY", NULL, NULL, &errmsg )) 
				== SQLITE_OK ? S_OK : E_UNEXPECTED;

	// Clean up
	_RELEASE(pTbls);
	_FREEMEM(pcLoc);

	return hr;
	}	// open

HRESULT TemporalSql :: store (	const ADTVALUE &vKey, 
											const ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IDictionary
	//
	//	PURPOSE
	//		-	Stores a value in the dictionary with the given key.
	//
	//	PARAMETERS
	//		-	vKey is the key
	//		-	vValue is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	void				*pvMem	= NULL;
	U64				id			= 0;
	U64				sz;
	int				ret;
	adtString		strLoc(vKey);
	adtDate			dNow;

	// Timestamp request
	dNow.now();

	// First step is to get the Id for the specified key/location
	// Skip leading slash
	CCLTRY ( locId ( (strLoc[0] != '/') ? strLoc : &strLoc[1], true, &id ) );

	// Persist the value into a blob and retrieve size
	CCLTRY ( pStmWr->seek(0,STREAM_SEEK_SET,NULL) );
	CCLTRY ( pPerWr->save(pStmWr, vValue) );
	CCLTRY ( pStmWr->seek(0,STREAM_SEEK_CUR,&sz) );

	// Lock down bits for writing
	CCLTRY ( pMemWr->getInfo ( &pvMem, NULL ) );

	// Precompiled statement
	if (hr == S_OK && stmtIns == NULL)
		{
		CCLOK ( sprintf_s ( cQryBfr, "INSERT INTO " TABLE_VALUES " VALUES ( ?, ?, ?, ? );" ); )
		if (hr == S_OK)
			hr = (sqliteDll.sqlite3_prepare_v2 ( pDb, cQryBfr, -1, &stmtIns, NULL ))
					== SQLITE_OK ? S_OK : E_UNEXPECTED;
		}	// if

	// Sequence number
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_bind_int ( stmtIns, 1, nSeqNum )
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	// Path Id
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_bind_int ( stmtIns, 2, (U32)id )
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	// Bind date directly to double
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_bind_double ( stmtIns, 3, dNow.vdbl )
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	// Bind blob memory directly
	if (hr == S_OK)
		hr = (sqliteDll.sqlite3_bind_blob ( stmtIns, 4, pvMem, (S32)sz, NULL )
				== SQLITE_OK) ? S_OK : E_UNEXPECTED;

	// Perform operation for next record
	if (hr == S_OK)
		hr = ((ret = sqliteDll.sqlite3_step(stmtIns)) == SQLITE_DONE) ? S_OK : E_UNEXPECTED;

	// Next sequence
	if (hr == S_OK)
		++nSeqNum;

	// Clean up
	if (stmtIns != NULL)
		{
		sqliteDll.sqlite3_clear_bindings(stmtIns);
		sqliteDll.sqlite3_reset(stmtIns);
		}	// if

	return hr;
	}	// store

//
// Contained functions
//

HRESULT TemporalSql :: clear ( void )
	{
	return E_NOTIMPL;
	}	// clear

HRESULT TemporalSql :: copyTo ( IContainer *pCont )
	{
	return E_NOTIMPL;
	}	// copyTo

HRESULT TemporalSql :: isEmpty ( void )
	{
	return E_NOTIMPL;
	}	// isEmpty

HRESULT TemporalSql :: iterate ( IIt **ppIt )
	{
	return E_NOTIMPL;
	}	// iterate

HRESULT TemporalSql :: keys ( IIt **ppKeys )
	{
	return E_NOTIMPL;
	}	// keys

HRESULT TemporalSql :: remove ( const ADTVALUE &v )
	{
	return E_NOTIMPL;
	}	// remove

HRESULT TemporalSql :: size ( U32 *s )
	{
	return E_NOTIMPL;
	}	// size

