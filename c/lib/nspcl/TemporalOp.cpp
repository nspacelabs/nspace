////////////////////////////////////////////////////////////////////////
//
//								TEMPORAL.CPP
//
//					Implementation of the temporal node.
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"

TemporalOp :: TemporalOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	strLoc		= L"";
	strLocF		= L"";
	pLocPar		= NULL;
	bRead			= true;
	pTmpl			= NULL;
	pTmplLocs	= NULL;
	pOpts			= NULL;
	pItLoc		= NULL;
	bFrom			= true;
	}	// Temporal

HRESULT TemporalOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when Temporal behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	adtValue		v;

	// Attach
	if (bAttach)
		{
		adtIUnknown	unkV;

		// Attributes
		if (pnDesc->load(adtString(L"Location"),v) == S_OK)
			strLoc = v;
		if (pnDesc->load(adtString(L"LocationFrom"),v) == S_OK)
			strLocF = v;
		if (pnDesc->load(adtString(L"ReadOnly"),v) == S_OK)
			bRead = adtBool(v);
		if (pnDesc->load(adtString(L"From"),v) == S_OK)
			bFrom = adtBool(v);

		// Options dictionary for general use
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pOpts ) );

		// Obtain reference to parent location
		CCLTRY ( pnLoc->load ( strnRefPar, v ) );
		CCLTRY ( _QISAFE((unkV=v),IID_IDictionary,&pLocPar) );
		if (hr == S_OK) pLocPar->Release();

		// Default to internal temporal database used by the namespace
//		CCLTRY ( pnSpc->temporal ( &pTmplLocs ) );
//		CCLTRY ( _QISAFE(pTmplLocs,IID_ITemporal,&pTmpl) );
		}	// if

	// Detach
	else
		{
		// Clean up
		pLocPar = NULL;
		_RELEASE(pOpts);
		_RELEASE(pTmpl);
		_RELEASE(pTmplLocs);
		_RELEASE(pItLoc);
		}	// else

	return hr;
	}	// onAttach

HRESULT TemporalOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IReceptor
	//
	//	PURPOSE
	//		-	A location has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	pl is the location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Turn on/off recording of path
	if (_RCP(Record))
		{
		adtString	strLocAbs,strLocAbsF;
		adtBool		bRec(v);
		adtString	strRec(v);

		// State check
		CCLTRYE ( pTmpl != NULL && strLoc.length() > 0,	ERROR_INVALID_STATE );

		// Ensure an absolute namespace is used
		CCLTRY ( nspcPathTo ( pLocPar, strLoc, strLocAbs ) );

		// Graph user can specify an alternative root 'from' which temporal
		// recordings previously occured.  This is useful for looking
		// into the past.
		if (hr == S_OK && strLocF.length() > 0)
			hr = nspcPathTo ( pLocPar, strLocF, strLocAbsF );

		// Allow for an On/Off string to be specified
		if (hr == S_OK && adtValue::type(v) == VTYPE_STR)
			bRec = (!WCASECMP(strRec,L"On") || !WCASECMP(strRec,L"true")) ? true : false;

		// Record
		CCLOK ( lprintf ( LOG_DBG, L"Record '%s' for '%s'\r\n", 
						(bRec) ? L"On" : L"Off", (LPCWSTR)strLoc ); )
		CCLTRY ( pTmpl->record ( pnSpc, strLocAbs,
						(strLocAbsF.length() > 0) ? strLocAbsF : strLocAbs, bRec, bFrom, bRead, false ) );

		// Result
		if (hr == S_OK)
			_EMT(Record,strLocAbs);
		else
			{
			_EMT(NotFound,strLocAbs);
			if (bRec == true)
				lprintf ( LOG_DBG, L"Temporal::receive:record failed:0x%x:%s:%s:%s (pTmpl %p)\r\n",	
								hr, (bRec) ? L"true" : L"false", (LPCWSTR)strLoc, (LPCWSTR)strLocAbs, pTmpl );
			}	// else

		}	// else if

	// Open temporal location or value
	else if (_RCP(Open))
		{
		IUnknown		*punkOp	= NULL;
		adtString	strLocAbs;
		adtBool		bRec(v);
		adtString	strRec(v);

		// State check
		CCLTRYE ( strLoc.length() > 0,	ERROR_INVALID_STATE );
		CCLTRYE ( pTmplLocs != NULL,		ERROR_INVALID_STATE );

		// Ensure an absolute namespace is used
		CCLTRY ( nspcPathTo ( pLocPar, strLoc, strLocAbs ) );

		// Put together options for open
		CCLTRY ( pOpts->clear() );
		CCLTRY ( pOpts->store ( adtString(L"Location"), strLocAbs ) );
		CCLTRY ( pOpts->store ( adtString(L"ReadOnly"), bRead ) );

		// Open location or value
		CCLTRY ( pTmplLocs->open ( pOpts, &punkOp ) );

		// Results  Hold reference count ?
		if (hr == S_OK)
//			_EMT(Open,adtIUnknownRef(punkOp));
			_EMT(Open,adtIUnknown(punkOp));
		else
			{
			_EMT(NotFound,strLocAbs);
			if (strLocAbs.length() > 1)
				lprintf ( LOG_DBG, L"Open failed:0x%x:%s\r\n", hr, (LPCWSTR)strLocAbs );
			}	// else

		// Clean up
		_RELEASE(punkOp);
		}	// else if

	// First location
	else if (_RCP(First))
		{
		adtString	strLocAbs;

		// State check
		CCLTRYE ( pTmplLocs != NULL,		ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0,	ERROR_INVALID_STATE );

		// Ensure an absolute namespace is used.
		// NOTE: This may not make sense for external DBs.
		CCLTRY ( nspcPathTo ( pLocPar, strLoc, strLocAbs ) );

		// Iterate the location
		_RELEASE(pItLoc);
		CCLTRY ( pTmplLocs->locations ( strLocAbs, &pItLoc ) );

		// Emit the next location
		CCLOK ( onReceive(prNext,v); )
		}	// else if

	// Next location
	else if (_RCP(Next))
		{
		adtValue vL;

		// State check
		CCLTRYE ( pItLoc != NULL,	ERROR_INVALID_STATE );

		// Read the next location
		CCLTRY ( pItLoc->read ( vL ) );

		// For next time
		CCLOK ( pItLoc->next(); )

		// Result
		if (hr == S_OK)
			_EMT(Next,vL);
		else
			_EMT(Last,adtInt(hr));
		}	// else if

	// Set sequence number for root temporal location
	else if (_RCP(Sequence))
		{
		IReceptor	*pRecep	= NULL;
		adtString	strLocAbs;
		adtLong		lSeq(v);
		adtValue		vLd;
		adtIUnknown	unkV;

		// State check
		CCLTRYE ( strLoc.length() > 0,	ERROR_INVALID_STATE );

		// Ensure an absolute namespace is used
		CCLTRY ( nspcPathTo ( pLocPar, strLoc, strLocAbs ) );

		// Attempt to load value with the given root
		CCLTRY ( pnSpc->get ( strLocAbs, vLd, NULL ) );

		// Receive the sequence number into the location
		CCLTRY ( _QISAFE((unkV=vLd),IID_IReceptor,&pRecep) );

		// Receive value into location
		CCLTRY ( pRecep->receive ( prSequence, STR_NSPC_SEQ, lSeq ) );

		// Clean up
		_RELEASE(pRecep);

		// Result
		if (hr == S_OK)
			_EMT(Sequence,lSeq);
		else
			_EMT(NotFound,adtInt(hr));
		}	// else if

	// Load a specific sequence number
	else if (_RCP(Load))
		{
		adtLong	uSq(v);
		adtValue	vLoc,vValue,vDate;

		// State check
		CCLTRYE ( pTmpl != NULL,	ERROR_INVALID_STATE );

		// Debug
//		if (hr == S_OK && uSq == 64)
//			lprintf ( LOG_DBG, L"Hi\r\n" );

		// Attempt to load a specific sequence number directly
		CCLTRY ( pTmpl->load ( uSq, vLoc, vValue, vDate ) );

		// Result
		if (hr == S_OK)
			{
			_EMT(Date,vDate);
			_EMT(Location,vLoc);
			_EMT(Load,vValue);
			}	// if
		else
			{
			lprintf ( LOG_DBG, L"Unable to load sequence number %ld\r\n", (U64)uSq );
			_EMT(NotFound,adtInt(hr));
			}	// else

		}	// else if

	// Limits of sequence numbers
	else if (_RCP(Limits))
		{
		U64	sqMin,sqMax;

		// State check
		CCLTRYE ( pTmpl != NULL,	ERROR_INVALID_STATE );

		// Set new maximum
		CCLTRY ( pTmpl->limits ( sqMin, sqMax ) );

		// Results
		if (hr == S_OK)
			{
			_EMT(First,adtLong(sqMin));
			_EMT(Last,adtLong(sqMax));
			}	// if
		else
			_EMT(NotFound,adtInt(hr));
		}	// else if

	// Set the maximum allowable sequence numbers
	else if (_RCP(Maximum))
		{
		adtLong	iMax(v);

		// State check
		CCLTRYE ( pTmpl != NULL,	ERROR_INVALID_STATE );

		// Set new maximum
		CCLTRY ( pTmpl->setMaximum ( iMax ) );
		}	// else if

	// Flush buffers
	else if (_RCP(Flush))
		{
		adtValue vF;

		// State check
		CCLTRYE ( pTmplLocs != NULL,	ERROR_INVALID_STATE );

		// Perform flush
		CCLTRY ( pTmplLocs->flush ( vF ) );
		}	// else if

	// Remove
	else if (_RCP(Remove))
		{
		// State check
		CCLTRYE ( pTmplLocs != NULL,	ERROR_INVALID_STATE );

		// Delete entire database
//		lprintf ( LOG_DBG, L"Remove hr 0x%x pTmplLocs %p\r\n", hr, pTmplLocs );
		CCLTRY ( pTmplLocs->remove(L"/"));
		}	// else if

	// State
	else if (_RCP(Location))
		hr = adtValue::toString ( v, strLoc );
	else if (_RCP(LocationFrom))
		hr = adtValue::toString ( v, strLocF );
	else if (_RCP(ReadOnly))
		bRead = v;
	else if (_RCP(From))
		bFrom = v;
	else if (_RCP(Temporal))
		{
		adtIUnknown unkV(v);
		_RELEASE(pTmpl);
		_RELEASE(pTmplLocs);
		CCLTRY(_QISAFE(unkV,IID_ITemporal,&pTmpl));
		CCLTRY(_QISAFE(unkV,IID_ILocations,&pTmplLocs));
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

