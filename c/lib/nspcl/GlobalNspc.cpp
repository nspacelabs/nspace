////////////////////////////////////////////////////////////////////////
//
//									GLBNSPC.CPP
//
//						Implementation of the global nSpace object.
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"
#include <stdio.h>

// For direct creation of ADT objects to avoid DLL unloading race conditions
#include "../adtl/adtl_.h"

// Globals
GlobalNspc	nspcglb;

GlobalNspc :: GlobalNspc ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	refcnt		= 0;
	pStkAbs		= NULL;
	pItAbs		= NULL;
	pcfDct		= NULL;
	pcfLst		= NULL;

	// AddRef self
	AddRef();
	}	// GlobalNspc

GlobalNspc :: ~GlobalNspc ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pcfDct);
	_RELEASE(pcfLst);

	// In case it did not get called
	refcnt = 1;
	Release();
	}	// GlobalNspc

ULONG GlobalNspc :: AddRef ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to increase reference on the object.
	//
	//	RETURN VALUE
	//		Reference count
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	Stack		*pStk		= NULL;

	// Reference count
	if (++refcnt > 1)
		return refcnt;

	// Create stack and iterator for strings
	CCLTRYE ( (pStk = new Stack()) != NULL, E_OUTOFMEMORY );
	CCLOK   ( pStk->AddRef(); )
	CCLTRY  ( pStk->construct() );
	CCLTRY  ( pStk->iterate ( &pItAbs ) );

	// Assign to internal variable
	if (hr == S_OK)
		{
		pStkAbs = pStk;
		_ADDREF(pStkAbs);
		}	// if

	// Clean up
	_RELEASE(pStk);

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_ERR, L"Unable to initialize global objects, 0x%x" );

	return refcnt;
	}	// AddRef

HRESULT GlobalNspc :: dict ( IDictionary **ppDct )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Create a new dictionary object
	//
	//	PARAMETERS
	//		-	ppDct will receive the dictionary
	//
	// RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Need class factory ?
	if (pcfDct == NULL)
		hr = cclGetFactory ( L"Adt.Dictionary", IID_IClassFactory, 
									(void **) &pcfDct );

	// Create object
	CCLTRY ( pcfDct->CreateInstance ( NULL, IID_IDictionary, (void **) ppDct ) );

	return hr;
	}	// dict

HRESULT GlobalNspc :: list ( IList **ppLst )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Create a new list object
	//
	//	PARAMETERS
	//		-	ppLst will receive the list
	//
	// RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Need class factory ?
	if (pcfLst == NULL)
		hr = cclGetFactory ( L"Adt.List", IID_IClassFactory, 
									(void **) &pcfLst );

	// Create object
	CCLTRY ( pcfLst->CreateInstance ( NULL, IID_IList, (void **) ppLst ) );

	return hr;
	}	// list

ULONG GlobalNspc :: Release ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decrement reference count on object.
	//
	////////////////////////////////////////////////////////////////////////

	// Reference count
	if (refcnt == 0 || --refcnt > 0)
		return refcnt;

	// Clean up
	_RELEASE(pcfDct);
	_RELEASE(pcfLst);
	_RELEASE(pStkAbs);
	_RELEASE(pItAbs);

	return 0;
	}	// Release

