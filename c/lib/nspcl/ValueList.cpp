////////////////////////////////////////////////////////////////////////
//
//									ValueList.CPP
//
//					Implementation of the value list node
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"

ValueList :: ValueList ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pLst	= NULL;
	iSel	= 0;
	bSync	= false;
	}	// Value

HRESULT ValueList :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	else
		{
		// Clean up
		_RELEASE(pLst);
		}	// else

	return hr;
	}	// onAttach

HRESULT ValueList :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IReceptor
	//
	//	PURPOSE
	//		-	A location has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	pl is the location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Add a value to the list
	if (_RCP(Add))
		{
		// Synchronizing ?
		if (!bSync)
			{
			// State check
			CCLTRYE ( pLst != NULL, ERROR_INVALID_STATE );

			// Write the value to the list
			CCLTRY ( pLst->write ( v ) );
			}	// if
		}	// if

	// Delete value from list
	else if (_RCP(Delete))
		{
		IDictionary *pDct = NULL;
		adtInt		iIdx(v);

		// Synchronizing ?
		if (!bSync)
			{
			// State check
			CCLTRYE ( pLst != NULL, ERROR_INVALID_STATE );

			// Delete item by index
			CCLTRY ( _QI(pLst,IID_IDictionary,&pDct) );
			CCLOK  ( pDct->remove ( iIdx ); )

			// If the current selection was the removed item then
			if (hr == S_OK && iIdx == iSel)
				{
				// New selection
				iSel = 0;

				// Notify
				_EMT(Index,iSel);
				}	// if
			}	// if

		// Clean up
		_RELEASE(pDct);
		}	// if

	// Selection by index
	else if (_RCP(Index))
		{
		U32		cnt = 0;
		adtInt	iSelN(v);

		// Synchronizing ?
		if (!bSync)
			{
			// State check
			CCLTRYE ( pLst != NULL, ERROR_INVALID_STATE );

			// Size of container
			CCLTRY ( pLst->size(&cnt) );

			// Ensure index is within range (index is 1-based)
			if (hr != S_OK || (S32)iSelN < 0 || iSelN > cnt)
				iSelN = 0;

			// New index, assumption is this is the index to set,
			// no need for additional notification (to avoid doubles)
			iSel = iSelN;
			}	// if

		}	// if

	// Clear list
	else if (_RCP(Clear))
		{
		// Synchronizing ?
		if (!bSync)
			{
			// State check
			CCLTRYE ( pLst != NULL, ERROR_INVALID_STATE );

			// Unselect
			if (hr == S_OK && iSel != 0)
				{
				iSel = 0;
				_EMT(Index,iSel);
				}	// if
			}	// if

		// Clear contents
		CCLOK ( pLst->clear(); )
		}	// else if

	// Select entry by value
	else if (_RCP(Value))
		{
		IIt		*pIt		= NULL;
		S32		idx,isel	= -1;
		adtValue	vL;

		// State check
		CCLTRYE ( pLst != NULL, ERROR_INVALID_STATE );

		// Search for first matching value
		CCLTRY ( pLst->iterate(&pIt) );
		for (idx = 0;hr == S_OK && isel == -1 && pIt->read(vL) == S_OK;++idx,pIt->next())
			{
			// Match ?
			if (adtValue::compare(v,vL) == 0)
				isel = idx;		
			}	// for

		// Notify
		if (isel != -1)
			_EMT(Index,adtInt((isel+1)));

		// Clean up
		_RELEASE(pIt);
		}	// else if

	// Updates to list have completed
	else if (_RCP(Update))
		{
		// Synchronizing ?
		if (!bSync)
			{
			// Send out the latest list
			_EMT(Update,adtIUnknown(pLst));
			}	// if
		}	// else if

	// Specify a new list
	else if (_RCP(List))
		{
		adtIUnknown unkV(v);
		_RELEASE(pLst);
		_QISAFE(unkV,IID_IList,&pLst);
		}	// if
	else if (_RCP(Sync))
		bSync = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

