////////////////////////////////////////////////////////////////////////
//
//									REFLECT.CPP
//
//					Implementation of the location reflection node
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"

Reflect :: Reflect ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	strLoc	= L"";
	bState	= true;
	bAuto		= true;
	bDbg		= false;
	}	// Reflect

Reflect :: ~Reflect ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	removeAll();
	_RELEASE(pRef);
	}	// Reflect

HRESULT Reflect :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN Reflect
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Create dictionary to keep track of active reflections
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pRef ) );

		// Defaults
 		if (pnDesc->load ( adtString(L"Root"), vL )	== S_OK)
			receive ( prRoot, L"Value", vL );
 		if (pnDesc->load ( adtString(L"Location"), vL )	== S_OK)
			receive ( prLocation, L"Value", vL );
 		if (pnDesc->load ( adtString(L"Value"), vL )	== S_OK)
			receive ( prValue, L"Value", vL );
		bState = true;
 		if (pnDesc->load ( adtString(L"State"), vL )	== S_OK)
			receive ( prState, L"Value", vL );
		bAuto	= true;
 		if (pnDesc->load ( adtString(L"Auto"), vL )	== S_OK)
			bAuto = vL;
 		if (pnDesc->load ( adtString(L"Debug"), vL )	== S_OK)
			bDbg = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		removeAll();
		_RELEASE(pRef);
		}	// else

	return hr;
	}	// onAttach

HRESULT Reflect :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IReceptor
	//
	//	PURPOSE
	//		-	A location has received a Reflect on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	pl is the location
	//		-	v is the Reflect
	//
	//	RETURN Reflect
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Add a receptor for a location
	if (_RCP(Add))
		{
		ReflectRx	*pRx	= NULL;
		ILocation	*pLoc	= NULL;
		adtString	strAbs;
		adtValue		vL;
		adtIUnknown	unkV;

		// Generate full path
		CCLTRY ( strAbs.append ( strRoot ) );
		CCLTRY ( strAbs.append ( strLoc ) );

		// State check
		CCLTRYE ( strAbs.length() > 0,	ERROR_INVALID_STATE );

		// Is location already reflected ?
//		CCLTRYE ( pRef->load ( strAbs, vL ) != S_OK, ERROR_INVALID_STATE );

		// Obtain location
		CCLTRY ( pnSpc->get ( strAbs, vL, NULL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ILocation,&pLoc) );

		// Create a local reflection reciever for location
		CCLTRYE ( (pRx = new ReflectRx(this,strAbs,pLoc)) != NULL, E_OUTOFMEMORY );
		CCLOK   ( pRx->AddRef(); )
		CCLTRY  ( pRx->construct() );
		CCLOK   ( pRx->bReflect = bState; )

		// Record that receiver was added (NOTE: Receptor stored as int with reference count)
		CCLOK  ( pRx->AddRef(); )
		CCLTRY ( pRef->store ( adtLong((U64)(IReceptor *)pRx), strAbs ) );

		// Associate location with receiver
//		CCLTRY ( pRef->store ( strAbs, adtIUnknown((IReceptor *)pRx) ) );

		// Emit new receptor before flood of reflected values start emitting.
		// This gives the outside world a chance to prepare.
		if (hr == S_OK)
			_EMT(Add,adtLong((U64)(IReceptor *)pRx));
		else
			_EMT(Error,adtInt(hr));

		// If initial reflection of state is disabled, temporarily disable output
		if (hr == S_OK && bState == false)
			pRx->bReflect = false;

		// Emit an internal value that signals the beginning of a reflection.
		// This can be used by the other end for synchronization.
//		CCLOK ( pRx->receive ( pr, L"_Reflect", adtBool(true) ); )

		// Reflect location
		CCLTRY ( pLoc->connect ( pRx, true, true ) );

		// Enable further changes
		if (hr == S_OK)
			pRx->bReflect = true;

		// End of reflection
//		CCLOK ( pRx->receive ( pr, L"_Reflect", adtBool(false) ); )

		// Clean up
		_RELEASE(pRx);
		_RELEASE(pLoc);
		}	// if

	// Remove a receptor from a location
	else if (_RCP(Remove))
		{
		adtString	strRx;

		// Remove receptor
		CCLTRY(remove(v,strRx));

		// Result
		if (hr == S_OK)
			_EMT(Remove,strRx);
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Store
	else if (_RCP(Store))
		{
		IReceptor	*pRcpSrc	= NULL;
		adtValue		vL;
		adtIUnknown	unkV;
		int			len;
		adtString	strKey;
		adtString	strAbs;
		adtString	strDef;

		// Generate full path
		CCLTRY ( strAbs.append ( strRoot ) );
		CCLTRY ( strAbs.append ( strLoc ) );

		// State check
		CCLTRYE ( (len = strAbs.length()) > 0,	ERROR_INVALID_STATE );
			
		// This 'store' only works for values so last character cannot
		// be a slash
		CCLTRYE ( strAbs[len-1] != '\0', E_INVALIDARG );

		// Obtain the last key string for filtering and storage
		if (hr == S_OK)
			{
			// Last slash
			WCHAR	*pw = wcsrchr ( &strAbs.at(), '/' );

			// Extract last key and terminate location
			if (pw != NULL)
				{
				// Key string
				strKey = pw+1;
				strKey.at();

				// End location
				*(pw+1) = '\0';
				}	// if

			// No path specified, error
			else
				hr = ERROR_INVALID_STATE;
			}	// if
	
		// Do not allow storage/reflection into reserved keys.
		// Done here so the outside world does not have to filter incoming
		// store requests.  These limitations are the same as the
		// local version of reflection (Location::reflect).
		CCLTRYE (	(	strKey[0] != CHR_NSPC_PRE && 
							strKey[0] != CHR_NSPC_PST ) || 
							(	WCASECMP(strKey,STR_NSPC_PARENT)	&&
								WCASECMP(strKey,STR_NSPC_NAME)	&&
								WCASECMP(strKey,STR_NSPC_TYPE)	&&
								WCASECMP(strKey,STR_NSPC_CONN)	&&
								WCASECMP(strKey,STR_NSPC_NSPC) ), E_INVALIDARG );

		// Special case.  It is possible to store a '.Location' into a location which
		// specified the definition for that location.  This allowed 'on-the fly' creation
		// of new subgraphs when reflecting location.
		if (hr == S_OK && !WCASECMP(strKey,STR_NSPC_LOC))
			{
			// The definition is the value
			hr = adtValue::toString ( vValue, strDef );
			if (bDbg)
				lprintf ( LOG_DBG, L"Location definition specified : %s\r\n", (LPCWSTR)strDef );
			}	// if

		// Obtain location down to the last key, this will allow any auto-instancing
		// to occur if necessary
		if (bDbg)
			lprintf ( LOG_DBG, L"Store 0x%x : %s : %s --->", hr, (LPCWSTR) strAbs, (LPCWSTR)strKey );
		if (bAuto || strDef.length() > 0)
			{
			// Allow auto-instancing if necessary
			CCLTRY ( pnSpc->get ( strAbs, vL, strDef ) );
			}	// if
		else
			{
			IDictionary	*pRoot	= NULL;

			// Obtain root of entire namespace
			CCLTRY ( pnSpc->get ( L"/", vL, NULL ) );
			CCLTRY ( _QISAFE((unkV = vL),IID_IDictionary,&pRoot) );

			// No auto-instancing, must already exist
			CCLTRY ( nspcLoadPath ( pRoot, strAbs, vL ) );

			// Clean up
			_RELEASE(pRoot);
			}	// else

		// Receive/store the value
		if (hr == S_OK && (IUnknown *)(NULL) != (unkV = vL))
			{
			IReceptor	*pRcp		= NULL;
			IDictionary	*pDct		= NULL;

			// Receive value if receptor
			if (_QI(unkV,IID_IReceptor,&pRcp) == S_OK)
				{
				if (bDbg)
					lprintf ( LOG_DBG, L"Storing key '%s' into receptor\r\n", (LPCWSTR)strKey );
				hr = pRcp->receive ( (pRcpSrc != NULL) ? pRcpSrc : this, strKey, vValue );
				pRcp->Release();
				}	// if

			// Ok for store to target dictionary directly
			else if (_QI(unkV,IID_IDictionary,&pDct) == S_OK)
				{
				hr = pDct->store ( strKey, vValue );
				pDct->Release();
				}	// else if

			// Error
			else
				{
				lprintf ( LOG_DBG, L"Destination object is not a receptor or dictionary\r\n" );
				hr = E_NOTIMPL;
				}	// else
			}	// if

		// Store/receive value into location
//		CCLTRY ( _QISAFE((unkV=vL),IID_IReceptor,&pRcp) );
//		lprintf ( LOG_DBG, L"Store : pRcp %p : strAbs %s\r\n", pRcp, (LPCWSTR) strAbs );
//		CCLTRY ( pRcp->receive ( (pRcpSrc != NULL) ? pRcpSrc : this, strKey, vValue ) );
//		lprintf ( LOG_DBG, L"Store : %s : %s <---", (LPCWSTR) strAbs, (LPCWSTR)strKey );

		// Debug
//		if (hr != S_OK && hr != ERROR_ALREADY_EXISTS)
//			lprintf ( LOG_DBG, L"Reflect::Store:%s:0x%x:%s:%s\r\n", 
//						(LPCWSTR)strnName, hr, (LPCWSTR)strRoot, (LPCWSTR)strLoc );

		// Clean up
		if (bDbg)
			lprintf ( LOG_DBG, L"Store results 0x%x\r\n", hr );
		_RELEASE(pRcpSrc);
		}	// else if

	// State
	else if (_RCP(Root))
		hr = adtValue::toString ( v, strRoot );
	else if (_RCP(Location))
		{
		hr = adtValue::toString ( v, strLoc );
//		dbgprintf ( L"Reflect::Receive:%s:Location:%s\r\n", (LPCWSTR)strnName, (LPCWSTR)strLoc );
		}	// else if
	else if (_RCP(Value))
		{
		hr = adtValue::copy ( v, vValue );
//		adtString	strV;
//		if (adtValue::toString ( v, strV ) == S_OK)
//			dbgprintf ( L"Reflect::Receive:%s:Value:%s\r\n", (LPCWSTR)strnName, (LPCWSTR)strV );
		}	// else if
	else if (_RCP(State))
		bState = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Reflect :: remove ( const ADTVALUE &v, adtString &strRx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Remove/disconnect a single reflection
	//
	//	PARAMETERS
	//		-	v contains the receptor Id to remove
	//		-	strRx will receive the location that was associated with the
	//			receptor.
	//	RETURN Reflect
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IReceptor	*pRx	= NULL;
	adtValue		vL;

	// Check if receptor is valid
	CCLTRY ( pRef->load ( v, vL ) );

	// Valid receptor
	CCLOK ( pRx = (IReceptor *)(U64)adtLong(v); )
	CCLOK ( ((ReflectRx *)pRx)->bReflect = false; )

	// Location that was associated with receptor
	CCLTRYE( (strRx = vL).length() > 0, E_UNEXPECTED );

	// Disconnect from location
	// Do NOT use 'get' to get location since that could cause a re-instancing of a graph
	if (hr == S_OK && ((ReflectRx *)pRx)->pLoc != NULL)
		((ReflectRx *)pRx)->pLoc->connect ( pRx, false, true );

	// Remove Id from dictionary and release
	CCLOK ( pRef->remove (v); )
	_RELEASE(pRx);

	return hr;
	}	// remove

HRESULT Reflect :: removeAll ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Remove/disconnect all reflected locations
	//
	//	RETURN Reflect
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IIt			*pIt	= NULL;
	U32			cnt0	= 0;
	U32			cnt1	= 0;
	adtValue		vL;
	adtString	strRx;

	// State check
	CCLTRYE ( pRef != NULL, E_UNEXPECTED );

	// Iterate list of receptors
	CCLTRY(pRef->keys(&pIt));
	while (hr == S_OK && pIt->read(vL) == S_OK)
		{
		// Current size
		CCLTRY(pRef->size(&cnt0));

		// Remove reflection
		CCLOK(remove(vL,strRx);)

		// If the size changed, then the removal was successful and the iteration
		// needs to be re-started since container state changed.
		CCLTRY(pRef->size(&cnt1));
		if (hr == S_OK)
			{
			// First/next entry
			if (cnt0 != cnt1)
				pIt->begin();
			else
				pIt->next();
			}	// if

		}	// while

	// Clean up
	_RELEASE(pIt);

	return hr;
	}	// removeAll

/////////////
// ReflectRx
/////////////

ReflectRx :: ReflectRx ( Reflect *_pParent, const WCHAR *wLoc,
									ILocation *_pLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//	
	//	PARAMETERS
	//		-	_pParent is the parent reflect node
	//		-	wLoc is the root location being reflected
	//		-	_pLoc is the connected location
	//
	////////////////////////////////////////////////////////////////////////
	pParent	= _pParent;
	strRoot	= wLoc;
	pLoc		= _pLoc;
	bReflect	= true;

	// Keep reference count on location, correct ?  recursive ?
	_ADDREF(pLoc);
	}	// ReflectRx

void ReflectRx :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	pParent = NULL;
	_RELEASE(pLoc);
	}	// destruct

HRESULT ReflectRx :: receive ( IReceptor *prSrc, const WCHAR *pwLoc, 
											const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IReceptor
	//
	//	PURPOSE
	//		-	Called to store a value at a location.
	//
	//	PARAMETERS
	//		-	prSrc is the source of the reception
	//		-	pwLoc is the location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	IDictionary		*pDct = NULL;
	size_t			len	= 0;

	// Still valid ?
	if (pParent == NULL)
		return S_OK;

	// DEBUG
//	{
//	adtString strV;
//	adtValue::toString(v,strV);
//	lprintf ( LOG_DBG, L"Root %s Loc %s Val %s --->", (LPCWSTR)strRoot, pwLoc,
//					(LPCWSTR)strV );
//	}

	// Discussion : It is necessary for nSpace internally to propagate values from receptors
	// in case of linked subgraphs due to nodes being 'inactive'.  However, at the moment
	// this reflection node does not externalize receptor values.  Externalizing receptor
	// values can lead to recursive logic in external graphs.  Is this the best place
	// to do this ?
	len = wcslen(pwLoc);
	if (	(len == 10 && !WCASECMP(pwLoc,L"Fire/Value")) ||
			(len > 10 && !WCASECMP(&pwLoc[len-11],L"/Fire/Value")) )
		{
//		lprintf ( LOG_DBG, L"Skipping %s : %s\r\n", (LPCWSTR) strRoot, pwLoc );
		return S_OK;
		}	// if

	// Emit reflection
//	lprintf ( LOG_DBG, L"prSrc %p : strRoot %s\r\n", prSrc, (LPCWSTR) strRoot );

	// For now to be safe, be inefficient by creating a new dictionary for each value
	if (bReflect)
		{
		CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDct));
		CCLTRY(pDct->store(adtString(L"Root"),strRoot) );
		CCLTRY(pDct->store(adtString(L"Location"),adtString(pwLoc)));
		CCLTRY(pDct->store(adtString(L"Value"),v));
		CCLTRY(pDct->store(adtString(L"Source"),adtLong((U64)(IReceptor *)this)));
		CCLOK	(pParent->peOnValue->receive( this, L"Value", adtIUnknown(pDct) );)
		}	// if

	// DEBUG
//	{
//	adtString strV;
//	adtValue::toString(v,strV);
//	lprintf ( LOG_DBG, L"Root %s Loc %s Val %s <---", (LPCWSTR)strRoot, pwLoc,
//					(LPCWSTR)strV );
//	}

	// Locking this critical section is causing deadlocks with other threads.
	// Is this unncecessary because of node protection ?
//	if (pParent->csRx.enter())
//		{
//		pParent->peOnRoot->receive		( this, L"Value", strRoot );
//		pParent->peOnLocation->receive( this, L"Value", adtString(pwLoc) );
//		pParent->peOnValue->receive	( this, L"Value", v );
//		pParent->csRx.leave();
//		}	// if

	// Clean up
	_RELEASE(pDct);

	return hr;
	}	// receive
