////////////////////////////////////////////////////////////////////////
//
//								tempimpl.CPP
//
//				Default implementation of the temporal nSpace object.
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"
#include <stdio.h>

// Globals
extern GlobalNspc	nspcglb;

Temporal :: Temporal ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pStmSrc		= NULL;
	pStmLoc		= NULL;
	pStmsSeq[0]	= NULL;
	pStmsSeq[1]	= NULL;
	pStmsVar[0]	= NULL;
	pStmsVar[1]	= NULL;
	pDctMap		= NULL;
	uSeqNum		= 1;
	uSeqOff[0]	= MAXUINT64;
	uSeqOff[1]	= MAXUINT64;
	uSeqMax		= 0;
	pDctOpts		= NULL;
	pDctStat		= NULL;
	pDctRec		= NULL;
	verVar		= 0;
	verSeq		= 0;
	szHdrVar		= 0;
	szHdrSeq		= 0;
	pPrsrL		= NULL;
	pPrsrS		= NULL;
	pDctSql		= NULL;
	}	// Temporal

HRESULT Temporal :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Close the resource.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Close open streams
	_RELEASE(pStmLoc);
	_RELEASE(pStmsSeq[0]);
	_RELEASE(pStmsSeq[1]);
	_RELEASE(pStmsVar[0]);
	_RELEASE(pStmsVar[1]);

	return S_OK;
	}	// close

HRESULT Temporal :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	CCLObject
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Create parsers used during load/save
	CCLTRY(COCREATE(L"Io.StmPrsBin",IID_IStreamPersist,&pPrsrL));
	CCLTRY(COCREATE(L"Io.StmPrsBin",IID_IStreamPersist,&pPrsrS));

	// Dictionary options
	CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctOpts));

	// Dictionary to obtain status
	CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctStat));

	// Dictionary to keep track of recordings
	CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctRec));

	return hr;
	}	// construct

HRESULT Temporal :: copy ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Copy a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// copy

void Temporal :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	close();
	_RELEASE(pPrsrL);
	_RELEASE(pPrsrS);
	_RELEASE(pDctOpts);
	_RELEASE(pDctRec);
	_RELEASE(pDctStat);
	_RELEASE(pDctMap);
	_RELEASE(pStmSrc);
	_RELEASE(pDctSql);
	}	// destruct

HRESULT Temporal :: flush ( ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Flushes or synchronous the stream source.
	//
	//	PARAMETERS
	//		-	v is a value for possible future use
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Flush buffers
	if (pStmLoc != NULL)
		pStmLoc->flush();
	if (pStmsSeq[0] != NULL)
		pStmsSeq[0]->flush();
	if (pStmsSeq[1] != NULL)
		pStmsSeq[1]->flush();
	if (pStmsVar[0] != NULL)
		pStmsVar[0]->flush();
	if (pStmsVar[1] != NULL)
		pStmsVar[1]->flush();

	return hr;
	}	// flush

HRESULT Temporal :: getResId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Return an identifier for the resource.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Not sure what the best 'resource' is here...
	adtValue::clear ( vId );

	return hr;
	}	// getResId

HRESULT Temporal :: getStreamIdx ( NSSQNM uSq, bool bRead, U32 *pIdx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Obtain the stream index to use for the provided sequence
	//			number.
	//
	//	PARAMETERS
	//		-	uSq is the sequence number
	//		-	bRead is true for read-only operation, false for writable
	//		-	pIdx will receive the index
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;

	// Determine stream index for existing streams
	*pIdx		=	(uSeqMax == 0)						? 0 :
					(	uSeqOff[0] != MAXUINT64 && 
						uSq >= uSeqOff[0] && 
						uSq < uSeqOff[0]+uSeqMax)	? 0 :
					(uSeqOff[1] != MAXUINT64 && 
						uSq >= uSeqOff[1] && 
						uSq < uSeqOff[1]+uSeqMax)	? 1 : -1;

	//
	// If the sequence number was not found and writing is necessary
	// then it is time to create room for the new sequence number
	//
	if (*pIdx == -1 && !bRead)
		{
		// Determine stream index in which the new sequence number should reside.
		// Either the alternate stream is not being used, or use the streams
		// with the 'oldest' sequence number (existing data will be deleted).
		*pIdx =	(uSeqOff[1] == MAXUINT64 || uSeqOff[0] > uSeqOff[1])	? 1 : 0;

		// Open pair of streams on truncated files with sequence number as initial value
		CCLTRY ( openStreams ( *pIdx, false, true, true, uSq ) );

		// New streams mean first entry is reserved so skip the current sequence
		// number to skip first entry in new stream
		++uSeqNum;
		}	// if

	// Valid ?
	CCLTRYE ( (*pIdx) != -1, ERROR_NOT_FOUND );

	return hr;
	}	// getStreamIdx

HRESULT Temporal :: limits ( U64 &uSqMin, U64 &uSqMax )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ITemporal
	//
	//	PURPOSE
	//		-	Retrieve the limits of the sequence numbers.
	//
	//	PARAMETERS
	//		-	uSq is the sequence number to look-up
	//		-	vLoc will receive the full namespace path for the value
	//		-	vValue will receive the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Limits of current database
	uSqMin = (uSeqOff[0] < uSeqOff[1]) ? uSeqOff[0] : uSeqOff[1];
	if (uSqMin == 0)	uSqMin = 1;
	uSqMax = uSeqNum-1;

	return hr;
	}	// limits

HRESULT Temporal :: load ( U64 uSq, ADTVALUE &vLoc, ADTVALUE &vValue,
									ADTVALUE &vDate )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ITemporal
	//
	//	PURPOSE
	//		-	Load the location and value associated with a specific
	//			sequence number.
	//
	//	PARAMETERS
	//		-	uSq is the sequence number to look-up
	//		-	vLoc will receive the full namespace path for the value
	//		-	vValue will receive the value
	//		-	vDate will receive the storage date
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	HDRSEQV2	hdrS;

	// Load header/value for sequence number
	CCLTRY ( locGet ( uSq, &hdrS, &vValue ) );

	// Storage data
	CCLTRY ( adtValue::copy ( adtDate(hdrS.date), vDate ) );

	// Obtain the full path associated with the Id

	// Thread safety
	csIO.enter();

	// The location Id represents the location within the location stream
	CCLTRY ( pStmLoc->seek ( hdrS.v1.lid, STREAM_SEEK_SET, NULL ) );

	// Latest sequence number is stored at location
	CCLTRY ( pStmLoc->seek ( sizeof(NSSQNM), STREAM_SEEK_CUR, NULL ) );

	// Full string of location is stored next
	CCLTRY ( pPrsrL->load ( pStmLoc, vLoc ) );

	// Thread safety
	csIO.leave();

	return hr;
	}	// load

HRESULT Temporal :: locGet ( LOCID uId, NSSQNM *puSq )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Obtain the latest sequence number of a location
	//
	//	PARAMETERS
	//		-	uId is the location Id
	//		-	puSq will receive the latest sequence number
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U64			uRd;

	// State check
	CCLTRYE ( pStmLoc != NULL, ERROR_NOT_FOUND );

	// Thread safety
	csIO.enter();

	// Seek to correct position for location, the Id is the location
	CCLTRY ( pStmLoc->seek ( uId, STREAM_SEEK_SET, &uRd ) );
	CCLTRYE( uRd == uId, E_UNEXPECTED );

	// Read header
	CCLTRY ( pStmLoc->read ( puSq, sizeof(NSSQNM), &uRd ) );
	CCLTRYE( uRd == sizeof(NSSQNM), E_UNEXPECTED );

	// Thread safety
	csIO.leave();

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Fail (get sequence) 0x%x\r\n", hr );

	return hr;
	}	// locGet

HRESULT Temporal :: locGet ( NSSQNM uSq, HDRSEQV2 *pHdr, ADTVALUE *pv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Obtain the emission header for a sequence number.
	//
	//	PARAMETERS
	//		-	uSq is the sequence number
	//		-	pHdr will receive the header information
	//		-	pv, if not null, will receive the value for the sequence number.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	U32		idx	= 0;
	U64		uRd,uIo;

	// State check
	CCLTRYE ( uSq != 0 && pStmLoc != NULL, ERROR_NOT_FOUND );

	// Thread safety
	csIO.enter();

	// Initialize possible unused fields
	CCLOK ( pHdr->date = 0; )

	// Stream for sequence number
	CCLTRY ( getStreamIdx ( uSq, true, &idx ) );

	// Seek to correct position for sequence number
	CCLOK		( uIo = (uSq-uSeqOff[idx])*szHdrSeq; )
	CCLTRY	( pStmsSeq[idx]->seek ( uIo, STREAM_SEEK_SET, &uRd ) );
	CCLTRYE	( uRd == uIo, E_UNEXPECTED );

	// Read header
//	CCLOK		( pHdr->date = NSPC_DATE_INV; )
	CCLTRY	( pStmsSeq[idx]->read ( pHdr, szHdrSeq, &uRd ) );
	CCLTRYE	( uRd == szHdrSeq, E_UNEXPECTED );

	// Does caller want the value ?
	if (hr == S_OK && pv != NULL)
		{
		bool	bVar	= false;
		U64	uRd;

		// Variable length value ?
		bVar = (	(adtValue::type(pHdr->v1.value) == VTYPE_STR) ||
					(adtValue::type(pHdr->v1.value) == VTYPE_UNK) );

		// Variable length values are stored in the value stream
		if (bVar)
			{
			// Seek to correct position for value (skip over header)
			CCLTRY ( pStmsVar[idx]->seek ( (pHdr->v1.value.vlong)+szHdrVar, STREAM_SEEK_SET, &uRd ) );
			CCLTRYE( uRd == (pHdr->v1.value.vlong)+szHdrVar, E_UNEXPECTED );

			// Load value from position
			CCLTRY ( pPrsrL->load ( pStmsVar[idx], *(pv) ) );
			}	// if

		// Otherwise the value in the header is used
		else
			hr = adtValue::copy(pHdr->v1.value,(*pv));
		}	// if

	// Thread safety
	csIO.leave();

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Fail (value) 0x%x\r\n", hr );

	return hr;
	}	// locGet

HRESULT Temporal :: locId ( const WCHAR *path, bool bRO,
										LOCID *pId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve or create the location Id for the given path.
	//
	//	PARAMETERS
	//		-	path is the path of the location
	//		-	bRO is true for read-only, false for write/create
	//		-	pId will receive the location Id
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtValue		v;
	adtInt		vId;

	// State check
	CCLTRYE ( pStmLoc != NULL, ERROR_NOT_FOUND );

	// Debug
//	if (wcsstr ( path, L"_Parent") != NULL)
//		dbgprintf ( L"Hi\r\n" );

	// Attempt access on location
	CCLTRY ( nspcLoadPath ( pDctMap, path, v ) );
//	CCLTRY ( pDctMap->load ( adtString(path), v ) );

	// Caller is asking for a location than is currently just a path
	CCLTRYE( v.vtype == VTYPE_I4, E_UNEXPECTED );
	CCLOK  ( vId = v; )

	// Not found but write requested
	if (hr == ERROR_NOT_FOUND && !bRO)
		{
		NSSQNM	sqZ	= 0;
		U64		at;

		// Reset status
		hr = S_OK;

		// Thread safety
		csIO.enter();

		// Location information will be placed at the end of the stream.
		CCLTRY( pStmLoc->seek ( 0, STREAM_SEEK_END, &at ) );

		// The location Id will be the position in the stream.
		CCLOK ( vId = (U32)at; )

		// No latest sequence number of location yet
		CCLTRY ( pStmLoc->write ( &sqZ, sizeof(sqZ), NULL ) );

		// Save full string immediately after header
		CCLTRY ( pPrsrS->save ( pStmLoc, adtString(path) ) );

		// Thread safety
		csIO.leave();

		// Store location Id in map
		CCLTRY ( nspcStoreValue ( pDctMap, path, vId, false ) );
		}	// if

	// Location Id
	CCLOK ( *pId = vId; )

	return hr;
	}	// locId

HRESULT Temporal :: locations ( const WCHAR *wLoc, IIt **ppIt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns an iterator for the sub-locations at the 
	//			specified location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the location
	//		-	ppIt will receive an location iterator.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IDictionary	*pLoc	= NULL;
	IList			*pLst	= NULL;
	IIt			*pIt	= NULL;
	adtValue		v;
	adtIUnknown	unkV;

	// Create a list to receive the list of locations
	CCLTRY(COCREATE(L"Adt.List",IID_IList,&pLst));

	// Obtain location in map
	CCLTRY ( nspcLoadPath ( pDctMap, wLoc, v ) );
//	CCLTRY ( pDctMap->load ( adtString(wLoc), v ) );

	// A location is a dictionary
	CCLTRY ( _QISAFE((unkV = v),IID_IDictionary,&pLoc) );
	CCLTRY ( pLoc->keys ( &pIt ) );
	while (hr == S_OK && pIt->read ( v ) == S_OK)
		{
		adtString	vK(v),vV;

		// Value at location
		CCLTRY ( pLoc->load ( vK, vV ) );

		// Ignore parent that stored in path hierarchy
		if (hr == S_OK && WCASECMP(vK,STR_NSPC_PARENT))
			{
			// A number at location means path is a value
			if (hr == S_OK && vV.vtype == VTYPE_I4)
				{
				// Add name to list
				CCLTRY ( pLst->write ( vK ) );
				}	// if

			// An object (dictionary) means path is a location
			else if (hr == S_OK && vV.vtype == VTYPE_UNK)
				{
				// Locations are identified by trailing slashes
				CCLTRY ( vK.append ( L"/" ) );

				// Add name to list
				CCLTRY ( pLst->write ( vK ) );
				}	// else if
			}	// if

		// Next location
		pIt->next();
		}	// while

	// Result
	CCLTRY ( pLst->iterate ( ppIt ) );

	// Clean up
	_RELEASE(pLst);
	_RELEASE(pIt);
	_RELEASE(pLoc);

	return hr;
	}	// locations

HRESULT Temporal :: locPut ( LOCID uId, NSSQNM uSq )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Save the latest sequence number of location
	//
	//	PARAMETERS
	//		-	uId is the location Id
	//		-	uSq is the sequence number
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U64			uWr;

	// State check
	CCLTRYE ( pStmLoc != NULL, ERROR_NOT_FOUND );

	// Thread safety
	csIO.enter();

	// Seek to correct position for location
	CCLTRY ( pStmLoc->seek ( uId, STREAM_SEEK_SET, &uWr ) );
	CCLTRYE( uWr == uId, E_UNEXPECTED );

	// Write header
	CCLTRY ( pStmLoc->write ( &uSq, sizeof(uSq), &uWr ) );
	CCLTRYE( uWr == sizeof(uSq), E_UNEXPECTED );

	// Thread safety
	csIO.leave();

	return hr;
	}	// locPut

HRESULT Temporal :: locPut ( NSSQNM uSq, const HDRSEQV2 *pHdr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Save the emission header for a sequence number.
	//
	//	PARAMETERS
	//		-	uSq is the sequence number
	//		-	pHdr container the information
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	U32		idx	= 0;
	U64		uIo,uWr;

	// State check
	CCLTRYE ( pStmLoc != NULL, ERROR_NOT_FOUND );

	// Thread safety
	csIO.enter();

	// Valid sequence number ?
	CCLTRYE ( uSq != 0, ERROR_NOT_FOUND );

	// Stream for sequence number
	CCLTRY ( getStreamIdx ( uSq, false, &idx ));

	// Seek to correct position for sequence number
	CCLOK		( uIo = (uSq-uSeqOff[idx])*szHdrSeq; )
	CCLTRY	( pStmsSeq[idx]->seek ( uIo, STREAM_SEEK_SET, &uWr ) );
	CCLTRYE	( uWr == uIo, E_UNEXPECTED );

	// Write header
	CCLTRY ( pStmsSeq[idx]->write ( pHdr, szHdrSeq, &uWr ) );
	CCLTRYE( uWr == szHdrSeq, E_UNEXPECTED );

	// Thread safety
	csIO.leave();

	return hr;
	}	// locPut

HRESULT Temporal :: locPut ( LOCID uId, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to write a value to a location stream.
	//
	//	PARAMETERS
	//		-	uId is the location Id
	//		-	v is the value to store.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	bool			bVar	= false;
	U32			idx	= 0;
	U64			uIo,uPos;
	HDRVARV2		hdrV;
	HDRSEQV2		hdrS;
	NSSQNM		uSeq,uSeqL;
	adtDate		dNow;

	// State check
	CCLTRYE ( pStmLoc != NULL, ERROR_NOT_FOUND );

	// Thread safety
	csIO.enter();

	// Variable length value ?
	bVar = (	(adtValue::type(v) == VTYPE_STR) ||
				(adtValue::type(v) == VTYPE_UNK) );

	// Stream for next sequence number.  The next sequence number may
	// change so get index first.
	CCLTRY ( getStreamIdx ( uSeqNum, false, &idx ) );

	// Next sequence number
	uSeq = uSeqNum++;

	// Now
	dNow.now();

	//
	// Value.
	//	Variable length values are stored in the value stream.
	//
	if (hr == S_OK && bVar)
		{
		// Obtain end-of-file position for next value
		CCLTRY ( pStmsVar[idx]->seek ( 0, STREAM_SEEK_END, &uPos ) );

		// Save value header
		hdrV.v1.lid		= uId;
		hdrV.v1.seqnum = uSeq;
		hdrV.date		= dNow;
		CCLTRY ( pStmsVar[idx]->write ( &hdrV, szHdrVar, &uIo ) );
		CCLTRYE( uIo == szHdrVar, E_UNEXPECTED );

		// Save value
		CCLTRY ( pPrsrS->save ( pStmsVar[idx], v ) );
		}	// if

	// Read latest sequence number
	CCLTRY ( locGet ( uId, &uSeqL ) );

	//
	// Location header
	//

	// Previous emission ?
	if (hr == S_OK && uSeqL != 0)
		{
		// Read latest emission
		CCLTRY ( locGet ( uSeqL, &hdrS ) );

		// New latest sequence number
		CCLOK ( hdrS.v1.next = uSeq; )

		// Update
		CCLTRY ( locPut ( uSeqL, &hdrS ) );
		}	// if

	// Save latest emission header/value
	if (hr == S_OK)
		{
		// Fill header and value.  
		hdrS.v1.lid		= uId;
		hdrS.v1.value	= v;
		hdrS.v1.prev	= uSeqL;
		hdrS.v1.next	= 0;
		hdrS.date		= dNow;

		// If variable length value, the position within
		// the value stream is stored as an integer in the value header.
		if (bVar)
			hdrS.v1.value.vlong = uPos;

		// Save info.
		CCLTRY ( locPut ( uSeq, &hdrS ) );
		}	// if

	// Write new latest sequence number for location
	CCLTRY ( locPut ( uId, uSeq ) );

	// Thread safety
	csIO.leave();

	return hr;
	}	// locPut

HRESULT Temporal :: open ( IDictionary *pOpts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM		IResource
	//
	//	PURPOSE
	//		-	Open a Temporal database.
	//
	//	PARAMETERS
	//		-	pOpts is a ptr. to the options dictionary
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr				= S_OK;
	IDictionary	*pStmOpts	= NULL;
	IByteStream	*pStmMap		= NULL;
	IUnknown		*pLoc			= NULL;
	adtBool		bRO(false);
	adtBool		bTrunc(false);
	adtValue		v;
	adtString	strFile;
	adtIUnknown	unkV;
	U64			pos;
	bool			bMap;

	// Stream locations (required)
	CCLTRY ( pOpts->load ( adtStringSt(L"Locations"), v ) );
	CCLTRY ( _QISAFE(v,IID_ILocations,&pStmSrc) );

	// Location inside stream source for root of database (required)
	CCLTRY ( pOpts->load ( adtStringSt(L"Location"), v ) );
	CCLTRYE( (strLoc = v).length() > 0, E_UNEXPECTED );

	// Read-only access to database ?
	if (hr == S_OK && pOpts->load ( adtStringSt(L"ReadOnly"), v ) == S_OK)
		bRO = v;
	if (hr == S_OK && pOpts->load ( adtStringSt(L"Truncate"), v ) == S_OK)
		bTrunc = v;

	// Non-sensible options
	CCLTRYE ( bRO == false || bTrunc == false, E_INVALIDARG );

	// Options for streams
	CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pStmOpts));

	//
	// LOC.BIN contains the sequence number of the first/latest value
	// of the location.
	//

	// Location of file
	CCLOK  ( strFile = strLoc; )
	CCLTRY ( strFile.append ( L"loc.bin" ) );

	// Access stream
	lprintf ( LOG_DBG, L"Location loc:%s", (LPCWSTR) strFile );
	CCLTRY ( pStmOpts->clear() );
	CCLTRY ( pStmOpts->store ( adtStringSt(L"Location"),	strFile ) );
	CCLTRY ( pStmOpts->store ( adtStringSt(L"ReadOnly"),	bRO ) );
	if (hr == S_OK && !bRO)
		{
		CCLTRY ( pStmOpts->store ( adtStringSt(L"Create"),		adtBool(true) ) );
		CCLTRY ( pStmOpts->store ( adtStringSt(L"Truncate"),	bTrunc ) );
		}	// if
	CCLTRY ( pStmSrc->open ( pStmOpts, &pLoc ) );
	CCLTRY ( _QI(pLoc,IID_IByteStream,&pStmLoc) );
	_RELEASE(pLoc);

	// Create new dictionary map
	CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctMap));

	// Format of location file is the sequence number of the latest value
	// for a location.  Each location Id is the position within the file.
	// After each sequence number the full path of the location is stored.
	// So <Sequence><Path><Sequence><Path>...

	// The size of the file will correlate with the next available location Id.
	CCLTRY ( pStmLoc->seek ( 0, STREAM_SEEK_END, &pos ) );
	if (hr == S_OK && pos == 0)
		{
		NSSQNM ver = 2;

		// Store version number in first entry followed by null string
		CCLTRY ( pStmLoc->write ( &ver, sizeof(ver), NULL ) );
		CCLTRY ( pPrsrS->save ( pStmLoc, adtString(L"") ) );
		}	// if

	//
	// Value streams
	//

	// Default last sequence number (first entry if reserved)
	uSeqNum		= 0;
	uSeqOff[0]	= MAXUINT64;
	uSeqOff[1]	= MAXUINT64;

	// Open any existing value streams (if truncate not set)
	if (hr == S_OK && !bTrunc)
		{
		CCLOK ( openStreams ( 0, bRO, true, false, 0 ); )
		CCLOK ( openStreams ( 1, bRO, false, false, 0 ); )
		}	// if

	// If new database, create initial stream pair
	if (hr == S_OK && uSeqOff[0] == MAXUINT64 && !bRO)
		hr = openStreams ( 0, false, true, true, 0 );

	// Figure out what the next sequence number will be for writing.
	// Use the end of the highest number stream pair.
	if (hr == S_OK)
		{
		// Stream index to use
		S32 
		iStmIdx =	(	uSeqOff[1] == MAXUINT64 || 
							uSeqOff[0] > uSeqOff[1]) ? 0 : 1;

		// Seek to end of stream
		CCLTRY ( pStmsSeq[iStmIdx]->seek ( 0, STREAM_SEEK_END, &uSeqNum ) );

		// Calculate the last sequence number (zero-based)
		uSeqNum = (uSeqNum/szHdrSeq)+uSeqOff[iStmIdx];
		}	// if

	//
	// Location map
	//
	bMap	= false;
	_RELEASE(pStmMap);
	if (hr == S_OK && !bMap)
		{
		U64	uSz,uPos;

		//
		// Generate location map.  A dictionary tree is cached to keep track of
		// location vs. Id for quick look-up.  Generate cache
		// by reading the locations into the map.
		//

		// Size of the file
		CCLTRY ( pStmLoc->seek ( 0, STREAM_SEEK_END, &uSz ) );

		// Beginning of file
		CCLTRY ( pStmLoc->seek ( 0, STREAM_SEEK_SET, &uPos ) );

		// Scan until complete
		while (hr == S_OK && uPos < uSz)
			{
			NSSQNM	seqL;

			// Read sequence number
			CCLTRY ( pStmLoc->read ( &seqL, sizeof(seqL), NULL ) );

			// Read location string
			CCLTRY ( pPrsrL->load ( pStmLoc, v ) );

			// Expecting namespace path string
			CCLTRYE ( (adtValue::type(v) == VTYPE_STR) && (v.pstr != NULL), E_UNEXPECTED );

			// DEBUG.  Used to fix some corrupted database, useful in future ?
//			bool bDup = false;
//			if (hr == S_OK && wcslen(v.pstr) > 0)
//				{
//				adtValue vL;
//				if (nspcLoadPath ( pDctMap, v.pstr, vL ) == S_OK)
//					{
//					lprintf ( LOG_DBG, L"Duplicate path %s\r\n", v.pstr );
//					bDup = true;
//					}	// if
//				}	// if

			// Store location id (which is the position) in map
//			CCLOK ( lprintf ( LOG_DBG, L"Location : %s\r\n", v.pstr ); )
			//if (hr == S_OK && !bDup)
//				{
			CCLTRY ( nspcStoreValue ( pDctMap, v.pstr, adtInt((U32)uPos) ) );
//				}	// if

			// Stream position
			CCLTRY ( pStmLoc->seek ( 0, STREAM_SEEK_CUR, &uPos ) );
			}	// for

		// Clean up
		_RELEASE(pStmMap);
		}	// if

	//
	// TESTING transition to database usage SO SLOW
	//
	/*
	if (hr == S_OK)
		{
		IResource	*pRes		= NULL;

		// Database options
		CCLTRY ( pDctOpts->clear() );
		CCLTRY ( pDctOpts->store ( adtString(L"Location"), 
											adtString(L"C:/dev/contrast/c/build/install/bin/Data/temporal.db") ) );
		CCLTRY ( pDctOpts->store ( adtString(L"ReadOnly"), adtBool(false) ) );
		CCLTRY ( pDctOpts->store ( adtString(L"Create"), adtBool(true) ) );

		// Access database
		CCLTRY(COCREATE(L"Sql.TemporalSql",IID_IResource,&pRes));
		CCLTRY(pRes->open(pDctOpts));
		CCLTRY(_QI(pRes,IID_IDictionary,&pDctSql));

		// Clean up
		_RELEASE(pRes);

		// Do not error out on test code
		hr = S_OK;
		}	// if
	*/

	// Clean up
	_RELEASE(pStmOpts);

	return hr;
	}	// open

HRESULT Temporal :: open ( IDictionary *pOpts, IUnknown **ppLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Open a a location inside the locations.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the stream.
	//		-	ppLoc will receive the location object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	int			len;
	adtValue		vL;
	adtBool		bRO(true);
	adtString	strLoc;

	// Access options
	CCLTRY ( pOpts->load ( strnRefLoc, vL ) );
	CCLTRYE( (len = (strLoc = vL).length()) > 0, E_UNEXPECTED );
	CCLTRY ( pOpts->load ( strnRefRO, vL ) );
	CCLOK  ( bRO = vL; )

	// If read-only, ensure location even exists
//	if (hr == S_OK && bRO)
//		hr = locId ( strLoc, true, &uId );

	// If the path ends in a slash then assume opening a location
	// is needed.
	if (hr == S_OK && strLoc[len-1] == '/')
		{
		TemporalLoc	*pDct	= NULL;

		// Create temporal dictionary for location
		CCLTRYE	( (pDct = new TemporalLoc ( this, strLoc, bRO )) != NULL, 
						E_OUTOFMEMORY );
		CCLOK		( pDct->AddRef(); )
		CCLTRY	( pDct->construct() );

		// Result
		CCLTRY	( _QI(pDct,IID_IUnknown,ppLoc) );

		// Clean up
		_RELEASE(pDct);
		}	// if

	// A path that does not end in a slash is assume to be a value
	// and the caller wants temporal access to a particular value
	else if (hr == S_OK)
		{
		TemporalValue	*pDct	= NULL;

		// Create temporal value for location
		CCLTRYE	( (pDct = new TemporalValue ( this, strLoc )) != NULL, 
						E_OUTOFMEMORY );
		CCLOK		( pDct->AddRef(); )
		CCLTRY	( pDct->construct() );

		// Result
		CCLTRY	( _QI(pDct,IID_IUnknown,ppLoc) );

		// Clean up
		_RELEASE(pDct);
		}	// else if

	return hr;
	}	// open

HRESULT Temporal :: openStreams ( U32 idx, bool bRO, bool bCr,
												bool bTrunc, NSSQNM uSqOff )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Open the necessary streams for the specified index.
	//
	//	PARAMETERS
	//		-	idx is the stream index
	//		-	bRO is true for read-only, false for writable (create)
	//		-	bCr is true to create if files do not exist
	//		-	bTrunc is true to create new files
	//		-	uSqOff is the sequence number offset for new streams.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr				= S_OK;
	IDictionary	*pStmOpts	= NULL;
	IUnknown		*pLoc			= NULL;
	U64			pos			= 0;
	adtString	strFile;

	// Free previous streams
	_RELEASE(pStmsSeq[idx]);
	_RELEASE(pStmsVar[idx]);

	// Options for streams
	CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pStmOpts));

	//
	// VAR.BIN.  Variable sized recorded values (strings and objects).
	//

	// Location of file
	CCLOK  ( strFile = strLoc; )
	CCLTRY ( strFile.append ( (idx == 0) ? L"var.bin" : L"var1.bin" ) );

	// Access stream
	CCLOK ( lprintf ( LOG_DBG, L"Location var:%s", (LPCWSTR) strFile ); )
	CCLTRY ( pStmOpts->store ( adtStringSt(L"Location"),	strFile ) );
	CCLTRY ( pStmOpts->store ( adtStringSt(L"ReadOnly"),	adtBool(bRO) ) );
	if (hr == S_OK && !bRO)
		{
		CCLTRY ( pStmOpts->store ( adtStringSt(L"Create"),		adtBool(bCr) ) );
		CCLTRY ( pStmOpts->store ( adtStringSt(L"Truncate"),	adtBool(bTrunc) ) );
		}	// if
	CCLTRY ( pStmSrc->open ( pStmOpts, &pLoc ) );
	CCLTRY ( _QI(pLoc,IID_IByteStream,&pStmsVar[idx]) );
	_RELEASE(pLoc);

	// If a new stream, store first value as version
	CCLTRY ( pStmsVar[idx]->seek ( 0, STREAM_SEEK_END, &pos ) );
	if (hr == S_OK && pos == 0)
		{
//		HDRVAR		val;
//		val.lid		= 1;
//		val.seqnum	= 0;
		HDRVARV2			val;
		val.v1.lid		= 2;
		val.v1.seqnum	= 0;
		val.date			= 0;
		CCLTRY ( pStmsVar[idx]->write ( &val, sizeof(val), NULL ) );
		}	// if

	// Read version of variable stream
	if (hr == S_OK && verVar == 0)
		{
		HDRVAR		var;

		// Read version from first header
		CCLTRY ( pStmsVar[idx]->seek ( 0, STREAM_SEEK_SET, &pos ) );
		CCLTRY ( pStmsVar[idx]->read ( &var, sizeof(var), NULL ) );

		// Version
		CCLOK		( verVar = (U32)var.lid; )
		}	// if

	// Size of property header based on version
	CCLTRYE	( (verVar > 0 && verVar <= 2), E_UNEXPECTED );
	CCLOK ( szHdrVar =	(verVar == 1) ? sizeof(HDRVAR) :
								(verVar == 2) ? sizeof(HDRVARV2) : 0; )

	//
	// SEQ.BIN contains fixed size emission headers.  Position in
	// file is correlated with the sequence number.  Each header contains
	//	pointer to value in value file.
	//

	// Location of file
	CCLOK  ( strFile = strLoc; )
	CCLTRY ( strFile.append ( (idx == 0) ? L"seq.bin" : L"seq1.bin" ) );

	// Access stream
	CCLOK ( lprintf ( LOG_DBG, L"Location seq:%s", (LPCWSTR) strFile ); )
	CCLTRY ( pStmOpts->store ( adtStringSt(L"Location"),	strFile ) );
	CCLTRY ( pStmOpts->store ( adtStringSt(L"ReadOnly"),	adtBool(bRO) ) );
	if (hr == S_OK && !bRO)
		{
		CCLTRY ( pStmOpts->store ( adtStringSt(L"Create"),		adtBool(bCr) ) );
		CCLTRY ( pStmOpts->store ( adtStringSt(L"Truncate"),	adtBool(bTrunc) ) );
		}	// if
	CCLTRY ( pStmSrc->open ( pStmOpts, &pLoc ) );
	CCLTRY ( _QI(pLoc,IID_IByteStream,&pStmsSeq[idx]) );
	_RELEASE(pLoc);

	// Seek to end of file for size
	CCLTRY ( pStmsSeq[idx]->seek ( 0, STREAM_SEEK_END, &pos ) );

	// If file is new, initialize the first entry with the sequence offset.
	if (hr == S_OK && pos == 0 && !bRO)
		{
		HDRSEQV2	hdr;
//		HDRSEQ	hdr;

		// Write version and sequence offset
		memset ( &hdr, 0, sizeof(hdr) );
		hdr.v1.lid	= 0x2;
		hdr.v1.next	= uSqOff;
//		hdr.lid	= 0x1;
//		hdr.next	= uSqOff;
		hr = pStmsSeq[idx]->write ( &hdr, sizeof(hdr), NULL );
		}	// if

	// Read header from beginning of file to get info.
	if (hr == S_OK)
		{
		HDRSEQ	hdr;

		// Beginning of file
		CCLTRY ( pStmsSeq[idx]->seek ( 0, STREAM_SEEK_SET, NULL ) );

		// Header
		CCLTRY ( pStmsSeq[idx]->read ( &hdr, sizeof(hdr), NULL ) );

		// Version
		CCLOK	( verSeq = (U32)hdr.lid; )

		// Initialize the sequence number offset for this stream
		CCLOK ( uSeqOff[idx] = hdr.next; )
		}	// if

	// Size of property header based on version
	CCLTRYE	( (verSeq > 0 && verSeq <= 2), E_UNEXPECTED );
	CCLOK ( szHdrSeq =	(verSeq == 1) ? sizeof(HDRSEQ) :
								(verSeq == 2) ? sizeof(HDRSEQV2) : 0; )

	// If there was an error, streams are invalid
	if (hr != S_OK)
		{
		uSeqOff[idx] = MAXUINT64;
		_RELEASE(pStmsSeq[idx]);
		_RELEASE(pStmsVar[idx]);
		}	// if

	// Clean up
	_RELEASE(pStmOpts);

	return hr;
	}	// openStreams

HRESULT Temporal :: record ( INamespace *pSpc, const WCHAR *wPath, 
										const WCHAR *wPathTmp, bool bRec, bool bFrom,
										bool bRo, bool bInit )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Initialize and turn on or off recording for a portion of the 
	//			namespace.
	//
	//	PARAMETERS
	//		-	pSpc is the namespace to use
	//		-	wPath is the namespace path to record.
	//		-	wPathTmp is the temporal path to use, usually the same as
	//			the namespace path.
	//		-	bRec is true to force record, false to turn off any recording.
	//		-	bFrom is true to go from temporal to namepsace (load namespace
	//			with values from recorded values), false to go to temporal
	//			from namespace (record value already in namespace).
	//			current value from the namespace.
	//		-	bRo is true for read-only, false for writable
	//		-	bInit is true if initializing an instance for the first time.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IDictionary	*pDesc	= NULL;
	IDictionary	*pLocRec	= NULL;
	IDictionary	*pLoc		= NULL;
	IIt			*pIt		= NULL;
	adtValue		vK,vV,vL;
	adtIUnknown	unkV;
	adtString	strPathLoc(wPath),strPathLocTmp(wPathTmp);
	adtString	strType,strV;

	// This function requires trailing slash
	if (hr == S_OK && strPathLoc[strPathLoc.length()-1] != '/')
		hr = strPathLoc.append ( L"/" );
	if (hr == S_OK && strPathLocTmp[strPathLocTmp.length()-1] != '/')
		hr = strPathLocTmp.append ( L"/" );

	// Access the namespace location
//	CCLTRY ( nspcLoadPath ( pDctRt, strPathLoc, vV ) );
	CCLTRY ( pSpc->get ( strPathLoc, vV, NULL ) );
	CCLTRY ( _QISAFE((unkV = vV),IID_IDictionary,&pLoc) );

	// Check if location is a node (has a descriptor with an attached behaviour)
	if (	hr == S_OK													&&
			pLoc->load ( strnRefDesc, vL ) == S_OK				&&
			(IUnknown *)(NULL) != (unkV = vL)					&&
			_QI(unkV,IID_IDictionary,&pDesc) == S_OK			&&
			pDesc->load ( strnRefType, vL ) == S_OK			&&
			(strType = vL).length() > 0							&&
			!WCASECMP(strType,L"Behaviour")						&&
			pDesc->load ( strnRefBehave, vL ) == S_OK			&&
			(strV = vL).length() > 0 )
		{
		// nSpace value ?
		if (!WCASECMP(strV,L"Nspc.Value"))
			{
			adtString	strLoct(strPathLoc),strLocTmpt(strPathLocTmp);

			// Temporal location of values
			CCLTRY ( strLoct.append ( L"OnFire/" ) );
			CCLTRY ( strLocTmpt.append ( L"OnFire/" ) );

			// Prepare to receive stats
			CCLOK ( pDctStat->clear(); )

			// Enable recording for pre-existing values and forced recordings.
			if (	hr == S_OK	&&
					(
						// Initializing temporal values that already exist
						(	bInit &&
							status ( strLocTmpt, pDctStat ) == S_OK &&
							pDctStat->load ( strnRefLoc, vL ) == S_OK )

						||

						// Forced recording
						!bInit
					)
				)	// if

				{
				// Access location
				CCLTRY ( pLoc->load ( adtStringSt(L"OnFire"), vV ) );
				CCLTRY ( _QISAFE((unkV = vV),IID_IDictionary,&pLocRec) );

				// Persistence
				CCLOK ( record ( strLoct, strLocTmpt, pLocRec, (bInit) ? true : bRec, bFrom, bRo ); )

				// Clean up
				_RELEASE(pLocRec);
				}	// if

			}	// if (!WCASECMP(strV,L"Nspc.Value"))

		}	// if

	// Not a node, process sub-locations
	else if (hr == S_OK)
		{
		// Iterate the possible sub-locations
		CCLTRY ( pLoc->keys ( &pIt ) );
		while (hr == S_OK && pIt->read ( vK ) == S_OK)
			{
			ILocation	*pLocLoc	= NULL;
			adtString	strName(vK);

			// Check if entry is, itself, a sublocation
			if (	WCASECMP(strName,STR_NSPC_PARENT)					&&
					pLoc->load ( vK, vV )				== S_OK			&&
					(IUnknown *)(NULL)					!= (unkV = vV)	&&
					_QI(unkV,IID_ILocation,&pLocLoc)	== S_OK )
				{
				adtString	strPathLocSub,strPathLocSubTmp;

				// Path to this sublocation
				CCLOK	( adtValue::copy ( strPathLoc, strPathLocSub ); )
				CCLTRY( strPathLocSub.append ( strName ) );
				CCLTRY( strPathLocSub.append ( L"/" ) );
				CCLOK	( adtValue::copy ( strPathLocTmp, strPathLocSubTmp ); )
				CCLTRY( strPathLocSubTmp.append ( strName ) );
				CCLTRY( strPathLocSubTmp.append ( L"/" ) );

				// Recursively record sub-location
				hr = record ( pSpc, strPathLocSub, strPathLocSubTmp, bRec, bFrom, bRo, bInit );

				// Clean up
				_RELEASE(pLocLoc);
				}	// if

			// Check the next key
			pIt->next();
			}	// while

		// Clean up
		_RELEASE(pIt);
		}	// else if

	// Clean up
	_RELEASE(pDesc);
	_RELEASE(pLoc);

	return hr;
	}	// record

/*
HRESULT Temporal :: record ( INamespace *pSpc, const WCHAR *wPath, 
										const WCHAR *wPathTmp, bool bRec, bool bFrom,
										bool bRo, bool bInit )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Initialize and turn on or off recording for a portion of the 
	//			namespace.
	//
	//	PARAMETERS
	//		-	pSpc is the namespace to use
	//		-	wPath is the namespace path to record.
	//		-	wPathTmp is the temporal path to use, usually the same as
	//			the namespace path.
	//		-	bRec is true to force record, false to turn off any recording.
	//		-	bFrom is true to go from temporal to namepsace (load namespace
	//			with values from recorded values), false to go to temporal
	//			from namespace (record value already in namespace).
	//			current value from the namespace.
	//		-	bRo is true for read-only, false for writable
	//		-	bInit is true if initializing an instance for the first time.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IDictionary	*pLoc		= NULL;
	IIt			*pIt		= NULL;
	adtValue		vK,vV;
	adtIUnknown	unkV;
	adtString	strPathLoc(wPath),strPathLocTmp(wPathTmp);

	// This function requires trailing slash
	if (hr == S_OK && strPathLoc[strPathLoc.length()-1] != '/')
		hr = strPathLoc.append ( L"/" );
	if (hr == S_OK && strPathLocTmp[strPathLocTmp.length()-1] != '/')
		hr = strPathLocTmp.append ( L"/" );

	// Access the namespace location
//	CCLTRY ( nspcLoadPath ( pDctRt, strPathLoc, vV ) );
	CCLTRY ( pSpc->get ( strPathLoc, vV, NULL ) );
	CCLTRY ( _QISAFE((unkV = vV),IID_IDictionary,&pLoc) );

	// Debug
//	if (!WCASECMP(wPath,L"/apps/auto/test/editor/interface/BrowseTo/interface/IntfNodes/"))
//		dbgprintf ( L"Hi\r\n" );

	// Iterate the sub-locations
	CCLTRY ( pLoc->keys ( &pIt ) );
	while (hr == S_OK && pIt->read ( vK ) == S_OK)
		{
		IDictionary	*pDesc	= NULL;
		IDictionary	*pLocDct	= NULL;
		IDictionary	*pLocRec	= NULL;
		ILocation	*pLocLoc	= NULL;
		adtIUnknown	unkV;
		adtValue		v;
		adtString	strV,strPathLocSub,strPathLocSubTmp;
		adtString	strName(vK);
		adtString	strType;

		// Path to this sublocation
		CCLOK	( adtValue::copy ( strPathLoc, strPathLocSub ); )
		CCLTRY( strPathLocSub.append ( strName ) );
		CCLTRY( strPathLocSub.append ( L"/" ) );
		CCLOK	( adtValue::copy ( strPathLocTmp, strPathLocSubTmp ); )
		CCLTRY( strPathLocSubTmp.append ( strName ) );
		CCLTRY( strPathLocSubTmp.append ( L"/" ) );

		// Debug
//		if (hr == S_OK && !WCASECMP(strName,L"TupleTest"))
//			dbgprintf ( L"Hi\r\n" );

		// Check if item is a location
		if (	hr == S_OK &&
				WCASECMP(strName,STR_NSPC_PARENT)					&&
				pLoc->load ( vK, vV )				== S_OK			&&
				(IUnknown *)(NULL)					!= (unkV = vV)	&&
				_QI(unkV,IID_ILocation,&pLocLoc)	== S_OK )
			{
			// Check if location has a descriptor with an attached behaviour
			if (	hr == S_OK													&&
					_QI(unkV,IID_IDictionary,&pLocDct)	== S_OK		&&
					pLocDct->load ( strnRefDesc, v )		== S_OK		&&
					(IUnknown *)(NULL) != (unkV = v)						&&
					_QI(unkV,IID_IDictionary,&pDesc) == S_OK			&&
					pDesc->load ( strnRefType, v ) == S_OK				&&
					(strType = v).length() > 0								&&
					!WCASECMP(strType,L"Behaviour")						&&
					pDesc->load ( strnRefBehave, v ) == S_OK			&&
					(strV = v).length() > 0 )
				{
				// nSpace value ?
				if (!WCASECMP(strV,L"Nspc.Value"))
					{
					adtString	strLoct(strPathLocSub),strLocTmpt(strPathLocSubTmp);

					// Temporal location of values
					CCLTRY ( strLoct.append ( L"OnFire/" ) );
					CCLTRY ( strLocTmpt.append ( L"OnFire/" ) );

					// Prepare to receive stats
					CCLOK ( pDctStat->clear(); )

					// Enable recording for pre-existing values and forced recordings.
					if (	hr == S_OK	&&
							(
								// Initializing temporal values that already exist
								(	bInit &&
									status ( strLocTmpt, pDctStat ) == S_OK &&
									pDctStat->load ( strnRefLoc, v ) == S_OK )

								||

								// Forced recording
								!bInit
							)
						)	// if

						{
						// Access location
						CCLTRY ( pLocDct->load ( adtStringSt(L"OnFire"), vV ) );
						CCLTRY ( _QISAFE((unkV = vV),IID_IDictionary,&pLocRec) );

						// Persistence
						CCLOK ( record ( strLoct, strLocTmpt, pLocRec, (bInit) ? true : bRec, bFrom, bRo ); )

						// Clean up
						_RELEASE(pLocRec);
						}	// if

					}	// if (!WCASECMP(strV,L"Nspc.Value"))

				}	// if

			// Process sub locations
			else
				hr = record ( pSpc, strPathLocSub, strPathLocSubTmp, bRec, bFrom, bRo, bInit );
			}	// if

		// Clean up
		_RELEASE(pDesc);
		_RELEASE(pLocDct);
		_RELEASE(pLocLoc);

		// Clean up
		pIt->next();
		}	// while

	// If caller explicity requested to turn on/off recording, flag the graph
	if (hr == S_OK && !bInit)
		{
		// Persist enable/disable
//		if (bRec)
//			hr = pGr->store ( strnRefPers, adtBool(true) );
//		else
//			pGr->remove ( strnRefPers );
		}	// if

	// Clean up
	_RELEASE(pIt);
	_RELEASE(pLoc);

	return hr;
	}	// record
*/

HRESULT Temporal :: record ( const WCHAR *wPath, const WCHAR *wPathTmp,
										IDictionary *pLoc, bool bRec, bool bFrom, bool bRo )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		INamespace
	//
	//	PURPOSE
	//		-	Record/stop recording a location.
	//
	//	PARAMETERS
	//		-	wPath is the path of the location
	//		-	wPathTmp is the temporal path to use, usually the same as
	//			the namespace path.
	//		-	pLoc is the location
	//		-	bRec is true to record, false to stop recording.
	//		-	bFrom is true to go from temporal to namepsace (load namespace
	//			with values from recorded values), false to go to temporal
	//			from nsmapce (record value already in namespace).
	//			current value from the namespace.
	//		-	bRo is true for read-only, false for writable
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr	= S_OK;
	adtValue		vL;
	adtIUnknown	unkV;

	// Debug
//	lprintf ( LOG_DBG, L"%p:%d:%s:%s\r\n", pLoc, bRec, 
//					(bFrom) ? L"from" : L"to", wPath );

	// Record 
	if (hr == S_OK && bRec)
		{
		// Ensure not already recorded
		if (pDctRec->load ( adtIUnknownRef(pLoc), vL ) != S_OK)
			{
			ILocation	*pLocSrc	= NULL;
			IReceptor	*pRcp		= NULL;
			IUnknown		*punkLoc	= NULL;

			// Open temporal dictionary for location.
			CCLTRY ( pDctOpts->store ( strnRefRO, adtBool(bRo) ) );
			CCLTRY ( pDctOpts->store ( strnRefLoc, adtStringSt(wPathTmp) ) );
			CCLTRY ( open ( pDctOpts, &punkLoc ) );

			// Set-up a mirrored connection between temporal location and
			// namespace location in desired direction.
			if (hr == S_OK && bFrom)
				{
				CCLTRY ( _QISAFE(punkLoc,IID_ILocation,&pLocSrc) );
				CCLTRY ( _QISAFE(pLoc,IID_IReceptor,&pRcp) );
				}	//
			else if (hr == S_OK)
				{
				CCLTRY ( _QISAFE(pLoc,IID_ILocation,&pLocSrc) );
				CCLTRY ( _QISAFE(punkLoc,IID_IReceptor,&pRcp) );
				}	// else if

			// Make connection
			CCLTRY ( pLocSrc->connect ( pRcp, true, true ) );

			// Store as recording 
			CCLTRY ( pDctRec->store ( adtIUnknownRef(pLoc), adtIUnknown(punkLoc) ) );

			// Clean up
			_RELEASE(pRcp);
			_RELEASE(pLocSrc);
			_RELEASE(punkLoc);
			}	// if
		else
			{
			// Debug
			lprintf ( LOG_DBG, L"Namespace::record:Already recorded %s:%s\r\n", wPath, wPathTmp );
			}	// else
		}	// if
	else if (hr == S_OK && !bRec)
		{
		// Access temporal dictionary for location
		if (pDctRec->load ( adtIUnknownRef(pLoc), vL ) == S_OK)
			{
			ILocation	*pLocSrc	= NULL;
			IReceptor	*pRcp		= NULL;
			adtIUnknown	unkV(vL);

			// Disconnect temporal location and namespace location
			CCLTRY(_QISAFE(unkV,IID_ILocation,&pLocSrc));
			CCLTRY(_QISAFE(pLoc,IID_IReceptor,&pRcp) );
			CCLTRY( pLocSrc->connect ( pRcp, false, false ) );

			// Remove recording from dictionary.
			pDctRec->remove ( adtIUnknownRef(pLoc) );

			// Clean up
			_RELEASE(pRcp);
			_RELEASE(pLocSrc);
			}	// if

		// Remove recording from dictionary.  This should clean up the temporal receptor, etc.
//		pDctRec->remove ( adtIUnknownRef(pLoc) );
		}	// else

	// Debug
//	dbgprintf ( L"} Namespace::record:%p:%d:%s:0x%x\r\n", pEmit, bRec, wPath, hr );

	return hr;
	}	// record

HRESULT Temporal :: link ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Create a symbolic link to a location.
	//
	//	PARAMETERS
	//		-	pwF is the source/link location
	//		-	pwT is the destination/target location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// link

HRESULT Temporal :: move ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Move a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// move

HRESULT Temporal :: remove ( const WCHAR *pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Delete the location inside the stream source.
	//
	//	PARAMETERS
	//		-	pwLoc identifies the location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// For database, treat 'root' as deleting the database.
	// TODO: Remove values ?
//	lprintf ( LOG_DBG, L"Temporal remove %s, pStmSrc %p\r\n", pwLoc, pStmSrc );

	// Delete entire database ?
	if (!WCASECMP(pwLoc,L"/"))
		{
		adtString	strStm;
		const WCHAR	*wFiles[] = {	L"var.bin", L"var1.bin",
											L"seq.bin", L"seq1.bin",
											L"loc.bin", L"" };

		// State check
		CCLTRYE ( pStmSrc != NULL, ERROR_INVALID_STATE );

		// Ensure database is closed
		close();

		// Remove locations used by database
		for (int i = 0;hr == S_OK && wFiles[i][0] != WCHAR('\0');++i)
			{
			// Delete location
			CCLTRY ( adtValue::copy ( strLoc, strStm ) );
			CCLTRY ( strStm.append(wFiles[i]) );
			CCLOK ( pStmSrc->remove ( strStm ); )

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Unable to delete file '%s' (%s) 0x%x\r\n", 
								wFiles[i], (LPCWSTR)strStm, hr );
			}	// for

		}	// if

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Error executing temporal remove of '%s' pStmSrc %p 0x%x\r\n",
							pwLoc, pStmSrc, hr );

	return hr;
	}	// remove

HRESULT Temporal :: resolve ( const WCHAR *pwLoc, bool bAbs, 
												ADTVALUE &vLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Resolve provided location to an absolute or relative path
	//			within the stream source.
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//		-	bAbs is true to produce an absolute path, or false to produce
	//			a relative path
	//		-	vLoc will receive the new location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// TODO: Make sense ?
	return E_NOTIMPL;
	}	// resolve

HRESULT Temporal :: setMaximum ( U64 _max )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ITemporal
	//
	//	PURPOSE
	//		-	Set the maximum number of sequence numbers allowed (will allow
	//			up to 2x).  Set to zero for no limit.
	//
	//	PARAMETERS
	//		-	_max is the new maximum
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Set new maximum for future storage
	uSeqMax = _max;

	return hr;
	}	// setMaximum

HRESULT Temporal :: status ( const WCHAR *wLoc, IDictionary *pSt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ITemporal
	//
	//	PURPOSE
	//		-	Returns information about the stream at the specified location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	pSt will receive the information about the stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	adtValue		v;

	// Check for location in map
	CCLTRY ( nspcLoadPath ( pDctMap, wLoc, v ) );
//	CCLTRY ( pDctMap->load ( adtString(wLoc), v ) );

	// A number at location means path is a value
	if (hr == S_OK && v.vtype == VTYPE_I4)
		{
		// Value location
		CCLTRY ( pSt->store ( strnRefVal, adtBool(true) ) );
		}	// if

	// An object (dictionary) means path is a location
	else if (hr == S_OK && v.vtype == VTYPE_UNK)
		{
		// Mark as location
		CCLTRY ( pSt->store ( strnRefLoc, adtBool(true) ) );
		}	// else if

	else if (hr == S_OK)
		hr = E_UNEXPECTED;

	return hr;
	}	// status

