////////////////////////////////////////////////////////////////////////
//
//									TEMPLOC.CPP
//
//				Implementation of the temporal location
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"
#include <stdio.h>

// Globals
extern GlobalNspc	nspcglb;

TemporalValue :: TemporalValue ( Temporal *_pTmp, const WCHAR *_pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	//	PARAMETERS
	//		-	_pTmp is the temporal implementation backing dictionary
	//		-	_pwLoc is the namespace location of the value
	//
	////////////////////////////////////////////////////////////////////////
	pTmp			= _pTmp; _ADDREF(pTmp);
	strLoc		= _pwLoc;
	strLoc.at();
	uId			= 0;
	}	// TemporalValue

HRESULT TemporalValue :: clear ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Resets the container.
	//
	////////////////////////////////////////////////////////////////////////

	// Read only
	return ERROR_ACCESS_DENIED;

	}	// clear

HRESULT TemporalValue :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	CCLObject
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;

	// Obtain the location is for the specified path.  If it does not
	// exist, future calls will error out, this is a read-only object for
	// now.
	if (pTmp->locId(strLoc,true,&uId) != S_OK)
		lprintf ( LOG_DBG, L"Warning, location does not exist in database '%s'\r\n",
									(LPCWSTR) strLoc );

	return hr;
	}	// construct

HRESULT TemporalValue :: copyTo ( IContainer *pCont )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ICopyTo
	//
	//	PURPOSE
	//		-	Copies values from this container to another container.
	//
	//	PARAMETERS
	//		-	pCont will receive the values
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// State check
	CCLTRYE ( uId != 0,	ERROR_INVALID_STATE );

	// Copy entire history of value into a container ?
	return E_NOTIMPL;
	}	// copyTo

void TemporalValue :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	_RELEASE(pTmp);

	}	// destruct

HRESULT TemporalValue :: isEmpty ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Returns the empty state of the container
	//
	//	RETURN VALUE
	//		S_OK if container is empty
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// State check
	CCLTRYE ( uId != 0,	ERROR_INVALID_STATE );

	// TODO
	return S_FALSE;
	}	// isEmpty

HRESULT TemporalValue :: iterate ( IIt **ppIt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Returns an iterator for the container.
	//
	//	PARAMETERS
	//		-	ppIt will receive a ptr. to the iterator
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr		= S_OK;
	TemporalValueIt	*pIt	= NULL;

	// State check
	CCLTRYE ( uId != 0,	ERROR_INVALID_STATE );

	// Create emitter for caller
	CCLTRYE ( (pIt = new TemporalValueIt(this,false)) != NULL, E_OUTOFMEMORY );
	CCLTRY ( pIt->construct() );

	// Result
	(*ppIt)	= pIt;
	_ADDREF(*ppIt);

	return hr;
	}	// iterate

HRESULT TemporalValue :: keys ( IIt **ppKeys )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITemporalValue
	//
	//	PURPOSE
	//		-	Returns an object to iterate through the keys in the tree.
	//
	//	PARAMETERS
	//		-	ppKeys will receive the iterator
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr		= S_OK;
	TemporalValueIt	*pIt	= NULL;

	// State check
	CCLTRYE ( uId != 0,	ERROR_INVALID_STATE );

	// Create emitter for caller
	CCLTRYE ( (pIt = new TemporalValueIt(this,true)) != NULL, E_OUTOFMEMORY );
	CCLTRY ( pIt->construct() );

	// Result
	(*ppKeys)	= pIt;
	_ADDREF(*ppKeys);

	return hr;
	}	// keys

HRESULT TemporalValue :: load ( const ADTVALUE &vKey, ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITemporalValue
	//
	//	PURPOSE
	//		-	Loads a value from the object with the given key.
	//
	//	PARAMETERS
	//		-	vKey is the sequence number
	//		-	vValue will receive the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtLong	vSeq(vKey);
	HDRSEQV2	hdrL;

	// State check
	CCLTRYE ( uId != 0,	ERROR_INVALID_STATE );
	CCLTRYE ( vSeq != 0,	E_INVALIDARG );

	// Read value
	CCLTRY ( pTmp->locGet ( vSeq, &hdrL, &vValue ) );

	// The first value is for internal use only
//	if (hr == S_OK  && hdrL.prev == 0)
//		hr = ERROR_NOT_FOUND;

	return hr;
	}	// load

HRESULT TemporalValue :: remove ( const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Removes an item from the container identified by the specified
	//			value.
	//
	//	PARAMETERS
	//		-	v identifies the key to remove
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Read only
	return ERROR_ACCESS_DENIED;
	}	// remove

HRESULT TemporalValue :: size ( U32 *s )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Returns the # of items in the container.
	//
	//	PARAMETERS
	//		-	s will return the size of the container
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// State check
	CCLTRYE ( uId != 0,	ERROR_INVALID_STATE );

	// Todo
	*s = 0;
	return E_NOTIMPL;
	}	// size

HRESULT TemporalValue :: store (	const ADTVALUE &vKey, 
											const ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITemporalValue
	//
	//	PURPOSE
	//		-	Stores a value in the TemporalValue with the given key.
	//
	//	PARAMETERS
	//		-	vKey is the key
	//		-	vValue is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Read only
	return ERROR_ACCESS_DENIED;
	}	// store

///////////////////
// TemporalValueIt
///////////////////

TemporalValueIt :: TemporalValueIt ( TemporalValue *_pDct, bool _bKeys )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	//	PARAMETERS
	//		-	_pDct is the parent
	//		-	_bKeys is true to iterator keys
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= _pDct;
	_ADDREF(pDct);
	bKeys		= _bKeys;
	sqnum		= 0;
	}	// TemporalValueIt

HRESULT TemporalValueIt :: begin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IIt
	//
	//	PURPOSE
	//		-	Resets the iterator position within the container.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;
//	HDRSEQV2	hdrS;
	NSSQNM	sqnumL;

	// Valid location ?
	CCLTRYE ( pDct->uId != 0, ERROR_INVALID_STATE );

	// Temporal iterations start with 'now' and go back in time during
	// 'next'ing.

	// Read the latest sequence number
	CCLTRY ( pDct->pTmp->locGet ( pDct->uId, &sqnumL ) );

	// Now at first entry
	CCLOK ( sqnum = sqnumL; )

	// First value is invalid so value at number has to be checked
//	if (	hr == S_OK && 
//			pDct->pTmp->locGet ( sqnum, &hdrS ) == S_OK &&
//			hdrS.v1.prev == 0 )
//		sqnum = 0;											// No values to iterate yet

	return hr;
	}	// begin

HRESULT TemporalValueIt :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being created.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Initialize to beginning
	CCLOK ( begin(); )

	return hr;
	}	// construct

void TemporalValueIt :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pDct);
	}	// destruct

HRESULT TemporalValueIt :: end ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IIt
	//
	//	PURPOSE
	//		-	Resets the iterator position within the container.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;
	HDRSEQV2	hdrS;

	// Valid location ?
	CCLTRYE ( pDct->uId != 0, ERROR_INVALID_STATE );

	// Temporal iterations start with 'now' and go back in time during
	// 'next'ing.

	// Read the latest sequence number
	CCLTRY ( pDct->pTmp->locGet ( pDct->uId, &sqnum ) );

	// Very slow, better way to do this ?  Follow sequence numbers
	// to beginning of available history.
	CCLOK ( hdrS.v1.prev = sqnum; )
	while (hr == S_OK)
		{
		// At first entry ?
		if (hdrS.v1.prev == 0)
			break;

		// At new sequence number
		sqnum = hdrS.v1.prev;

		// Next header
		CCLTRY ( pDct->pTmp->locGet ( sqnum, &hdrS ) );
		}	// while

//	while (	hr == S_OK && 
//				pDct->pTmp->locGet ( sqnum, &hdrS ) == S_OK &&
//				hdrS.prev != 0)
//		sqnum = hdrS.prev;

	return hr;
	}	// end

HRESULT TemporalValueIt :: goTo ( const ADTVALUE &vGoTo, ADTVALUE &vAt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ISearchIt
	//
	//	PURPOSE
	//		-	Seeks to or around the specified value
	//
	//	PARAMETERS
	//		-	vGoTo is the value to seek to
	//		-	vAt is the value where iterator is at
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtLong	lGoTo(vGoTo);
	HDRSEQV2	hdrS;

	// If a previous iteration exhausted itself attempt to reset
	// the search position to the newest point
	if (hr == S_OK && sqnum == 0)
		{
		// Attempt to begin iteration
		CCLTRY ( begin(); )

		// If sequence number is still zero, then no values
		if (hr == S_OK && sqnum == 0)
			hr = ERROR_NOT_FOUND;
		}	// if

	// TODO: In future sequence values for a location will probably need
	// to be a b-tree but for now just a linear search
	// Search until the sequence number at or just past the goto value.
	while (hr == S_OK)
		{
		// Read header for the current sequence number
		CCLTRY ( pDct->pTmp->locGet ( sqnum, &hdrS ) );

		// Valid ?
		if (hr == S_OK)
			{
			// Equal to target
			if (sqnum == lGoTo)
				break;

			// Less than target
			else if (sqnum < lGoTo)
				{
				// Valid next sequence ?
				if (hdrS.v1.next == 0)
					break;

				// Less than target but next is greater than target
				else if (hdrS.v1.next > lGoTo)
					break;

				// Move to next number (if available)
				else
					sqnum = hdrS.v1.next;
				}	// else if

			// Greater than target
			else
				{
				// The first entry is actually not a valid value, so if
				// here then next is real minimum
				if (hdrS.v1.prev == 0)
					{
					sqnum = hdrS.v1.next;
					break;
					}	// if

				// Move to previous sequence
				else
					sqnum = hdrS.v1.prev;
				}	// else
			}	// if

		}	// while

	// Where it ended up
	CCLTRY ( adtValue::copy ( adtLong(sqnum), vAt ) );

	return hr;
	}	// goTo

HRESULT TemporalValueIt :: next ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IIt
	//
	//	PURPOSE
	//		-	Moves to the next position within the container.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	HDRSEQV2	hdrS;

	// State check
	CCLTRYE ( sqnum != 0, ERROR_INVALID_STATE );

	// Read the header at the current sequence number to retrieve the next
	// sequence number for this value

	// Read header
	CCLTRY ( pDct->pTmp->locGet ( sqnum, &hdrS ) );

	// New sequence number
	CCLOK ( sqnum = hdrS.v1.prev; )

	return hr;
	}	// next

HRESULT TemporalValueIt :: prev ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IIt
	//
	//	PURPOSE
	//		-	Moves to the previous position within the container.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	HDRSEQV2	hdrS;

	// State check
	CCLTRYE ( sqnum != 0, ERROR_INVALID_STATE );

	// Read the header at the current sequence number to retrieve the next
	// sequence number for this value

	// Read header
	CCLTRY ( pDct->pTmp->locGet ( sqnum, &hdrS ) );

	// New sequence number
	CCLOK ( sqnum = hdrS.v1.next; )

	return hr;
	}	// prev

HRESULT TemporalValueIt :: read ( ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IIt
	//
	//	PURPOSE
	//		-	Reads the current item from the container.
	//
	//	PARAMETERS
	//		-	v will receive the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	HDRSEQV2	hdrS;

	// State check
	CCLTRYE ( sqnum != 0,	ERROR_INVALID_STATE );

	// Reading the 'end' of the list is also invalid

	// Keys
	if (hr == S_OK && bKeys)
		v = adtLong(sqnum);

	// Read value
	else if (hr == S_OK)
		hr = pDct->pTmp->locGet ( sqnum, &hdrS, &v );

	return hr;
	}	// read
