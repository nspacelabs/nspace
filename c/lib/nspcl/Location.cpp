////////////////////////////////////////////////////////////////////////
//
//								LOC.CPP
//
//					Implementation of the namespace location node.
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"

// Globals
extern GlobalNspc	nspcglb;

Location :: Location ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pDctc			= NULL;
	pDctOut		= NULL;
	pLocOut		= NULL;
	pRcpOut		= NULL;
	pPar			= NULL;
	pDctPar		= NULL;
	pSpc			= NULL;
	bActive		= false;
	bRx			= false;
	bRcp			= false;
	bEmt			= false;
	pRxQ			= NULL;
	pRxIt			= NULL;
	pBehave		= NULL;
	bBehaveV		= false;
//	bLocThis		= false;
//	pRcpConn		= NULL;
	pDctConns	= NULL;
	bLocRef		= false;
	uCntKey		= 0;
	}	// Location

HRESULT Location :: active ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Process the active state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Debug
//	dbgprintf ( L"Location::active:%s:%d\r\n", (LPCWSTR)strName, bActive );

	// Send uninit signal down the chain
	if (hr == S_OK && !bActive)
		receive ( pDctOut, L"Uninitialize/Fire", strName, false );

	// Notify sub-locations
	CCLTRY ( keyNotify(strnRefAct,adtBool(bActive)) );

	// Process descriptor and connections, order depends on active state
	if (hr == S_OK)
		{
		// If active, initialize descriptors first then connect
		if (bActive)
			desc(bActive);
		else
			connect ( pDctConns, bActive );

		// If inactive, disconnect then shutdown descriptors
		if (bActive)
			connect ( pDctConns, bActive );
		else
			desc(bActive);
		}	// if

	// Initialize graphs as they become active
	if (hr == S_OK && bActive)
		receive ( pDctOut, L"Initialize/Fire", strName );

	return hr;
	}	// active

HRESULT Location :: clear ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Resets the container.
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	IList		*pKeys	= NULL;
	IIt		*pIt		= NULL;
	adtValue	vN;

	// To avoid restarting iteration every time there is a 'remove', 
	// pre-generate key list before removal.
	CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pKeys ) );
	CCLTRY ( keys ( &pIt ) );
	while (hr == S_OK && pIt->read ( vN ) == S_OK)
		{
		// Add key to list
		CCLTRY ( pKeys->write ( vN ) );

		// Next entry
		pIt->next();
		}	// while	

	// Clean up
	_RELEASE(pIt);

	// Remove sub-locations before everything else
	CCLTRY ( pKeys->iterate ( &pIt ) );
	while (hr == S_OK && pIt->read ( vN ) == S_OK)
		{
		ILocation	*pSub	= NULL;
		adtString	strKey (vN);
		adtIUnknown	unkV;

		// Debug
//		dbgprintf ( L"Location::clear:%s:%s\r\n", (LPCWSTR)strName, (LPCWSTR)strKey );

		// Remove locations
		if (	strKey[0] != CHR_NSPC_PRE						&&
				strKey[0] != CHR_NSPC_PST						&&
				load ( vN, vN ) == S_OK							&&
				vN.vtype == VTYPE_UNK							&&	// This is here to avoid dealing with references 
																			// to IUnknown that might already be deleted
				(IUnknown *)(NULL) != (unkV=vN)				&&
				_QI (unkV,IID_ILocation,&pSub) == S_OK )
			remove ( strKey );

		// Clean up
		adtValue::clear(vN);
		adtValue::clear(unkV);
		_RELEASE (pSub);
		pIt->next();
		}	// while

	// Clean up
	_RELEASE(pIt);
	_RELEASE(pKeys);

	// Ensure all items are removed
	pDctc->clear();

	// Flush existing value
//	bLocThis = false;
//	adtValue::clear(vLocThis);

	return hr;
	}	// clear

HRESULT Location :: connect ( IContainer *pCs, bool bConnect )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Make/break all connections in the dictionary
	//
	//	PARAEMTERS
	//		-	pCs is the ordered connection list
	//		-	bConnect is true to connect, false to disconnect
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IIt			*pIt		= NULL;
	adtValue		v;
	adtIUnknown	unkV;
	adtString	strE;

	// State check
	CCLTRYE ( pCs != NULL, ERROR_INVALID_STATE );

	// Iterate connection dictionaries in list
	CCLTRY ( pCs->iterate ( &pIt ) );
	if (hr == S_OK && bConnect)
		hr = pIt->begin();
	else if (hr == S_OK)
		{
		pIt->end();
		pIt->prev();
		}	// else if
	while (hr == S_OK && pIt->read (v) == S_OK)
		{
		IDictionary	*pDct	= NULL;
		adtString	strF,strT;

		// Connection information
		CCLTRY ( _QISAFE((unkV=v),IID_IDictionary,&pDct) );
		CCLTRY ( pDct->load ( strnRefFrom, v ) );
		CCLTRYE( (strF = v).length() > 0, E_UNEXPECTED );
		CCLTRY ( pDct->load ( strnRefTo, v ) );
		CCLTRYE( (strT = v).length() > 0, E_UNEXPECTED );
//		CCLOK ( dbgprintf ( L"Location %d:%s -> %s\r\n", idx, (LPCWSTR)strF, (LPCWSTR)strT ); )
//		if (hr == S_OK && !WCASECMP(strF,L"TupleTest/A1/Fire"))
//			dbgprintf ( L"Hi\r\n" );

		// Process connection
		CCLOK ( connect ( strF, strT, bConnect ); )

		// Clean up
		_RELEASE(pDct);

		// Next connection
		if (bConnect)
			pIt->next();
		else
			pIt->prev();
		}	// while

	// Clean up
	_RELEASE(pIt);

	return hr;
	}	// connect

HRESULT Location :: connect (	const WCHAR *wFrom,
										const WCHAR *wTo, bool bConnect )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IGraph
	//
	//	PURPOSE
	//		-	Connects two locations
	//
	//	PARAMETERS
	//		-	wFrom is the 'from' location
	//		-	wTo is the 'to' location
	//		-	bConnect is true to connect, false to disconnect
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	ILocation	*pLoc	= NULL;
	IReceptor	*pR	= NULL;
	adtValue		vF,vT;
	adtIUnknown	unkV;

	// A relative path can be retrieved from the specified graph.
	// An 'external' connection will have to be retrieved from the namespace.

	// State check
	CCLTRYE ( pSpc != NULL, ERROR_INVALID_STATE );

	// Debug
//	lprintf ( LOG_DBG, L"%s : %s -> %s\r\n", (bConnect) ? L"connect" : L"disconnect", wFrom, wTo );
//	if (!WCASECMP(wFrom,L"Initialize/OnFire") && !WCASECMP(wTo,L"Debug/Fire"))
//	if (!WCASECMP(wTo,L"ReceiveVal/Value"))
//	if (!WCASENCMP(wTo,L"StoreImgKeys",12))
//		dbgprintf ( L"Hi\r\n" );

	// 'From'
	if (hr == S_OK)
		{
		// Local connection
		if (wFrom[0] != '/')
			hr = nspcLoadPath ( pDctOut, wFrom, vF );

		// Non-local
		else
			hr = pSpc->get ( wFrom, vF, NULL );
		}	// if

	// 'To'
	if (hr == S_OK)
		{
		// Local connection
		if (wTo[0] != '/')
			hr = nspcLoadPath ( pDctOut, wTo, vT );

		// Non-local
		else
			hr = pSpc->get ( wTo, vT, NULL );
		}	// if

	// Debug.  Sanity check in case a non-emitter is connecting to a non-receptor.
/*
	if (hr == S_OK)
		{
		IDictionary	*pDctTst	= NULL;
		adtValue		vType;

		// Emitter ?
		CCLTRY ( _QISAFE((unkV = vF),IID_IDictionary,&pDctTst) );
		if (	hr == S_OK												&& 
				pDctTst->load ( strnRefType, vType ) == S_OK &&
				WCASECMP(vType.pstr,L"Emitter"))
			dbgprintf ( L"Location::connect:Source not emitter:%s\r\n", wFrom );
		_RELEASE(pDctTst);			

		// Receptor ?
		CCLTRY ( _QISAFE((unkV = vT),IID_IDictionary,&pDctTst) );
		if (	hr == S_OK												&& 
				pDctTst->load ( strnRefType, vType ) == S_OK &&
				WCASECMP(vType.pstr,L"Receptor"))
			dbgprintf ( L"Location::connect:Destination not receptor:%s\r\n", wTo );
		_RELEASE(pDctTst);			
		}	// if
*/

	// Make connection
	CCLTRY ( _QISAFE((unkV = vF),IID_ILocation,&pLoc) );
	CCLTRY ( _QISAFE((unkV = vT),IID_IReceptor,&pR) );
	CCLTRY ( pLoc->connect ( pR, bConnect, false ) );

	// Debug
//	lprintf ( LOG_DBG, L"%s : %s -> %s (0x%x)\r\n", (bConnect) ? L"connect" : L"disconnect", wFrom, wTo, hr );
	if (hr != S_OK)// && bConnect)
		lprintf ( LOG_WARN, L"Missing connection:%s:%s:%s:0x%x",
						wFrom, wTo, (bConnect) ? L"connect" : L"disconnect", hr );

	// Clean up
	_RELEASE(pR);
	_RELEASE(pLoc);

	return hr;
	}	// connect

HRESULT Location :: connect ( IReceptor *pR, bool bC, bool bM )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IReceptor
	//
	//	PURPOSE
	//		-	Connect a receptor to this location.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	bC is true to connect, false to disconnect
	//		-	bM is true to mirror locations, false for single direction
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	ILocation	*prL	= NULL;

	//
	// Connect
	//
	if (bC)
		{
		// Add receptor to list
		CCLTRY ( pConns->add ( pR, true ) );

		// When a new connection is made, receive the current state of
		// values in the branch into the target.
		CCLTRY ( reflect ( L"", pR ) );

		// Notify other side of connection and mirror mode
		if (hr == S_OK && _QI(pR, IID_ILocation, &prL) == S_OK)
			hr = prL->connected(pRcpOut, true, bM);
		_RELEASE(prL);
		}	// if

	//
	// Disconnect
	//
	else
		{
		// Find specified receptor
		if (pR != NULL)
			{
			// Ensure other end is disconnected
			if (hr == S_OK && _QI(pR,IID_ILocation,&prL) == S_OK)
				prL->connected ( pRcpOut, false, false );
			_RELEASE(prL);

			// Disconnect from here
			pConns->remove ( pR );
			}	// if

		// Special case, disconnect all receptors
		else if (pR == NULL)
			{
			IUnknown		*pConn = NULL;

			// Remove all connections
			ConnListIt it ( pConns );
			while (it.next ( &pConn ) == S_OK)
				{
				IReceptor *r = (IReceptor *)(pConn);

				// Notify and remove
				if (hr == S_OK && _QI(r,IID_ILocation,&prL) == S_OK)
					prL->connected ( pRcpOut, false, false );
				_RELEASE(prL);
				pConns->remove ( r );
				_RELEASE(pConn);
				}	// while

			}	// else if

		}	// else

	return hr;
	}	// connect

HRESULT Location :: connected ( IReceptor *pR, bool bC, bool bM )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IReceptor
	//
	//	PURPOSE
	//		-	Notifies when this location is now connected to another.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	bC is true for connected, false for disconnected
	//		-	bM is true to mirror location, false for single direction
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Connected/disconnected
	if (bC)
		pConns->add ( pR, bM );
	else
		pConns->remove ( pR );

	return hr;
	}	// connected

HRESULT Location :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being created.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Create a contained dictionary for storing items
	CCLTRY ( nspcglb.dict ( &pDctc ) );

	// Create run-time receptor object
	CCLTRYE ( (pConns = new ConnList()) != NULL, E_OUTOFMEMORY );
	CCLOK   ( pConns->AddRef(); )
	CCLTRY  ( pConns->construct() );

	// Obtain outer dictionary interface in case of aggregation
	CCLTRY ( _QI(this,IID_IDictionary,&pDctOut) );
	CCLOK  ( pDctOut->Release(); )
	CCLTRY ( _QI(this,IID_ILocation,&pLocOut) );
	CCLOK  ( pLocOut->Release(); )
	CCLTRY ( _QI(this,IID_IReceptor,&pRcpOut) );
	CCLOK  ( pRcpOut->Release(); )

	return hr;
	}	// construct

HRESULT Location :: create ( const WCHAR *pwKey, ILocation **ppLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocation
	//
	//	PURPOSE
	//		-	Called to create a new location of the same type.
	//
	//	PARAMETERS
	//		-	pwKey is the key name that will be used for the location.
	//		-	ppLoc will receive the location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IDictionary	*pDctLoc	= NULL;
	adtString	strKey(pwKey);

	// Need to create a location object ? (allows for outer object to create location)
	if (hr == S_OK && *ppLoc == NULL)
		{
		Location		*pLoc		= NULL;

		// Create new location object
		CCLTRYE( (pLoc = new Location()) != NULL, E_OUTOFMEMORY );
		CCLOK  ( pLoc->AddRef(); )
		CCLTRY ( pLoc->construct() );

		// Result
		CCLTRY ( _QI(pLoc,IID_ILocation,ppLoc) );

		// Clean up
		_RELEASE(pLoc);
		}	// if

	// Set state
	CCLTRY ( _QISAFE(*ppLoc,IID_IDictionary,&pDctLoc) );
	CCLTRY ( pDctLoc->store ( strnRefNspc,	adtIUnknownRef(pSpc) ) );
	CCLTRY ( pDctLoc->store ( strnRefPar,	adtIUnknownRef((ILocation *)this) ) );
	CCLTRY ( pDctLoc->store ( strnRefName, adtString(pwKey) ));

	// Store in location
	CCLTRY ( pDctOut->store ( strKey, adtIUnknown(*ppLoc) ) );
//	CCLOK ( pLocOut->stored ( pLocOut, bRcp, pRcpOut, strKey, adtIUnknown(*ppLoc) ); )

	// Receive into location, required so listeners are notified
//	CCLTRY ( receive ( pRcpOut, strKey, adtIUnknown(*ppLoc) ) );

	// Clean up
	_RELEASE(pDctLoc);

	return hr;
	}	// create

HRESULT Location :: desc ( bool bAct )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Process the descriptor for the location.
	//
	//	PARAMETERS
	//		-	bAct is true activate the instance based on the descriptor,
	//			false to deactivate.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IDictionary	*pDsc	= NULL;
	adtValue		v;
	adtIUnknown	unkV;
	adtString	strType,strDscName;

	// Does location have a descriptor ?
	if (pDctOut->load ( strnRefDesc, v ) != S_OK)
		return S_OK;

	// Descriptor information
	CCLTRY ( _QISAFE((unkV=v),IID_IDictionary,&pDsc) );
	CCLTRY ( pDsc->load ( strnRefType, v ) );
	CCLTRYE( (strType = v).length() > 0, E_UNEXPECTED );

	// Debug
	CCLTRY ( pDctOut->load ( strnRefName, v ) );
	CCLTRYE( (strDscName = v).length() > 0, E_UNEXPECTED );

	//
	// Behaviour
	//
	if (hr == S_OK && !WCASECMP(strType,L"Behaviour"))
		{
		adtString	strB;

		// Attach
		if (bAct)
			{
			// Obtain the behaviour
			CCLTRY ( pDsc->load ( strnRefBehave, v ) );
			CCLTRYE( (strB = v).length() > 0, E_UNEXPECTED );
//			dbgprintf ( L"Location::%s:%s:%s\r\n", (LPCWSTR) strDscName, (LPCWSTR) strType, 
//							(LPCWSTR) strB );

			// Create the behaviour for the node
			// Object class Id
			CCLTRY ( cclCreateObject ( strB, NULL, IID_IBehaviour, (void **) &pBehave ) );

			// Attach using the wrapper as the outer receptor
			CCLTRY ( pBehave->attach ( this, true ) );

			// nSpace value behaviour ?
			CCLOK ( bBehaveV = !WCASECMP(strB,L"nSpc.Value"); )

			// Clean up
			if (hr != S_OK)
				{
				lprintf ( LOG_WARN, L"Unable to attach behaviour : %s\r\n", (LPCWSTR) strB );
				strB = L"";
				_RELEASE(pBehave);
				}	// if
			}	// if

		// Detach
		else if (pBehave != NULL)
			{
			// Obtain the behaviour (DEBUG)
			CCLTRY ( pDsc->load ( strnRefBehave, v ) );
			CCLTRYE( (strB = v).length() > 0, E_UNEXPECTED );

			// Detach from location
//			lprintf ( LOG_DBG, L"%s::desc:detach:%s:%s:%s\r\n", 
//							(LPCWSTR)strName, (LPCWSTR) strDscName, (LPCWSTR) strType, (LPCWSTR) strB );
			pBehave->attach ( this, false );

			// Clean up
			strB = L"";
			_RELEASE(pBehave);
			}	// else

		}	// if

	//
	// Location
	//
	else if (hr == S_OK && !WCASECMP(strType,L"Location"))
		{
//		adtString	strSubName;

		// Name of location
//		CCLTRY ( pDsc->load ( strnRefName, v ) );
//		CCLTRYE( (strSubName = v).length() > 0, E_UNEXPECTED );

		// Active
		if (bAct)
			{
			ILocation	*pLocRef	= NULL;
			adtString	strPath,strRef(LOC_NSPC_REF);

			// Obtain the reference location
			CCLTRY ( pDsc->load ( strnRefLocn, v ) );
			CCLTRYE( (strPath = v).length() > 0, E_UNEXPECTED );

//			CCLOK  ( lprintf ( LOG_DBG, L"Location::%s:%s:%s:%s\r\n", 
//						(LPCWSTR) strDscName, (LPCWSTR) strType, (LPCWSTR) strName, (LPCWSTR) strPath); )

			// Debug
//			if (hr == S_OK && !WCASECMP(strPath,L"lib/interface/list/"))
//				dbgprintf ( L"Hi\r\n" );

			//
			// Place an instance of the specified reference location at this location.
			//

			// Obtain the reference information
			CCLTRY ( strRef.append ( strPath ) );
			CCLTRY ( pSpc->get ( strRef, v, NULL ) );

			// Paths are auto-create so make sure a real graph was instanced
			if (hr == S_OK)
				{
				IContainer *pCnt = NULL;
				U32			sz;

				// Obtain number of items in container
				CCLTRY ( _QISAFE((unkV=v),IID_IContainer,&pCnt) );
				CCLTRY ( pCnt->size ( &sz ) );

				// Even empty locations get a certain number of keys due to initialization
				if (hr == S_OK && sz <= 3)
					lprintf ( LOG_WARN, L"Missing reference path : %s\r\n", (LPCWSTR) strPath );
//				CCLTRYE ( sz > 3, ERROR_NOT_FOUND );

				// Clean up
				_RELEASE(pCnt);
				}	// if

			// Create a non-mirrored link between source and this location
			CCLTRY ( _QISAFE((unkV=v),IID_ILocation,&pLocRef) );
			CCLTRY ( pLocRef->connect ( pRcpOut, true, false ) );

			// Move location into active state
//			CCLTRY ( keyNotify ( strnRefAct, adtBool(true) ) );

			// Receive path to reference into location so listeners are notified
			// of activation, leave off the 'ref/' prefix
			CCLTRY ( receive ( pRcpOut, strnRefLocn, strPath ) );
//			CCLTRY ( pDctOut->store ( strnRefLocn, strRef ) );

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_WARN, L"Missing reference path : %s\r\n", (LPCWSTR) strPath );

			// Clean up
			_RELEASE(pLocRef);
			}	// if

		// In active
		else
			{
			// No need to notify sublocation of active change since it will
			// be handled by this location.
			}	// else

		}	// if

	// Clean up
	_RELEASE(pDsc);

	return hr;
	}	// desc

void Location :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
//	if (strName.length() > 0)
//		lprintf(LOG_DBG, L"%s:%d {\r\n", (LPCWSTR)strName,bActive);

	// Protect from double deletion and deativate location
	AddRef();

	// If parent is still valid, remove to shutdown location
	if (pPar != NULL)
		store ( strnRefPar, adtIUnknown(NULL) );

	// Clear items from location
	clear();

	// Clean up
	_RELEASE(pDctConns);
//	_RELEASE(pRcpConn);
	_RELEASE(pBehave);
	_RELEASE(pRxQ);
	_RELEASE(pRxIt);
	_RELEASE(pConns);
	_RELEASE(pDctc);

//	if (strName.length ( ) > 0)
//		lprintf ( LOG_DBG, L"} %s:%d\r\n", (LPCWSTR)strName, bActive );
	}	// destruct

HRESULT Location :: getKeyCount ( U32 *pCnt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Obtain the count of received keys in this location
	//
	//	PARAMETERS
	//		-	pCnt will receive the count
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	if (pCnt != NULL)
		*pCnt = uCntKey;

	return (pCnt != NULL) ? S_OK : S_FALSE;
	}	// getKeyCount

HRESULT Location :: isTemporal ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Determines if this location is a temporal location.
	//
	//	RETURN VALUE
	//		S_OK if temporal, S_FALSE if not
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_FALSE;
	ITemporalLoc	*pLocTmp	= NULL;

	// Is this location a reference location ?  Reference locations
	// are just temporal locations that act as a source of the definition
	// for other locations (graph instances).
	if (	pDctOut != NULL && _QI(pDctOut,IID_ITemporalLoc,&pLocTmp) == S_OK)
		hr = S_OK;

	// Clean up
	_RELEASE(pLocTmp);

	return hr;
	}	// isTemporal

HRESULT Location :: keyNotify ( const ADTVALUE &vK, const ADTVALUE &vV,
											bool bRx, bool bNotifyConn )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Notify sub-locations of key/value
	//
	//	PARAMETERS
	//		-	vK is the key
	//		-	vV is the value
	//		-	bRx is true to 'receive' the value, false to 'store' it.
	//		-	bNotifyConn is true to notify connector level locations.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	ILocation	*pL	= NULL;
	IDictionary	*pLd	= NULL;
	IReceptor	*pRcp	= NULL;
	IIt			*pIt	= NULL;
	adtValue		v;
	adtIUnknown	unkV;

	// Do not propagate notifications beyond the connector level to avoid
	// possible false signalling to other graph instances that might be in
	// the connector value itself.
	if ((bRcp || bEmt) && !bNotifyConn)
		{
		// To avoid possible recursive reference counts, clear any stored/cached
		// values for emitters
//		if (bLocThis)
//			{
			// Remove the value from the connector
//			pDctc->remove ( strnRefVal );

			// Clear cached value
//			bLocThis = false;
//			adtValue::clear ( vLocThis );
//			}	// if

		// Done
		return S_OK;
		}	// if

	// Propagate active state to sub-locations
	CCLTRY ( keys ( &pIt ) );
	while (hr == S_OK && pIt->read ( v ) == S_OK)
		{
		adtString	strKey(v);

		// Send value state to sub-location (do not go 'up' to parent)
		if (	hr == S_OK										&&
				(	(	strKey[0] != CHR_NSPC_PRE &&
						strKey[0] != CHR_NSPC_PST ) ||
					!WCASECMP(strKey,STR_NSPC_CONN) )	&&
				load( strKey, v ) == S_OK					&&
				(IUnknown *)(NULL) != (unkV=v)			&&
				_QI(unkV,IID_ILocation,&pL) == S_OK)
			{
			// Receive
			if (bRx && _QI(unkV,IID_IReceptor,&pRcp) == S_OK)
				hr = pRcp->receive ( pRcpOut, adtString(vK), vV );

			// Store
			else if (!bRx && _QI(unkV,IID_IDictionary,&pLd) == S_OK )
				hr = pLd->store ( vK, vV );
			}	// if

		// Clean up
		_RELEASE(pL);
		_RELEASE(pRcp);
		_RELEASE(pLd);
		pIt->next();
		}	// while

	// Clean up
	_RELEASE(pIt);

	return hr;
	}	// keyNotify

HRESULT Location :: load ( const ADTVALUE &vKey, ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITemporalLoc
	//
	//	PURPOSE
	//		-	Loads a value from the TemporalLoc with the given key.
	//
	//	PARAMETERS
	//		-	vKey is the key
	//		-	vValue will receive the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Common 'value' is cached
//	if (	adtValue::type(vKey) == VTYPE_STR && 
//			!WCASECMP(vKey.pstr,L"Value") &&
//			!adtValue::empty(vLocThis) )
//		hr = adtValue::copy ( vLocThis, vValue );
//	else
		hr = pDctc->load ( vKey, vValue );

	return hr;
	}	// load

HRESULT Location :: receive ( IDictionary *pDct, 
										const WCHAR *wPath, const ADTVALUE &v,
										bool bRec )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Receive a value into a receptor.
	//
	//	PARAMETERS
	//		-	pDct is the root dictionary
	//		-	wPath is the path to the receptor
	//		-	v is the value to receive
	//		-	bRec is true to receive recursively.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IReceptor	*pRecep	= NULL;
	adtValue		vL,vN;
	adtIUnknown	unkV;

	// Send to sub-locations
	if (hr == S_OK && bRec)
		{
		ILocation	*pL	= NULL;
		IDictionary	*pLd	= NULL;
		IIt			*pIt	= NULL;

		// Propagate active state to sub-locations
		CCLTRY ( pDct->keys ( &pIt ) );
		while (hr == S_OK && pIt->read ( vL ) == S_OK)
			{
			adtString	strKey(vL);

			// Send active state to sub-location (do not go 'up' to parent)
			if (	hr == S_OK									&&
					WCASECMP(strKey,STR_NSPC_PARENT)		&&
					pDct->load ( vL, vN ) == S_OK &&
					(IUnknown *)(NULL) != (unkV=vN)		&&
					_QI(unkV,IID_ILocation,&pL) == S_OK &&
					_QI(unkV,IID_IDictionary,&pLd) == S_OK )
				receive ( pLd, wPath, v, bRec );

			// Clean up
			_RELEASE(pL);
			_RELEASE(pLd);
			pIt->next();
			}	// while

		// Clean up
		_RELEASE(pIt);
		}	// if

	// Load specified receptor
	CCLTRY ( nspcLoadPath ( pDct, wPath, vL ) );
	CCLTRY ( _QISAFE((unkV = vL),IID_IReceptor,&pRecep) );

	// Send signal
	if (hr == S_OK)
		{
//		dbgprintf ( L"Location::receive:%s:%s\r\n", wPath, (LPCWSTR)strName );
		pRecep->receive ( pRcpOut, L"Value", v );
		}	// if

	// Clean up
	_RELEASE(pRecep);

	return hr;
	}	// receive

HRESULT Location :: receive ( IReceptor *prSrc, const WCHAR *pwLoc, 
										const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IReceptor
	//
	//	PURPOSE
	//		-	Called to store a value at a location.
	//
	//	PARAMETERS
	//		-	prSrc is the source of the reception
	//		-	pwLoc is the location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	const WCHAR	*pw	= NULL;

	// A received value goes 'down' into the levels until a single
	// key/value pair is left.  No slash means a single key.
	if ((pw = wcschr ( pwLoc, '/')) == NULL)
		hr = receiveKey ( prSrc, pwLoc, v );

	// Key is still a path, move down to next level
	else
		{
		IReceptor	*pRcp	= NULL;
		adtIUnknown	unkV;
		adtString	strKey(pwLoc);
		adtValue		vL;

		// Isolate key
		strKey.at(pw-pwLoc) = '\0';

		// Load sub-location
		if (hr == S_OK && load ( strKey, vL ) != S_OK)
			{
			ILocation	*pLoc		= NULL;

			// Debug
			if (pBehave != NULL)
				dbgprintf ( L"Location::receiveInt:Behaviour but unknown connector:%s/%s(%s)\r\n",
								(LPCWSTR) strName, (LPCWSTR) strKey, pwLoc );

			// Create a new sub-location in order to continue storage
			CCLTRY ( pLocOut->create ( strKey, &pLoc ) );

			// Loaded value
			CCLTRY ( adtValue::copy ( adtIUnknown((IReceptor *)pLoc), vL ) );

			// Clean up
			_RELEASE(pLoc);
			}	// if

		// Receive remaining key path into the next level
		if (hr == S_OK)
			{
			CCLTRY ( _QISAFE((unkV=vL),IID_IReceptor,&pRcp) );
			CCLTRY ( pRcp->receive ( prSrc, pw+1, v ) );
			}	// if

		// Clean up
		_RELEASE(pRcp);
		}	// if

	return hr;
	}	// receive

HRESULT Location :: receiveEmpty ( IReceptor *prSrc, const WCHAR *pwKey )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocation
	//
	//	PURPOSE
	//		-	Called when an empty value has been received.
	//
	//	PARAMETERS
	//		-	prSrc is the source receptor
	//		-	pwKey is the key being emptied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	ILocation		*pLoc	= NULL;
	adtValue			vL;
	adtIUnknown		unkV;
	adtString		strKey(pwKey);

//	dbgprintf ( L"Location::receiveEmpty:%s\r\n", (LPCWSTR)strKey );

	// If key is a location, all of its keys must be removed via 'receive'
	// in order for notifications to propogate throughout hierarchy
	if (	hr == S_OK								&&
			load ( strKey, vL ) == S_OK		&&
			(IUnknown *)(NULL) != (unkV=vL)	&&
			_QI(unkV,IID_ILocation,&pLoc) == S_OK )
		{
		IDictionary		*pDct	= NULL;
		IIt				*pIt	= NULL;
		IReceptor		*pRcp	= NULL;

		// In order to disassemble a hierarchy from the bottom, take two
		// passes where the first pass empties locations and the second
		// the remaining keys.
		for (int i = 0;hr == S_OK && i < 2;++i)
			{
			// Search keys for locations
			CCLTRY ( _QI(pLoc,IID_IReceptor,&pRcp) );
			CCLTRY ( _QI(pLoc,IID_IDictionary,&pDct) );
			CCLTRY ( pDct->keys ( &pIt ) );
			while (hr == S_OK && pIt->read ( vL ) == S_OK)
				{
				ILocation	*pLocSub	= NULL;
				bool			bT			= false;
				adtString	strKeySub(vL);

				// Certain internal, local, run-time only keys are not touched
				bT = (	(	strKeySub[0] != CHR_NSPC_PRE &&
								strKeySub[0] != CHR_NSPC_PST ) || 
							(	WCASECMP(strKeySub,STR_NSPC_PARENT)	&&
								WCASECMP(strKeySub,STR_NSPC_NAME)	&&
								WCASECMP(strKeySub,STR_NSPC_ACTIVE) &&
								WCASECMP(strKeySub,STR_NSPC_TYPE) &&
								WCASECMP(strKeySub,STR_NSPC_NSPC) ) );

				// On the first pass, only empty value if it is a location
				if (bT && i == 0)
					bT = (pDct->load ( vL, vL ) == S_OK		&&
							(IUnknown *)(NULL) != (unkV=vL)	&&
							_QI(unkV,IID_ILocation,&pLocSub) == S_OK);
				if (bT)
					{
					// Empty contents
					pRcp->receive ( prSrc, strKeySub, adtValue() );

					// Removal affects container state
					pIt->begin();
					}	// if

				// Next entry
				else
					pIt->next();

				// Clean up
				_RELEASE(pLocSub);
				}	// while

			// Clean up
			_RELEASE(pIt);
			_RELEASE(pDct);
			_RELEASE(pRcp);
			}	// for

		}	// if

	// Remove key from dictionary
	hr = pDctOut->remove ( strKey );

	// Clean up
	_RELEASE(pLoc);

	return hr;
	}	// receiveEmpty

HRESULT Location :: receiveKey ( IReceptor *prSrc, const WCHAR *pwLoc, 
											const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Internal routine to receive a single key/value pair
	//			into the location.
	//
	//	PARAMETERS
	//		-	prSrc is the source of the reception
	//		-	pwLoc is the location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	const WCHAR	*pw	= NULL;
	bool			bVal	= !WCASECMP(pwLoc,L"Value");
	bool			bConn	= (bRcp || bEmt);

	// SPECIAL CASE OPTIMIZATION: Connectors only receive 'value' locations/keys.
	if (bConn && !bVal)
		{
		// Must let sequence numbers through in order to view the past.
		if (pwLoc[0] != CHR_NSPC_PRE || WCASECMP(STR_NSPC_SEQ,pwLoc))
			return S_OK;
		}	// if

	// Thread safety
	if (bConn)
		csInt.enter();

	// If a connector is already receiving a value, the current value
	// will have to be queued for later. Non-connector locations
	// are fully asynchronous.
	if (bRx && bConn)
		{
		// Need a queue ?
		if (pRxQ == NULL)
			{
			// Create the reception value queue and set up iterator on demand
			// since the relative number of re-entrant locations is low.
			CCLTRY(COCREATE(L"Adt.Queue",IID_IList,&pRxQ));
			CCLTRY(pRxQ->iterate ( &pRxIt ) );
			}	// if

		// Queue information for later processing
		CCLTRY ( pRxQ->write ( adtIUnknown(prSrc) ) );
		CCLTRY ( pRxQ->write ( adtString(pwLoc) ) );
		CCLTRY ( pRxQ->write ( v ) );

		// Debug
		if (hr == S_OK)
			{
			U32	sz;
			pRxQ->size ( &sz );
			if (!(sz % 1000))
				lprintf ( LOG_WARN, L"%s Q:%d\r\n", (LPCWSTR)strName, sz );
			}	// if

		// Done
		csInt.leave();
		}	// if

	// Not a connector or not already receiving
	else
		{
		// Now receiving
		bRx = true;
		if (bConn)
			csInt.leave();

		////

		// Ok to store just locations
		if (*pwLoc != '\0')
			{
			++uCntKey;

			// Store/remove the value at this level
			if (!adtValue::empty(v))
				{
				// Store the value in the dictionary
				CCLTRY ( pDctOut->store ( (bVal) ? strnRefVal : adtString(pwLoc), v ) );
				}	// if
			else
				hr = receiveEmpty ( prSrc, pwLoc );

			// Notification
			CCLOK ( pLocOut->stored ( pLocOut, bRcp, prSrc, pwLoc, v ); )
			}	// if

		////

		// Were any values received during current reception ?
		if (bConn)
			csInt.enter();
		if (hr == S_OK && pRxQ != NULL && pRxQ->isEmpty() != S_OK)
			{
			adtValue	vqSrc,vqLoc,vqV;

			// Process values in queue until empty
			while (hr == S_OK && pRxIt->read ( vqSrc ) == S_OK)
				{
				// Read location and value
				CCLOK  ( pRxIt->next(); )
				CCLTRY ( pRxIt->read ( vqLoc ) );
				CCLOK  ( pRxIt->next(); )
				CCLTRY ( pRxIt->read ( vqV ) );
				CCLOK  ( pRxIt->next(); )

				// Release internal lock
				csInt.leave();

				// Receive value, direct cast ok since this function queued values.
				// Exact same logic as above.
				if (hr == S_OK)
					{
					////

					// Ok to store just locations
					if (*(vqLoc.pstr) != '\0')
						{
						++uCntKey;

						// Store/remove the value at this level
						if (!adtValue::empty(vqV))
							{
							// Store the value in the dictionary
							CCLTRY ( pDctOut->store ( vqLoc, vqV ) );
							}	// if
						else
							hr = receiveEmpty ( (IReceptor *)vqSrc.punk, vqLoc.pstr );

						// Notification
						CCLOK ( pLocOut->stored ( pLocOut, bRcp, (IReceptor *)vqSrc.punk, vqLoc.pstr, vqV ); )
						}	// if

					////
					}	// if

				// Internal lock
				csInt.enter();
				}	// while
				
			}	// if (Q not empty)

		// No longer receiving
		bRx = false;
		if (bConn)
			csInt.leave();
		}	// else

	return hr;
	}	// receiveKey

HRESULT Location :: reflect ( const WCHAR *pwLoc, IReceptor *prDst )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocation
	//
	//	PURPOSE
	//		-	Reflects the current level to the provided receptor.
	//
	//	PARAMETERS
	//		-	pwLoc is the current location
	//		-	prDst will receive the updates.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IIt			*pIt	= NULL;
	ILocation	*pLoc	= NULL;
	U32			nA		= 0;
	U32			nR		= 0;
	U32			sz;
	adtValue		vK,vV;
	adtIUnknown	unkV;
	adtString	strLoc(pwLoc),strKeyLoc;

	// SPECIAL OPTIMIZATION: Currently no need to reflect locations with
	// non-nSpace value behaviours or receptors.
	if ((pBehave != NULL && !bBehaveV) || bRcp)
		return S_OK;

	// Empty location ?
	if (pDctOut->size ( &sz ) != S_OK || sz == 0)
		return S_OK;

	// Debug
//	if (hr == S_OK && prDst != NULL)
//		{
//		IDictionary *pTst = NULL;
//		if (_QI(prDst,IID_IDictionary,&pTst) == S_OK)
//			pTst->remove ( adtString(L"Value") );
//		_RELEASE(pTst);
//		}	// if

	// Iterate the key/values for this location
	CCLTRY ( pDctOut->keys ( &pIt ) );
	while (hr == S_OK && pIt->read ( vK ) == S_OK)
		{
		adtString	strKey(vK);
		bool			bR;

		// Debug
//		lprintf ( LOG_DBG, L"key:%s\r\n", (LPCWSTR)strKey );
//		if (!WCASECMP(strKey,L"Value"))
//			dbgprintf ( L"Hi\r\n" );

		// Certain internal, local, run-time only keys are not reflected
		bR = (	(	strKey[0] != CHR_NSPC_PRE &&
						strKey[0] != CHR_NSPC_PST ) || 
					(	WCASECMP(strKey,STR_NSPC_PARENT)	&&
						WCASECMP(strKey,STR_NSPC_NAME)	&&
						WCASECMP(strKey,STR_NSPC_ACTIVE) &&
						WCASECMP(strKey,STR_NSPC_TYPE) &&
						WCASECMP(strKey,STR_NSPC_NSPC) ) );
		if (hr == S_OK && bR)
			{
			// Generate current path
			CCLTRY ( adtValue::copy ( strLoc, strKeyLoc ) );
			CCLTRY ( strKeyLoc.append ( strKey ) );

			// Access value for key
			CCLTRY ( pDctOut->load ( vK, vV ) );

			// DEBUG
//			if (hr == S_OK)
//				{
//				adtString strV;
//				adtValue::toString(vV,strV);
//				lprintf ( LOG_DBG, L"%s : %s\r\n", (LPCWSTR)strKey, (LPCWSTR)strV );
//				}	// if

			// Sub-location ? 
			if (	hr == S_OK &&
					(IUnknown *)(NULL) != (unkV=vV) &&
					_QI(unkV,IID_ILocation,&pLoc) == S_OK )
				{
				IDictionary	*pDctSub	= NULL;

				// A location with a behaviour always reflects its locations
				// even if they are empty.  For non-behaviours, do not 
				// reflect empty locations to handle cases such as a deleted temporal location.
				CCLTRY ( _QI(pLoc,IID_IDictionary,&pDctSub) );
				if (hr == S_OK && ( (pBehave != NULL) || (pDctSub->isEmpty() != S_OK) ) )
					{
					// Sub-location path
					CCLTRY ( strKeyLoc.append ( L"/" ) );

					// Continue reflection in sub-location
					CCLTRY ( pLoc->reflect ( strKeyLoc, prDst ) );
					}	// if

				// Clean up
				_RELEASE(pDctSub);
				_RELEASE(pLoc);
				}	// if

			// Value, reflect
			else if (hr == S_OK && !adtValue::empty(vV))
				prDst->receive ( pRcpOut, strKeyLoc, vV );

			// Key reflected
			CCLOK ( ++nR; )
			}	// if

		// Clean up
		++nA;
		pIt->next();
		}	// while

	// Ensure that 'this' location exists.  This handles the 
	// case where the location is necessary (e.g. connection) 
	// but has no contents yet.
	if (hr == S_OK && nR == 0 && nA != 0 && *pwLoc != '\0')
		prDst->receive ( pRcpOut, pwLoc, adtValue() );

	// Clean up
	_RELEASE(pIt);

	return hr;
	}	// reflect

HRESULT Location :: remove ( const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IContainer
	//
	//	PURPOSE
	//		-	Removes an item from the container identified by the specified
	//			value.
	//
	//	PARAMETERS
	//		-	v identifies the key to remove
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// Access item
	if (load ( v, vL ) == S_OK)
		{
		IDictionary	*pDct	= NULL;
		ILocation	*pLoc	= NULL;
		adtIUnknown	unkV (vL);

		// Parent now invalid.
		if (	(IUnknown *)(NULL) != unkV					&&
				adtValue::type(v) == VTYPE_STR			&&
				v.pstr[0] != CHR_NSPC_PRE					&&
				v.pstr[0] != CHR_NSPC_PST					&&
				_QI(unkV,IID_ILocation,&pLoc) == S_OK	&&
				_QI(unkV,IID_IDictionary,&pDct) == S_OK )
			pDct->store(strnRefPar, adtIUnknown(NULL));

		// Clean up
		_RELEASE(pDct);
		_RELEASE(pLoc);
		}	// if

	// If key is 'Value', remove reference
	if (v.vtype == VTYPE_STR && v.pstr != NULL && !WCASECMP(v.pstr,L"Value"))
		{
		// Remove reference
		pDctc->remove ( strnRefVal );

		// Value is removed
//		bLocThis = false;
		}	// if

	// Remove item from internal dictionary
	return pDctc->remove ( v );
	}	// remove

HRESULT Location :: shutdown ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Shut down this location for operation.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Move to inactive state
	if (bActive)
		{
		bActive = false;
		active ();
		}	// if

	// Clear items from location
	clear();

	return hr;
	}	// shutdown

HRESULT Location :: store ( const ADTVALUE &vKey, const ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IDictionary
	//
	//	PURPOSE
	//		-	Stores a value in the dictionary with the given key.
	//
	//	PARAMETERS
	//		-	vKey is the key
	//		-	vValue is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	adtString	strKey(vKey);

	// Debug
//	if (WCASECMP(strKey,L"Value"))
//		lprintf ( LOG_DBG, L"%s:key %s\r\n", (LPCWSTR)strName, (LPCWSTR)strKey );

	// Default behaviour
	if (strKey[0] != CHR_NSPC_PRE && strKey[0] != CHR_NSPC_PST)
		{
		// SPECIAL CASE: 'Value's are updated frequently so copy updated
		// value to internally referenced value.
		if (!WCASECMP(strKey,L"Value"))
			{
			// Cache copy of value
//			CCLTRY ( adtValue::copy ( vValue, vLocThis ) );

			// Store if necessary
//			if (!bLocThis)
//				{
				// Store reference to local copy
//				CCLTRY ( pDctc->store ( strnRefVal, adtValue(&vLocThis) ) );
				CCLTRY ( pDctc->store ( strnRefVal, vValue ) );

				// Valid is stored
//				CCLOK ( bLocThis = true; )
//				}	// if
			}	// if

		// Store value
		else 
			hr = pDctc->store ( strKey, vValue );
		}	// if

	// Internal/reserved keys
	else
		{
		// Default is to store for self
		bool	bStInt	= true;

		// Parent
		if (!WCASECMP(strKey,STR_NSPC_PARENT))
			{
			adtIUnknown	unkV(vValue);

			// If parent is being removed, shutdown location with current parent first.
			if (pPar != NULL)
				{
				// De-activate if necessary
				if (bActive)
					{
					bActive = false;
					active();
					}	// if

				// Propogate false activation to children regardless of state of parent
				// to ensure entire branch is shutdown
				else
					keyNotify ( strnRefAct, adtBool(false) );

				// Ensure all connections are disconnected since no parent leaves
				// location is disconnected state.
				connect ( NULL, false, false );
				}	// if

			// Parent location
			pPar		= NULL;
			pDctPar	= NULL;
			if ((IUnknown *)(NULL) != unkV && _QI(unkV,IID_ILocation,&pPar) == S_OK)
				pPar->Release();
			if ((IUnknown *)(NULL) != unkV && _QI(unkV,IID_IDictionary,&pDctPar) == S_OK)
				pDctPar->Release();
			}	// if

		// Namespace
		else if (!WCASECMP(strKey,STR_NSPC_NSPC))
			{
			adtIUnknown	unkV(vValue);

			// Namespace
			pSpc = NULL;
			if ((IUnknown *)(NULL) != unkV && _QI(unkV,IID_INamespace,&pSpc) == S_OK)
				pSpc->Release();

			// Location is now considered active
			if (pSpc != NULL)
				bActive = true;
			}	// else if

		// Type
		else if (!WCASECMP(strKey,STR_NSPC_TYPE))
			{
			adtString	strV(vValue);

			// Connector ?
			bRcp = (!WCASECMP(L"Receptor",strV));
			bEmt = (!WCASECMP(L"Emitter",strV));
			}	// else if

		// Name
		else if (!WCASECMP(strKey,STR_NSPC_NAME))
			{
			adtValue::toString(vValue,strName);
//			lprintf ( LOG_DBG, L"STR_NSPC_NAME %s\r\n", (LPCWSTR) strName );
			}	// else if
//{
//			hr = adtValue::toString(vValue,strName);
//strName.at();
//}

		// Location
		else if (!WCASECMP(strKey,STR_NSPC_LOC))
			{
			adtString strV(vValue);
//			lprintf ( LOG_DBG, L"%s Location received %s\r\n", (LPCWSTR)strName, (LPCWSTR)strV );
			}	// else if

		// Reference ?
		else if (!WCASECMP(strKey,STR_NSPC_REF))
			{
//			adtString strV(vValue);
			bLocRef = adtBool(vValue);
//			lprintf ( LOG_DBG, L"%s:%s Location received reference %s\r\n", (LPCWSTR)strName, (LPCWSTR)strKey, 
//							(bLocRef) ? L"true" : L"false" );
			}	// else if

		// Set sequence number for temporal location
		else if (!WCASECMP(strKey,STR_NSPC_SEQ))
			{
			adtLong lSeq(vValue);

			// When a location receives a sequence number, the sub-location
			// must be notified as well.  Currently an entire sub-tree is
			// temporally linked
//			lprintf ( LOG_DBG, L"Sequence number set : %ld (%s,%p)\r\n", 
//										(U64)lSeq, (LPCWSTR)strName, this );

			// NOTE: This causes recursion errors when using linked graphs
			// to the past. Leaving here in case it is needed in the future.
			// Notify locally connected listeners about the sequence number,
			// this is how it gets to attached temporal locations.  It will
			// not propogate up the parents since it is a 'reserved' key
//			pLocOut->stored ( pLocOut, bRcp, pRcpOut, strKey, vValue );

			// Receive sequence number into children locations
			keyNotify(adtString(STR_NSPC_SEQ),lSeq,true,true);
			}	// if

		// Descriptor
		else if (!WCASECMP(strKey,STR_NSPC_DESC))
			{
			adtValue vChk;

			// Is this location a reference location ?  Reference locations
			// are just temporal locations that act as a source of the definition
			// for other locations (graph instances).
			if (isTemporal() != S_OK && bActive &&
			// NOTE: If location already has a descriptor, do not
			// let it change.  At the moment the only reason a location
			// will receive a second descriptor is during reflection.
			// In the future this will need to be allowed for live
			// editing but the previous descriptor will need to be torn down.
			pDctc->load ( strKey, vChk ) != S_OK)
				{
				ICloneable		*pClone	= NULL;
				IUnknown			*pDesc	= NULL;
				adtIUnknown		unkV(vValue);
//				lprintf ( LOG_DBG, L"%s:Descriptor\r\n", (LPCWSTR)strName );

				// A clone of the descriptor must be used to ensure each instance has
				// its own default attributes.
				CCLTRY ( _QISAFE(unkV,IID_ICloneable,&pClone) );
				CCLTRY ( pClone->clone ( &pDesc ) );

				// Store clone in dictionary instead of original
				CCLOK  ( bStInt = false; )
				CCLTRY ( pDctc->store ( vKey, adtIUnknown(pDesc) ) );

				// Location is now considered active
				CCLOK ( desc(bActive); )

				// Clean up
				_RELEASE(pDesc);
				_RELEASE(pClone);
				}	// if

			}	// else if

		// Connections
		else if (!WCASECMP(strKey,STR_NSPC_CONN))
			{
			adtIUnknown		unkV(vValue);

			// Instance ?
			if (isTemporal() != S_OK && bActive)
				{
				IDictionary		*pDctConnsN = NULL;

//				lprintf ( LOG_DBG, L"%s:Connections {\r\n", (LPCWSTR) strName );

				// Connection list 
				// TODO: Unconnect on new list (live editing)
				// NOTE: Assumes new list to re-do connection to avoid duplicates,
				// this may fall out when fixing incremental changes.

				// New connection list
				_QISAFE(unkV,IID_IDictionary,&pDctConnsN);
				if (pDctConnsN !=  pDctConns)
					{
					// Remove existing connections
					if (pDctConns != NULL)
						connect ( pDctConns, false );

					// New connection list
					_RELEASE(pDctConns);
					pDctConns = pDctConnsN;
					_ADDREF(pDctConns);

					// Make connections
					if (pDctConns != NULL)
						connect ( pDctConns, true );
					}	// if

				// Clean up
				_RELEASE(pDctConnsN);
//				lprintf ( LOG_DBG, L"} %s:Connections %p\r\n", (LPCWSTR) strName, pDctConns );
				}	// if

			}	// else if

		// Synchronization key is temporalied with a definition in order to 
		// signal 'end' of definition
		else if (!WCASECMP(strKey,STR_NSPC_SYNC))
			{
			// Auto initialize graph instance at the end of the definition playback
			if (isTemporal() != S_OK && bActive)
				{
				// Send init. signal
//				lprintf ( LOG_DBG, L"%s:Sync {\r\n", (LPCWSTR)strName );
				receive ( pDctOut, L"Initialize/Fire", strName, false );
//				lprintf ( LOG_DBG, L"} %s:Sync \r\n", (LPCWSTR)strName );
				}	// if

			// Do not sure sync flah in internal dictionary to avoid repeating
			// it during reflection
			bStInt = false;
			}	// else if

		// Activation/de-activation
		// Support entire hierarchy activation/de-activation.  This is needed for things
		// like cross-linking states.
		else if (!WCASECMP(strKey,STR_NSPC_ACTIVE))
			{
			adtBool	bV(vValue);

//			lprintf ( LOG_DBG, L"%s:New activation state:%d\r\n", (LPCWSTR)strName,  (bool)bV );

			// Active state has changed
			if (bActive != bV)
				{
				// Process new state
				bActive = bV;
//				dbgprintf ( L"Location::store:Active %d\r\n", bActive );
				active();
				}	// if

			// Propogate false activation to children regardless of state of parent
			// to ensure entire branch is shutdown
			else if (bV == false)
				keyNotify ( strnRefAct, bV );

			}	// else if

/*
		// Connections
		else if (!WCASECMP(strKey,STR_NSPC_CONN))
			{
//			ILocation		*pLocC	= NULL;
			adtIUnknown		unkV(vValue);

			// Cache away connection dictionary
			_RELEASE(pDctConns);
			CCLTRY(_QISAFE(unkV,IID_IDictionary,&pDctConns));
			
			// In order to manage connections, create and attach a connect receptor object.

			// Valid ?
			if ((IUnknown *)(NULL) != unkV && _QI(unkV,IID_ILocation,&pLocC) == S_OK)
				{
				// Create connect object
				if (pRcpConn == NULL)
					{
					Connect	*pObj	= NULL;

					// Create/initialize object
					CCLTRYE ( (pObj = new Connect()) != NULL, E_OUTOFMEMORY );
					CCLOK   ( pObj->AddRef(); )
					CCLTRY  ( pObj->construct() );

					// Interface
					CCLTRY	( _QI(pObj,IID_IReceptor,&pRcpConn) );

					// Clean up
					_RELEASE(pObj);
					}	// if

				// Connect the connection manager to location
				CCLTRY ( pLocC->connect ( pRcpConn, true, false ) );

				// Clean up
				_RELEASE(pLocC);
				}	// if
			

			}	// else if
*/

		// Store in own dictionary
		if (hr == S_OK && bStInt)
			hr = pDctc->store ( vKey, vValue );
		}	// else

	return hr;
	}	// store

HRESULT Location :: stored ( ILocation *pAt, bool bAtRcp, IReceptor *prSrc,
										const WCHAR *pwKey, const ADTVALUE &vValue )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocation
	//
	//	PURPOSE
	//		-	Called when a value is stored at a location.
	//
	//	PARAMETERS
	//		-	pAt is the location at which the value was stored 
	//		-	bAtRcp is true if original location is a receptor.
	//		-	prSrc is the original source receptor of the store.
	//		-	pwKey is the key path at source location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Debug
//	if (hr == S_OK)
//		{
//		IDictionary	*pAtDct	= NULL;
//		if (_QI(pAt,IID_IDictionary,&pAtDct) == S_OK)
//			{
//			adtString strLoc;
//			CCLTRY ( nspcPathTo ( pAtDct, pwKey, strLoc, this ) );
//			dbgprintf ( L"Location::stored:%s\r\n", (LPCWSTR)strLoc );
//			_RELEASE(pAtDct);
//			if (!WCASECMP(strLoc,L"apps/auto/default/GDI/This/Cache/TagGet/Fire/Value"))
//				dbgprintf ( L"Hi\r\n" );
//			}	// if
//		}	// if

	// Does this location have any listeners ?
	if (!pConns->isEmpty())
		{
		IDictionary	*pAtDct	= NULL;
		adtString	*pstrLoc	= NULL;
		IUnknown		*pConn	= NULL;
		bool			bRx;
//		adtString	strLoc(pwKey);

		// Optimization, no need to generate path if at same level
		if (pAt != pLocOut)
			{
			// Need own buffer for full path
			CCLTRYE ( (pstrLoc = new adtString()) != NULL, E_OUTOFMEMORY );

			// Generate the path from the source level to this level.
			CCLTRY ( _QISAFE(pAt,IID_IDictionary,&pAtDct) );
//			CCLTRY ( nspcPathTo ( pAtDct, pwKey, strLoc, this ) );
			CCLTRY ( nspcPathTo ( pAtDct, pwKey, *pstrLoc, pDctOut ) );
			}	// if

		// Debug
//		if (pDctPar != NULL && pDctPar->load ( strnRefName, vL ) == S_OK)
//			{
//			adtString	strPar(vL);
//			dbgprintf ( L"Location::stored:Parent %s:Name %s:Loc %s:Head %p\r\n", 
//								(LPCWSTR)strPar,
//								(LPCWSTR)strName, (LPCWSTR)strLoc, pConns->head() );
//			if (!WCASECMP(strPar,L"Uninitialize"))
//				dbgprintf ( L"Hi\r\n" );
//			}	// if

		// Debug
//		if (wcsstr ( strLoc, L"TagGet/" ) != NULL)
//			dbgprintf ( L"Location::stored:%s:%s:%s\r\n", (LPCWSTR)strName, pwKey, (LPCWSTR) strLoc );

		// Receive store into connected receptors.  Ignore original source
		// of store to avoid loops.
		ConnListIt	it ( pConns );
		while (it.next ( &pConn, &bRx ) == S_OK)
			{
			// Can avoid QI for speed
			IReceptor	*r = (IReceptor *)(pConn);

			// Reception if not source and enabled
			if (r != prSrc && bRx)
				r->receive ( pRcpOut, ((pstrLoc != NULL) ? ((const WCHAR *)*pstrLoc) : pwKey), vValue );
//				r->receive ( this, strLoc, vValue );
//				connect ( r, false );

			// Clean up
			_RELEASE(pConn);
			}	// for

		// Clean up
		_RELEASE(pAtDct);
		if (pstrLoc != NULL)
			delete pstrLoc;
		}	// if

	// Notify up the chain.
	if (pPar != NULL)
		{
		// SPECIAL OPTIMIZATION: 
		// Currently no need to propagate values from receptors of behaviours or
		// from non-nSpace value behaviours.  
		if (pBehave == NULL || (bBehaveV && !bAtRcp))
			{
			// Certain internal, local, run-time only keys are not propogated
			bool
			bUp = (	(	pwKey[0] != CHR_NSPC_PRE && pwKey[0] != CHR_NSPC_PST ) || 
						(	WCASECMP(pwKey,STR_NSPC_PARENT)	&&
							WCASECMP(pwKey,STR_NSPC_NAME)		&&
							WCASECMP(pwKey,STR_NSPC_ACTIVE)	&&
							WCASECMP(pwKey,STR_NSPC_SEQ)		&&
							WCASECMP(pwKey,STR_NSPC_TYPE)		&&
							WCASECMP(pwKey,STR_NSPC_NSPC) ) );

			// Also, do not propagate keywords
//			if (pwKey[0] != CHR_NSPC_PRE && pwKey[0] != CHR_NSPC_PST)
//			if (pwKey[0] != '_')
			if (bUp)
				pPar->stored ( pAt, bAtRcp, prSrc, pwKey, vValue );
			}	// if
		}	// if

	return hr;
	}	// stored

//
// Default functionality
//

HRESULT Location :: copyTo ( IContainer *pCont )
	{
	return pDctc->copyTo(pCont);
	}	// copyTo

HRESULT Location :: isEmpty ( void )
	{
	return pDctc->isEmpty();
	}	// isEmpty

HRESULT Location :: iterate ( IIt **ppIt )
	{
	return pDctc->iterate ( ppIt );
	}	// keys

HRESULT Location :: keys ( IIt **ppIt )
	{
	return pDctc->keys ( ppIt );
	}	// keys

HRESULT Location :: size ( U32 *pusz )
	{
	return pDctc->size ( pusz );
	}	// size

