////////////////////////////////////////////////////////////////////////
//
//										NSPCL.H
//
//								nSpace library
//
////////////////////////////////////////////////////////////////////////

#ifndef	NSPCL_H
#define	NSPCL_H

// System includes
#include "../adtl/adtl.h"
#include "../iol/iol.h"

///////////////
// Definitions
///////////////

// Predefined location for references in temporal nSpace
#define	LOC_NSPC_REF			L"ref/"

// Pre/post fixes for alphabet for internal names
#define	CHR_NSPC_PRE			WCHAR('.')
#define	CHR_NSPC_PST			WCHAR('~')
#define	STR_NSPC_PRE			L"."
#define	STR_NSPC_PST			L"~"

// Internal names
#define	STR_NSPC_NAME			STR_NSPC_PRE L"Name"
#define	STR_NSPC_NAMES			STR_NSPC_PRE L"Names"
#define	STR_NSPC_ITEMS			STR_NSPC_PRE L"Items"
#define	STR_NSPC_BEHAVE		STR_NSPC_PRE L"Behaviour"
#define	STR_NSPC_LOC			STR_NSPC_PRE L"Location"
#define	STR_NSPC_TYPE			STR_NSPC_PRE L"Type"
#define	STR_NSPC_MOD			STR_NSPC_PRE L"Modified"
#define	STR_NSPC_DESC			STR_NSPC_PRE L"Descriptor"
#define	STR_NSPC_ONDESC		STR_NSPC_PRE L"OnDescriptor"
#define	STR_NSPC_PARENT		STR_NSPC_PRE L"Parent"
#define	STR_NSPC_NSPC			STR_NSPC_PRE L"Namespace"
#define	STR_NSPC_CONNTR		STR_NSPC_PRE L"Connector"
#define	STR_NSPC_PERS			STR_NSPC_PRE L"Persist"
#define	STR_NSPC_ACTIVE		STR_NSPC_PRE L"Active"
#define	STR_NSPC_REV			STR_NSPC_PRE L"Revision"
#define	STR_NSPC_RCVR			STR_NSPC_PRE L"Receiver"
#define	STR_NSPC_REF			STR_NSPC_PRE L"Reference"
#define	STR_NSPC_SEQ			STR_NSPC_PRE L"Sequence"
#define	STR_NSPC_CONN			STR_NSPC_PST L"Connect"
#define	STR_NSPC_SYNC			STR_NSPC_PST L"Sync"

// Pre-generated string reference objects
extern	adtStringSt	strnRefName;
extern	adtStringSt	strnRefNames;
extern	adtStringSt	strnRefItms;
extern	adtStringSt	strnRefBehave;
extern	adtStringSt	strnRefLocn;
extern	adtStringSt	strnRefType;
extern	adtStringSt	strnRefMod;
extern	adtStringSt	strnRefDesc;
extern	adtStringSt	strnRefOnDesc;
extern	adtStringSt	strnRefPar;
extern	adtStringSt	strnRefNspc;
extern	adtStringSt	strnRefConn;
extern	adtStringSt	strnRefConntr;
extern	adtStringSt	strnRefPers;
extern	adtStringSt	strnRefAct;
extern	adtStringSt	strnRefRcvr;
extern	adtStringSt	strnRefRef;

extern	adtStringSt	strnRefFrom;
extern	adtStringSt	strnRefTo;
extern	adtStringSt	strnRefRO;
extern	adtStringSt	strnRefLoc;
extern	adtStringSt	strnRefKey;
extern	adtStringSt strnRefVal;

//
// BEGIN_BEHAVIOUR	- Begin a node behaviour
//	DECLARE_RECEPTOR	- Declare a receptor for the node
// DECLARE_EMITTER	- Declare an emitter for the node
//	END_BEHAVIOUR		- End behaviour
//
// (Do not keep references, node has reference on behaviour)
//

#define	DECLARE_LOC_NAME(a,b)	a##b
#define	DECLARE_RCP(a)														\
			IReceptor	*DECLARE_LOC_NAME(pr,a);
#define	DECLARE_EMT(a)														\
			IReceptor	*DECLARE_LOC_NAME(peOn,a);
#define	DECLARE_CON(a)														\
			DECLARE_RCP(a)														\
			DECLARE_EMT(a)

// Define the behaviour logic, connections, etc
#define	BEGIN_BEHAVIOUR()													\
	private :																	\
	STDMETHOD(attach)		( IDictionary *_pnLoc, bool bAttach )	\
			{																		\
			/* Default logic first */										\
			HRESULT	hr	= Behaviour::attach(_pnLoc,bAttach);		\

#if		defined(__GNUC__) || _MSC_VER >= 1900

#define	DEFINE_EMT(a)														\
			CCLTRY(pnSpc->connection ( pnLoc, L"On" #a,				\
							L"Emitter", this,									\
							(bAttach) ? &DECLARE_LOC_NAME(pe,On ## a) : NULL ));
#define	DEFINE_RCP(a)														\
			CCLTRY(pnSpc->connection ( pnLoc, L## #a,					\
							L"Receptor", this,								\
							(bAttach) ? &DECLARE_LOC_NAME(pr,a) : NULL ));
#define	DEFINE_CON(a)														\
			DEFINE_RCP(a)														\
			DEFINE_EMT(a)		
#else

#define	DEFINE_EMT(a)														\
			if (bAttach && hr == S_OK)										\
				hr = pnSpc->connection ( pnLoc, L"On" ## L#a,		\
								L"Emitter", this,								\
								&DECLARE_LOC_NAME(pe,On ## a) );

#define	DEFINE_RCP(a)														\
			if (bAttach && hr == S_OK)										\
				hr = pnSpc->connection ( pnLoc, L#a,					\
								L"Receptor", this,							\
								&DECLARE_LOC_NAME(pr,a) );
#define	DEFINE_CON(a)														\
			DEFINE_RCP(a)														\
			DEFINE_EMT(a)		

#endif

#define	END_BEHAVIOUR()													\
			CCLTRY(onAttach(bAttach));										\
			return hr; }														\
		/* Callback function to process received values */			\
		STDMETHOD(onReceive)	( IReceptor *, const ADTVALUE & );
 
#define	END_BEHAVIOUR_NOTIFY()											\
		END_BEHAVIOUR()														\
	STDMETHOD(onAttach)( bool );

#define	_RCP(a)		(DECLARE_LOC_NAME(pr,a) == pr)
#define	_EMT(a,b)	(DECLARE_LOC_NAME(pe,On##a)->receive(this,L"Value",(b)))

// Prototypes
extern "C"
bool rcp ( const WCHAR *pwSrc, const WCHAR *pwDst );

////////////
// Typedefs
////////////

// Sequence numbers
typedef U64 NSSQNM;
typedef U32 LOCID;

//////////////
// Interfaces
//////////////

// Forward dec.
//struct IEmitter;
struct INamespace;
struct INamespaceCB;
struct INamespaceValue;
//struct INode;
struct IReceiver;
struct IReceptor;
struct ITemporal;

//
// Interface - IReceptor.  Interface to a receptor for a location.
//

DEFINE_GUID	(	IID_IReceptor, 0x2534d016, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IReceptor,IUnknown)
	{
	STDMETHOD(receive)	( IReceptor *, const WCHAR *, const ADTVALUE & ) PURE;
	};

//
// Interface - IBehaviour.  Base interface to an object that
//		has defined node behaviour.
//

DEFINE_GUID	(	IID_IBehaviour, 0x2534d00a, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IBehaviour,IReceptor)
	{
	STDMETHOD(attach)		( IDictionary *, bool )	PURE;
	};

//
// Interface - ILocation.  Interface to a namespace location.
//

DEFINE_GUID	(	IID_ILocation, 0x2534d00f, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(ILocation,IUnknown)
	{
	STDMETHOD(connect)	( IReceptor *, bool, bool )				PURE;
	STDMETHOD(connected)	( IReceptor *, bool, bool )				PURE;
	STDMETHOD(create)		( const WCHAR *, ILocation ** )			PURE;
	STDMETHOD(reflect)	( const WCHAR *, IReceptor * )			PURE;
	STDMETHOD(stored)		( ILocation *, bool, IReceptor *,
									const WCHAR *, const ADTVALUE & )	PURE;
	};

//
// Interface - ILocDebug.  Interface to a debug a namespace location.
//

DEFINE_GUID	(	IID_ILocDebug, 0x2534d018, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(ILocDebug,IUnknown)
	{
	STDMETHOD(getKeyCount)	( U32 * ) PURE;
	};

//
// Interface - INamespace.  Interface to a namespace.
//

DEFINE_GUID	(	IID_INamespace, 0x2534d017, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(INamespace,IUnknown)
	{
	STDMETHOD(connection)	( IDictionary *, const WCHAR *, const WCHAR *, 
										IReceptor *, IReceptor ** )						PURE;
	STDMETHOD(get)				( const WCHAR *, ADTVALUE &, const WCHAR * )		PURE;
	STDMETHOD(link)			( const WCHAR *, const WCHAR *, bool )				PURE;
	STDMETHOD(open)			( ILocations * )											PURE;
	STDMETHOD(temporal)		( ILocations ** )											PURE;
	};

//
// Interface - ITemporal.  Interface to temporal nSpace implementation.
//

DEFINE_GUID	(	IID_ITemporal, 0x2534d012, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(ITemporal,IUnknown)
	{
	STDMETHOD(limits)			( U64 &, U64 & ) PURE;
	STDMETHOD(load)			( U64, ADTVALUE &, ADTVALUE &, ADTVALUE & ) PURE;
	STDMETHOD(record)			( INamespace *, const WCHAR *, const WCHAR *, bool, bool, bool, bool ) PURE;
	STDMETHOD(setMaximum)	( U64 ) PURE;
	};

//
// Interface - ITemporalLoc.  Interface to a temporal location.
//

DEFINE_GUID	(	IID_ITemporalLoc, 0x2534d113, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(ITemporalLoc,ILocation)
	{
	// No methods yet, interface used for Id
	STDMETHOD(nop)			( void ) PURE;
	};

// Prototypes
HRESULT nspcLoadPath		( IDictionary *, const WCHAR *, ADTVALUE & );
HRESULT nspcStoreValue	( IDictionary *, const WCHAR *, const ADTVALUE &, bool = true );
HRESULT nspcPathTo		( IDictionary *, const WCHAR *, adtString &, IDictionary * = NULL );
HRESULT nspcTokens		( const WCHAR *, const WCHAR *, IList * );

///////////
// Classes
///////////

///
///	\brief Base class from which to derive for a behaviour for a node.
///

class Behaviour :
	public IBehaviour										// Interface
	{
	public :
	Behaviour ( void );									// Constructor
	virtual ~Behaviour ( void );						// Destructor

	// Run-time data
	INamespace		*pnSpc;								///< The namespace object for which the node belongs.
	IDictionary		*pnLoc;								///< The location at which the node is installed.
	IDictionary		*pnDesc;								///< Descriptor for the node, that contains the node properties
 	adtString		strnName;							///< Contains a cached version of the user defined node name 
	const WCHAR		*prl;									///< Latest 'onReceive'd location

	// Utilities
	STDMETHOD(onAttach)	( bool );					// Behaviour being attached/detached
	STDMETHOD(onReceive)	( IReceptor *,				// Value received
									const ADTVALUE & );

	// 'IBehaviour' members
	STDMETHOD(attach)		( IDictionary *, bool );

	// 'IReceptor' memebers
	STDMETHOD(receive)	( IReceptor *, const WCHAR *, const ADTVALUE & );

	private :

	// Run-time data
	sysCS			csRx,csInt;								// Thread safety
	IList			*pRxQ;									// Receiver queue
	IIt			*pRxIt;									// Receiver iterator
	bool			bReceive,bReceiving;					// Receive flags
	};

#endif
