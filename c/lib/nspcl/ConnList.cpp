////////////////////////////////////////////////////////////////////////
//
//								CONNLST.CPP
//
//				Implementation of a ConnList list.
//
////////////////////////////////////////////////////////////////////////

#include "nspcl_.h"

ConnList :: ConnList ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pLst		= NULL;
	nalloc	= 0;
	ncnt		= 0;
	}	// ConnList

HRESULT ConnList :: add ( IUnknown *punkC, bool bRx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Add a connector to the list
	//
	//	PARAMETERS
	//		-	punkC is the connector
	//		-	bRx is true if reception should receive values
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	CNNE		*e	= NULL;

	// Thread safety
	cs.enter();

	// Ensure there is enough room
	if (ncnt == nalloc)
		{
		// New count
		nalloc = ((nalloc/10) + 1) * 10;

		// Make room
		CCLTRYE ( (pLst = (CNNE *) _REALLOCMEM(pLst,nalloc*sizeof(CNNE))) 
						!= NULL, E_OUTOFMEMORY );

		// Clear new entries
		CCLOK ( memset ( &pLst[ncnt], 0, (nalloc-ncnt)*sizeof(CNNE) ); )
		}	// if

	// Add new list
	if (hr == S_OK)
		{
		pLst[ncnt].bRx		= bRx;
		pLst[ncnt].pConn	= punkC;
		_ADDREF(punkC);
		++ncnt;
		}	// if

	// Thread safety
	cs.leave();

	return hr;
	}	// add

void ConnList :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	for (U32 i = 0;i < ncnt;++i)
		_RELEASE((pLst[i].pConn));
	_FREEMEM(pLst);

	}	// destruct

HRESULT ConnList :: remove ( IUnknown *punkC )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Remove a connector from the list
	//
	//	PARAMETERS
	//		-	punkC is the connector
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	CNNE		*e		= NULL;
	CNNE		*p		= NULL;
	U32		i;

	// Thread safety
	cs.enter();

	// Find specified connector.  Slow but not worth overhread just yet
	// of a separated, sorted array.  Order matters with connections.
	for (i = 0;i < ncnt;++i)
		if (pLst[i].pConn == punkC)
			break;

	// Found ?
	if (i < ncnt)
		{
		// One less entry
		_RELEASE(pLst[i].pConn);
		--ncnt;

		// Shift items above target location
		if (i < ncnt)
			memmove ( &pLst[i], &pLst[i+1], (ncnt-i)*sizeof(CNNE) );
		}	// if
	else
		{
//		dbgprintf (L"ConnList::remove:Connector not found:%p:%d\r\n", punkC, ncnt );
		hr = ERROR_NOT_FOUND;
		}	// if

	// Thread safety
	cs.leave();

	return hr;
	}	// remove

//////////////
// ConnListIt
//////////////

ConnListIt :: ConnListIt ( ConnList *pLst )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	//	PARMAETERS
	//		-	pLst is the list to iterate
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Setup
	pE		= NULL;
	ncnt	= 0;
	nit	= 0;

	// Make a snapshot of the current iteration list for this iterator.
	// This is necessary due to the list possibly changing during run time.
	pLst->cs.enter();

	// Any entries ?
	if ((ncnt = pLst->ncnt) > 0)
		{
		// Allocate own list
		CCLTRYE ( (pE = (CNNE *) _ALLOCMEM(ncnt*sizeof(CNNE)))
						!= NULL, E_OUTOFMEMORY );

		// Copy entries
		CCLOK ( memcpy ( pE, pLst->pLst, ncnt*sizeof(CNNE) ); )

		// Necessary ?
		if (hr == S_OK)
			for (U32 i = 0;i < ncnt;++i)
				_ADDREF(pE[i].pConn);
		}	// if

	// Done
	pLst->cs.leave();
	}	// ConnListIt

ConnListIt :: ~ConnListIt ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	for (U32 i = 0;i < ncnt;++i)
		_RELEASE((pE[i].pConn));
	_FREEMEM(pE);
	}	// ~ConnListIt

HRESULT ConnListIt :: next ( IUnknown **ppunkC, bool *pbRx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve the next entry from the list
	//
	//	PARAMETERS
	//		-	ppunkC will receive an AddRef'd pointer
	//		-	pbRx will receive the reception flag
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Any more ?
	if (nit >= ncnt)
		return S_FALSE;

	// Next entry
	*(ppunkC) = pE[nit].pConn;
	_ADDREF(*(ppunkC));
	if (pbRx != NULL)
		*pbRx = pE[nit].bRx;
	++nit;

	return hr;
	}	// next

