////////////////////////////////////////////////////////////////////////
//
//									SERVICEOP.CPP
//
//					Implementation of the service node
//
////////////////////////////////////////////////////////////////////////

#include "netl_.h"

ServiceOp :: ServiceOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	strName	= L"";
	hLookup	= NULL;
	pqs		= NULL;
	szqs		= 0;
	ulFlags	= 0;
	}	// Socket

HRESULT ServiceOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue	vL;

		// Attributes
		if (pnDesc->load ( adtString(L"Name"), vL ) == S_OK)
			hr = adtValue::toString(vL,strName);

		}	// if

	// Detach
	else
		{
		// Clean up
		if (hLookup != NULL)
			{
			WSALookupServiceEnd(hLookup);
			hLookup = NULL;
			}	// if
		_FREEMEM(pqs);
		}	// else

	return hr;
	}	// onAttach

HRESULT ServiceOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// First
	if (_RCP(First))
		{
		// Previous lookup
		if (hLookup != NULL)
			{
			WSALookupServiceEnd(hLookup);
			hLookup = NULL;
			_FREEMEM(pqs);
			}	// if

		// Also doing device queries
		ulFlags = LUP_CONTAINERS;

		// Return friendly name
		ulFlags |= (LUP_RETURN_NAME);

		// Obtain address
		ulFlags |= (LUP_RETURN_ADDR);

		// Ask lookup service to do a fresh lookup
		ulFlags |= (LUP_FLUSHCACHE);

		// Initial allocation of query set structure
		CCLTRYE ( (pqs = (LPWSAQUERYSET) _ALLOCMEM (sizeof(WSAQUERYSET)))
						!= NULL, E_OUTOFMEMORY );
		CCLOK ( szqs = sizeof(WSAQUERYSET); )

		// Start the lookup service search
		if (hr == S_OK)
			{
			// Add support for other specifications
			memset ( pqs, 0, szqs );
			pqs->dwNameSpace	= (!WCASECMP(strName,L"Bluetooth")) ? NS_BTH : NS_ALL;
			pqs->dwSize			= szqs;
			CCLTRYE ( WSALookupServiceBegin ( pqs, ulFlags, &hLookup )
							!= SOCKET_ERROR, WSAGetLastError() );
			}	// if

		// Proceed to 'next' service
		CCLTRY ( receive ( prNext, prl, v ) );
		}	// if

	// Next
	else if (_RCP(Next))
		{
		// State check
		CCLTRYE ( hLookup != NULL, ERROR_INVALID_STATE );

		// Next service
		CCLTRYE ( WSALookupServiceNext ( hLookup, ulFlags, &szqs, pqs )
						!= SOCKET_ERROR, WSAGetLastError() );

		// Success
		if (hr == S_OK)
			{
			lprintf ( LOG_DBG, L"Service Instance Name found : %s\r\n",
										pqs->lpszServiceInstanceName );
			}	// if

		// The buffer was not big enough
		else if (hr == WSAEFAULT)
			{
			// Allocate to larger size
			hr = S_OK;
			CCLTRYE ( (pqs = (LPWSAQUERYSET) _REALLOCMEM(pqs,szqs))
							!= NULL, E_OUTOFMEMORY );
			}	// else if

		// Result
//		if (hr == S_OK)
//			_EMT(Next,adtIUnknown(pDct) );
//		else
//			_EMT(Last,adtInt(hr) );

/*
		IDictionary	*pDct	= NULL;

		#ifdef	_WIN32
		// State check
		CCLTRYE ( pAddrs != NULL, ERROR_INVALID_STATE );

		// Current adapter
		if (hr == S_OK)
			{
			IList			*pAddrList	= NULL;
			IDictionary	*pAddrDct	= NULL;
			adtString	strName;

			// Proceed to next adapter
			PIP_ADAPTER_ADDRESSES
				pAddr	= pAddrs;
			pAddrs	= pAddrs->Next;

			// Create a context to receive the results
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );

			// Results.  More properties can be added over time as necessary.
//			CCLOK ( dbgprintf ( L"Friendly name : %s\r\n", pAddr->FriendlyName ); )
			CCLOK  ( strName = pAddr->AdapterName; )
			CCLTRY ( pDct->store ( adtString(L"Name"), strName ) );
			CCLTRY ( pDct->store ( adtString(L"FriendlyName"), adtString(pAddr->FriendlyName) ) );
			CCLTRY ( pDct->store ( adtString(L"Description"), adtString(pAddr->Description) ) );
			CCLTRY ( pDct->store ( adtString(L"Status"), 
							adtString	(	(pAddr->OperStatus == IfOperStatusDown)	? L"Down" :
												(pAddr->OperStatus == IfOperStatusUp)		? L"Up" : L"Unknown" ) ) );

			// Create a list to receive the addresses
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pAddrList ) );
			CCLTRY ( pDct->store ( adtString ( L"AddressList" ), adtIUnknown(pAddrList) ) );

			// Addresses
			for (PIP_ADAPTER_UNICAST_ADDRESS	pU	= pAddr->FirstUnicastAddress;
					hr == S_OK && pU != NULL;pU = pU->Next )
				{
				char				hostname[255];
				ULONG				msk;
				struct in_addr	in;

				// Create context for address information
				CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAddrDct ) );

				// Fill
				CCLTRY ( pAddrDct->store ( adtString(L"LeaseLifetime"), adtInt(pU->LeaseLifetime) ) );

				// Obtain string version of address
				CCLTRYE ( getnameinfo ( pU->Address.lpSockaddr, pU->Address.iSockaddrLength,	
												hostname, sizeof(hostname), NULL, 0, NI_NUMERICHOST ) == 0, GetLastError() );
//				CCLOK ( dbgprintf ( L"Address :       %S\r\n", hostname ); )
				CCLOK ( strName = hostname; )
				CCLTRY( pAddrDct->store ( adtString ( L"Address" ), strName ) );

				// String version of subnet
				CCLTRYE ( ConvertLengthToIpv4Mask ( pU->OnLinkPrefixLength, &msk ) == NO_ERROR, GetLastError() );
				CCLOK   ( in.s_addr = msk; )
				CCLTRYE(inet_ntop(AF_INET, &in, hostname, sizeof(hostname)) != NULL, GetLastError() );
				CCLOK ( strName = hostname; )
				CCLTRY( pAddrDct->store ( adtString ( L"Subnet" ), strName ) );

				// Add to list
				CCLTRY ( pAddrList->write ( adtIUnknown(pAddrDct) ) );

				// Clean up
				_RELEASE(pAddrDct);
				}	// for

			// Clean up
			_RELEASE(pAddrList);
			}	// if

		#elif	__unix__ || __APPLE__

		// State check
		CCLTRYE ( ifnxt != NULL, ERROR_INVALID_STATE );

		// Next interface
		if (hr == S_OK)
			{
			IList						*pAddrList	= NULL;
			IDictionary				*pAddrDct	= NULL;
			adtString				str;
			struct sockaddr_in	*pin	= (struct sockaddr_in *) (ifnxt->ifa_addr);

			// Information context
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );

			// Currently only emitted IPV4 addresses
			while (hr == S_OK && ifnxt != NULL)
				{
				// Address for this adapter
				pin	= (struct sockaddr_in *) (ifnxt->ifa_addr);
				if (pin->sin_family == AF_INET)
					break;
				
				// Next interface
				ifnxt = ifnxt->ifa_next;
				}	// while

			// Valid interface ?
			CCLTRYE ( ifnxt != NULL, ERROR_INVALID_STATE );
			
			// Interface name
			CCLOK  ( str = ifnxt->ifa_name; )
//			CCLOK  ( dbgprintf ( L"CommNetInterfaces::Name:%s\r\n", (PCWSTR)str ); )
			CCLTRY ( pDct->store ( adtString(L"Name"), str ) );
			CCLTRY ( pDct->store ( adtString(L"FriendlyName"), str ) );

			// Up flag for unix
			CCLTRY ( pDct->store ( adtString(L"Status"),
							adtString	( (ifnxt->ifa_flags & IFF_UP) ? L"Up" : L"Down" ) ) );
//			if (hr == S_OK)
//				{
//				dbgprintf ( L"Interfaces::Next:ifa_name %S\r\n", ifnxt->ifa_name );
//				dbgprintf ( L"Interfaces::Next:ifa_flags 0x%x\r\n", ifnxt->ifa_flags );
//				dbgprintf ( L"Interfaces::Next:ifa_addr 0x%x\r\n", pin->sin_addr );
//				dbgprintf ( L"Interfaces::Next:ifa_family 0x%x\r\n", pin->sin_family );
//				dbgprintf ( L"Interfaces::Next:ifa_port 0x%x\r\n", pin->sin_port );
//				dbgprintf ( L"Interfaces::Next:ifa_next 0x%x\r\n", (int)ifnxt->ifa_next );
//				}	// if
			
			// Create a list to receive the addresses
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pAddrList ) );
			CCLTRY ( pDct->store ( adtString ( L"AddressList" ), adtIUnknown(pAddrList) ) );

			// Currently just one address ?

			// Create context for address information
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAddrDct ) );

			// Address
			CCLOK		( str = inet_ntoa ( pin->sin_addr ); )
			CCLTRY	( pAddrDct->store ( adtString ( L"Address" ), str ) );

			// Add to list
			CCLTRY ( pAddrList->write ( adtIUnknown(pAddrDct) ) );

			// Next interface
			CCLOK   ( ifnxt = ifnxt->ifa_next; )

			// Clean up
			_RELEASE(pAddrDct);
			_RELEASE(pAddrList);
			}	// if
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Next,adtIUnknown ( pDct ) );
		else
			_EMT(Last,adtInt(hr) );

		// Clean up
		_RELEASE(pDct);
*/
		}	// if

	// State
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

