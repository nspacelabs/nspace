////////////////////////////////////////////////////////////////////////
//
//									MULTICAST.CPP
//
//					Implementation of the multicast socket node
//
////////////////////////////////////////////////////////////////////////

#include "netl_.h"

MulticastOp :: MulticastOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pSkt		= NULL;
	iAddrM	= 0;
	iAddrI	= 0;
	}	// Multicast

HRESULT MulticastOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		NetSkt_AddRef();

	// Detach
	else
		{
		_RELEASE(pSkt);
		NetSkt_Release();
		}	// else

	return S_OK;
	}	// onAttach

HRESULT MulticastOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Add
	if (_RCP(Add))
		{
		SOCKET	skt = INVALID_SOCKET;
		adtValue	vSkt;
		adtLong	lSkt;
		struct	ip_mreq	mreq;

		// State check
		CCLTRYE	( (pSkt != NULL), ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (SOCKET) (lSkt = vSkt); )
		CCLTRYE	( iAddrM != 0,	ERROR_INVALID_STATE );

		// Add address to membership for socket
		if (hr == S_OK)
			{
//			lprintf ( LOG_DBG, L"Add:%d\r\n", (U32)iAddrI );
			memset ( &mreq, 0, sizeof(mreq) );
			mreq.imr_multiaddr.s_addr	= htonl ( iAddrM );
			mreq.imr_interface.s_addr	= (iAddrI != 0) ? htonl ( iAddrI ) : INADDR_ANY;
			CCLTRYE ( setsockopt ( skt, IPPROTO_IP, IP_ADD_MEMBERSHIP,
							(char *) &mreq, sizeof(mreq) ) != -1, WSAGetLastError() );

			// Debug
			if (hr != S_OK)
				{
				char	asciiStrI[100],asciiStrM[100];

				// To string
				// Newer inet_ntop, etc not available on XP
				#if WINVER >= _WIN32_WINNT_VISTA
				inet_ntop(AF_INET, &(mreq.imr_interface), asciiStrI, sizeof(asciiStrI));
				inet_ntop(AF_INET, &(mreq.imr_multiaddr), asciiStrM, sizeof(asciiStrM));
				#else
				char				*cp;
				if ((cp = inet_ntoa ( mreq.imr_interface )) != NULL)
					STRCPY ( asciiStrI, sizeof ( asciiStrI ), cp );
				if ((cp = inet_ntoa ( mreq.imr_multiaddr )) != NULL)
					STRCPY ( asciiStrM, sizeof ( asciiStrM ), cp );
				#endif

				// Log error
				lprintf ( LOG_DBG, L"Unable to add membership:Interface:%S:Multicast:%S:hr %d\r\n",
											asciiStrI, asciiStrM, hr );
				}	// if

			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Add,adtIUnknown(pSkt) );
		else
			_EMT(Error,adtInt(hr) );

		// Debug
		if (hr != S_OK)
			lprintf(LOG_DBG, L"Failed:hr %d(0x%x)\r\n", hr, hr);
		}	// if

	// Remove
	else if (_RCP(Remove))
		{
		SOCKET	skt = INVALID_SOCKET;
		adtValue	vSkt;
		adtLong	lSkt;
		struct	ip_mreq	mreq;

		// State check
		CCLTRYE	( (pSkt != NULL), ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (SOCKET) (lSkt = vSkt); )
		CCLTRYE	( iAddrM != 0,	ERROR_INVALID_STATE );

		// Remove address from membership for socket
//		lprintf ( LOG_DBG, L"Remove:%d\r\n", (U32)iAddrI );
		if (hr == S_OK)
			{
			memset ( &mreq, 0, sizeof(mreq) );
			mreq.imr_multiaddr.s_addr	= htonl ( iAddrM );
			mreq.imr_interface.s_addr	= (iAddrI != 0) ? htonl ( iAddrI ) : INADDR_ANY;
			CCLTRYE ( setsockopt ( skt, IPPROTO_IP, IP_DROP_MEMBERSHIP,
							(char *) &mreq, sizeof(mreq) ) != -1, WSAGetLastError() );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Remove,adtIUnknown(pSkt) );
//		else
//			_EMT(Error, adtInt(hr) );
		}	// else if

	// Set a default outgoing interface
	else if (_RCP(Outgoing))
		{
		SOCKET			skt = INVALID_SOCKET;
		adtValue			vSkt;
		adtLong			lSkt;
		struct in_addr addr;
		adtInt			iPort;
		adtInt			iAddrO;

		// State check
		CCLTRYE	( (pSkt != NULL), ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (SOCKET) (lSkt = vSkt); )

		// Interface address
		CCLTRY ( NetSkt_Resolve ( v, iAddrO, iPort ) );

		// Set the provided interface address as the default outgoing interface
		CCLOK ( addr.s_addr = htonl ( iAddrO ); )
		CCLTRYE ( setsockopt ( skt, IPPROTO_IP, IP_MULTICAST_IF,
						(char *) &addr, sizeof(addr) ) != -1, WSAGetLastError() );

		// Result
		if (hr == S_OK)
			_EMT(Outgoing,adtIUnknown(pSkt) );
		else
			_EMT(Error, adtInt(hr) );
		}	// else if

	// Addresses
	else if (_RCP(Multicast))
		{
		adtInt	iPort;

		// Multi-cast address
		CCLTRY ( NetSkt_Resolve ( v, iAddrM, iPort ) );
		}	// else if
	else if (_RCP(Interface))
		{
		adtInt	iPort;

		// Interface address
		CCLTRY ( NetSkt_Resolve ( v, iAddrI, iPort ) );
		}	// else if

	// State
	else if (_RCP(Socket))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSkt);
		hr = _QISAFE(unkV,IID_IDictionary,&pSkt);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive


