////////////////////////////////////////////////////////////////////////
//
//									Interfaces.CPP
//
//			Implementation of the network interfaces node
//
////////////////////////////////////////////////////////////////////////

#include "netl_.h"

Interfaces :: Interfaces ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pThrd			= NULL;
	bRun			= false;
	hNotify		= NULL;
	hevNotify	= NULL;
	pDctNet		= NULL;
	pItNet		= NULL;
	pDctPrv		= NULL;

	}	// Interfaces

HRESULT Interfaces :: collect ( IDictionary **ppDct )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Collect the network information.
	//
	//	PARAMETERS
	//		-	ppDct will receive the network information dictionary
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr					= S_OK;
	IDictionary		*pDct				= NULL;
	#ifdef		_WIN32
	PIP_ADAPTER_ADDRESSES
						pAddr,pAddrs	= NULL;								// Adapter addresses
	DWORD				dwRet;
	ULONG				ulLen		= 0;
	#endif

	// Create master dictionary
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, ppDct ) );

	#ifdef	_WIN32

	// Default allocation
	CCLTRYE ( (pAddrs = (PIP_ADAPTER_ADDRESSES) _ALLOCMEM(sizeof(IP_ADAPTER_ADDRESSES)))
					!= NULL, E_OUTOFMEMORY );

	// Currently defaults to IPv4
	CCLTRYE ( (dwRet = GetAdaptersAddresses ( AF_INET, 0, NULL, pAddrs, &ulLen )) 
					== ERROR_SUCCESS, dwRet );
	
	// Need more space ?					
	if (hr == ERROR_BUFFER_OVERFLOW)
		{
		// Allocate required amount of memory for full information
		hr	= S_OK;
		_FREEMEM ( pAddrs );
		CCLTRYE	( ulLen != 0, ERROR_INVALID_STATE );
		CCLTRYE ( (pAddrs = (PIP_ADAPTER_ADDRESSES) _ALLOCMEM(ulLen)) != NULL, E_OUTOFMEMORY );

		// Obtain all information
		CCLTRYE ( (dwRet = GetAdaptersAddresses ( AF_INET, 0, NULL, pAddrs, &ulLen )) 
						== ERROR_SUCCESS, dwRet );
		}	// if

	// Collect information across all adapters
	CCLOK ( pAddr = pAddrs; )
	while (hr == S_OK && pAddr != NULL)
		{
		IList			*pAddrList	= NULL;
		IDictionary	*pAddrDct	= NULL;
		adtString	strName;

		// Create a context to receive the results
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );

		// Results.  More properties can be added over time as necessary.
//		CCLOK ( dbgprintf ( L"Friendly name : %s\r\n", pAddr->FriendlyName ); )
		CCLOK  ( strName = pAddr->AdapterName; )
		CCLTRY ( (*ppDct)->store ( strName, adtIUnknown(pDct) ) );
		CCLTRY ( pDct->store ( adtString(L"Name"), strName ) );
		CCLTRY ( pDct->store ( adtString(L"FriendlyName"), adtString(pAddr->FriendlyName) ) );
		CCLTRY ( pDct->store ( adtString(L"Description"), adtString(pAddr->Description) ) );
		CCLTRY ( pDct->store ( adtString(L"Status"), 
						adtString	(	(pAddr->OperStatus == IfOperStatusDown)	? L"Down" :
											(pAddr->OperStatus == IfOperStatusUp)		? L"Up" : L"Unknown" ) ) );

		// Create a list to receive the addresses
		CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pAddrList ) );
		CCLTRY ( pDct->store ( adtString ( L"AddressList" ), adtIUnknown(pAddrList) ) );

		// Addresses
		for (PIP_ADAPTER_UNICAST_ADDRESS	pU	= pAddr->FirstUnicastAddress;
				hr == S_OK && pU != NULL;pU = pU->Next )
			{
			char				hostname[255];
			ULONG				msk;
			struct in_addr	in;

			// Create context for address information
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAddrDct ) );

			// Fill
			CCLTRY ( pAddrDct->store ( adtString(L"LeaseLifetime"), adtInt(pU->LeaseLifetime) ) );

			// Obtain string version of address
			CCLTRYE ( getnameinfo ( pU->Address.lpSockaddr, pU->Address.iSockaddrLength,	
											hostname, sizeof(hostname), NULL, 0, NI_NUMERICHOST ) == 0, GetLastError() );
//				CCLOK ( dbgprintf ( L"Address :       %S\r\n", hostname ); )
			CCLOK ( strName = hostname; )
			CCLTRY( pAddrDct->store ( adtString ( L"Address" ), strName ) );

			// String version of subnet
			CCLTRYE ( ConvertLengthToIpv4Mask ( pU->OnLinkPrefixLength, &msk ) == NO_ERROR, GetLastError() );
			CCLOK   ( in.s_addr = msk; )
			CCLTRYE(inet_ntop(AF_INET, &in, hostname, sizeof(hostname)) != NULL, GetLastError() );
			CCLOK ( strName = hostname; )
			CCLTRY( pAddrDct->store ( adtString ( L"Subnet" ), strName ) );

			// Add to list
			CCLTRY ( pAddrList->write ( adtIUnknown(pAddrDct) ) );

			// Clean up
			_RELEASE(pAddrDct);
			}	// for

		// Clean up
		_RELEASE(pAddrList);
		_RELEASE(pDct);

		// Proceed to next adapter
		pAddr	= pAddr->Next;
		}	// while

	// Clean up
	_FREEMEM(pAddrs);

	#elif	__unix__ || __APPLE__
	struct ifaddrs	*ifnxt, *ifap	= NULL;

	// Obtain new list
	CCLTRYE ( getifaddrs ( &ifap ) == 0, errno );
	CCLOK   ( ifnxt = ifap; )

	// Collect information about all adapters
	while (hr == S_OK && ifnxt != NULL)
		{
		// Next interface
		IList						*pAddrList	= NULL;
		IDictionary				*pAddrDct	= NULL;
		struct sockaddr_in	*pin			= (struct sockaddr_in *) (ifnxt->ifa_addr);
		adtString				str;

		// Information context
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );

		// Currently only emitted IPV4 addresses
		while (hr == S_OK && ifnxt != NULL)
			{
			// Address for this adapter
			pin	= (struct sockaddr_in *) (ifnxt->ifa_addr);
			if (pin->sin_family == AF_INET)
				break;
				
			// Next interface
			ifnxt = ifnxt->ifa_next;
			}	// while

		// Valid interface ?
		if (ifnxt != NULL)
			{
			// Interface name
			CCLOK  ( str = ifnxt->ifa_name; )
//			CCLOK  ( dbgprintf ( L"CommNetInterfaces::Name:%s\r\n", (PCWSTR)str ); )
			CCLTRY ( (*ppDct)->store ( str, adtIUnknown(pDct) ) );
			CCLTRY ( pDct->store ( adtString(L"Name"), str ) );
			CCLTRY ( pDct->store ( adtString(L"FriendlyName"), str ) );

			// Up flag for unix
			CCLTRY ( pDct->store ( adtString(L"Status"),
							adtString	( (ifnxt->ifa_flags & IFF_UP) ? L"Up" : L"Down" ) ) );
//			if (hr == S_OK)
//				{
//				dbgprintf ( L"Interfaces::Next:ifa_name %S\r\n", ifnxt->ifa_name );
//				dbgprintf ( L"Interfaces::Next:ifa_flags 0x%x\r\n", ifnxt->ifa_flags );
//				dbgprintf ( L"Interfaces::Next:ifa_addr 0x%x\r\n", pin->sin_addr );
//				dbgprintf ( L"Interfaces::Next:ifa_family 0x%x\r\n", pin->sin_family );
//				dbgprintf ( L"Interfaces::Next:ifa_port 0x%x\r\n", pin->sin_port );
//				dbgprintf ( L"Interfaces::Next:ifa_next %p\r\n", ifnxt->ifa_next );
//				}	// if
				
			// Create a list to receive the addresses
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pAddrList ) );
			CCLTRY ( pDct->store ( adtString ( L"AddressList" ), adtIUnknown(pAddrList) ) );

			// Currently just one address ?

			// Create context for address information
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAddrDct ) );

			// Address
			CCLOK		( str = inet_ntoa ( pin->sin_addr ); )
			CCLTRY	( pAddrDct->store ( adtString ( L"Address" ), str ) );

			// Add to list
			CCLTRY ( pAddrList->write ( adtIUnknown(pAddrDct) ) );

			// Next interface
			CCLOK   ( ifnxt = ifnxt->ifa_next; )
			}	// if

		// Clean up
		_RELEASE(pAddrDct);
		_RELEASE(pAddrList);
		_RELEASE(pDct);
		}	// while

	// Clean up
	if (ifap != NULL)
		freeifaddrs(ifap);
	#endif

	return S_OK;
	}	// collect

HRESULT Interfaces :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Initialize run event
	CCLTRYE ( evRun.init() == true, E_UNEXPECTED );

	return hr;
	}	// construct

HRESULT Interfaces :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		NetSkt_AddRef();
	
	// Detach
	else
		{
		// Verify thread shutdown
		if (pThrd != NULL)
			pThrd->threadStop(5000);
		_RELEASE(pThrd);

		// Clean up
		_RELEASE(pDctPrv);
		_RELEASE(pItNet);
		_RELEASE(pDctNet);
		NetSkt_Release();
		}	// if

	return S_OK;
	}	// onAttach

HRESULT Interfaces :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	
	// First
	if (_RCP(First))
		{
		// Previous state
		_RELEASE(pItNet);
		_RELEASE(pDctNet);

		// Collect the latest information and iterate
		CCLTRY ( collect ( &pDctNet ) );
		CCLTRY ( pDctNet->iterate ( &pItNet ) );

		// Proceed to 'next' adapter
		CCLTRY ( receive ( prNext, prl, v ) );
		}	// if

	// Next
	else if (_RCP(Next))
		{
		IDictionary		*pDct	= NULL;
		adtValue			vL;
		adtIUnknown		unkV;

		// Obtain the next adapter dictionary
		CCLTRY ( pItNet->read ( vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IDictionary,&pDct) );

		// For next time
		CCLOK ( pItNet->next(); )

		// Result
		if (hr == S_OK)
			_EMT(Next,adtIUnknown ( pDct ) );
		else
			_EMT(Last,adtInt(hr) );

		// Clean up
		_RELEASE(pDct);
		}	// if

	// Start monitoring for changes
	else if (_RCP(Start))
		{
		// State check
		CCLTRYE ( pThrd == NULL, ERROR_INVALID_STATE );

		// Create monitoring thread
		CCLOK (bRun = true;)
		CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
		CCLTRY(pThrd->threadStart ( this, 0 ));
		}	// else if

	// Stop monitoring for changes
	else if (_RCP(Stop))
		{
		// State check
		CCLTRYE ( pThrd != NULL, ERROR_INVALID_STATE );

		// Shutdown worker thread
		CCLOK ( pThrd->threadStop(5000); )

		// Clean up
		_RELEASE(pThrd);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Interfaces :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Signal stop
	bRun = false;
	evRun.signal();
	#ifdef	_WIN32
	if (hevNotify != NULL)
		SetEvent ( hevNotify );
	#endif

	return S_OK;
	}	// tickAbort

HRESULT Interfaces :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr	= S_OK;

	// Debug
//	lprintf ( LOG_DBG, L"Interfaces::tick {\n" );

	// Issue a change signal immediately for the outside world since
	// the first state is always new
//	_EMT(Change,adtInt(0));

	// Simple delay between checks
	hr = (evRun.wait(5000) == FALSE) ? S_OK : S_FALSE;

/*
//		#ifdef	_WIN32
		OVERLAPPED	ov;

		// Request notification of address changes (this needs to be done every time)
		CCLOK ( memset ( &ov, 0, sizeof(ov) ); )
		CCLOK ( ov.hEvent = hevNotify; )
		CCLTRYE ( NotifyAddrChange ( &hNotify, &ov ) == ERROR_IO_PENDING,
						GetLastError() );

		// Wait for event
		CCLTRYE ( WaitForSingleObject ( hevNotify, INFINITE ) == WAIT_OBJECT_0,
						GetLastError() );
		CCLTRYE ( (bRun == true), S_FALSE );

		// Signal change
		CCLOK ( dbgprintf ( L"Interfaces::tick:Change\r\n" ); )
		CCLOK ( _EMT(Change,adtInt(0) ); )
//		#elif	__unix__ || __APPLE__
*/

	// Collect recent network information to compare against previous
	// No obvious API for *nix to notify of network changes so doing it manually.
	// Use same logic for Windows even though Windows has built-in notification ?
	if (hr == S_OK)
		{
		IDictionary	*pDctNow = NULL;
		IIt			*pItp		= NULL;
		IIt			*pItn		= NULL;
		bool			bDiff		= false;
		U32			szn,szp;
		adtValue		vn,vp;
		adtIUnknown	unkV;

		// Obtain current network info
		CCLTRY ( collect ( &pDctNow ) );

		// Sanity check
		CCLTRYE ( pDctPrv != NULL && pDctNow != NULL, E_UNEXPECTED );

		// First easy check to see if the count is different
		CCLTRY( pDctPrv->size ( &szp ));
		CCLTRY( pDctNow->size ( &szn ));
		CCLOK	( bDiff = (szp != szn); )

		// Iterate all adapters
		CCLTRY ( pDctPrv->iterate ( &pItp ) );
		CCLTRY ( pDctNow->iterate ( &pItn ) );
		while (	hr == S_OK						&&
					!bDiff							&&
					pItp->read ( vp ) == S_OK	&& 
					pItn->read ( vn ) == S_OK)
			{
			IDictionary *pDctAn = NULL;
			IDictionary *pDctAp = NULL;

			// Access adapter dictionaries
			CCLTRY ( _QISAFE((unkV=vp), IID_IDictionary, &pDctAp ) );
			CCLTRY ( _QISAFE((unkV=vn), IID_IDictionary, &pDctAn ) );

			// Compare names
			if (	hr == S_OK	&&
					!bDiff		&&
					pDctAp->load ( adtString(L"Name"), vp ) == S_OK &&
					pDctAn->load ( adtString(L"Name"), vn ) == S_OK )
				bDiff = (adtValue::compare ( vp, vn ) != 0);

			// Compare status
			if (	hr == S_OK	&&
					!bDiff		&&
					pDctAp->load ( adtString(L"Status"), vp ) == S_OK &&
					pDctAn->load ( adtString(L"Status"), vn ) == S_OK )
				bDiff = (adtValue::compare ( vp, vn ) != 0);

			// Access the address lists
			if (	hr == S_OK	&& 
					!bDiff		&&
					pDctAp->load ( adtString(L"AddressList"), vp ) == S_OK &&
					pDctAn->load ( adtString(L"AddressList"), vn ) == S_OK )
				{
				// Address list dictionary
				IContainer	*pLstp	= NULL;
				IContainer	*pLstn	= NULL;
				IIt			*pItlp	= NULL;
				IIt			*pItln	= NULL;

				// Obtain iterators
				CCLTRY ( _QISAFE((unkV=vp), IID_IContainer, &pLstp ) );
				CCLTRY ( pLstp->iterate ( &pItlp ) );
				CCLTRY ( _QISAFE((unkV=vn), IID_IContainer, &pLstn ) );
				CCLTRY ( pLstn->iterate ( &pItln ) );

				// Compare the addresses
				while (	hr == S_OK						&& 
							!bDiff							&&
							pItlp->read ( vp ) == S_OK &&
							pItln->read ( vn ) == S_OK )
					{
					IDictionary	*pDctp	= NULL;
					IDictionary	*pDctn	= NULL;

					// Compare address in list, do all ?
					bDiff = (
								// First address in previous state
								(IUnknown *)(NULL)					== (unkV=vp)		||
								_QI(unkV,IID_IDictionary,&pDctp) != S_OK				||
								pDctp->load ( adtString(L"Address"), vp ) != S_OK	||

								// First address in now state
								(IUnknown *)(NULL)					== (unkV=vn)		||
								_QI(unkV,IID_IDictionary,&pDctn) != S_OK				||
								pDctn->load ( adtString(L"Address"), vn ) != S_OK	||

								// Different address ?
								adtValue::compare ( vn, vp ) != 0);

//					lprintf ( LOG_DBG, L"Network interface address compare from %s to %s\r\n",
//										(LPCWSTR)adtString(vp), (LPCWSTR)adtString(vn) );
					if (bDiff)
						lprintf ( LOG_DBG, L"Network interface address has changed from %s to %s\r\n",
										(LPCWSTR)adtString(vp), (LPCWSTR)adtString(vn) );

					// Clean up
					_RELEASE(pDctn);
					_RELEASE(pDctp);

					// Next address pair
					pItlp->next();
					pItln->next();
					}	// while

				// Clean up
				_RELEASE(pItln);
				_RELEASE(pLstn);
				_RELEASE(pItlp);
				_RELEASE(pLstp);
				}	// if

			// Clean up
			_RELEASE(pDctAn);
			_RELEASE(pDctAp);

			// Proceed to next adapter
			pItp->next();
			pItn->next();
			}	// while

		// Difference ?
		if (hr == S_OK && bDiff)
			{
			// Now is now the previous
			_RELEASE(pDctPrv);
			pDctPrv = pDctNow;
			_ADDREF(pDctPrv);

			// Notify
			_EMT(Change,adtInt(0) );
			}	// if

		// Clean up
		_RELEASE(pItp);
		_RELEASE(pItn);
		_RELEASE(pDctNow);
		}	// if

	// Debug
//	lprintf ( LOG_DBG, L"} Interfaces::tick (0x%x)\n", hr );

	return hr;
	}	// tick

HRESULT Interfaces :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;

	// Debug
//	lprintf ( LOG_DBG, L"Interfaces::tickBegin {\n" );

	// Create an event to be signaled for address changes
	#if	defined(_WIN32)
	CCLTRYE ( (hevNotify = CreateEvent ( NULL, FALSE, FALSE, NULL )) != NULL,
					GetLastError() );

	#endif

	// Start with the current network info
	CCLTRY ( collect ( &pDctPrv ) );

	// Debug
//	lprintf ( LOG_DBG, L"} Interfaces::tickBegin (0x%x)\n", hr );

	return hr;
	}	// tickBegin

HRESULT Interfaces :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"Interfaces::tickEnd {}\n" );

	// Clean up
	_RELEASE(pDctPrv);
	#if	defined(_WIN32)
	if (hevNotify != NULL)
		{
		CloseHandle ( hevNotify );
		hevNotify = NULL;
		}	// if
	#endif

	return S_OK;
	}	// tickEnd


		/*
		#ifdef	_WIN32
		DWORD	dwRet;
		ULONG	ulLen = 0;

		// Clean up
		_FREEMEM(pAddrs);

		// Default allocation
		CCLTRYE ( (pAddrs = (PIP_ADAPTER_ADDRESSES) _ALLOCMEM(sizeof(IP_ADAPTER_ADDRESSES)))
						!= NULL, E_OUTOFMEMORY );

		// Currently defaults to IPv4
		CCLTRYE ( (dwRet = GetAdaptersAddresses ( AF_INET, 0, NULL, pAddrs, &ulLen )) 
						== ERROR_SUCCESS, dwRet );
	
		// Need more space ?					
		if (hr == ERROR_BUFFER_OVERFLOW)
			{
			// Allocate required amount of memory for full information
			hr	= S_OK;
			_FREEMEM ( pAddrs );
			CCLTRYE	( ulLen != 0, ERROR_INVALID_STATE );
			CCLTRYE ( (pAddrs = (PIP_ADAPTER_ADDRESSES) _ALLOCMEM(ulLen)) != NULL, E_OUTOFMEMORY );

			// Obtain all information
			CCLTRYE ( (dwRet = GetAdaptersAddresses ( AF_INET, 0, NULL, pAddrs, &ulLen )) 
							== ERROR_SUCCESS, dwRet );
			}	// if

		#elif	__unix__ || __APPLE__
		if (hr == S_OK)
			{
			// Previous interface list
			if (ifap != NULL)
				{
				freeifaddrs(ifap);
				ifap 	= NULL;
				ifnxt	= NULL;
				}	// if

			// Obtain new list
			CCLTRYE ( getifaddrs ( &ifap ) == 0, errno );
			CCLOK   ( ifnxt = ifap; )
			}	// if
		#endif
		*/

		/*
		#ifdef	_WIN32
		// State check
		CCLTRYE ( pAddrs != NULL, ERROR_INVALID_STATE );

		// Current adapter
		if (hr == S_OK)
			{
			IList			*pAddrList	= NULL;
			IDictionary	*pAddrDct	= NULL;
			adtString	strName;

			// Proceed to next adapter
			PIP_ADAPTER_ADDRESSES
				pAddr	= pAddrs;
			pAddrs	= pAddrs->Next;

			// Create a context to receive the results
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );

			// Results.  More properties can be added over time as necessary.
//			CCLOK ( dbgprintf ( L"Friendly name : %s\r\n", pAddr->FriendlyName ); )
			CCLOK  ( strName = pAddr->AdapterName; )
			CCLTRY ( pDct->store ( adtString(L"Name"), strName ) );
			CCLTRY ( pDct->store ( adtString(L"FriendlyName"), adtString(pAddr->FriendlyName) ) );
			CCLTRY ( pDct->store ( adtString(L"Description"), adtString(pAddr->Description) ) );
			CCLTRY ( pDct->store ( adtString(L"Status"), 
							adtString	(	(pAddr->OperStatus == IfOperStatusDown)	? L"Down" :
												(pAddr->OperStatus == IfOperStatusUp)		? L"Up" : L"Unknown" ) ) );

			// Create a list to receive the addresses
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pAddrList ) );
			CCLTRY ( pDct->store ( adtString ( L"AddressList" ), adtIUnknown(pAddrList) ) );

			// Addresses
			for (PIP_ADAPTER_UNICAST_ADDRESS	pU	= pAddr->FirstUnicastAddress;
					hr == S_OK && pU != NULL;pU = pU->Next )
				{
				char				hostname[255];
				ULONG				msk;
				struct in_addr	in;

				// Create context for address information
				CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAddrDct ) );

				// Fill
				CCLTRY ( pAddrDct->store ( adtString(L"LeaseLifetime"), adtInt(pU->LeaseLifetime) ) );

				// Obtain string version of address
				CCLTRYE ( getnameinfo ( pU->Address.lpSockaddr, pU->Address.iSockaddrLength,	
												hostname, sizeof(hostname), NULL, 0, NI_NUMERICHOST ) == 0, GetLastError() );
//				CCLOK ( dbgprintf ( L"Address :       %S\r\n", hostname ); )
				CCLOK ( strName = hostname; )
				CCLTRY( pAddrDct->store ( adtString ( L"Address" ), strName ) );

				// String version of subnet
				CCLTRYE ( ConvertLengthToIpv4Mask ( pU->OnLinkPrefixLength, &msk ) == NO_ERROR, GetLastError() );
				CCLOK   ( in.s_addr = msk; )
				CCLTRYE(inet_ntop(AF_INET, &in, hostname, sizeof(hostname)) != NULL, GetLastError() );
				CCLOK ( strName = hostname; )
				CCLTRY( pAddrDct->store ( adtString ( L"Subnet" ), strName ) );

				// Add to list
				CCLTRY ( pAddrList->write ( adtIUnknown(pAddrDct) ) );

				// Clean up
				_RELEASE(pAddrDct);
				}	// for

			// Clean up
			_RELEASE(pAddrList);
			}	// if

		#elif	__unix__ || __APPLE__

		// State check
		CCLTRYE ( ifnxt != NULL, ERROR_INVALID_STATE );

		// Next interface
		if (hr == S_OK)
			{
			IList						*pAddrList	= NULL;
			IDictionary				*pAddrDct	= NULL;
			adtString				str;
			struct sockaddr_in	*pin	= (struct sockaddr_in *) (ifnxt->ifa_addr);

			// Information context
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );

			// Currently only emitted IPV4 addresses
			while (hr == S_OK && ifnxt != NULL)
				{
				// Address for this adapter
				pin	= (struct sockaddr_in *) (ifnxt->ifa_addr);
				if (pin->sin_family == AF_INET)
					break;
				
				// Next interface
				ifnxt = ifnxt->ifa_next;
				}	// while

			// Valid interface ?
			CCLTRYE ( ifnxt != NULL, ERROR_INVALID_STATE );
			
			// Interface name
			CCLOK  ( str = ifnxt->ifa_name; )
//			CCLOK  ( dbgprintf ( L"CommNetInterfaces::Name:%s\r\n", (PCWSTR)str ); )
			CCLTRY ( pDct->store ( adtString(L"Name"), str ) );
			CCLTRY ( pDct->store ( adtString(L"FriendlyName"), str ) );

			// Up flag for unix
			CCLTRY ( pDct->store ( adtString(L"Status"),
							adtString	( (ifnxt->ifa_flags & IFF_UP) ? L"Up" : L"Down" ) ) );
//			if (hr == S_OK)
//				{
//				dbgprintf ( L"Interfaces::Next:ifa_name %S\r\n", ifnxt->ifa_name );
//				dbgprintf ( L"Interfaces::Next:ifa_flags 0x%x\r\n", ifnxt->ifa_flags );
//				dbgprintf ( L"Interfaces::Next:ifa_addr 0x%x\r\n", pin->sin_addr );
//				dbgprintf ( L"Interfaces::Next:ifa_family 0x%x\r\n", pin->sin_family );
//				dbgprintf ( L"Interfaces::Next:ifa_port 0x%x\r\n", pin->sin_port );
//				dbgprintf ( L"Interfaces::Next:ifa_next 0x%x\r\n", (int)ifnxt->ifa_next );
//				}	// if
			
			// Create a list to receive the addresses
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pAddrList ) );
			CCLTRY ( pDct->store ( adtString ( L"AddressList" ), adtIUnknown(pAddrList) ) );

			// Currently just one address ?

			// Create context for address information
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAddrDct ) );

			// Address
			CCLOK		( str = inet_ntoa ( pin->sin_addr ); )
			CCLTRY	( pAddrDct->store ( adtString ( L"Address" ), str ) );

			// Add to list
			CCLTRY ( pAddrList->write ( adtIUnknown(pAddrDct) ) );

			// Next interface
			CCLOK   ( ifnxt = ifnxt->ifa_next; )

			// Clean up
			_RELEASE(pAddrDct);
			_RELEASE(pAddrList);
			}	// if
		#endif
		*/
