////////////////////////////////////////////////////////////////////////
//
//									InterfaceOp.CPP
//
//			Implementation of the network interface node
//
////////////////////////////////////////////////////////////////////////

#include "netl_.h"
#if	defined(__unix__)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <net/if_arp.h>
#endif

InterfaceOp :: InterfaceOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////

	// Defaults
	strName	= L"";
	strAddr	= L"";
	strGate	= L"";
	strHost	= L"";
	strDNS	= L"";
	bDHCP		= false;
	}	// InterfaceOp

HRESULT InterfaceOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		{
		adtValue	v;

		// Defaults
		if (pnDesc->load ( adtString(L"Name"), v ) == S_OK)
			adtValue::toString(v,strName);
		if (pnDesc->load ( adtString(L"Address"), v ) == S_OK)
			adtValue::toString(v,strAddr);
		if (pnDesc->load ( adtString(L"Netmask"), v ) == S_OK)
			adtValue::toString(v,strMask);
		if (pnDesc->load ( adtString(L"Gateway"), v ) == S_OK)
			adtValue::toString(v,strGate);
		if (pnDesc->load ( adtString(L"Hostname"), v ) == S_OK)
			adtValue::toString(v,strHost);
		if (pnDesc->load ( adtString(L"DNS"), v ) == S_OK)
			adtValue::toString(v,strDNS);
		if (pnDesc->load ( adtString(L"DHCP"), v ) == S_OK)
			bDHCP = adtBool(v);

		}	// if
	
	// Detach
	else
		{
		}	// if

	return S_OK;
	}	// onAttach

HRESULT InterfaceOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Apply network settings
	if (_RCP(Apply))
		{
		//
		// Linux
		//
		#if	defined(__unix__)
		char 	*pc	= NULL;
		int	ret;

		// Hostname
//		lprintf ( LOG_DBG, L"Apply Name %s Hostname %s Address %s DHCP %s\r\n", 
//						(LPCWSTR)strName, (LPCWSTR)strHost, (LPCWSTR)strAddr, (bDHCP) ? L"true" : L"false" );
		if (hr == S_OK && strHost.length() > 0)
			{
			// ASCII version
			CCLTRY ( strHost.toAscii(&pc) );

			// Activate
			if (hr == S_OK)
				{
				ret = sethostname ( pc, strlen(pc) );
				if (ret != 0)
					lprintf ( LOG_DBG, L"Error setting hostname : %d\r\n", ret );
				}	// if

			// Clean up
			_FREEMEM(pc);
			}	// if

		// State check
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Interface name to ASCII
		CCLTRY ( strName.toAscii(&pc) );

		//
		// Fixed IP
		//
		if (hr == S_OK && bDHCP == false)
			{
			char	cmd[1024];

			// Initally no command.  Placing all command in one string to avoid executing too fast.
			strcpy ( cmd, "" );

			// Valid address ?
			if (strAddr.length() > 0)
				{
				char	*pcAddr	= NULL;
				char	*pcMask	= NULL;
				char	subcmd[200];

				// Addresses to ASCII
				CCLTRY ( strAddr.toAscii(&pcAddr) );
				if (hr == S_OK && strMask.length() > 0)
					hr = strMask.toAscii(&pcMask);

				// Generate the 'ifconfig' command line to set IP address
				sprintf ( subcmd, "/sbin/ifconfig %s %s %s %s", pc, pcAddr,
								(pcMask != NULL) ? "netmask" : "",
								(pcMask != NULL) ? pcMask : "" );


				// Add to command
				strcpy ( cmd, subcmd );

				// Execute
//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", cmd );
//				CCLTRYE ( (ret = system ( cmd )) == 0, errno );
//				lprintf ( LOG_DBG, L"Result : %d", hr );

				// Clean up
				_FREEMEM(pcMask);
				_FREEMEM(pcAddr);
				}	// if

			// Gateway update ?
			if (strGate.length() > 0)
				{
				char	*pcAddr	= NULL;
				char	subcmd[200];

				// Addresses to ASCII
				CCLTRY ( strGate.toAscii(&pcAddr) );

				// The current default gateway must be deleted to avoid an error
				// add the new one
				sprintf ( subcmd, "/sbin/route del default %s", pc );
				if (strlen(cmd) > 0)
					strcat ( cmd, " ; " );
				strcat ( cmd, subcmd );

//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", cmd );
//				CCLTRYE ( (ret = system ( cmd )) == 0, errno );
//				lprintf ( LOG_DBG, L"Result : %d", hr );

				// Execute the 'route' command line to set gateway
				sprintf ( subcmd, "/sbin/route add default gw %s %s", pcAddr, pc );
				if (strlen(cmd) > 0)
					strcat ( cmd, " ; " );
				strcat ( cmd, subcmd );
//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", cmd );
//				CCLTRYE ( (ret = system ( cmd )) == 0, errno );
//				lprintf ( LOG_DBG, L"Result : %d", hr );

				// Clean up
				_FREEMEM(pcAddr);
				}	// if

			// Execute command
			if (strlen(cmd) > 0)
				{
//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", cmd );
				CCLTRYE ( (ret = system ( cmd )) == 0, errno );
//				lprintf ( LOG_DBG, L"Result : %d", hr );
				}	// if

			}	// if

		//
		// DHCP
		//
		else if (hr == S_OK)
			{
			// Linux environment DHCP can be tricky.
			struct stat st;

			// For embedded/Busybox environment the 'udhcpc' utility can
			// be use to request an address
			// /sbin/udhcpc -R -b -i eth0
			// -R means to release IP on exit
			//	-b means run in background if lease not obtained
			// -i specifices the interface name

			// Does the utility exist ?
			if (stat("/sbin/udhcpc",&st) == 0)
				{
				char	cmd[200];

				// Format and execute the command
				// -R Release IP on exit
				// -b Run in background if lease not obtained (accumulates processes after each run, really need it ?)
				// -f Run in foreground
				// -q Quit after obtaining lease
				// -i <Interface to use>
//				sprintf ( cmd, "/sbin/udhcpc -R -b -i %s", pc );
//				sprintf ( cmd, "/sbin/udhcpc -f -q -i %s", pc );
				sprintf ( cmd, "/sbin/udhcpc -b -q -S -i %s", pc );
//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", cmd );
				ret = system ( cmd );
//				if (ret)
//					lprintf ( LOG_DBG, L"Result : %d", ret );
				}	// if
			else
				lprintf ( LOG_DBG, L"Unable to find suitable DHCP client\r\n" );
			}	// else

		// Clean up
		_FREEMEM(pc);
		#endif
		}	// if

	// Apply WiFi settings
	else if (_RCP(ApplyWiFi))
		{
		#if	defined(__unix__)

		// State check
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Anything specified ?
		if (hr == S_OK && strSsid.length() > 0)
			{
			// UGLY, not portable: On Linux, there does not seem to be an easy command line way to 
			// change the wireless settings so a temporary 'wpa_supplicant.conf' file is
			// created with the required setings and then reloaded by the 'wpa_supplicant' utility.
			FILE *cf 	= NULL;
			char 	*pc	= NULL;
			char 	*ps	= NULL;
			char 	*pk	= NULL;
			char 	cfg[41],bfr[1024];
			int	ret;

			// Debug
//			CCLOK ( lprintf ( LOG_DBG, L"Apply Name %s Ssid %s Key %s\r\n", 
//									(LPCWSTR)strName, (LPCWSTR)strSsid, (LPCWSTR)strKey ); )

			// Strings to ASCII
			CCLTRY ( strName.toAscii(&pc) );
			CCLTRY ( strSsid.toAscii(&ps) );
			CCLTRY ( strKey.toAscii(&pk) );

			// Use internal file system objects ?  For now this should work...
			if (hr == S_OK)
				{
				sprintf ( cfg, "/tmp/%s_wpa_nspace.conf", pc );
				hr = ( (cf = fopen ( cfg, "w" )) != NULL ) ? S_OK : E_UNEXPECTED;
				}	// if

			// Put out the needed configuration.  Make node properties as necessary.
			if (hr == S_OK)
				{
				fprintf ( cf, "country=us\r\n" );
				fprintf ( cf, "update_config=1\r\n" );
				fprintf ( cf, "ctrl_interface=/var/run/wpa_nspace\r\n" );
				fprintf ( cf, "\r\n" );
				fprintf ( cf, "network={\r\n" );
				fprintf ( cf, "ssid=\"%s\"\r\n", ps );
				fprintf ( cf, "psk=\"%s\"\r\n", pk );
				fprintf ( cf, "scan_ssid=1\r\n" );
				fprintf ( cf, "}\r\n" );
				fclose ( cf );
				cf = NULL;
				}	// if

			// Ensure the wpa_supplicant is running on this configuration.
			// This will give errors if it is already running which is fine but
			// supress output.
			if (hr == S_OK)
				{
				// Execute
				sprintf ( bfr, "/usr/sbin/wpa_supplicant -qqq -B -c %s -i %s", cfg, pc );
//				sprintf ( bfr, "/usr/sbin/wpa_supplicant -B -c %s -i %s", cfg, pc );
//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", bfr );
				CCLTRYE ( (ret = system ( bfr )) >= 0, ret );
//				lprintf ( LOG_DBG, L"Result : %d", hr );

				// Give new process time to create the control socket used by wpa_cli
				CCLOK ( usleep ( 100000 ); )
				}	// if

			// Tell the wpa_supplicant to re-load the configuration
			if (hr == S_OK)
				{
				// Execute
				sprintf ( bfr, "/usr/sbin/wpa_cli -p /var/run/wpa_nspace -i %s reconfigure", pc );
//				lprintf ( LOG_DBG, L"Executing '%S'\r\n", bfr );
				CCLTRYE ( (ret = system ( bfr )) >= 0, ret );
//				lprintf ( LOG_DBG, L"Result : %d", hr );
				}	// if

			// Result
			if (hr != S_OK)
				{
				lprintf ( LOG_DBG, L"Unable to apply WiFi settings %s (%S,%d)\r\n",
											(LPCWSTR)strName, bfr, ret );
				_EMT(Error,adtInt(hr));
				}	// if

			// Clean up
			_FREEMEM(pk);
			_FREEMEM(ps);
			_FREEMEM(pc);

			// Generate the 'iwconfig' command line needed to set wireless parmaters
//			CCLOK ( sprintf ( cmd, "/usr/sbin/iwconfig %s mode Managed essid '%s' key s:'%s'", 
//							pc, ps, pk ); )

			}	// if

		#endif

		/*
		//
		// Linux
		// Assumes wireless tools are available (iwconfig and wpa_supplicant)
		//	NOTE: This functionality re-writes the wpa_supplicant.conf file which
		// requires root access.  Not pretty but there doesn't seem to be a
		// command line way of configuration WPA connections
		//
		char	cmd[1024];

		// State check
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Anything specified ?
		if (hr == S_OK && strSsid.length() > 0)
			{
			// Debug
			CCLOK ( lprintf ( LOG_DBG, L"Apply Name %s Ssid %s Key %s\r\n", 
									(LPCWSTR)strName, (LPCWSTR)strSsid, (LPCWSTR)strKey ); )

			// Interface name to ASCII
			CCLTRY ( strName.toAscii(&pc) );
			CCLTRY ( strSsid.toAscii(&ps) );
			CCLTRY ( strKey.toAscii(&pk) );

			// Generate the 'iwconfig' command line needed to set wireless parmaters
			CCLOK ( sprintf ( cmd, "/usr/sbin/iwconfig %s mode Managed essid '%s' key s:'%s'", 
							pc, ps, pk ); )

			// Execute
			lprintf ( LOG_DBG, L"Executing '%S'\r\n", cmd );
			CCLTRYE ( (ret = system ( cmd )) == 0, ret );
			lprintf ( LOG_DBG, L"Result : %d", hr );

			// Result
			if (hr != S_OK)
				{
				lprintf ( LOG_DBG, L"Unable to apply WiFi settings %s (%s,%d)\r\n",
											(LPCWSTR)strName, cmd, ret );
				_EMT(Error,adtInt(hr));
				}	// if

			// Clean up
			_FREEMEM(pk);
			_FREEMEM(ps);
			_FREEMEM(pc);
			}	// if
		#endif
		*/
		}	// else if

	// Query
	else if (_RCP(Query))
		{
		#if	defined(__unix__)
		adtString	strV;
		char			cBfr[1024];
		int			ret;

		// Hostname
		if (hr == S_OK)
			{
			// Retrieve
			ret = gethostname ( cBfr, sizeof(cBfr) );
			if (ret != 0)
				lprintf ( LOG_DBG, L"Error getting hostname : %d\r\n", ret );
			
			// Result
			if (!ret)
				_EMT(Hostname,(strV = cBfr));
			}	// if

		// State check
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Retrieve information about interface
		if (hr == S_OK)
			{
			SOCKET			skt	= INVALID_SOCKET;
			char 				*pc	= NULL;
			struct ifreq 	ifr;

			// Create a socket to access network layer
			CCLTRYE ( (skt = socket ( AF_INET, SOCK_DGRAM, 0 )) != INVALID_SOCKET,
							WSAGetLastError() );

			// Interface name
			CCLTRY ( strName.toAscii ( &pc ) );
			CCLOK  ( strcpy ( ifr.ifr_name, pc ); )
			CCLOK  ( ifr.ifr_addr.sa_family = AF_INET; )

			// Flags
			CCLTRYE( (ioctl ( skt, SIOCGIFFLAGS, &ifr ) == 0), errno );

			// Interface up/down
			_EMT(Up,adtBool( ((ifr.ifr_flags & IFF_UP) == IFF_UP) ? true : false ));

			// Address
			CCLTRYE( (ioctl ( skt, SIOCGIFADDR, &ifr ) == 0), errno );
			CCLOK(_EMT(Address,adtInt( ntohl(((struct sockaddr_in *)&(ifr.ifr_addr))->sin_addr.s_addr) ));)

			// Address (MAC)
			CCLTRYE( (ioctl ( skt, SIOCGIFHWADDR, &ifr ) == 0), errno );
			if (hr == S_OK && ifr.ifr_hwaddr.sa_family == ARPHRD_ETHER)
				{
				const unsigned char *mac = (unsigned char*)ifr.ifr_hwaddr.sa_data;
				WCHAR						wMac[21];

				// Sending out as string version
				swprintf	(	SWPF(wMac,20), L"%02X:%02X:%02X:%02X:%02X:%02X",
							    mac[0], mac[1], mac[2], mac[3], mac[4], mac[5] );
				_EMT(AddressHw,adtString(wMac));
				}	// if

			// Netmask
			CCLTRYE( (ioctl ( skt, SIOCGIFNETMASK, &ifr ) == 0), errno );
			CCLOK(_EMT(Netmask,adtInt( ntohl(((struct sockaddr_in *)&(ifr.ifr_netmask))->sin_addr.s_addr) ));)

			// Gateway
			if (hr == S_OK)
				{
				FILE 			*pf 		= NULL;
				char			*cp,*np	= NULL;
				adtString	strV;

				// Use shell to grab routing network status

				// Execute command while retrieving access to the pipe stream
				CCLTRYE ( (pf = popen ("/bin/netstat -rn", "r" )) != NULL, errno );

				//
				// Typical output from command is :
				// Kernel IP routing table
				// Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
				// 0.0.0.0         192.168.1.1     0.0.0.0         UG        0 0          0 eth0
				// 192.168.1.0     0.0.0.0         255.255.255.0   U         0 0          0 eth0
				//
				// Look for and use the gateay on the first line that has a 0.0.0.0 destination
				// as the default gateway.  Primitive but should work for single Ethernet systems.
				// 

				// Debug
//				lprintf ( LOG_DBG, L"%s : Get gateway for %s : %d\r\n", 
//								(LPCWSTR)strnName, (LPCWSTR)strName, hr );
				while (hr == S_OK && fgets ( cBfr, sizeof(cBfr), pf ) != NULL)
					{
					// Does line start with null address ?
					if (strncmp ( cBfr, "0.0.0.0", 7 ))
						continue;

					// Skip white space
					cp = &cBfr[7];
					while (*cp == ' ' && *cp != '\0')
						++cp;

					// Find first trailing space of gateway IP
					np = cp;
					while (*np != ' ' && *np != '\0')
						++np;

					// Gateway address is in between...
//					lprintf ( LOG_DBG, L"%s: cp %S : np %S\r\n", (LPCWSTR)strnName, cp, np );
					if (*cp != '\0' && *np != '\0')
						{
						// Terminate at space
						*np = '\0';

						// Address
						strV = cp;
						}	// if

					}	// while

				// Clean up
				if (pf != NULL)
					pclose(pf);

				// Valid gateway string ?
				CCLTRYE ( strV.length() > 0, E_UNEXPECTED );

				// Result
				CCLOK ( _EMT(Gateway,strV); )

				// Debug
				if (hr != S_OK)
					lprintf ( LOG_DBG, L"%s : Error getting gateway %s : %d\r\n", 
									(LPCWSTR)strnName, (LPCWSTR)strName, hr );

				// Do not signal error just for gateway
				if (hr != S_OK)
					hr = S_OK;
				}	// if

			// Debug
			if (hr != S_OK)
				{
//				lprintf ( LOG_DBG, L"Error retrieving interface information : %s (%d)\r\n",
//								(LPCWSTR)strName, errno );
				if (hr == ENODEV)
					_EMT(NotFound,strName);
				else
					_EMT(Error,adtInt(hr));
				}	// if

			// Clean up
			_FREEMEM(pc);
			if (skt != INVALID_SOCKET)
				close(skt);
			}	// if

		#endif
		}	// else if

	// Interface up/down
	else if (_RCP(Up))
		{
		adtBool 	bUp(v);

		// State check
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		//
		// Linux
		//
		#if	defined(__unix__)
		char 	*pc	= NULL;
		char	cmd[1024];
		int	ret;

		// Ascii version for API
		CCLTRY ( strName.toAscii(&pc) );

		// Command for interface
		CCLOK ( sprintf ( cmd, "/sbin/ifconfig %s %s", pc, 
								(bUp == true) ? "up" : "down" ); )

		// Up/Down
//		CCLOK ( lprintf ( LOG_DBG, L"Executing : %S\r\n", cmd ); )
		CCLTRYE ( (ret = system ( cmd )) == 0, errno );

		// Result
		if (hr != S_OK && strName.length() > 0)
			{
			lprintf ( LOG_DBG, L"Unable to change state of adapter %s (%d)\r\n",
										(LPCWSTR)strName, ret );
			_EMT(Error,adtInt(hr));
			}	// if

		// Clean up
		_FREEMEM(pc);
		#endif
		}	// else if

	// State
	else if (_RCP(Name))
		hr = adtValue::toString ( v, strName );
	else if (_RCP(Address))
		hr = adtValue::toString ( v, strAddr );
	else if (_RCP(Netmask))
		hr = adtValue::toString ( v, strMask );
	else if (_RCP(Gateway))
		hr = adtValue::toString ( v, strGate );
	else if (_RCP(Hostname))
		hr = adtValue::toString ( v, strHost );
	else if (_RCP(DHCP))
		bDHCP = adtBool(v);
	else if (_RCP(DNS))
		hr = adtValue::toString ( v, strDNS );
	else if (_RCP(Ssid))
		hr = adtValue::toString ( v, strSsid );
	else if (_RCP(Key))
		hr = adtValue::toString ( v, strKey );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

/*
IOCTL way

SOCKET	skt	= INVALID_SOCKET;
struct 	ifreq 		ifr;
struct	sockaddr_in	addr;
adtInt	iAddr,iPort;

// Create a socket to access network layer
CCLTRYE ( (skt = socket ( AF_INET, SOCK_DGRAM, 0 )) != INVALID_SOCKET,
				WSAGetLastError() );

// Resolve address and set
if (strAddr.length() > 0 && NetSkt_Resolve(strAddr,iAddr,iPort) == S_OK)
	{
	// New address
	memset ( &addr, 0, sizeof(addr) );
	addr.sin_family 		= AF_INET;
	addr.sin_addr.s_addr	= htonl((U32)iAddr);

	// Prepare request
	memset ( &ifr, 0, sizeof(ifr) );
	strcpy ( ifr.ifr_name, pc );
	memcpy ( &(ifr.ifr_addr), &addr, sizeof(struct sockaddr) );

	// Execute
	CCLTRYE ( (ioctl ( skt, SIOCSIFADDR, &ifr ) == 0), errno );
	}	// if

// Resolve address and set
if (strMask.length() > 0 && NetSkt_Resolve(strMask,iAddr,iPort) == S_OK)
	{
	// New address
	memset ( &addr, 0, sizeof(addr) );
	addr.sin_family 		= AF_INET;
	addr.sin_addr.s_addr	= htonl((U32)iAddr);

	// Prepare request
	memset ( &ifr, 0, sizeof(ifr) );
	strcpy ( ifr.ifr_name, pc );
	memcpy ( &(ifr.ifr_addr), &addr, sizeof(struct sockaddr) );

	// Execute
	CCLTRYE ( (ioctl ( skt, SIOCSIFNETMASK, &ifr ) == 0), errno );
	}	// if


// Clean up
_FREEMEM(pc);
if (skt != INVALID_SOCKET)
	close(skt);
*/

