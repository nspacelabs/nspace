////////////////////////////////////////////////////////////////////////
//
//									SEND.CPP
//
//					Implementation of the send stream node
//
////////////////////////////////////////////////////////////////////////

#include "netl_.h"

// Globals
DWORD dwTickTx = 0;

Send :: Send ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pSkt		= NULL;
	pStm		= NULL;
	iSz		= 0;
	pBfr		= NULL;
	uSzBfr	= 0;
//	iTo		= 2000;
	iTo		= 20000;
	}	// Send

HRESULT Send :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		NetSkt_AddRef();

	// Detach
	else
		{
		// Clean up
		_RELEASE(pSkt);
		_RELEASE(pStm);
		_FREEMEM(pBfr);
		uSzBfr = 0;
		NetSkt_Release();
		}	// else

	return S_OK;
	}	// onAttach

HRESULT Send :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Transmit
	if (_RCP(Fire))
		{
		SOCKET	skt	= INVALID_SOCKET;
		U32		uWrd	= 0;
		adtValue	vSkt;
		adtLong	lSkt;
		U32		uWrl,uIdx;
		U64		uWr;

//		DWORD	dwThen = GetTickCount();

		// State check
		CCLTRYE	( pSkt != NULL && pStm != NULL && iSz > 0, ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );
		CCLOK		( skt = (SOCKET) (lSkt = vSkt); )

		// Ensure internal buffer has enough memory for outgoing stream
		if (hr == S_OK && uSzBfr < iSz)
			{
			// Re-size internal buffer
			CCLTRYE	( (pBfr = (U8 *) _REALLOCMEM ( pBfr, iSz+1 )) != NULL, E_OUTOFMEMORY );
			CCLOK		( uSzBfr = iSz; )
			}	// if

		// Access stream data
		CCLTRY ( pStm->read ( pBfr, iSz, &uWr ) );

		// Debug
//		if (hr == S_OK)
//			{
//			pBfr[uWr] = '\0';
//			dbgprintf ( L"Transmit : %S\r\n", (char *)pBfr );
//			}	// if

		// Send entire buffer
		CCLOK ( uWrl 	= (U32)uWr; )
		uIdx	= 0;
		while (hr == S_OK && uWrl > 0)
			{
			// Send buffer
			CCLTRY ( NetSkt_Send ( skt, &pBfr[uIdx], uWrl, &uWrd, iTo ) );
//			if (hr == S_OK && uWrd != uWr)
//				lprintf ( LOG_DBG, L"NetSkt_Send:Incomplete send hr 0x%x:uWrl %d uWr %d:uWrd %d\r\n", hr, uWrl, uWr, uWrd );
//		dwTickTx = GetTickCount();

			// Block complete
			CCLOK ( uWrl -= uWrd; )
			CCLOK ( uIdx += uWrd; )
			}	// while

		// Result
		if (hr == S_OK)	_EMT(Fire,adtInt(uWrd));
		else					_EMT(Error,adtInt(hr));

		// Debug
//		dbgprintf ( L"Send::receive:hr 0x%x:skt %d:uWrd %d:uWr %d\r\n", hr, skt, uWrd, uWr );
		if (hr != S_OK)
			dbgprintf ( L"Send::receive:Error:hr 0x%x\r\n", hr );
		}	// if

	// State
	else if (_RCP(Socket))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSkt);
		hr = _QISAFE(unkV,IID_IDictionary,&pSkt);
		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStm);
		hr = _QISAFE(unkV,IID_IByteStream,&pStm);
		}	// else if
	else if (_RCP(Size))
		iSz = v;
	else if (_RCP(Timeout))
		iTo = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

