////////////////////////////////////////////////////////////////////////
//
//									Avail.CPP
//
//					Implementation of the socket Avail node
//
////////////////////////////////////////////////////////////////////////

#include "netl_.h"
#include <stdio.h>

Avail :: Avail ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the availability node
	//
	////////////////////////////////////////////////////////////////////////
	pThrd			= NULL;
	pAvails		= NULL;
	pInSkt		= NULL;
	bAvail		= false;
	pSkt			= NULL;
	bRead			= true;
	bWrite		= false;
	iTo			= 1000;
	sktNotify	= INVALID_SOCKET;
	nsds			= 0;
	psds			= NULL;
	bsds			= false;
	}	// Avail

void Avail :: notify ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Trigger internal notification
	//
	////////////////////////////////////////////////////////////////////////

	// Save current notification socket
	SOCKET tmp 	= sktNotify;

	// Create new notification socket for next time
	sktNotify = socket ( AF_INET, SOCK_DGRAM, 0 );

	// Signal current notification socekt by closing it
	if (tmp != INVALID_SOCKET)
		closesocket(tmp);

	}	// notify

HRESULT Avail :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue	v;

		// Initialize sockets
		CCLTRY ( NetSkt_AddRef() );

		// Objects
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pAvails ) );
		CCLTRY ( pAvails->keys ( &pInSkt ));

		// Attributes
		if (hr == S_OK && pnDesc->load ( adtString(L"Timeout"), v ) == S_OK)
			iTo = v;
		if (hr == S_OK && pnDesc->load ( adtString(L"Read"), v ) == S_OK)
			bRead = v;
		if (hr == S_OK && pnDesc->load ( adtString(L"Write"), v ) == S_OK)
			bWrite = v;
		}	// if

	// Detach
	else
		{
		// Shutdown thread
		if (pThrd != NULL)
			{
			pThrd->threadStop(5000);
			pThrd->Release();
			pThrd = NULL;
			}	// if

		// Clean up
		_RELEASE(pInSkt);
		_RELEASE(pSkt);
		_RELEASE(pAvails);

		// Free sockets
		CCLTRY ( NetSkt_Release() );
		}	// if

	return hr;
	}	// onAttach

HRESULT Avail :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Start
	if (_RCP(Start))
		{
		// State check
		CCLTRYE ( pThrd == NULL, ERROR_INVALID_STATE );
//		dbgprintf ( L"%s:Avail::receive:Start\r\n", (LPCWSTR)strnName );

		// Debug
//		lprintf ( LOG_DBG, L"%s(%p):Start:pAvails %p:%d\n", 
//						(LPCWSTR)strnName, this, pAvails, hr );

		// Start server thread
		CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
		CCLOK (bAvail = true;)
		CCLTRY(pThrd->threadStart ( this, 5000, strnName ));
		}	// if

	// Stop
	else if (_RCP(Stop))
		{
		// State check
		CCLTRYE ( pThrd != NULL, ERROR_INVALID_STATE );
//		dbgprintf ( L"%s:Avail::receive:Stop\r\n", (LPCWSTR)strnName );

		// Debug
//		lprintf ( LOG_DBG, L"%s(%p):Stop:pAvails %p:%d\n",
//						(LPCWSTR)strnName, this, pAvails, hr );

		// Shutdown worker thread
		if (pThrd != NULL)
			pThrd->threadStop(10000);
		_RELEASE(pThrd);
		}	// if

	// Add
	else if (_RCP(Add))
		{
		adtValue	vSkt;
		adtLong	lSkt;

		// State check
		CCLTRYE	( pSkt != NULL, ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );

		// Store in dictionary with socket as key and dictionary as value
//		lprintf ( LOG_DBG, L"%s:Avail::receive:Add:%d\r\n", (LPCWSTR)strnName, vSkt.vlong );
		CCLTRY	( pAvails->store ( (lSkt = vSkt), adtIUnknown(pSkt) ) );
		CCLOK		( bsds = true; )
		CCLOK		( notify(); )
		}	// else if

	// Remove
	else if (_RCP(Remove))
		{
		adtValue	vSkt;
		adtLong	lSkt;

		// State check
		CCLTRYE	( pSkt != NULL, ERROR_INVALID_STATE );
		CCLTRY	( pSkt->load ( adtString(L"Socket"), vSkt ) );

		// Remove from list
//		lprintf ( LOG_DBG, L"%s:Avail::receive:Remove:%d\r\n", (LPCWSTR)strnName, vSkt.vlong );
		CCLTRY	( pAvails->remove ( (lSkt = vSkt) ) );
		CCLOK		( bsds = true; )
		CCLOK		( notify(); )
		}	// else if

	// State
	else if (_RCP(Socket))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSkt);
		hr = _QISAFE(unkV,IID_IDictionary,&pSkt);
		}	// else if
	else if (_RCP(Timeout))
		iTo = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Avail :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Turn off ticking
//	lprintf ( LOG_DBG, L"%s(%p)\r\n", (LPCWSTR)strnName, this );
	bAvail = false;
	notify();

	return S_OK;
	}	// tickAbort

HRESULT Avail :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	int				s			= 0;
	adtValue			vSkt;

	// Debug
//	dbgprintf ( L"Avail::tick { %s\r\n", (LPCWSTR)strnName );

	// Sanity check
	CCLTRYE ( (bAvail == true), S_FALSE );
	if (hr != S_OK)
		return hr;

	// Has descriptor list changed ?
	if (bsds)
		{
		U32	cnt;

		// Updating
		bsds = false;
		nsds = 0;
 
		// Count total number of sockets in list
		CCLTRY ( pAvails->size ( &cnt ) );
		CCLOK  ( ++cnt; )								// Plus one for notification socket

		// Memory for list
		CCLTRYE	( (psds = (POLLFD *) _REALLOCMEM ( psds, cnt*sizeof(POLLFD) )) != NULL,
						E_OUTOFMEMORY );
		CCLOK		( memset ( psds, 0, cnt*sizeof(POLLFD) ); )

		// Add internal notification socket for kicking out of wait
		if (hr == S_OK && sktNotify != INVALID_SOCKET)
			{
			psds[nsds].events	= POLLIN;
			psds[nsds++].fd	= sktNotify;
			}	// if
		else if (sktNotify == INVALID_SOCKET)
			lprintf ( LOG_WARN, L"sktNotify is invalid\r\n" );

		// Add list of sockets
		CCLTRY( pInSkt->begin() );
		while (hr == S_OK && pInSkt->read ( vSkt ) == S_OK)
			{
			// Requested status
 			if (bRead)	psds[nsds].events	|= POLLIN;
 			if (bWrite) psds[nsds].events	|= POLLOUT;

			// Add to list
			psds[nsds++].fd = (SOCKET)(U64)adtLong(vSkt);

			// Next
			pInSkt->next();
			}	// while

		}	// if

	// Wait for activity
	if (hr == S_OK && psds != NULL)
		s = POLL ( psds, nsds, iTo );

	// Any events ?
	if (hr == S_OK && s > 0)
		{
		// Process events for each socket, skip notification socket
		for (int idx = 1;idx < (int)nsds;++idx)
			{
			// Any events for this socket ?
			if (!psds[idx].revents) continue;

			// Access socket dictionary
			if (pAvails->load ( adtLong(psds[idx].fd), vSkt ) == S_OK)
				{
				// Handle events
				if (psds[idx].revents & POLLIN)
					{
					_EMT(Read,vSkt);
					}	// if
				if (psds[idx].revents & POLLOUT)
					{
					_EMT(Write,vSkt);
					}	// if
				if (psds[idx].revents & (POLLERR|POLLHUP|POLLNVAL))
					{
					_EMT(Error,vSkt);
					}	// if
				}	// if
			}	// for
		}	// if

	/*
	// Debug
	#ifdef	_WIN32
	if (hr != S_OK && bAvail == true)
		{
		dbgprintf ( L"Avail::tick:Returning error! 0x%x\n", hr );
		}	// if
	#endif
//	dbgprintf ( L"} Avail::tick\n" );
	*/

	return hr;
	}	// tick

HRESULT Avail :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

//	lprintf ( LOG_DBG, L"%s(%p):Read:%s:Write:%s {}\n",
//						(LPCWSTR)strnName, this,
//						(bRead == true) ? L"true" : L"false",
//						(bWrite == true)? L"true" : L"false" );

	// Initial notification socket creation
	notify();

	// A poll list is needed
	bsds	= true;

	return hr;
	}	// tickBegin

HRESULT Avail :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"%s(%p)\r\n", (LPCWSTR)strnName, this );

	// Clean up
	_FREEMEM(psds);
	nsds = 0;
	notify();

	return S_OK;
	}	// tickEnd

