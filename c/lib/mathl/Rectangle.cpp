////////////////////////////////////////////////////////////////////////
//
//									Rectangle.CPP
//
//					Implementation of the rectangle node
//
////////////////////////////////////////////////////////////////////////

#include "mathl_.h"
#include <stdio.h>
#include <math.h>

Rectangle :: Rectangle ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	dX[0] = 0.0;
	dY[0] = 0.0;
	dX[1] = 0.0;
	dY[1] = 0.0;
	dX0	= HUGE_VAL;
	dY0	= HUGE_VAL;
	dX1	= HUGE_VAL;
	dY1	= HUGE_VAL;
	}	// Rectangle

void Rectangle :: infoEmit ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Emit the latest rectangle information
	//
	////////////////////////////////////////////////////////////////////////

	// Corners
	_EMT(X0,adtDouble(dX[0]));
	_EMT(Y0,adtDouble(dY[0]));
	_EMT(X1,adtDouble(dX[1]));
	_EMT(Y1,adtDouble(dY[1]));

	// Size
	_EMT(Width,adtDouble(dX[1]-dX[0]));
	_EMT(Height,adtDouble(dY[1]-dY[0]));

	// Center
	_EMT(CenterX,adtDouble(dX[0] + (dX[1]-dX[0])/2.0));
	_EMT(CenterY,adtDouble(dY[0] + (dY[1]-dY[0])/2.0));

	}	// infoEmit

void Rectangle :: infoUpdate ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Update rectangle information based on received values.
	//
	////////////////////////////////////////////////////////////////////////
	double tmp;

	// Defaults (empty)
	dX[0] = dX[1] = dY[0] = dY[0] = 0.0;

	//
	// X
	//

	// Size specified ?
	if (dW != 0)
		{
		// Center specified ?
		if (dXc != 0)
			{
			dX[0] = dXc - dW/2;
			dX[1] = dXc + dW/2;
			}	// if

		// Left specified ?
		else if (dX0 != HUGE_VAL)
			{
			dX[0] = dX0;
			dX[1] = dX0 + dW;
			}	// else if

		// Right specified ?
		else if (dX1 != HUGE_VAL)
			{
			dX[0] = dX1 - dW;
			dX[1] = dX1;
			}	// else if

		}	// if

	// Assume corners
	else
		{
		dX[0] = dX0;
		dX[1] = dX1;
		}	// else

	//
	// Y
	//

	// Size specified ?
	if (dH != 0)
		{
		// Center specified ?
		if (dYc != 0)
			{
			dY[0] = dYc - dH/2;
			dY[1] = dYc + dH/2;
			}	// if

		// Top specified ?
		else if (dY0 != HUGE_VAL)
			{
			dY[0] = dY0;
			dY[1] = dY0 + dH;
			}	// else if

		// Bottom specified ?
		else if (dY1 != HUGE_VAL)
			{
			dY[0] = dY1 - dH;
			dY[1] = dY1;
			}	// else if

		}	// if

	// Assume corners
	else
		{
		dY[0] = dY0;
		dY[1] = dY1;
		}	// else

	// Ensure X0,Y0 are the lesser coordinate
	if (dX[0] > dX[1])
		{
		tmp	= dX[0];
		dX[0]	= dX[1];
		dX[1]	= tmp;
		}	// if
	if (dY[0] > dY[1])
		{
		tmp	= dY[0];
		dY[0]	= dY[1];
		dY[1]	= tmp;
		}	// if

	}	// infoUpdate

HRESULT Rectangle :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// State check
	if (!bAttach) return S_OK;

	// Default states
	if (pnDesc->load ( adtString(L"X0"), vL ) == S_OK)
		dX0 = vL;
	if (pnDesc->load ( adtString(L"X1"), vL ) == S_OK)
		dX1 = vL;
	if (pnDesc->load ( adtString(L"Y0"), vL ) == S_OK)
		dY0 = vL;
	if (pnDesc->load ( adtString(L"Y1"), vL ) == S_OK)
		dY1 = vL;
	if (pnDesc->load ( adtString(L"X"), vL ) == S_OK)
		dXpt = vL;
	if (pnDesc->load ( adtString(L"Y"), vL ) == S_OK)
		dYpt = vL;
	if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
		dW = vL;
	if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
		dH = vL;

	return S_OK;
	}	// onAttach

HRESULT Rectangle :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Is point inside rectangle
	if (_RCP(PointIn))
		{
		bool	bInt;

		// Intersection ? Handle inverted coordinates
		bInt = ( dXpt >= dX[0] && dXpt <= dX[1] &&
					dYpt >= dY[0] && dYpt <= dY[1] );

		// Debug
//		lprintf ( LOG_DBG, L"(%g,%g) in (%g,%g,%g,%g) ? %d\r\n",
//						(double)dXpt, (double)dYpt,
//						dX[0], dY[0], dX[1], dY[1], (bInt) ? 1 : 0 );

		// Result
		if (bInt)	_EMT(In,adtBool(bInt));
		else			_EMT(NotIn,adtBool(bInt));
		}	// if

	// Rectangle parameters are updated after each new parameter
	else if (_RCP(CenterX)	|| _RCP(CenterY)	||
				_RCP(Width)		|| _RCP(Height)	||
				_RCP(X0)			|| _RCP(Y0)			||
				_RCP(X1)			|| _RCP(Y1)			)
		{
		// Update relevant parameter
		if			(_RCP(CenterX))	dXc = v;
		else if	(_RCP(CenterY))	dYc = v;
		else if	(_RCP(Width))		dW = v;
		else if	(_RCP(Height))		dH = v;
		else if	(_RCP(X0))			dX0 = v;
		else if	(_RCP(Y0))			dY0 = v;
		else if	(_RCP(X1))			dX1 = v;
		else if	(_RCP(Y1))			dY1 = v;

		// Update the rectangle
		infoUpdate();

		// Emit the latest rectangle information
		infoEmit();
		}	// else if

	// State
	else if (_RCP(X))
		dXpt = v;
	else if (_RCP(Y))
		dYpt = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive


