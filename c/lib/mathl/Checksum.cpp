////////////////////////////////////////////////////////////////////////
//
//									Checksum.CPP
//
//					Implementation of the checksum node
//
////////////////////////////////////////////////////////////////////////

#include "mathl_.h"
#include <stdio.h>

Checksum :: Checksum ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	bTwos = false;
	sType = L"Sum";
	iSz	= 1;
	iChks	= 0;
	iOff	= 0;
	iLen	= 0;
	}	// Checksum

HRESULT Checksum :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// State check
	if (!bAttach) return S_OK;

	// Debug
//	if (!WCASECMP(this->strnName,L"MissAdd"))
//		dbgprintf ( L"Hi\r\n" );

	// Default states

	// Size of checksum (1,2,4,etc) (required)
	if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
		iSz = vL;

	// Length of block, 0 = all of data
	if (pnDesc->load ( adtString(L"Length"), vL ) == S_OK)
		iLen = vL;

	// Offset into block
	if (pnDesc->load ( adtString(L"Offset"), vL ) == S_OK)
		iOff = vL;

	// Twos compliment
	if (pnDesc->load ( adtString(L"ComplementTwo"), vL ) == S_OK)
		bTwos = vL;

	// Checksum type
	if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
		adtValue::toString(vL,sType);

	// Checksum start
	if (pnDesc->load ( adtString(L"Checksum"), vL ) == S_OK)
		iChks = vL;

	return S_OK;
	}	// onAttach

HRESULT Checksum :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Fire
	if (_RCP(Fire))
		{
		bool		bXOR		= false;
		bool		bFletch	= false;
		bool		bPoly		= false;
		U32		chkRes	= 0;
		U32		i,j,flag;
		U8			byte;
		adtValue	vUse;

		// Value to use
		if (hr == S_OK && adtValue::empty ( vSource ) == false )
			hr = adtValue::copy ( vSource, vUse );
		else if (hr == S_OK)
			hr = adtValue::copy ( v, vUse );

		// Node properties
		CCLOK ( bXOR		= !WCASECMP(sType,L"XOR"); )
		CCLOK ( bFletch	= !WCASECMP(sType,L"FLETCHER"); )
		CCLOK ( bPoly		= !WCASECMP(sType,L"POLYNOMIAL"); )
		CCLTRYE ( iSz == (U32)1 || iSz == (U32)2 || iSz == (U32)4, ERROR_INVALID_STATE );

		// String checksum
		if (hr == S_OK && vUse.vtype == VTYPE_STR)
			{
			adtString	sUse(vUse);
			U32			sLen	= sUse.length();

			// Size
			switch (iSz.vint)
				{
				// 8-bit
				case 1 :
					{
					U8	chk = 0;

					// Checksum
					chk = 0;
					for (i = iOff.vint;hr == S_OK && i < sLen;++i)
						chk += (char)(sUse[i] & 0xff);

					// Two's compliment ?
					if (bTwos == true)
						chk = ~(chk) + 1;

					// Result
					chkRes = chk;
					}	// case 1
					break;

				// 16-bit
				case 2 :
					{
					U8		sum1	= 0;
					U8		sum2	= 0;
					U8		byte;
					for (i = iOff.vint;i < sLen;++i)
						{
						// Current byte
						byte = (U8)(sUse[i] & 0xff);

						// Fletcher checksum
						if (bFletch)
							{
							// Algorithm
							if ((255 - sum1) < byte) ++sum1;
							sum1 = sum1 + byte;
							if (sum1 == 255) sum1 = 0;

							if ((255 - sum2) < sum1) ++sum2;
							sum2 = sum2 + sum1;
							if (sum2 == 255) sum2 = 0;
							}	// if
						}	// for

					// Store result
					chkRes = (U8) ((sum1 << 8) | sum2);

					// Two's compliment ?
					if (bTwos == true)
						chkRes = ~(chkRes) + 1;
					}	// case 2
					break;

				default :
					hr = E_NOTIMPL;
				}	// switch

			}	// if

		// Binary checksum
		else if (hr == S_OK && vUse.vtype == VTYPE_UNK)
			{
			IByteStream		*pStm	= NULL;
			U8					sum1,sum2,chk;

			// Byte stream ?
			if (vUse.punk != NULL && _QI(vUse.punk,IID_IByteStream,&pStm) == S_OK)
				{
				// Offset into stream
				if (hr == S_OK && iOff != 0)
					hr = pStm->seek ( iOff, STREAM_SEEK_CUR, NULL );

				// Only supports 2 byte checksum for these types
				CCLTRYE ( !bFletch || iSz.vint == 2, E_UNEXPECTED );
				CCLTRYE ( !bPoly || iSz.vint == 2, E_UNEXPECTED );

				// Checksum start
				CCLOK ( chk	= (bXOR) ? 0xff : 0x00; )
				if (hr == S_OK && iChks != 0)
					chk = (U8)iChks;

				// Checksum
				sum1	= sum2 = 0;
				for (i = 0;i < iLen && hr == S_OK;++i)
					{
					// Next byte
					CCLTRY ( pStm->read ( &byte, sizeof(byte), NULL ) );

					// Checksum
					if (bFletch && iSz.vint == 2)
						{
						// Algorithm
						if ((255 - sum1) < byte) ++sum1;
						sum1 = sum1 + byte;
						if (sum1 == 255) sum1 = 0;

						if ((255 - sum2) < sum1) ++sum2;
						sum2 = sum2 + sum1;
						if (sum2 == 255) sum2 = 0;

						// Update result
						chkRes = (sum1 << 8) | sum2;
						}	// if

					// Polynomial
					else if (bPoly && iSz.vint == 2)
						{
						chkRes = (chkRes ^ byte);
						for (j = 0;j < 8;++j)
							{
							flag = (chkRes & 1);
							chkRes  = (chkRes >> 1);
							if (flag)
								chkRes = chkRes ^ (0xA001);
							}	// for
						}	// else if

					else
						{
						if (bXOR)	chk ^= byte;
						else			chk += byte;
						chkRes = chk;
						}	// else

					}	// for

				// Two's compliment ?
				if (bTwos == true)
					chkRes = ~(chkRes) + 1;

				// Clean up
				_RELEASE(pStm);
				}	// if

			}	// else if
		else hr = E_NOTIMPL;

		// Emit checksum
		if (hr == S_OK)
			{
			if			(iSz == 1)	_EMT(Fire, adtInt ( (chkRes & 0xff) ) );
			else if	(iSz == 2)	_EMT(Fire, adtInt ( (chkRes & 0xffff) ) );
			else if	(iSz == 4)	_EMT(Fire, adtInt ( chkRes ) );
			}	// if

		}	// if

	// State
	else if (_RCP(Source))
		hr = adtValue::copy ( v, vSource );
	else if (_RCP(Length))
		iLen = v;

	return hr;
	}	// receive
