////////////////////////////////////////////////////////////////////////
//
//										MATHL_.H
//
//				Implementaiton include file for the math library
//
////////////////////////////////////////////////////////////////////////

#ifndef	MATHL__H
#define	MATHL__H

// Includes
#include	"mathl.h"
#include <random>

// Operations
#define	MATHOP_NOP		-1

// Arithmetic
#define	MATHOP_ADD		1
#define	MATHOP_SUB		2
#define	MATHOP_MUL		3
#define	MATHOP_DIV		4
#define	MATHOP_MOD		5

// Bitwise
#define	MATHOP_AND		10
#define	MATHOP_OR		11
#define	MATHOP_XOR		12
#define	MATHOP_LEFT		13
#define	MATHOP_RIGHT	14
#define	MATHOP_NOT		15

// Vector
#define	MATHOP_DOT		20
#define	MATHOP_CROSS	21

// Trig
#define	MATHOP_COS		30
#define	MATHOP_SIN		31
#define	MATHOP_TAN		32
#define	MATHOP_ACOS		33
#define	MATHOP_ASIN		34
#define	MATHOP_ATAN		35
#define	MATHOP_ATAN2	36
#define	MATHOP_C2P		37								// Cartesian to polar
#define	MATHOP_P2C		38								// Power to cartesian

// Other
#define	MATHOP_ABS		40
#define	MATHOP_NORM		41
#define	MATHOP_CEIL		42
#define	MATHOP_FLOOR	43
#define	MATHOP_SQRT		44
#define	MATHOP_MIN		45
#define	MATHOP_MAX		46
#define	MATHOP_RAND		47
#define	MATHOP_ROTL		48
#define	MATHOP_ROTR		49

// Casting
#define	MATHOP_INT		50
#define	MATHOP_LONG		51
#define	MATHOP_FLOAT	52
#define	MATHOP_DOUBLE	53
#define	MATHOP_DATE		54
#define	MATHOP_STRING	55
#define	MATHOP_UINT		56
#define	MATHOP_ULONG	57

// Comparison
#define	MATHOP_EQ		60
#define	MATHOP_GT		61
#define	MATHOP_GE		62
#define	MATHOP_LT		63
#define	MATHOP_LE		64
#define	MATHOP_NE		65

// Conversions
#define	MATHOP_QTOER	70								// Quaternion to Euler (radians)
#define	MATHOP_QTOED	71								// Quaternion to Euler (deg)
#define	MATHOP_ERTOQ	72								// Euler (radians) to Quaternion
#define	MATHOP_EDTOQ	73								// Euler (deg) to Quaternion

// Power
#define	MATHOP_EXP		80								// e^x
#define	MATHOP_POW10	81								// 10^x
#define	MATHOP_LOG10	82								// log10(x)
#define	MATHOP_LOG		83								// log(x) (natural log)
#define	MATHOP_POW		84								// x^y

// Radians <-> degrees
#define	RAD_TO_DEG(a)		(a)*(180.0/3.14159265358979323846)
#define	DEG_TO_RAD(a)		(a)*(3.14159265358979323846/180.0)

//
// Class - Binary.  Node to perform a binary operation.
//

class Binary :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Binary ( void );										// Constructor

	// Run-time data
	adtValue		vL,vR;									// Parameters
	adtValue		vRes;										// Result
	int			iOp;										// Math operation

	// CCL
	CCL_OBJECT_BEGIN(Binary)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	DECLARE_RCP(Left)
	DECLARE_RCP(Right)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
		DEFINE_RCP(Left)
		DEFINE_RCP(Right)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Checksum.  Node to calculate the checksum.
//

class Checksum :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Checksum ( void );									// Constructor

	// Run-time data
	adtInt			iSz,iOff,iLen,iChks;				// Parameters
	adtString		sType;								// Checksum type
	adtValue			vSource;								// Data source for checksum
	adtBool			bTwos;								// Two's compliment ?

	// CCL
	CCL_OBJECT_BEGIN(Checksum)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Fire)
	DECLARE_RCP(Length)
	DECLARE_RCP(Source)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Fire)
		DEFINE_RCP(Length)
		DEFINE_RCP(Source)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Counter.  Node for a Counter.
//

class Counter :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Counter ( void );										// Constructor

	// Run-time data
	adtInt			vCnt;									// Current count
	adtInt			vReset;								// Reset value
	adtInt			iDelta;								// Delta

	// CCL
	CCL_OBJECT_BEGIN(Counter)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Decrement)
	DECLARE_RCP(Increment)
	DECLARE_RCP(Nocrement)
	DECLARE_RCP(Delta)
	DECLARE_RCP(Reset)
	DECLARE_CON(Fire)
	DECLARE_RCP(Set)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Decrement)
		DEFINE_RCP(Increment)
		DEFINE_RCP(Nocrement)
		DEFINE_RCP(Delta)
		DEFINE_RCP(Reset)
		DEFINE_CON(Fire)
		DEFINE_RCP(Set)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Formula.  Node for a forumla.
//

class Formula :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Formula ( void );										// Constructor

	// Run-time data
	adtString	strForm;									// Forumla string
	IList			*pLstPost;								// Postfix list
	IDictionary	*pVals;									// Value dictionary
	IDictionary	*pRcps;									// Receptors
	IList			*pStkEval;								// Eval stack
	IIt			*pItStkEval;							// Eval stack iterator

	// CCL
	CCL_OBJECT_BEGIN(Formula)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Eval)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Eval)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :
	HRESULT eval		( IList *, IDictionary *, ADTVALUE & );
	HRESULT post		( IList *, IList ** );
	HRESULT symbols	( const WCHAR *, IList ** );

	};
/*
//
// Class - Function.  Node for a function.
//

class Function :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Function ( void );									// Constructor

	// Run-time data
	IReceptor	*prN,*prClr,*prPr,*prF;				// Receptors
	IEmitter		*peF,*peErr;							// Emitters
	adtString	strName;									// Function name
	IList			*pLstP;									// Parameter list

	// CCL
	CCL_OBJECT_BEGIN(Function)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Fire)
	DECLARE_RCP(Function)
	DECLARE_RCP(Clear)
	DECLARE_RCP(Param)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Fire)

		DEFINE_RCP(Function)
		DEFINE_RCP(Clear)
		DEFINE_RCP(Param)

		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	};
*/

//
// Class - Line.  Node to handle a line.
//

class Line :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Line ( void );											// Constructor

	// Run-time data
	adtDouble	dY2,dY1,dX1,dX2;						// Two-points for a line
	adtDouble	dM,dB;									// Slope/intercept
	bool			bVert;									// Line is vertical

	// CCL
	CCL_OBJECT_BEGIN(Line)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections

	// Evaluation
	DECLARE_CON(X)
	DECLARE_CON(Y)
	DECLARE_RCP(Two)

	// Slope/intercept
	DECLARE_CON(M)
	DECLARE_CON(B)

	// Two-point
	DECLARE_RCP(X1)
	DECLARE_RCP(X2)
	DECLARE_RCP(Y1)
	DECLARE_RCP(Y2)

	BEGIN_BEHAVIOUR()
		DEFINE_CON(X)
		DEFINE_CON(Y)
		DEFINE_CON(M)
		DEFINE_CON(B)
		DEFINE_RCP(Two)
		DEFINE_RCP(X1)
		DEFINE_RCP(X2)
		DEFINE_RCP(Y1)
		DEFINE_RCP(Y2)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Matrix3D.  Node for a 3D matrix.
//

class Matrix3D :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Matrix3D ( void );									// Constructor

	// Run-time data
	double			dScl[3],dRot[3],dTrns[3];		// Run-time data
	IDictionary		*pA,*pB,*pC;						// Matrix dictionaries
	double			dA[16],dB[16],dC[16];			// Matrix buffers
	U32				nA,nB;								// Count of elements
	adtValue			vK,vL;								// Key value
	adtIUnknown		unkV;									// Internal value
	adtInt			iV;									// Internal value
	adtDouble		vD;									// Internal value

	// CCL
	CCL_OBJECT_BEGIN(Matrix3D)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(A)
	DECLARE_CON(B)
	DECLARE_CON(C)
	DECLARE_EMT(Error)
	DECLARE_EMT(Fire)
	DECLARE_RCP(Identity)
	DECLARE_RCP(Invert)
	DECLARE_RCP(Multiply)
	DECLARE_RCP(Apply)
	DECLARE_RCP(ScaleX)
	DECLARE_RCP(ScaleY)
	DECLARE_RCP(ScaleZ)
	DECLARE_RCP(RotateX)
	DECLARE_RCP(RotateY)
	DECLARE_RCP(RotateZ)
	DECLARE_RCP(TranslateX)
	DECLARE_RCP(TranslateY)
	DECLARE_RCP(TranslateZ)
	BEGIN_BEHAVIOUR()
		// Operands/result
		DEFINE_RCP(A)
		DEFINE_RCP(B)
		DEFINE_RCP(C)

		DEFINE_EMT(Error)
		DEFINE_EMT(Fire)
		DEFINE_RCP(Identity)
		DEFINE_RCP(Invert)
		DEFINE_RCP(Multiply)

		// Scale/Rotate/Translate
		DEFINE_RCP(Apply)

		DEFINE_RCP(ScaleX)
		DEFINE_RCP(ScaleY)
		DEFINE_RCP(ScaleZ)
		DEFINE_RCP(RotateX)
		DEFINE_RCP(RotateY)
		DEFINE_RCP(RotateZ)
		DEFINE_RCP(TranslateX)
		DEFINE_RCP(TranslateY)
		DEFINE_RCP(TranslateZ)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities

	};

//
// Class - Rectangle.  Node to handle rectangle calculations.
//

class Rectangle :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Rectangle ( void );									// Constructor

	// Run-time data
	double		dX[2],dY[2];							// Active rectangle
	adtDouble	dW,dH;									// Width/height
	adtDouble	dX0,dY0,dX1,dY1;						// Corners
	adtDouble	dXc,dYc;									// Center points
	adtDouble	dXpt,dYpt;								// Current point

	// CCL
	CCL_OBJECT_BEGIN(Rectangle)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections

	// Specifiers

	// Corners
	DECLARE_CON(X0)
	DECLARE_CON(Y0)
	DECLARE_CON(X1)
	DECLARE_CON(Y1)

	// Current point
	DECLARE_CON(X)
	DECLARE_CON(Y)

	// Center of rectangle
	DECLARE_CON(CenterX)
	DECLARE_CON(CenterY)

	// Size
	DECLARE_CON(Width)
	DECLARE_CON(Height)

	// Point in rectangle ?
	DECLARE_CON(PointIn)
	DECLARE_EMT(In)
	DECLARE_EMT(NotIn)

	BEGIN_BEHAVIOUR()
		DEFINE_CON(X0)
		DEFINE_CON(Y0)
		DEFINE_CON(X1)
		DEFINE_CON(Y1)
		DEFINE_CON(CenterX)
		DEFINE_CON(CenterY)
		DEFINE_CON(Width)
		DEFINE_CON(Height)
		DEFINE_RCP(X)
		DEFINE_RCP(Y)
		DEFINE_RCP(PointIn)
		DEFINE_EMT(In)
		DEFINE_EMT(NotIn)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	void infoEmit		( void );
	void infoUpdate	( void );
	};

//
// Class - Transform3D.  Node for computing 3D transformation matrix.
//

class Transform3D :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Transform3D ( void );								// Constructor

	// Run-time data
	double			dIn[16],dOut[16],dOutP[16];	// Matrix values
	double			dSrt[9];								// Transform values
	adtDouble		vD;									// Internal value

	// CCL
	CCL_OBJECT_BEGIN(Transform3D)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(11)
	DECLARE_CON(12)
	DECLARE_CON(13)
	DECLARE_CON(21)
	DECLARE_CON(22)
	DECLARE_CON(23)
	DECLARE_CON(31)
	DECLARE_CON(32)
	DECLARE_CON(33)
	DECLARE_CON(41)
	DECLARE_CON(42)
	DECLARE_CON(43)

	DECLARE_RCP(ScaleX)
	DECLARE_RCP(ScaleY)
	DECLARE_RCP(ScaleZ)
	DECLARE_RCP(RotateX)
	DECLARE_RCP(RotateY)
	DECLARE_RCP(RotateZ)
	DECLARE_RCP(TranslateX)
	DECLARE_RCP(TranslateY)
	DECLARE_RCP(TranslateZ)
	BEGIN_BEHAVIOUR()
		// Matrix input/output
		DEFINE_CON(11)
		DEFINE_CON(12)
		DEFINE_CON(13)
		DEFINE_CON(21)
		DEFINE_CON(22)
		DEFINE_CON(23)
		DEFINE_CON(31)
		DEFINE_CON(32)
		DEFINE_CON(33)
		DEFINE_CON(41)
		DEFINE_CON(42)
		DEFINE_CON(43)

		// Scale/Rotate/Translate
		DEFINE_RCP(ScaleX)
		DEFINE_RCP(ScaleY)
		DEFINE_RCP(ScaleZ)
		DEFINE_RCP(RotateX)
		DEFINE_RCP(RotateY)
		DEFINE_RCP(RotateZ)
		DEFINE_RCP(TranslateX)
		DEFINE_RCP(TranslateY)
		DEFINE_RCP(TranslateZ)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	HRESULT	emit		( void );
	HRESULT	input		( double *, int, double );
	HRESULT	update	( void );
	};

//
// Class - Unary.  Node to perform a unary operation.
//

class Unary :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Unary ( void );										// Constructor

	// Run-time data
	adtValue		vV;										// Parameter
	adtValue		vRes;										// Result
	int			iOp;										// Math operation

	// CCL
	CCL_OBJECT_BEGIN(Unary)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	DECLARE_RCP(Value)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
		DEFINE_RCP(Value)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Vector3.  Node to perform operations on 3-vectors.
//

class Vector3 :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Vector3 ( void );										// Constructor

	// Run-time data
	double			dX[2],dY[2],dZ[2];				// Parameters

	// CCL
	CCL_OBJECT_BEGIN(Vector3)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(X0)
	DECLARE_RCP(Y0)
	DECLARE_RCP(Z0)
	DECLARE_RCP(X1)
	DECLARE_RCP(Y1)
	DECLARE_RCP(Z1)
	DECLARE_RCP(Angle)
	DECLARE_RCP(Cross)
	DECLARE_RCP(Dot)
	DECLARE_RCP(Length)
	DECLARE_RCP(Normalize)
	DECLARE_EMT(X)
	DECLARE_EMT(Y)
	DECLARE_EMT(Z)
	DECLARE_EMT(Fire)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		// Input
		DEFINE_RCP(X0)
		DEFINE_RCP(Y0)
		DEFINE_RCP(Z0)
		DEFINE_RCP(X1)
		DEFINE_RCP(Y1)
		DEFINE_RCP(Z1)

		// Operations
		DEFINE_RCP(Angle)
		DEFINE_RCP(Cross)
		DEFINE_RCP(Dot)
		DEFINE_RCP(Length)
		DEFINE_RCP(Normalize)

		// Output
		DEFINE_EMT(X)
		DEFINE_EMT(Y)
		DEFINE_EMT(Z)
		DEFINE_EMT(Fire)
		DEFINE_EMT(Error)

	END_BEHAVIOUR_NOTIFY()
	};

// Prototypes
HRESULT mathBinary	( int, const ADTVALUE &, const ADTVALUE &, ADTVALUE & );
HRESULT mathInv		( double *, double * );
HRESULT mathUnary		( int, const ADTVALUE &, ADTVALUE & );
HRESULT mathOp			( const WCHAR *, int * );
HRESULT mathSRT		( double [16], double [3], double [3], double [3], double [16] );

// Called internally
HRESULT	mathInit		( void );
void		mathUninit	( void );

#endif
