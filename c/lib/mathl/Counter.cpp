////////////////////////////////////////////////////////////////////////
//
//									COUNTER.CPP
//
//					Implementation of the counter node
//
////////////////////////////////////////////////////////////////////////

#include "mathl_.h"
#include <stdio.h>

Counter :: Counter ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	vReset	= 0;
	iDelta	= 1;
	}	// Counter

HRESULT Counter :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue vL;

	// State check
	if (!bAttach) return S_OK;

	// Default states
	pnDesc->load ( adtString(L"Reset"),	vReset );
	if (pnDesc->load ( adtString(L"Delta"), vL ) == S_OK)
		iDelta = vL;

	return S_OK;
	}	// onAttach

HRESULT Counter :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Increment
	if (_RCP(Increment) || _RCP(Decrement) || _RCP(Nocrement))
		{
		// Update value
		if (_RCP(Increment))
			vCnt	= vCnt+iDelta;
		else if (_RCP(Decrement))
			vCnt	= vCnt-iDelta;

		// Emit
		_EMT(Fire,vCnt);
		}	// if

	// Reset
	else if (_RCP(Reset))
		{
		// Update value
		vCnt = (U32) vReset;

		// Emit
		_EMT(Fire,vCnt);
		}	// else if

	// Reset value
	else if (_RCP(Fire))
		hr = adtValue::copy ( adtInt(v), vReset );

	// Set value
	else if (_RCP(Set))
		{
		// Set to specified value and emit
		vCnt = adtInt(v);
//		_EMT(Fire,vCnt);
		}	// else if

	// State
	else if (_RCP(Delta))
		iDelta = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

