////////////////////////////////////////////////////////////////////////
//
//										MATHL.H
//
//									Math library
//
////////////////////////////////////////////////////////////////////////

#ifndef	MATHL_H
#define	MATHL_H

// System includes
#include "../../lib/nspcl/nspcl.h"

// Prototypes
HRESULT	mathMult		( double *, U32, double *, U32, double *, U32 * );
float		mathRand		( void );

#endif
