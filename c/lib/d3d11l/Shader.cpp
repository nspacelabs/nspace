////////////////////////////////////////////////////////////////////////
//
//									Shader.CPP
//
//				Implementation of the Direct3D Shader node.
//
////////////////////////////////////////////////////////////////////////

#include "d3d11l_.h"
#include <stdio.h>
#include <d3dcompiler.h>

// Globals

Shader :: Shader ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctShd	= NULL;
	pDctDev	= NULL;
	pStmShd	= NULL;
	strType	= L"Vertex";
	}	// Shader

HRESULT Shader :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			hr = adtValue::toString(vL,strType);
		if (pnDesc->load ( adtString(L"Entry"), vL ) == S_OK)
			hr = adtValue::toString(vL,strEntry);
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pStmShd);
		_RELEASE(pDctDev);
		_RELEASE(pDctShd);
		}	// else

	return hr;
	}	// onAttach

HRESULT Shader :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		ID3D11Device			*pDev		= NULL;
		ID3D11InputLayout		*pIL		= NULL;
		ID3DBlob					*pBlb		= NULL;
		ID3D11VertexShader	*pVs		= NULL;
		U8							*pcShd	= NULL;
		char						*pcEntry	= NULL;
		adtValue					vL;
		adtIUnknown				unkV;
		U64						sz;

		// State check
		CCLTRYE ( pDctShd != NULL && pDctDev != NULL, ERROR_INVALID_STATE );

		// Shader information
		CCLTRYE ( pStmShd != NULL && strType.length() > 0 && strEntry.length() > 0, ERROR_INVALID_STATE );

		// Device (required)
		CCLTRY ( pDctDev->load ( adtString(L"Device"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Device,&pDev) );

		// Read in shader data
		CCLTRY	( pStmShd->available(&sz) );
		CCLTRYE	( (pcShd = (U8 *) _ALLOCMEM((U32)sz)) != NULL, E_OUTOFMEMORY );
		CCLTRY	( pStmShd->read ( pcShd, sz, NULL ) );

		// Compile the shader
		CCLTRY ( strEntry.toAscii ( &pcEntry ) );
		CCLTRY ( D3DCompile ( pcShd, sz, "Shader::onReceive", NULL, NULL, pcEntry, 
										"vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pBlb, NULL ) );

		// Create a shader of the specified type
		if (hr == S_OK)
			{
			// Vertex
			if (!WCASECMP(strType,L"Vertex"))
				hr = pDev->CreateVertexShader ( pBlb->GetBufferPointer(), pBlb->GetBufferSize(),
															nullptr, &pVs );
			else
				hr = E_NOTIMPL;
			}	// if

		//
		// Input layout for shader
		//
		if (hr == S_OK && pDctShd->load ( adtString(L"InputLayoutDesc"), vL ) == S_OK)
			{
			IContainer						*pCntL		= NULL;
			IIt								*pItL			= NULL;
			D3D11_INPUT_ELEMENT_DESC	*pd			= NULL;
			char								**pcName		= NULL;
			U32								idx,sz		= 0;

			// Extract input layout specification (list of dictionaries)
			CCLTRY ( _QISAFE((unkV=vL),IID_IContainer,&pCntL));
			CCLTRY ( pCntL->iterate(&pItL) );

			// Allocate memory for descriptors
			CCLTRY ( pCntL->size(&sz) );
			CCLTRYE( (pd = (D3D11_INPUT_ELEMENT_DESC *) _ALLOCMEM(sz*sizeof(D3D11_INPUT_ELEMENT_DESC))) 
						!= NULL, E_OUTOFMEMORY );
			CCLTRYE( (pcName = (char **) _ALLOCMEM(sz*sizeof(char *))) != NULL, E_OUTOFMEMORY );

			// Fill descriptors
			CCLOK ( idx = 0; )
			while (hr == S_OK && pItL->read ( vL ) == S_OK)
				{
				IDictionary		*pDsc = NULL;
				adtString		strName;

				// Descriptor information
				if (	hr == S_OK && 
						(IUnknown *)NULL != (unkV=vL) &&
						_QI(unkV,IID_IDictionary,&pDsc) == S_OK)
					{
					// Defaults
					memset ( &pd[idx], 0, sizeof(D3D11_INPUT_ELEMENT_DESC) );
					pd[idx].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

					// Semantic name
					if (	pDsc->load ( adtString(L"Name"), vL ) == S_OK &&
							(strName = vL).length() > 0 &&
							strName.toAscii ( &(pcName[idx]) ) == S_OK)
						pd[idx].SemanticName = pcName[idx];

					// Format of input
					if (	pDsc->load ( adtString(L"Format"), vL ) == S_OK &&
							(strName = vL).length() > 0 )
						pd[idx].Format =	(!WCASECMP(strName,L"R32G32f"))			?	DXGI_FORMAT_R32G32_FLOAT :
												(!WCASECMP(strName,L"R32G32B32A32f"))	?	DXGI_FORMAT_R32G32B32A32_FLOAT :
																										DXGI_FORMAT_R32G32B32A32_FLOAT;

					// Next descriptor
					++idx;
					}	// if

				// Clean up
				_RELEASE(pDsc);
				pItL->next();
				}	// while

			// Create the layout
			if (hr == S_OK && idx > 0)
				hr = pDev->CreateInputLayout ( pd, idx, pBlb->GetBufferPointer(), pBlb->GetBufferSize(), &pIL );

			// Clean up
			for (idx = 0;idx < sz;++idx)
				_FREEMEM(pcName[idx]);
			_FREEMEM(pcName);
			_FREEMEM(pd);
			_RELEASE(pItL);
			_RELEASE(pCntL);
			}	// if

		// Result
		CCLTRY ( pDctShd->store ( adtString(L"Shader"), adtIUnknown(pVs) ) );
		CCLTRY ( pDctShd->store ( adtString(L"ShaderBlob"), adtIUnknown(pBlb) ) );
		CCLTRY ( pDctShd->store ( adtString(L"InputLayout"), adtIUnknown(pIL) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDctShd));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pIL);
		_RELEASE(pVs);
		_RELEASE(pBlb);
		_FREEMEM(pcEntry);
		_RELEASE(pDev);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		// State check
		CCLTRYE ( pDctShd != NULL, ERROR_INVALID_STATE );

		// Remove/release resources
		CCLOK ( pDctShd->remove ( adtString(L"Shader") ); )
		CCLOK ( pDctShd->remove ( adtString(L"ShaderBlob") ); )
		CCLOK ( pDctShd->remove ( adtString(L"InputLayout") ); )
		}	// if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctShd);
		_QISAFE(unkV,IID_IDictionary,&pDctShd);
		}	// else if
	else if (_RCP(Device))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctDev);
		_QISAFE(unkV,IID_IDictionary,&pDctDev);
		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStmShd);
		_QISAFE(unkV,IID_IByteStream,&pStmShd);
		}	// else if
	else if (_RCP(Type))
		hr = adtValue::toString(v,strType);
	else if (_RCP(Entry))
		hr = adtValue::toString(v,strEntry);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
