////////////////////////////////////////////////////////////////////////
//
//									Device.CPP
//
//				Implementation of the Direct3D Device node.
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#include "d3d11l_.h"
#include <stdio.h>
#include <directxcolors.h>

// Globals

Device :: Device ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	pAd	= NULL;
	bDbg	= false;
	}	// Device

HRESULT Device :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Debug"), vL ) == S_OK)
			bDbg = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDct);
		_RELEASE(pAd);
		}	// else

	return hr;
	}	// onAttach

HRESULT Device :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		ID3D11Device			*pDev = NULL;
		ID3D11DeviceContext	*pCtx	= NULL;
		D3D_FEATURE_LEVEL		fl		= D3D_FEATURE_LEVEL_11_0;
		UINT						flags	= (bDbg) ? D3D11_CREATE_DEVICE_DEBUG : 0;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Create device
		// Odd, but if the adapter is specified, driver type must be unknown
		CCLTRY ( D3D11CreateDevice ( pAd, 
						(pAd != NULL) ? D3D_DRIVER_TYPE_UNKNOWN : D3D_DRIVER_TYPE_HARDWARE, 
						NULL, flags, NULL, 0, D3D11_SDK_VERSION, &pDev, &fl, &pCtx ) );

		// Place results in dictionary
		if (hr == S_OK)
			{
			// Update
			CCLTRY ( pDct->store ( adtString(L"Device"), adtIUnknown (pDev) ) );
			CCLTRY ( pDct->store ( adtString(L"DeviceContext"), adtIUnknown (pCtx) ) );
			CCLTRY ( pDct->store ( adtString(L"FeatureLevel"), adtInt(fl) ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pCtx);
		_RELEASE(pDev);

		}	// if

	// Close
	else if (_RCP(Close))
		{
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Release interfaces by removing them from the dictionary
		CCLOK ( pDct->remove ( adtString(L"RenderTargetView") ); )
		if (hr == S_OK && pDct->load ( adtString(L"DeviceContext"), vL ) == S_OK)
			{
			ID3D11DeviceContext	*pCtx	= NULL;
			adtIUnknown				unkV(vL);

			// Clean up context
			if (	(IUnknown *)NULL != unkV &&
					_QI(unkV,IID_ID3D11DeviceContext,&pCtx) == S_OK)
				pCtx->ClearState();
			pDct->remove ( adtString(L"DeviceContext") );
			_RELEASE(pCtx);
			}	// if
		CCLOK ( pDct->remove ( adtString(L"Device") ); )
		}	// if

	// Prepare newly created swap chain
	else if (_RCP(SwapChain))
		{
		IDictionary					*pDctSwp	= NULL;
		ID3D11Device				*pDev		= NULL;
		ID3D11DeviceContext		*pCtx		= NULL;
		IDXGISwapChain				*pSwp		= NULL;
		ID3D11Texture2D			*pBack	= NULL;
		ID3D11RenderTargetView	*pRtView	= NULL;
		adtIUnknown					unkV(v);
		adtValue						vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Access swap chain
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctSwp) );
		CCLTRY ( pDctSwp->load ( adtString(L"SwapChain"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IDXGISwapChain,&pSwp) );

		// Access device in active dictionary
		CCLTRY ( pDct->load ( adtString(L"Device"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Device,&pDev) );

		// Access context in active dictionary
		CCLTRY ( pDct->load ( adtString(L"DeviceContext"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11DeviceContext,&pCtx) );

		//
		// Perform default preparation
		//

		// Obtain back buffer associated with swap chain
		CCLTRY ( pSwp->GetBuffer ( 0, __uuidof(ID3D11Texture2D), (void **) &pBack ) );

		// Create render-target view for accessing resource data
		CCLTRY ( pDev->CreateRenderTargetView ( pBack, nullptr, &pRtView ) );
		CCLTRY ( pDct->store ( adtString(L"RenderTargetView"), adtIUnknown(pRtView) ) );

		// Assign to output-merger(OM) stage
		CCLOK ( pCtx->OMSetRenderTargets ( 1, &pRtView, nullptr ); )

		// Assign a default viewport
		if (hr == S_OK)
			{
			D3D11_VIEWPORT			vp;
			D3D11_TEXTURE2D_DESC	descTex;

			// Get size of buffer
			pBack->GetDesc ( &descTex );

			// Setup
			memset ( &vp, 0, sizeof(vp) );
			vp.MaxDepth = 1.0;
			vp.Width		= (float)descTex.Width;
			vp.Height	= (float)descTex.Height;

			// Assign
			pCtx->RSSetViewports ( 1, &vp );
			}	// if

		// Clean up
		_RELEASE(pRtView);
		_RELEASE(pBack);
		_RELEASE(pCtx);
		_RELEASE(pDev);
		_RELEASE(pSwp);
		_RELEASE(pDctSwp);
		}	// else if

	// Clear render target to color
	else if (_RCP(Clear))
		{
		ID3D11DeviceContext		*pCtx		= NULL;
		ID3D11RenderTargetView	*pRtView	= NULL;
		adtValue						vL;
		adtIUnknown					unkV;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Device context TODO: Cache
		CCLTRY ( pDct->load ( adtString(L"DeviceContext"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL), IID_ID3D11DeviceContext, &pCtx) );

		// Render target
		CCLTRY ( pDct->load ( adtString(L"RenderTargetView"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL), IID_ID3D11RenderTargetView, &pRtView) );

		// Set all elements to same color
		CCLOK ( pCtx->ClearRenderTargetView ( pRtView, DirectX::Colors::MidnightBlue ); )

		// Clean up
		_RELEASE(pRtView);
		_RELEASE(pCtx);
		}	// else if

	// State
	else if (_RCP(Adapter))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pAd);
		_QISAFE(unkV,IID_IDXGIAdapter,&pAd);
		}	// else if
	else if (_RCP(Dictionary))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
