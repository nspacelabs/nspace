////////////////////////////////////////////////////////////////////////
//
//										D3D11L_.H
//
//				Implementation include file for Direct3D 11 library
//
////////////////////////////////////////////////////////////////////////

#ifndef	D3D11L__H
#define	D3D11L__H

// Includes
#include "d3d11l.h"
#include "../../lib/nspcl/nspcl.h"

// External API
#include <d3d11.h>
#include <dxgi.h>

///////////
// Objects
///////////


/////////
// Nodes
/////////

//
// Class - Buffer.  Direct3D Buffer.
//

class Buffer :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Buffer ( void );										// Constructor

	// Run-time data
	IDictionary		*pDctBfr;							// Buffer dictionary
	IDictionary		*pDctDev;							// Device dictionary
	IContainer		*pContV;								// Container for values
	adtString		strType;								// Buffer type
	adtInt			iPitch;								// Value bitch

	// CCL
	CCL_OBJECT_BEGIN(Buffer)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Close)
	DECLARE_RCP(Device)
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Open)
	DECLARE_RCP(Value)
	DECLARE_RCP(Type)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Close)
		DEFINE_RCP(Device)
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Open)
		DEFINE_RCP(Value)
		DEFINE_RCP(Type)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Device.  Direct3D device.
//

class Device :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Device ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Run-time dictionary
	IDXGIAdapter	*pAd;									// Active adapter
	adtBool			bDbg;									// Flags

	// CCL
	CCL_OBJECT_BEGIN(Device)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Adapter)
	DECLARE_RCP(Clear)
	DECLARE_RCP(Close)
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Open)
	DECLARE_EMT(Error)
	DECLARE_RCP(SwapChain)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Adapter)
		DEFINE_RCP(Clear)
		DEFINE_RCP(Close)
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Open)
		DEFINE_EMT(Error)
		DEFINE_RCP(SwapChain)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Shader.  Direct3D shader.
//

class Shader :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Shader ( void );										// Constructor

	// Run-time data
	IDictionary		*pDctShd;							// Shader dictionary
	IDictionary		*pDctDev;							// Device dictionary
	IByteStream		*pStmShd;							// Stream to shader data
	adtString		strType;								// Shader type
	adtString		strEntry;							// Shader entry point

	// CCL
	CCL_OBJECT_BEGIN(Shader)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Close)
	DECLARE_RCP(Device)
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Entry)
	DECLARE_CON(Open)
	DECLARE_RCP(Stream)
	DECLARE_RCP(Type)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Close)
		DEFINE_RCP(Device)
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Entry)
		DEFINE_CON(Open)
		DEFINE_RCP(Stream)
		DEFINE_RCP(Type)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Texture.  Direct3D texture.
//

class Texture :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Texture ( void );										// Constructor

	// Run-time data
	IDictionary		*pDctTex;							// Run-time dictionary
	IDictionary		*pDctDev;							// Device dictionary
	IDictionary		*pDctImg;							// Image dictionary
	adtBool			bShare;								// Shared resource ?
	adtInt			iW,iH;								// Size
	adtString		strFmt;								// Desired format

	// CCL
	CCL_OBJECT_BEGIN(Texture)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)
	DECLARE_RCP(Close)
	DECLARE_CON(Copy)
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Image)
	DECLARE_RCP(Width)
	DECLARE_RCP(Height)
	DECLARE_RCP(Format)
	DECLARE_CON(Open)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)
		DEFINE_RCP(Close)
		DEFINE_CON(Copy)
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Image)
		DEFINE_RCP(Width)
		DEFINE_RCP(Height)
		DEFINE_RCP(Format)
		DEFINE_CON(Open)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

#endif
