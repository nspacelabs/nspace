////////////////////////////////////////////////////////////////////////
//
//									Texture.CPP
//
//				Implementation of the Direct3D Texture node.
//
////////////////////////////////////////////////////////////////////////

#include "d3d11l_.h"
#include <stdio.h>

// Utilties
#include "../imagel/imagel.h"

// Globals

Texture :: Texture ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctTex	= NULL;
	pDctDev	= NULL;
	pDctImg	= NULL;
	bShare	= false;
	}	// Texture

HRESULT Texture :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Share"), vL ) == S_OK)
			bShare = vL;
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			onReceive(prWidth,vL);
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			onReceive(prHeight,vL);
		if (pnDesc->load ( adtString(L"Format"), vL ) == S_OK)
			onReceive(prFormat,vL);
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDctImg);
		_RELEASE(pDctDev);
		_RELEASE(pDctTex);
		}	// else

	return hr;
	}	// onAttach

HRESULT Texture :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		ID3D11Device					*pDev		= NULL;
		ID3D11Texture2D				*pTex		= NULL;
		ID3D11ShaderResourceView	*psrView	= NULL;
		DXGI_FORMAT						fmt		= DXGI_FORMAT_UNKNOWN;
		adtValue							vL;
		adtIUnknown						unkV;
		ImageDct							dctImg;

		// State check
		CCLTRYE ( pDctTex != NULL && pDctDev != NULL, ERROR_INVALID_STATE );

		// Device (required)
		CCLTRY ( pDctDev->load ( adtString(L"Device"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Device,&pDev) );

		// Image information
//		CCLTRY ( dctImg.lock ( pDctImg ) );

		// Determine texture format, add more as necessary
//		if (hr == S_OK)
//			fmt = (dctImg.iFmt == IMGFMT_RGBAf)	?	DXGI_FORMAT_R32G32B32A32_FLOAT : 
//																DXGI_FORMAT_B8G8R8A8_UNORM;

		// Image information
		CCLTRYE ( iW > 0 && iH > 0 && strFmt.length() > 0, ERROR_INVALID_STATE );
		if (hr == S_OK)
			fmt = (!WCASECMP(strFmt,L"RGBAf"))		?	DXGI_FORMAT_R32G32B32A32_FLOAT : 
					(!WCASECMP(strFmt,L"B8G8R8A8"))	?	DXGI_FORMAT_B8G8R8A8_UNORM :
					(!WCASECMP(strFmt,L"R8G8B8A8"))	?	DXGI_FORMAT_R8G8B8A8_UNORM :
																	DXGI_FORMAT_B8G8R8A8_UNORM;

		// Create the texture.  For now the image is provided in the desired format
		// for the texture.
		if (hr == S_OK)
			{
			D3D11_TEXTURE2D_DESC		desc;
//			D3D11_SUBRESOURCE_DATA	data;

			// Setup.  Convert defaults to properties as needed.
			memset ( &desc, 0, sizeof(desc) );
			desc.ArraySize				= 1;
			desc.BindFlags				= D3D11_BIND_SHADER_RESOURCE;		// Assume input to a shader
			desc.Format					= fmt;
			desc.Width					= iW;
			desc.Height					= iH;
			desc.MipLevels				= 1;
			desc.SampleDesc.Count	= 1;
			desc.SampleDesc.Quality	= 0;
			desc.Usage					= D3D11_USAGE_DEFAULT;
//			desc.Usage					= D3D11_USAGE_DYNAMIC;
//			desc.Usage					= D3D11_USAGE_STAGING;

			// NOTE: For some reason this combination of flags is required for sharing, others
			// cause 'CreateSharedHandle' to fail.
			if (bShare)
				desc.MiscFlags			= D3D11_RESOURCE_MISC_SHARED_NTHANDLE | D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX;

			// Prepare initial input
//			memset ( &data, 0, sizeof(data) );
//			data.SysMemPitch	= dctImg.iW*(dctImg.iBpp*dctImg.iCh)/8;
//			data.pSysMem		= dctImg.pvBits;

			// Create/initialize texture
//			hr = pDev->CreateTexture2D ( &desc, &data, &pTex );
			hr = pDev->CreateTexture2D ( &desc, NULL, &pTex );
			}	// if

		// Create a view into the texture resource that a shader can use
		if (hr == S_OK)
			{
			D3D11_SHADER_RESOURCE_VIEW_DESC	desc;

			// Setup
			memset ( &desc, 0, sizeof(desc) );
			desc.Format								= fmt;
			desc.ViewDimension					= D3D11_SRV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MipLevels			= 1;
			desc.Texture2D.MostDetailedMip	= 0;

			// Create/initialize view
			hr = pDev->CreateShaderResourceView ( pTex, &desc, &psrView );
			}	// if

		// Place results in dictionary
		if (hr == S_OK)
			{
			CCLTRY ( pDctTex->store ( adtString(L"Texture"), adtIUnknown (pTex) ) );
			CCLTRY ( pDctTex->store ( adtString(L"ShaderResourceView"), adtIUnknown (psrView) ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDctTex));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(psrView);
		_RELEASE(pTex);
		_RELEASE(pDev);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		adtValue		vL;

		// State check
		CCLTRYE ( pDctTex != NULL, ERROR_INVALID_STATE );

		// Release interfaces by removing them from the dictionary
		CCLOK ( pDctTex->remove ( adtString(L"ShaderResourceView") ); )
		CCLOK ( pDctTex->remove ( adtString(L"Texture") ); )
		}	// if

	// Copy textures
	else if (_RCP(Copy))
		{
		ID3D11Device			*pDev		= NULL;
		ID3D11DeviceContext	*pCtx		= NULL;
		ID3D11Resource			*pResSrc	= NULL;
		ID3D11Resource			*pResDst	= NULL;
		IDictionary				*pDctDst	= NULL;
		IDXGIKeyedMutex		*pMtxSrc	= NULL;
		IDXGIKeyedMutex		*pMtxDst	= NULL;
		adtIUnknown				unkV;
		adtValue					vL;

		// State check
		CCLTRYE ( pDctTex != NULL && pDctDev != NULL, ERROR_INVALID_STATE );

		// Device (required)
		CCLTRY ( pDctDev->load ( adtString(L"Device"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Device,&pDev) );

		// Texture (source)
		CCLTRY ( pDctTex->load ( adtString(L"Texture"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Resource,&pResSrc) );

		// Texture (destination passed in with signal)
		CCLTRY ( _QISAFE((unkV=v),IID_IDictionary,&pDctDst) );
		CCLTRY ( pDctDst->load ( adtString(L"Texture"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Resource,&pResDst) );

		// Possible texture synchronization
		CCLOK ( _QI(pResSrc,IID_IDXGIKeyedMutex,&pMtxSrc); )
		CCLOK ( _QI(pResDst,IID_IDXGIKeyedMutex,&pMtxDst); )

		// Obtain a device context and perform copy
		CCLOK ( pDev->GetImmediateContext ( &pCtx ); )
		if (hr == S_OK)
			{
			HRESULT hrs = (pMtxSrc != NULL) ? pMtxSrc->AcquireSync ( 0, 0 ) : S_OK;
			HRESULT hrd = (pMtxDst != NULL) ? pMtxDst->AcquireSync ( 0, 0 ) : S_OK;

			// Perform copy
			if (hrs == S_OK && hrd == S_OK)
				pCtx->CopyResource ( pResDst, pResSrc );

			// Result
			hr = (hrs == S_OK && hrd == S_OK) ? S_OK : E_FAIL;

			// Clean up
			if (pMtxSrc != NULL) pMtxSrc->ReleaseSync(0);
			if (pMtxDst != NULL) pMtxDst->ReleaseSync(0);
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Copy,v);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pCtx);
		_RELEASE(pMtxDst);
		_RELEASE(pMtxSrc);
		_RELEASE(pResDst);
		_RELEASE(pResSrc);
		_RELEASE(pDev);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctTex);
		_QISAFE(unkV,IID_IDictionary,&pDctTex);
		}	// else if
	else if (_RCP(Device))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctDev);
		_QISAFE(unkV,IID_IDictionary,&pDctDev);
		}	// else if
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctImg);
		_QISAFE(unkV,IID_IDictionary,&pDctImg);
		}	// else if
	else if (_RCP(Width))
		iW = v;
	else if (_RCP(Height))
		iH = v;
	else if (_RCP(Format))
		hr = adtValue::toString(v,strFmt);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
