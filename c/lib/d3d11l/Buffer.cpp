////////////////////////////////////////////////////////////////////////
//
//									Buffer.CPP
//
//				Implementation of the Direct3D buffer node.
//
////////////////////////////////////////////////////////////////////////

#include "d3d11l_.h"
#include <stdio.h>

// Globals

Buffer :: Buffer ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctBfr	= NULL;
	pDctDev	= NULL;
	pContV	= NULL;
	strType	= L"Vertex";
	}	// Buffer

HRESULT Buffer :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			onReceive(prType,vL);
		if (pnDesc->load ( adtString(L"Pitch"), vL ) == S_OK)
			iPitch = adtInt(vL);
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pContV);
		_RELEASE(pDctDev);
		_RELEASE(pDctBfr);
		}	// else

	return hr;
	}	// onAttach

HRESULT Buffer :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		ID3D11Device	*pDev		= NULL;
		ID3D11Buffer	*pBfr		= NULL;
		adtIUnknown		unkV;
		adtValue			vL;

		// State check
		CCLTRYE ( pDctBfr != NULL && pDctDev != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pContV != NULL && iPitch > 0, ERROR_INVALID_STATE );

		// Device (required)
		CCLTRY ( pDctDev->load ( adtString(L"Device"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ID3D11Device,&pDev) );

		// Supported types
		CCLTRYE( WCASECMP(strType,L"Vertex") == 0, E_NOTIMPL );

		// Create a buffer of the specified type
		if (hr == S_OK)
			{
			D3D11_BUFFER_DESC			desc	= { 0, D3D11_USAGE_DEFAULT, D3D11_BIND_VERTEX_BUFFER, 0, 0 };
			D3D11_SUBRESOURCE_DATA	data	= { NULL, iPitch, 0 };
			U32							sz		= 0;
			float							*pv	= NULL;
			IIt							*pIt	= NULL;

			// Number of values
			CCLTRY ( pContV->size(&sz) );

			// Allocate/read values into buffer
			CCLTRYE ( (pv = (float *) _ALLOCMEM (sz*sizeof(float))) != NULL, E_OUTOFMEMORY );
			CCLTRY ( pContV->iterate ( &pIt ) );
			CCLOK  ( sz = 0; )
			while (hr == S_OK && pIt->read ( vL ) == S_OK)
				{
				adtFloat	vFlt(vL);
				pv[sz++] = vFlt;
				pIt->next();
				}	// while

			// Update structures
			CCLOK ( desc.ByteWidth		= sz*sizeof(float); )
			CCLOK ( data.pSysMem			= pv; )
			CCLOK ( data.SysMemPitch	= iPitch; )

			// Vertex buffer
			if (!WCASECMP(strType,L"Vertex"))
				hr = pDev->CreateBuffer ( &desc, &data, &pBfr );
			else
				hr = E_NOTIMPL;

			// Clean up
			_RELEASE(pIt);
			_FREEMEM(pv);
			}	// if

		// Result
		CCLTRY ( pDctBfr->store ( adtString(L"Buffer"), adtIUnknown(pBfr) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDctBfr));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pBfr);
		_RELEASE(pDev);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		// State check
		CCLTRYE ( pDctBfr != NULL, ERROR_INVALID_STATE );

		// Remove/release resources
		CCLOK ( pDctBfr->remove ( adtString(L"Buffer") ); )
		}	// if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctBfr);
		_QISAFE(unkV,IID_IDictionary,&pDctBfr);
		}	// else if
	else if (_RCP(Device))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctDev);
		_QISAFE(unkV,IID_IDictionary,&pDctDev);
		}	// else if
	else if (_RCP(Value))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pContV);
		_QISAFE(unkV,IID_IContainer,&pContV);
		}	// else if
	else if (_RCP(Type))
		hr = adtValue::toString(v,strType);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
