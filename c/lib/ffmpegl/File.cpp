////////////////////////////////////////////////////////////////////////
//
//								File.CPP
//
//			Implementation of the FFMPEG file node
//
////////////////////////////////////////////////////////////////////////

#include "ffmpegl_.h"

File :: File ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	iIdx	= 0;
	iCnt	= 0;
	}	// File

HRESULT File :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Location"), vL ) == S_OK)
			onReceive(prLocation,vL);
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT File :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open file
	if (_RCP(Open))
		{
		AVFormatContext	*pavCtx	= NULL;
		char					*pcLoc	= NULL;
		int					ret;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// API needs ascii
		CCLTRY ( strLoc.toAscii ( &pcLoc ) );

		// Access file
		CCLTRYE ( (ret = avformat_open_input ( &pavCtx, pcLoc, NULL, NULL ))
						== 0, ret );

		// Retrieve the stream information
		CCLTRYE ( (ret = avformat_find_stream_info ( pavCtx, NULL ))
						>= 0, ret );

		// Fill in file information in context
		if (hr == S_OK)
			{
			CCLTRY ( pDct->store ( adtString(L"CountStream"), adtInt(pavCtx->nb_streams) ) );
			CCLTRY ( pDct->store ( adtString(L"Duraton"), adtFloat(pavCtx->duration/1000000.0f) ) );
			}	// if

		// Pre-guess the video frame rate for the outside world, too useful to wait
		// until media starts playing

		// Video stream present ?
		if ((ret = av_find_best_stream ( pavCtx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0 )) >= 0)
			{
			AVRational	
			fps = av_guess_frame_rate(pavCtx,pavCtx->streams[ret],NULL);

			// Update context
			if (fps.den != 0)
				hr = pDct->store ( adtString(L"Fps"), adtFloat(fps.num/((float)fps.den)));
			}	// for

		// Result
		CCLTRY ( pDct->store ( adtString(L"AVFormatContext"), adtLong((U64)pavCtx) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcLoc);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		AVFormatContext	*pavCtx	= NULL;
		adtValue				vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Closing...
		CCLOK ( _EMT(Close,adtIUnknown(pDct)); )

		// Clean up
		if (hr == S_OK && pDct->load ( adtString(L"AVFormatContext"), vL ) == S_OK)
			{
			AVFormatContext *ctx = (AVFormatContext *)(U64)adtLong(vL);
			avformat_close_input(&ctx);
			pDct->remove(adtString(L"AVFormatContext"));
			}	// if

		}	// else if

	// First/next
	else if (_RCP(First) || _RCP(Next))
		{
		AVFormatContext	*pavCtx	= NULL;
		IDictionary			*pDctStm	= NULL;
		adtValue				vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pDct->load ( adtString(L"AVFormatContext"), vL ) );
		CCLTRYE( (pavCtx = (AVFormatContext *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Reset enumeration.  Currently just enumerating streams.
		if (hr == S_OK && _RCP(First))
			{
			iIdx = 0;
			iCnt = pavCtx->nb_streams;
			}	// if

		// More streams ?
		CCLTRYE ( (iIdx < iCnt), ERROR_NOT_FOUND );

		// Stream info
		if (hr == S_OK)
			{
			AVStream	*pStm	= (pavCtx->streams[iIdx]);

			// Info
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctStm ) );
			CCLTRY ( pDctStm->store ( adtString("Index"), adtInt(pStm->index) ) );
			CCLTRY ( pDctStm->store ( adtString("Id"), adtInt(pStm->id) ) );
			CCLTRY ( pDctStm->store ( adtString("CountFrame"), adtLong(pStm->nb_frames) ) );

			// Codec information
			if (hr == S_OK && pStm->codecpar != NULL)
				{
				AVCodecParameters *p = pStm->codecpar;

				// Info
				CCLTRY ( pDctStm->store ( adtString(L"Type"), adtString ( 
							(p->codec_type == AVMEDIA_TYPE_VIDEO) ? L"Video" :
							(p->codec_type == AVMEDIA_TYPE_AUDIO) ? L"Audio" : L"Unknown" ) ) );
				CCLTRY ( pDctStm->store ( adtString(L"Codec"), adtString ( 
							// Add more as necessary
							(p->codec_id == AV_CODEC_ID_MPEG1VIDEO)	? L"mpeg1" :
							(p->codec_id == AV_CODEC_ID_MPEG2VIDEO)	? L"mpeg2" :
							(p->codec_id == AV_CODEC_ID_H264)			? L"h.264" :
							(p->codec_id == AV_CODEC_ID_H265)			? L"h.265" : L"Unknown" ) ) );
				CCLTRY ( pDctStm->store ( adtString(L"Format"), adtInt(p->format) ) );
				CCLTRY ( pDctStm->store ( adtString(L"BitRate"), adtLong(p->bit_rate) ) );
				if (hr == S_OK && p->codec_type == AVMEDIA_TYPE_VIDEO)
					{
					CCLTRY ( pDctStm->store ( adtString(L"Width"), adtInt(p->width) ) );
					CCLTRY ( pDctStm->store ( adtString(L"Height"), adtInt(p->height) ) );
					}	// if

				}	// if
			// Next enumeration
			++iIdx;
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Next,adtIUnknown(pDctStm));
		else 
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pDctStm);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Location))
		hr = adtValue::toString(v,strLoc);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

