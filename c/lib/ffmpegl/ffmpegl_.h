////////////////////////////////////////////////////////////////////////
//
//										FFMPEGL_.H
//
//		Implementation include file for the FFMPEG node library
//
////////////////////////////////////////////////////////////////////////

#ifndef	FFMPEGL__H
#define	FFMPEGL__H

#include "ffmpegl.h"

extern "C"
	{
	#include	<libavcodec/avcodec.h>
	#include <libswscale/swscale.h>
	#include <libavutil/imgutils.h>
	#include <libavutil/opt.h>
	#include <libavformat/avformat.h>
	#include <libavutil/timestamp.h>
	#include <libswresample/swresample.h>
	}

/////////
// Nodes
/////////

//
// Class - Decode.  Decoder node.
//

class Decode :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Decode ( void );										// Constructor

	// Run-time data
	IDictionary			*pDct;							// Current context
	IDictionary			*pDctImg;						// Image dictionary
	adtString			strDecode;						// Name of decoder
	SwsContext			*pswsCtx;						// Conversion context
	U32					uWidthL,uHeightL;				// Last width/height
	bool					bHdrL;							// Last HDR flag
	IMemoryMapped		*pBits[2];						// Memory buffers for RGB image
	U8						*pcVid[4];						// Video data
	int					iVidSz[4];						// Video line size

	// Decode state.  Single decode at a time (fix ?)
	AVFormatContext	*pCtxFmt;						// AV source
	AVFrame				*pFrm;							// Frame buffer
	AVPacket				pkt,pktRx;						// Packet data during enumeration
	int					iIdxV,iIdxA;					// A/V stream index
	bool					bEop;								// End of packets ?
	U32					uFrmCnt;							// Frame count

	// CCL
	CCL_OBJECT_BEGIN(Decode)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Devices
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Close)
	DECLARE_CON(Open)
	DECLARE_CON(Next)
	DECLARE_EMT(End)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_CON(Next)
		DEFINE_EMT(End)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities		
	void		decode_packet			( AVPacket *, int * );
	HRESULT	open_codec_context	( AVFormatContext *, enum AVMediaType, int * );

	};
/*
//
// Class - Encode.  Encode video into a provided byte stream or
//		to an output file.
//

class Encode :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Encode ( void );										// Constructor

	// Run-time data
	IDictionary	*pDct;									// Current context
	IByteStream	*pStm;									// Output stream
	adtString	strEncode;								// Name of encoder
	adtString	strLoc;									// Output file location (for containers)
	adtInt		iFps;										// Frames per second
	adtInt		iRate;									// Bit rate (quality)
	adtBool		bSilence;								// Add 'silence' audio track
	int64_t		next_ptsa,next_ptsv;					// Timestmap of the the next frame
	int64_t		samples_count;							// Audio sample count
//	U16			*pBfrA[2];								// Audio buffer
//	U16			szBfrA;									// Audio buffer sample size

	// CCL
	CCL_OBJECT_BEGIN(Encode)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Devices
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Encoder)
	DECLARE_RCP(Flush)
	DECLARE_RCP(Fps)
	DECLARE_RCP(Location)
	DECLARE_RCP(Rate)
	DECLARE_RCP(Stream)
	DECLARE_CON(Close)
	DECLARE_CON(Open)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Encoder)
		DEFINE_RCP(Flush)
		DEFINE_RCP(Fps)
		DEFINE_RCP(Location)
		DEFINE_RCP(Rate)
		DEFINE_RCP(Stream)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	
	private :

	// Internal utilities
	HRESULT encodeV				( CTXENC *, void *, U32, U32 );
	HRESULT encodeA				( CTXENC *, bool );
	HRESULT fillContext			( IDictionary *, CTXENC * );
	HRESULT updateContext		( CTXENC *, IDictionary * );
	HRESULT writePackets			( CTXENC *, bool );	

	};
*/

//
// Structure - CTXENC.  Encoder context.
//

typedef struct
	{
	AVFormatContext	*pCtxFmt;						// Container format context
	AVCodec				*pCodecV,*pCodecA;			// Codec descriptors
	AVCodecContext		*pCtxV,*pCtxA;					// Codec contexts
	AVStream				*pStmV,*pStmA;					// Container streams
	AVFrame				*pFrmV,*pFrmA,*pFrmAt;		// Frame buffers
	SwsContext			*pswsCtx;						// Scaling/conversion context (video)
	SwrContext			*pswrCtx;						// Re-sample context (audio)
	} CTXENC;

//
// Class - Encode.  Encode video into a provided byte stream or
//		to an output file.
//

class Encode :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Encode ( void );										// Constructor

	// Run-time data
	IDictionary	*pDct;									// Current context
	adtString	strLoc;									// Output file location (for containers)

	// This belongs in context
	int64_t		samples_count;							// Audio sample count

	// CCL
	CCL_OBJECT_BEGIN(Encode)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Devices
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Location)
	DECLARE_CON(Close)
	DECLARE_CON(Open)
	DECLARE_RCP(Flush)
	DECLARE_RCP(Image)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Location)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_RCP(Flush)
		DEFINE_RCP(Image)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	
	private :

	// Internal utilities
	HRESULT	encodeV			( CTXENC *, void *, U32, U32, U32 );
	HRESULT	encodeA			( CTXENC *, bool );
	HRESULT	fillContext		( CTXENC *, bool = true );
	HRESULT	writePackets	( CTXENC *, bool );	

	};

//
// Class - File.  Node to query information about a media file.
//

class File :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	File ( void );											// Constructor

	// Run-time data
	IDictionary	*pDct;									// Context
	adtString	strLoc;									// File location
	U32			iCnt,iIdx;								// Enumeration info

	// CCL
	CCL_OBJECT_BEGIN(File)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Devices
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Location)
	DECLARE_CON(Open)
	DECLARE_CON(Close)
	DECLARE_EMT(Error)
	DECLARE_RCP(First)
	DECLARE_CON(Next)
	DECLARE_EMT(End)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Location)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_EMT(Error)
		DEFINE_RCP(First)
		DEFINE_CON(Next)
		DEFINE_EMT(End)
	END_BEHAVIOUR_NOTIFY()
	
	private :

	};

#endif
