////////////////////////////////////////////////////////////////////////
//
//								Decode.CPP
//
//			Implementation of the FFMPEG decoder node
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#include "ffmpegl_.h"

// Globals
extern void avlog_cb ( void *p, int level, const char *szFmt, va_list varg );

Decode :: Decode ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	pDctImg	= NULL;
	pswsCtx	= NULL;
	uWidthL	= 0;
	uHeightL	= 0;
	memset ( pBits, 0, sizeof(pBits) );
	memset ( pcVid, 0, sizeof(pcVid) );
	memset ( iVidSz, 0, sizeof(iVidSz) );
	pCtxFmt	= NULL;
	iIdxV		= iIdxA = -1;
	uFrmCnt	= 0;
	bHdrL		= false;
	}	// Decode

void Decode :: decode_packet ( AVPacket *pkt, int *pgot )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decode the provided packet of data
	//
	//	PARAMETERS
	//		-	pkt contains the next block ofdata
	//		-	pgot will be set to true if a frame was retreived.
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr				= S_OK;
	IMemoryMapped	*pBitsUse	= (uFrmCnt & 0x1) ? pBits[0] : pBits[1];
	bool				bHdr			= false;
	int				ret;

	// Video ?
	if (pkt->stream_index == iIdxV)
		{
		// Continue decoding frames
		ret = avcodec_decode_video2 ( pCtxFmt->streams[iIdxV]->codec, pFrm, 
												pgot, pkt );
		if (ret < 0)
			{
			lprintf ( LOG_DBG, L"Error decoding video frame %d\r\n", ret );
			return;
			}	// if

		// Bytes used
		pkt->size -= ret;
		pkt->data += ret;

		// Frame ?
		if (!(*pgot))
			return;

		// Need HDR image output ?  Not sure about this detection so just use
		// what is known to work for now.
		bHdr = (pFrm->colorspace >= AVCOL_SPC_BT2020_NCL);

		// Output bytes per pixels
		int bytespp = (bHdr) ? 6 : 3;

		// Create a context for image conversion.  NOTE: This will need
		// to be its own node to make it more general, this is just for 
		// the current application.
		if (pswsCtx == NULL || pFrm->width != uWidthL || pFrm->height != uHeightL || bHdr != bHdrL)
			{
			// Create conversion context to RGB
			CCLTRYE ( (pswsCtx = sws_getContext (	
							pFrm->width, pFrm->height, (AVPixelFormat) pFrm->format,
							pFrm->width, pFrm->height, 
							(bHdr) ? AV_PIX_FMT_RGB48LE : AV_PIX_FMT_BGR24,
//							(bHdr) ? AV_PIX_FMT_BGR48LE : AV_PIX_FMT_BGR24,
							0, 0, 0, 0 ) ) != NULL, E_UNEXPECTED );

			// Context size
			CCLOK ( uWidthL	= pFrm->width; )
			CCLOK ( uHeightL	= pFrm->height; )
			CCLOK ( bHdrL = bHdr; )
			}	// if

		// Ensure size is sufficient and access bits
		if (hr == S_OK && pBitsUse->setSize ( pFrm->width*pFrm->height*bytespp ) == S_OK)
			{
			void		*pvBits		= NULL;
			int		stride[1]	= { pFrm->width*bytespp };
			uint8_t	*dstData[1];

			// Access bits
			CCLTRY ( pBitsUse->getInfo ( &pvBits, NULL ) );
			CCLOK  ( dstData[0] = (uint8_t *) pvBits; )

			// Scale/convert image
			CCLTRYE ( (ret = sws_scale ( pswsCtx, pFrm->data, pFrm->linesize, 
							0, pFrm->height, dstData, stride )) > 0, ret );

			//
			// Image dictionary
			//

			// Image size
			CCLTRY ( pDctImg->store ( adtString(L"Width"), adtInt(pFrm->width) ) );
			CCLTRY ( pDctImg->store ( adtString(L"Height"), adtInt(pFrm->height) ) );

			// Format based on HDR
			if (bHdr)
				{
				CCLTRY ( pDctImg->store ( adtString(L"Format"), adtString(L"B16G16R16") ) );
				CCLTRY ( pDctImg->store ( adtString(L"HDR"), adtBool(true) ) );
				}	// if
			else
				{
				CCLTRY( pDctImg->store ( adtString(L"Format"), adtString(L"B8G8R8") ) );
				CCLOK	( pDctImg->remove ( adtString(L"HDR") ); )
				}	// else

			// Bits
			CCLTRY ( pDctImg->store ( adtString(L"Bits"), adtIUnknown(pBitsUse) ) );

			// Frame count and timestamp (?)
			CCLTRY ( pDctImg->store ( adtString(L"Frame"), adtInt(++uFrmCnt) ) );
//			lprintf ( LOG_DBG, L"pkt_pos %ld\r\n", pFrm->pkt_pos );
			
			// Image output
//			lprintf ( LOG_DBG, L"avcodec_receive_frame ret %d frame %d time %d,%d format %d\r\n", 
//							ret, pCtx->frame_number, 
//							pCtx->time_base.num, pCtx->time_base.den, pCtx->pix_fmt );
			_EMT(Next,adtIUnknown(pDctImg));
			}	// if

		}	// if

	// Audio ?
	else if (pkt->stream_index == iIdxA)
		{
		// Not yet implemented.
		pkt->size = 0;
		}	// else if

	}	// decode_packet

HRESULT Decode :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"HDR"), vL ) == S_OK)
//			onReceive(prHdr,vL);

		// Create a memory block for RGB images
		CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pBits[0] ) );
		CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pBits[1] ) );

		// Image dictionary for results
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctImg ) );
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pBits[0]);
		_RELEASE(pBits[1]);
		_RELEASE(pDctImg);

		}	// else

	return hr;
	}	// onAttach

HRESULT Decode :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open a decoder context
	if (_RCP(Open))
		{
		adtValue vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Debug
//		av_log_set_callback ( avlog_cb );
//		av_log_set_level ( AV_LOG_DEBUG );

		// Decoding mode - Previously opened AV source.
		if (hr == S_OK && pDct->load ( adtString(L"AVFormatContext"), vL ) == S_OK)
			{
			// Context
			CCLTRYE( (pCtxFmt = (AVFormatContext *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

			// Prepare/demux the streams for decoding
			memset ( &pkt, 0, sizeof(pkt) );
			av_init_packet(&pktRx);
			pktRx.data	= NULL;
			pktRx.size	= 0;
			iIdxA			= iIdxV = -1;
			bEop			= false;

			// Video
			if (	hr == S_OK &&
					open_codec_context ( pCtxFmt, AVMEDIA_TYPE_VIDEO, &iIdxV ) >= 0)
				{
				// Allocate image where the decoded image will be put
				lprintf ( LOG_DBG, L"%dX%d@%d\r\n",
								pCtxFmt->streams[iIdxV]->codec->width,
								pCtxFmt->streams[iIdxV]->codec->height,
								pCtxFmt->streams[iIdxV]->codec->pix_fmt );
				}	// if

			// Audio
			if (	hr == S_OK &&
					open_codec_context ( pCtxFmt, AVMEDIA_TYPE_AUDIO, &iIdxA ) >= 0)
				{
				}	// if

			}	// if

		// TODO: Reincorporate the raw stream decoding commented out below
		else 
			hr = ERROR_INVALID_STATE;

		// Allocate a frame structure for internal use
		CCLTRYE ( (pFrm = av_frame_alloc()) != NULL, ERROR_OUTOFMEMORY );

		// Result
		uFrmCnt = 0;
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		}	// if

	// Close
	else if (_RCP(Close))
		{
		// Clean up
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Closing...
		CCLOK ( _EMT(Close,adtIUnknown(pDct)); )

		// Clean up resources
		if (hr == S_OK && pDct->load ( adtString(L"ContextVideo"), vL ) == S_OK)
			{
			AVCodecContext *pCtx = (AVCodecContext *)(U64)adtLong(vL);
			if (pCtx != NULL)
				avcodec_close(pCtx);
			pDct->remove ( adtString(L"ContextVideo") );
			pDct->remove ( adtString(L"IndexVideo") );
			}	// if
		if (hr == S_OK && pDct->load ( adtString(L"ContextAudio"), vL ) == S_OK)
			{
			AVCodecContext *pCtx = (AVCodecContext *)(U64)adtLong(vL);
			if (pCtx != NULL)
				avcodec_close(pCtx);
			pDct->remove ( adtString(L"ContextAudio") );
			pDct->remove ( adtString(L"IndexAudio") );
			}	// if
		if (hr == S_OK && pDct->load ( adtString(L"Frame"), vL ) == S_OK)
			{
			av_free((void *)(U64)adtLong(vL));
			pDct->remove ( adtString(L"Frame") );
			}	// if

		// Decoder shutdown
		pCtxFmt	= NULL;
		iIdxV		= iIdxA = -1;
		}	// else if

	// Next frame
	else if (_RCP(Next))
		{
		AVFrame	*pFrm		= NULL;
		int		idxV		= -1;
		int		idxA		= -1;
		int		iGot		= 0;
		int		ret;
		adtValue	vL;

		// State check
		CCLTRYE ( pCtxFmt != NULL, ERROR_INVALID_STATE );

		// Continue decoding
		while (hr == S_OK)
			{
			// Need a new packet ?
			if (pkt.size == 0 && !bEop)
				{
				// Done with previous packet
				av_packet_unref ( &pktRx );

				// New packet
				ret = av_read_frame ( pCtxFmt, &pktRx );

				// Out of data ?
				bEop = (pktRx.size <= 0);

				// Work with copy
				if (!bEop)
					pkt = pktRx;
				else
					memset ( &pkt, 0, sizeof(pkt) );
				}	// if

			// Decode the current packet
			iGot = 0;
			while (hr == S_OK && (pkt.size > 0 || bEop))
				{
				// Process packet
				decode_packet ( &pkt, &iGot );

				// If flushing decoder and no frame then done
				if (bEop && !iGot)
					break;
				}	// while

			// If the packets have ended and no frame was retreived, done with decoding
			if (bEop && !iGot)
				{
				// Notify
				_EMT(End,adtInt());

				// Ensure another call to 'Next' will fail without restarting
				pCtxFmt	= NULL;
 
				break;
				}	// if

			// If a frame was retreived, wait until 'next' signal
			if (iGot)
				break;
			}	// while

		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive

HRESULT Decode :: open_codec_context ( AVFormatContext *pCtx,
													enum AVMediaType t, int *piStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Prepare stream type for decoding
	//
	//	PARAMETERS
	//		-	pCtx is the current format context
	//		-	t is the media type
	//		-	piStm will receive the stream index
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	AVStream	*pStm		= NULL;
	AVCodec	*pDec		= NULL;
	int		ret;

	// Find the best stream for the provided media type
	CCLTRYE ( (ret = av_find_best_stream ( pCtx, t, -1, -1, NULL, 0 ))
					>= 0, ret );

	// Stream info
	CCLOK ( *piStm = ret; )
	CCLTRYE ( (pStm = pCtx->streams[*piStm]) != NULL, E_UNEXPECTED );

	// Find the decoder for the stream
	CCLTRYE ( (pDec = avcodec_find_decoder ( pStm->codecpar->codec_id )) 
					!= NULL, ERROR_NOT_FOUND );

	// Open the decoder 
	CCLTRYE ( (ret = avcodec_open2 ( pStm->codec, pDec, NULL ))
					>= 0, ret );

	return hr;
	}	// open_codec_context

/*
HRESULT Decode :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open a decoder context
	if (_RCP(Open))
		{
		AVCodec					*pCodec	= NULL;
		AVCodecContext			*pCtx		= NULL;
		AVCodecParserContext	*pPrs		= NULL;
		AVFrame					*pFrm		= NULL;
		AVCodecID				id			= AV_CODEC_ID_MPEG2VIDEO;
		char						*pcName	= NULL;
		int						ret;
		adtValue					vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		//
		// Two decoding modes supported, a previously open source or
		// raw decoding of bytes.

		// Previously open source ?
		if (hr == S_OK && pDct->load ( adtString(L"AVFormatContext"), vL ) == S_OK)
			{
			AVFormatContext	*pavCtx	= NULL;

			// Context
			CCLTRYE( (pavCtx = (AVFormatContext *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

			// Setup
			iStmV = -1;

			// Access the video stream
			if (	hr == S_OK &&
					open_codec_context ( pavCtx, AVMEDIA_TYPE_VIDEO, &iStmV ) >= 0)
				{
				// Allocate image where the decoded image will be put
				lprintf ( LOG_DBG, L"%dX%d@%d\r\n",
								pavCtx->streams[iStmV]->codec->width,
								pavCtx->streams[iStmV]->codec->height,
								pavCtx->streams[iStmV]->codec->pix_fmt );

				// Store decoder context
				CCLTRY ( pDct->store ( adtString(L"ContextVideo"), 
								adtLong((U64)pavCtx->streams[iStmV]->codec) ) );
				}	// if

			// Access the audio stream

			}	// if

		// Raw decoding
		else if (hr == S_OK)
			{
			// Decode specified ?
			if (hr == S_OK && strDecode.length() > 0)
				id = (!WCASECMP(strDecode,L"mpeg1") ?	AV_CODEC_ID_MPEG1VIDEO :
						!WCASECMP(strDecode,L"h.264") ?	AV_CODEC_ID_H264 :
						!WCASECMP(strDecode,L"h.265") ?	AV_CODEC_ID_H265 :
																	AV_CODEC_ID_MPEG2VIDEO);

			// Create the decoder
			CCLTRYE( (pCodec = avcodec_find_decoder ( id )) != NULL,
						ERROR_NOT_FOUND );

			// Initialize a parser for dealing with incoming frames
			CCLTRYE( (pPrs = av_parser_init ( pCodec->id )) != NULL,
						E_UNEXPECTED );

			// Allocate a decoding context
			CCLTRYE ( (pCtx = avcodec_alloc_context3(pCodec)) != NULL,
							ERROR_OUTOFMEMORY );

			// Open the context for the decoder
			CCLTRYE ( (ret = avcodec_open2(pCtx,pCodec,NULL)) >= 0, ret );
//			CCLOK ( lprintf ( LOG_DBG, L"Capabilities : 0x%x\r\n", pCodec->capabilities ); )
			}	// else


		// Results
		CCLTRY ( pDct->store ( adtString(L"Codec"),		adtLong((U64)pCodec) ) );
		CCLTRY ( pDct->store ( adtString(L"Parser"),		adtLong((U64)pPrs) ) );
		CCLTRY ( pDct->store ( adtString(L"Context"),	adtLong((U64)pCtx) ) );
		CCLTRY ( pDct->store ( adtString(L"Frame"),		adtLong((U64)pFrm) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		if (hr != S_OK)
			{
			if (pFrm != NULL)
				av_free(pFrm);
			if (pCtx != NULL)
				av_free(pCtx);
			}	// if
		_FREEMEM(pcName);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// About to close
		CCLOK ( _EMT(Close,adtIUnknown(pDct)); )

		// Clean up resources
		if (hr == S_OK && pDct->load ( adtString(L"ContextVideo"), vL ) == S_OK)
			{
			AVCodecContext *pCtx = (AVCodecContext *)(U64)adtLong(vL);
			if (pCtx != NULL)
				avcodec_close(pCtx);
			pDct->remove ( adtString(L"CodecVideo") );
			}	// if
		if (hr == S_OK && pDct->load ( adtString(L"Frame"), vL ) == S_OK)
			{
			av_free((void *)(U64)adtLong(vL));
			pDct->remove ( adtString(L"Frame") );
			}	// if
		if (hr == S_OK && pDct->load ( adtString(L"Parser"), vL ) == S_OK)
			{
			av_parser_close((AVCodecParserContext *)(U64)adtLong(vL));
			pDct->remove ( adtString(L"Parser") );
			}	// if
		if (hr == S_OK && pDct->load ( adtString(L"Context"), vL ) == S_OK)
			{
			AVCodecContext	*pCtx	= (AVCodecContext *)(U64)adtLong(vL);

			// Shutdown
			avcodec_close(pCtx);

			// Clean up
			av_free((void *)(U64)adtLong(vL));
			pDct->remove ( adtString(L"Context") );
			}	// if

		}	// else if

	// Stream/Image
	else if (_RCP(Fire))
		{
		IByteStream				*pStm		= NULL;
		AVCodecContext			*pCtx		= NULL;
		AVCodecContext			*pCtxV	= NULL;
		AVCodecParserContext	*pPrs		= NULL;
		AVFrame					*pFrm		= NULL;
		AVFormatContext		*pCtxFmt	= NULL;
//		AVFormatContext		*pavCtx	= NULL;
		bool						bEnd		= false;
		adtIUnknown				unkV(v);
		BYTE						bfr[2048];
		AVPacket					pkt,pktRx;
		U64						nx;
		adtValue					vL;
		int						ret,gf;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Contexts
		CCLTRY ( pDct->load ( adtString(L"Frame"), vL ) );
		CCLTRYE( (pFrm = (AVFrame *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Possible demuxed content
		if (hr == S_OK && pDct->load ( adtString(L"AVFormatContext"), vL ) == S_OK)
			{
			pCtxFmt = (AVFormatContext *)(U64)adtLong(vL);

			// Possible streams
			if (pDct->load ( adtString(L"ContextVideo"), vL ) == S_OK)
				pCtxV = (AVCodecContext *)(U64)adtLong(vL);
			}	// if

		// Direct decode, single stream
		else if (hr == S_OK)
			{
			// Direct decode
			CCLTRY ( pDct->load ( adtString(L"Context"), vL ) );
			CCLTRYE( (pCtx = (AVCodecContext *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );
			CCLTRY ( pDct->load ( adtString(L"Parser"), vL ) );
			CCLTRYE( (pPrs = (AVCodecParserContext *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

			// Incoming byte stream
			CCLTRY ( _QISAFE(unkV,IID_IByteStream,&pStm) );
			}	// else if

		//
		// Existing context
		//
		if (hr == S_OK && pCtxV != NULL)
			{
			// Read AV frames from the file
			av_init_packet(&pktRx);
			while (hr == S_OK)
				{
				// Read next or packet or end of stream
				if (!bEnd) bEnd = (av_read_frame ( pCtxFmt, &pktRx ) < 0);

				// Use up packet data
				gf		= 0;
				pkt	= pktRx;
				while (hr == S_OK)
					{
					// Decode the packet, currently just decoding video
					if (pkt.stream_index == iStmV)
						{
						// Decode next video frame
						CCLTRYE ( (ret = avcodec_decode_video2 ( pCtxV, pFrm, &gf, &pkt ))
										>= 0, ret );

						// Frame ?
						if (hr == S_OK && gf)
							{
							lprintf ( LOG_DBG, L"Got frame! pic_num %d\r\n",
														pFrm->coded_picture_number );
							}	// if

						// Data used
						CCLOK ( pkt.data += ret; )
						CCLOK ( pkt.size -= ret; )
						}	// if
					else
						pkt.size = 0;

					// Stop decoding at end of packet or until no more images
					if (pkt.size <= 0 || !gf)
						break;
					}	// while

				// Clean up
				av_free_packet(&pktRx);

				// If at end of stream and no more images, done
				if (bEnd && !gf)
					break;
				}	// while

			}	// if

		// Transfer stream bytes into the decoder
		while (hr == S_OK)
			{


			BYTE *data = bfr;

			// Read bytes, ok if fails with no bytes
			if (pStm->read ( bfr, sizeof(bfr), &nx ) != S_OK || nx == 0)
				break;

			// Use the parser to split the data into frames
			while (nx > 0)
				{
				// Perform parsing
				ret = av_parser_parse2 ( pPrs, pCtx, &(pkt.data), &(pkt.size), data, (int)nx,
													AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0 );
				if (ret < 0)
					{
					lprintf ( LOG_DBG, L"Error during parsing %d\r\n", ret );
					break;
					}	//i f

				// Data used
				nx		-= ret;
				data	+= ret;

				// Any packet data ?
				if (pkt.size < 1)
					continue;

				// Send packet to decoder
				hr = ((ret = avcodec_send_packet ( pCtx, &pkt )) == 0) ? S_OK : ret;

				// Read images generated from bytes
				while (hr == S_OK)
					{
					// Any images yet ?
					ret = avcodec_receive_frame ( pCtx, pFrm );

					// EAGAIN or EOF is fine, just means no data
					if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
						break;

					// Create a context for image conversion.  NOTE: This will need
					// to be its own node to make it more general, this is just for 
					// the current application.
					if (pswsCtx == NULL || pFrm->width != uWidthL || pFrm->height != uHeightL)
						{
						// Create conversion context to RGB
						CCLTRYE ( (pswsCtx = sws_getContext (	
										pFrm->width, pFrm->height, (AVPixelFormat) pFrm->format,
										pFrm->width, pFrm->height, AV_PIX_FMT_BGR24,
										0, 0, 0, 0 ) ) != NULL, E_UNEXPECTED );

						// Pre-fill image size
						CCLTRY ( pDctImg->store ( adtString(L"Width"), adtInt(pFrm->width) ) );
						CCLTRY ( pDctImg->store ( adtString(L"Height"), adtInt(pFrm->height) ) );

						// Context size
						CCLOK ( uWidthL	= pFrm->width; )
						CCLOK ( uHeightL	= pFrm->height; )
						}	// if

					// Ensure size is sufficient and access bits
					if (hr == S_OK && pBits->setSize ( pFrm->width*pFrm->height*3 ) == S_OK)
						{
						void		*pvBits		= NULL;
						int		stride[1]	= { pFrm->width*3 };
						uint8_t	*dstData[1];

						// Access bits
						CCLTRY ( pBits->getInfo ( &pvBits, NULL ) );
						CCLOK  ( dstData[0] = (uint8_t *) pvBits; )

						// Scale/convert image
						CCLTRYE ( (ret = sws_scale ( pswsCtx, pFrm->data, pFrm->linesize, 
										0, pFrm->height, dstData, stride )) > 0, ret );

						// Image output
	//					lprintf ( LOG_DBG, L"avcodec_receive_frame ret %d frame %d time %d,%d format %d\r\n", 
	//									ret, pCtx->frame_number, 
	//									pCtx->time_base.num, pCtx->time_base.den, pCtx->pix_fmt );
						_EMT(Fire,adtIUnknown(pDctImg));
						}	// if

					}	// while

				}	// while


			}	// while

		// Result
		if (hr != S_OK)
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pStm);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Decoder))
		hr = adtValue::toString(v,strDecode);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive
*/

