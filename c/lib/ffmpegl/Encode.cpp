////////////////////////////////////////////////////////////////////////
//
//								Encode.CPP
//
//			Implementation of the FFMPEG Encoder node
//
////////////////////////////////////////////////////////////////////////

#include "ffmpegl_.h"

// Utilties
#include	"../imagel/imagel.h"

#ifdef _DEBUG
static char buffer[1024];
void avlog_cb ( void *p, int level, const char *szFmt, va_list varg )
	{
	vsnprintf_s(buffer, sizeof(buffer), szFmt, varg );
	OutputDebugStringA ( buffer );
	}	// avlog_cb
#endif

Encode :: Encode ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDct			= NULL;
	}	// Encode

HRESULT Encode :: encodeA ( CTXENC *pEnc, bool bFlush )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to encode audio into the current context.
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	bFlush if flushing output
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr					= S_OK;
	int64_t	dst_nb_samples = 0;
	int		got				= 0;
	AVPacket	pkt				= { 0 };
//	int		ret;

	// State check
	CCLTRYE ( pEnc->pFrmA != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( pEnc->pswrCtx != NULL, ERROR_INVALID_STATE );

	// Convert samples from native format to destination codec format
	if (hr == S_OK && !bFlush)
		{
		// Compute destination # of samples
		dst_nb_samples = av_rescale_rnd ( swr_get_delay ( pEnc->pswrCtx, 
								pEnc->pCtxA->sample_rate ) + pEnc->pFrmA->nb_samples,
								pEnc->pCtxA->sample_rate, pEnc->pCtxA->sample_rate, AV_ROUND_UP );

		// Do no overrite reference
		av_frame_make_writable(pEnc->pFrmA);

		// Convert to destination format
		hr = ((got = swr_convert ( pEnc->pswrCtx, 
									pEnc->pFrmA->data,							(int)dst_nb_samples, 
									(const uint8_t **)pEnc->pFrmAt->data,	pEnc->pFrmAt->nb_samples )) > 0)
								? S_OK : S_FALSE;
		lprintf ( LOG_DBG, L"hr 0x%x swr_convert %d\r\n", hr, got );
		}	// if

	// Timestamp
	if (hr == S_OK && !bFlush)
		{
		AVRational tb = { 1, pEnc->pCtxA->sample_rate };

		// Compute timestamp for frame
		pEnc->pFrmA->pts = av_rescale_q (	samples_count, tb,
														pEnc->pCtxA->time_base );
		samples_count	+= dst_nb_samples;
		}	// if

	#if 0
	// Encode the frame
	av_init_packet(&pkt);
	CCLTRYE ( (avcodec_encode_audio2 ( pEnc->pCtxA, &pkt,
					(!bFlush) ? pEnc->pFrmA : NULL, &got ) == 0), S_FALSE );

	// If a packet was generated, write the frame
	if (hr == S_OK && got)
		{
		// Rescale output packet timestamp values from codec to stream timebase
		av_packet_rescale_ts ( &pkt, pEnc->pCtxA->time_base, pEnc->pStmA->time_base );
		pkt.stream_index = pEnc->pStmA->index;

		// DEBUG
		AVRational *time_base = &pEnc->pCtxFmt->streams[pkt.stream_index]->time_base;
		if (hr == S_OK)
			{
			lprintf(LOG_DBG, L"pts:%ld pts_time:%g dts:%ld dts_time:%g duration:%ld duration_time:%g stream_index:%d\n",
				pkt.pts, (av_q2d(*time_base) * pkt.pts),
				pkt.dts, (av_q2d(*time_base) * pkt.dts),
				pkt.duration, (av_q2d(*time_base) * pkt.duration),
				pkt.stream_index);
			}	// if

		// Write compressed frame to the media output
		CCLTRYE ( (ret = av_interleaved_write_frame ( pEnc->pCtxFmt, &pkt )) == 0, ret );
		}	// if
	#endif

	// Encode the next frame
	CCLTRYE ( (avcodec_send_frame ( pEnc->pCtxA, 
					(!bFlush) ? pEnc->pFrmA : NULL ) == 0), S_FALSE );

	// Transfer to stream
	CCLTRY ( writePackets(pEnc,false) );

	return hr;
	}	// encodeA

HRESULT Encode :: encodeV ( CTXENC *pEnc, void *pvBits, 
										U32 stride, U32 h, U32 ts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to encode an image frame into the current context.
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	pvBits is the source data (NULL to flush pipeline)
	//		-	stride is the stride of the source image
	//		-	height is the height of the source image
	//		-	ts is the timestamp to use
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	int		got	= 0;
	AVPacket	pkt	= { 0 };
//	int		ret;

	// State check
	CCLTRYE ( pEnc->pswsCtx != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( pEnc->pFrmV != NULL,	ERROR_INVALID_STATE );

	// Keep reference
	CCLOK ( av_frame_make_writable(pEnc->pFrmV); )

	// Convert image into prepared buffer
	if (hr == S_OK && pvBits != NULL)
		{
		// Calculate stride
		int		astride[1]	= { (int)stride };

		// Source data
		uint8_t	*srcData[1]	= { (uint8_t *) pvBits };

		// Perform conversion
		hr = (sws_scale ( pEnc->pswsCtx, srcData, astride,  0, h,
								pEnc->pFrmV->data, pEnc->pFrmV->linesize ) > 0) 
								? S_OK : S_FALSE;
		}	// if

	#if 0
	// Encode the image
	av_init_packet(&pkt);
	CCLOK ( pEnc->pFrmV->pts = ts; )
	CCLTRYE ( (avcodec_encode_video2 ( pEnc->pCtxV, &pkt,
					(pvBits != NULL) ? pEnc->pFrmV : NULL, &got ) == 0), S_FALSE );

	// If a packet was generated, write the frame
	if (hr == S_OK && got)
		{
		// Rescale output packet timestamp values from codec to stream timebase
		av_packet_rescale_ts ( &pkt, pEnc->pCtxV->time_base, pEnc->pStmV->time_base );
		pkt.stream_index = pEnc->pStmV->index;

		// DEBUG
		if (hr == S_OK)
			{
			AVRational *time_base = &pEnc->pCtxFmt->streams[pkt.stream_index]->time_base;
			lprintf(LOG_DBG, L"pts:%ld pts_time:%g dts:%ld dts_time:%g duration:%ld duration_time:%g stream_index:%d\n",
				pkt.pts, (av_q2d(*time_base) * pkt.pts),
				pkt.dts, (av_q2d(*time_base) * pkt.dts),
				pkt.duration, (av_q2d(*time_base) * pkt.duration),
				pkt.stream_index);
			}	// if

		// Write compressed frame to the media output
		CCLTRYE ( (ret = av_interleaved_write_frame ( pEnc->pCtxFmt, &pkt )) == 0, ret );
		}	// if
	#endif

	// Encode the next frame
	CCLOK ( pEnc->pFrmV->pts = ts; )
	CCLTRYE ( (avcodec_send_frame ( pEnc->pCtxV, 
					(pvBits != NULL) ? pEnc->pFrmV : NULL ) == 0), S_FALSE );

	// Transfer to stream
	CCLTRY ( writePackets(pEnc,true) );

	return hr;
	}	// encodeV

HRESULT Encode :: fillContext ( CTXENC *pCtx, bool bFrom )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Fill context structure from state dictionary
	//
	//	PARAMETERS
	//		-	pCtx will receive the state
	//		-	bFrom is true to transfer 'from' dictionary, false 'to' dictionary.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtValue		vL;

	// State check
	CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

	// Setup
	if (bFrom) memset ( pCtx, 0, sizeof(CTXENC) );

	// Fill context with information from the dictionary
	if (hr == S_OK && bFrom && pDct->load ( adtString(L"ContextFormat"), vL ) == S_OK)	
		pCtx->pCtxFmt = (AVFormatContext *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"ContextFormat"), adtLong((U64)pCtx->pCtxFmt) );

	// Video 
	if (hr == S_OK && bFrom && pDct->load ( adtString(L"CodecVideo"), vL ) == S_OK)	
		pCtx->pCodecV = (AVCodec *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"CodecVideo"), adtLong((U64)pCtx->pCodecV) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"ContextVideo"), vL ) == S_OK)	
		pCtx->pCtxV = (AVCodecContext *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"ContextVideo"), adtLong((U64)pCtx->pCtxV) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"StreamVideo"), vL ) == S_OK)	
		pCtx->pStmV = (AVStream *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"StreamVideo"), adtLong((U64)pCtx->pStmV) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"FrameVideo"), vL ) == S_OK)	
		pCtx->pFrmV = (AVFrame *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"FrameVideo"), adtLong((U64)pCtx->pFrmV) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"ContextScale"), vL ) == S_OK)	
		pCtx->pswsCtx = (SwsContext *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"ContextScale"), adtLong((U64)pCtx->pswsCtx) );

	// Audio
	if (hr == S_OK && bFrom && pDct->load ( adtString(L"CodecAudio"), vL ) == S_OK)	
		pCtx->pCodecA = (AVCodec *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"CodecAudio"), adtLong((U64)pCtx->pCodecA) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"ContextAudio"), vL ) == S_OK)	
		pCtx->pCtxA = (AVCodecContext *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"ContextAudio"), adtLong((U64)pCtx->pCtxA) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"StreamAudio"), vL ) == S_OK)	
		pCtx->pStmA = (AVStream *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"StreamAudio"), adtLong((U64)pCtx->pStmA) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"FrameAudio"), vL ) == S_OK)	
		pCtx->pFrmA = (AVFrame *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"FrameAudio"), adtLong((U64)pCtx->pFrmA) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"FrameAudioTemp"), vL ) == S_OK)	
		pCtx->pFrmAt = (AVFrame *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"FrameAudioTemp"), adtLong((U64)pCtx->pFrmAt) );

	if (hr == S_OK && bFrom && pDct->load ( adtString(L"ContextSample"), vL ) == S_OK)	
		pCtx->pswrCtx = (SwrContext *)(U64)adtLong(vL);
	else if (hr == S_OK && !bFrom)
		hr = pDct->store ( adtString(L"ContextSample"), adtLong((U64)pCtx->pswrCtx) );

	return hr;
	}	// fillContext

HRESULT Encode :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"Encoder"), vL ) == S_OK)
//			onReceive(prEncoder,vL);
//		if (pnDesc->load ( adtString(L"Fps"), vL ) == S_OK)
//			onReceive(prFps,vL);
//		if (pnDesc->load ( adtString(L"Rate"), vL ) == S_OK)
//			onReceive(prRate,vL);
//		if (pnDesc->load ( adtString(L"Silence"), vL ) == S_OK)
//			bSilence = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Encode :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open an encoder context
	if (_RCP(Open))
		{
//		AVCodec				*pCodecV	= NULL;
//		AVCodec				*pCodecA = NULL;
		AVOutputFormat		*pFmt		= NULL;
//		AVFormatContext	*pCtxFmt = NULL;
		char					*pcLoc	= NULL;
		char					*pcFmt	= NULL;
//		AVCodecID			id			= AV_CODEC_ID_NONE;
		int					ret		= 0;
		CTXENC				ctx;
		adtValue				vL;
		adtString			strFmt;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Setup
		memset ( &ctx, 0, sizeof(ctx) );

		// Debug
		#ifdef _DEBUG
		av_log_set_callback ( avlog_cb );
		av_log_set_level ( AV_LOG_DEBUG );
		#endif

		//
		// Output format
		//

		// Is a location specified ?  Convert to ASCII for use in API
		if (hr == S_OK && strLoc.length() > 0)
			hr = strLoc.toAscii ( &pcLoc );

		// Is an output format being forced ?
		if (hr == S_OK && pDct->load ( adtString(L"OutputFormat"), vL ) == S_OK)
			hr = adtString(vL).toAscii ( &pcFmt );

		// Create and store an output format context
		CCLTRYE ( (pFmt = av_guess_format(pcFmt,pcLoc,NULL)) != NULL,
						ERROR_NOT_FOUND );

		// Allocate an output format context
		CCLTRYE ( (ret = avformat_alloc_output_context2 ( &(ctx.pCtxFmt), 
						pFmt, NULL, NULL )) >= 0, ret );

		//
		// Encoders
		//

		// Obtain all specified encoders for video, audio, subtitles ?
		for (int i = 0;hr == S_OK && i < 2;++i)
			{
			AVCodec	*pCodec	= NULL;
			char		*pcEnc	= NULL;

			// Encoder
			adtString	strEnc = (i == 0) ? L"Video" : L"Audio";

			// Encoder specified ?
			if (pDct->load ( strEnc, vL ) != S_OK)
				continue;

			// To ASCII for API
			CCLTRY ( adtString(vL).toAscii ( &pcEnc ) );

			// Map the name to encoder
			CCLTRYE ( (pCodec = avcodec_find_encoder_by_name ( pcEnc )) != NULL,
							ERROR_NOT_FOUND );

			// Prepare the appropriate stream
			switch (i)
				{
				// Video
				case 0 :
					{
					AVDictionary	*pOpts	= NULL;
					int				iRate		= 30;
					AVPixelFormat	fmt;

					// Assign context
					ctx.pCodecV = pCodec;

					// Create an output stream
					CCLTRYE ( (ctx.pStmV = avformat_new_stream ( ctx.pCtxFmt, NULL ))
									!= NULL, E_OUTOFMEMORY );

					// Stream Id
					CCLOK ( ctx.pStmV->id = i; )

					// Allocate an encoding context
					CCLTRYE ( (ctx.pCtxV = avcodec_alloc_context3 ( ctx.pCodecV ))
									!= NULL, E_OUTOFMEMORY );

					// Fill out the encoding context
					if (hr == S_OK)
						{
						// Defaults

						// Codec parameters
						AVCodecParameters *
						param = ctx.pStmV->codecpar;

						// Codec setup
						param->codec_id	= ctx.pCodecV->id;
						param->codec_type	= AVMediaType::AVMEDIA_TYPE_VIDEO;

						// Image attributes
						param->width	= 320;
						if (pDct->load ( adtString(L"Width"), vL ) == S_OK)
							param->width = adtInt(vL);
						param->height	= 320;
						if (pDct->load ( adtString(L"Height"), vL ) == S_OK)
							param->height = adtInt(vL);
						if (pDct->load ( adtString(L"Rate"), vL ) == S_OK)
							iRate = adtInt(vL);

						// Desired container pixel format.
						param->format = AV_PIX_FMT_YUV420P;

						// Bit rate.  Based roughly on Google YouTube recommendations.
						// Very high, use as 'maximum' quality.
						if (param->height <= 1080)
							param->bit_rate = (iRate <= 30) ? 8 : 12;
						else
							param->bit_rate = (iRate <= 30) ? 35 : 55;

						// Quality override, lookup ?
						if (hr == S_OK && pDct->load ( adtString(L"Quality"), vL ) == S_OK)
							{
							adtString strQ(vL);

							// Maximum quality uses above values
							if (!WCASECMP(strQ,L"Default"))
								{
								// More realistic speeds
								// https://support.video.ibm.com/hc/en-us/articles/207852117-Internet-connection-and-recommended-encoding-settings
								if (param->height <= 1080)
									param->bit_rate = (iRate <= 30) ? 6 : 10;
								else
									param->bit_rate = (iRate <= 30) ? 14 : 30;
								}	// if

							}	// if

						// Final calculation
						param->bit_rate *= (1<<20);	// * 1 MBit

						// Bit rate
//						param->bit_rate	= 2000000;
//						param->bit_rate	= 32*(1<<20);

						// Transfer parameters to context
						avcodec_parameters_to_context ( ctx.pCtxV, param );

						// Timebase
						ctx.pStmV->time_base = { 1, iRate };
						ctx.pCtxV->time_base = ctx.pStmV->time_base;

						// Maximum number of b-frames (this add latency ?)
						ctx.pCtxV->max_b_frames = 2;

						// Emit one intra frame every X frames
						ctx.pCtxV->gop_size	= 25;

						// Global headers required ?
						if (ctx.pCtxFmt->oformat->flags & AVFMT_GLOBALHEADER)
							ctx.pCtxV->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
						}	// if

					// Open the context for the video
					CCLTRYE ( (ret = avcodec_open2 ( ctx.pCtxV, ctx.pCodecV, &pOpts ))
									>= 0, ret );

					// Prepare a frame to receive the converted frame

					// Create a frame for use
					CCLTRYE ( (ctx.pFrmV = av_frame_alloc()) != NULL, E_OUTOFMEMORY );

					// Pre-fill the frame information about the image
					if (hr == S_OK)
						{
						// Image parameters
						ctx.pFrmV->format = ctx.pCtxV->pix_fmt;
						ctx.pFrmV->width	= ctx.pCtxV->width;
						ctx.pFrmV->height	= ctx.pCtxV->height;
						}	// if

					// Allocate memory for the frame buffer
					CCLTRYE ( (ret = av_image_alloc ( ctx.pFrmV->data, ctx.pFrmV->linesize, ctx.pCtxV->width,
									ctx.pCtxV->height, ctx.pCtxV->pix_fmt, 32 ) > 0), ret );

					// Supported nSpace formats
					CCLTRY( pDct->load ( adtString(L"PixelFormat"), vL ) );
					CCLTRY( adtValue::toString(vL,strFmt) );
					CCLOK	( fmt =	(!WCASECMP(strFmt,L"B8G8R8"))		? AV_PIX_FMT_BGR24 :
										(!WCASECMP(strFmt,L"R8G8B8"))		? AV_PIX_FMT_RGB24 :
										(!WCASECMP(strFmt,L"B8G8R8A8"))	? AV_PIX_FMT_BGRA :
										(!WCASECMP(strFmt,L"ABGR32"))		? AV_PIX_FMT_ABGR :
										(!WCASECMP(strFmt,L"R8G8B8A8"))	? AV_PIX_FMT_RGBA :
										(!WCASECMP(strFmt,L"A8R8G8B8"))	? AV_PIX_FMT_ARGB :
										(!WCASECMP(strFmt,L"U8"))		? AV_PIX_FMT_GRAY8 :
										// HDR
										(!WCASECMP(strFmt,L"B16G16R16"))	? AV_PIX_FMT_RGB48LE :
										AV_PIX_FMT_NONE; )
					CCLTRYE	( (fmt != AV_PIX_FMT_NONE), E_NOTIMPL );

					// Prepare for conversion to required encoding format
					CCLTRYE ( (ctx.pswsCtx = sws_getContext ( ctx.pCtxV->width, ctx.pCtxV->height, 
									fmt, ctx.pCtxV->width, ctx.pCtxV->height, 
									ctx.pCtxV->pix_fmt, 0, 0, 0, 0 ) ) != NULL, E_UNEXPECTED );

					// Copy the stream parameters to the muxer
					CCLOK ( avcodec_parameters_from_context ( ctx.pStmV->codecpar, ctx.pCtxV ); )

					// Initial "timestamp"
					CCLTRY ( pDct->store ( adtString(L"NextPtsV"), adtInt(0) ) );
					}	// Video
					break;

				// Audio
				// For now audio channel is just to encoder silence
				case 1 :

					// Assign context
					ctx.pCodecA		= pCodec;
					samples_count	= 0;

					// Create an output stream
					CCLTRYE ( (ctx.pStmA = avformat_new_stream ( ctx.pCtxFmt, NULL ))
									!= NULL, E_OUTOFMEMORY );

					// Stream Id
					CCLOK ( ctx.pStmA->id = i; )

					// Allocate an encoding context
					CCLTRYE ( (ctx.pCtxA = avcodec_alloc_context3 ( ctx.pCodecA ))
									!= NULL, E_OUTOFMEMORY );

					// Fill out the audio stream codec parameters
					if (hr == S_OK)
						{
						AVCodecContext *pCtx = ctx.pCtxA;

						// Fill parameters
						pCtx->sample_fmt =	(	ctx.pCodecA->sample_fmts) ? 
														ctx.pCodecA->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;

						// Defaults for synthentic audio track
						pCtx->bit_rate		= 64000;
						pCtx->sample_rate = 44100;
						if (ctx.pCodecA->supported_samplerates)
							{
							// Default sample rate
							pCtx->sample_rate = ctx.pCodecA->supported_samplerates[0];
							for (int i = 0;(ctx.pCodecA)->supported_samplerates[i];++i)
								if (ctx.pCodecA->supported_samplerates[i] == 44100)
									pCtx->sample_rate = 44100;
							}	// if
						pCtx->channels			= av_get_channel_layout_nb_channels(pCtx->channel_layout);
						pCtx->channel_layout	= AV_CH_LAYOUT_STEREO;
						if (ctx.pCodecA->channel_layouts)
							{
							// Default sample rate
							pCtx->channel_layout = ctx.pCodecA->channel_layouts[0];
							for (int i = 0;(ctx.pCodecA)->channel_layouts[i];++i)
								if (ctx.pCodecA->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
									pCtx->channel_layout = AV_CH_LAYOUT_STEREO;
							}	// if
						pCtx->channels					= av_get_channel_layout_nb_channels(pCtx->channel_layout);
						ctx.pStmA->time_base.num	= 1;
						ctx.pStmA->time_base.den	= pCtx->sample_rate;
			//			ctx.pCtxA->time_base			= ctx.pStmA->time_base;

						// Some formats want stream headers to be separate.
						if (ctx.pCtxFmt->oformat->flags & AVFMT_GLOBALHEADER)
							ctx.pCtxA->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

						// Codec parameters
						AVCodecParameters *param = ctx.pStmA->codecpar;

						// Codec setup
						param->codec_id	= ctx.pCodecA->id;
						param->codec_type	= AVMediaType::AVMEDIA_TYPE_AUDIO;
						}	// if

					// Open the context for the audio
					CCLTRYE ( (ret = avcodec_open2(ctx.pCtxA,ctx.pCodecA,NULL)) >= 0, ret );

					// Samples
					int nb_samples = ctx.pCtxA->frame_size;
					if (ctx.pCodecA->capabilities & AV_CODEC_CAP_VARIABLE_FRAME_SIZE)
						nb_samples = 1000;

					// Create a frame for use
					CCLTRYE( ((ctx.pFrmA = av_frame_alloc()) != NULL), E_OUTOFMEMORY );
					if (hr == S_OK)
						{
						// Fill in known frame information
//						av_frame_make_writable(ctx.pFrmA);
						ctx.pFrmA->format				= ctx.pCtxA->sample_fmt;
						ctx.pFrmA->channel_layout	= ctx.pCtxA->channel_layout;
						ctx.pFrmA->sample_rate		= ctx.pCtxA->sample_rate;
						ctx.pFrmA->nb_samples		= nb_samples;

						// Allocate a buffer for the samples
						CCLTRYE ( (ret = av_frame_get_buffer ( ctx.pFrmA, 0 )) >= 0, ret );

						// Create own frame for 16-bit signed samples
						CCLTRYE( ((ctx.pFrmAt = av_frame_alloc()) != NULL), E_OUTOFMEMORY );

						// Fill in known frame information
						if (hr == S_OK)
							{
							ctx.pFrmAt->format			= AV_SAMPLE_FMT_S16;
							ctx.pFrmAt->channel_layout	= ctx.pCtxA->channel_layout;
							ctx.pFrmAt->sample_rate		= ctx.pCtxA->sample_rate;
							ctx.pFrmAt->nb_samples		= nb_samples;
							}	// if

						// Allocate a buffer for the samples
						CCLTRYE ( (ret = av_frame_get_buffer ( ctx.pFrmAt, 0 )) >= 0, ret );

						// Fill with silence
//						int16_t *pi = (int16_t *)ctx.pFrmA->data[0];
//						for (int i = 0;hr == S_OK && i < nb_samples;++i)
//							*pi++ = 0;
//						CCLOK ( memset ( ctx.pFrmA->data[0], 0, 2*nb_samples ); )
						for (int j = 0; j <ctx.pFrmAt->nb_samples; j++)
							((int16_t*)ctx.pFrmAt->data[0])[j] = 0;

 						// Copy the stream parameters to the muxer
						CCLOK ( avcodec_parameters_from_context(ctx.pStmA->codecpar, ctx.pCtxA ); )

						// Create a resampler context
						CCLTRYE ( (ctx.pswrCtx = swr_alloc()) != NULL, E_OUTOFMEMORY );

						// Set options
						if (hr == S_OK)
							{
							av_opt_set_int       (ctx.pswrCtx, "in_channel_count",   ctx.pCtxA->channels,		0 );
							av_opt_set_int       (ctx.pswrCtx, "in_sample_rate",     ctx.pCtxA->sample_rate,	0 );
							av_opt_set_sample_fmt(ctx.pswrCtx, "in_sample_fmt",      AV_SAMPLE_FMT_S16,				0 );
							av_opt_set_int       (ctx.pswrCtx, "out_channel_count",  ctx.pCtxA->channels,		0 );
							av_opt_set_int       (ctx.pswrCtx, "out_sample_rate",    ctx.pCtxA->sample_rate,	0 );
							av_opt_set_sample_fmt(ctx.pswrCtx, "out_sample_fmt",     ctx.pCtxA->sample_fmt,		0 );
							}	// if

						// Initialize resampling context
						CCLTRYE ( (ret = swr_init ( ctx.pswrCtx )) >= 0, ret );
						}	// if

					break;
				}	// switch

			// Clean up
			_FREEMEM(pcEnc);
			}	// for

		//
		// Output
		//

		// Container file requested ?
		if (hr == S_OK && pcLoc != NULL)
			{
			// Debug
			av_dump_format ( ctx.pCtxFmt, 0, pcLoc, 1 );

			// Open target location
			CCLTRYE ( (ret = avio_open ( &(ctx.pCtxFmt->pb), 
							pcLoc, AVIO_FLAG_WRITE )) >= 0, ret );
			}	// if

		// Write the header
		CCLTRYE ( (ret = avformat_write_header ( ctx.pCtxFmt, NULL )) >= 0, ret );

		// If successful, transfer encoding context to dictionary for future calls
		if (pDct != NULL)
			fillContext ( &ctx, false );

		// Results
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			{
			// Clean up
			onReceive(prClose,v);

			// Notify
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_FREEMEM(pcFmt);
		_FREEMEM(pcLoc);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		adtValue		vL;
		CTXENC		ctx;

		// Obtain encoder context info
		memset ( &ctx, 0, sizeof(ctx) );
		CCLTRY ( fillContext ( &ctx ) );

		// Free valid resources
		if (ctx.pCtxV != NULL)
			{
			avcodec_free_context ( &(ctx.pCtxV) );
			pDct->remove ( adtString(L"ContextVideo") );
			}	// if
		if (ctx.pCtxA != NULL)
			{
			avcodec_free_context ( &(ctx.pCtxA) );
			pDct->remove ( adtString(L"ContextAudio") );
			}	// if
		if (ctx.pFrmV != NULL)
			{
			av_frame_free ( &(ctx.pFrmV) );
			pDct->remove ( adtString(L"FrameVideo") );
			}	// if
		if (ctx.pFrmA != NULL)
			{
			av_frame_free ( &(ctx.pFrmA) );
			pDct->remove ( adtString(L"FrameAudio") );
			}	// if
		if (ctx.pFrmAt != NULL)
			{
			av_frame_free ( &(ctx.pFrmAt) );
			pDct->remove ( adtString(L"FrameAudioTemp") );
			}	// if
		if (ctx.pswsCtx != NULL)
			{
			sws_freeContext ( ctx.pswsCtx );
			pDct->remove ( adtString(L"ContextScale") );
			}	// if
		if (ctx.pswrCtx != NULL)
			{
			swr_free(&(ctx.pswrCtx));
			pDct->remove ( adtString(L"ContextSample") );
			}	// if
		if (hr == S_OK && ctx.pCtxFmt != NULL)
			{
			// Close possible output location
			if (ctx.pCtxFmt->pb != NULL)
				avio_close ( ctx.pCtxFmt->pb );

			// Clean up
			avformat_free_context ( ctx.pCtxFmt );
			pDct->remove ( adtString(L"StreamVideo") );
			pDct->remove ( adtString(L"StreamAudio") );
			pDct->remove ( adtString(L"ContextFormat") );
			}	// if

		// Clean up
		if (pDct != NULL)
			{
			pDct->remove ( adtString(L"CodecVideo") );
			pDct->remove ( adtString(L"CodecAudio") );
			pDct->remove ( adtString(L"NextPtsV") );
			}	// if

		}	// else if

	// Image to encode 
	else if (_RCP(Image))
		{
		IDictionary		*pDctImg = NULL;
		U32				ts			= 0;
		CTXENC			ctx;
		adtIUnknown		unkV(v);
		ImageDct			dctImg;
		adtValue			vL;

		// Context
		CCLTRY ( fillContext ( &ctx ) );

		// Image information
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctImg) );
		CCLTRY ( dctImg.lock ( pDctImg ) );

		// Next timestamp 
		if (hr == S_OK && pDct->load ( adtString(L"NextPtsV"), vL ) == S_OK)
			ts = adtInt(vL);

		// Encode frame
		CCLOK ( lprintf ( LOG_DBG, L"Image %dx%dx%d\r\n", dctImg.iW, dctImg.iH, dctImg.iBpp ); )
		CCLTRY ( encodeV ( &ctx, dctImg.pvBits, dctImg.iW*(dctImg.iBpp/8), dctImg.iH, ts ) );

		// Encoding audio track ?
		if (hr == S_OK && ctx.pCtxA != NULL)
			hr = encodeA ( &ctx, false );

		// Next timestamp
		if (hr == S_OK)
			hr = pDct->store ( adtString(L"NextPtsV"), adtInt(ts+1) );

		// Clean up
		_RELEASE(pDctImg);
		}	// else if

	// Flush encoder stream of remaining data
	else if (_RCP(Flush))
		{
		CTXENC				ctx;

		// Grab available info
		CCLTRY ( fillContext ( &ctx ) );

		// Encode 'nothing' to flush output
		CCLTRY ( encodeV ( &ctx, NULL, 0, 0, 0 ) );
		if (hr == S_OK && ctx.pCtxA != NULL)
			hr = encodeA ( &ctx, true );

		// If writing to location, write the file trailer
		if (hr == S_OK && ctx.pCtxFmt != NULL)
			av_write_trailer(ctx.pCtxFmt);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Location))
		hr = adtValue::toString(v,strLoc);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive

HRESULT Encode :: writePackets ( CTXENC *pEnc, bool bV )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Transfers any encoded data into destination
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	bV true means video, false means audio
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	bool		bUp	= false;
	AVPacket	pkt	= { 0 };
	int		ret;

	// State check
	CCLTRYE ( pEnc->pCtxFmt != NULL, ERROR_INVALID_STATE );

	// Active context and stream
	AVCodecContext *
	pavCtx	= (bV) ? pEnc->pCtxV : pEnc->pCtxA;
	AVStream *
	pavStm	= (bV) ? pEnc->pStmV : pEnc->pStmA;

	// State check
	if (pavCtx == NULL || pavStm == NULL)
		return hr;

	// See if there is any encoding data
	while (hr == S_OK)
		{
		// Output buffer info
		av_init_packet(&pkt);
//		pkt.flags |= AV_PKT_FLAG_KEY;
		ret = avcodec_receive_packet ( pavCtx, &pkt );

		// EAGAIN or EOF is fine, just means no data
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			break;

		// Other error ?
		hr = (ret >= 0) ? S_OK : S_FALSE;

		// Debug
//		lprintf ( LOG_DBG, L"%d) pts %d dts %d\r\n", (bV) ? 0 : 1, pkt.pts, pkt.dts );

		// Rescale output packet timestamp values from codec to stream timebase
		if (bV)
			{
			av_packet_rescale_ts ( &pkt, pEnc->pCtxV->time_base, pEnc->pStmV->time_base );
//			av_packet_rescale_ts ( &pkt, pEnc->pStmV->codec->time_base, pEnc->pStmV->time_base );
			pkt.stream_index = pEnc->pStmV->index;
			}	// if
		else
			{
			av_packet_rescale_ts ( &pkt, pEnc->pCtxA->time_base, pEnc->pStmA->time_base );
//			av_packet_rescale_ts ( &pkt, pEnc->pStmA->codec->time_base, pEnc->pStmA->time_base );
			pkt.stream_index = pEnc->pStmA->index;
			}	// else

		// Debug
		#if _DEBUG
		lprintf ( LOG_DBG, L"Packet :\r\n" );
		lprintf ( LOG_DBG, L"Stream           : %s (%d)\r\n", (bV) ? L"video" : L"audio", pkt.stream_index );
		lprintf ( LOG_DBG, L"Data             : %p\r\n", pkt.data );
		lprintf ( LOG_DBG, L"Size             : %d\r\n", pkt.size );
		lprintf ( LOG_DBG, L"Decompression ts : %d\r\n", pkt.dts );
		lprintf ( LOG_DBG, L"Presentation ts  : %d\r\n", pkt.pts );
		lprintf ( LOG_DBG, L"Flags            : 0x%x\r\n", pkt.flags );
		lprintf ( LOG_DBG, L"Duration         : 0x%x\r\n", pkt.duration );
		lprintf ( LOG_DBG, L"Position         : 0x%x\r\n", pkt.pos );
		#endif

		// Write the frame
		ret = av_interleaved_write_frame ( pEnc->pCtxFmt, &pkt );
		if (ret < 0)
			lprintf ( LOG_DBG, L"av_interleaved_write_frame failed %d\r\n", ret );			

		// Done w/packet
		av_packet_unref(&pkt);
		}	// while

	return hr;
	}	// writePackets

/*
Encode :: Encode ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDct			= NULL;
	pStm			= NULL;
	iFps			= 30;
	iRate			= 0;		// Estimate
	strEncode	= L"MPEG2";
	}	// Encode

HRESULT Encode :: encodeA ( CTXENC *pEnc, bool bFlush )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to encode audio into the current context.
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	bFlush if flushing output
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr					= S_OK;
	int64_t	dst_nb_samples = 0;
	int		got				= 0;
	AVPacket	pkt				= { 0 };
//	int		ret;

	// State check
	CCLTRYE ( pEnc->pFrmA != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( pEnc->pswrCtx != NULL, ERROR_INVALID_STATE );

	// Convert samples from native format to destination codec format
	if (hr == S_OK && !bFlush)
		{
		// Update time stamp
//		CCLOK ( pEnc->pFrmA->pts = next_pts; )

		// Compute destination # of samples
		dst_nb_samples = av_rescale_rnd ( swr_get_delay ( pEnc->pswrCtx, 
								pEnc->pCtxA->sample_rate ) + pEnc->pFrmA->nb_samples,
								pEnc->pCtxA->sample_rate, pEnc->pCtxA->sample_rate, AV_ROUND_UP );

		// Do no overrite reference
		av_frame_make_writable(pEnc->pFrmA);

		// Convert to destination format
		hr = (swr_convert ( pEnc->pswrCtx, 
									pEnc->pFrmA->data,							(int)dst_nb_samples, 
									(const uint8_t **)pEnc->pFrmAt->data,	pEnc->pFrmAt->nb_samples ) > 0)
								? S_OK : S_FALSE;
		}	// if

	// Timestamp
	if (hr == S_OK && !bFlush)
		{
		AVRational tb = { 1, pEnc->pCtxA->sample_rate };

		// Compute timestamp for frame
		pEnc->pFrmA->pts = av_rescale_q ( samples_count, tb,
														pEnc->pCtxA->time_base );
		samples_count	+= dst_nb_samples;
		next_ptsa		+= pEnc->pFrmA->nb_samples;
		}	// if

	#if 0
	// Encode the frame
	av_init_packet(&pkt);
	CCLTRYE ( (avcodec_encode_audio2 ( pEnc->pCtxA, &pkt,
					(!bFlush) ? pEnc->pFrmA : NULL, &got ) == 0), S_FALSE );

	// If a packet was generated, write the frame
	if (hr == S_OK && got)
		{
		// Rescale output packet timestamp values from codec to stream timebase
		av_packet_rescale_ts ( &pkt, pEnc->pCtxA->time_base, pEnc->pStmA->time_base );
		pkt.stream_index = pEnc->pStmA->index;

		// DEBUG
		AVRational *time_base = &pEnc->pCtxFmt->streams[pkt.stream_index]->time_base;
		if (hr == S_OK)
			{
			lprintf(LOG_DBG, L"pts:%ld pts_time:%g dts:%ld dts_time:%g duration:%ld duration_time:%g stream_index:%d\n",
				pkt.pts, (av_q2d(*time_base) * pkt.pts),
				pkt.dts, (av_q2d(*time_base) * pkt.dts),
				pkt.duration, (av_q2d(*time_base) * pkt.duration),
				pkt.stream_index);
			}	// if

		// Write compressed frame to the media output
		CCLTRYE ( (ret = av_interleaved_write_frame ( pEnc->pCtxFmt, &pkt )) == 0, ret );
		}	// if
	#endif

	// Encode the next frame
	CCLTRYE ( (avcodec_send_frame ( pEnc->pCtxA, 
					(!bFlush) ? pEnc->pFrmA : NULL ) == 0), S_FALSE );

	// Transfer to stream
	CCLTRY ( writePackets(pEnc,false) );

	return hr;
	}	// encodeA

HRESULT Encode :: encodeV ( CTXENC *pEnc, void *pvBits, U32 stride, U32 h )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to encode an image frame into the current context.
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	pvBits is the source data (NULL to flush pipeline)
	//		-	stride is the stride of the source image
	//		-	height is the height of the source image
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	int		got	= 0;
	AVPacket	pkt	= { 0 };
//	int		ret;

	// State check
	CCLTRYE ( pEnc->pswsCtx != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( pEnc->pFrmV != NULL, ERROR_INVALID_STATE );

	// Keep reference
	CCLOK ( av_frame_make_writable(pEnc->pFrmV); )

	// Convert image into prepared buffer
	if (hr == S_OK && pvBits != NULL)
		{
		// Calculate stride
		int		astride[1]	= { (int)stride };

		// Source data
		uint8_t	*srcData[1]	= { (uint8_t *) pvBits };

		// Perform conversion
		hr = (sws_scale ( pEnc->pswsCtx, srcData, astride,  0, h,
								pEnc->pFrmV->data, pEnc->pFrmV->linesize ) > 0) 
								? S_OK : S_FALSE;
		}	// if

	#if 0
	// Encode the image
	av_init_packet(&pkt);
	CCLOK ( pEnc->pFrmV->pts = next_ptsv++; )
	CCLTRYE ( (avcodec_encode_video2 ( pEnc->pCtxV, &pkt,
					(pvBits != NULL) ? pEnc->pFrmV : NULL, &got ) == 0), S_FALSE );

	// If a packet was generated, write the frame
	if (hr == S_OK && got)
		{
		// Rescale output packet timestamp values from codec to stream timebase
		av_packet_rescale_ts ( &pkt, pEnc->pCtxV->time_base, pEnc->pStmV->time_base );
		pkt.stream_index = pEnc->pStmV->index;

		// DEBUG
		if (hr == S_OK)
			{
			AVRational *time_base = &pEnc->pCtxFmt->streams[pkt.stream_index]->time_base;
			lprintf(LOG_DBG, L"pts:%ld pts_time:%g dts:%ld dts_time:%g duration:%ld duration_time:%g stream_index:%d\n",
				pkt.pts, (av_q2d(*time_base) * pkt.pts),
				pkt.dts, (av_q2d(*time_base) * pkt.dts),
				pkt.duration, (av_q2d(*time_base) * pkt.duration),
				pkt.stream_index);
			}	// if

		// Write compressed frame to the media output
		CCLTRYE ( (ret = av_interleaved_write_frame ( pEnc->pCtxFmt, &pkt )) == 0, ret );
		}	// if
	#endif

	// Encode the next frame
	CCLOK ( pEnc->pFrmV->pts = next_ptsv++; )
	CCLTRYE ( (avcodec_send_frame ( pEnc->pCtxV, 
					(pvBits != NULL) ? pEnc->pFrmV : NULL ) == 0), S_FALSE );

	// Transfer to stream
	CCLTRY ( writePackets(pEnc,true) );


	return hr;
	}	// encodeV

HRESULT Encode :: fillContext ( IDictionary *pDct, CTXENC *pEnc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Fills the encoder context structure from dictionary
	//
	//	PARAMETERS
	//		-	pDct contains the context
	//		-	pEnc will receive the ptrs. from the context
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	adtValue	vL;

	// Setup
	memset ( pEnc, 0, sizeof(CTXENC) );

	// Valid dictionary
	CCLTRYE ( (pDct != NULL), ERROR_INVALID_STATE );
	if (hr == S_OK)
		{
		// Fill context with information from the dictionary
		if (pDct->load ( adtString(L"Format"), vL ) == S_OK)	
			pEnc->pCtxFmt = (AVFormatContext *)(U64)adtLong(vL);

		// Video
		if (pDct->load ( adtString(L"CodecVideo"), vL ) == S_OK)
			pEnc->pCodecV = (AVCodec *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"ContextVideo"), vL ) == S_OK)
			pEnc->pCtxV = (AVCodecContext *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"StreamVideo"), vL ) == S_OK)	
			pEnc->pStmV = (AVStream *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"FrameVideo"), vL ) == S_OK)
			pEnc->pFrmV = (AVFrame *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"ContextScale"), vL ) == S_OK)
			pEnc->pswsCtx = (SwsContext *)(U64)adtLong(vL);

		// Audio
		if (pDct->load ( adtString(L"ContextAudio"), vL ) == S_OK)
			pEnc->pCtxA = (AVCodecContext *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"StreamAudio"), vL ) == S_OK)	
			pEnc->pStmA = (AVStream *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"FrameAudio"), vL ) == S_OK)
			pEnc->pFrmA = (AVFrame *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"FrameAudioTemp"), vL ) == S_OK)
			pEnc->pFrmAt = (AVFrame *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"CodecAudio"), vL ) == S_OK)
			pEnc->pCodecA = (AVCodec *)(U64)adtLong(vL);
		if (pDct->load ( adtString(L"ContextSample"), vL ) == S_OK)
			pEnc->pswrCtx = (SwrContext *)(U64)adtLong(vL);
		}	// if

	return hr;
	}	// fillContext

HRESULT Encode :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Encoder"), vL ) == S_OK)
			onReceive(prEncoder,vL);
		if (pnDesc->load ( adtString(L"Fps"), vL ) == S_OK)
			onReceive(prFps,vL);
		if (pnDesc->load ( adtString(L"Rate"), vL ) == S_OK)
			onReceive(prRate,vL);
		if (pnDesc->load ( adtString(L"Silence"), vL ) == S_OK)
			bSilence = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pStm);
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Encode :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open an encoder context
	if (_RCP(Open))
		{
		AVCodec				*pCodecV	= NULL;
		AVCodec				*pCodecA = NULL;
		AVFormatContext	*pCtxFmt = NULL;
		char					*pcTmp	= NULL;
		AVOutputFormat		*fmt		= NULL;
		AVCodecID			id			= AV_CODEC_ID_NONE;
		int					ret		= 0;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strEncode.length() > 0, ERROR_INVALID_STATE );

		// Debug
		av_log_set_callback ( avlog_cb );
		av_log_set_level ( AV_LOG_DEBUG );

		// Location specified ?
		if (hr == S_OK && strLoc.length() > 0)
			hr = strLoc.toAscii ( &pcTmp );

		// See if specified Encoder is availabe TODO: What are the available Encoder names ?
		if (hr == S_OK)
			{
			// Own mapping of string to constant (better way ?)
			if (!WCASECMP(strEncode,L"MPEG2"))
				id = AV_CODEC_ID_MPEG2VIDEO;
			else if (!WCASECMP(strEncode,L"H264"))
				id = AV_CODEC_ID_H264;
			else if (!WCASECMP(strEncode,L"H265"))
				id = AV_CODEC_ID_H265;
			else if (!WCASECMP(strEncode,L"VP9"))
				id = AV_CODEC_ID_VP9;
			else if (!WCASECMP(strEncode,L"FLV"))
				id = AV_CODEC_ID_FLV1;
			else
				hr = E_NOTIMPL;
id = AV_CODEC_ID_FLV1;
id = AV_CODEC_ID_WEBP;
id = AV_CODEC_ID_H264;

			// Locate encoder
			CCLTRYE( (pCodecV = avcodec_find_encoder ( id )) != NULL,
							ERROR_NOT_FOUND );
			CCLTRY ( pDct->store ( adtString(L"CodecVideo"),	adtLong((U64)pCodecV) ) );
			}	// if

		// Default audio codec if needed
		CCLTRYE( (pCodecA = avcodec_find_encoder ( AV_CODEC_ID_MP3 )) != NULL,
//		CCLTRYE( (pCodecA = avcodec_find_encoder ( AV_CODEC_ID_AAC )) != NULL,
						ERROR_NOT_FOUND );
		CCLTRY ( pDct->store ( adtString(L"CodecAudio"), adtLong((U64)pCodecA) ) );

		// Output format
		if (hr == S_OK)
			{
			// Use output as guess
			CCLTRYE ( (fmt = av_guess_format(NULL,pcTmp,NULL)) != NULL, ERROR_INVALID_STATE );

			// If necessary, use default (node property ?)
			if (hr != S_OK || fmt == NULL)
//				hr = ((fmt = av_guess_format("mpeg",NULL,NULL)) != NULL) ?
				hr = ((fmt = av_guess_format("flv",NULL,NULL)) != NULL) ?
//				hr = ((fmt = av_guess_format("mp4",NULL,NULL)) != NULL) ?
//				hr = ((fmt = av_guess_format("webp",NULL,NULL)) != NULL) ?
//				hr = ((fmt = av_guess_format("webm",NULL,NULL)) != NULL) ?
						S_OK : E_UNEXPECTED;
			}	// if

		// Allocate an output format context
		CCLTRYE ( (ret = avformat_alloc_output_context2 ( &pCtxFmt, fmt, NULL, NULL ))
						>= 0, ret );
		CCLTRY ( pDct->store ( adtString(L"Format"),	adtLong((U64)pCtxFmt) ) );

		// Results
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcTmp);
		}	// if

	// Close
	else if (_RCP(Close))
		{
		adtValue		vL;
		CTXENC		ctx;

		// Grab available info
		CCLTRY ( fillContext ( pDct, &ctx ) );

		// About to close
		CCLOK ( _EMT(Close,adtIUnknown(pDct)); )

		// Clean up resources
		if (hr == S_OK && ctx.pCtxV != NULL)
			{
			avcodec_free_context(&(ctx.pCtxV));
			av_frame_free ( &(ctx.pFrmV) );
			sws_freeContext(ctx.pswsCtx);
			}	// if
		if (hr == S_OK && ctx.pCtxA != NULL)
			{
			avcodec_free_context(&(ctx.pCtxA));
			av_frame_free ( &(ctx.pFrmA) );
			av_frame_free ( &(ctx.pFrmAt) );
			swr_free(&(ctx.pswrCtx));
			}	// if

		if (hr == S_OK && ctx.pCtxFmt != NULL)
			{
			// Close stream
			avio_close(ctx.pCtxFmt->pb);

			// Free up resources
			avformat_free_context(ctx.pCtxFmt);
			}	// if

		// Clean up dictionary
		if (hr == S_OK)
			{
			// Global
			pDct->remove ( adtString(L"Format") );

			// Video
			pDct->remove ( adtString(L"CodecVideo") );
			pDct->remove ( adtString(L"ContextVideo") );
			pDct->remove ( adtString(L"StreamVideo") );
			pDct->remove ( adtString(L"FrameVideo") );
			pDct->remove ( adtString(L"ContextScale") );

			// Audio
			pDct->remove ( adtString(L"ContextAudio") );
			pDct->remove ( adtString(L"StreamAudio") );
			pDct->remove ( adtString(L"FrameAudio") );
			pDct->remove ( adtString(L"FrameAudioTemp") );
			pDct->remove ( adtString(L"CodecAudio") );
			pDct->remove ( adtString(L"ContextSample") );
			}	// if

		}	// else if

	// Add image to stream
	else if (_RCP(Fire))
		{
		IDictionary		*pDctImg = NULL;
		AVCodecContext	*pCtx		= NULL;
		AVFrame			*pFrmV	= NULL;
		SwsContext		*pswsCtx	= NULL;
		adtIUnknown		unkV(v);
		ImageDct			dctImg;
		adtValue			vL;
		CTXENC			ctx;

		// Grab available info
		CCLTRY ( fillContext ( pDct, &ctx ) );

		// Image information
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctImg) );
		CCLTRY ( dctImg.lock ( pDctImg ) );

		// Ensure context is valid
		CCLTRY ( updateContext(&ctx,pDctImg) );

		// Encode frame
		CCLTRY ( encodeV ( &ctx, dctImg.pvBits, dctImg.iW*(dctImg.iBpp/8), dctImg.iH ) );

		// Encoding audio track ?
		if (hr == S_OK && bSilence)
			hr = encodeA ( &ctx, false );

		// Clean up
		_RELEASE(pDctImg);
		}	// else if

	// Flush encoder stream of remaining data
	else if (_RCP(Flush))
		{
		IDictionary			*pDctImg = NULL;
		adtIUnknown			unkV(v);
		ImageDct				dctImg;
		adtValue				vL;
		CTXENC				ctx;

		// Grab available info
		CCLTRY ( fillContext ( pDct, &ctx ) );

		// Encode 'nothing' to flush output
		CCLTRY ( encodeV ( &ctx, NULL, 0, 0 ) );
		if (hr == S_OK && bSilence)
			hr = encodeA ( &ctx, true );

		// If writing to location, write the file trailer
		if (hr == S_OK && ctx.pCtxFmt != NULL)
			av_write_trailer(ctx.pCtxFmt);

		// Clean up
		_RELEASE(pDctImg);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown unkV(v);
		_RELEASE(pStm);
		hr = _QISAFE(unkV,IID_IByteStream,&pStm);
		}	// else if
	else if (_RCP(Location))
		hr = adtValue::toString(v,strLoc);
	else if (_RCP(Fps))
		iFps = v;
	else if (_RCP(Rate))
		iRate = v;
	else if (_RCP(Encoder))
		hr = adtValue::toString(v,strEncode);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Encode :: updateContext ( CTXENC *pEnc, IDictionary *pDctImg )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to set up encoding context using the template image.
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	pDctImg is the image dictionary
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	AVPixelFormat	fmt		= AV_PIX_FMT_NONE;
	int				ret		= 0;
	AVDictionary	*pOpts	= NULL;
	bool				bHdr		= false;
	adtValue			vL;
	ImageDct			dctImg;

	// Does context already exist ?
	if (pEnc->pCtxV != NULL)
		return hr;

	// Needed info
	CCLTRYE ( pEnc->pCodecV != NULL, ERROR_INVALID_STATE );

	// Image information
	CCLTRY ( dctImg.lock ( pDctImg ) );

	//
	// Video stream
	//

	// Create an output stream
	CCLTRYE ( (pEnc->pStmV = avformat_new_stream ( pEnc->pCtxFmt, NULL )) != NULL, E_OUTOFMEMORY );
	CCLOK   ( pEnc->pStmV->id = pEnc->pCtxFmt->nb_streams-1; )
	CCLTRY ( pDct->store ( adtString(L"StreamVideo"), adtLong((U64)pEnc->pStmV) ) );

	// Allocate an encoding context
	CCLTRYE ( (pEnc->pCtxV = avcodec_alloc_context3(pEnc->pCodecV)) != NULL,
					ERROR_OUTOFMEMORY );
	CCLTRY ( pDct->store ( adtString(L"ContextVideo"), adtLong((U64)pEnc->pCtxV) ) );
	CCLOK ( next_ptsa = next_ptsv = samples_count = 0; )

	// Fill out the video stream codec parameters
	if (hr == S_OK)
		{
		AVRational	time_base = { 1, (int)iFps };
		AVRational	framerate = { (int)iFps, 1 };

		// Codec parameters
		AVCodecParameters *param = pEnc->pStmV->codecpar;

		// Setup
		param->codec_id	= pEnc->pCodecV->id;
		param->codec_type	= AVMediaType::AVMEDIA_TYPE_VIDEO;

		// Image attributes
		param->width	= dctImg.iW;
		param->height	= dctImg.iH;

		// Desired container pixel format 420P required for 'high' H264 profile
		if (dctImg.iFmt == IMGFMT_B16G16R16)
			{
//			param->format	= AV_PIX_FMT_YUV422P10LE;
			param->format	= AV_PIX_FMT_YUV420P10LE;
			bHdr				= true;
			}	// if
		else
			param->format	= AV_PIX_FMT_YUV420P;
			
		// Bit rate determines the 'quality' of the stream
		// Default to 'high' quality, 10x over the recommended 1000*width
		if (iRate == 0)
			param->bit_rate	= param->width * 10000ll;
//			param->bit_rate	= param->width * 1000ll;
		else
			param->bit_rate	= iRate;
		param->bit_rate	= 2 * 1024 * 1024;
//		param->bit_rate	= 400000;
		param->bit_rate	= 1000000;
		param->bit_rate	= 2000000;

		// Set up the context based on parameters from stream
		avcodec_parameters_to_context ( pEnc->pCtxV, param );

		// Emit one intra frame every X frames
		pEnc->pCtxV->gop_size	= 25;
//		pEnc->pCtxV->gop_size	= 12;

		// Time base
		pEnc->pStmV->time_base	= time_base;
		pEnc->pCtxV->time_base	= pEnc->pStmV->time_base;
//		pEnc->pCtxV->framerate	= framerate;

		// Maximum number of b-frames (this add latency ?)
		pEnc->pCtxV->max_b_frames = 2;

		// H264 and other special flags
		if (param->codec_id == AV_CODEC_ID_H264)
			{
//			av_opt_set ( pEnc->pCtxV, "preset", "ultrafast", 0 );
			av_opt_set ( pEnc->pCtxV, "preset", "slow", 0 );
//			av_opt_set ( pEnc->pCtxV->priv_data, "profile", "main", 0 );
//			av_opt_set ( pEnc->pCtxV, "crf", "26", 0 );

			// Standard 'broadcast quality', requires YUV420
			if (bHdr)
//				pEnc->pCtxV->profile = FF_PROFILE_H264_HIGH_422;
				pEnc->pCtxV->profile = FF_PROFILE_H264_HIGH_10;
			else
				pEnc->pCtxV->profile = FF_PROFILE_H264_HIGH;
//			pEnc->pCtxV->profile = FF_PROFILE_H264_MAIN;
			}	// if

		// Some formats want stream headers to be separate.
		if (pEnc->pCtxFmt->oformat->flags & AVFMT_GLOBALHEADER)
			pEnc->pCtxV->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

		// Transfer parameters
		avcodec_parameters_from_context(param,pEnc->pCtxV);
		}	// if

	//
	// Audio stream
	//

	// Does graph want an audio channel of silence ?
	if (hr == S_OK && bSilence)
		{
		// Create an output stream
		CCLTRYE ( (pEnc->pStmA = avformat_new_stream ( pEnc->pCtxFmt, NULL )) != NULL, E_OUTOFMEMORY );
		CCLOK   ( pEnc->pStmA->id = pEnc->pCtxFmt->nb_streams-1; )
		CCLTRY  ( pDct->store ( adtString(L"StreamAudio"), adtLong((U64)pEnc->pStmA) ) );

		// Allocate an encoding context
		CCLTRYE ( (pEnc->pCtxA = avcodec_alloc_context3(pEnc->pCodecA)) != NULL,
						ERROR_OUTOFMEMORY );
		CCLTRY ( pDct->store ( adtString(L"ContextAudio"), adtLong((U64)pEnc->pCtxA) ) );

		// Fill out the audio stream codec parameters
		if (hr == S_OK)
			{
			AVCodecContext *pCtx = pEnc->pCtxA;

			// Fill parameters
			pCtx->sample_fmt =	(pEnc->pCodecA->sample_fmts) ? 
										pEnc->pCodecA->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;

			// Defaults for synthentic audio track
			pCtx->bit_rate		= 64000;
			pCtx->sample_rate = 44100;
			if (pEnc->pCodecA->supported_samplerates)
				{
				// Default sample rate
				pCtx->sample_rate = pEnc->pCodecA->supported_samplerates[0];
				for (int i = 0;(pEnc->pCodecA)->supported_samplerates[i];++i)
					if (pEnc->pCodecA->supported_samplerates[i] == 44100)
						pCtx->sample_rate = 44100;
				}	// if
			pCtx->channels			= av_get_channel_layout_nb_channels(pCtx->channel_layout);
			pCtx->channel_layout	= AV_CH_LAYOUT_STEREO;
			if (pEnc->pCodecA->channel_layouts)
				{
				// Default sample rate
				pCtx->channel_layout = pEnc->pCodecA->channel_layouts[0];
				for (int i = 0;(pEnc->pCodecA)->channel_layouts[i];++i)
					if (pEnc->pCodecA->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
						pCtx->channel_layout = AV_CH_LAYOUT_STEREO;
				}	// if
			pCtx->channels					= av_get_channel_layout_nb_channels(pCtx->channel_layout);
			pEnc->pStmA->time_base.num	= 1;
			pEnc->pStmA->time_base.den	= pCtx->sample_rate;
//			pEnc->pCtxA->time_base		= pEnc->pStmA->time_base;

			// Some formats want stream headers to be separate.
			if (pEnc->pCtxFmt->oformat->flags & AVFMT_GLOBALHEADER)
				pEnc->pCtxA->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;


			// Codec parameters
//			AVCodecParameters *param = pEnc->pStmA->codecpar;

			// Setup
//			param->codec_id	= pEnc->pCodecA->id;
//			param->codec_type	= AVMediaType::AVMEDIA_TYPE_AUDIO;

			// Set up the context based on parameters from stream
//			avcodec_parameters_to_context ( pEnc->pCtxA, param );

			// Audio format
//			pEnc->pCtxA->sample_fmt			= AV_SAMPLE_FMT_S16P;
//			pEnc->pCtxA->bit_rate			= 64000;
//			pEnc->pCtxA->sample_rate		= 44100;
//			pEnc->pCtxA->channels			= 1;
//			pEnc->pCtxA->channel_layout	= AV_CH_LAYOUT_MONO;
//			pEnc->pCtxA->frame_size			= pEnc->pCtxA->sample_rate;

			// Timebase based on sample rate
//			pEnc->pCtxA->time_base.num = 1;
//			pEnc->pCtxA->time_base.den = pEnc->pCtxA->sample_rate;

			// Transfer parameters
//			avcodec_parameters_from_context(param,pEnc->pCtxA);

			}	// if

		}	// if

	//
	// Video - Open
	//

	// Open the context for the video
	CCLTRYE ( (ret = av_dict_set ( &pOpts, "movflags", "faststart", 0 )) >= 0, ret );
	CCLTRYE ( (ret = avcodec_open2(pEnc->pCtxV,pEnc->pCodecV,&pOpts)) >= 0, ret );
	CCLOK ( lprintf ( LOG_DBG, L"Capabilities : 0x%x\r\n", pEnc->pCodecV->capabilities ); )

	//
	// Prepare a frame to receive the converted RGB frame and
	// sent to encoder for processing.
	//

	// Create a frame for use
	if (hr == S_OK)
		hr = ((pEnc->pFrmV = av_frame_alloc()) != NULL ) ? S_OK : E_UNEXPECTED;
	CCLTRY ( pDct->store ( adtString(L"FrameVideo"), adtLong((U64)pEnc->pFrmV) ) );
	CCLOK ( pEnc->pFrmV->pts = 0; )

	// Pre-fill frame info
	if (hr == S_OK)
		{
		// Pre-determined values
		pEnc->pFrmV->format	= pEnc->pCtxV->pix_fmt;
		pEnc->pFrmV->width	= pEnc->pCtxV->width;
		pEnc->pFrmV->height	= pEnc->pCtxV->height;

		// HDR
		if (bHdr)
			{
			pEnc->pFrmV->color_primaries	= AVCOL_PRI_BT2020;
			pEnc->pFrmV->color_trc			= AVCOL_TRC_SMPTE2084;
			pEnc->pFrmV->color_range		= AVCOL_RANGE_MPEG;
			}	// if
		}	// if

	// Allocate memory for the output buffer
	if (hr == S_OK)
		av_image_alloc ( pEnc->pFrmV->data, pEnc->pFrmV->linesize, 
								pEnc->pCtxV->width, pEnc->pCtxV->height, pEnc->pCtxV->pix_fmt, 32 );

	// Supported formats
	CCLOK		( fmt =	
					(dctImg.iFmt == IMGFMT_B8G8R8)		? AV_PIX_FMT_BGR24 :
					(dctImg.iFmt == IMGFMT_R8G8B8)		? AV_PIX_FMT_RGB24 :
					(dctImg.iFmt == IMGFMT_B8G8R8A8)		? AV_PIX_FMT_BGRA :
					(dctImg.iFmt == IMGFMT_A8B8G8R8)		? AV_PIX_FMT_ABGR :
					(dctImg.iFmt == IMGFMT_R8G8B8A8)		? AV_PIX_FMT_RGBA :
					(dctImg.iFmt == IMGFMT_A8R8G8B8)		? AV_PIX_FMT_ARGB :
					(dctImg.iFmt == IMGFMT_U8)			? AV_PIX_FMT_GRAY8 :
					// HDR
					(dctImg.iFmt == IMGFMT_B16G16R16)	? AV_PIX_FMT_RGB48LE :
					AV_PIX_FMT_NONE; )
	CCLTRYE	( (fmt != AV_PIX_FMT_NONE), E_NOTIMPL );

	// Prepare for YUV conversion
	CCLTRYE ( (pEnc->pswsCtx = sws_getContext ( pEnc->pCtxV->width, pEnc->pCtxV->height, 
					fmt, pEnc->pCtxV->width, pEnc->pCtxV->height, pEnc->pCtxV->pix_fmt,
					0, 0, 0, 0 ) ) != NULL, E_UNEXPECTED );
	CCLTRY ( pDct->store ( adtString(L"ContextScale"), adtLong((U64)pEnc->pswsCtx) ) );

	// Copy the stream parameters to the muxer
	CCLOK ( avcodec_parameters_from_context(pEnc->pStmV->codecpar, pEnc->pCtxV ); )

	//
	// Audio - Open
	//
	if (hr == S_OK && pEnc->pCtxA != NULL)
		{
		// Open the context for the audio
		CCLTRYE ( (ret = avcodec_open2(pEnc->pCtxA,pEnc->pCodecA,NULL)) >= 0, ret );

		// Samples
		int nb_samples = pEnc->pCtxA->frame_size;
		if (pEnc->pCodecA->capabilities & AV_CODEC_CAP_VARIABLE_FRAME_SIZE)
			nb_samples = 1000;

		// Create a frame for use
		CCLTRYE( ((pEnc->pFrmA = av_frame_alloc()) != NULL), E_OUTOFMEMORY );
		CCLTRY ( pDct->store ( adtString(L"FrameAudio"), adtLong((U64)pEnc->pFrmA) ) );
		if (hr == S_OK)
			{
			// Fill in known frame information
//			av_frame_make_writable(pEnc->pFrmA);
			pEnc->pFrmA->format				= pEnc->pCtxA->sample_fmt;
			pEnc->pFrmA->channel_layout	= pEnc->pCtxA->channel_layout;
			pEnc->pFrmA->sample_rate		= pEnc->pCtxA->sample_rate;
			pEnc->pFrmA->nb_samples			= nb_samples;

			// Allocate a buffer for the samples
			CCLTRYE ( (ret = av_frame_get_buffer ( pEnc->pFrmA, 0 )) >= 0, ret );

			// Create own frame for 16-bit signed samples
			CCLTRYE( ((pEnc->pFrmAt = av_frame_alloc()) != NULL), E_OUTOFMEMORY );
			CCLTRY ( pDct->store ( adtString(L"FrameAudioTemp"), adtLong((U64)pEnc->pFrmAt) ) );

			// Fill in known frame information
			if (hr == S_OK)
				{
				pEnc->pFrmAt->format				= AV_SAMPLE_FMT_S16;
				pEnc->pFrmAt->channel_layout	= pEnc->pCtxA->channel_layout;
				pEnc->pFrmAt->sample_rate		= pEnc->pCtxA->sample_rate;
				pEnc->pFrmAt->nb_samples		= nb_samples;
				}	// if

			// Allocate a buffer for the samples
			CCLTRYE ( (ret = av_frame_get_buffer ( pEnc->pFrmAt, 0 )) >= 0, ret );

			// Fill with silence
//int16_t *pi = (int16_t *)pEnc->pFrmA->data[0];
//for (int i = 0;hr == S_OK && i < nb_samples;++i)
//	*pi++ = 0;
//			CCLOK ( memset ( pEnc->pFrmA->data[0], 0, 2*nb_samples ); )
for (int j = 0; j <pEnc->pFrmAt->nb_samples; j++)
	((int16_t*)pEnc->pFrmAt->data[0])[j] = 0;

 			// Copy the stream parameters to the muxer
			CCLOK ( avcodec_parameters_from_context(pEnc->pStmA->codecpar, pEnc->pCtxA ); )

			// Create a resampler context
			CCLTRYE ( (pEnc->pswrCtx = swr_alloc()) != NULL, E_OUTOFMEMORY );
			CCLTRY ( pDct->store ( adtString(L"ContextSample"), adtLong((U64)pEnc->pswrCtx) ) );

			// Set options
			if (hr == S_OK)
				{
				av_opt_set_int       (pEnc->pswrCtx, "in_channel_count",   pEnc->pCtxA->channels,		0 );
				av_opt_set_int       (pEnc->pswrCtx, "in_sample_rate",     pEnc->pCtxA->sample_rate,	0 );
				av_opt_set_sample_fmt(pEnc->pswrCtx, "in_sample_fmt",      AV_SAMPLE_FMT_S16,				0 );
				av_opt_set_int       (pEnc->pswrCtx, "out_channel_count",  pEnc->pCtxA->channels,		0 );
				av_opt_set_int       (pEnc->pswrCtx, "out_sample_rate",    pEnc->pCtxA->sample_rate,	0 );
				av_opt_set_sample_fmt(pEnc->pswrCtx, "out_sample_fmt",     pEnc->pCtxA->sample_fmt,		0 );
				}	// if

			// Initialize resampling context
			CCLTRYE ( (ret = swr_init ( pEnc->pswrCtx )) >= 0, ret );
			}	// if

		}	// if

	//
	// Output
	//

	// Is a container file requested ?
	if (hr == S_OK && strLoc.length() > 0)
		{
		char				*pcLoc	= NULL;
		AVDictionary	*pOpts	= NULL;

		// Windows vs Linux
		#ifdef _WIN32
//		CCLOK ( strLoc.replace ( '/', '\\' ); )
		#else
//		CCLOK ( strLoc.replace ( '\\', '/' ); )
		#endif

		// ASCII for library
		CCLTRY ( strLoc.toAscii(&pcLoc) );

		// Debug
		CCLOK ( av_dump_format ( pEnc->pCtxFmt, 0, pcLoc, 1 ); )

		// Open target location
		CCLTRYE ( (ret = avio_open ( &(pEnc->pCtxFmt->pb), pcLoc, AVIO_FLAG_WRITE )) >= 0, ret );

		// Clean up
		_FREEMEM(pcLoc);
		}	// if

	// Write the header
	CCLTRYE ( (ret = avformat_write_header ( pEnc->pCtxFmt, NULL )) >= 0, ret );

	// Debug
	if (hr != S_OK)
		{
		char bfr[255];
		av_strerror(ret,bfr,sizeof(bfr));
		lprintf ( LOG_DBG, L"hr 0x%x (%d:%S)", hr, ret, bfr );
		}	// if

	return hr;
	}	// updateContext

HRESULT Encode :: writePackets ( CTXENC *pEnc, bool bV )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Transfers any encoded data into destination
	//
	//	PARAMETERS
	//		-	pEnc is the encoder context
	//		-	bV true means video, false means audio
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	bool		bUp	= false;
	AVPacket	pkt	= { 0 };
	int		ret;

	// State check
	CCLTRYE ( pEnc->pCtxV != NULL, ERROR_INVALID_STATE );

	// Active context and stream
	AVCodecContext *
	pavCtx	= (bV) ? pEnc->pCtxV : pEnc->pCtxA;
	AVStream *
	pavStm	= (bV) ? pEnc->pStmV : pEnc->pStmA;

	// State check
	if (pavCtx == NULL || pavStm == NULL)
		return hr;

	// See if there is any encoding data
	while (hr == S_OK)
		{
		// Output buffer info
		av_init_packet(&pkt);
//		pkt.flags |= AV_PKT_FLAG_KEY;
		ret = avcodec_receive_packet ( pavCtx, &pkt );

		// EAGAIN or EOF is fine, just means no data
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			break;

		// Other error ?
		hr = (ret >= 0) ? S_OK : S_FALSE;

		// Graph specified own raw stream
		if (pStm != NULL)
			{
			// Write data to stream
			CCLTRY ( pStm->write ( pkt.data, pkt.size, NULL ) );
			CCLOK ( bUp = true; )
			}	// if

		// Graph specified container
		else if (pEnc->pCtxFmt != NULL)
			{
			// Debug
//			lprintf ( LOG_DBG, L"%d) pts %d dts %d\r\n", (bV) ? 0 : 1, pkt.pts, pkt.dts );

			// Rescale output packet timestamp values from codec to stream timebase
			if (bV)
				{
				av_packet_rescale_ts ( &pkt, pEnc->pCtxV->time_base, pEnc->pStmV->time_base );
//				av_packet_rescale_ts ( &pkt, pEnc->pStmV->codec->time_base, pEnc->pStmV->time_base );
				pkt.stream_index = pEnc->pStmV->index;
				}	// if
			else
				{
				av_packet_rescale_ts ( &pkt, pEnc->pCtxA->time_base, pEnc->pStmA->time_base );
//				av_packet_rescale_ts ( &pkt, pEnc->pStmA->codec->time_base, pEnc->pStmA->time_base );
				pkt.stream_index = pEnc->pStmA->index;
				}	// else

			// Debug
//			#if 0
			lprintf ( LOG_DBG, L"Packet :\r\n" );
			lprintf ( LOG_DBG, L"Stream           : %s (%d)\r\n", (bV) ? L"video" : L"audio", pkt.stream_index );
			lprintf ( LOG_DBG, L"Data             : %p\r\n", pkt.data );
			lprintf ( LOG_DBG, L"Size             : %d\r\n", pkt.size );
			lprintf ( LOG_DBG, L"Decompression ts : %d\r\n", pkt.dts );
			lprintf ( LOG_DBG, L"Presentation ts  : %d\r\n", pkt.pts );
			lprintf ( LOG_DBG, L"Flags            : 0x%x\r\n", pkt.flags );
			lprintf ( LOG_DBG, L"Duration         : 0x%x\r\n", pkt.duration );
			lprintf ( LOG_DBG, L"Position         : 0x%x\r\n", pkt.pos );
//			#endif

			// Write the frame
			ret = av_interleaved_write_frame ( pEnc->pCtxFmt, &pkt );
			if (ret < 0)
				lprintf ( LOG_DBG, L"av_interleaved_write_frame failed %d\r\n", ret );			
			}	// else if

		// Done w/packet
		av_packet_unref(&pkt);
		}	// while

	// Let outside world know if stream was updated
	if (bUp && pStm != NULL)
		_EMT(Fire,adtIUnknown(pStm));

	return hr;
	}	// writePackets
*/

