////////////////////////////////////////////////////////////////////////
//
//									CCL.H
//
//			Include file for the COM Compatibility Layer library
//
////////////////////////////////////////////////////////////////////////

#ifndef	CCL_H
#define	CCL_H

// Includes
#include	"../sysl/sysl.h"

// COM implementation
#define	CCL_OBJLIST_BEGIN()			CCLENTRY cclobjlist[] = {
#ifdef	_WIN32
#define	CCL_OBJLIST_ENTRY(a)			{ a::CreateObject, CCL_OBJ_MODULE L"." L#a },
#else
#define	CCL_OBJLIST_ENTRY(a)			{ a::CreateObject, CCL_OBJ_MODULE L"." L## #a },
#endif
#define	CCL_OBJLIST_END()				{ NULL, NULL } };															\
												CCL_DLLMAIN()
#define	CCL_OBJLIST_END_EX(a)		{ NULL, NULL } };															\
												CCL_DLLMAIN_EX(a)
#define	CCL_OBJLIST_END_NOTDLL()	{ NULL, NULL } };
#define	CCL_OBJECT_BEGIN_INT(a)		public :																		\
												STDMETHOD_(ULONG,AddRef)	( void )									\
													{ return CCLObject::AddRef(); }									\
												STDMETHOD(QueryInterface)	( REFIID iid, void **ppv )			\
													{ return CCLObject::QueryInterface(iid,ppv); }				\
												STDMETHOD_(ULONG,Release)	( void )									\
													{ return CCLObject::Release(); }									\
												STDMETHOD(InnerQueryInterface)( REFIID iid, void **ppv )		\
													{ return _InnerQueryInterface(_getIntfs(),iid,ppv ); }	\
												private :																	\
												typedef a _intf_class;													\
												/** \brief Returns ptr to the supported interfaces table */	\
												const static CCLINTF *_getIntfs() {									\
												static const CCLINTF _intfs[] = {
#define	CCL_OBJECT_BEGIN(a)			static CCLObject *CreateObject ( void )							\
													{ return new (a); }													\
												CCL_OBJECT_BEGIN_INT(a)
												// Note, this v just gets offset of interface in class
#define	CCL_INTF(a)						{ &IID_##a, ((UINT_PTR)(static_cast<a*>((_intf_class *)8))-8), 1 },
#define	CCL_INTF_FROM(a,b)			{ &IID_##a, ((UINT_PTR)(static_cast<a*>(static_cast<b *>((_intf_class *)8)))-8), 1 },
#define	CCL_INTF_AGG(a,b)				{ &IID_##a, ((UINT_PTR)(&((static_cast<_intf_class *>((void *)8))->b)) - 8), 2 },
#define	CCL_OBJECT_END()				{ NULL, 0 } }; return _intfs; }										\
												public :

// Helper macros
#define	CCLTRY(a)						if (hr == S_OK) hr = a
#define	CCLTRYE(a,b)					if (hr == S_OK && !((a))) hr = (b)
#define	CCLOK(a)							if (hr == S_OK) { a }

#define	_ADDREF(a)						if ((a) != NULL) (a)->AddRef();
#define	_RELEASE(a)						if ((a) != NULL) { (a)->Release(); (a) = NULL; }
#define	_QI(a,b,c)						(a)->QueryInterface ( (b), (void **) (c) )
#define	_QISAFE(a,b,c)					((IUnknown *)(NULL) != (IUnknown *)(a)) ? \
												((IUnknown *)(a))->QueryInterface ( (b), (void **) (c) ) : E_UNEXPECTED;

// COM Creation
#define	COCREATE(a,b,c)				cclCreateObject ( a, NULL, (b), (void **) (c) )
#define	COCREATEA(a,b,c)				cclCreateObject ( a, (b), IID_IUnknown, (void **) (c) )

// Modules
#ifdef	_WIN32
#define	CCL_DLLMAIN()					EXTERN_C BOOL WINAPI DllMain ( HANDLE _hInst, DWORD dwReason, void * )	\
													{ return cclDllMain ( _hInst, dwReason ); }
#define	CCL_DLLMAIN_EX(a)				BOOL WINAPI	DllMain ( HANDLE _hInst, DWORD dwReason, void * )				\
													{ if (! (a) (_hInst, dwReason) || !cclDllMain ( _hInst, dwReason ))	\
														return FALSE; else return TRUE; }
#else
#define	CCL_DLLMAIN()
#define	CCL_DLLMAIN_EX(a)
#endif

#ifdef __cplusplus

//
//	Structure - CCLENTRY.  An entry for a list of objects in a module.
//
class CCLObject;											// Forward dec.
typedef struct tagCCLENTRY
	{
	CCLObject *		(*create)();						// Creation function
	const WCHAR 	*progid;								// Prog ID of class
	} CCLENTRY;

//
// Structure - CCLINTF.  An interface.
//

typedef struct tagCCLINTF
	{
	const	IID	*piid;									// Interface ID ptr.
	UINT_PTR		uip;										// Interface ptr.
	U32			itype;									// (uip) 0 = end, 1 = offset, 2 = pointer
	} CCLINTF;

//
// Interface - IInnerUnknown.  Ptr. to the inner unknown of an object.
//

class IInnerUnknown
	{
	public :
	STDMETHOD(InnerQueryInterface)	( REFIID, void ** )	PURE;
	STDMETHOD_(ULONG,InnerAddRef)		( void )					PURE;
	STDMETHOD_(ULONG,InnerRelease)	( void )					PURE;

	// GCC gets upset without a virtual destructor even though its a pure virtual interface
	#ifdef	__GNUC__
	virtual ~IInnerUnknown				( void ) {}
	#endif
	};

// Forward declarations
class		CCLFactory;
struct	IDictionary;

//
// Class - CCLObject.  Base class for a COM object.
//

class CCLObject :
	protected IUnknown,									// Interface
	public IInnerUnknown									// Interface
	{
	public :
	CCLObject				( void );					// Constructor
	virtual ~CCLObject	( void );					// Destructor

	// 'CCLObject' members
	virtual HRESULT	construct	( void );		// Construct object
	virtual void		destruct		( void );		// Destruct object
	virtual HRESULT	cclRegister	( bool );		// Object (un)registration

	// 'IInnerUnknown' members
	STDMETHOD_(ULONG,InnerAddRef)		( void );
	STDMETHOD(InnerQueryInterface)	( REFIID, void ** ) = 0;
	STDMETHOD_(ULONG,InnerRelease)	( void );

	protected :

	// 'IUnknown' members
	STDMETHOD_(ULONG,AddRef)	( void );
	STDMETHOD(QueryInterface)	( REFIID iid, void **ppv );
	STDMETHOD_(ULONG,Release)	( void );

	public :

	// Operators
//	void *operator new		( size_t );
//	void operator	delete	( void * );

// Operator overload to use global memory manager
//#define			_NEW			new
//#define			_DELETE(a)	delete a
//void *operator new		( size_t );
//void operator	delete	( void * );
//void *operator new[]		( size_t );
//void operator	delete[]	( void * );

//wchar_t	*sysStringAlloc		( const wchar_t * );
//wchar_t	*sysStringAllocLen	( const wchar_t *, U32 );
//void		sysStringFree			( wchar_t * );


	protected :

	// Run-time data
	IUnknown		*unkOuter;								// Ptr. to outer unknown

	// Utilities
	HRESULT	_InnerQueryInterface	( const CCLINTF *, REFIID, void ** );

	public :
	LONG		objidx;										// Object number (debug)

	private :
	LONG		refcnt;										// Reference count ob object
	friend	class CCLFactory;							// Friend class
	};

//
// Class - CCLFactory.  Class factory, creates objects.
//

class CCLFactory :
	public CCLObject,										// Base class
	public IClassFactory									// Interface
	{
	public :
	CCLFactory ( void );									// Constructor
	CCLFactory ( const WCHAR * );						// Constructor

	// Run-time data
	U32		objidx;										// Index of object into CCLENTRY table

	// 'IClassFactory' members
	STDMETHOD(CreateInstance)	( IUnknown *, REFIID, void ** );
	STDMETHOD(LockServer)		( BOOL );

	private :

	// CCL
	CCL_OBJECT_BEGIN(CCLFactory)
		CCL_INTF(IClassFactory)
	CCL_OBJECT_END()
	};

// Prototypes
BOOL		cclDllMain	( HANDLE, U32 );
extern "C"
HRESULT DLLEXPORT cclAddLibrary		( const wchar_t *, U64 );
extern "C"
HRESULT DLLEXPORT cclCreateObject	( const WCHAR *, IUnknown *, REFIID, void ** );
extern "C"
HRESULT DLLEXPORT cclGetFactory 		( const WCHAR *, REFIID, void ** );
extern "C"
HRESULT DLLEXPORT cclGetClassObject ( const WCHAR *, REFIID, void ** );
extern "C"
HRESULT DLLEXPORT cclInitialize		( void *, DWORD );
extern "C"
void	  DLLEXPORT	cclUninitialize	( void );
#endif
#endif

