////////////////////////////////////////////////////////////////////////
//
//									CCL.CPP
//
//					Main file for the COM Compatibility Layer
//
////////////////////////////////////////////////////////////////////////

#include "ccl.h"
#include "../adtl/adtl.h"
#if      __unix__ || __APPLE__
#include <libgen.h>
#include <dlfcn.h>
#endif

// Globals
extern		CCLENTRY		cclobjlist[];				// Objects for module
HINSTANCE	ccl_hInst	= NULL;						// Module handle
U32			dwLockCount = 0;							// Module lock count
U32			dwObjCount	= 0;							// Object reference count
LONG			dwCtd[255];									// Constructed
LONG			dwDtd[255];									// Destructed

// Prototypes
STDAPI cclRegister	( CLSID, const WCHAR *, bool );

BOOL cclDllMain ( HANDLE _hInst, U32 dwReason )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	CCL Entry point into DLL
	//
	//	PARAMETERS
	//		-	_hInst is a handle to the module
	//		-	dwReason specifies the reason for calling this function
	//
	//	RETURN VALUE
	//		TRUE if successful
	//
	////////////////////////////////////////////////////////////////////////
	if			(dwReason == DLL_PROCESS_ATTACH)
		{
		ccl_hInst = (HINSTANCE) _hInst;
		memset ( dwCtd, 0, sizeof(dwCtd) );
		memset ( dwDtd, 0, sizeof(dwDtd) );
		}	// if
	else if	(dwReason == DLL_PROCESS_DETACH)
		{
		U32	i,j = 0;
		#ifdef	_WIN32
		WCHAR	dllname[MAX_PATH];
		GetModuleFileName ( ccl_hInst, dllname, sizeof(dllname)/sizeof(WCHAR) );
		#elif	__unix__ || __APPLE__
		char 	dllname[1024];
		i = readlink ( "/proc/self/exe", dllname, sizeof(dllname) );
		#endif
		for (i = 0;i < (sizeof(dwCtd)/sizeof(LONG))-1;++i)
			{
			if (dwCtd[i] != dwDtd[i])
				{
				#ifdef	_WIN32
				if (!j) { dbgprintf ( L"** MISMATCH:%s:", dllname ); j = 1; }
				#elif	__unix__ || __APPLE__
				if (!j) { dbgprintf ( L"** MISMATCH:" ); j = 1; }
				#endif
				dbgprintf ( L"(%s(%d),%d(+%d,-%d))", cclobjlist[i].progid, i, 
								dwCtd[i]-dwDtd[i], dwCtd[i], dwDtd[i] );
				}	// if
			else
				{
				#ifdef	_WIN32
//				dbgprintf ( L"Unload:%s:%d objects", dllname, dwCtd[i] );
				#else
//				dbgprintf ( L"Unload:%d objects", dwCtd[i] );
				#endif
				}	// else
			}	// for
		if (j) dbgprintf ( L"\r\n" );
		}	// else if

	return TRUE;
	}	// cclDllMain

extern "C"
DLLEXPORT
HRESULT cclGetClassObject ( const WCHAR *pwId, REFIID refiid, void **ppv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called by the operating system to grab a class factory for
	//			the specified object.
	//
	//	PARAMETERS
	//		-	pwId is the class Id
	//		-	refiid specifies the interface on the class factory
	//		-	ppv will receive a ptr. to the factory
	//
	//	RETURN VALUE
	//		S_OK if ok to be unloaded
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Known factory capabilities
	if (	!IsEqualGUID ( refiid, IID_IUnknown ) &&
			!IsEqualGUID ( refiid, IID_IClassFactory ) )
		return E_NOINTERFACE;

	// Factory for object
	CCLFactory *
	factory = new CCLFactory ( pwId );
	_ADDREF(factory);

	// Valid object ?
	if (factory != NULL && factory->objidx != 0xffffffff)
		{
		// Class factory interface
		IClassFactory	*
		fct = factory;

		// Result
		fct->AddRef();
		(*ppv) = fct;
		}	// if
	else
		hr = CLASS_E_CLASSNOTAVAILABLE;

	// Clean up
	_RELEASE(factory);

	return hr;
	}	// cclGetClassObject

// Linux specific stuff

#if	__unix__ || __APPLE__

// Simulate DLL load/unload cycle in Linux
#include <stdio.h>

void __attribute__ ((constructor))	dll_init(void)
	{
	cclDllMain(0,DLL_PROCESS_ATTACH);
	}
	
void __attribute__  ((destructor)) dll_uninit(void)
	{
	cclDllMain(0,DLL_PROCESS_DETACH);
	}

// Gcc has linker problems without this defined using pure functions...

extern "C"
void __pure_virtual()
	{
	}	// __pure_virtual

#endif
