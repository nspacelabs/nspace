////////////////////////////////////////////////////////////////////////
//
//										NSHXL.H
//
//			Include file for the external nSpace client library.
//
////////////////////////////////////////////////////////////////////////

#ifndef	NSHXL_H
#define	NSHXL_H

// In order the for includes to not give errors
#ifndef _WINSOCKAPI_
#define _WINSOCKAPI_
#endif

// nSpace
#include "../netl/netl.h"

// Generated .H from IDL file
#include	"../nshl/nshl.h"

// Operations for async client
#define	OP_LOAD						1
#define	OP_STORE						2
#define	OP_LISTEN					3
#define	OP_UNLISTEN					4

// Networking
#define	PORT_NSPC_TCP				62831

// Forward decs.
class nSpaceClientCB;
class nSpaceClientTcp;
class ShellX;

#ifdef _WIN32

//
// Class - nSpaceClient.  An nSpace client object to communicate
//		with the ActiveX nSpace object.
//

class nSpaceClient
	{
	public :
	nSpaceClient ( void );								// Constructor
	virtual ~nSpaceClient ( void );					// Destructor

	// Run-time data
	DWORD							dwSvc;					// Service Id in global interface table
	IGlobalInterfaceTable	*pGIT;					// Global interface table
	IDictionary					*pLstns;					// Listen dictionary
	nSpaceClientCB				*pCB;						// Default callback
	const WCHAR					*pwRoot;					// Default root
	adtVariant					varS,varL;				// Variant helper
	sysCS							csMtx;					// Thread mutex
	BOOL							bDirect;					// Direct namespace interface							
	IShellX						*pShD;					// Direct namespace shell
	INamespaceX					*pSpcD;					// Direct namespace

	// Utilities
	STDMETHOD(close)	( void );						// Close connection
	STDMETHOD(load)	( const WCHAR *,				// Get value from namespace
								ADTVALUE & );
	STDMETHOD(listen)	( const WCHAR *, BOOL,		// Listen to path
								nSpaceClientCB * = NULL );
	STDMETHOD(open)	( const WCHAR *, BOOL, BOOL,
																// Open connection to namespace
								nSpaceClientCB * = NULL );
	STDMETHOD(root)	( const WCHAR * );			// Set default root location
	STDMETHOD(store)	( const WCHAR *,				// Put value into namespace
								const ADTVALUE & );

	// Internal utilities
	STDMETHOD(pathCreate)	( const WCHAR *,		// Allocate/create full path
										BSTR * );					

	private :

	// Internal utilities
	STDMETHOD(getSvc)	( INamespaceX ** );			// Obtain service ptr.
	};
	
#endif

//
// Class - nSpaceClientCB.  Callback object for received values.
//

class nSpaceClientCB
	{
	public :
	// A listened path has been received
	STDMETHOD(onReceive)	( const WCHAR *, const WCHAR *, const ADTVALUE & )	PURE;
	};

#ifdef _WIN32
	
/// \cond
//
// Class - nSpaceClientL.  Internal listening object.
//

class nSpaceClientL :
	public CCLObject,										// Base class
	public IListenX										// Interface
	{
	public :
	nSpaceClientL ( nSpaceClient *,					// Constructor
							nSpaceClientCB * );	

	// Run-time data
	nSpaceClient	*pThis;								// Parent object
	nSpaceClientCB	*pCB;									// Callback object
	adtVariant		varL;									// Variant helper
	sysCS				csRx;									// Thread mutex

	// 'IListenX' members
	STDMETHOD(receive)			( BSTR, BSTR, VARIANT * );

	// CCL
	CCL_OBJECT_BEGIN_INT(nSpaceClientL)
		CCL_INTF(IListenX)
	CCL_OBJECT_END()
	};

/// \endcond
#endif

//
// Class - nSpaceTcpCB.  Callback object for TCP client.
//

class nSpaceTcpCB
	{
	public :
	// Connection has been established with remote system
	STDMETHOD(onConnect)	( void )	PURE;

	// Connection lost to remote system
	STDMETHOD(onDisconnect)	( void )	PURE;
	};
	
//
// Class - nSpaceClientTcp.  An nSpace client object to communicate
//		with a running namespace over TCP/IP.
//

class nSpaceClientTcp :
	public CCLObject,										// Base class
	public ITickable,										// Interface
	private IByteStream									// Interface
	{
	public :
	nSpaceClientTcp ( void );							// Constructor
	virtual ~nSpaceClientTcp ( void );				// Destructor

	// Run-time data
	IThread			*pThrd;								// Worker thread
	bool				bRun;									// Running ?
	IDictionary		*pLstns;								// Listen dictionary
	IDictionary		*pDctSt,*pDctLd,*pDctLs;		// Run-time dictionaries
	IStreamPersist	*pPrs;								// Parser
	nSpaceClientCB	*pCB;									// Default callback
	nSpaceTcpCB		*pCBTcp;								// TCP callback
	adtString		strAddr;								// Connection address

	// Utilities
	STDMETHOD(close)	( void );						// Close connection
	STDMETHOD(load)	( const WCHAR *,				// Get value from namespace
								ADTVALUE & );
	STDMETHOD(listen)	( const WCHAR *, BOOL,		// Listen to path
								nSpaceClientCB * = NULL );
	STDMETHOD(open)	( const WCHAR *,				// Open connection to namespace
								nSpaceTcpCB * = NULL,
								nSpaceClientCB * = NULL );
	STDMETHOD(store)	( const WCHAR *,				// Put value into namespace
								const ADTVALUE & );

	// 'ITickable' members
	STDMETHOD(tick)		( void );
	STDMETHOD(tickAbort)	( void );
	STDMETHOD(tickBegin)	( void );
	STDMETHOD(tickEnd)	( void );

	// 'IByteStream' members
	STDMETHOD(available)	( U64 * );
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * );
	STDMETHOD(flush)		( void );
	STDMETHOD(read)		( void *, U64, U64 * );
	STDMETHOD(seek)		( S64, U32, U64 * );
	STDMETHOD(setSize)	( U64 );
	STDMETHOD(write)		( void const *, U64, U64 * );

	// CCL
	CCL_OBJECT_BEGIN_INT(nSpaceClientTcp)
		CCL_INTF(ITickable)
	CCL_OBJECT_END()

	protected :

	// Run-time data
	UINT_PTR	sktCli;										// Client socket

	private :

	// Internal utilities
	void		disconnect	( void );
	HRESULT	onLoad		( const ADTVALUE & );

	};

//
// Class - nSpaceClientAsync.  An asynchronous 'pulling' version of
//		the nSpace client.  Useful for environments that cannot handle
//		callbacks well or at all and has weird threading issues (e.g. C#).
//

class nSpaceClientAsync :
	public CCLObject,										// Base class
	public nSpaceClientCB,								// Base class
	public ITickable										// Interface
	{
	public :
	nSpaceClientAsync ( void );						// Constructor
	virtual ~nSpaceClientAsync ( void );			// Destructor

	// Run-time data
	IThread			*pThrd;								// Worker thread
	IList				*pQWrk;								// Work queue
	IIt				*pQWrkIt;							// Work queue iterator
	sysEvent			evWork;								// Work event
	IList				*pQVal;								// Value queue
	IIt				*pQValIt;							// Value queue iterator
	sysCS				csVal;								// Queue protection
	bool				bRun;									// Running ?
	adtString		strCmdLine;							// Specified command line
	BOOL				bShare;								// Share namespace ?
	BOOL				bDirect;								// Direct namespace ?
	IMemoryMapped	*pPerMemLd,*pPerMemSv;			// Binary persistence
	IStreamPersist	*pPerBinLd,*pPerBinSv;			// Binary persistence
	IByteStream		*pPerStmLd,*pPerStmSv;			// Binary persistence
	bool				bTCP;									// Use TCP connection ?
	nSpaceTcpCB		*pcbTCP;								// TCP callbacks

	// Utilities
	STDMETHOD(close)		( void );					// Close connection
	STDMETHOD(load)		( const WCHAR * );		// Get value from namespace
	STDMETHOD(listen)		( const WCHAR *, BOOL );// Listen to path
	STDMETHOD(open)		( const WCHAR *, BOOL,	// Open connection to namespace
									BOOL );
	STDMETHOD(open)		( const WCHAR *,			// Open connection to namespace
									nSpaceTcpCB * = NULL );
	STDMETHOD(received)	( adtString &,				// Read a received value
									adtString &, ADTVALUE & );
	STDMETHOD(received)	( adtString &,				// Read a received value
									adtString &, BYTE **, U32 * );
	STDMETHOD(store)		( const WCHAR *,			// Put value into namespace
									const ADTVALUE & );

	// 'ITickable' members
	STDMETHOD(tick)		( void );
	STDMETHOD(tickAbort)	( void );
	STDMETHOD(tickBegin)	( void );
	STDMETHOD(tickEnd)	( void );

	// 'nSpaceClientCB' memebers
	STDMETHOD(onReceive)	( const WCHAR *, const WCHAR *, const ADTVALUE & );

	// CCL
	CCL_OBJECT_BEGIN_INT(nSpaceClientAsync)
		CCL_INTF(ITickable)
	CCL_OBJECT_END()

	private :

	// Run-time data
	#ifdef _WIN32
	nSpaceClient		*pnCliD;							// Contained nSpace client
	#endif
	nSpaceClientTcp	*pnCliT;							// Contained nSpace client
	};

#endif
