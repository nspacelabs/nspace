////////////////////////////////////////////////////////////////////////
//
//								CLIENT.CPP
//
//					nSpace shell external client
//
////////////////////////////////////////////////////////////////////////

#include	"nshxl.h"

nSpaceClientAsync :: nSpaceClientAsync ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pnCliT		= NULL;
	#ifdef _WIN32
	pnCliD		= NULL;
	#endif
	pThrd			= NULL;
	pQWrk			= NULL;
	pQWrkIt		= NULL;
	bRun			= false;
	pQVal			= NULL;
	pQValIt		= NULL;
	pPerMemLd	= NULL;
	pPerMemSv	= NULL;
	pPerBinLd	= NULL;
	pPerBinSv	= NULL;
	pPerStmLd	= NULL;
	pPerStmSv	= NULL;
	bShare		= FALSE;
	bDirect		= FALSE;
	bTCP			= false;
	pcbTCP		= NULL;
	AddRef();
	}	// nSpaceClientAsync

nSpaceClientAsync :: ~nSpaceClientAsync()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	close();
	}	// ~nSpaceClientAsync

HRESULT nSpaceClientAsync :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Close connection to namespace.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Shutdown the worker thread
	lprintf ( LOG_DBG, L"pThrd %p", pThrd );
	if (pThrd != NULL)
		{
		pThrd->threadStop(30000);
		_RELEASE(pThrd);
		}	// if

	return hr;
	}	// close

HRESULT nSpaceClientAsync :: listen ( const WCHAR *szPath, BOOL bL )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Listen/unlisten to an emitter in the namespace.
	//
	//	PARAMETERS
	//		-	szPath is the namespace path
	//		-	bL is TRUE to listen, FALSE to unlisten
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// State check
	CCLTRYE ( pQWrk != NULL, ERROR_INVALID_STATE );

	// Debug
	lprintf ( LOG_DBG, L"%s %d {", szPath, bL );

	// Add request to worker queue
	CCLTRY ( pQWrk->write(adtInt((bL) ? OP_LISTEN : OP_UNLISTEN)) );
	CCLTRY ( pQWrk->write(adtString(szPath)) );

	// Work to do...
	evWork.signal();

	// Debug
	lprintf ( LOG_DBG, L"} %d", hr );

	return hr;
	}	// listen

HRESULT nSpaceClientAsync :: load ( const WCHAR *szPath )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Get a value from the namespace.  The asynchronous version will
	//			make loaded values available through the value queue.
	//
	//	PARAMETERS
	//		-	szPath is the namespace path
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// State check
	CCLTRYE ( pQWrk != NULL, ERROR_INVALID_STATE );

	// Debug
	lprintf ( LOG_DBG, L"%s {", szPath );

	// Add request to worker queue
	CCLTRY ( pQWrk->write(adtInt(OP_LOAD)) );
	CCLTRY ( pQWrk->write(adtString(szPath)) );

	// Work to do...
	evWork.signal();

	// Debug
	lprintf ( LOG_DBG, L"} %d", hr );

	return hr;
	}	// load

HRESULT nSpaceClientAsync :: onReceive (	const WCHAR *pwRoot,
														const WCHAR *pwLoc,
														const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	A value on a listened path has been received.
	//
	//	PARAMETERS
	//		-	pwRoot is the root location
	//		-	pwLoc is the namepsace location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtString	strRoot(pwRoot),strLoc(pwLoc);

	// State check
	if (pQVal == NULL)
		return S_OK;

	// Ensure ownership since copying (necessary ?)
	strRoot.at(); strLoc.at();

	// Add to value queue
	csVal.enter();
	pQVal->write ( strRoot );
	pQVal->write ( strLoc );
	pQVal->write ( v );
	csVal.leave();

	return S_OK;
	}	// onReceive

HRESULT nSpaceClientAsync :: open ( const WCHAR *pwCmdLine, BOOL _bShare,
												BOOL _bDirect )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Open a connection to an nSpace namespace.
	//
	//	PARAMETERS
	//		-	pCmdLine is the command line to use for the namespace
	//		-	_bShare is true to open an existing or create a new service or 
	//			false to create a private one.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Debug
	lprintf ( LOG_DBG, L"%s %d {", pwCmdLine, bShare );

	// State check
	CCLTRYE ( (pThrd == NULL), E_UNEXPECTED );

	// Startup parameters
	strCmdLine	= pwCmdLine;
	bShare		= _bShare;
	bDirect		= _bDirect;
	bTCP			= false;
	pcbTCP		= NULL;

	// Start worker thread
	CCLOK  ( bRun = true; )
	CCLTRY ( COCREATE ( L"Sys.Thread", IID_IThread, &pThrd ) );
	CCLTRY ( pThrd->threadStart ( this, 50000 ) );

	// If there was a startup error, bRun will be false
	CCLTRYE ( bRun == true, E_UNEXPECTED );

	// Debug
	lprintf ( LOG_INFO, L"nSpaceClientAsync::open:0x%x:%s\r\n", hr, pwCmdLine );

	return hr;
	}	// open

HRESULT nSpaceClientAsync :: open ( const WCHAR *pwOpts, nSpaceTcpCB *_pCBTcp )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Open a TCP connection to an nSpace namespace.
	//
	//	PARAMETERS
	//		-	pwOpts are a list of options
	//		-	_pCBTcp is the callback interface for the Tcp connection.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Debug
	lprintf ( LOG_DBG, L"%s %p {", pwOpts, _pCBTcp );

	// State check
	CCLTRYE ( (pThrd == NULL), E_UNEXPECTED );

	// Startup parameters
	strCmdLine	= pwOpts;
	bTCP			= true;
	pcbTCP		= _pCBTcp;

	// Start worker thread
	CCLOK  ( bRun = true; )
	CCLTRY ( COCREATE ( L"Sys.Thread", IID_IThread, &pThrd ) );
	CCLTRY ( pThrd->threadStart ( this, 50000 ) );

	// If there was a startup error, bRun will be false
	CCLTRYE ( bRun == true, E_UNEXPECTED );

	// Debug
	lprintf ( LOG_INFO, L"nSpaceClientAsync::open:0x%x:%s\r\n", hr, pwOpts );

	return hr;
	}	// open

HRESULT nSpaceClientAsync :: received	(	adtString &strRoot,
														adtString &strLoc,
														ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Read the oldest value from the value queue.
	//
	//	PARAMETERS
	//		-	strRoot will receive the root location
	//		-	strLoc will receive the namepsace location
	//		-	v will receive the false
	//
	//	RETURN VALUE
	//		S_OK if successful, S_FALSE if there are no values.
	//
	////////////////////////////////////////////////////////////////////////
	adtValue		vL;
	U32			sz;

	// In case thread has shutdown due to error
	if (pQVal == NULL)
		return S_FALSE;

	// There must be at least three values for a valid tuple
	if (pQVal->size(&sz) != S_OK || sz < 3)
		return S_FALSE;

	// Read value from queue
	csVal.enter();

	pQValIt->read ( vL );
	pQValIt->next();
	adtValue::toString(vL,strRoot);

	pQValIt->read ( vL );
	pQValIt->next();
	adtValue::toString(vL,strLoc);

	pQValIt->read ( vL );
	pQValIt->next();
	adtValue::copy ( vL, v );

	csVal.leave();

	return S_OK;
	}	// received

HRESULT nSpaceClientAsync :: received	(	adtString &strRoot,
														adtString &strLoc,
														BYTE **ppb, U32 *psz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Read the oldest value from the value queue.  This variation
	//			is used when the caller would prefer the value be persisted
	//			to byte memory.
	//
	//	PARAMETERS
	//		-	strRoot will receive the root location
	//		-	strLoc will receive the namepsace location
	//		-	ppb will receive a ptr. to the persisted value, the ptr.
	//			is owned by this object.
	//		-	psz will receive the number of bytes persisted to memory
	//
	//	RETURN VALUE
	//		S_OK if successful, S_FALSE if there are no values.
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtValue		vL;
	U64			pos;

	// Any values ?
	if ((hr = received ( strRoot, strLoc, vL )) != S_OK)
		return hr;

	// Prepare stream for receiving data
	CCLTRY ( pPerStmSv->setSize(0) );

	// Save value to stream
	CCLTRY ( pPerBinSv->save ( pPerStmSv, vL ) );

	// Current position of stream will be count
	CCLTRY ( pPerStmSv->seek(0,STREAM_SEEK_CUR,&pos) );

	// Results ptr. NOTE: Keeping locked due to knowledge of how object works.
	CCLTRY( pPerMemSv->getInfo((void **)ppb,NULL) );
	CCLOK ( *psz = (U32)pos; )

	return hr;
	}	// received

HRESULT nSpaceClientAsync :: store ( const WCHAR *szPath, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Store a value into the namespace.
	//
	//	PARAMETERS
	//		-	szPath is the namespace path
	//		-	v is the value to put into the namespace.
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	adtString	strPath(szPath);

	// Debug
//	lprintf ( LOG_DBG, L"%s {", szPath );

	// State check
	if (pQWrk == NULL)
		return ERROR_INVALID_STATE;

	// Own string since to ensure validity
	strPath.at();

	// Add request to worker queue
	CCLTRY ( pQWrk->write(adtInt(OP_STORE)) );
	CCLTRY ( pQWrk->write(strPath) );
	CCLTRY ( pQWrk->write(v) );

	// Work to do...
	evWork.signal();

	// Debug
//	lprintf ( LOG_DBG, L"} 0x%x", hr );

	return hr;
	}	// store

HRESULT nSpaceClientAsync :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtValue	vOp,vLoc,vVal;

	// Wait for work
	evWork.wait(-1);

	// Keep running ?
	CCLTRYE ( (bRun == true), S_FALSE );

	// Process work
	while (hr == S_OK && pQWrk->isEmpty() == S_FALSE)
		{
		U32		sz	= 0;
		adtInt	iOp;

		// Op code and size
		CCLTRY(pQWrkIt->read(vOp));
		CCLTRY(pQWrk->size(&sz));

		// Load
		if ((iOp = adtInt(vOp)) == OP_LOAD)
			{
			adtString strLoc;

			// Enough parameters ?
			if (sz < 2)
				break;

			// Location to load
			CCLOK (pQWrkIt->next();)
			CCLTRY(pQWrkIt->read(vLoc));
			CCLOK (pQWrkIt->next();)

			// Perform load and place result in queue
			#ifdef _WIN32
			if (hr == S_OK && (strLoc = vLoc).length() > 0 )
				{
				// For the direct client, loads are synchronous
				if (pnCliD != NULL)
					{
					// Attempt load, an 'empty' value will be queued if load fails
					// so that requestor knows request did not get lost.
					if (pnCliD->load ( strLoc, vVal ) != S_OK)
						adtValue::clear(vVal);

					// Add to value queue
					csVal.enter();
					pQVal->write ( adtString(L"") );
					pQVal->write ( strLoc );
					pQVal->write ( vVal );
					csVal.leave();
					}	// if
				}	 //	if
			#endif

			}	// if

		// Store
		else if (iOp == OP_STORE)
			{
			adtString strLoc;

			// Enough parameters ?
			if (sz < 3)
				break;

			// Location and value to store
			CCLOK (pQWrkIt->next();)
			CCLTRY(pQWrkIt->read(vLoc));
			CCLOK (pQWrkIt->next();)
			CCLTRY(pQWrkIt->read(vVal));
			CCLOK (pQWrkIt->next();)

			// Perform store
			if (	(strLoc = vLoc).length() > 0 )
				{
				if			(pnCliT != NULL)	pnCliT->store ( strLoc, vVal );
				#ifdef _WIN32
				else if	(pnCliD != NULL)	pnCliD->store ( strLoc, vVal );
				#endif
				}	// if
			}	// if

		// Listen/unlisten
		else if (iOp == OP_LISTEN || iOp == OP_UNLISTEN)
			{
			adtString strLoc;

			// Enough parameters ?
			if (sz < 2)
				break;

			// Location to listen
			CCLOK (pQWrkIt->next();)
			CCLTRY(pQWrkIt->read(vLoc));
			CCLOK (pQWrkIt->next();)

			// Perform load and place result in queue
			if (	(strLoc = vLoc).length() > 0 )
				{
				if			(pnCliT != NULL)	pnCliT->listen ( strLoc, (iOp == OP_LISTEN) );
				#ifdef _WIN32
				else if	(pnCliD != NULL)	pnCliD->listen ( strLoc, (iOp == OP_LISTEN) );
				#endif				
				}	// if

			}	// else if

		// ????
		else
			{
			lprintf ( LOG_ERR, L"Unepxected op code 0x%x", (U32)adtInt(vOp) );
			pQWrkIt->next();
			}	// else

		}	// while

	return hr;
	}	// tick

HRESULT nSpaceClientAsync :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	bRun = false;
	evWork.signal();
	return S_OK;
	}	// tickAbort

HRESULT nSpaceClientAsync :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Create queues
	CCLTRY ( COCREATE ( L"Adt.Queue", IID_IList, &pQWrk ) );
	CCLTRY ( pQWrk->iterate(&pQWrkIt) );
	CCLTRY ( COCREATE ( L"Adt.Queue", IID_IList, &pQVal ) );
	CCLTRY ( pQVal->iterate(&pQValIt) );

	// As a convenience create objects for persistence of values for
	// external users
	CCLTRY ( COCREATE ( L"Io.StmPrsBin", IID_IStreamPersist, &pPerBinLd ) );
	CCLTRY ( COCREATE ( L"Io.StmPrsBin", IID_IStreamPersist, &pPerBinSv ) );
	CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pPerMemLd ) );
	CCLTRY ( pPerMemLd->stream ( &pPerStmLd ) );
	CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pPerMemSv ) );
	CCLTRY ( pPerMemSv->stream ( &pPerStmSv ) );

	// Open own synchronous nSpace client
	#ifdef	_WIN32
	if (!bTCP)
		{
		CCLTRYE ( (pnCliD = new nSpaceClient()) != NULL, E_OUTOFMEMORY );
		CCLTRY  ( pnCliD->open ( strCmdLine, bShare, bDirect, this ) );
		}	// if
	else
	#endif
		{
		CCLTRYE ( (pnCliT = new nSpaceClientTcp()) != NULL, E_OUTOFMEMORY );
		CCLTRY  ( pnCliT->open ( strCmdLine, pcbTCP, this ) );
		}	// else

	// Debug
	lprintf ( LOG_DBG, L"nSpaceClientAsync:tickBegin: hr 0x%x", hr );

	return hr;
	}	// tickBegin

HRESULT nSpaceClientAsync :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr = S_OK;

	// Debug
	lprintf ( LOG_DBG, L"nSpaceClientAsync:tickEnd" );

	// Clean up
//MessageBeep(0);
	if (pnCliT != NULL)
		{
		pnCliT->close();
		delete pnCliT;
		pnCliT = NULL;
		}	// if
	#ifdef _WIN32
	if (pnCliD != NULL)
		{
		pnCliD->close();
		delete pnCliD;
		pnCliD = NULL;
		}	// if
	#endif
	_RELEASE(pQValIt);
	_RELEASE(pQVal);
	_RELEASE(pQWrkIt);
	_RELEASE(pQWrk);
	_RELEASE(pPerBinLd);
	_RELEASE(pPerBinSv);
	_RELEASE(pPerStmLd);
	_RELEASE(pPerStmSv);
	_RELEASE(pPerMemLd);
	_RELEASE(pPerMemSv);

	return hr;
	}	// tickEnd
