////////////////////////////////////////////////////////////////////////
//
//								ClientTcp.CPP
//
//					nSpace TCP external client
//
////////////////////////////////////////////////////////////////////////

#include	"nshxl.h"

// Internal libraries
#include "../netl/netl_.h"

// Objects in this module (for CCL linkage, no objects created in app)
CCL_OBJLIST_BEGIN()
CCL_OBJLIST_END_NOTDLL()

// Protoypes (netl)
//HRESULT	NetSkt_AddRef	( void );
//HRESULT	NetSkt_Receive ( SOCKET, void *, U32, U32 *, U32 );
//HRESULT	NetSkt_Release	( void );
//HRESULT	NetSkt_Resolve ( const ADTVALUE &, adtInt &, adtInt & );
//HRESULT	NetSkt_Send		( SOCKET, void const *, U32, U32 *, U32 );

nSpaceClientTcp :: nSpaceClientTcp()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pThrd		= NULL;
	bRun		= false;
	sktCli	= INVALID_SOCKET;
	pCB		= NULL;
	pDctSt	= NULL;
	pDctLd	= NULL;
	pDctLs	= NULL;
	pPrs		= NULL;
	pLstns	= NULL;
	strAddr	= L"127.0.0.1";
	AddRef();
	}	// nSpaceClientTcp

nSpaceClientTcp :: ~nSpaceClientTcp()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	close();
	}	// ~nSpaceClientTcp

HRESULT nSpaceClientTcp :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Close connection to namespace.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Shutdown the worker thread
	if (pThrd != NULL)
		{
		pThrd->threadStop(30000);
		_RELEASE(pThrd);
		}	// if
	_RELEASE(pDctLd);
	_RELEASE(pDctSt);
	_RELEASE(pDctLs);
	_RELEASE(pPrs);
	_RELEASE(pLstns);

	return hr;
	}	// close

void nSpaceClientTcp :: disconnect ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Close connection to namespace.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Debug
	lprintf ( LOG_DBG, L"Disconnect sktCli 0x%x\r\n", sktCli );

	// Close socket, will go back to retrying connection
	shutdown ( sktCli, 2 );
	::closesocket ( sktCli );
	sktCli = INVALID_SOCKET;

	// Disconnected
	if (pCBTcp != NULL)
		pCBTcp->onDisconnect();

	// Listens are now considered invalid
	pLstns->clear();

	}	// disconnect

HRESULT nSpaceClientTcp :: listen ( const WCHAR *szPath, BOOL bL,
												nSpaceClientCB *pCBAlt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Listen/unlisten to an emitter in the namespace.
	//
	//	PARAMETERS
	//		-	szPath is the namespace path
	//		-	bL is TRUE to listen, FALSE to unlisten
	//		-	pCBAlt is an alternate callback to receive notifications.
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	adtValue			vL;

	// Debug
	lprintf ( LOG_DBG, L"nSpaceClient::%s:bstrPath %s pCBAlt %p {\r\n", 
						(bL) ? L"listen" : L"unlisten", szPath, pCBAlt );

	// Listen
	if (hr == S_OK && bL)
		{
		// Associate path with callback
		CCLTRY ( pLstns->store ( adtString(szPath), 
					adtLong((U64) ((pCBAlt != NULL) ? pCBAlt : pCB)) ) );

		// Issue listen request to remote system
		if (hr == S_OK && sktCli != INVALID_SOCKET)
			{
			// Root path
			CCLTRY ( pDctLs->store ( adtString(L"From"), adtString(szPath) ) );

			// Send
			CCLTRY ( pPrs->save ( this, adtIUnknown(pDctLs) ) );
			}	// if

		}	// if

	// Unlisten
	else if (hr == S_OK && !bL)
		{
		// Issue unlisten TODO:

		// Remove from listens
		CCLOK ( pLstns->remove ( adtString(szPath) ); )

		}	// else if

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"} nSpaceClient::%s:hr 0x%x\r\n", 
						(bL) ? L"listen" : L"unlisten", hr );

	return hr;
	}	// listen

HRESULT nSpaceClientTcp :: load ( const WCHAR *szPath, ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Get a value from the namespace.
	//
	//	PARAMETERS
	//		-	szPath is the namespace path
	//		-	v will receive the value 
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= E_NOTIMPL;
	adtString		strPath(szPath);
	U32				len;

	// State check
	CCLTRYE ( (sktCli != INVALID_SOCKET), ERROR_INVALID_STATE );

	// Path must currently be a 'root' path (current working path ?)
	// Minimum path length : /X/OnFire
	CCLTRYE (	(len = strPath.length()) > 8 && szPath[0] == WCHAR('/'), 
					E_INVALIDARG );

	// Since loading only values (not locations) is supported through this interface,
	// allow caller to just specify '/OnFire' at the end of the path.
	// Full path needs to be 'XXX/OnFire/Value/'
	if (hr == S_OK && strPath[len-1] != '/')
		{
		CCLTRY ( strPath.append ( L"/" ) );
		CCLOK  ( ++len; )
		}	// if
	if (hr == S_OK && !WCASECMP(&strPath[len-7],L"OnFire/"))
		hr = strPath.append ( L"Value/" );

	// Generate command
	CCLTRY ( pDctLd->store ( adtString(L"From"), strPath ) );

	// Persist command to stream
//	CCLTRY ( pPrs->save ( this, adtIUnknown(pDctLd) ) );

	// Loading is asynchronous, wait for a response with a timeout
	lprintf ( LOG_DBG, L"Not implemented : %s", (LPCWSTR)strPath );

	return hr;
	}	// load

HRESULT nSpaceClientTcp :: onLoad ( const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when a value has been loaded from the connection.
	//
	//	PARAMETERS
	//		-	v contains the loaded value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	IDictionary		*pDct	= NULL;
	adtIUnknown		unkV(v);
	adtString		strVerb;
	adtValue			vL;

	// Communication is done with dictionaries
	CCLTRY(_QISAFE(unkV,IID_IDictionary,&pDct));

	// Access incoming verb
	CCLTRY(pDct->load(adtString(L"Verb"),vL));
	CCLTRYE((strVerb = vL).length() > 0,E_UNEXPECTED);

	// Value ?
	if  (hr == S_OK && !WCASECMP(strVerb,L"Value"))
		{
		adtString	strRoot,strLoc;
		adtValue		vV;

		// Load the parameters for the value verb
		CCLTRY(pDct->load(adtString(L"Root"),vL));
		CCLTRYE((strRoot = vL).length() > 0,E_UNEXPECTED);
		CCLTRY(pDct->load(adtString(L"Location"),vL));
		CCLTRYE((strLoc = vL).length() > 0,E_UNEXPECTED);
		CCLTRY(pDct->load(adtString(L"Value"),vV));

		// Deliver the value to registered callback
//		lprintf ( LOG_DBG, L"Root : %s : Location %s\r\n", 
//						(LPCWSTR)strRoot, (LPCWSTR)strLoc );
		if (hr == S_OK && pLstns->load ( strRoot, vL ) == S_OK)
			{
			adtLong	lCB(vL);

			// Value received
//			lprintf ( LOG_DBG, L"lCB %p\r\n", (U64)lCB );
			if (lCB != 0)
				((nSpaceClientCB *)(U64)lCB)->onReceive(strRoot,strLoc,vV);
			}	// if

		// Special case, if caller is expected a response to a 'load',
		// check if this is it.

		}	// if

	// Clean up
	_RELEASE(pDct);

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"0x%x\r\n", hr );

	return hr;
	}	// onLoad

HRESULT nSpaceClientTcp :: open ( const WCHAR *pwOpts, 
		nSpaceTcpCB *_pCBTcp, nSpaceClientCB *_pCB )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Open a connection to an nSpace namespace.
	//
	//	PARAMETERS
	//		-	pwOpts are a list of options
	//		-	_pCBTcp is the callback interface for the Tcp connection.
	//		-	_pCB is the callback client for notifications.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Setup
	pCBTcp	= _pCBTcp;
	pCB		= _pCB;

	// State check
	CCLTRYE ( (pThrd == NULL), ERROR_INVALID_STATE );
	CCLOK   ( strAddr = pwOpts; )

	// Create run-time objects
	CCLTRY ( COCREATE ( L"Io.StmPrsBin", IID_IStreamPersist, &pPrs ) );

	// Listens
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pLstns ) );

	// Prepare verbs
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctLs ) );
	CCLTRY ( pDctLs->store ( adtString(L"Verb"), adtString(L"Listen") ) );
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctSt ) );
	CCLTRY ( pDctSt->store ( adtString(L"Verb"), adtString(L"Store") ) );
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctLd ) );
	CCLTRY ( pDctLd->store ( adtString(L"Verb"), adtString(L"Load") ) );

	// Start worker thread
	CCLOK  ( bRun = true; )
	CCLTRY ( COCREATE ( L"Sys.Thread", IID_IThread, &pThrd ) );
	CCLTRY ( pThrd->threadStart ( this, 50000 ) );

	// Debug
	lprintf ( LOG_DBG, L"0x%x:%s\r\n", hr, pwOpts );

	return hr;
	}	// open

HRESULT nSpaceClientTcp :: store ( const WCHAR *szPath, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Store a value into the namespace.
	//
	//	PARAMETERS
	//		-	szPath is the namespace path
	//		-	v is the value to put into the namespace.
	//
	//	RETURN TYPE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	adtString		strPath;
	U32				len;

	// State check
	CCLTRYE ( (sktCli != INVALID_SOCKET), ERROR_INVALID_STATE );

	// Storing is done from the root location so ignore any leading slashes
	CCLOK ( strPath = (szPath != NULL && szPath[0] == '/') ? 
								szPath+1 : szPath; )

	// Ensure path is not a location
	if (hr == S_OK && strPath[(len=strPath.length())-1] == '/')
		strPath.at(--len) = '\0';

	// Did caller specify the trailing '/Value' for the key ?  If not
	// add it automatically
	if (	hr == S_OK &&
			(len = strPath.length()) > 6 &&
			(WCASECMP(&strPath[len-6],L"/Value")) )
		hr = strPath.append ( L"/Value" );

	// Generate command
	CCLTRY ( pDctSt->store ( adtString(L"To"), strPath ) );
	CCLTRY ( pDctSt->store ( adtString(L"Value"), v ) );

	// Persist command to stream
	CCLTRY ( pPrs->save ( this, adtIUnknown(pDctSt) ) );

	// Clean up
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"nSpaceClient::store:szPath %s:type %d:hr 0x%x\r\n", 
						szPath, v.vtype, hr );

	return hr;
	}	// store

HRESULT nSpaceClientTcp :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtValue	vOp,vLoc,vVal;

	// Keep running ?
	CCLTRYE ( bRun == true, S_FALSE );

	//
	// Connection
	//
	if (hr == S_OK && sktCli == INVALID_SOCKET)
		{
		struct sockaddr_in	sockaddr;
		u_long					cmd = 1;
		adtInt					iAddr,iPort;

		// Create client socket
		CCLTRYE ( (sktCli = socket ( AF_INET, SOCK_STREAM, 0 )) 
							!= INVALID_SOCKET, WSAGetLastError() );

		// Make a non-blocking socket
		#ifdef _WIN32
		CCLOK	( ioctlsocket ( sktCli, FIONBIO, &cmd ); )
		#endif

		// Resolve address.  TODO: Allow alternate address to be specified in 'open'
		CCLTRY ( NetSkt_Resolve ( strAddr, iAddr, iPort ) );
		CCLOK ( iPort = PORT_NSPC_TCP; )

		// Target address 
		CCLOK ( sockaddr.sin_family		= AF_INET; )
		CCLOK ( sockaddr.sin_port			= htons ( iPort ); )
		CCLOK ( sockaddr.sin_addr.s_addr = htonl ( iAddr ); )

		// Initiate connection to server
		lprintf ( LOG_DBG, L"Attempting connection" );
		CCLTRYE ( (connect ( sktCli, (struct sockaddr *) &sockaddr,
						sizeof(sockaddr) ) != SOCKET_ERROR), WSAGetLastError() );

		// If connection is pending, wait for timeout
		#ifdef	_WIN32
		if (hr == WSAEWOULDBLOCK)
		#elif		__unix__ || __APPLE__
		if (hr == EINPROGRESS)
		#endif
			{
			fd_set			wfds;
			int				s;
			struct timeval	to = { 5, 0 };

			// Wait for writability (connected) with timeout, 
			// this allows thread to be shut down even if connection is pending
			FD_ZERO ( &wfds );
			FD_SET ( sktCli, &wfds );
			hr = ((s = select ( (int)sktCli+1, NULL, &wfds, NULL, &to )) >= 0) ? S_OK : WSAGetLastError();

			// Connected ?
			hr = (hr == S_OK && FD_ISSET(sktCli,&wfds)) ? S_OK : ERROR_TIMEOUT;

			// If not connected, close socket and try again later
			if (hr != S_OK)
				{
				shutdown ( sktCli, 2 );
				::closesocket ( sktCli );
				sktCli = INVALID_SOCKET;

				// No need to exit thread on connection error
				hr = S_OK;
				}	// if

			else
				{
				lprintf ( LOG_DBG, L"Connected! pCBTcp %p\r\n", pCBTcp );
				if (pCBTcp != NULL)
					pCBTcp->onConnect();
				}	// else

			}	// if

		}	// if

	//
	// Receive
	//
	if (hr == S_OK && sktCli != INVALID_SOCKET)
		{
		fd_set			rfds;
		int				s;
		struct timeval	to = { 2, 0 };

		// Wait for incoming data, readbility
		FD_ZERO ( &rfds );
		FD_SET ( sktCli, &rfds );
		hr = ((s = select ( (int)sktCli+1, &rfds, NULL, NULL, &to )) >= 0) ? S_OK : WSAGetLastError();
//		lprintf ( LOG_DBG, L"select(r) %d\r\n", s );

		// Bad socket/disconnected ?
		#ifdef	_WIN32
		if (hr == WSAENOTSOCK)
		#elif		__unix__ || __APPLE__
		if (hr == EBADF)
		#endif
			disconnect();

		// Readability
		else if (hr == S_OK && s > 0 && FD_ISSET(sktCli,&rfds))
			{
			adtValue		vL;

			// Debug
//			lprintf ( LOG_DBG, L"Reading from socket {\r\n" );

			// Read value from stream
			CCLTRY ( pPrs->load ( this, vL ) );

			// Process value
			CCLOK ( onLoad ( vL ); )

			// Debug
//			lprintf ( LOG_DBG, L"} Read from socket 0x%x\r\n", hr );

			// If a read from the socket failed, it is possible the other end
			// has closed the connection.
			#ifdef	_WIN32
			if (hr == WSAECONNRESET)
			#elif		__unix__ || __APPLE__
			if (hr == ECONNRESET)
			#endif
				{
				// Clean up
				disconnect();

				// Just going back to connect
				hr = S_OK;
				}	// if

			}	// else if

		}	// if

	return hr;
	}	// tick

HRESULT nSpaceClientTcp :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Thread is designed to loop around every few seconds, just signal shutdown
	bRun = false;

	return S_OK;
	}	// tickAbort

HRESULT nSpaceClientTcp :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Debug
	lprintf ( LOG_DBG, L"" );

	// Networking
	CCLTRY ( NetSkt_AddRef() );

	// Calling 'tick' from here so that if a connection to the remote
	// system is available, the 'open' call can return ready-to-go.
	CCLTRY ( tick() );

	return hr;
	}	// tickBegin

HRESULT nSpaceClientTcp :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr = S_OK;

	// Debug
	lprintf ( LOG_DBG, L"" );

	// Done w/connection
	disconnect();

	// Networking
	NetSkt_Release();

	return hr;
	}	// tickEnd

///////////////
// IByteStream
///////////////

HRESULT nSpaceClientTcp :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr					= S_OK;
	struct	timeval	to		= { 0, 0 };
	int		ret				= 0;
	fd_set	rfds;

	// Initialize descriptor
	CCLOK ( FD_ZERO ( &rfds ); )
	CCLOK ( FD_SET ( sktCli, &rfds ); )

	// Socket readable ?
	CCLTRYE	( (ret = select ( (int)sktCli+1, &rfds, NULL, NULL, &to )) 
					!= SOCKET_ERROR, WSAGetLastError() );
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( FD_ISSET ( sktCli, &rfds ), ERROR_TIMEOUT );

	// If socket is readbale, assume at least one byte is available.
	(*puAv) = (hr == S_OK) ? 1 : 0;

	return hr;
	}	// available

HRESULT nSpaceClientTcp :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
	U8			cpybufr[4096];
	U64		nio,nw,nr;

	// State check
	CCLTRYE ( sktCli != INVALID_SOCKET, ERROR_INVALID_STATE );
	CCLTRYE ( uSz != 0, ERROR_INVALID_STATE );
 
	// Setup
	if (puSz != NULL)
		*puSz	= 0;

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK ( nio = (sizeof(cpybufr) < uSz) ? sizeof(cpybufr) : uSz; )
		CCLTRY( read ( cpybufr, nio, &nr ) );

		// Write full block to stream
		CCLOK ( fp		= cpybufr; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;

		// If at end of file before request has been satisfied, stop
		if (uSz && (nr < nio))
			break;
		}	// while

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// copyTo

HRESULT nSpaceClientTcp :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Nothing to do for socket

	return hr;
	}	// flush

HRESULT nSpaceClientTcp :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	U32		nr;

	// State check
	CCLTRYE ( (sktCli != INVALID_SOCKET), ERROR_INVALID_STATE );

	// Read next block of data
	CCLTRY ( NetSkt_Receive ( sktCli, pvBfr, (U32)nio, &nr, 5000 ) );

	// Results
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// read

HRESULT nSpaceClientTcp :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Not supported on a socket
	return E_NOTIMPL;
	}	// seek

HRESULT nSpaceClientTcp :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// A socket can be thought of has having any amount of data
	return S_OK;
	}	// setSize

HRESULT nSpaceClientTcp :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	const char	*pc	= (const char *)pcvBfr;
	U32			nleft,nw;

	// State check
	CCLTRYE ( (sktCli != INVALID_SOCKET), ERROR_INVALID_STATE );

	// Write all the data
	CCLOK ( nleft = (U32)nio; )
	while (hr == S_OK && nleft > 0)
		{
		// Next write
		CCLTRY ( NetSkt_Send ( sktCli, pc, nleft, &nw, 10000 ) );
//		dbgprintf ( L"nSpaceClientTcp::write:nleft %d:nw %d\r\n", nleft, nw );

		// Result
		CCLOK ( nleft	-= nw; )
		CCLOK ( pc		+= nw; )
		}	// while

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nio;

	// Debug
	if (hr != S_OK)
		dbgprintf ( L"nSpaceClientTcp::write:Failed:0x%x\r\n", hr );

	return hr;
	}	// write

	