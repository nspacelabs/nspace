////////////////////////////////////////////////////////////////////////
//
//									Context.CPP
//
//							SSL context management
//
////////////////////////////////////////////////////////////////////////

#include "openssll_.h"

// Globals
extern libSSL	libS;

Context :: Context ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	}	// Context

HRESULT Context :: context ( IDictionary *pDct, const WCHAR *pwName, 
										objSSL **ppctx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Extract the SSL object reference from the dictionary
	//
	//	PARAMETERS
	//		-	pDct is the SSL context
	//		-	pwName is the key name to use
	//		-	ppctx will receive a ptr to the active context
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IThis			*pThis	= NULL;
	adtValue		vL;
	adtIUnknown	unkV;

	// State check
	CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( ppctx != NULL, E_INVALIDARG );
	CCLOK   ( *ppctx = NULL; )

	// Access keyed object
	CCLTRY	( pDct->load ( adtString(pwName), vL ) );
	CCLTRY	( _QISAFE((unkV=vL),IID_IThis,&pThis) );
	CCLTRY	( pThis->getThis ( (void **) ppctx ) );

	// Return referenced ptr so don't release 'pThis'
//	_RELEASE(pThis);

	return hr;
	}	// context

HRESULT Context :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// Library reference
	if (bAttach)
		{
		// Library reference
		libS.AddRef();

		// Defaults
		if (pnDesc->load ( adtString(L"PeerCert"), vL ) == S_OK)
			bPeerCert = vL;

		}	// if
	else
		{
		// Clean up
		_RELEASE(pDct);

		// Lbrary reference
		libS.Release();
		}	// else

	return S_OK;
	}	// onAttach

HRESULT Context :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Initalize/set an SSL security context
	if (_RCP(Open))
		{
		SSL_CTX	*sslctx	= NULL;
		objSSL	*pctx		= NULL;
		IThis		*obj		= NULL;
		char 		*pcStr	= NULL;
		int		ret;
		adtValue	vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Create a new SSL context, TLS negotitation
		CCLTRYE ( (sslctx = SSL_CTX_new(TLS_method())) != NULL, E_UNEXPECTED );
		CCLTRYE ( (pctx = new objSSL(sslctx)) != NULL, E_OUTOFMEMORY );
		CCLTRY  ( pDct->store ( adtString(L"sslCtx"), adtIUnknown(obj = pctx) ) );

		// Defaults
		CCLOK ( SSL_CTX_set_min_proto_version ( sslctx, TLS1_2_VERSION ); )
		CCLOK ( SSL_CTX_set_options ( sslctx, SSL_OP_ALL ); )

		// Necessary during dev until own CA is in place.  This forces the client of
		// a server to send certificate so the public key can extracted.
		if (hr == S_OK && bPeerCert)
			SSL_CTX_set_verify ( sslctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, verify );
//		CCLOK ( SSL_CTX_set_cert_verify_callback ( sslctx, verify, this ); )

		// Certificate ?
		if (hr == S_OK && pDct->load ( adtString(L"Certificate"), vL ) == S_OK)
			{
			adtString	strV(vL);

			// Need ASCII for function
			CCLTRY ( strV.toAscii ( &pcStr ) );

			// Assign to context
			CCLTRYE ( (ret = SSL_CTX_use_certificate_file ( sslctx, pcStr, 
							SSL_FILETYPE_PEM )) > 0, ret );
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Error assigning certificate file : '%S' 0x%x\r\n", pcStr, hr );

			// Clean up
			_FREEMEM(pcStr);
			}	// if

		// Private key ?
		if (hr == S_OK && pDct->load ( adtString(L"PrivateKey"), vL ) == S_OK)
			{
			adtString	strV(vL);

			// Need ASCII for function
			CCLTRY ( strV.toAscii ( &pcStr ) );

			// Assign to context
			CCLTRYE ( (ret = SSL_CTX_use_PrivateKey_file ( sslctx, pcStr, 
							SSL_FILETYPE_PEM )) > 0, ret );
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Error assigning private key file : '%S' 0x%x\r\n", pcStr, hr );

			// Clean up
			_FREEMEM(pcStr);
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			{
			lprintf ( LOG_DBG, L"Open failed, hr 0x%x\r\n", hr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pctx);
		}	// if

	// Close/clean up context
	else if (_RCP(Close))
		{
		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// About to close
		CCLOK ( _EMT(Close,adtIUnknown(pDct)); )

		// Done w/context, this will free the embedded context
		if (pDct != NULL)
			pDct->remove ( adtString(L"sslCtx") );
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive

int Context :: verify ( int preverify_ok, X509_STORE_CTX *ctx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Certificate verification callback function.
	//
	//	PARAMETERS
	//		-	pre specifies whether the verification of the certificate in
	//			question was passed.
	//		-	ctx is the certificate chain
	//
	//	RETURN VALUE
	//		1 if verified
	//
	////////////////////////////////////////////////////////////////////////

	// Temporarily just validate the cert. until CA is in place.
//	lprintf ( LOG_DBG, L"verify : pre %d ctx %p\r\n", preverify_ok, ctx );

	return 1;
	}	// verify


