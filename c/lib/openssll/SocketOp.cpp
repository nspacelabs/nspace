////////////////////////////////////////////////////////////////////////
//
//									SocketOp.CPP
//
//							SSL based socket operations
//
////////////////////////////////////////////////////////////////////////

//
// Sample code
//
//	https://github.com/openssl/openssl/tree/691064c47fd6a7d11189df00a0d1b94d8051cbe0/demos/ssl
//

#include "openssll_.h"

// Globals
extern libSSL	libS;

#define	SIZE_COPY_BFR	0x100000

SocketOp :: SocketOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pCtx	= NULL;
	pSkt	= NULL;
	}	// SocketOp

HRESULT SocketOp :: context ( IDictionary *pDct, objSSL **ppctx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Verifies proper initialization of the SSL context for socket
	//			operations.
	//
	//	PARAMETERS
	//		-	pDct is the SSL context
	//		-	ppctx will receive a ptr to the active context
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	const	SSL_METHOD	*method	= NULL;
	SSL_CTX				*sslctx	= NULL;
	IThis					*obj		= NULL;
	char 					*pcStr	= NULL;
	int					ret;
	adtValue				vL;

	// Setup
	*ppctx = NULL;

	// State check
	CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

	// Is context already initialized ?
	if (hr == S_OK && pDct->load ( adtString(L"SslCtx"), vL ) == S_OK)
		{
		adtIUnknown		unkV(vL);
		IThis	*pThis	= NULL;

		// Obtain context object
		CCLTRY ( _QISAFE(unkV,IID_IThis,&pThis) );
		CCLTRY ( pThis->getThis ( (void **) ppctx ) );

		// Keep reference count on object for return
		// Clean up
//		_RELEASE(pThis);

		// If successful, done
		if (hr == S_OK)
			return hr;
		}	// if

	// Assume SSL/TLS communication
	CCLTRYE ( (method = SSLv23_method()) != NULL, E_UNEXPECTED );

	// Create a new SSL context
	CCLTRYE ( (sslctx = SSL_CTX_new(method)) != NULL, E_UNEXPECTED );
	CCLTRYE ( (*ppctx = new objSSL(sslctx)) != NULL, E_OUTOFMEMORY );
	CCLTRY  ( pDct->store ( adtString(L"SslCtx"), adtIUnknown(obj = *ppctx) ) );

	// Certificate ?
	if (hr == S_OK && pDct->load ( adtString(L"Certificate"), vL ) == S_OK)
		{
		adtString	strV(vL);

		// Need ASCII for function
		CCLTRY ( strV.toAscii ( &pcStr ) );

		// Assign to context
		CCLTRYE ( (ret = SSL_CTX_use_certificate_file ( sslctx, pcStr, 
						SSL_FILETYPE_PEM )) > 0, ret );
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"Error assigning certificate file : '%S' 0x%x\r\n", pcStr, hr );

		// Clean up
		_FREEMEM(pcStr);
		}	// if

	// Private key ?
	if (hr == S_OK && pDct->load ( adtString(L"PrivateKey"), vL ) == S_OK)
		{
		adtString	strV(vL);

		// Need ASCII for function
		CCLTRY ( strV.toAscii ( &pcStr ) );

		// Assign to context
		CCLTRYE ( (ret = SSL_CTX_use_PrivateKey_file ( sslctx, pcStr, 
						SSL_FILETYPE_PEM )) > 0, ret );
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"Error assigning private key file : '%S' 0x%x\r\n", pcStr, hr );

		// Clean up
		_FREEMEM(pcStr);
		}	// if

	return hr;
	}	// context

HRESULT SocketOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// Library reference
	if (bAttach)
		{
		// Library reference
		libS.AddRef();
		}	// if
	else
		{
		_RELEASE(pSkt);
		_RELEASE(pCtx);

		// Lbrary reference
		libS.Release();
		}	// else

	return S_OK;
	}	// onAttach

HRESULT SocketOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Initalize/set an SSL security context
	if (_RCP(Context))
		{
		objSSL		*pctx	= NULL;
		adtIUnknown	unkV(v);

		// New context
		_RELEASE(pCtx);
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pCtx) );

		// Initialize the context
		CCLTRY ( context ( pCtx, &pctx ) );

		// Clean up
		_RELEASE(pctx);

		// Result
		if (hr == S_OK)
			_EMT(Context,adtIUnknown(pCtx));
		else
			{
			lprintf ( LOG_DBG, L"Context failed, hr 0x%x\r\n", hr );
			_EMT(Error,adtInt(hr));
			}	// else

		}	// if

	// Accept an incoming socket connection
	else if (_RCP(Accept))
		{
		objSSL	*pctx		= NULL;
		objSSL	*pssl		= NULL;
		int		fd			= -1;
		int		ret		= 0;
		int		sslerr	= SSL_ERROR_NONE;
		adtValue	vL;

		// State check
		CCLTRYE ( pCtx != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pSkt != NULL, ERROR_INVALID_STATE );

		// Extract actual socket file descriptor
		CCLTRY ( pSkt->load ( adtString(L"Socket"), vL ) );
		CCLTRYE ( (fd	= (int)adtLong(vL)) != -1, E_INVALIDARG );

		// Obtain context
		CCLTRY ( context ( pCtx, &pctx ) );

		// Create an SSL context for the socket
		if (pctx != NULL)
			{
			IThis		*pthis = NULL;

			// Create a new SSL object
			CCLTRYE ( (pssl = new objSSL( SSL_new ( pctx->ctx ) )) != NULL,
							E_UNEXPECTED );
			CCLTRY  ( pSkt->store ( adtString(L"Ssl"), adtIUnknown(pthis = pssl) ) );
			
			// Assign it to the socket descriptor
			CCLOK ( SSL_set_fd ( pssl->ssl, fd ); )

			// Perform the SSL specific accept.  Multiple calls are necessary
			// during the negotiation.  Externalize this to make async ?
			ret = -1;
			while (hr == S_OK && ret < 0)
				{
				int sslerr = SSL_ERROR_NONE;

				// Try accept again
				ret = SSL_accept ( pssl->ssl );

				// SSL specific error
				sslerr = SSL_get_error ( pssl->ssl, ret );

				// Should be 'SSL_ERROR_WAND_READ' if protocol is still running
				if (	sslerr != SSL_ERROR_NONE &&
						sslerr != SSL_ERROR_WANT_READ )
					hr = sslerr;
				}	// while

			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Accept,adtIUnknown(pSkt));
		else
			{
			lprintf ( LOG_DBG, L"Accept failed, hr 0x%x, sslerr %d\r\n", hr , sslerr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pssl);
		_RELEASE(pctx);

		}	// if


	// Attach and existing socket to the SSL context
	else if (_RCP(Attach))
		{
		objSSL	*pctx		= NULL;
		objSSL	*pssl		= NULL;
		int		fd			= -1;
		int		ret		= 0;
		adtValue	vL;

		// State check
		CCLTRYE ( pCtx != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pSkt != NULL, ERROR_INVALID_STATE );

		// Extract actual socket file descriptor
		CCLTRY ( pSkt->load ( adtString(L"Socket"), vL ) );
		CCLTRYE ( (fd	= (int)adtLong(vL)) != -1, E_INVALIDARG );

		// Obtain context
		CCLTRY ( context ( pCtx, &pctx ) );

		// Create an SSL context for the socket
		if (pctx != NULL)
			{
			IThis		*pthis = NULL;

			// Create a new SSL object
			CCLTRYE ( (pssl = new objSSL( SSL_new ( pctx->ctx ) )) != NULL,
							E_UNEXPECTED );
			CCLTRY  ( pSkt->store ( adtString(L"Ssl"), adtIUnknown(pthis = pssl) ) );
			
			// Assign it to the socket descriptor
			CCLTRYE (	(ret = SSL_set_fd ( pssl->ssl, fd )) == 0, 
							SSL_get_error ( pssl->ssl, ret ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Attach,adtIUnknown(pSkt));
		else
			{
			lprintf ( LOG_DBG, L"Attach failed, hr 0x%x\r\n", hr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pssl);
		_RELEASE(pctx);

		}	// if

	// Initiate the TLS/SSL handshakre with TLS/SSL server
	else if (_RCP(Connect))
		{
		objSSL	*pctx		= NULL;
		objSSL	*pssl		= NULL;
		int		fd			= -1;
		int		ret		= 0;
		int		sslerr	= SSL_ERROR_NONE;
		adtValue	vL;

		// State check
		CCLTRYE ( pCtx != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pSkt != NULL, ERROR_INVALID_STATE );

		// Extract actual socket file descriptor
		CCLTRY ( pSkt->load ( adtString(L"Socket"), vL ) );
		CCLTRYE ( (fd	= (int)adtLong(vL)) != -1, E_INVALIDARG );

		// Obtain context
		CCLTRY ( context ( pCtx, &pctx ) );

		// Create an SSL context for the socket
		if (pctx != NULL)
			{
			IThis		*pthis = NULL;

			// Create a new SSL object
			CCLTRYE ( (pssl = new objSSL( SSL_new ( pctx->ctx ) )) != NULL,
							E_UNEXPECTED );
			CCLTRY  ( pSkt->store ( adtString(L"Ssl"), adtIUnknown(pthis = pssl) ) );
			
			// Assign it to the socket descriptor
			CCLOK ( SSL_set_fd ( pssl->ssl, fd ); )

			// Perform the SSL specific connect.  Multiple calls are necessary
			// during the negotiation.  Externalize this to make async ?
			ret = -1;
			while (hr == S_OK && ret < 0)
				{
				int sslerr = SSL_ERROR_NONE;

				// Try again
				ret = SSL_connect ( pssl->ssl );

				// SSL specific error
				sslerr = SSL_get_error ( pssl->ssl, ret );

				// Should be 'SSL_ERROR_WAND_READ' if protocol is still running
				if (	sslerr != SSL_ERROR_NONE &&
						sslerr != SSL_ERROR_WANT_READ )
					hr = sslerr;
				}	// while

			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Connect,adtIUnknown(pSkt));
		else
			{
			lprintf ( LOG_DBG, L"Connect failed, hr 0x%x, sslerr %d\r\n", hr , sslerr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pssl);
		_RELEASE(pctx);

		}	// if

	// Queyr information about connection
	else if (_RCP(Query))
		{
		IThis			*pThis	= NULL;
		objSSL		*pssl		= NULL;
		X509			*cert		= NULL;
		adtValue		vL;
		adtIUnknown	unkV;

		// State check
		CCLTRYE ( pSkt != NULL, ERROR_INVALID_STATE );

		// Access SSL object
		CCLTRY	( pSkt->load ( adtString(L"SSL"), vL ) );
		CCLTRY	( _QISAFE((unkV=vL),IID_IThis,&pThis) );
		CCLTRY	( pThis->getThis ( (void **) &pssl ) );

		// Obtain the client certificate
		if (	hr == S_OK && 
				(cert = SSL_get_peer_certificate ( pssl->ssl )) != NULL )
			{
			adtString	strV;
			char			*str	= NULL;
			EVP_PKEY		*key	= NULL;

			// Fill in relevant information.
			CCLTRY ( pSkt->store ( adtString(L"X509Version"),
											adtInt(X509_get_version(cert)) ) );
			if (	hr == S_OK &&	
					(str = X509_NAME_oneline(X509_get_subject_name(cert),0,0)) != NULL)
				{
				hr = pSkt->store ( adtString(L"X509SubjectName"), (strV = str) );
				OPENSSL_free(str);
				}	// if
			if (	hr == S_OK &&	
					(str = X509_NAME_oneline(X509_get_issuer_name(cert),0,0)) != NULL)
				{
				hr = pSkt->store ( adtString(L"X509IssuerName"), (strV = str) );
				OPENSSL_free(str);
				}	// if

			// Public key in string format
			if	(	hr == S_OK &&
					(key = X509_get_pubkey ( cert )) != NULL )
				{
				IByteStream *pStm		= NULL;
				BYTE			*pbKey	= NULL;
				int			ret		= 0;

				// Extract key
				CCLTRYE ( (ret = i2d_PUBKEY(key,&pbKey)) > 0, E_UNEXPECTED );
				CCLTRYE ( pbKey != NULL, E_UNEXPECTED );

				// Store as a byte stream
				CCLTRY ( COCREATE ( L"Io.StmMemory", IID_IByteStream, &pStm ) );
				CCLTRY ( pStm->write ( pbKey, ret, NULL ) );
				CCLOK  ( pStm->seek ( 0, STREAM_SEEK_SET, NULL ); )

				// Store as key
				CCLTRY ( pSkt->store ( adtString(L"X509PublicKey"), adtIUnknown(pStm) ) );

				// Clean up
				_RELEASE(pStm);
				}	// if

			// Clean up
			X509_free(cert);
			}	// if

		// Result
		if (hr != S_OK)
			_EMT(Error,adtInt(hr));

		// Clean up
		}	// else if

	// Attach byte stream to socket
	else if (_RCP(Stream))
		{
		IByteStream	*pStm		= NULL;

		// Ensure stream exists for socket
		CCLTRY ( sktStm ( pSkt, strnName, &pStm ) );

		// Result
		if (hr == S_OK)
			_EMT(Stream,adtIUnknown(pStm));
		else 
			_EMT(Error,adtInt(hr));

		}	// else if

	// State
	else if (_RCP(Socket))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSkt);
		hr = _QISAFE(unkV,IID_IDictionary,&pSkt);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive

HRESULT SocketOp :: sktStm ( 	IDictionary *pSkt, const WCHAR *pwName,
										IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve the persistence socket stream for the socket 
	//			dictionary.
	//
	//	PARAMETERS
	//		-	pSkt is the socket dictionary
	//		-	pwName is the name of the requestor (for debug)
	//		-	ppStm will receive the stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	objSSL		*pssl		= NULL;
	IThis			*pThis	= NULL;
	adtValue		vL;
	adtIUnknown	unkV;

	// State check
	CCLTRYE ( pSkt != NULL, ERROR_INVALID_STATE );

	// Access SSL object
	CCLTRY	( pSkt->load ( adtString(L"SSL"), vL ) );
	CCLTRY	( _QISAFE((unkV=vL),IID_IThis,&pThis) );
	CCLTRY	( pThis->getThis ( (void **) &pssl ) );

	// Does socket have a persistence stream yet ?
	if (hr == S_OK)
		{
		// Need a new stream ?
		if (pSkt->load ( adtString(L"Stream"), vL ) != S_OK)
			{
//			lprintf ( LOG_DBG, L"New stream for socket %d\r\n", skt );

			// Persistence stream object
			CCLTRYE ( (*ppStm = new sslSktStm ( pwName, pssl )) != NULL, E_OUTOFMEMORY );
			(*ppStm)->AddRef();

			// Store in dictionary
			CCLTRY ( pSkt->store ( adtString(L"Stream"), adtIUnknown(*ppStm) ) );
			}	// if

		// Existing stream
		else
			{
			CCLTRY ( _QISAFE((unkV=vL),IID_IByteStream,ppStm) );
			}	// else
		}	// if

	// Clean up
	_RELEASE(pThis);

	return hr;
	}	// sktStm

//
//
//

sslSktStm :: sslSktStm ( const WCHAR *pNameP, objSSL *_pssl )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	pNameP is the name of the parent.
	//		-	_pssl is the SSL socket object
	//
	////////////////////////////////////////////////////////////////////////
	strNameP	= pNameP;
	pssl		= _pssl;
	_ADDREF(_pssl);
	iWrIdx	= 0;
	iRdCnt	= 0;
	iRdIdx	= 0;
	puCpy		= NULL;
	}	// sslSktStm

sslSktStm :: ~sslSktStm ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	_FREEMEM(puCpy);
	_RELEASE(pssl);
	}	// ~sslSktStm

HRESULT sslSktStm :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr					= S_OK;
	struct	timeval	to		= { 0, 0 };
	int		ret				= 0;
	int		skt				= -1;
	fd_set	rfds;

	// State check
	CCLTRYE ( pssl != NULL, ERROR_INVALID_STATE );

	// Extract socket file descriptor
	CCLOK ( skt = SSL_get_fd(pssl->ssl); )

	// Initialize descriptor
	CCLOK ( FD_ZERO ( &rfds ); )
	CCLOK ( FD_SET ( skt, &rfds ); )

	// Socket readable ?
	CCLTRYE	( (ret = select ( (int)skt+1, &rfds, NULL, NULL, &to )) 
					!= SOCKET_ERROR, WSAGetLastError() );
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( FD_ISSET ( skt, &rfds ), ERROR_TIMEOUT );

	// If socket is readbale, assume at least one byte is available.
	(*puAv) = (hr == S_OK) ? 1 : 0;

	return hr;
	}	// available

HRESULT sslSktStm :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
//	U8			cpybufr[4096];
	U64		nio,nw,nr;

	// State check
	CCLTRYE ( pssl != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( uSz != 0, ERROR_INVALID_STATE );

	// Setup
	if (puSz != NULL)
		*puSz	= 0;

	// Using on demand, large, internal copy buffer for speed
	if (hr == S_OK && puCpy == NULL)
		{
		CCLTRYE ( (puCpy = (U8 *) _ALLOCMEM ( SIZE_COPY_BFR )) != NULL,
						E_OUTOFMEMORY );
		}	// if

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK ( nio = (SIZE_COPY_BFR < uSz) ? SIZE_COPY_BFR : uSz; )
		CCLTRY( read ( puCpy, nio, &nr ) );
//		lprintf ( LOG_DBG, L"uSz %d nio %d nr %d\r\n", uSz, nio, nr );

		// Any bytes available ?
		if (hr == S_OK && nr == 0)
			{
			lprintf ( LOG_DBG, L"Successful read of 0 bytes uSz %ld nio %d\r\n", uSz, nio );
			break;
			}	// if

		// Write full block to stream
		CCLOK ( fp		= puCpy; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;

		// If at end of file before request has been satisfied, stop
//		if (uSz && (nr < nio))
//			{
//			lprintf ( LOG_DBG, L"Breaking uSz %ld nr %d nio %d\r\n", uSz, nr, nio );
//			break;
//			}	// if
		}	// while

	// Debug
//	if (hr != S_OK)
//		dbgprintf ( L"sslSktStm::copyTo:Failed:0x%x\r\n", hr );

	return hr;
	}	// copyTo

HRESULT sslSktStm :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U32			nleft,nw;

	// Flush will send out anything in the write cache
	if (iWrIdx > 0)
		{
		// Debug
//		adtDate then;
//		then.now();
//		lprintf ( LOG_DBG, L"skt 0x%x iWrIdx %d \r\n", skt, iWrIdx );

		// Write all the data
		CCLOK ( nleft 	= iWrIdx; )
		CCLOK ( iWrIdx = 0; )
		while (hr == S_OK && nleft > 0)
			{
			// Next write
//			CCLTRY ( SslSkt_Send ( pssl->ssl, &bWr[iWrIdx], nleft, &nw, 10000 ) );
			nw = SSL_write ( pssl->ssl, &bWr[iWrIdx], nleft );
//			if (nw > 1024)
//				lprintf ( LOG_DBG, L"skt 0x%x nleft %d nw %d hr 0x%x\r\n", skt, nleft, nw, hr );

			// Result
			CCLOK ( nleft	-= nw; )
			CCLOK ( iWrIdx	+= nw; )
			}	// while

		// Debug
//		adtDate now;
//		now.now();
//		lprintf ( LOG_DBG, L"skt 0x%x iWrIdx %d hr 0x%x time %g\r\n", 
//									skt, iWrIdx, hr, ((now-then)*SECINDAY) );

		// Cache empty
		iWrIdx = 0;
		}	// if

	return hr;
	}	// flush

HRESULT sslSktStm :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U8			*pcBfr	= (U8 *) pvBfr;
	int		nr;//,ncpy,nleft;

	// State check
	CCLTRYE ( (pssl != NULL), ERROR_INVALID_STATE );

	// Setup
	if (pnio != NULL) *pnio = 0;
/*
	lprintf ( LOG_DBG, L"nio %d\r\n", nio );

	// Continue reading until the request is satified or a short read occurs.
	// The idea is to simulate what could happen during a single socket 'read'.
	nleft = (U32) nio;
	while (hr == S_OK && nleft > 0)
		{
		// Need more data ?
		nr = 0;
		if (iRdCnt-iRdIdx <= 0)
			{
			// Perform read
			hr = NetSkt_Receive ( skt, bRd, sizeof(bRd), &nr, 5000 );
			lprintf ( LOG_DBG, L"Cached read : %d bytes\r\n", nr );
			for (int c = 0;c < nr;++c)
				lprintf ( LOG_DBG, L"c%d) %d 0x%x %c\r\n", c, bRd[c], bRd[c], bRd[c] );

			// Update state
			iRdIdx = 0;
			iRdCnt = (hr == S_OK) ? nr : 0;
			}	// if

		// Copy requested data
		if (hr == S_OK)
			{
			// Maximum amount to copy
			ncpy = (nleft < (U32)(iRdCnt-iRdIdx)) ? nleft : (U32)(iRdCnt-iRdIdx);
			lprintf ( LOG_DBG, L"ncpy %d iRdCnt %d iRdIdx %d nleft %d\r\n",
										ncpy, iRdCnt, iRdIdx, nleft );
if (ncpy < nleft)
	{
	lprintf ( LOG_DBG, L"Partial copy!\r\n" );
	}	// if
			if (ncpy > 0)
				{
				// Copy into buffer
				memcpy ( pcBfr, &bRd[iRdIdx], ncpy );

				// Update state
				pcBfr		+= ncpy;
				nleft		-= ncpy;
				iRdIdx	+= ncpy;
				if (pnio != NULL)
					*pnio += ncpy;
				}	// if
			}	// if

		// If data was read from the socket but not a full cache amount, done
//		if (nr > 0 && nr < sizeof(bRd))
//			{
//			if (nleft != 0)
//				break;
//			break;
//			}	// if
		}	// while
*/

	// Read next block of data
//	CCLTRY ( SslSkt_Receive ( pssl->ssl, pvBfr, (U32)nio, &nr, 5000 ) );
	CCLTRYE ( (nr = SSL_read ( pssl->ssl, pvBfr, (U32)nio )) > 0,
					SSL_get_error(pssl->ssl,nr) );

	// If 'want more' is the read result this just means there was data but
	// not enough to produce real bytes (protocol stuff ?)
	if (hr == SSL_ERROR_WANT_READ)
		{
		// Just treat it as a valid read of zero bytes.
		hr = S_OK;
		nr	= 0;
		}	// if

	// Getting 'SSL_ERROR_SYSCALL' before it starts working.. need to figure this out..
// || hr == SSL_ERROR_SYSCALL)

	// Results
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"sslSktStm:Parent %s:ssl %p(%d): nio %d:Failed:0x%x (%d)\r\n", 
									(LPCWSTR)strNameP, (pssl != NULL) ? pssl->ssl : NULL, 
									(pssl != NULL) ? SSL_get_fd(pssl->ssl) : 0, nio, hr,
									ERR_get_error() );
/*
	lprintf ( LOG_DBG, L"skt %d nio %d pnio %d (nr %d)\r\n", 
					skt, nr, (pnio != NULL) ? *pnio : -1, nr );
//	lprintf ( LOG_DBG, L"skt %d nio %d pnio %d (nleft %d) hr 0x%x iRdIdx %d iRdCnt %d\r\n", 
//					skt, nio, (pnio != NULL) ? *pnio : -1, nleft, hr, iRdIdx, iRdCnt );
	for (int d = 0;d < nio && (pnio == NULL || d < *pnio);++d)
		{
		lprintf ( LOG_DBG, L"%d) %d 0x%x %c\r\n", 
						d, ((U8 *)pvBfr)[d], ((U8 *)pvBfr)[d], ((U8 *)pvBfr)[d] );
		}	// for
//	if (nio > 200)
//		lprintf ( LOG_DBG, L"Hi\r\n" );
*/

	return hr;
	}	// read

HRESULT sslSktStm :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Not supported on a socket
	return E_NOTIMPL;
	}	// seek

HRESULT sslSktStm :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// A socket can be thought of has having any amount of data
	return S_OK;
	}	// setSize

HRESULT sslSktStm :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	const char	*pc	= (const char *)pcvBfr;
	U32			ntot,nw;

	// State check
	CCLTRYE ( (pssl != NULL), ERROR_INVALID_STATE );

	// Fill cache before sending to minimize calls
	CCLOK ( ntot = (U32)nio; )
	CCLOK ( nio = 0; )
	while (hr == S_OK && ntot > 0)
		{
		// Amount to copy into cache
		nw = (ntot < (U32)(SIZE_PERSIST_CACHE-iWrIdx)) ? ntot : (U32)(SIZE_PERSIST_CACHE-iWrIdx);
//		lprintf ( LOG_DBG, L"Cache write : %d / %d / %d bytes\r\n", nw, ntot, nio );

		// Copy into cache
		if (nw > 0)
			{
			// Write to cache
			memcpy ( &bWr[iWrIdx], pc, nw );

			// Next block
			iWrIdx	+= nw;
			pc 		+= nw;
			nio		+= nw;
			ntot	 	-= nw;
			}	// if

		// Is cache full ? 
		if (iWrIdx >= SIZE_PERSIST_CACHE)
			flush();
		}	// while

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nio;

	// Debug
//	if (hr != S_OK)
//		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// write
