////////////////////////////////////////////////////////////////////////
//
//									Bio.CPP
//
//							SSL Basic I/O management
//
////////////////////////////////////////////////////////////////////////

#include "openssll_.h"

// Globals
extern libSSL	libS;

// Definitions
#define	SIZE_BFR_RD		0x100000
#define	SIZE_COPY_BFR	0x100000

Bio :: Bio ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pCtx		= NULL;
	pDct		= NULL;
	pStmWr	= NULL;
	pcBfrWr	= NULL;
	pStmRd	= NULL;
	}	// Bio

HRESULT Bio :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Library reference
	if (bAttach)
		{

		// Library reference
		libS.AddRef();

		// Create a byte stream that will be used to send out encrypted
		// bytes that will need to be sent
		CCLTRY ( COCREATE(L"Io.StmMemory",IID_IByteStream,&pStmWr) );

		// Create a byte stream to receive descrypted bytes.
		CCLTRY ( COCREATE(L"Io.StmMemory",IID_IByteStream,&pStmRd) );
		}	// if
	else
		{
		// Clean up
		_RELEASE(pCtx);
		_RELEASE(pDct);
		_FREEMEM(pcBfrWr);
		_RELEASE(pStmWr);
		_RELEASE(pStmRd);

		// Lbrary reference
		libS.Release();
		}	// else

	return hr;
	}	// onAttach

HRESULT Bio :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open SSL state
	if (_RCP(Open))
		{
		objSSL		*pctx		= NULL;
		objSSL		*pssl		= NULL;
		IThis			*pThis	= NULL;
		BIO			*pbior	= NULL;
		BIO			*pbiow	= NULL;
		sslBioStm	*pStmBio	= NULL;
		sslStm		*pStmSsl	= NULL;
		adtIUnknown	unkV;
		adtValue		vL;

		// State check
		CCLTRYE ( pCtx != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Extract context
		CCLTRY ( pCtx->load ( adtString(L"sslCtx"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IThis,&pThis) );
		CCLTRY ( pThis->getThis ( (void **) &pctx ) );
		CCLTRYE( pctx != NULL && pctx->ctx != NULL, ERROR_INVALID_STATE );

		// Create a new SSL object
		CCLTRYE ( (pssl = new objSSL( SSL_new ( pctx->ctx ) )) != NULL,
						E_UNEXPECTED );
		CCLTRY  ( pDct->store ( adtString(L"ssl"), adtIUnknown(pThis = pssl) ) );

		// For debug
		CCLOK ( SSL_set_info_callback ( pssl->ssl, SSL_get_info_cb ); )

		// Set up memory BIOs by default, make available as an option in future ?
		CCLTRYE ( (pbior = BIO_new ( BIO_s_mem() ) ) != NULL,
						E_UNEXPECTED );
		CCLTRYE ( (pbiow = BIO_new ( BIO_s_mem() ) ) != NULL,
						E_UNEXPECTED );

		// Standard setup
		CCLOK ( BIO_set_mem_eof_return ( pbior, -1 ); )
		CCLOK ( BIO_set_mem_eof_return ( pbiow, -1 ); )

		// Assign to the SSL context
		CCLOK ( SSL_set_bio ( pssl->ssl, pbior, pbiow ); )

		// Create a BIO byte stream for transfers to/form the BIOs.
		CCLTRYE ( (pStmBio = new sslBioStm ( this, pDct )) != NULL, 
						E_OUTOFMEMORY );
		CCLTRY ( pDct->store ( adtString(L"StreamBio"), adtIUnknown((IByteStream *)pStmBio) ) );

		// Create a SSL byte stream for application data to/from the BIO stream.
		CCLTRYE ( (pStmSsl = new sslStm ( pStmBio )) != NULL, 
						E_OUTOFMEMORY );
		CCLTRY ( pDct->store ( adtString(L"StreamSsl"), adtIUnknown((IByteStream *)pStmSsl) ) );

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pStmSsl);
		_RELEASE(pStmBio);
		_RELEASE(pThis);
		}	// if

	// Close SSL state
	else if (_RCP(Close))
		{
		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// About to close
		CCLOK ( _EMT(Close,adtIUnknown(pDct)); )

		// Done w/context, this will free the embedded context
		if (pDct != NULL)
			{
			pDct->remove ( adtString(L"ssl") );
			pDct->remove ( adtString(L"StreamBio") );
			pDct->remove ( adtString(L"StreamSsl") );
			}	// if

		}	// else if

	// Set connection state
	else if (_RCP(Accept) || _RCP(Connect))
		{
		objSSL		*pssl		= NULL;

		// Access SSL object
		CCLTRY	( Context::context ( pDct, L"ssl", &pssl ) );

		// Accept or connect specified ?
		if (hr == S_OK && _RCP(Accept))
			SSL_set_accept_state ( pssl->ssl );
		else if (hr == S_OK && _RCP(Connect))
			{
			// Set the connection state
			SSL_set_connect_state ( pssl->ssl );

			// Initiate the hand shake for clients
			SSL_do_handshake ( pssl->ssl );

			// Notify outside world if there is data to send
			CCLOK ( writes(pssl->ssl); )
			}	// else if

		// Clean up
		_RELEASE(pssl);
		}	// else if

	// Query information about connection
	else if (_RCP(Query))
		{
		objSSL		*pssl		= NULL;
		X509			*cert		= NULL;
		adtValue		vL;
		adtIUnknown	unkV;

		// Access SSL object
		CCLTRY	( Context::context ( pDct, L"ssl", &pssl ) );

		// Obtain the client certificate
		if (	hr == S_OK && 
				(cert = SSL_get_peer_certificate ( pssl->ssl )) != NULL )
			{
			adtString	strV;
			char			*str	= NULL;
			EVP_PKEY		*key	= NULL;

			// Fill in relevant information.
			CCLTRY ( pDct->store ( adtString(L"X509Version"),
											adtInt(X509_get_version(cert)) ) );
			if (	hr == S_OK &&	
					(str = X509_NAME_oneline(X509_get_subject_name(cert),0,0)) != NULL)
				{
				hr = pDct->store ( adtString(L"X509SubjectName"), (strV = str) );
				OPENSSL_free(str);
				}	// if
			if (	hr == S_OK &&	
					(str = X509_NAME_oneline(X509_get_issuer_name(cert),0,0)) != NULL)
				{
				hr = pDct->store ( adtString(L"X509IssuerName"), (strV = str) );
				OPENSSL_free(str);
				}	// if

			// Public key in string format
			if	(	hr == S_OK &&
					(key = X509_get_pubkey ( cert )) != NULL )
				{
				IByteStream *pStm		= NULL;
				BYTE			*pbKey	= NULL;
				int			ret		= 0;

				// Extract key
				CCLTRYE ( (ret = i2d_PUBKEY(key,&pbKey)) > 0, E_UNEXPECTED );
				CCLTRYE ( pbKey != NULL, E_UNEXPECTED );

				// Store as a byte stream
				CCLTRY ( COCREATE ( L"Io.StmMemory", IID_IByteStream, &pStm ) );
				CCLTRY ( pStm->write ( pbKey, ret, NULL ) );
				CCLOK  ( pStm->seek ( 0, STREAM_SEEK_SET, NULL ); )

				// Store as key
				CCLTRY ( pDct->store ( adtString(L"X509PublicKey"), adtIUnknown(pStm) ) );

				// Clean up
				_RELEASE(pStm);
				}	// if

			// Clean up
			X509_free(cert);
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Query,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pssl);
		}	// else if

	// State
	else if (_RCP(Context))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pCtx);
		hr = _QISAFE(unkV,IID_IDictionary,&pCtx);
		}	// else if
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive

HRESULT Bio :: reads ( sslBioStm *pStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Cache any application data that might be available.  Calling
	//			'SSL_read' is also apparently necessary for the internal
	//			state machine of SSL even if there is no app data.
	//
	//	PARAMETERS
	//		-	pStm is the BIO stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	size_t	nr,nrt,nrf;
	int		ret;

	// Debug
//	lprintf ( LOG_DBG, L"pStm %p\r\n", pStm );

	// Thread safety
	pStm->csRd.enter();
	nr = 0;

	// Number of bytes in cache
	nrt = (pStm->ifRd+sizeof(pStm->bRd)-pStm->ibRd) % sizeof(pStm->bRd);

	// Remaining space for new bytes
	nrt = (sizeof(pStm->bRd) - nrt);

	// Amount to read first ( to avoid copying past end )
	nrf = (pStm->ifRd + nrt >= sizeof(pStm->bRd)) ? 
			(sizeof(pStm->bRd) - pStm->ifRd) : nrt;

	// Read available bytes
//	lprintf ( LOG_DBG, L"SSL_read 1 hr %x nr %d ifRd %d nrf %d\r\n",
//								hr, nr, pStm->ifRd, nrf );
	CCLTRYE ( (ret = SSL_read_ex ( pStm->pssl->ssl, &pStm->bRd[pStm->ifRd], (int)nrf, &nr )
					> 0), SSL_get_error(pStm->pssl->ssl,(int)nr) );
	CCLOK ( pStm->ifRd = (pStm->ifRd + nr) % sizeof(pStm->bRd); )
//	lprintf ( LOG_DBG, L"SSL_read 1 hr %x nr %d ifRd %d nrf %d\r\n",
//								hr, nr, pStm->ifRd, nrf );

	// More to read ?
	if (hr == S_OK && nrf < nrt && nr == nrf)
		{
		// Read available bytes
//		lprintf ( LOG_DBG, L"SSL_read 2 hr %x nr %d ifRd %d nrf %d\r\n",
//									hr, nr, pStm->ifRd, (nrt-nrf) );
		CCLTRYE ( (ret = SSL_read_ex ( pStm->pssl->ssl, &pStm->bRd[pStm->ifRd], (int)(nrt-nrf), &nr )
						> 0), SSL_get_error(pStm->pssl->ssl,(int)nr) );
		CCLOK ( pStm->ifRd = (pStm->ifRd + nr) % sizeof(pStm->bRd); )
//		lprintf ( LOG_DBG, L"SSL_read 2 hr %x nr %d ifRd %d nrf %d\r\n",
//									hr, nr, pStm->ifRd, (nrt-nrf) );
		}	// if

	// Update non-empty event
	if (pStm->ifRd != pStm->ibRd)
		pStm->evRd.signal();
	else
		pStm->evRd.reset();

	// Thread safety
	pStm->csRd.leave();

	// Debug
//	lprintf ( LOG_DBG, L"%p: nr %d hr %d\r\n", this, nr, hr );

	// Data is avaialble
	if (hr == S_OK && nr > 0)
		_EMT(Read,adtInt(0));

	return hr;
	}	// reads

void Bio :: SSL_get_info_cb ( const SSL *ssl, int where, int ret )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Debug callback for SSL
	//
	//	PARAMETERS
	//		-	where is where this callback was called
	//		-	ret is the return value (0 on error)
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"where %d ret %d\r\n", where, ret );
	}	// SSL_get_info_cb

HRESULT Bio :: stream (	IDictionary *pDct, const WCHAR *pwName,
								IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Retrieve the persistence stream for the SSL layer.
	//
	//	PARAMETERS
	//		-	pDct is the SSL dictionary
	//		-	pwName is the name of the requestor (for debug)
	//		-	ppStm will receive the stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	objSSL		*pssl		= NULL;
	IThis			*pThis	= NULL;
	adtValue		vL;
	adtIUnknown	unkV;

	// State check
	CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

	// Access SSL object
	CCLTRY	( pDct->load ( adtString(L"SSL"), vL ) );
	CCLTRY	( _QISAFE((unkV=vL),IID_IThis,&pThis) );
	CCLTRY	( pThis->getThis ( (void **) &pssl ) );

	// Does socket have a persistence stream yet ?
	if (hr == S_OK)
		{
		// Need a new stream ?
		if (pDct->load ( adtString(L"Stream"), vL ) != S_OK)
			{
//			lprintf ( LOG_DBG, L"New stream for socket %d\r\n", skt );

			// Persistence stream object
			CCLTRYE ( (*ppStm = new sslBioStm ( this, pDct )) != NULL, E_OUTOFMEMORY );
			(*ppStm)->AddRef();

			// Store in dictionary
			CCLTRY ( pDct->store ( adtString(L"Stream"), adtIUnknown(*ppStm) ) );
			}	// if

		// Existing stream
		else
			{
			CCLTRY ( _QISAFE((unkV=vL),IID_IByteStream,ppStm) );
			}	// else
		}	// if

	// Clean up
	_RELEASE(pThis);

	return hr;
	}	// stream

HRESULT Bio :: writes ( SSL *ssl )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Sends out encryptes bytes the SSL layer might have written.
	//
	//	PARAMETERS
	//		-	ssl is the SSL context
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	BIO		*pbio	= NULL;
	size_t	pending,nr;

	// Debug
//	lprintf ( LOG_DBG, L"ssl %p\r\n", ssl );

	// Setup
	CCLTRYE ( (pbio = SSL_get_wbio(ssl)) != NULL, E_UNEXPECTED );

	// Debug
//	lprintf ( LOG_DBG, L"%p: hr 0x%x\r\n", this, hr );

	// Pending data in the BIO ?
	if (hr == S_OK)
		{
		pending = BIO_ctrl_pending(pbio);
//		lprintf ( LOG_DBG, L"Pending Write %d\r\n", pending );
		if (pending == 0)
			return S_OK;
		}	// if

	// Match size of incoming data
	CCLTRY ( pStmWr->setSize ( pending ) );
	CCLTRY ( pStmWr->seek ( 0, STREAM_SEEK_SET, NULL ) );
	CCLTRYE( (pcBfrWr = (U8 *) _REALLOCMEM ( pcBfrWr, (U32)pending )) != NULL,
					E_OUTOFMEMORY );

	// Transfer pending data into write stream
	CCLTRYE ( BIO_read_ex ( pbio, pcBfrWr, pending, &nr ) != 0, E_UNEXPECTED );
	CCLTRY ( pStmWr->write ( pcBfrWr, nr, NULL ) );
	CCLTRY ( pStmWr->seek ( 0, STREAM_SEEK_SET, NULL ) );

	// Notify outside world
	CCLOK ( _EMT(Write,adtIUnknown(pStmWr)); )

	return hr;
	}	// writes

//
//	sslBioStm
//

sslBioStm :: sslBioStm ( Bio *_pThis, IDictionary *_pDct )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	_pThis is the parent node
	//		-	_pDct is the SSL context dictionary
	//
	////////////////////////////////////////////////////////////////////////
	pThis		= _pThis;
	pDct		= _pDct;
	_ADDREF(pDct);
	puCpy		= NULL;
	ifRd		= 0;
	ibRd		= 0;

	// Initial manual set event for non-empty
	evRd.init(true);

	// Auto reference on new
	AddRef();

	// Extract SSL context
	Context::context ( pDct, L"ssl", &pssl );
	}	// sslBioStm

sslBioStm :: ~sslBioStm ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pssl);
	_RELEASE(pDct);
	_FREEMEM(puCpy);
	}	// ~sslBioStm

HRESULT sslBioStm :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Does the write BIO have any encrypted bytes that need to be written out ? 
	if (hr == S_OK && puAv != NULL)
		*puAv = BIO_ctrl_pending(SSL_get_wbio(pssl->ssl));

	return hr;
	}	// available

HRESULT sslBioStm :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
	U64		nio,nw,nr;

	// State check
	CCLTRYE ( pssl != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( uSz != 0, ERROR_INVALID_STATE );

	// Amount of data available to read
	CCLTRY ( available(&nio) );

	// Amount to copy based on requested amount
	CCLOK ( uSz = (uSz != 0 && uSz < nio) ? uSz : nio; )

	// Using on demand, large, internal copy buffer for speed
	if (hr == S_OK && puCpy == NULL)
		{
		CCLTRYE ( (puCpy = (U8 *) _ALLOCMEM ( SIZE_COPY_BFR )) != NULL,
						E_OUTOFMEMORY );
		}	// if

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK ( nio = (SIZE_COPY_BFR < uSz) ? SIZE_COPY_BFR : uSz; )
		CCLTRY( read ( puCpy, nio, &nr ) );

		// Any bytes available ?
		if (hr == S_OK && nr == 0)
			{
			lprintf ( LOG_DBG, L"Successful read of 0 bytes uSz %ld nio %d\r\n", uSz, nio );
			break;
			}	// if

		// Write full block to stream
		CCLOK ( fp		= puCpy; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;

		}	// while

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"sslBioStm::copyTo:Failed:0x%x\r\n", hr );

	return hr;
	}	// copyTo

HRESULT sslBioStm :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Nothing really to do here, bytes hang out in the memory BIO.
	return S_OK;
	}	// flush

HRESULT sslBioStm :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads encrypted bytes from the BIO.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	BIO		*pbio	= NULL;
	size_t	pending,nr;

	// Does the write BIO have any encrypted bytes that need to be written 
	// out ? Call this read to get the bytes and then write them
	// to the outgoing communication channels.

	// Setup
	CCLTRYE ( (pbio = SSL_get_wbio(pssl->ssl)) != NULL, E_UNEXPECTED );
	if (pnio != NULL) *pnio = 0;

	// Any pending data in the BIO ?
	if (hr == S_OK && (pending = BIO_ctrl_pending(pbio)) == 0)
		return S_OK;

	// Amount to read will depend on pending and requested amount
	CCLOK ( nr = (nio < pending) ? nio : pending; )

	// Transfer pending write data
	CCLTRYE ( BIO_read_ex ( pbio, pvBfr, nr, &nr ) != 0, E_UNEXPECTED );

	// Debug
//	lprintf ( LOG_DBG, L"pvBfr %p nio %d pnio %p nr %d hr 0x%x\r\n",
//					pvBfr, nio, pnio, nr, hr );

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	return hr;
	}	// read

HRESULT sslBioStm :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Not supported on a BIO
	return E_NOTIMPL;
	}	// seek

HRESULT sslBioStm :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// BIOs grow automatically
	return S_OK;
	}	// setSize

HRESULT sslBioStm :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the encrypted bytes into the BIO.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	BIO		*pbio	= NULL;
	size_t	nw;

	// Call this to write encrypted data into the 'read' BIO for SSL processing.

	// Setup
	CCLTRYE ( (pbio = SSL_get_rbio(pssl->ssl)) != NULL, E_UNEXPECTED );
	if (pnio != NULL) *pnio = 0;

	// Send into BIO for processing
	CCLTRYE ( BIO_write_ex ( pbio, pcvBfr, nio, &nw ) != 0, E_UNEXPECTED );

	// Debug
//	lprintf ( LOG_DBG, L"pcvBfr %p nio %d pnio %p nw %d hr 0x%x\r\n",
//					pcvBfr, nio, pnio, nw, hr );

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nw;

	// If handshake is happening, step it
	if (hr == S_OK && !SSL_is_init_finished(pssl->ssl))
		{
		// Step the handshake
		int ret = SSL_do_handshake(pssl->ssl);

		// Handshake complete
		if (ret >= 1)
			{
			// SSL side of connection is complete
			lprintf ( LOG_DBG, L"SSL Handshake complete (last write %d)\r\n", nw );
			pThis->peOnConnect->receive(pThis,L"Value",adtIUnknown(pDct));
			}	// if
		else if (ret == 0)
			lprintf ( LOG_DBG, L"SSL_do_handshake was not successful but was shutdown by spec, error %d\r\n", 
								ret, SSL_get_error(pssl->ssl,ret) );
		else
			lprintf ( LOG_DBG, L"SSL_do_handshake not yet complete, error %d\r\n", SSL_get_error(pssl->ssl,ret) );
		}	// if

	// State of BIOs has changed
	CCLOK ( pThis->writes(pssl->ssl); )
	CCLOK ( pThis->reads(this); )

	// DEBUG
//	lprintf ( LOG_DBG, L"Pending Read %d\r\n",	BIO_ctrl_pending(SSL_get_rbio(pssl->ssl)) );
//	lprintf ( LOG_DBG, L"Pending Write %d\r\n",	BIO_ctrl_pending(SSL_get_wbio(pssl->ssl)) );
//	lprintf ( LOG_DBG, L"Want Read %d\r\n",		SSL_want_read(pssl->ssl) );
//	lprintf ( LOG_DBG, L"Want Write %d\r\n",		SSL_want_write(pssl->ssl) );
//	lprintf ( LOG_DBG, L"SSL_Pending %d\r\n",		SSL_pending(pssl->ssl) );

	return hr;
	}	// write

//
//	sslStm
//

sslStm :: sslStm ( sslBioStm *_pStmBio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	//	PARAMETERS
	//		-	_pStmBio is the pair BIO stream.
	//
	////////////////////////////////////////////////////////////////////////
	pStmBio	= _pStmBio;
	_ADDREF(pStmBio);
	puCpy		= NULL;
	iWrIdx	= 0;

	// Auto reference on new
	AddRef();
	}	// sslStm

sslStm :: ~sslStm ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pStmBio);
	_FREEMEM(puCpy);
	}	// ~sslStm

HRESULT sslStm :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Pending bytes in how much is in the cache
	if (puAv != NULL)
		*puAv = (pStmBio->ifRd+sizeof(pStmBio->bRd)-pStmBio->ibRd) 
					% sizeof(pStmBio->bRd);

	return hr;
	}	// available

HRESULT sslStm :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
	U64		nio,nw,nr;

	// State check
	CCLTRYE ( pStmBio->pssl != NULL, ERROR_INVALID_STATE );
	CCLTRYE ( uSz != 0,					ERROR_INVALID_STATE );

	// Amount to copy based on requested amount
//	CCLOK ( uSz = (uSz != 0 && uSz < nio) ? uSz : nio; )

	// Using on demand, large, internal copy buffer for speed
	if (hr == S_OK && puCpy == NULL)
		{
		CCLTRYE ( (puCpy = (U8 *) _ALLOCMEM ( SIZE_COPY_BFR )) != NULL,
						E_OUTOFMEMORY );
		}	// if

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Wait for data to become available
		CCLTRYE ( pStmBio->evRd.wait(2000) == true, ERROR_TIMEOUT );

		// Read next block
		CCLOK ( nio = (SIZE_COPY_BFR < uSz) ? SIZE_COPY_BFR : uSz; )
		CCLTRY( read ( puCpy, nio, &nr ) );

		// Any bytes available ?
		if (hr == S_OK && nr == 0)
			{
			lprintf ( LOG_DBG, L"Successful read of 0 bytes uSz %ld nio %d\r\n", uSz, nio );
			break;
			}	// if

		// Write full block to stream
		CCLOK ( fp		= puCpy; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;
		}	// while

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"sslStm::copyTo:Failed:0x%x\r\n", hr );

	return hr;
	}	// copyTo

HRESULT sslStm :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	U32			nleft,nw;

	// Flush will send out anything in the write cache
	if (iWrIdx > 0)
		{
		// Debug
//		adtDate then;
//		then.now();
//		lprintf ( LOG_DBG, L"skt 0x%x iWrIdx %d \r\n", skt, iWrIdx );

		// Write all the data
		CCLOK ( nleft 	= iWrIdx; )
		CCLOK ( iWrIdx = 0; )
		while (hr == S_OK && nleft > 0)
			{
			// Next write
//			CCLTRY ( SslSkt_Send ( pssl->ssl, &bWr[iWrIdx], nleft, &nw, 10000 ) );
			nw = SSL_write ( pStmBio->pssl->ssl, &bWr[iWrIdx], nleft );
//			if (nw > 1024)
//				lprintf ( LOG_DBG, L"skt 0x%x nleft %d nw %d hr 0x%x\r\n", skt, nleft, nw, hr );
//			lprintf ( LOG_DBG, L"SSL_write ssl %p, iWrIdx %d nleft %d nw %d\r\n",
//										pStmBio->pssl->ssl, iWrIdx, nleft, nw );

			// Result
			CCLOK ( nleft	-= nw; )
			CCLOK ( iWrIdx	+= nw; )
			}	// while

		// Debug
//		adtDate now;
//		now.now();
//		lprintf ( LOG_DBG, L"skt 0x%x iWrIdx %d hr 0x%x time %g\r\n", 
//									skt, iWrIdx, hr, ((now-then)*SECINDAY) );

		// Cache empty
		iWrIdx = 0;

		// Write application data could/will result in encrypted data
		// needing to be sent out.
		CCLOK ( pStmBio->pThis->writes(pStmBio->pssl->ssl); )
		CCLOK ( pStmBio->pThis->reads(pStmBio); )
		}	// if

	return hr;
	}	// flush

HRESULT sslStm :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads encrypted bytes from the BIO.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	size_t	nr,nrf;
	U64		nt;

	// Wait for data to become available
	CCLTRYE ( pStmBio->evRd.wait(2000) == true, ERROR_TIMEOUT );

	// Thread safety
	pStmBio->csRd.enter();

	// Total amount of available bytes
	CCLTRY ( available(&nt) );

	// Total amount to read
	CCLOK ( nt = (nio < nt) ? nio : nt; )

	// Debug
//	if (pStmBio->ibRd + nt > sizeof(pStmBio->bRd))
//		DebugBreak();

	// Amount to read first ( to avoid copying past end )
	nrf = (pStmBio->ibRd + nt > sizeof(pStmBio->bRd)) ? 
			(sizeof(pStmBio->bRd) - pStmBio->ibRd) : nt;

	// Copy out available bytes
	nr = 0;
	if (hr == S_OK && nt > 0)
		{
		// Copy portion of cache
		memcpy ( pvBfr, &pStmBio->bRd[pStmBio->ibRd], nrf );

		// Bytes consumed
		pStmBio->ibRd	= ( (pStmBio->ibRd + nrf ) % sizeof(pStmBio->bRd) );
		nr					= nrf;

		// More to read ?
		if (hr == S_OK && nrf < nt)
			{
			// Copy portion of cache
			memcpy ( pvBfr, &pStmBio->bRd[pStmBio->ibRd], (nt-nrf) );

			// Bytes consumed
			pStmBio->ibRd	= ( (pStmBio->ibRd + (nt-nrf) ) % sizeof(pStmBio->bRd) );
			nr					+= (nt-nrf);
			}	// if

		}	// if

	// Debug
//	lprintf ( LOG_DBG, L"pvBfr %p nio %d pnio %p nr %d hr 0x%x\r\n",
//					pvBfr, nio, pnio, nr, hr );

	// Read available bytes 
//	CCLTRYE ( (nr = SSL_read ( pStmBio->pssl->ssl, pvBfr, (U32)nr )) > 0,
//					SSL_get_error(pStmBio->pssl->ssl,(int)nr) );

	// Thread safety
	pStmBio->csRd.leave();

	// Results
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"%s:ssl %p: nio %d:Failed:0x%x (%d)\r\n", 
									(LPCWSTR)pStmBio->pThis->strnName, pStmBio->pssl->ssl,
									nio, hr, ERR_get_error() );

	// Whenever the internal cache runs dry, see if there is more data waiting in the Bio.
	// What is the real algorithm that should be used here ?  Sitting in a loop doing "reads"
	// until SSL_WANT_READ just hangs everything.
	if (pStmBio->ibRd == pStmBio->ifRd)
		pStmBio->pThis->reads(pStmBio);

	return hr;
	}	// read

HRESULT sslStm :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Not supported on a BIO
	return E_NOTIMPL;
	}	// seek

HRESULT sslStm :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// BIOs grow automatically
	return S_OK;
	}	// setSize

HRESULT sslStm :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the encrypted bytes into the BIO.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	const char	*pc	= (const char *)pcvBfr;
	U32			ntot,nw;

	// State check
	CCLTRYE ( (pStmBio->pssl != NULL), ERROR_INVALID_STATE );

	// Fill cache before sending to minimize calls
	CCLOK ( ntot = (U32)nio; )
	CCLOK ( nio = 0; )
	while (hr == S_OK && ntot > 0)
		{
		// Amount to copy into cache
		nw = (ntot < (U32)(sizeof(bWr)-iWrIdx)) ? ntot : (U32)(sizeof(bWr)-iWrIdx);
//		lprintf ( LOG_DBG, L"Cache write : %d / %d / %d bytes\r\n", nw, ntot, nio );

		// Copy into cache
		if (nw > 0)
			{
			// Write to cache
			memcpy ( &bWr[iWrIdx], pc, nw );

			// Next block
			iWrIdx	+= nw;
			pc 		+= nw;
			nio		+= nw;
			ntot	 	-= nw;
			}	// if

		// Is cache full ? 
		if (iWrIdx >= sizeof(bWr))
			flush();
		}	// while

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = nio;

	// Debug
//	if (hr != S_OK)
//		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// write
