////////////////////////////////////////////////////////////////////////
//
//										OPENSSLL_.H
//
//			Implementaiton include file for the Open SSL node library
//
////////////////////////////////////////////////////////////////////////

#ifndef	OPENSSLL__H
#define	OPENSSLL__H

// Includes
#include	"openssll.h"

// OpenSSL
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

// Winsock

#if	defined(_WIN32)
#include <winsock2.h>
#include <ws2tcpip.h>
#include <IPHlpApi.h>
#elif	defined(__unix__) || defined(__APPLE__)

// Includes
#include <sys/socket.h>

// Definitions
#define	INVALID_SOCKET				-1
#define	SOCKET_ERROR				-1
#define	closesocket					close
#define	WSAGetLastError()			errno
typedef	int							SOCKET;

#endif

#define	SIZE_PERSIST_CACHE		0x100000
//#define	SIZE_PERSIST_CACHE		2048

////////////
// Objects
////////////

//
// Class - libSSL.  OpenSSL library object.
//

class libSSL
	{
	public :
	libSSL ( void );										// Constructor

	// Run-time data
	bool				bValid;								// Library valid
	U32				lcnt;									// Reference count

	// Utilities
	virtual LONG		AddRef		();				// Add reference to library
	virtual HRESULT	errors		( const WCHAR * );
	virtual LONG		Release		();				// Remove reference from library
//	virtual LONG		sslAddRef	();				// Add reference to SSL library
//	virtual LONG		sslRelease	();				// Release reference to SSL library
	};

//
// Class - objSSL.  Reference counted OpenSSL objects.
//

class objSSL :
	public CCLObject,										// Base class
	public IThis											// Interface
	{
	public :
	objSSL ( RSA * );										// Constructor
	objSSL ( SSL_CTX * );								// Constructor
	objSSL ( SSL * );										// Constructor

	// Run-time data
	RSA		*rsa;											// RSA Object
	SSL_CTX	*ctx;											// SSL context
	SSL		*ssl;											// SSL object

	// 'IThis' members
	STDMETHOD(getThis)	( void **ppv ) { *ppv = this; return S_OK; }

	// CCL
	CCL_OBJECT_BEGIN_INT(objSSL)
		CCL_INTF(IThis)
	CCL_OBJECT_END()
	virtual void		destruct	( void );			// Destruct object
	};

//
// Class - sslBioStm.  A byte stream for a pair of BIOs.
//
class Bio;
class sslBioStm :
	public CCLObject,										// Base class
	public IByteStream									// Interface
	{
	public :
	sslBioStm ( Bio *, IDictionary * );				// Constructor
	virtual ~sslBioStm ( void );						// Destructor

	// Run-time data
	Bio			*pThis;									// Parent object
	IDictionary	*pDct;									// SSL context
	objSSL		*pssl;									// Extracted SSL object
	U8				*puCpy;									// Copy buffer
	U8				bRd[SIZE_PERSIST_CACHE];			// Read buffer
	S32			ifRd,ibRd;								// Read queue positions
	sysCS			csRd;										// Read mutex
	sysEvent		evRd;										// Event if there is data to read

	// 'IByteStream' members
	STDMETHOD(available)	( U64 * );
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * );
	STDMETHOD(flush)		( void );
	STDMETHOD(read)		( void *, U64, U64 * );
	STDMETHOD(seek)		( S64, U32, U64 * );
	STDMETHOD(setSize)	( U64 );
	STDMETHOD(write)		( void const *, U64, U64 * );

	// CCL
	CCL_OBJECT_BEGIN_INT(sslBioStm)
		CCL_INTF(IByteStream)
	CCL_OBJECT_END()

	};

//
// Class - sslStm.  An application byte stream for I/O.
//
class sslStm :
	public CCLObject,										// Base class
	public IByteStream									// Interface
	{
	public :
	sslStm ( sslBioStm * );								// Constructor
	virtual ~sslStm ( void );							// Destructor

	// Run-time data
	sslBioStm	*pStmBio;								// Paired BIO stream
	U8				*puCpy;									// Copy buffer
	U8				bWr[SIZE_PERSIST_CACHE];			// Write cache
	S32			iWrIdx;									// Write cache position

	// 'IByteStream' members
	STDMETHOD(available)	( U64 * );
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * );
	STDMETHOD(flush)		( void );
	STDMETHOD(read)		( void *, U64, U64 * );
	STDMETHOD(seek)		( S64, U32, U64 * );
	STDMETHOD(setSize)	( U64 );
	STDMETHOD(write)		( void const *, U64, U64 * );

	// CCL
	CCL_OBJECT_BEGIN_INT(sslStm)
		CCL_INTF(IByteStream)
	CCL_OBJECT_END()

	private :
	};

//
// Class - sslSktStm.  An SSL socket stream for I/O.
//
class sslSktStm :
	public CCLObject,										// Base class
	public IByteStream									// Interface
	{
	public :
	sslSktStm ( const WCHAR *, objSSL * );			// Constructor
	virtual ~sslSktStm ( void );						// Destructor

	// Run-time data
	adtString		strNameP;							// Parent name
	objSSL			*pssl;								// SSL object
	U8					bWr[SIZE_PERSIST_CACHE];		// Write cache
	S32				iWrIdx;								// Write cache position
	U8					bRd[SIZE_PERSIST_CACHE];		// Read cache
	S32				iRdIdx,iRdCnt;						// Read cache index/count
	U8					*puCpy;								// Copy buffer

	// 'IByteStream' members
	STDMETHOD(available)	( U64 * );
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * );
	STDMETHOD(flush)		( void );
	STDMETHOD(read)		( void *, U64, U64 * );
	STDMETHOD(seek)		( S64, U32, U64 * );
	STDMETHOD(setSize)	( U64 );
	STDMETHOD(write)		( void const *, U64, U64 * );

	// CCL
	CCL_OBJECT_BEGIN_INT(sslSktStm)
		CCL_INTF(IByteStream)
	CCL_OBJECT_END()
	};

/////////
// Nodes
/////////

//
// Class - Bio.  Basic I/O abstraction for SSL.
//

class Bio :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Bio ( void );											// Constructor

	// Run-time data
	IDictionary		*pCtx;								// SSL context
	IDictionary		*pDct;								// SSL dictionar
	IByteStream		*pStmWr;								// Encrypted bytes to write
	U8					*pcBfrWr;							// Write buffer
	IByteStream		*pStmRd;								// Unencrypted bytes read

	// Utlities
	HRESULT	reads		( sslBioStm * );				// Check if there is pending BIO reads
	HRESULT	writes	( SSL * );						// Check if there is pending BIO writes

	// CCL
	CCL_OBJECT_BEGIN(Bio)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Context)
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Accept)
	DECLARE_CON(Connect)
	DECLARE_CON(Open)
	DECLARE_CON(Close)
	DECLARE_CON(Query)
	DECLARE_EMT(Read)
	DECLARE_EMT(Write)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Context)
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Accept)
		DEFINE_CON(Connect)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_CON(Query)
		DEFINE_EMT(Read)
		DEFINE_EMT(Write)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	HRESULT	stream ( IDictionary *, const WCHAR *, IByteStream ** );
	static void SSL_get_info_cb ( const SSL *, int, int );
	};

//
// Class - Context.  Node to handle SSL/TLS contexts.
//

class Context :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Context ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context
	adtBool			bPeerCert;							// Require peer certificate

	// Extract objSSL context from a dictionary
	static HRESULT context	( IDictionary *, const WCHAR *, objSSL ** );
	static int		verify	( int, X509_STORE_CTX * );

	// CCL
	CCL_OBJECT_BEGIN(Context)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Open)
	DECLARE_CON(Close)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - EVPSign.  Node for EVP signing functionality.
//

class EVPSign :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	EVPSign ( void );										// Constructor

	// Run-time data
	adtString		strType;								// Signature type
	EVP_MD_CTX		*pctx;								// EVP context
	IByteStream		*pStm;								// Current stream
	char				cBfr[1024];							// Stream buffer
	IThis				*pKey;								// Private key

	// CCL
	CCL_OBJECT_BEGIN(EVPSign)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Begin)
	DECLARE_EMT(Error)
	DECLARE_CON(Final)
	DECLARE_RCP(Key)
	DECLARE_RCP(Stream)
	DECLARE_CON(Update)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Begin)
		DEFINE_EMT(Error)
		DEFINE_CON(Final)
		DEFINE_RCP(Key)
		DEFINE_RCP(Stream)
		DEFINE_CON(Update)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - EVPVerify.  Node for EVP signing verification functionality.
//

class EVPVerify :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	EVPVerify ( void );									// Constructor

	// Run-time data
	adtString		strType;								// Signature type
	EVP_MD_CTX		*pctx;								// EVP context
	IByteStream		*pStm;								// Current stream
	char				cBfr[1024];							// Stream buffer
	IThis				*pKey;								// Private key

	// CCL
	CCL_OBJECT_BEGIN(EVPVerify)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Begin)
	DECLARE_EMT(Error)
	DECLARE_CON(Final)
	DECLARE_RCP(Key)
	DECLARE_RCP(Stream)
	DECLARE_CON(Update)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Begin)
		DEFINE_EMT(Error)
		DEFINE_CON(Final)
		DEFINE_RCP(Key)
		DEFINE_RCP(Stream)
		DEFINE_CON(Update)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - PEMImpl.  Node to perform PEM algorithms.
//

class PEMImpl :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	PEMImpl ( void );										// Constructor

	// Run-time data
	IByteStream		*pStm;								// Byte stream
	IThis				*pObj;								// Active object
	adtBool			bPub;									// Public/private
	adtString		strType;								// Key type

	// CCL
	CCL_OBJECT_BEGIN(PEMImpl)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_RCP(Object)
	DECLARE_CON(Read)
	DECLARE_RCP(Stream)
	DECLARE_CON(Write)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_RCP(Object)
		DEFINE_CON(Read)
		DEFINE_RCP(Stream)
		DEFINE_CON(Write)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - RSAImpl.  Node to perform RSA algorithms.
//

class RSAImpl :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	RSAImpl ( void );											// Constructor

	// Run-time data
	adtInt			iBits;								// Run-time data

	// CCL
	CCL_OBJECT_BEGIN(RSAImpl)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Bits)
	DECLARE_EMT(Error)
	DECLARE_CON(Generate)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Bits)
		DEFINE_EMT(Error)
		DEFINE_CON(Generate)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - SocketOp.  Node to handle SSL/TLS socket logic.
//

class SocketOp :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	SocketOp ( void );									// Constructor

	// Run-time data
	IDictionary		*pCtx;								// SSL context
	IDictionary		*pSkt;								// Socket context

	// Utilities
	static HRESULT sktStm ( IDictionary *, const WCHAR *, IByteStream ** );

	// CCL
	CCL_OBJECT_BEGIN(SocketOp)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Accept)
	DECLARE_CON(Attach)
	DECLARE_CON(Connect)
	DECLARE_CON(Context)
	DECLARE_RCP(Query)
	DECLARE_RCP(Socket)
	DECLARE_CON(Stream)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Accept)
		DEFINE_CON(Attach)
		DEFINE_CON(Connect)
		DEFINE_CON(Context)
		DEFINE_RCP(Query)
		DEFINE_RCP(Socket)
		DEFINE_CON(Stream)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	HRESULT context ( IDictionary *, objSSL ** );

	};

//
// Class - SSLConnect.  Node to handle SSL connection.
//

class SSLConnect :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	SSLConnect ( void );									// Constructor

	// Run-time data
	IDictionary		*pDsc;								// Descriptor
	IByteStream		*pStm;								// I/O stream
	adtInt			iSz;									// I/O size

	// CCL
	CCL_OBJECT_BEGIN(SSLConnect)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Attach)
	DECLARE_CON(Detach)
	DECLARE_RCP(Descriptor)
	DECLARE_CON(Read)
	DECLARE_RCP(Size)
	DECLARE_RCP(Stream)
	DECLARE_CON(Write)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Attach)
		DEFINE_CON(Detach)
		DEFINE_RCP(Descriptor)
		DEFINE_CON(Read)
		DEFINE_RCP(Size)
		DEFINE_RCP(Stream)
		DEFINE_CON(Write)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :
	// Internal utilities
	HRESULT connect ( SSL * );

	};

// Prototypes
HRESULT SslSkt_Receive	( SSL *ssl, void *pvBfr, U32 nio, U32 *pnio, U32 toms );
HRESULT SslSkt_Send		( SSL *ssl, void const *pcvBfr, U32 nio, U32 *pnio, U32 toms );

#endif
