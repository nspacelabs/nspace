////////////////////////////////////////////////////////////////////////
//
//									OPENSSLL.CPP
//
//								General utilities
//
////////////////////////////////////////////////////////////////////////

#include "openssll_.h"

// Globals
libSSL	libS;

//
// libSSL
//

libSSL :: libSSL ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	bValid	= false;
	lcnt		= 0;
	}	// libSSL

LONG libSSL :: AddRef ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Adds a reference count to the library.  Attempts load
	//			on first count.
	//
	//	RETURN VALUE
	//		Current reference count
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Reference count
	if (InterlockedIncrement(&lcnt) != 1)
		return lcnt;

	// Initialize library
	CCLTRYE ( SSL_library_init() == 1, E_UNEXPECTED );
	CCLTRYE ( SSL_load_error_strings() == 1, E_UNEXPECTED );
	CCLTRYE ( OpenSSL_add_all_algorithms() == 1, E_UNEXPECTED );

	// Valid ?
	bValid = (hr == S_OK);

	// For any errors, reference count stays zero
	if (!bValid)
		{
		lcnt = 0;
		lprintf ( LOG_WARN, L"libSSL not in a valid state, 0x%x\r\n", hr );
		}	// if

	return lcnt;
	}	// AddRef

HRESULT libSSL :: errors ( const wchar_t *strCtx )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Internal utility to check for error conditions.
	//
	//	PARAMETERS
	//		-	strCtx is the context of the call
	//
	//	RETURN VALUE
	//		S_OK if no error
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr = S_OK;
	unsigned long	e;
	char				bfr[1024];

	// Continue until no more errors
	while ((e = ERR_get_error()) != 0)
		{
		ERR_error_string_n ( e, bfr, sizeof(bfr) );
		dbgprintf ( L"%s:Error:%S:%d(0x%x)\r\n", strCtx, bfr, e, e );
		if (hr == S_OK)
			hr = e;
		}	// while

	return hr;
	}	// errors

LONG libSSL :: Release ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Adds a reference count to the library.  Attempts load
	//			on first count.
	//
	//	RETURN VALUE
	//		Current reference count
	//
	////////////////////////////////////////////////////////////////////////

	// Reference count
	if (InterlockedDecrement(&lcnt) != 0)
		return lcnt;

	// Clean up resources
//	CONF_modules_unload(1);
	ERR_free_strings();
	EVP_cleanup();
	sk_SSL_COMP_free(SSL_COMP_get_compression_methods());
	CRYPTO_cleanup_all_ex_data();

	return lcnt;
	}	// Release

//
// objSSL
//

// Constructors for the object

objSSL :: objSSL ( RSA *_rsa )
	{
	rsa		= _rsa;
	ctx		= NULL;
	ssl		= NULL;
	AddRef();
	}	// objSSL

objSSL :: objSSL ( SSL_CTX *_ctx )
	{
	rsa		= NULL;
	ctx		= _ctx;
	ssl		= NULL;
	AddRef();
	}	// objSSL

objSSL :: objSSL ( SSL *_ssl )
	{
	rsa		= NULL;
	ctx		= NULL;
	ssl		= _ssl;
	AddRef();
	}	// objSSL

void objSSL :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	if (rsa != NULL)
		RSA_free(rsa);
	if (ctx != NULL)
		SSL_CTX_free(ctx);
	if (ssl != NULL)
		SSL_free(ssl);
	}	// destruct

//
// Utilities
//

HRESULT SslSkt_Receive ( SSL *ssl, void *pvBfr, U32 nio, U32 *pnio, U32 toms )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the socket.
	//
	//	PARAMETERS
	//		-	ssl is the ssl socket
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//		-	toms is the timeout in milliseconds for the read
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	SOCKET			skt	= INVALID_SOCKET;
	struct timeval	to		= { 0, 0 };
	int				len	= 0;
	int				ret	= 0;
	fd_set			rfds,efds;

	// Setup
	CCLTRYE	( (ssl != NULL), E_INVALIDARG );
	CCLTRYE  ( (skt = SSL_get_fd(ssl)) != INVALID_SOCKET, E_UNEXPECTED );

	// Debug
	#ifdef	TIMING
	adtDate dThen,dNow;
	dThen.now();
	#endif

	// Initialize descriptors
	CCLOK ( FD_ZERO ( &rfds ); )
	CCLOK ( FD_SET ( skt, &rfds ); )
	CCLOK ( FD_ZERO ( &efds ); )
	CCLOK ( FD_SET ( skt, &efds ); )

	// Timeout
	CCLOK ( to.tv_sec		= (toms/1000); )
	CCLOK ( to.tv_usec	= (toms % 1000) * 1000; )

	// Wait for readability.  This makes sure the recv does not hang forever.
//	lprintf ( LOG_DBG, L"select 0x%x\r\n", skt );
	CCLTRYE	( (ret = select ( (int)skt+1, &rfds, NULL, &efds, &to )) != SOCKET_ERROR, WSAGetLastError() );
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( FD_ISSET ( skt, &rfds ), ERROR_TIMEOUT );

	// Debug
	#ifdef	TIMING
	dNow.now ();
	double dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow select %g s\r\n", dt );
	#endif

	// Read data
//	lprintf ( LOG_DBG, L"hr 0x%x ret %d nio %d\r\n", hr, ret, nio );
	CCLTRYE  ( (len = SSL_read ( ssl, pvBfr, (U32)nio )) >= 0, SSL_get_error(ssl,len) );
//	lprintf ( LOG_DBG, L"hr 0x%x len %d\r\n", hr, len );

	// A readable socket that returns 0 bytes means socket has been closed gracefully
	if (hr == S_OK && len == 0)
		hr = WSAECONNRESET;

	// Debug
//	dbgprintf ( L"CommSktLoad::read:pvBfr %p nio %d len %d\r\n", pvBfr, nio, len );

	// Result
	if (hr == S_OK && pnio != NULL)
		*pnio = len;

	// Debug
//	dbgprintf ( L"Net_Receive:skt 0x%x hr 0x%x len %d nio %d\n", skt, hr, len, nio );
//	#if	defined(_DEBUG) || defined(DEBUG)
	if (hr != S_OK)
		{
//		lprintf ( LOG_DBG, L"Net_Receive:skt 0x%x hr 0x%x len %d nio %d\n", skt, hr, len, nio );
		}	// if
//	#endif

	// Debug
//	CommSkt_Log ( skt, L"Net_Receive", pvBfr, nio, len, hr );

	return hr;
	}	// SslSkt_Receive

HRESULT SslSkt_Send ( SSL *ssl, void const *pcvBfr, U32 nio, 
								U32 *pnio, U32 toms )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the SSL socket.
	//
	//	PARAMETERS
	//		-	ssl is the ssl socket
	//		-	pcvBfr contains the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//		-	toms is the timeout in milliseconds for the write
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	SOCKET			skt	= INVALID_SOCKET;
	struct timeval	to		= { 0, 0 };
	int				len	= 0;
	int				ret	= 0;
	fd_set			wfds,efds;

	// Setup
	CCLTRYE	( (ssl != NULL), E_INVALIDARG );
	CCLTRYE  ( (skt = SSL_get_fd(ssl)) != INVALID_SOCKET, E_UNEXPECTED );
	if (pnio != NULL) *pnio = 0;

	// Debug
	#ifdef	TIMING
	adtDate dThen,dNow;
	dThen.now();
	#endif

	// Timeout
	CCLOK ( to.tv_sec		= (toms/1000); )
	CCLOK ( to.tv_usec	= (toms % 1000) * 1000; )

	// Initialize descriptors
	CCLOK ( FD_ZERO ( &wfds ); )
	CCLOK ( FD_SET ( SSL_get_fd(ssl), &wfds ); )
	CCLOK ( FD_ZERO ( &efds ); )
	CCLOK ( FD_SET ( skt, &efds ); )

	// Wait for writeability.  This is how the timeout is used.
	if (hr == S_OK)
		{
		CCLTRYE	( (ret = select ( (int)skt+1, NULL, &wfds, &efds, &to )) != SOCKET_ERROR, WSAGetLastError() );

		// If error on select, socket is bad
		if (hr != S_OK) skt = INVALID_SOCKET;
		}	// if
	CCLTRYE	( (ret != 0), ERROR_TIMEOUT );
	CCLTRYE	( FD_ISSET ( skt, &wfds ), ERROR_TIMEOUT );

	// Debug
	#ifdef	TIMING
	dNow.now ();
	double dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow select %g s\r\n", dt );
	#endif

	// Write data
	if (hr == S_OK)
		{
		// Attempt write
		CCLTRYE	( (len = SSL_write ( ssl, (const char *) pcvBfr, (int)nio )) >= 0, SSL_get_error(ssl,len) );
		}	// if

	// Result
	if (pnio != NULL && hr == S_OK)	*pnio = len;

	// Debug
	#ifdef	TIMING
	dNow.now ();
	dt = (dNow-dThen)*SECINDAY;
	if (dt > 0.200)
		lprintf ( LOG_DBG, L"Slow read %g s\r\n", dt );
	#endif

	// If the write gets a timeout, do best to detect a reset connection to avoid continual retry
	if (hr == ERROR_TIMEOUT)
		{
		fd_set	rfds;

		// Initialize descriptors
		FD_ZERO ( &rfds );
		FD_SET ( skt, &rfds );
		memset ( &to, 0, sizeof(to) );

		// See if data is available on socket, a 'reset' connection will have data waiting
		ret = select ( (int)skt+1, &rfds, NULL, NULL, &to );

		// Socket marked ?
		if (ret > 0 && FD_ISSET ( skt, &rfds ))
			{
			char	byte;

			// Peek at incoming data just to see if 'recv' returns an error
//			ret = recv ( skt, &byte, 1, MSG_PEEK );
			ret = SSL_peek ( ssl, &byte, 1 );

			// If connection is bad, read returns immediately with 0 bytes read
			if (ret == 0)
				{
				// Close socket ourselves, this will cause future access to error out
				shutdown ( skt, 2 );
				closesocket ( skt );
				hr = E_UNEXPECTED;
				}	// if

			}	// if
		}	// if

	// Debug
//	dbgprintf ( L"NetSkt_Send:hr 0x%x skt 0x%x len %d nio %d\n", hr, skt, len, nio );
	#if	defined(_DEBUG) || defined(DEBUG)
	if (hr != S_OK && skt != INVALID_SOCKET)
		{
		lprintf ( LOG_DBG, L"NetSkt_Send:hr 0x%x skt 0x%x len %d nio %d\n", hr, skt, len, nio );
		}	// if
	#endif

	// Debug
//	Net_Log ( skt, L"NetSkt_Send", pcvBfr, nio, len, hr );
//	dNow.now();
//	lprintf ( LOG_DBG, L"hr 0x%x pnio %d Time %g\r\n", 
//					hr, (pnio != NULL) ? *pnio : 0, (dNow-dThen)*SECINDAY );

	return hr;
	}	// SslSkt_Send
