////////////////////////////////////////////////////////////////////////
//
//									Environment.CPP
//
//					Implementation of the environment node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <stdio.h>
#ifdef	_WIN32
#include <Psapi.h>
#endif

Environment :: Environment ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	hQuery	= NULL;
	hCnt		= NULL;
	}	// Environment

HRESULT Environment :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		hCnt = NULL;
		if (hQuery != NULL)
			{
			PdhCloseQuery(hQuery);
			hQuery = NULL;
			}	// if
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Environment :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Memory
	if (_RCP(Memory))
		{
		// State check
		CCLTRYE ( pDct		!= NULL,	ERROR_INVALID_STATE );
		#ifdef			_WIN32
		MEMORYSTATUSEX	ms; 

		// Memory statistics
		if (hr == S_OK)
			{
			// Status
			memset ( &ms, 0, sizeof(ms) );
			ms.dwLength = sizeof(ms);
			CCLTRYE ( GlobalMemoryStatusEx ( &ms ) == TRUE, GetLastError() );
			CCLTRY ( pDct->store ( adtString(L"MemoryLoad"),			adtLong(ms.dwMemoryLoad) ) );
			CCLTRY ( pDct->store ( adtString(L"TotalPhysical"),		adtLong(ms.ullTotalPhys) ) );
			CCLTRY ( pDct->store ( adtString(L"AvailablePhysical"),	adtLong(ms.ullAvailPhys) ) );
			CCLTRY ( pDct->store ( adtString(L"TotalVirtual"),			adtLong(ms.ullTotalVirtual) ) );
			CCLTRY ( pDct->store ( adtString(L"AvailableVirtual"),	adtLong(ms.ullAvailVirtual) ) );
			}	// if

		// Process specific
		#if	!defined(UNDER_CE)
		HANDLE						hProc = NULL;
		PROCESS_MEMORY_COUNTERS	pms;

		// Access process
		if (	(hr == S_OK)	&&
				((hProc = OpenProcess (	PROCESS_QUERY_INFORMATION|PROCESS_VM_READ,
												FALSE, GetCurrentProcessId() )) != NULL) &&
				(GetProcessMemoryInfo ( hProc, &pms, sizeof(pms) ) == TRUE) )
			hr = pDct->store ( adtString(L"PageFileUsage"),	adtLong(pms.PagefileUsage) );
		#endif
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Memory,adtIUnknown(pDct));

		}	// if

	// Display
	else if (_RCP(Display))
		{
		HDC hDC;

		// State check
		CCLTRYE ( pDct		!= NULL,	ERROR_INVALID_STATE );

		// (Active) Display information
		#ifdef			_WIN32
		#if	!defined(__NOGDI__)
		if (hr == S_OK && ((hDC = GetDC(NULL)) != NULL))
			{
			// Store parameters
			CCLTRY(pDct->store ( adtString(L"Width"),		adtInt(GetDeviceCaps(hDC,HORZRES)) ));
			CCLTRY(pDct->store ( adtString(L"Height"),	adtInt(GetDeviceCaps(hDC,VERTRES)) ));
			CCLTRY(pDct->store ( adtString(L"Bpp"),		adtInt(GetDeviceCaps(hDC,BITSPIXEL)) ));

			// Clean up
			ReleaseDC(NULL,hDC);			
			}	// if
		#endif
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Display,adtIUnknown(pDct));
		}	// else if

	// CPU
	else if (_RCP(CPU))
		{
		// State check
		CCLTRYE ( pDct		!= NULL,	ERROR_INVALID_STATE );

		// (Active) Display information
		#ifdef			_WIN32

		// First time for query ?
		if (hr == S_OK && hQuery == NULL)
			{
			// Create query
			CCLTRYE ( PdhOpenQuery ( NULL, NULL, &hQuery ) == ERROR_SUCCESS, E_UNEXPECTED );

			// Required counters
			CCLTRYE ( PdhAddEnglishCounter ( hQuery, L"\\Processor(_Total)\\% Processor Time",
															NULL, &hCnt ) == ERROR_SUCCESS, E_UNEXPECTED );
			}	// if

		// Processor timer
		CCLTRYE ( hQuery != NULL, ERROR_INVALID_STATE );
		if (hr == S_OK)
			{
			PDH_FMT_COUNTERVALUE	val;

			// Retrieve value and update
			CCLTRYE ( PdhCollectQueryData(hQuery) == ERROR_SUCCESS, E_UNEXPECTED );
			CCLTRYE ( PdhGetFormattedCounterValue (hCnt,PDH_FMT_DOUBLE,NULL,&val) ==
							ERROR_SUCCESS, E_UNEXPECTED );

			// Store parameters
			CCLTRY(pDct->store ( adtString(L"CpuLoad"),	adtDouble(val.doubleValue) ));
			}	// if
		#endif

		// Result
		if (hr == S_OK)
			_EMT(CPU,adtIUnknown(pDct));
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

