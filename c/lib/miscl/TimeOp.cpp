////////////////////////////////////////////////////////////////////////
//
//									TIME.CPP
//
//					Implementation of the time node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <math.h>
#include <stdio.h>

TimeOp :: TimeOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	bLocal	= false;
	iDa		= 0;
	iMo		= 0;
	iYr		= 0;
	iHr		= 0;
	iMn		= 0;
	iSc		= 0;
	iMs		= 0;
	}	// Time

HRESULT TimeOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		v;

		// Defaults
		if (pnDesc->load ( adtString(L"Local"), v ) == S_OK)
			bLocal = adtBool(v);
		}	// if

	return hr;
	}	// onAttach

HRESULT TimeOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IBehaviour
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Current time
	if (_RCP(Now))
		{
		adtDate		dNow;

		// Set date to now
		CCLOK ( dNow.now(bLocal); )

		// Result
		CCLOK ( _EMT(Now,dNow); )
		}	// if

	// Break time
	else if (_RCP(Break))
		{
		adtDate		date(v);
		U32			calendar;
		#ifdef		_WIN32
		SYSTEMTIME	st;

		// Convert the current date to its pieces
		CCLTRY ( adtDate::toSystemTime ( date, &st ) );

		// Emit result
		CCLOK ( _EMT(Year,adtInt(st.wYear) ); )
		CCLOK ( _EMT(Month,adtInt(st.wMonth) ); )
		CCLOK ( _EMT(Day,adtInt(st.wDay) ); )
		CCLOK ( _EMT(Hour,adtInt(st.wHour) ); )
		CCLOK ( _EMT(Minute,adtInt(st.wMinute) ); )
		CCLOK ( _EMT(Second,adtInt(st.wSecond) ); )
		CCLOK ( _EMT(Millisecond,adtInt(st.wMilliseconds) ); )

		#else
		time_t		sec;
		S32			usec;
		time_t		sect;
		struct tm	*ptm = NULL;

		// Obtain time_t value (Epoch seconds)
		CCLTRY ( adtDate::toEpochSeconds ( date, &sec, &usec ) );

		// Break time into its components
		CCLOK ( sect = sec; )
		CCLTRYE ( ((ptm = gmtime ( (time_t *)&sect )) != NULL),
						E_UNEXPECTED );

		// Emit result
		CCLOK ( _EMT(Year,adtInt(ptm->tm_year+1900 ) ); )
		CCLOK ( _EMT(Month,adtInt(ptm->tm_mon+1) ); )
		CCLOK ( _EMT(Day,adtInt(ptm->tm_mday ) ); )
		CCLOK ( _EMT(Hour,adtInt(ptm->tm_hour ) ); )
		CCLOK ( _EMT(Minute,adtInt(ptm->tm_min ) ); )
		CCLOK ( _EMT(Second,adtInt(ptm->tm_sec ) ); )
		CCLOK ( _EMT(Millisecond,adtInt((S32)(usec/1000))); )
		#endif

		// Emit date (calendar) portion and time portion of full date
		CCLOK ( calendar = (U32) floor(date); )
		CCLOK ( _EMT(Date, adtDate ( calendar ) ); )
		CCLOK ( _EMT(Time, adtDate ( (date.vdate - calendar) ) ); )

		// Done
		CCLOK ( _EMT(Break,adtInt() ); )
		}	// else if

	// Make
	else if (_RCP(Make))
		{
		WCHAR		wBfr[101];
		adtDate	d;

		// Generate a string that can be converted to a local date
		swprintf ( SWPF(wBfr,100),
						L"%d/%d/%d %d:%02d:%02d.%03d",
						iMo, iDa, iYr, iHr, iMn, iSc, iMs );

		// Convert into a date
		CCLTRY ( adtDate::fromString ( wBfr, d ) );

		// Result
		_EMT(Make,d);
		}	// else if

	// Clear state
	else if (_RCP(Clear))
		{
		iDa = 0;
		iMo = 0;
		iYr = 0;
		iHr = 0;
		iMn = 0;
		iSc = 0;
		iMs = 0;
		}	// else if

	// State
	else if (_RCP(Day))
		iDa = adtInt(v);
	else if (_RCP(Month))
		iMo = adtInt(v);
	else if (_RCP(Year))
		iYr = adtInt(v);
	else if (_RCP(Hour))
		iHr = adtInt(v);
	else if (_RCP(Minute))
		iMn = adtInt(v);
	else if (_RCP(Second))
		iSc = adtInt(v);
	else if (_RCP(Millisecond))
		iMs = adtInt(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

