////////////////////////////////////////////////////////////////////////
//
//									DIST.CPP
//
//					Implementation of the distribution node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <stdio.h>
#ifdef	_WIN32
#include <psapi.h>
#endif

// Globals
static WCHAR	wDbgBfr[16384];
static WCHAR	wDbgBfr2[sizeof(wDbgBfr)/sizeof(WCHAR)];
static sysCS	csDebug;
#define	DBGSZ	sizeof(wDbgBfr)/sizeof(WCHAR)

Debug :: Debug ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pDctLog		= NULL;
	bPath			= true;
	pDctCnt		= NULL;
	iKeyCntMin	= 0;

	// Windows specific performance frequency
	#ifdef	_WIN32
	LARGE_INTEGER	li;
	QueryPerformanceFrequency ( &li );
	lFreq = li.QuadPart;
	#endif
	}	// Debug

void Debug :: appendDbg ( const WCHAR *pwStr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Append a string to the debug output buffer.
	//
	//	PARAMETERS
	//		-	pwStr is the string to append
	//
	////////////////////////////////////////////////////////////////////////

	// Only append if there is room
	if (wcslen(wDbgBfr)+wcslen(pwStr)+1 < DBGSZ)
		WCSCAT ( wDbgBfr, sizeof(wDbgBfr)/sizeof(wDbgBfr[0]), pwStr );
	else
		dbgprintf ( L"WARNING:Debug buffer too small\r\n" );
	}	//	appendDbg

HRESULT Debug :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		v;

		// Default message ?
		if (pnDesc->load ( adtString(L"Message"), v ) == S_OK)
			strMsg = v;
		if (pnDesc->load ( adtString(L"Dump"), v ) == S_OK)
			bDump = v;
		if (pnDesc->load ( adtString(L"Path"), v ) == S_OK)
			bPath = v;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDctLog);
		}	// else

	return hr;
	}	// onAttach

void Debug :: logCallback	( cLogEntry *e, void *p )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Call back function for logging.
	//
	//	PARAMETERS
	//		-	e is the log entry
	//		-	p is the callback parameters
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	Debug			*pThis	= (Debug *)p;
	adtDate		date;

	// Logging dictionary needed ?
	if (hr == S_OK && pThis->pDctLog == NULL)
		hr = COCREATE(L"Adt.Dictionary",IID_IDictionary,&(pThis->pDctLog));

	// Transfer information to dictionary
	CCLTRY ( pThis->pDctLog->clear() );
//	CCLTRY ( adtDate::fromSystemTime ( &(e->date), &(date.vdate) ) );
//	CCLTRY ( pThis->pDctLog->store ( adtString(L"Date"), date ) );
//	CCLTRY ( pThis->pDctLog->store ( adtString(L"Function"), adtString((WCHAR *)(e->func+1)) ) );
	CCLTRY ( pThis->pDctLog->store ( adtString(L"File"), adtString((WCHAR *)(e->file+1)) ) );
	CCLTRY ( pThis->pDctLog->store ( adtString(L"Line"), adtInt(e->line) ) );
	CCLTRY ( pThis->pDctLog->store ( adtString(L"Level"), adtInt(e->level) ) );
	CCLTRY ( pThis->pDctLog->store ( adtString(L"Value"), adtString((WCHAR *)(e->str+1)) ) );

	// Emit entry
//	CCLOK ( pThis->peOnLog->emit(adtIUnknown(pThis->pDctLog) ); )
	}	// logCallback

HRESULT Debug :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IBehaviour
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Fire
	if (_RCP(Fire))
		{
		adtString	sValue;
		adtValue		vL,vDbg;

		// Thread safety
		if (!csDebug.enter())
			return E_UNEXPECTED;

		// Setup
		WCSCPY ( wDbgBfr, sizeof(wDbgBfr)/sizeof(wDbgBfr[0]), L"" );

		// Show path ?
		if (bPath)
			{
			// For easier identification of source of debug, access the definition
			// of the container graph.
			if (strPath.length() == 0)
				{
				IDictionary	*pPar	= NULL;
				IDictionary	*pDsc	= NULL;
				adtIUnknown	unkV;

				// Path to this location
				nspcPathTo ( pnLoc, L"./", strPath );

	//			if (!WCASECMP(strPath,L"/apps/auto/default/TreeDirect/Render/list/static/tree/.visual/Debug/"))
	//				dbgprintf ( L"Hi\r\n" );

				// Reference location for hosting graph
				if (	pnLoc->load ( strnRefPar, vL ) == S_OK		&&
						(IUnknown *)(NULL) != (unkV=vL)				&&
						_QI(unkV,IID_IDictionary,&pPar) == S_OK	&&
	//					pPar->load ( strnRefDesc, vL ) == S_OK		&&
	//					(IUnknown *)(NULL) != (unkV=vL)				&&
	//					_QI(unkV,IID_IDictionary,&pDsc) == S_OK	&&
	//					pDsc->load ( strnRefLocn, vL ) == S_OK		&&
						pPar->load ( strnRefLocn, vL ) == S_OK		&&
						adtValue::toString ( vL, sValue ) == S_OK)
					{
					strPath.append ( L" (" );
					strPath.append ( sValue );
					strPath.append ( L")" );
					}	// if

				// Clean up
				_RELEASE(pDsc);
				_RELEASE(pPar);
				}	// if

			// Path to node
			appendDbg ( strPath );
			appendDbg ( L": " );
			}	// if

		// Message
		if (strMsg.length())
			{
			appendDbg ( strMsg );
			appendDbg ( L": " );
			}	// if

		// Value for debug
		hr = adtValue::copy ( v, vDbg );

		// Debug
//		if (!WCASECMP(strPath,L"/apps/auto/default/TestInst/Render/list/static/tree/Debug/ (render/gdi/dict/)"))
//			dbgprintf ( L"Hi\r\n" );

		// Value
		switch (adtValue::type(vDbg))
			{
			// Object ?
			case VTYPE_UNK :
				{
				IUnknown			*punk		= NULL;
				IDictionary		*pLoc		= NULL;
				IReceptor		*pRecep	= NULL;
				IByteStream		*pStm		= NULL;
				IMemoryMapped	*pMem		= NULL;
				IDictionary		*pD		= NULL;
				IList				*pL		= NULL;

				// Unknown ptr.
				punk =	vDbg.punk;

				// Object type
				if (punk == NULL)
					appendDbg ( L"null," );
				else if (_QI(punk,IID_IDictionary,&pD) == S_OK)
					{
					IIt		*pKeys	= NULL;
					adtValue	vKey,vValue;
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Dictionary(%p):", punk );
					appendDbg ( wDbgBfr2 );
					CCLTRY ( pD->keys ( &pKeys ) );
					while (pKeys->read ( vKey ) == S_OK)
						{
						// Key
						if (	adtValue::toString ( vKey, sValue ) == S_OK)
							{
							swprintf ( SWPF(wDbgBfr2,DBGSZ), L"%ls(%d)=", (LPCWSTR) sValue, vKey.vtype );
							appendDbg ( wDbgBfr2 );
							}	// if
						if (	pD->load ( vKey, vValue ) == S_OK &&
								adtValue::toString ( vValue, sValue ) == S_OK)
							{
							swprintf ( SWPF(wDbgBfr2,DBGSZ), L"%ls(%d),", (LPCWSTR) sValue, vValue.vtype );
							appendDbg ( wDbgBfr2 );
							}	// if

						// Next key
						pKeys->next();
						}	// while

					// Clean up
					_RELEASE(pKeys);
					}	// else if 
				else if (_QI(punk,IID_IList,&pL) == S_OK)
					{
					IIt		*pIt		= NULL;
					adtValue	vValue;

					// Print out values
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Container(%p):", punk );
					appendDbg ( wDbgBfr2 );
					CCLTRY ( pL->iterate ( &pIt ); )
					while (hr == S_OK && pIt->read ( vValue ) == S_OK)
						{
						if ( adtValue::toString ( vValue, sValue ) == S_OK)
							{
							swprintf ( SWPF(wDbgBfr2,DBGSZ), L"%ls,", (LPCWSTR) sValue );
							appendDbg ( wDbgBfr2 );
							}	// if

						// Next key
						pIt->next();
						}	// while

					// Clean up
					_RELEASE(pIt);
					}	// else if
/*				else if (_QI(punk,IID_ILocation,&pLoc) == S_OK)
					{
					IDictionary	*pDesc	= NULL;
					IDictionary	*pEmit	= NULL;
					adtValue		v;
					adtIUnknown	unkV;
					adtString	strName,strBehave;

					// Optional descriptor
					CCLTRY ( pLoc->load ( adtString(STR_NSPC_DESC), v ) );
					CCLTRY ( _QISAFE((unkV=v),IID_IDictionary,&pDesc) );
					CCLTRY ( pDesc->load ( adtString(STR_NSPC_NAME), v ) );
					CCLOK  ( strName = v; )
					CCLTRY ( pDesc->load ( adtString(STR_NSPC_BEHAVE), v ) );
					CCLOK  ( strBehave = v; )

					// Debug
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Location:%p", pLoc );
					appendDbg ( wDbgBfr2 );
					if (hr == S_OK)
						{
						swprintf ( SWPF(wDbgBfr2,DBGSZ), L":%ls:%ls", (LPCWSTR)strName, (LPCWSTR)strBehave );
						appendDbg ( wDbgBfr2 );
						}	// if

					// Clean up
					_RELEASE(pDesc);
					_RELEASE(pEmit);
					_RELEASE(pLoc);
					}	// else if
*/
				else if (_QI(punk,IID_IReceptor,&pRecep) == S_OK)
					{
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Receptor:%p", punk );
					appendDbg ( wDbgBfr2 );
					}	// else if
				else if (_QI(punk,IID_IByteStream,&pStm) == S_OK)
					{
					U64 pos = 0,len = 0;

					// Current position and length
					CCLTRY ( pStm->seek ( 0, STREAM_SEEK_CUR, &pos ) );
					CCLTRY ( pStm->available ( &len ) );
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Stream:Position %d/Length %d", (S32)pos, (S32)(pos+len) );
					appendDbg ( wDbgBfr2 );

					// Dump bytes ? Use with caution
					if (bDump && len > 0)
						{
						BYTE b;
						for (int i=0;i < len;++i)
							{
							if (pStm->read ( &b, 1, NULL ) == S_OK)
								{
								swprintf ( SWPF(wDbgBfr2,DBGSZ), L" 0x%x", b );
								appendDbg ( wDbgBfr2 );
								}	// if		
							}	// for

						// Restore original position
						pStm->seek ( pos, STREAM_SEEK_SET, NULL );
						}	// if
					}	// else if
				else if (_QI(punk,IID_IMemoryMapped,&pMem) == S_OK)
					{
					U32 	sz		= 0;
					void	*ptr	= NULL;

					// Current size
					CCLTRY( pMem->getInfo(&ptr,&sz) );
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Memory:Ptr %p:Size %d", ptr, sz );
					appendDbg ( wDbgBfr2 );
					}	// else if
				else
					{
					swprintf ( SWPF(wDbgBfr2,DBGSZ), L"Object (%p)", punk );
					appendDbg ( wDbgBfr2 );
					}	// else

				// Clean up
				_RELEASE(pL);
				_RELEASE(pD);
				_RELEASE(pMem);
				_RELEASE(pStm);
				_RELEASE(pRecep);
				_RELEASE(pLoc);
				}	// VT_UNKNOWN
				break;

			// Other
			default :
				adtValue::toString ( vDbg, sValue );
				swprintf ( SWPF(wDbgBfr2,DBGSZ), L"%ls (%d)", (LPCWSTR)sValue, vDbg.vtype );
				appendDbg ( wDbgBfr2 );
			}	// switch

		// Done
		if (bPath)
			lprintf ( LOG_DBG, L"%s\r\n", wDbgBfr );
		else
			dbgprintf ( L"%s\r\n", wDbgBfr );
		csDebug.leave();
		}	// if

	// Key count
	else if (_RCP(KeyCount))
		{
		IDictionary		*pDctRt = NULL;
		adtValue			vL;
		adtIUnknown		unkV;

		// Expensive but for debug so infrequent

		// Create a dictionary to receive the paths vs. counts
		CCLTRY ( COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctCnt) );

		// Visit all locations starting with root
		CCLTRY ( pnSpc->get ( L"/", vL, NULL ) );
		CCLTRY ( _QISAFE((unkV = vL),IID_IDictionary,&pDctRt) );
		CCLOK ( visit ( pDctRt, L"/" ); )

		// Result
		CCLOK ( _EMT(KeyCount,adtIUnknown(pDctCnt)); )

		// Clean up
		_RELEASE(pDctRt);
		_RELEASE(pDctCnt);

		}	// else if
	else if (_RCP(KeyCountMin))
		iKeyCntMin = adtInt(v);

/*
	// Logging on/off
	else if (_RCP(Log))
		{
		adtBool	bLog(v);

		// Enable/disable logging sink
		// TODO: Multiple callbacks
		logSink ( (bLog == true) ? logCallback : NULL, this );
		}	// else if
*/
	// Process status
	else if (_RCP(Status))
		{
		#if	_WIN32
		MEMORYSTATUSEX				ms; 
		PROCESS_MEMORY_COUNTERS mc;

		// Local
		memset ( &mc, 0, sizeof(mc) );
		mc.cb = sizeof(mc);
		GetProcessMemoryInfo(GetCurrentProcess(),&mc,sizeof(mc));
		lprintf ( LOG_DBG, L"WorkingSetSize    : %lld Mib\r\n", mc.WorkingSetSize/(1<<20) );
		lprintf ( LOG_DBG, L"PageFileUsage     : %lld Mib\r\n", mc.PagefileUsage/(1<<20) );

		// Global
		memset ( &ms, 0, sizeof(ms) );
		ms.dwLength = sizeof(ms);
		GlobalMemoryStatusEx ( &ms );
		lprintf ( LOG_DBG, L"MemoryLoad        : %d %%\r\n", ms.dwMemoryLoad );
		lprintf ( LOG_DBG, L"TotalPhysical     : %lld Mib\r\n", ms.ullTotalPhys/(1<<20) );
		lprintf ( LOG_DBG, L"AvailablePhysical : %lld Mib\r\n", ms.ullAvailPhys/(1<<20) );
		lprintf ( LOG_DBG, L"TotalVirtual      : %lld Mib\r\n", ms.ullTotalVirtual/(1<<20) );
		lprintf ( LOG_DBG, L"AvailableVirtual  : %lld Mib\r\n", ms.ullAvailVirtual/(1<<20) );

		#elif	__unix__
		// For now just dump the process status
		FILE *file = fopen ( "/proc/self/status", "r" );
		char	line[128];
		if (file != NULL)
			{
			while (fgets(line, 128, file) != NULL)
				lprintf ( LOG_DBG, L"%S", line );
			fclose(file);
			}	// if
		lprintf ( LOG_DBG, L"---------\r\n" );
		file = fopen ( "/proc/meminfo", "r" );
		if (file != NULL)
			{
			while (fgets(line, 128, file) != NULL)
				lprintf ( LOG_DBG, L"%S", line );
			fclose(file);
			}	// if
		#endif
		}	// else if

	// Debug break
	else if (_RCP(Break))
		{
		dbgprintf ( L"MiscDebug::receive:Break @ %s\r\n", (LPCWSTR)strnName );
		#ifdef	_DEBUG
		DebugBreak();
		#endif
		dbgprintf ( L"MiscDebug::receive:Break @ %s\r\n", (LPCWSTR)strnName );
		}	// else if

	// Timing
	else if (_RCP(Reset))
		{
		#ifdef	_WIN32
		LARGE_INTEGER lCnt;
		QueryPerformanceCounter ( &lCnt );
		lRst = lCnt.QuadPart;
		#elif	__APPLE__ || __unix__
		dRst.now();
		#endif
//		dbgprintf ( L"MiscDebug::%s:%d\r\n", (LPCWSTR)strMsg, dwT0 );
		}	// else if
	else if (_RCP(Mark))
		{
		#ifdef	_WIN32
		LARGE_INTEGER lCnt;
		QueryPerformanceCounter ( &lCnt );

		// Compute difference
		double
		dt = ((lCnt.QuadPart-lRst) * 1.0) / lFreq;
		lprintf ( LOG_DBG, L"%s:%s:%g s\r\n", (LPCWSTR)strMsg, (LPCWSTR) strnName, dt );//, dwT1, dwT0 );
		#elif	__APPLE__ || __unix__
		adtDate dNow;
		dNow.now();
		double
		dt = ((dNow-dRst)*SECINDAY);
		lprintf ( LOG_DBG, L"%s:%s:%g\r\n", (LPCWSTR)strMsg, (LPCWSTR)strnName, dt);
		#endif
		}	// else if
	else if (_RCP(Sleep))
		{
		adtInt	iMs(v);
		#ifdef	_WIN32
		Sleep(iMs);
		#else
//		lprintf ( LOG_DBG, L"Sleep %d us {\r\n", iMs*1000 );
		usleep(iMs*1000);
//		lprintf ( LOG_DBG, L"} Sleep %d us\r\n", iMs*1000 );
		#endif
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Debug :: visit ( IDictionary *pDct, const WCHAR *wPath )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Perform a visitation of all paths in the dictionary.
	//
	//	PARAMETERS
	//		-	pDct is the dictionary to visit
	//		-	wPath is the current path of the provided dictionary
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IIt			*pIt		= NULL;
	adtValue		vK;

	// State check
	CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

	// Perform current operation

	// Retrieving count ?
	if (hr == S_OK && pDctCnt != NULL)
		{
		ILocDebug *pDbg = NULL;

		// Does the current location support location debug ?
		if (_QI(pDct,IID_ILocDebug,&pDbg) == S_OK)
			{
			U32	cnt;

			// Retrieve the count
			CCLTRY ( pDbg->getKeyCount(&cnt) );
//			lprintf ( LOG_DBG, L"Count %d\r\n", cnt );

			// Store count under path
			if (hr == S_OK && cnt >= iKeyCntMin)
				hr = pDctCnt->store ( adtString(wPath), adtInt(cnt) );

			// Clean up
			_RELEASE(pDbg);
			}	// if

		}	// if

	// Visit sub-locations in graph
	CCLTRY ( pDct->keys ( &pIt ) );
	while (hr == S_OK && pIt->read ( vK ) == S_OK)
		{
		IDictionary	*pDctV	= NULL;
		adtString	strKey,strPath(wPath);
		adtValue		vV;
		adtIUnknown	unkV;

		// Update current path
		CCLTRY ( adtValue::toString ( vK, strKey ) );
		CCLTRY ( strPath.append ( strKey ) );
		CCLTRY ( strPath.append ( L"/" ) );
//		lprintf ( LOG_DBG, L"Path %s\r\n", (LPCWSTR)strPath );

		// Retrieve entry
		CCLTRY ( pDct->load ( vK, vV ) );

		// If value at current key is a dictionary, time to visit that dictionary
		if (hr == S_OK									&&
				WCASECMP(strKey,STR_NSPC_PARENT)	&&
				(IUnknown *)(NULL) != (unkV=vV)	&&
				_QI(unkV,IID_IDictionary,&pDctV) == S_OK)
			visit ( pDctV, strPath );

		// Clean up
		_RELEASE(pDctV);
		pIt->next();
		}	// while

	// Clean up
	_RELEASE(pIt);

	return hr;
	}	// visit

