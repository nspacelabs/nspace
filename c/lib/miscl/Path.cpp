////////////////////////////////////////////////////////////////////////
//
//									PATH.CPP
//
//					Implementation of the path node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"

Path :: Path ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pNames	= NULL;
	pNamesIt	= NULL;
	bAbs		= false;
	}	// Path

void Path :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pNamesIt);
	_RELEASE(pNames);
	}	// destruct

HRESULT Path :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Create list to use for the names in the path
		CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pNames ) );
		CCLTRY ( pNames->iterate ( &pNamesIt ) );

		// Defaults
		if (pnDesc->load(adtString(L"Path"),vL) == S_OK)
			strPath = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pNamesIt);
		_RELEASE(pNames);
		}	// else
	return hr;
	}	// onAttach

HRESULT Path :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IReceptor
	//
	//	PURPOSE
	//		-	A location has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	pl is the location
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Up.  Move 'up' one level in the path.
	if (_RCP(Up))
		{
		adtString	strRes;
		U32			i,cnt;
		adtValue		vN;

		// If path originally had root separator, preserve it.
		if (hr == S_OK && bAbs)
			hr = strRes.append ( L"/" );

		// Re-generate path minus the last name
		CCLTRY ( pNames->size ( &cnt ) );
		CCLTRY ( pNamesIt->begin() );
		for (i = 0;hr == S_OK && i < cnt-1;++i)
			{
			adtString strN;

			// Next entry
			CCLTRY ( pNamesIt->read ( vN ) );
			CCLTRYE( (strN = vN).length() > 0, E_UNEXPECTED );

			// Continue path
			CCLTRY ( strRes.append ( strN ) );
			CCLTRY ( strRes.append ( L"/" ) );

			// Next entry in list
			CCLOK ( pNamesIt->next(); )
			}	// for

		// If original path was not a location, remove trailing slash
		if (hr == S_OK && !bLoc)
			strRes.at(strRes.length()-1) = '\0';

		// Result
		CCLTRY ( adtValue::copy(strRes,strPath) );
		CCLOK ( _EMT(Path,strPath); )
		}	// if

	// Move 'down'
	else if (_RCP(Down))
		{
		adtString	strDown(v);

		// State check
		CCLTRYE ( strDown.length() > 0, ERROR_INVALID_STATE );

		// Need a slash to continue path ?
		if (	hr == S_OK && 
				strPath.length() > 0 && 
				strPath[strPath.length()-1] != '/' &&
				strDown[0] != '/' )
			hr = strPath.append(L"/");

		// Append to current level
		CCLTRY ( strPath.append(strDown) );

		// If location, continue it (if necessary)
		if (hr == S_OK && bLoc && strPath[strPath.length()-1] != '/')
			hr = strPath.append(L"/");

		// Result
		CCLOK ( _EMT(Path,strPath); )

		}	// else if

	// Break apart the path
	else if (_RCP(Break))
		{
		WCHAR			*pDot		= NULL;
		WCHAR			*pSlash	= NULL;
		adtString 	strNull	= L"";
		adtString	sStr;
		WCHAR			wTmp;

		// Valid path ?
		CCLTRYE ( (strPath.length() > 0), E_INVALIDARG );

		// Own string for our purposes
		CCLTRY ( adtValue::copy ( strPath, sStr ) );

		// Ensure 'nSpace' format
		CCLOK ( sStr.replace ( '\\', '/' ); )

		// nSpace Transport / Address / Connection prefix ?
		// Format is <transport>://<Address>/<Path>/
		if (hr == S_OK && (pSlash = wcsstr ( &sStr.at(), L"//" )) != NULL)
			{
			adtString 	strRemain;
			bool 			bColon = false;

			// If there is a colon preceeding the double slash there is
			// also a transport specified
//			lprintf ( LOG_DBG, L"!!!! pSlash %s (%c)\r\n", pSlash, *(pSlash-1) );
			if (pSlash != &sStr.at() && *(pSlash-1) == WCHAR(':'))
				{
				// Transport runs from the start of the string to the colon
				*(pSlash-1) 	= WCHAR('\0');
//				lprintf ( LOG_DBG, L"!!!! Transport (%s)\r\n", (LPCWSTR)sStr );
				_EMT(Transport,adtString(&sStr.at()));
				*(pSlash-1) 	= WCHAR(':');
				bColon			= true;
				}	// if
			else
				_EMT(Transport,strNull);

			// First slash after address
			pDot = wcschr ( pSlash+2, WCHAR('/') );
			if (pDot != NULL) *pDot = WCHAR('\0');
//			lprintf ( LOG_DBG, L"!!!! Address %s\r\n", (pSlash+2) );
			_EMT(Address,adtString(pSlash+2));

			// If a transport was specified, emit full connection string
			// from the beginning to the end of the address
//			lprintf ( LOG_DBG, L"!!!! Connection %s\r\n", (LPCWSTR)sStr );
			if (bColon)
				_EMT(Connection,adtString(&sStr.at()));
			else
				_EMT(Connection,strNull);

			// String after address if full root path starting with slash
			if (pDot != NULL)
				{
				*pDot = WCHAR('/');
				strRemain = pDot;
				}	// if

			// Own remaining string and re-assign
			strRemain.at();
			hr = adtValue::copy(strRemain,sStr);
//			lprintf ( LOG_DBG, L"!!!! Remain %s\r\n", (LPCWSTR)sStr );
			}	// if
		else
			{
			// No prefix
			_EMT(Transport,strNull);
			_EMT(Address,strNull);
			_EMT(Connection,strNull);
			}	// else

		// First emit just the path and filename 
		if (hr == S_OK)
			{
			pSlash = wcsrchr ( &sStr.at(), WCHAR('/') );
			if (pSlash != NULL)
				{
				// Paths always end with '/'
				wTmp			= *(pSlash+1);
				*(pSlash+1) = WCHAR('\0');
				_EMT(Path,adtString ( (PCWSTR)sStr ));
				*(pSlash+1) = wTmp;

				// Filename
				*pSlash = WCHAR('\0');
				++pSlash;
				_EMT(Filename,adtString ( pSlash ));
				}	// if
			else
				{
				pSlash = &sStr.at();
				_EMT(Path,adtString(L""));
				_EMT(Filename,sStr);
				}	// else
			}	// if

		// Find the last period in the provided string
		CCLOK ( pDot = wcsrchr ( pSlash, WCHAR('.') ); )
		if (hr == S_OK && pDot != NULL)
			(*pDot) = WCHAR('\0');

		// Name
		CCLOK ( _EMT(Name,adtString(pSlash) ); )

		// Extension
		if (hr == S_OK)
			{
			if (pDot != NULL)
				_EMT(Extension,adtString(pDot+1) );
			else
				_EMT(Extension,adtString(L"") );
			}	// if

		}	// else if

	// State
	else if (_RCP(Path))
		{
		int	len;

		// Active path
		strPath	= v;
		len		= strPath.length();

		// Break path into its names
		CCLTRY ( pNames->clear() );
		CCLTRY ( nspcTokens ( strPath, L"/\\", pNames ) );

		// Absolute or location path ?
		bAbs = bLoc = false;
		if (hr == S_OK && len > 0)
			{
			CCLOK ( bAbs = (strPath[0] == '/' || strPath[0] == '\\'); )
			CCLOK ( bLoc = (strPath[len-1] == '/' || strPath[len-1] == '\\'); )
			}	// if

		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

