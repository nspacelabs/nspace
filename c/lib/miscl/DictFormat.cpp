////////////////////////////////////////////////////////////////////////
//
//									DICTFMT.CPP
//
//					Implementation of the dictionary format node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <stdio.h>

DictFormat :: DictFormat ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pStm			= NULL;
	pDct			= NULL;
	pFmt			= NULL;
	bEndianBig	= false;

	// Frequently used keys
	strkSize	= L"Size";
	strkName	= L"Name";
	strkVal	= L"Value";
	strkMap	= L"Map";
	strkType	= L"Type";
	strkSub	= L"Subformat";
	strkReq	= L"Req";

	}	// DictFormat

HRESULT DictFormat :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		v;
		adtIUnknown	unkV;

		// Allow format to specified as a node property
		if (pnDesc->load ( adtString(L"Format"),	v ) == S_OK)
			_QISAFE((unkV=v),IID_IContainer,&pFmt);
		if (pnDesc->load ( adtString(L"Endian"),	v ) == S_OK)
			onReceive ( prEndian, v );
		}	// if

	// Detach
	else
		{
		_RELEASE(pFmt);
		_RELEASE(pDct);
		_RELEASE(pStm);
		}	// else

	return hr;
	}	// onAttach

HRESULT DictFormat :: format ( IContainer *pFmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Perform formatting for the current list of fields.
	//
	//	PARAMETERS
	//		-	pFmt is the current format list
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr				= S_OK;
	IDictionary		*pDictSpec	= NULL;
	IDictionary		*pDictSub	= NULL;
	IDictionary		*pMap			= NULL;
	IContainer		*pFmtSub		= NULL;
	IIt				*pFmtIt		= NULL;
	bool				bSz			= false;
	bool				bMiss			= false;
	bool				bReq			= true;
	adtIUnknown		unkV;
	adtInt			oSz;
	adtString		oName,strType;
	adtValue			vSz,oKey,oVal,oValueUn,v;

	// Iterate format specifications
	CCLTRY ( pFmt->iterate ( &pFmtIt ) );

	// Format fields from specification
	CCLTRY ( pFmtIt->begin() );
	while (hr == S_OK && pFmtIt->read ( v ) == S_OK)
		{
		// Flags
		bReq	= true;
		bMiss	= false;

		// Dictionary for field
		CCLTRYE	( (IUnknown *)(NULL) != (unkV = v), E_INVALIDARG );
		CCLTRY	( _QI(unkV,IID_IDictionary,&pDictSpec) );

		// Size of output field
		CCLOK		( oSz = 0; )
		CCLOK		( bSz = false; )
		if (hr == S_OK && pDictSpec->load ( strkSize, vSz ) == S_OK)
			{
			// Size specified
			bSz = true;

			// Size, if a string is specified for the 'Size' field then it refers
			// to a field in the dictionary.
			if (adtValue::type(vSz) == VTYPE_STR)
				hr = pDct->load ( vSz, vSz );
			if (vSz.vtype == VTYPE_I4)
				oSz = vSz.vint;
			else if (vSz.vtype == VTYPE_I8)
				oSz = (S32)(vSz.vlong);
			else
				{
				lprintf ( LOG_DBG, L"%s: Size is an unexpected type/value (%d)\r\n", (LPCWSTR)strnName, vSz.vtype );
				hr = E_UNEXPECTED;
				}	// else
			}	// if

		// If no key name is specified, a predefined 'value' must be specified
		if (hr == S_OK)
			{
			if (pDictSpec->load ( strkName, oName ) == S_OK)
				{
				// Attempt to load value
				if ((hr = pDct->load ( oName, oVal )) != S_OK)
					bMiss = true;

				// If failed, check if it is required.
				if (	bMiss													&&
						pDictSpec->load ( strkReq, oVal ) == S_OK &&
						adtBool(oVal) == false )
					bReq = false;

				// Debug
				if (bMiss && bReq)
					lprintf ( LOG_DBG, L"%s: Error loading required value for name %s (0x%x)\r\n",
							(LPCWSTR)strnName, (LPCWSTR)adtString(oName), hr );
//				else
//					lprintf ( LOG_DBG, L"%s: Name %s Value %s\r\n", (LPCWSTR)strnName,
//							(LPCWSTR)adtString(oName), (LPCWSTR)adtString(oVal) );
				}	// if
			else
				{
				hr = pDictSpec->load ( strkVal, oVal );
				if (hr != S_OK)
					lprintf ( LOG_DBG, L"%s: Error loading predefined value (0x%x)\r\n",
							(LPCWSTR)strnName, hr );
				}	// else
			}	// if

		// Keep a copy of the 'unmapped' value
		CCLOK  ( adtValue::copy ( oVal, oValueUn ); )

		// Mapping specified ?  Allow format to specify a dictionary that will 'map'
		// incoming 'from' dictionary values to 'to' output values to use during format.
		if (hr == S_OK && pDictSpec->load ( strkMap, v ) == S_OK)
			{
			CCLTRY(_QISAFE((unkV=v),IID_IDictionary,&pMap));
			CCLTRY(adtValue::copy ( oVal, oKey ));
			CCLTRY(pMap->load ( oKey, oVal ));
			_RELEASE(pMap);

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"%s: Error loading key from map %s (0x%x)\r\n",
						(LPCWSTR)strnName, (LPCWSTR)adtString(oKey), hr );
			}	// if

		// If a type if specified for the value, allow conversion to the destination type
		if (hr == S_OK && pDictSpec->load ( strkType, strType ) == S_OK)
			{
			// Destination type
			VALUETYPE
			type =	(!WCASECMP ( strType, L"Double"))	? VTYPE_R8 :
						(!WCASECMP ( strType, L"Float"))		? VTYPE_R4 :
						(!WCASECMP ( strType, L"Integer" ))	? VTYPE_I4 :
						(!WCASECMP ( strType, L"Long" ))		? VTYPE_I8 :
						(!WCASECMP ( strType, L"Boolean" ))	? VTYPE_BOOL : 
						(!WCASECMP ( strType, L"Date" ))		? VTYPE_DATE : VTYPE_STR;

			// Convert
			hr = adtValue::toType ( oValueUn, type, oVal );
			}	// if

		// Write value to output based on type.
		CCLTRY ( writeValue ( oVal, pDictSpec, bSz, oSz ) );

		// 'Sub'format specified ?
		if (hr == S_OK && pDictSpec->load ( strkSub, v ) == S_OK)
			{
			// Load the subformat for the current value.  Ok if missing, just means
			// there are no additional parameters for the current value
			CCLTRY ( _QISAFE((unkV=v),IID_IDictionary,&pDictSub) );
			if (hr == S_OK && pDictSub->load ( oValueUn, v ) == S_OK)
				{
				// Format new specification
				CCLTRY ( _QISAFE((unkV=v),IID_IContainer,&pFmtSub) );
				CCLTRY ( format ( pFmtSub ) );

				// Clean up
				_RELEASE(pFmtSub);
				}	// if

			// Clean up
			_RELEASE(pDictSub);
			}	// if

		// If a value was missing but not required, it is okay to continue.
		if (bMiss && !bReq)
			hr = S_OK;

		// Clean up
		_RELEASE(pDictSpec);
		pFmtIt->next();
		}	// while

	// Clean up
	_RELEASE(pFmtIt);
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"%s: Format error 0x%x\r\n", (LPCWSTR)strnName, hr );

	return hr;
	}	// format

HRESULT DictFormat :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pR is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Fire
	if (_RCP(Fire))
		{
		// State check
		CCLTRYE ( pFmt != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// TEMP until string support added
		CCLTRYE ( pStm != NULL, ERROR_INVALID_STATE );

		// DEBUG
//		if (!WCASECMP(strnName,L"FmtTag"))
//			{
//			DebugBreak();
//			}	// if

		// Perform format
		CCLTRY ( format ( pFmt ) );

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pStm));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Format
	else if (_RCP(Format))
		{
		// A format specification is a list of dictionaries describing each field in the output.
		adtIUnknown	unkV(v);
		_RELEASE(pFmt);
		hr = _QISAFE(unkV,IID_IContainer,&pFmt);
		}	// else if

	// Dictionary
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		hr = _QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if

	// Stream
	else if (_RCP(Stream))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStm);
		hr = _QISAFE(unkV,IID_IByteStream,&pStm);
		}	// else if

	// State
	else if (_RCP(Endian))
		{
		adtString	strEnd(v);
		if (!WCASECMP(strEnd,L"Big"))
			bEndianBig = true;
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT DictFormat :: writeValue	(	adtValue &vWr, IDictionary *pFmt,
												bool bSz, U32 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Write a value to the stream.
	//
	//	PARAMETERS
	//		-	vWr is the value to write.
	//		-	pFmt is the dictionary format specification.
	//		-	bSz is true if the size was specified
	//		-	uSz is the size specified for the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr = S_OK;
	adtValue			vL;
	adtIUnknown		unkV;

	// Write value to output based on type.  Little endian default.
	// Integer
	if (hr == S_OK && (vWr.vtype == VTYPE_I4 || vWr.vtype == VTYPE_I8))
		{
		// If no size was specified, use default for type
		if (!bSz)	uSz = (vWr.vtype == VTYPE_I4) ? 4 : 8;

		// Optional bit masking available
		if (pFmt->load ( adtString(L"Bits"), vL ) == S_OK)
			{
			IDictionary	*pBits	= NULL;
			IIt			*pItK		= NULL;

			// Iterate the specified key names
			CCLTRY(_QISAFE((unkV=vL),IID_IDictionary,&pBits));
			CCLTRY(pBits->keys ( &pItK ) );
			while (hr == S_OK && pItK->read ( vL ) == S_OK)
				{
				IDictionary		*pBit		= NULL;
				IDictionary		*pBitMap = NULL;
				adtString		strKey(vL);
				adtValue			vRes;
				U64				lMask;

				// Bit mask descriptor
				CCLTRY(pBits->load ( vL, vL ));
				CCLTRY(_QISAFE((unkV=vL),IID_IDictionary,&pBit));

				// Bit mask
				CCLTRY(pBit->load ( adtString(L"Mask"), vL ));
				CCLOK (lMask = adtLong(vL);)

				// Load the value for the bit from the dictionary
				if (hr == S_OK && pDct->load ( strKey, vRes ) == S_OK)
					{
					// Map result if specified
					if (	hr == S_OK													&& 
							pBit->load ( adtString(L"Map"), vL ) == S_OK		&&
							(IUnknown *)(NULL) != (unkV=vL)						&&
							_QI((unkV=vL),IID_IDictionary,&pBitMap) == S_OK	&&
							pBitMap->load ( vRes, vL ) == S_OK )
						hr = adtValue::copy ( vL, vRes );

					// Mask in/out the bit
					if	(adtValue::type(vWr) == VTYPE_I4)
						{
						adtInt	iRes(vRes);

						// Clear existing mask then or in masked value
						vWr.vint &= ~((U32)lMask);
						vWr.vint |= (iRes.vint & (U32)lMask);
						}	// if
					else if	(adtValue::type(vWr) == VTYPE_I8)
						{
						adtLong	lRes(vRes);

						// Clear existing mask then or in masked value
						vWr.vlong &= ~(lMask);
						vWr.vlong |= (lRes.vlong & lMask);
						}	// else if

					}	// if

				// Clean up
				_RELEASE(pBitMap);
				_RELEASE(pBit);
				pItK->next();
				}	// while

			// Debug
//				lprintf ( LOG_DBG, L"Bits 0x%x\r\n", vWr.vint );

			// Clean up
			_RELEASE(pItK);
			_RELEASE(pBits);
			}	// if

		// Write value
		switch ((U32)uSz)
			{
			// Explicit set of size 0 means 'no-op' just using key in dictionary for something else
			case 0 :
				// No-op
				break;

			// Byte
			case 1 :
				{
				U8	b = (vWr.vtype == VTYPE_I8) ? (U8)(vWr.vlong & 0xff) : (U8)(vWr.vint & 0xff);
				CCLTRY ( pStm->write ( &b, 1, NULL ) );
				}	// case 1
				break;

			// Short
			case 2 :
				{
				U16	s = (vWr.vtype == VTYPE_I8) ? (U16)(vWr.vlong & 0xffff) : (U16)(vWr.vint & 0xffff);
				if (bEndianBig)	s = SWAPS(s);
				CCLTRY ( pStm->write ( &s, 2, NULL ) );
				}	// case 2
				break;

			// Long
			case 4 :
				{
				U32	i = (vWr.vtype == VTYPE_I8) ? (U32)(vWr.vlong & 0xffffffffff) : (U32)(vWr.vint);
				if (bEndianBig)	i = SWAPI(i);
				CCLTRY ( pStm->write ( &i, 4, NULL ) );
				}	// case 4
				break;

			// 64-bits
			case 8 :
				{
				U64	i = (vWr.vtype == VTYPE_I8) ? (U64)(vWr.vlong) : (U64)(vWr.vint);
				if (bEndianBig)	i = SWAPL(i);
				CCLTRY ( pStm->write ( &i, 8, NULL ) );
				}	// case 4
				break;

			default :
				lprintf ( LOG_DBG, L"Invalid integer size %d\n", uSz );
				hr = E_UNEXPECTED;
			}	// switch
		}	// if

	// Long
	else if (hr == S_OK && vWr.vtype == VTYPE_I8 && (uSz > 0 || !bSz))
		{
		U64 v = vWr.vlong;
		CCLTRY ( pStm->write ( &v, sizeof(v), NULL ) );
		}	// if

	// Float
	else if (hr == S_OK && vWr.vtype == VTYPE_R4 && (uSz > 0 || !bSz))
		{
		float	vflt = vWr.vflt;
		CCLTRY ( pStm->write ( &vflt, sizeof(vflt), NULL ) );
		}	// if

	// Double
	else if (hr == S_OK && vWr.vtype == VTYPE_R8 && (uSz > 0 || !bSz))
		{
		double vdbl = vWr.vdbl;
		CCLTRY ( pStm->write ( &vdbl, sizeof(vdbl), NULL ) );
		}	// if

	// String.  If size was explicitly set to zero, then specifier is
	// using value for other reasons.
	else if (hr == S_OK && adtValue::type(vWr) == VTYPE_STR && (uSz > 0 || !bSz))
		{
		adtString	sVal(vWr);

		// Write each ASCIIified character to the stream
		U32	i,len = sVal.length();
		U8		c,z	= 0;

		// Null termination ?
		adtBool 	bNull(false);
		if (pFmt->load ( adtString(L"Null"), vL ) == S_OK)
			bNull = vL;

		// String, allow for variable length string by setting size to zero
		for (i = 0;i < len && (uSz == (U32)0 || i < uSz) && hr == S_OK;++i)
			{
			// Convert
			CCLOK ( c = (U8) (sVal[i] & 0xff); )

			// Write
			CCLTRY ( pStm->write ( &c, 1, NULL ) );
			}	// for

		// Null termination ?
		if (hr == S_OK && bNull)
			{
			hr = pStm->write ( &z, 1, NULL );
			++i;
			}	// if

		// Remaining space
		for (;i < uSz && hr == S_OK;++i)
			hr = pStm->write ( &z, 1, NULL );
		}	// else if

	// Boolean
	else if (hr == S_OK && vWr.vtype == VTYPE_BOOL && (uSz > 0 || !bSz))
		{
		U16 vb = vWr.vbool;
		CCLTRY ( pStm->write ( &vb, sizeof(vb), NULL ) );
		}	// if

	// Object
	else if (hr == S_OK && vWr.vtype == VTYPE_UNK)
		{
		IByteStream		*pStmSrc	= NULL;
		IMemoryMapped	*pMem		= NULL;
		IContainer		*pCnt		= NULL;

		// Byte stream ?
		if (vWr.punk != NULL && _QI(vWr.punk,IID_IByteStream,&pStmSrc) == S_OK)
			{
			U64	pos = 0;

			// Remember original position of stream
			CCLTRY ( pStmSrc->seek ( 0, STREAM_SEEK_CUR, &pos ) );

			// Copy to destination
			CCLTRY ( pStmSrc->copyTo ( pStm, uSz, NULL ) );

			// Restore position
			CCLTRY ( pStmSrc->seek ( pos, STREAM_SEEK_SET, NULL ) );

			// Clean up
			_RELEASE(pStmSrc);
			}	// if

		// Memory mapped ?
		else if (vWr.punk != NULL && _QI(vWr.punk,IID_IMemoryMapped,&pMem) == S_OK)
			{
			void	*pvMem	= NULL;
			U32	sz			= uSz;

			// Access memory
			CCLTRY(pMem->getInfo(&pvMem,&sz));

			// If a size is specified, use only that amount
			if (uSz > 0) sz = uSz;

			// Copy to destination
			CCLTRY ( pStm->write ( pvMem, sz, NULL ) );

			// Clean up
			_RELEASE(pMem);
			}	// else if

		// List of values ?
		else if (vWr.punk != NULL && _QI(vWr.punk,IID_IContainer,&pCnt) == S_OK)
			{
			IIt	*pIt	= NULL;

			// Iterate the values in the list
			CCLTRY ( pCnt->iterate ( &pIt ) );
			while (hr == S_OK && pIt->read ( vL ) == S_OK)
				{
				// Write each value out using the same specification for each value
				CCLTRY ( writeValue ( vL, pFmt, bSz, uSz ) );

				// Continue with iteration
				pIt->next();
				}	// while

			// Clean up
			_RELEASE(pIt);
			_RELEASE(pCnt);
			}	// else if

		// Unhandled object
		else
			{
			lprintf ( LOG_DBG, L"Unsupported object for value (%p)\r\n", vWr.punk );
			hr = ERROR_NOT_SUPPORTED;
			}	// else

		}	// else if

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"%s: Error writing value (0x%x)\r\n", (LPCWSTR)strnName, hr );

	return hr;
	}	// writeValue
