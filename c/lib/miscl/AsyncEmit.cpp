////////////////////////////////////////////////////////////////////////
//
//									ASYNCE.CPP
//
//				Implementation of the asynchronous emission node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <stdio.h>

AsyncEmit :: AsyncEmit ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the CD Player node
	//
	////////////////////////////////////////////////////////////////////////
	pThrd		= NULL;
	bRun		= false;
	iPri		= 0;
	bSingle	= false;
	bDbg		= false;
	evEmit.init();
	}	// AsyncEmit

HRESULT AsyncEmit :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Default states
		pnDesc->load ( adtString(L"Value"), vVal );
		if (pnDesc->load ( adtString(L"Single"), vL ) == S_OK)
			bSingle = vL;
		if (pnDesc->load ( adtString(L"Debug"), vL ) == S_OK)
			bDbg = adtBool(vL);
		}	// if

	// Detach
	else if (!bAttach)
		{
		// Shutdown thread
		if (pThrd != NULL)
			{
			pThrd->threadStop(5000);
			_RELEASE(pThrd);
			}	// if
		}	// else if

	return hr;
	}	// onAttach

HRESULT AsyncEmit :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Emit
	if (_RCP(Fire))
		{
		// Debug
//		lprintf ( LOG_DBG, L"%s:%p:Fire {\r\n", (LPCWSTR)strnName, this );

		// Single shot ?
		if (bSingle == true && pThrd != NULL)
			{
			bRun = false;
			pThrd->threadStop(5000);
			_RELEASE(pThrd);
			}	// if

		// Create thread if necessary
		if (hr == S_OK && pThrd == NULL)
			{
			CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
			CCLOK (bRun = true;)
			CCLTRY(pThrd->threadStart ( this, 5000, strnName ));
			}	// if

		// Copy value to emit
		if (hr == S_OK)
			{
			// Thread safety
			csVal.enter();

			// Available ?
			if (hr == S_OK)
				{
				// Is the emission value empty ?
				CCLTRYE ( adtValue::empty(vEmit) == true, E_UNEXPECTED );

				// Debug
				if (hr != S_OK && bDbg)
					lprintf ( LOG_WARN, L"%s:Lost value to busy emitter\r\n", (LPCWSTR)strnName );
				}	// if

			// New value
//			if (hr == S_OK)	lprintf ( LOG_DBG, L"%s:Setting value\r\n", (LPCWSTR)strnName );
//			if (hr != S_OK) 	lprintf ( LOG_DBG, L"%s:Busy\r\n", (LPCWSTR)strnName );
			CCLTRY ( adtValue::copy ( adtValue::empty(vVal) ? v : vVal, vEmit ) );

			// Thread safety
			csVal.leave();

			// Signal availability
			CCLOK ( evEmit.signal(); )
			}	// if

		// Debug
//		lprintf ( LOG_DBG, L"%s:%p:} Fire hr 0x%x\r\n", (LPCWSTR)strnName, this, hr );
		}	// if

	// Stop processing
	else if (_RCP(Stop))
		{
		// Shutdown thread
		if (pThrd != NULL)
			{
			pThrd->threadStop(5000);
			_RELEASE(pThrd);
			}	// if

		// Clear last emission value
		adtValue::clear(vEmit);
		}	// else if

	// State
	else if (_RCP(Value))
		adtValue::copy ( v, vVal );
	else if (_RCP(Priority))
		iPri = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT AsyncEmit :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	adtValue	vEmitNow;

	// Wait for value to emit
//	CCLOK ( lprintf ( LOG_DBG, L"%s:Waiting\r\n", (LPCWSTR)strnName ); )
	CCLTRYE ( evEmit.wait(-1), ERROR_TIMEOUT );
//	CCLOK ( lprintf ( LOG_DBG, L"%s:Waited\r\n", (LPCWSTR)strnName ); )

	// Still running ?
	CCLTRYE ( bRun == true, S_FALSE );

	// Emit value
	if (hr == S_OK && !adtValue::empty(vEmit))
		{
		// Debug
//		lprintf ( LOG_DBG, L"%s:%p:Tick {\r\n", (LPCWSTR)strnName, this );

		// Make local copy so next value can be readied immediately
		csVal.enter();
		CCLTRY ( adtValue::copy ( vEmit, vEmitNow ) );

		// Clear value to signal slot available
		CCLOK ( adtValue::clear ( vEmit ); )
		csVal.leave();

		// Emit
		CCLOK ( _EMT(Fire,vEmitNow); )

		// Debug
//		lprintf ( LOG_DBG, L"%s:%p:} Tick hr 0x%x\r\n", (LPCWSTR)strnName, this, hr );

		// No need to exit thread due to emission error
		hr = S_OK;
		}	// if

	// Single shot ?
	if (hr == S_OK && bSingle == true)
		hr = S_FALSE;

	return hr;
	}	// tick

HRESULT AsyncEmit :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;

	// Priority
	#ifdef	_WIN32
	if (	hr == S_OK && iPri != 0 )
		{
		if (!SetThreadPriority ( GetCurrentThread(),
				(iPri.vint == 2)	?	THREAD_PRIORITY_HIGHEST :
				(iPri.vint == 1)	?	THREAD_PRIORITY_ABOVE_NORMAL :
				(iPri.vint == -1)	?	THREAD_PRIORITY_BELOW_NORMAL :
				(iPri.vint == -2)	?	THREAD_PRIORITY_LOWEST :
											THREAD_PRIORITY_NORMAL ))
			dbgprintf ( L"Timer::tickBegin:Unable to set priority\n" );
		}	// if
	#endif

	#if	defined(__unix__) || defined(__APPLE__)
	if (hr == S_OK && iPri != 0)
		{
		pthread_t	self;
		sched_param	sp;
		int			p,ret;

		// Get current priority and adjust by the specified amount
		CCLOK		( self = pthread_self(); )
		CCLTRYE	( (ret = pthread_getschedparam ( self, &p, &sp )) == 0, ret );
		CCLOK		( sp.sched_priority += ((S32)iPri); )
		CCLTRYE	( (ret = pthread_setschedparam ( self, p, &sp )) == 0, ret );
		}	// if
	#endif

	return S_OK;
	}	// tickBegin
