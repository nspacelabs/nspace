////////////////////////////////////////////////////////////////////////
//
//									DCTPRS.CPP
//
//		Implementation of the stream or string to dictionary parse node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <stdio.h>

DictParse :: DictParse ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pFmt			= NULL;
	pStm			= NULL;
	pDict			= NULL;
	bEndianBig	= false;
//	strParse		= "";
	}	// DictParse

HRESULT DictParse :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		v;
		adtIUnknown	unkV;

		// Allow format to specified as a node property
		if (pnDesc->load ( adtString(L"Format"),	v ) == S_OK)
			_QISAFE((unkV=v),IID_IContainer,&pFmt);
		if (pnDesc->load ( adtString(L"Endian"),	v ) == S_OK)
			onReceive ( prEndian, v );
		}	// if

	// Detach
	else
		{
		_RELEASE(pFmt);
		_RELEASE(pDict);
		_RELEASE(pStm);
		}	// else

	return hr;
	}	// onAttach

HRESULT DictParse :: parse ( IContainer *pFmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Parses a binary stream or string into a dictionary using 
	//			the specified format.
	//
	//	PARAMETERS
	//		-	pFmt is the format specification
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr					= S_OK;
	IDictionary	*pDictSpec		= NULL;
	IDictionary	*pDictSub		= NULL;
	IContainer	*pFmtSub			= NULL;
	IIt			*pFmtIn			= NULL;
	bool			bEndianBigLcl	= false;
	bool			bSz;
	adtIUnknown	unkV;
	adtValue		vSz;
	adtString	sName,sType;
	adtValue		vVal,vValUn,vValChk,vL;
	adtInt		uSz;

	// Iterator
	CCLTRY ( pFmt->iterate ( &pFmtIn ) );

	// Convert each field from stream according to specification
	CCLTRY ( pFmtIn->begin() );
	while (hr == S_OK && pFmtIn->read ( unkV ) == S_OK)
		{
		// Setup
		uSz 				= 0;
		bSz				= false;
		bEndianBigLcl	= false;

		// Property set
		CCLTRY	( _QISAFE ( unkV, IID_IDictionary, &pDictSpec ) );

		// Fields (required)
		CCLTRY	( pDictSpec->load ( adtString(L"Type"), vL ) );
		CCLTRY 	( adtValue::toString(vL,sType) );

		// The 'type' field generally specifies the string version of
		// the value type.  Also allow it to specify a key in the dictionary
		// in case the type is specified somewhere else.
		if (hr == S_OK && pDict->load ( sType, vL ) == S_OK)
			{
			// Allow type to specified as the integer version of the type (VTYPE_XXX)
			if (adtValue::type(vL) == VTYPE_I4)
				{
				adtInt 	iType(vL);	
//				lprintf ( LOG_DBG, L"Integer type specified : %d\r\n", (S32)iType );
				sType = 	(iType == VTYPE_I4) 		? L"Integer" :
							(iType == VTYPE_I8) 		? L"Long" :
							(iType == VTYPE_R4) 		? L"Float" :
							(iType == VTYPE_R8) 		? L"Doble" :
							(iType == VTYPE_BOOL) 	? L"Bool" :
							(iType == VTYPE_STR) 	? L"String" : L"";

				// For integers, a default size is inferred/needed
				if			(iType == VTYPE_I4)	uSz = 4;
				else if 	(iType == VTYPE_I8)	uSz = 8;
				}	// if
			else
				hr = adtValue::toString(vL,sType);
			}	// if

		// Endian per field (optional)
		if (	hr == S_OK && 
				pDictSpec->load ( adtString(L"Endian"), vL ) == S_OK &&
				!WCASECMP(adtString(vL),L"Big") )
			bEndianBigLcl = true;

		// Size field required for certain types
		if (pDictSpec->load ( adtString(L"Size"), vSz ) == S_OK)
			{
			// Size set explicitly
			bSz = true;

			// Size, if a string is specified for the 'Size' field then it refers
			// to a field in the dictionary.
			if (hr == S_OK && adtValue::type(vSz) == VTYPE_STR)
				hr = pDict->load ( vSz, uSz );
			else if (hr == S_OK && vSz.vtype == VTYPE_I4)
				uSz = vSz.vint;
			else
				hr = E_UNEXPECTED;
			}	// if

		// Debug
//		U64 av;
//		pStm->available(&av);
//		lprintf ( LOG_DBG, L"Name %s Type %s uSz %d Avail %d\r\n", (LPCWSTR) sName, (LPCWSTR)sType, (S32)uSz, av );

		// If size is explicitly set to zero then a previously stored is value is being
		// used to specify a map and/or subformat without storing additional bytes
		// in output stream.
		if (hr == S_OK && uSz == 0 && bSz)
			{
			// A name must be specific for previously parsed field
			CCLTRY ( pDictSpec->load ( adtString(L"Name"), sName ) );
			CCLTRY ( pDict->load ( sName, vVal ) );
//			lprintf ( LOG_DBG, L"Size set to zero : %s = %s (0x%x)\r\n", 
//							(LPCWSTR)sName, (LPCWSTR)adtString(vVal), hr );
			}	// if

		// Convert field based on type.  Little endian default.
		else if (hr == S_OK && !WCASENCMP ( L"Int", sType, 3 ))
			{
			adtInt	uInt;
			adtBool	bSigned(false);

			// Signed option
			if (hr == S_OK && pDictSpec->load ( adtString(L"Signed"), vL ) == S_OK)
				bSigned = vL;

			// Read value
			switch ((U32)uSz)
				{
				// Byte
				case 1 :
					{
					if (bSigned)
						{
						S8		b;
						CCLTRY ( read ( pStm, &b, 1 ) );
						CCLOK  ( uInt.vint = (S32)b; )
						}	// if
					else
						{
						CCLTRY ( read ( pStm, &(uInt.vint), uSz ) );
						}	// else
					}	// case 1
					break;

				// Short
				case 2 :
					{
					if (bSigned)
						{
						S16	i;
						CCLTRY ( read ( pStm, &i, 2) );
						if (hr == S_OK && (bEndianBig || bEndianBigLcl))
							i = ((S16) SWAPS(i));
						CCLOK  ( uInt.vint = (S32)i; )
						}	// if
					else
						{
						CCLTRY ( read ( pStm, &(uInt.vint), 2 ) );
						if (hr == S_OK && (bEndianBig || bEndianBigLcl))
							uInt.vint = ((U16) SWAPS(uInt.vint));
						}	// else
					}	// case 1
					break;

				// Integer
				case 4 :
					if (bSigned)
						{
						S32	i;
						CCLTRY ( read ( pStm, &i, 4 ) );
						if (hr == S_OK && (bEndianBig || bEndianBigLcl))
							i = ((S32) SWAPI(i));
						CCLOK  ( uInt.vint = (S32)i; )
						}	// if
					else
						{
						CCLTRY ( read ( pStm, &(uInt.vint), 4 ) );
						if (hr == S_OK && (bEndianBig || bEndianBigLcl))
							uInt.vint = ((U32) SWAPI(uInt.vint));
						}	// else
					break;

				// Long
				case 8 :
					if (bSigned)
						{
						S64	i;
						CCLTRY ( read ( pStm, &i, 8 ) );
						if (hr == S_OK && (bEndianBig || bEndianBigLcl))
							i = ((S64) SWAPL(i));
						CCLOK  ( uInt.vlong = (S64)i; )
						}	// if
					else
						{
						CCLTRY ( read ( pStm, &(uInt.vlong), 8 ) );
						if (hr == S_OK && (bEndianBig || bEndianBigLcl))
							uInt.vlong = ((U64) SWAPL(uInt.vlong));
						}	// else
					break;

				default :
					lprintf ( LOG_DBG, L"Invalid integer size %d\n", (U32)uSz );
					hr = E_UNEXPECTED;
				}	// switch

			// Optional bit masking available
			if (pDictSpec->load ( adtString(L"Bits"), vL ) == S_OK)
				{
				IDictionary	*pBits	= NULL;
				IIt			*pItK		= NULL;

				// Debug
//				lprintf ( LOG_DBG, L"Bits 0x%x\r\n", (U32)adtInt(vL) );

				// Iterate the specified key names
				CCLTRY(_QISAFE((unkV=vL),IID_IDictionary,&pBits));
				CCLTRY(pBits->keys ( &pItK ) );
				while (hr == S_OK && pItK->read ( vL ) == S_OK)
					{
					IDictionary		*pBit		= NULL;
					IDictionary		*pBitMap = NULL;
					adtString		strKey(vL);
					adtValue			vRes;
					U32				iMask;

					// Bit mask descriptor
					CCLTRY(pBits->load ( vL, vL ));
					CCLTRY(_QISAFE((unkV=vL),IID_IDictionary,&pBit));
					
					// Bit mask
					CCLTRY(pBit->load ( adtString(L"Mask"), vL ));

					// Apply mask
					CCLTRY ( adtValue::copy ( adtInt ( ((iMask = adtInt(vL)) & uInt) ), vRes ) );

					// Map result if available
					if (	hr == S_OK													&& 
							pBit->load ( adtString(L"Map"), vL ) == S_OK		&&
							(IUnknown *)(NULL) != (unkV=vL)						&&
							_QI((unkV=vL),IID_IDictionary,&pBitMap) == S_OK &&
							pBitMap->load ( vRes, vL ) == S_OK )
						hr = adtValue::copy ( vL, vRes );

					// Store result under key
					CCLTRY ( pDict->store ( strKey, vRes ) );

					// Clean up
					_RELEASE(pBitMap);
					_RELEASE(pBit);
					pItK->next();
					}	// while

				// Clean up
				_RELEASE(pItK);
				_RELEASE(pBits);
				}	// if

			// Store
			CCLOK  ( adtValue::copy ( uInt, vVal ); )
			}	// if

		else if (hr == S_OK && !WCASECMP ( L"Long", sType ))
			{
			adtLong	lV;

			// TODO: Variable sizes of integer into a long like I4 above.

			// Read
			CCLTRY ( read ( pStm, &(lV.vlong), sizeof(lV.vlong) ) );
			if (hr == S_OK && (bEndianBig || bEndianBigLcl))	
				lV.vlong = ((U32) SWAPL(lV.vlong));
			CCLOK  ( adtValue::copy ( lV, vVal ); )
			}	// if

		else if (hr == S_OK && !WCASECMP ( L"Float", sType ))
			{
			adtFloat	fFlt;

			// Read
			CCLTRY ( read ( pStm, &(fFlt.vflt), sizeof(fFlt.vflt) ) );
			if (hr == S_OK && (bEndianBig || bEndianBigLcl))	
				fFlt.vint = ((U32) SWAPI(fFlt.vint));
			CCLOK  ( adtValue::copy ( fFlt, vVal ); )
			}	// if

		else if (hr == S_OK && !WCASECMP ( L"Double", sType ))
			{
			adtDouble	fDbl;

			// Read
			CCLTRY ( read ( pStm, &(fDbl.vdbl), sizeof(fDbl.vdbl) ) );
			if (hr == S_OK && (bEndianBig || bEndianBigLcl))	
				fDbl.vlong = ((U32) SWAPL(fDbl.vlong));
			CCLOK  ( adtValue::copy ( fDbl, vVal ); )
			}	// if

		else if (hr == S_OK && !WCASENCMP ( L"Bool", sType, 4 ))
			{
			adtBool bV;

			// Read
			CCLTRY ( read ( pStm, &(bV.vbool), sizeof(bV.vbool) ) );
			CCLOK  ( adtValue::copy ( bV, vVal ); )
			}	// if

		else if (hr == S_OK && (!WCASECMP ( L"String", sType ) || !WCASECMP ( L"StringW", sType )))
			{
			// Converting to internal string from assumed ASCII.
			WCHAR			*pwStr	= NULL;
			U16			csz,c		= 0;
			bool			bEnd		= false;
			adtValue		vL;
			adtString	sEnd;
			U32			i,j,uAllocd,elen;

			// String (ASCII) or StringW (Wide char - 2bytes per char)
			csz = (!WCASECMP ( L"StringW", sType )) ? 2 : 1;

			// If an 'end' field is specified, an ending sequence of
			// characters can be specified and the size will be the max allowed
			uAllocd = 0;
			if (	hr == S_OK && 
					pDictSpec->load ( adtString(L"End"), vL ) == S_OK )
				{
				hr = adtValue::toString ( vL, sEnd );
				CCLOK ( bEnd = true; )
				}	// if
			elen = sEnd.length();

			// Pre-allocate space for string
			i = 0;
			while (hr == S_OK && read ( pStm, &c, csz ) == S_OK)
				{
				// Ensure enough space in buffer
				if (hr == S_OK && i >= uAllocd)
					{
					// Re-allocate buffer
					uAllocd	+= 128;
					pwStr		= (WCHAR *) _REALLOCMEM ( pwStr, (uAllocd+1)*sizeof(WCHAR) );
					}	// if

				// Add character
				CCLOK ( pwStr[i++] = (csz == 1) ? WCHAR(c & 0xff) : c; )

				// End of string ?
				if (hr == S_OK && uSz > 0 && i >= uSz)
					break;

				// Check for ending sequence ?
				if (hr == S_OK && bEnd)
					{
					// Special case is the end string is a 0 length string
					// which means end at null termination
					if (elen == 0 && (c & 0xff) == '\0')
						break;
					else if (elen > 0 && i >= elen)
						{
						bool	bMatch = true;
						for (j = 0;j < sEnd.length() && bMatch;++j)
							if (pwStr[i-sEnd.length()+j] != sEnd.at(j))
								bMatch = false;

						// Match ?
						if (bMatch)
							break;
						}	// else if
					}	// if

				}	// for

			// Terminate string
			if (hr == S_OK && pwStr != NULL)
				{
				pwStr[i] = WCHAR('\0');
				adtValue::copy ( adtString(pwStr), vVal );
				}	// if

			// Clean up
			_FREEMEM(pwStr);
			}	// if

		// NOTE: New types are 'Block' for a block of memory, or 'Stream' for a memory backed byte stream
		// 'Binary' left for backward compatibility (needed ?)
		else if (hr == S_OK && !WCASECMP ( L"Binary", sType ))
			{
			IByteStream	*pStmDst = NULL;
			U32			uSzElem	= 1;
			U32			uSzMult	= 1;
			U64			nrd		= uSz;
			adtIUnknown	unkV;

			// If size is zero, use remaining stream
			if (nrd == 0)
				hr = pStm->available(&nrd);

//			if (uSz == 10201)
//				lprintf ( LOG_DBG, L"Hi\r\n" );

			// Size must be specified, ok if zero just means no data
			if (hr == S_OK && nrd > 0)
				{
				// Allow an optional element size (e.g., arrays of structures)
				if (hr == S_OK && pDictSpec->load ( adtString(L"SizeElem"), vL ) == S_OK)
					uSzElem = adtInt(vL);

/*
				// An alternative 'multiplier' for the size can be specified
				if (hr == S_OK && pDictSpec->load ( adtString(L"SizeMult"), vL ) == S_OK)
					{
					// Size, if a string is specified for the 'Size' field then it refers
					// to a field in the dictionary.
					if (hr == S_OK && adtValue::type(vL) == VTYPE_STR)
						{
						CCLTRY(pDict->load ( adtString(vL), vL ));
						CCLOK (uSzMult = adtInt(vL);)
						}	// if
					else if (hr == S_OK && vSz.vtype == VTYPE_I4)
						uSzMult = adtInt(vL);
					else
						hr = E_UNEXPECTED;
					}	// if
*/
//				lprintf ( LOG_DBG, L"Binary nrd %d\r\n", nrd );

				// Unformatted data, store as stream
				CCLTRY ( COCREATE ( L"Io.StmMemory", IID_IByteStream, &pStmDst ) );

				// Copy bytes
				CCLTRY( pStm->copyTo ( pStmDst, (nrd*uSzElem)*uSzMult, NULL ); )
				CCLTRY( pStmDst->seek ( STREAM_SEEK_SET, 0, NULL ) );
				CCLOK ( adtValue::copy ( adtIUnknown(pStmDst), vVal ); )

				// Clean up
				_RELEASE(pStmDst);
				}	// if
			}	// if

		// Block of memory
		else if (hr == S_OK && !WCASECMP ( L"Block", sType ))
			{
			IMemoryMapped	*pMem		= NULL;
			void				*pvMem	= NULL;
			U64				nrd		= uSz;
			U64				nrdd		= 0;

			// Create a memory mapped block
			CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pMem ) );

			// If size is zero, use remaining stream
			if (nrd == 0)
				hr = pStm->available(&nrd);

			// Ok for zero ?
			if (nrd > 0)
				{
				// Size the stream
				CCLTRY ( pMem->setSize ( (U32) nrd ) );

				// Access memory
				CCLTRY ( pMem->getInfo(&pvMem,NULL) );

				// Read stream into memory
				CCLTRY ( pStm->read ( pvMem, nrd, &nrdd ) );

				// If size was less than specified, re-size memory block
				// so it matches
				if (hr == S_OK && nrdd != nrd)
					hr = pMem->setSize ( (U32)nrdd );
				}	// if

			// Result
			CCLTRY ( adtValue::copy ( adtIUnknown(pMem), vVal ) );

			// Clean up
			_RELEASE(pMem);
			}	// if

		// GUID
		#ifdef	_WIN32
		else if (hr == S_OK && !WCASENCMP ( L"GUID", sType, 4 ))
			{
			LPOLESTR	pstr = NULL;
			GUID		guid;

			// Allow a GUID stored in binary in the byte stream to be parsed
			// It will be stored as a string GUID in the dictionary
			CCLTRY ( pStm->read ( &guid, sizeof(guid), NULL ) );
			CCLTRY ( StringFromCLSID ( guid, &pstr ) );

			// Store result
			CCLTRY ( adtValue::copy ( adtString(pstr), vVal ) );

			// Clean up
			_FREETASKMEM(pstr);
			}	// else if
		#endif

		// Cache unmapped value for indexing
		CCLTRY(adtValue::copy ( vVal, vValUn ));

		// Mapping ?  Format specification can contain a mapping of from/to values
		if (hr == S_OK && pDictSpec->load ( adtString(L"Map"), unkV ) == S_OK)
			{
			IDictionary	*pMap	= NULL;

			// Map specifier
			CCLTRY(_QISAFE(unkV,IID_IDictionary,&pMap));

			// Load mapping, no need to error out on non-map (?)
			CCLOK(pMap->load ( vValUn, vVal );)

			// Clean up
			_RELEASE(pMap);
			}	// if
		else if (hr == S_OK)
			hr = adtValue::copy ( vVal, vValUn );

		// Process
		if (hr == S_OK)
			{
			// If name is specified, store under specified key
			if (pDictSpec->load ( adtString(L"Name"), sName ) == S_OK)
				{
				// TODO: Enable debug output in node descriptor to avoid having to recompile

				// Debug
//				{
//				adtString strV;
//				adtValue::toString(vVal,strV);
//				lprintf ( LOG_DBG, L"%s = %s\r\n", (LPCWSTR)sName, (LPCWSTR) strV );
//				}	// Debug
//				if (!WCASECMP(sName,L"Dump"))
//					lprintf ( LOG_DBG, L"Break!\r\n" );

				// Store
				hr = pDict->store ( sName, vVal );
				}	// if

			// If a value is specified, make sure it matches
			else if (pDictSpec->load ( adtString(L"Value"), vValChk ) == S_OK)
				hr = (adtValue::compare ( vValChk, vVal ) == 0) ? S_OK : S_FALSE;
			}	// if

		// 'Sub'format specified ?
		if (hr == S_OK && pDictSpec->load ( adtString(L"Subformat"), unkV ) == S_OK)
			{
			// Load the subformat for the current value.  Ok if missing, just means
			// there are no additional parameters for the current value
			CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDictSub) );
			if (hr == S_OK && pDictSub->load ( vValUn, unkV ) == S_OK)
				{
				// Parse new specification
				CCLTRY ( _QISAFE(unkV,IID_IContainer,&pFmtSub) );
				CCLTRY ( parse ( pFmtSub ) );

				// Clean up
				_RELEASE(pFmtSub);
				}	// if

			// Clean up
			_RELEASE(pDictSub);
			}	// if

		// Clean up
		_RELEASE(pDictSpec);
		pFmtIn->next();
		}	// while

	// Clean up
	_RELEASE(pFmtIn);

	return hr;
	}	// parse

HRESULT DictParse :: read ( IByteStream *pStm, void *pvBfr, U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Reads from stream until error or complete.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pvBfr will receive the data.
	//		-	sz is the size of the transfer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U8			*pcBfr	= (U8 *) pvBfr;
	U64		nleft		= sz;
	U64		nx;

	// Continue until error or done
	while (hr == S_OK && nleft)
		{
		CCLTRY( pStm->read ( pcBfr, nleft, &nx ) );
		CCLOK	( nleft -= nx; )
		CCLOK	( pcBfr += nx; )
		}	// while

	// Debug
//	if (hr != S_OK)
//		lprintf ( LOG_DBG, L"hr 0x%x pcBfr %p nleft %d nx %d\r\n",
//									hr, pcBfr, nleft, nx );
 
	return hr;
	}	// read

HRESULT DictParse :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Fire
	if (_RCP(Fire))
		{
		adtIUnknown	unkV;
		adtString	sName,sType;
		adtValue		vVal;

		// State check
		CCLTRYE ( (pFmt != NULL),		ERROR_INVALID_STATE );
		CCLTRYE ( (pStm != NULL),		ERROR_INVALID_STATE );
		CCLTRYE ( (pDict != NULL),		ERROR_INVALID_STATE );

		// Debug
//		if (!WCASECMP(this->strnName,L"FrmPrs"))
//			dbgprintf(L"Hi\r\n" );

		// Parse as much as possible
		CCLTRY ( parse ( pFmt ); )

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDict) );
		else
			{
//			lprintf ( LOG_DBG, L"Failed : 0x%x\r\n", hr );
			_EMT(Error,adtIUnknown(pDict) );
			}	// else
		}	// if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	vUnk(v);

		// New value
		_RELEASE(pDict);
		CCLTRYE	( (IUnknown *)(NULL) != vUnk, E_INVALIDARG );
		CCLTRY	( _QI(vUnk,IID_IDictionary,&pDict) );
		}	// else if
	else if (_RCP(Format))
		{
		adtIUnknown		vUnk(v);

		// Clean up
		_RELEASE(pFmt);

		// A format specification is a list of property sets containing information
		// about each field
		CCLTRYE	( (IUnknown *)(NULL) != vUnk, E_INVALIDARG );
		CCLTRY	( _QI(vUnk,IID_IContainer,&pFmt) );
		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown	vUnk(v);

		// New stream
		_RELEASE(pStm);
		CCLTRYE	( (IUnknown *)(NULL) != vUnk, E_INVALIDARG );
		CCLTRY	( _QI(vUnk,IID_IByteStream,&pStm) );
		}	// else if
//	else if (prStr == pR)
//		hr = adtValue::copy ( adtString(v), strParse );
	// State
	else if (_RCP(Endian))
		{
		adtString	strEnd(v);
		if (!WCASECMP(strEnd,L"Big"))
			bEndianBig = true;
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

