////////////////////////////////////////////////////////////////////////
//
//									Pass.CPP
//
//					Implementation of the pass-through node
//
////////////////////////////////////////////////////////////////////////

#include "miscl_.h"
#include <stdio.h>

Pass :: Pass ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pRcps = NULL;
	}	// Pass

Pass :: ~Pass ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pRcps);
	}	// Pass

HRESULT Pass :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// Attach
	if (bAttach)
		{
		adtIUnknown	unkV;

		// Create a dictionary to keep track of receptors by object
		CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pRcps));

		// Add receptors/emitter's for each specified key.
		if (pnDesc->load ( adtString(L"Keys"), unkV ) == S_OK)
			{
			IContainer	*pKeys	= NULL;
			IIt			*pIt		= NULL;
			adtValue		v;

			// Iterate keys, add connectors 
			CCLTRY(_QISAFE(unkV,IID_IContainer,&pKeys));
			CCLTRY(pKeys->iterate(&pIt));
			while (hr == S_OK && pIt->read ( v ) == S_OK)
				{
				adtString	strIn,strOut;
				IReceptor	*pR	= NULL;

				// String version of provided key
				CCLTRY ( adtValue::toString ( v, strIn ) );

				// Add a receptor for the specified name
				CCLTRY ( pnSpc->connection ( pnLoc, strIn, L"Receptor", this, &pR ) );

				// Associate receptor with key
				CCLTRY ( pRcps->store ( adtLong((U64)pR), v ) );

				// Create outgoing emitter for result
				CCLTRY ( adtValue::copy ( strIn, strOut ) );
				CCLTRY ( strOut.prepend ( L"On" ) );
				CCLTRY ( pnSpc->connection ( pnLoc, strOut, L"Emitter", this, &pR ) );

				// Clean up
				pIt->next();
				}	// while

			// Clean up
			_RELEASE(pKeys);
			_RELEASE(pIt);
			}	// if

		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pRcps);
		}	// else

	return hr;
	}	// onAttach

HRESULT Pass :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IReceptor	*pR	= NULL;
	adtValue		vRecep;
	adtString	strEmit;
	adtValue		vL;
	adtIUnknown	unkV;

	// State check
	CCLTRYE ( pRcps != NULL, ERROR_INVALID_STATE );

	// Access key name
	CCLTRY ( pRcps->load ( adtLong((U64)pr), vRecep ) );

	// Generate emitter string
	CCLTRY ( adtValue::toString ( vRecep, strEmit ) );
//	lprintf ( LOG_DBG, L"Recep : %s 0x%x {\r\n", (LPCWSTR)strEmit, hr );
	CCLTRY ( strEmit.prepend(L"On") );
	CCLTRY ( pnLoc->load ( strEmit, vL ) );

	// Load the connection itself
	CCLTRY ( _QISAFE((unkV=vL),IID_IReceptor,&pR) );

	// Forward value
//	lprintf ( LOG_DBG, L"Recep : %s %p 0x%x {\r\n", (LPCWSTR)strEmit, pR, hr );
	CCLOK ( pR->receive ( this, prl, v ); )
//	lprintf ( LOG_DBG, L"} Recep : %s %p 0x%x\r\n", (LPCWSTR)strEmit, pR, hr );

	// Clean up
	_RELEASE(pR);

	return hr;
	}	// receive

