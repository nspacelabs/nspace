////////////////////////////////////////////////////////////////////////
//
//									CS.CPP
//
//							Critical section object
//
////////////////////////////////////////////////////////////////////////

#include "sysl.h"

sysCS :: sysCS ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef	_WIN32
	InitializeCriticalSection(&cs);
	#elif defined(__APPLE__) || defined(__unix__)
	// Need to be a recursive mutex
	pthread_mutexattr_t		attr;
	pthread_mutexattr_init ( &attr );
	pthread_mutexattr_settype ( &attr, PTHREAD_MUTEX_RECURSIVE );
	pthread_mutex_init ( &mtx, &attr );
	#endif
	}	// sysCS

sysCS :: ~sysCS ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef	_WIN32
	DeleteCriticalSection(&cs);
	#elif defined(__APPLE__) || defined(__unix__)
	pthread_mutex_destroy ( &mtx );
	#endif
	}	// ~sysCS

bool sysCS :: enter ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Enters critical section.
	//
	//	RETURN VALUE
	//		TRUE if successful
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef	_WIN32
	EnterCriticalSection ( &cs );
	return true;
	#elif		__APPLE__	|| __unix__
	pthread_mutex_lock ( &mtx );
	return true;
	#else
	return true;
	#endif
	}	// enter

bool sysCS :: leave ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Leaves critical section.
	//
	//	RETURN VALUE
	//		TRUE if successful
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef	_WIN32
	LeaveCriticalSection ( &cs );
	return true;
	#elif		__APPLE__	|| __unix__
	pthread_mutex_unlock ( &mtx );
	return true;
	#else
	return true;
	#endif
	}	// leave

