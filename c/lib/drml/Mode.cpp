////////////////////////////////////////////////////////////////////////
//
//									MODE.CPP
//
//				Implementation of the DRM mode node.
//
////////////////////////////////////////////////////////////////////////

#include "drml_.h"
#include <stdio.h>

// Globals

Mode :: Mode ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctDv	= NULL;
	pDctMd	= NULL;
	pDctFb	= NULL;
	iConn		= 0;
	iEnc		= 0;
	}	// Mode

HRESULT Mode :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Connector"), vL ) == S_OK)
			iConn = adtInt(vL);
		if (pnDesc->load ( adtString(L"Encoder"), vL ) == S_OK)
			iEnc = adtInt(vL);
		if (pnDesc->load ( adtString(L"Mode"), vL ) == S_OK)
			onReceive(prMode,vL);
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDctFb);
		_RELEASE(pDctMd);
		_RELEASE(pDctDv);
		}	// else

	return hr;
	}	// onAttach

HRESULT Mode :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Set mode
	if (_RCP(Set))
		{
		drmModeRes 			*pRes 		= NULL;
		drmModeConnector	*pCon			= NULL;
		drmModeEncoder		*pEnc			= NULL;
		drmModePlane		*pPln 		= NULL;
		struct kms_driver	*kms			= NULL;
		struct kms_bo		*bo			= NULL;
		uint32_t				fb_id			= -1;
		int					midx			= -1;
		int					iDev			= -1;
		uint32_t				handles[4]	= { 0, 0, 0, 0 };
		uint32_t				pitches[4]	= { 0, 0, 0, 0 };
		uint32_t				offsets[4]	= { 0, 0, 0, 0 };
		drmModeModeInfo	mode;
		int					c,e,m,ret;
		adtValue				vL;
		adtInt				iW,iH;

		// State check
		CCLTRYE( pDctDv != NULL && pDctMd != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pDctDv->load ( adtString(L"Drm"), vL ) );
		CCLOK  ( iDev = adtInt(vL); )
		CCLTRY ( pDctDv->load ( adtString(L"Kms"), vL ) );
		CCLTRYE( (kms = (struct kms_driver *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Obtain resources
		CCLTRYE ( (pRes = drmModeGetResources(iDev)) != NULL, errno );

		//
		// Connector
		//

		// Find specified connector information
//		lprintf ( LOG_DBG, L"Connector count %d\r\n", pRes->count_connectors );
		for (c = 0;hr == S_OK && c < pRes->count_connectors;++c)
			{
			// Retrieve connector info
			if ((pCon = drmModeGetConnector ( iDev, pRes->connectors[c] )) == NULL)
				{
				lprintf ( LOG_DBG, L"Could not get connector %d: %S\r\n",
											pRes->connectors[c], strerror(errno) );
				continue;
				}	// if

			// Connector match ?
//			lprintf ( LOG_DBG, L"Modes %d Id %d\r\n", pCon->count_modes, pCon->connector_id );
			if (pCon->connector_id == iConn)
				{
//				lprintf ( LOG_DBG, L"Found matching connector resource %d\r\n", pCon->connector_id );
				break;
				}	// if

			// Clean up
			if (pCon != NULL)
				{
				drmModeFreeConnector(pCon);
				pCon = NULL;
				}	// if
			}	// for

		// Found connector ?
		CCLTRYE ( pCon != NULL, ERROR_NOT_FOUND );

		//
		// Encoder
		//

		// Get the encoder
//		lprintf ( LOG_DBG, L"Encoder count %d\r\n", pRes->count_encoders );
		for (e = 0;hr == S_OK && e < pRes->count_encoders;++e)
			{
			// Retrieve encoder info
			if ((pEnc = drmModeGetEncoder ( iDev, pRes->encoders[e] )) == NULL)
				{
				lprintf ( LOG_DBG, L"Could not get encoder %d: %S\r\n",
											pRes->encoders[c], strerror(errno) );
				continue;
				}	// if

			// Connector match ?
//			lprintf ( LOG_DBG, L"Id %d Crtc_Id %d\r\n", pEnc->encoder_id, pEnc->crtc_id );
			if (pEnc->encoder_id == iEnc)
				{
//				lprintf ( LOG_DBG, L"Found matching encoder resource %d\r\n", pEnc->encoder_id );
				break;
				}	// if

			// Clean up
			if (pEnc != NULL)
				{
				drmModeFreeEncoder(pEnc);
				pEnc = NULL;
				}	// if
			}	// for

		// Found encoder ?
		CCLTRYE ( pEnc != NULL, ERROR_NOT_FOUND );

		//
		// Mode
		//

		// Possible mode matching parameters
		if (hr == S_OK && pDctMd->load ( adtString("Width"), vL ) == S_OK)
			iW = vL;
		if (hr == S_OK && pDctMd->load ( adtString("Height"), vL ) == S_OK)
			iH = vL;

		// Search mode
//		lprintf ( LOG_DBG, L"Searching for %dX%d\r\n", (U32)iW, (U32)iH );
		for (m = 0;hr == S_OK && m < pCon->count_modes && midx == -1;++m)
			{
			// Match ?
//			lprintf ( LOG_DBG, L"%d) Name %S %dX%d@%d flags 0x%x type %d\r\n", m, pCon->modes[m].name,
//							pCon->modes[m].hdisplay, pCon->modes[m].vdisplay,
//							pCon->modes[m].vrefresh, pCon->modes[m].flags, pCon->modes[m].type );
			if (	(iW == 0 || iW == pCon->modes[m].hdisplay) &&
					(iH == 0 || iH == pCon->modes[m].vdisplay) )
				{
//				lprintf ( LOG_DBG, L"Matching mode found %d\r\n", m );
				mode = pCon->modes[m];
				midx = m;
				}	// if
			}	// for

		// Matching mode found ?
		if (hr == S_OK)
			{
			CCLTRYE ( midx != -1, ERROR_NOT_FOUND );
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Unable to find a matching mode for %dx%d\r\n", (U32) iW, (U32) iH );
			}	// if

		//
		// Frame buffer
		//

		// Is a frame buffer specified for the new mode ?
		if (hr == S_OK && pDctFb != NULL)
			{
			// Retrieve Id for frame buffer
			CCLTRY ( pDctFb->load ( adtString(L"Id"), vL ) );
			CCLOK  ( fb_id = adtInt(vL); )
//			lprintf ( LOG_DBG, L"Frame buffer Id %d\r\n", fb_id );
			}	// if

		// Additional mode information
		if (hr == S_OK)
			{
			// Adjust mode based on addtional mode dictionary
			if (pDctMd->load ( adtString(L"clock"), vL ) == S_OK)
				mode.clock = adtInt(vL);
			if (pDctMd->load ( adtString(L"htotal"), vL ) == S_OK)
				mode.htotal = adtInt(vL);
			if (pDctMd->load ( adtString(L"hsync_start"), vL ) == S_OK)
				mode.hsync_start = adtInt(vL);
			if (pDctMd->load ( adtString(L"hsync_end"), vL ) == S_OK)
				mode.hsync_end = adtInt(vL);
			if (pDctMd->load ( adtString(L"vtotal"), vL ) == S_OK)
				mode.vtotal = adtInt(vL);
			if (pDctMd->load ( adtString(L"vsync_start"), vL ) == S_OK)
				mode.vsync_start = adtInt(vL);
			if (pDctMd->load ( adtString(L"vsync_end"), vL ) == S_OK)
				mode.vsync_end = adtInt(vL);
			}	// if

		// Set the mode
		if (hr == S_OK)
			{
//			lprintf ( LOG_DBG, L"Set Mode %dX%d@%d iDev %d crtc_id %d connector_id %d\r\n", 
//							mode.hdisplay, mode.vdisplay,
//							mode.vrefresh, iDev, pEnc->crtc_id, pCon->connector_id );
//			lprintf ( LOG_DBG, L"Mode clock %d vrefresh %d flags 0x%x type %d name %S\r\n",
//							mode.clock, mode.vrefresh, mode.flags, mode.type, mode.name );
//			lprintf ( LOG_DBG, L"Mode hdisp %d hstart %d hend %d htotal %d\r\n",
//							mode.hdisplay, mode.hsync_start, mode.hsync_end, mode.htotal );
//			lprintf ( LOG_DBG, L"Mode vdisp %d vstart %d vend %d vtotal %d\r\n",
//							mode.vdisplay, mode.vsync_start, mode.vsync_end, mode.vtotal );
			ret = drmModeSetCrtc ( iDev, pEnc->crtc_id, fb_id, 0, 0, &(pCon->connector_id), 1, &mode );
//			lprintf ( LOG_DBG, L"drmModeSetCrtc ret %d %S\r\n", ret, (ret == 0) ? "None" : strerror(errno) );
			}	// if

		// Associate plane, frame buffer and crtc
//		if (hr == S_OK)
//			{
//			lprintf ( LOG_DBG, L"drmModeSetPlane planes[0] %d crtc %d fb_id %d\r\n", 
//							pPlnRes->planes[0], pEnc->crtc_id, fb_id );
//			ret = drmModeSetPlane ( iDev, pPlnRes->planes[0], pEnc->crtc_id, fb_id, 0,
//											0, 0, mode.hdisplay, mode.vdisplay,
//											0, 0, mode.hdisplay << 16, mode.vdisplay << 16 );
//			lprintf ( LOG_DBG, L"drmModeSetPlane ret %d %S\r\n", ret, (ret == 0) ? "None" : strerror(errno) );
//			}	// if
/*
		// DEBUG
		if (hr == S_OK && pDctFb->load ( adtString(L"BufferObject"), vL ) == S_OK)
			{
			struct kms_bo 	*bo = (struct kms_bo *)(U64)adtLong(vL);
			void				*plane = NULL;
			kms_bo_map ( bo, &plane );
			lprintf ( LOG_DBG, L"BO Debug %p : plane %p\r\n", bo, plane );
			kms_bo_unmap(bo);
			}	// if
*/
		// Result
		if (hr == S_OK)
			_EMT(Set,adtIUnknown(pDctMd));
		else
			{
			lprintf ( LOG_DBG, L"Error setting mode : hr 0x%d\r\n", hr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		if (pEnc != NULL)
			drmModeFreeEncoder(pEnc);
		if (pCon != NULL)
			drmModeFreeConnector(pCon);
		if (pRes != NULL)
			drmModeFreeResources(pRes);
		}	// else if

	// State
	else if (_RCP(Device))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctDv);
		_QISAFE(unkV,IID_IDictionary,&pDctDv);
		}	// else if
	else if (_RCP(Mode))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctMd);
		_QISAFE(unkV,IID_IDictionary,&pDctMd);
		}	// else if
	else if (_RCP(Buffer))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctFb);
		_QISAFE(unkV,IID_IDictionary,&pDctFb);
		}	// else if
	else if (_RCP(Connector))
		iConn = adtInt(v);
	else if (_RCP(Encoder))
		iEnc = adtInt(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

/*
		drmModePlaneRes	*pPlnRes = NULL;
		CCLTRYE ( (pPlnRes = drmModeGetPlaneResources(iDev)) != NULL, errno );
		// Get the planes
		lprintf ( LOG_DBG, L"Plane count %d\r\n", pPlnRes->count_planes );
		for (p = 0;hr == S_OK && p < pPlnRes->count_planes;++p)
			{
			// Retrieve info
			if ((pPln = drmModeGetPlane ( iDev, pPlnRes->planes[p] )) == NULL)
				{
				lprintf ( LOG_DBG, L"Could not get plane %d: %S\r\n",
											pPlnRes->planes[c], strerror(errno) );
				continue;
				}	// if

			// Plane match ?
			lprintf ( LOG_DBG, L"Id %d/%d Crtc_Id %d fb_id %d\r\n", 
							pPlnRes->planes[p], pPln->plane_id, pPln->crtc_id, pPln->fb_id );

			// Clean up
			if (pPln != NULL)
				{
				drmModeFreePlane(pPln);
				pPln = NULL;
				}	// if
			}	// for

		// Found encoder ?
//		CCLTRYE ( pEnc != NULL, ERROR_NOT_FOUND );
		if (pPlnRes != NULL)
			drmModeFreePlaneResources(pPlnRes);

		// Set mode for specified, pre-existing plane.  It turns out if you are trying to display a plane that differs
		// in resolution from the mode that is set next, the mode will revert to whatever the plane needs.
		// NOTE: As generalality of this code increases, this will need to be moved to a more general plane
		// mode setting section.
		if (hr == S_OK && iPlane != 0)
			{
			// Set the plane to match the resolution
//			CCLTRYE ( (ret = drmModeSetPlane ( iDev, iPlane, pEnc->crtc_id, -1, 0, 
//							0, 0, pCon->modes[midx].hdisplay, pCon->modes[midx].vdisplay,
//							0, 0, pCon->modes[midx].hdisplay, pCon->modes[midx].vdisplay )) == 0, ret );
			ret = drmModeSetPlane ( iDev, iPlane, pEnc->crtc_id, 0, 0, 
							0, 0, pCon->modes[midx].hdisplay, pCon->modes[midx].vdisplay,
							0, 0, pCon->modes[midx].hdisplay << 16, pCon->modes[midx].vdisplay << 16 );
			lprintf ( LOG_DBG, L"SetPlane iDev %d iPlane %d crtc_id %d %dx%d ret %d:%S\r\n", 
							iDev, iPlane, pEnc->crtc_id, pCon->modes[midx].hdisplay, pCon->modes[midx].vdisplay,
							ret, strerror(errno) );
			}	// if

*/

