////////////////////////////////////////////////////////////////////////
//
//									Property.CPP
//
//				Implementation of the DRM property node.
//
////////////////////////////////////////////////////////////////////////

#include "drml_.h"
#include <stdio.h>

// Globals

Property :: Property ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctDv	= NULL;
	iObj		= 0;
	}	// Property

HRESULT Property :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		pnDesc->load ( adtString(L"Key"), vPrpId );
		pnDesc->load ( adtString(L"Value"), vPrpVal );
		if (pnDesc->load ( adtString(L"Object"), vL ) == S_OK)
			iObj = adtInt(vL);
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDctDv);
		}	// else

	return hr;
	}	// onAttach

HRESULT Property :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Store property
	if (_RCP(Store))
		{
		int		ret,iDev			= -1;
		adtInt	iPrpId(vPrpId),iPrpVal(vPrpVal);
		adtValue	vL;

		// State check
		CCLTRYE( pDctDv != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pDctDv->load ( adtString(L"Drm"), vL ) );
		CCLOK  ( iDev = adtInt(vL); )
		CCLTRYE( !adtValue::empty(vPrpId) && !adtValue::empty(vPrpVal), ERROR_INVALID_STATE );
		CCLTRYE( iObj != 0, ERROR_INVALID_STATE );

		// TODO: Search for Id from name

		// Set property
//		lprintf ( LOG_DBG, L"Store property : hr 0x%d iDev %d Object %d %d=%d\r\n", 
//							hr, iDev, iObj, (U32)iPrpId, (U32)iPrpVal );
		CCLTRYE ( (ret = drmModeObjectSetProperty ( iDev, iObj, DRM_MODE_OBJECT_ANY, iPrpId, 
							adtLong(iPrpVal) )) == 0, ret );
//		CCLTRYE ( (ret = drmModeConnectorSetProperty ( iDev, iConn, iPrpId, 
//						adtLong(iPrpVal) )) == 0, ret );

		// Result
		if (hr == S_OK)
			_EMT(Store,adtIUnknown(pDctDv));
		else if (iDev != -1)
			{
			lprintf ( LOG_DBG, L"Error setting property : 0x%x %d,%d,%d=%d\r\n", 
							hr, iDev, iObj, (U32)iPrpId, (U32)iPrpVal );
			_EMT(Error,adtInt(hr));
			}	// else

		}	// else if

	// State
	else if (_RCP(Device))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctDv);
		_QISAFE(unkV,IID_IDictionary,&pDctDv);
		}	// else if
	else if (_RCP(Object))
		iObj = adtInt(v);
	else if (_RCP(Key))
		hr = adtValue::copy(v,vPrpId);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive


