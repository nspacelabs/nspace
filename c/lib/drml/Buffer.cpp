////////////////////////////////////////////////////////////////////////
//
//									Buffer.CPP
//
//				Implementation of the DRM frame buffer node.
//
////////////////////////////////////////////////////////////////////////

#include "drml_.h"
#include <stdio.h>

// Globals

Buffer :: Buffer ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctDv	= NULL;
	pDctFb	= NULL;
	}	// Mode

HRESULT Buffer :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			onReceive(prWidth,vL);
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			onReceive(prHeight,vL);
		}	// if

	// Detach
	else
		{
		// Shutdown
		onReceive(prClose,adtInt());
		_RELEASE(pDctFb);
		_RELEASE(pDctDv);
		}	// else

	return hr;
	}	// onAttach

HRESULT Buffer :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		uint32_t				handles[4]	= { 0, 0, 0, 0 };
		uint32_t				pitches[4]	= { 0, 0, 0, 0 };
		uint32_t				offsets[4]	= { 0, 0, 0, 0 };
		struct kms_driver	*kms			= NULL;
		struct kms_bo		*bo			= NULL;
		uint32_t				fb_id			= -1;
		int					ret,iDev		= -1;
		adtValue				vL;

		// State check
		CCLTRYE( pDctDv != NULL && pDctFb != NULL, ERROR_INVALID_STATE );
		CCLTRYE( pDctFb->load ( adtString(L"FrameBuffer"), vL ) != S_OK, ERROR_INVALID_STATE );
		CCLTRYE( iW > 0 && iH > 0, ERROR_INVALID_STATE );
		CCLTRY ( pDctDv->load ( adtString(L"Drm"), vL ) );
		CCLOK  ( iDev = adtInt(vL); )
		CCLTRY ( pDctDv->load ( adtString(L"Kms"), vL ) );
		CCLTRYE( (kms = (struct kms_driver *)(U64)adtLong(vL)) != NULL, ERROR_INVALID_STATE );

		// Create a new kernel memory block buffer buffer with the desired attributes
		if (hr == S_OK)
			{
			unsigned bo_attribs[] = { 
					KMS_WIDTH,		iW,
					KMS_HEIGHT, 	iH,
					KMS_BO_TYPE,	KMS_BO_TYPE_SCANOUT_X8R8G8B8,
					KMS_TERMINATE_PROP_LIST };

			// Create the buffer
			CCLTRYE ( (ret = kms_bo_create ( kms, bo_attribs, &bo )) == 0, ret );
//			lprintf ( LOG_DBG, L"kms_bo_create ret %d bo %p\r\n", ret, bo );
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"kms_bo_create failed %d\r\n", ret );

			// Will need to be freed with the context
//			CCLTRY ( pDctFb->store ( adtString(L"BufferObject"), adtIUnknown(new drmRef(bo))) );
			CCLTRY ( pDctFb->store ( adtString(L"BufferObject"), adtLong((U64)bo) ) );
			}	// if

		// Attributes for the buffer
		if (hr == S_OK)
			{
			// Retrieve needed properties
			CCLTRYE ( (ret = kms_bo_get_prop ( bo, KMS_PITCH, &pitches[0] )) == 0, ret );
//			lprintf ( LOG_DBG, L"Buffer object : ret %d/%S Pitch %d,%d,%d,%d\r\n", ret, strerror(ret),
//							pitches[0], pitches[1], pitches[2], pitches[3] );
			CCLTRYE ( (ret = kms_bo_get_prop ( bo, KMS_HANDLE, &handles[0] )) == 0, ret );
//			lprintf ( LOG_DBG, L"Buffer object : ret %d/%S Handle %d,%d,%d,%d\r\n", ret, strerror(ret), 
//							handles[0], handles[1], handles[2], handles[3] );
			offsets[0] = offsets[1] = offsets[2] = offsets[3] = 0;
			}	// if

		// DEBUG: Initialize buffer with pattern
		if (hr == S_OK)
			{
			void *plane = NULL;
			kms_bo_map ( bo, &plane );
//			lprintf ( LOG_DBG, L"kms_bo_map bo %p plane %p pitches[0] %d %dX%d\r\n", bo, plane, pitches[0], (S32)iW, (S32)iH );
			if (plane != NULL)
				{
/*
				unsigned char *pc = (unsigned char *)plane;
				memset ( plane, 0x00, iH*pitches[0] );
				for (int r = 0;r < iH/2;++r)
					{
					*((U32 *)(pc+0)) = 0x00ff0000;
					*((U32 *)(pc+3)) = 0x00ff0000;
					*((U32 *)(pc+9)) = 0x00ff0000;
					*((U32 *)(pc+12)) = 0x00ff0000;
					pc += pitches[0];
//					for (int c = 0;c < mode.hdisplay;++c)
//					for (int c = 0;c < iW;++c)
//					for (int c = 0;c < pitches[0];c+=4)
//						{
//						*((U32 *)pc) = 0x000000ff;
//						pc += 4;
//						*pc++ = c;
//						}	// for

					}	// for
*/

				// Initialize plane to default
				memset ( plane, 0x00, iH*pitches[0] );
				kms_bo_unmap(bo);
				}	// if
				
			}	// if

		// Add the frame buffer to the device context
		if (hr == S_OK)
			{
			// Create buffer
			CCLTRYE ( (ret = drmModeAddFB2 ( iDev, iW, iH,
							DRM_FORMAT_RGB888, handles, pitches, offsets, &fb_id, 0 )) == 0, ret );
//			lprintf ( LOG_DBG, L"drmModeAddFB2 : ret %d %S fb_id %d\r\n", ret, strerror(ret), fb_id );

			// Will need to be freed with the context
//			CCLTRY ( pDctFb->store ( adtString(L"Id"), adtIUnknown(new drmRef(iDev,fb_id))) );
			CCLTRY ( pDctFb->store ( adtString(L"Id"), adtInt(fb_id) ) );
			}	// if

		// Result
		CCLTRY ( pDctFb->store ( adtString(L"Width"), iW ) );
		CCLTRY ( pDctFb->store ( adtString(L"Height"), iH ) );
		CCLTRY ( pDctFb->store ( adtString(L"Format"), adtString(L"B8G8R8") ) );
		CCLTRY ( pDctFb->store ( adtString(L"Step"), adtInt(pitches[0]) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDctFb));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		if (hr != S_OK)
			{
			if (bo != NULL)
				kms_bo_destroy(&bo);
			}	// if

		}	// if

	// Close
	else if (_RCP(Close))
		{
		int		iDev		= -1;
		adtValue	vL;

		// State check
		CCLTRYE ( pDctDv != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pDctFb != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pDctDv->load ( adtString(L"Drm"), vL ) );
		CCLOK  ( iDev = adtInt(vL); )

		// Closing
		CCLOK ( _EMT(Close,adtIUnknown(pDctFb)); )

		// Ensure not mapped
		CCLOK ( onReceive ( prUnmap, v ); )

		// Clean up
		if (hr == S_OK && pDctFb->load ( adtString(L"Id"), vL ) == S_OK)
			{
//			lprintf ( LOG_DBG, L"Id %d\r\n", (U32)adtInt(vL) );
			drmModeRmFB(iDev,adtInt(vL));
			pDctFb->remove ( adtString(L"Id") );
			}	// if
		if (hr == S_OK && pDctFb->load ( adtString(L"BufferObject"), vL ) == S_OK)
			{
			struct kms_bo 	*bo = (struct kms_bo *)(U64)adtLong(vL);
			kms_bo_unmap ( bo );
//			lprintf ( LOG_DBG, L"bo %p\r\n", bo );
			kms_bo_destroy(&bo);
			pDctFb->remove ( adtString(L"BufferObject") );
			}	// if
		}	// else if

	// Map buffer to address
	else if (_RCP(Map))
		{
		struct kms_bo 	*bo		= NULL;
		IMemoryMapped	*pBits	= NULL;
		void				*pvBits	= NULL;
		adtValue			vL;
		adtInt			iH,iS;
		int				ret;

		// State check
		CCLTRYE ( (pDctFb != NULL), ERROR_INVALID_STATE );

		// Extract buffer object
		CCLTRY ( pDctFb->load ( adtString(L"BufferObject"), vL ) );
		CCLOK  ( bo = (struct kms_bo *)(U64)adtLong(vL); )
		CCLTRYE( (bo != NULL), ERROR_INVALID_STATE );

		// Map to local ptr.
		CCLTRYE ( (ret = kms_bo_map ( bo, &pvBits )) >= 0, ret );

		// Create a static memory block for the pointer
		CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pBits ) );

		// Load buffer info for calculation
		CCLTRY ( pDctFb->load ( adtString(L"Height"), vL ) );
		CCLTRYE( (iH = vL) > 0, ERROR_INVALID_STATE );
		CCLTRY ( pDctFb->load ( adtString(L"Step"), vL ) );
		CCLTRYE( (iS = vL) > 0, ERROR_INVALID_STATE );

		// Initialize static memory information
		CCLTRY ( pBits->setInfo ( pvBits, iH*iS ) );

		// Store as the 'bits'
		CCLTRY ( pDctFb->store ( adtString(L"Bits"), adtIUnknown(pBits) ) );

		// Result
		if (hr == S_OK)
			_EMT(Map,adtIUnknown(pDctFb));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pBits);
		}	// else if

	// Unmap buffer
	else if (_RCP(Unmap))
		{
		struct kms_bo 	*bo		= NULL;
		adtValue			vL;

		// State check
		CCLTRYE ( (pDctFb != NULL), ERROR_INVALID_STATE );

		// About to unmap
		CCLOK ( _EMT(Unmap,adtIUnknown(pDctFb)); )

		// Extract buffer object
		CCLTRY ( pDctFb->load ( adtString(L"BufferObject"), vL ) );
		CCLOK  ( bo = (struct kms_bo *)(U64)adtLong(vL); )
		CCLTRYE( (bo != NULL), ERROR_INVALID_STATE );

		// Ensure unmapped
		CCLOK ( kms_bo_unmap(bo); )

		// Clean up
		if (pDctFb != NULL)
			pDctFb->remove ( adtString(L"Bits") );
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctFb);
		_QISAFE(unkV,IID_IDictionary,&pDctFb);
		}	// else if
	else if (_RCP(Device))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctDv);
		_QISAFE(unkV,IID_IDictionary,&pDctDv);
		}	// else if
	else if (_RCP(Width))
		iW = v;
	else if (_RCP(Height))
		iH = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive


