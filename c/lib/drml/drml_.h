////////////////////////////////////////////////////////////////////////
//
//										DRML_.H
//
//				Implementation include file for DRM library
//
////////////////////////////////////////////////////////////////////////

#ifndef	DRML__H
#define	DRML__H

// Includes
#include "drml.h"
#include "../../lib/nspcl/nspcl.h"

// External API
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <libkms/libkms.h>
#include <drm/drm_fourcc.h>

///////////
// Objects
///////////

//
// Class - drmRef.  Reference counted objects for drm objects.
//

class drmRef :
	public CCLObject										// Base class
	{
	public :
	drmRef ( struct kms_bo *_bo )						// Constructor
		{ bo = _bo; AddRef(); fb_id = -1;
lprintf ( LOG_DBG, L"bo %p\r\n", bo ); }
	drmRef ( int _iDev, uint32_t _fb_id )
		{ iDev = _iDev; fb_id = _fb_id; bo = NULL;
lprintf ( LOG_DBG, L"fb_id %d\r\n", fb_id ); }
drmRef ( void )
{
lprintf ( LOG_DBG, L"Empty constructor!\r\n" );
}
	virtual ~drmRef ( void )							// Destructor
		{ 	lprintf ( LOG_DBG, L"%p %d %d\r\n", bo, iDev, fb_id );
			if (bo != NULL) kms_bo_destroy(&bo);
			if (fb_id != -1) drmModeRmFB(iDev,fb_id); }

	// Run-time data
	struct kms_bo 	*bo;									// Buffer object
	uint32_t			fb_id;								// Frame buffer Id
	int 				iDev;									// Device file descriptor

	// CCL
	CCL_OBJECT_BEGIN_INT(drmRef)
	CCL_OBJECT_END()
	};

/////////
// Nodes
/////////

//
// Class - Buffer.  DRM frame buffers.
//


class Buffer :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Buffer ( void );										// Constructor

	// Run-time data
	IDictionary	*pDctDv;									// Device dictionary
	IDictionary	*pDctFb;									// Frame buffer dictionary
	adtInt		iW,iH;									// Dimensions

	// CCL
	CCL_OBJECT_BEGIN(Buffer)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Map)
	DECLARE_CON(Unmap)
	DECLARE_CON(Open)
	DECLARE_CON(Close)
	DECLARE_EMT(Error)
	DECLARE_RCP(Width)
	DECLARE_RCP(Height)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Map)
		DEFINE_CON(Unmap)
		DEFINE_CON(Open)
		DEFINE_CON(Close)
		DEFINE_EMT(Error)
		DEFINE_RCP(Width)
		DEFINE_RCP(Height)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Device.  DRM device node.
//

class Device :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Device ( void );										// Constructor

	// Run-time data
	IDictionary	*pDctDv;									// Device dictionary
	adtString	strMod;									// Module name

	// CCL
	CCL_OBJECT_BEGIN(Device)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)
	DECLARE_CON(Close)
	DECLARE_CON(Open)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)
		DEFINE_CON(Close)
		DEFINE_CON(Open)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	HRESULT fill ( IDictionary *, int );
	};

//
// Class - Mode.  DRM mode manipulation.
//


class Mode :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Mode ( void );											// Constructor

	// Run-time data
	IDictionary	*pDctDv;									// Device dictionary
	IDictionary *pDctMd;									// Mode dictionary
	IDictionary	*pDctFb;									// Frame buffer dictionary

	// Linux
	uint32_t		iConn;									// Connector Id
	uint32_t		iEnc;										// Encoder Id

	// CCL
	CCL_OBJECT_BEGIN(Mode)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)
	DECLARE_RCP(Connector)
	DECLARE_RCP(Encoder)
	DECLARE_RCP(Buffer)
	DECLARE_EMT(Error)
	DECLARE_CON(Set)
	DECLARE_RCP(Mode)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)
		DEFINE_RCP(Connector)
		DEFINE_RCP(Encoder)
		DEFINE_RCP(Buffer)
		DEFINE_EMT(Error)
		DEFINE_CON(Set)
		DEFINE_RCP(Mode)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

//
// Class - Property.  DRM property manipulation.
//


class Property :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Property ( void );									// Constructor

	// Run-time data
	IDictionary	*pDctDv;									// Device dictionary
	uint32_t		iObj;										// Object id
	adtValue		vPrpId,vPrpVal;						// Property Id and value

	// CCL
	CCL_OBJECT_BEGIN(Property)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Device)
	DECLARE_RCP(Key)
	DECLARE_RCP(Object)
	DECLARE_EMT(Error)
	DECLARE_CON(Store)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Device)
		DEFINE_RCP(Key)
		DEFINE_RCP(Object)
		DEFINE_EMT(Error)
		DEFINE_CON(Store)
	END_BEHAVIOUR_NOTIFY()

	private :

	};

#endif

