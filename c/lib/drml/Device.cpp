////////////////////////////////////////////////////////////////////////
//
//									DEVICE.CPP
//
//				Implementation of the DRM device node.
//
////////////////////////////////////////////////////////////////////////

#include "drml_.h"
#include <stdio.h>

// Globals

Device :: Device ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctDv = NULL;
	}	// Device

HRESULT Device :: fill ( IDictionary *pDct, int fd )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Fill in the resources for the open device.
	//
	//	PARAMETERS
	//		-	pDct is the root dictionary
	//		-	fd is the file descriptor of the device
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr 		= S_OK;
	IDictionary			*pDctRes	= NULL;
	drmModeRes 			*pRes 	= NULL;
	drmModeConnector	*pCon		= NULL;
	drmModeEncoder		*pEnc		= NULL;
	drmModePlane		*pPln 	= NULL;
	drmModePlaneRes	*pPrs		= NULL;
	drmModeCrtc			*pCrt 	= NULL;
	int					i,j;
	adtString			strV;

	// Most logic based on 'modetest' source code

	// Expose all planes
	CCLTRYE ( drmSetClientCap ( fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1 ) == 0, S_FALSE );

	// Obtain resources
	CCLTRYE ( (pRes = drmModeGetResources(fd)) != NULL, errno );
//	lprintf ( LOG_DBG, L"pRes %p fd %d\r\n", pRes, fd );
//	if (pRes != NULL)
//		lprintf ( LOG_DBG, L"counts %d,%d,%d,%d w,h %d,%d,%d,%d\r\n",
//					pRes->count_fbs, pRes->count_crtcs, pRes->count_connectors,
//					pRes->count_encoders, pRes->min_width, pRes->min_height,
//					pRes->max_width, pRes->max_height );

	// Have not encountered frame buffers yet, TODO:
	if (hr == S_OK && pRes->count_fbs > 0)
		lprintf ( LOG_DBG, L"Frame buffers encountered, TBI : %d\r\n", pRes->count_fbs );

	// Encoders
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctRes ) );
	CCLTRY ( pDct->store ( adtString(L"Encoder"), adtIUnknown(pDctRes) ) );
	for (i = 0;hr == S_OK && i < pRes->count_encoders;++i)
		{
		IDictionary		*pDctInf	= NULL;
		drmModeEncoder *pEnc		= NULL;

		// Retrieve object
//		lprintf ( LOG_DBG, L"encoders[%d] = %d\r\n", i, pRes->encoders[i] );
		if ( (pEnc = drmModeGetEncoder ( fd, pRes->encoders[i] )) == NULL )
			continue;
//		lprintf ( LOG_DBG, L"pEnc %p id %d\r\n", pEnc, pEnc->encoder_id );

		// Fill info
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctInf ) );
		CCLTRY ( pDctRes->store ( adtInt(i+1), adtIUnknown(pDctInf) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Id"), adtInt(pEnc->encoder_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"IdCrtc"), adtInt(pEnc->crtc_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Crtcs"), adtInt(pEnc->possible_crtcs) ) );
		_RELEASE(pDctInf);
		drmModeFreeEncoder(pEnc);
		}	// for
	_RELEASE(pDctRes);

	// CRTCs
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctRes ) );
	CCLTRY ( pDct->store ( adtString(L"Crtc"), adtIUnknown(pDctRes) ) );
	for (i = 0;hr == S_OK && i < pRes->count_crtcs;++i)
		{
		IDictionary					*pDctInf		= NULL;
		IDictionary					*pDctPrps	= NULL;
		drmModeCrtc 				*pCrtc		= NULL;
		drmModeObjectProperties	*pPrps		= NULL;

		// Retrieve object
//		lprintf ( LOG_DBG, L"crtc[%d] = %d\r\n", i, pRes->crtcs[i] );
		if ( (pCrtc = drmModeGetCrtc ( fd, pRes->crtcs[i] )) == NULL )
			continue;
//		lprintf ( LOG_DBG, L"pCrtc %p, id %d\r\n", pCrtc, pCrtc->crtc_id );

		// Fill info
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctInf ) );
		CCLTRY ( pDctRes->store ( adtInt(i+1), adtIUnknown(pDctInf) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Id"), adtInt(pCrtc->crtc_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"IdFb"), adtInt(pCrtc->buffer_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"X"), adtInt(pCrtc->x) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Y"), adtInt(pCrtc->y) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Width"), adtInt(pCrtc->width) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Height"), adtInt(pCrtc->height) ) );

		// Properties
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctPrps ) );
		CCLTRY ( pDctInf->store ( adtString(L"Properties"), adtIUnknown(pDctPrps) ) );
		if (hr == S_OK && (pPrps = drmModeObjectGetProperties ( fd, 
				pCrtc->crtc_id, DRM_MODE_OBJECT_CRTC )) != NULL )
			{
			// Retrieve and store properties under name
			for (j = 0;hr == S_OK && j < pPrps->count_props;++j)
				{
				IDictionary				*pDctPrp	= NULL;
				drmModePropertyRes	*pPrp		= NULL;

				// Grab property information
				if ( (pPrp = drmModeGetProperty ( fd, pPrps->props[j] )) == NULL )
					continue;
//				lprintf ( LOG_DBG, L"Property %d : %S : vals %d : enums %d\r\n",
//								pPrp->prop_id, pPrp->name, pPrp->count_values, pPrp->count_enums );

				// Fill info
				CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctPrp ) );
				CCLTRY ( pDctPrps->store ( adtString(strV = pPrp->name), adtIUnknown(pDctPrp) ) );
				CCLTRY ( pDctPrp->store ( adtString(L"Id"), adtInt(pPrp->prop_id) ) );

				// Clean up
				_RELEASE(pDctInf);
				drmModeFreeProperty(pPrp);
				}	// for
			}	// if

		// Clean up
//		lprintf ( LOG_DBG, L"pPrps %p, hr 0x%x\r\n", pPrps, hr );
		_RELEASE(pDctPrps);
		if (pPrps != NULL)
			drmModeFreeObjectProperties(pPrps);
		_RELEASE(pDctInf);
		drmModeFreeCrtc(pCrtc);
		}	// for
	_RELEASE(pDctRes);

	// Connectors
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctRes ) );
	CCLTRY ( pDct->store ( adtString(L"Connector"), adtIUnknown(pDctRes) ) );
	for (i = 0;hr == S_OK && i < pRes->count_connectors;++i)
		{
		IDictionary			*pDctInf		= NULL;
		IDictionary			*pDctPrps	= NULL;
		drmModeConnector 	*pCon			= NULL;

		// Retrieve object
//		lprintf ( LOG_DBG, L"connectors[%d] = %d\r\n", i, pRes->connectors[i] );
		if ( (pCon = drmModeGetConnector ( fd, pRes->connectors[i] )) == NULL )
			continue;
//		lprintf ( LOG_DBG, L"pCon %p, id %d\r\n", pCon, pCon->connector_id );

		// Fill info
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctInf ) );
		CCLTRY ( pDctRes->store ( adtInt(i+1), adtIUnknown(pDctInf) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Id"), adtInt(pCon->connector_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"IdEnc"), adtInt(pCon->encoder_id) ) );

		// Retrieve and store properties under name
//		lprintf ( LOG_DBG, L"Property count %d\r\n", pCon->count_props );
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctPrps ) );
		CCLTRY ( pDctInf->store ( adtString(L"Properties"), adtIUnknown(pDctPrps) ) );
		for (j = 0;hr == S_OK && j < pCon->count_props;++j)
			{
			IDictionary				*pDctPrp	= NULL;
			drmModePropertyRes	*pPrp		= NULL;

			// Grab property information
			if ( (pPrp = drmModeGetProperty ( fd, pCon->props[j] )) == NULL )
				continue;
//			lprintf ( LOG_DBG, L"Property %d : %S : vals %d : enums %d\r\n",
//							pPrp->prop_id, pPrp->name, pPrp->count_values, pPrp->count_enums );

			// Fill info
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctPrp ) );
			CCLTRY ( pDctPrps->store ( adtString(strV = pPrp->name), adtIUnknown(pDctPrp) ) );
			CCLTRY ( pDctPrp->store ( adtString(L"Id"), adtInt(pPrp->prop_id) ) );

			// Clean up
			_RELEASE(pDctInf);
			drmModeFreeProperty(pPrp);
			}	// for

		// Clean up
		_RELEASE(pDctPrps);
		_RELEASE(pDctInf);
		drmModeFreeConnector(pCon);
		}	// for
	_RELEASE(pDctRes);

	// Plane information, apparently not part of the resources structure even
	// though it show it in source for 'modetest'.
	CCLTRYE ( (pPrs = drmModeGetPlaneResources(fd)) != NULL, errno );
//	lprintf ( LOG_DBG, L"count_planes %d\r\n", i, (pPrs != NULL) ? pPrs->count_planes : -1 );
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctRes ) );
	CCLTRY ( pDct->store ( adtString(L"Plane"), adtIUnknown(pDctRes) ) );
	for (i = 0;hr == S_OK && i < pPrs->count_planes;++i)
		{
		IDictionary					*pDctInf		= NULL;
		IDictionary					*pDctPrps	= NULL;
		drmModePlane				*pPl			= NULL;
		drmModeObjectProperties	*pPrps		= NULL;

		// Retrieve object
//		lprintf ( LOG_DBG, L"planes[%d] = %d\r\n", i, pPrs->planes[i] );
		if ( (pPl = drmModeGetPlane ( fd, pPrs->planes[i] )) == NULL )
			continue;
//		lprintf ( LOG_DBG, L"pPl %p, id %d\r\n", pPl, pPl->plane_id );

		// Fill info
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctInf ) );
		CCLTRY ( pDctRes->store ( adtInt(i+1), adtIUnknown(pDctInf) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Id"), adtInt(pPl->plane_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"IdCrtc"), adtInt(pPl->crtc_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"IdFb"), adtInt(pPl->fb_id) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Xcrtc"), adtInt(pPl->crtc_x) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Ycrtc"), adtInt(pPl->crtc_x) ) );
		CCLTRY ( pDctInf->store ( adtString(L"X"), adtInt(pPl->x) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Y"), adtInt(pPl->y) ) );
		CCLTRY ( pDctInf->store ( adtString(L"Crtcs"), adtInt(pPl->possible_crtcs) ) );

		// Properties
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctPrps ) );
		CCLTRY ( pDctInf->store ( adtString(L"Properties"), adtIUnknown(pDctPrps) ) );
		if (hr == S_OK && (pPrps = drmModeObjectGetProperties ( fd, 
				pPl->plane_id, DRM_MODE_OBJECT_PLANE )) != NULL )
			{
			// Retrieve and store properties under name
			for (j = 0;hr == S_OK && j < pPrps->count_props;++j)
				{
				IDictionary				*pDctPrp	= NULL;
				drmModePropertyRes	*pPrp		= NULL;

				// Grab property information
				if ( (pPrp = drmModeGetProperty ( fd, pPrps->props[j] )) == NULL )
					continue;
//				lprintf ( LOG_DBG, L"Property %d : %S : vals %d : enums %d\r\n",
//								pPrp->prop_id, pPrp->name, pPrp->count_values, pPrp->count_enums );

				// Fill info
				CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctPrp ) );
				CCLTRY ( pDctPrps->store ( adtString(strV = pPrp->name), adtIUnknown(pDctPrp) ) );
				CCLTRY ( pDctPrp->store ( adtString(L"Id"), adtInt(pPrp->prop_id) ) );

				// Clean up
				_RELEASE(pDctInf);
				drmModeFreeProperty(pPrp);
				}	// for
			}	// if

		// Clean up
		_RELEASE(pDctPrps);
		if (pPrps != NULL)
			drmModeFreeObjectProperties(pPrps);
		_RELEASE(pDctInf);
		drmModeFreePlane(pPl);
		}	// for
	_RELEASE(pDctRes);

	// Clean up
	if (pPrs != NULL)
		drmModeFreePlaneResources(pPrs);
	if (pRes != NULL)
		drmModeFreeResources(pRes);

//	lprintf ( LOG_DBG, L"hr 0x%x\r\n", hr );

	return hr;
	}	// fill

HRESULT Device :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Module"), vL ) == S_OK)
			hr = adtValue::toString(vL,strMod);
		}	// if

	// Detach
	else
		{
		// Shutdown
		onReceive(prClose,adtInt());
		_RELEASE(pDctDv);
		}	// else

	return hr;
	}	// onAttach

HRESULT Device :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open device
	if (_RCP(Open))
		{
		int	fd						= -1;
		int 	ret					= 0;
		char	*pcMod				= NULL;
		struct kms_driver *kms	= NULL;

		// State check
		CCLTRYE ( pDctDv != NULL, ERROR_INVALID_STATE );

		// Module specified ?
		if (strMod.length() > 0)
			hr = strMod.toAscii(&pcMod);

		// Open device
		CCLTRYE ( (fd = drmOpen ( pcMod, NULL )) >= 0, errno );

		// Fill in resources for device
		CCLTRY ( fill ( pDctDv, fd ) );

		// Open associated KMS utilities
		CCLTRYE ( (ret = kms_create ( fd, &kms )) >= 0, errno );

		// Result
		CCLTRY ( pDctDv->store ( adtString(L"Drm"), adtInt(fd) ) );
		CCLTRY ( pDctDv->store ( adtString(L"Kms"), adtLong((U64)kms) ) );
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDctDv));
		else
			{
			lprintf ( LOG_DBG, L"Open : pcMod %S : hr 0x%x : fd %d kms %p\r\n", 
							pcMod, hr, fd, kms );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		if (hr != S_OK)
			{
			if (kms != NULL)
				kms_destroy(&kms);
			if (fd != -1)
				drmClose(fd);
			}	// if
		_FREEMEM(pcMod);
		}	// else if

	// Close active device
	else if (_RCP(Close))
		{
		adtValue vL;

		// State check
		CCLTRYE ( pDctDv != NULL, ERROR_INVALID_STATE );

		// Closing
		CCLOK ( _EMT(Close,adtIUnknown(pDctDv)); )

		// Clean up
		if (hr == S_OK && pDctDv->load ( adtString(L"Kms"), vL ) == S_OK)
			{
			struct kms_driver *kms = (struct kms_driver *)(U64)adtLong(vL);
			kms_destroy ( &kms );
			pDctDv->remove ( adtString(L"Kms") );
			}	// if
		if (hr == S_OK && pDctDv->load ( adtString(L"Drm"), vL ) == S_OK)
			{
			drmClose ( adtInt(vL) );
			pDctDv->remove ( adtString(L"Drm") );
			}	// if

		}	// else if

	// State
	else if (_RCP(Device))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctDv);
		_QISAFE(unkV,IID_IDictionary,&pDctDv);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

