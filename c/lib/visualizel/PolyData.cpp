////////////////////////////////////////////////////////////////////////
//
//									PolyData.CPP
//
//				Implementation of the poly data node.
//
////////////////////////////////////////////////////////////////////////

#include "visualizel_.h"
#include <stdio.h>

// Globals

PolyData :: PolyData ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	pItm	= NULL;
	}	// PolyData

HRESULT PolyData :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pItm);
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT PolyData :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		visObjRef	*pRef = NULL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Create poly data object for dictionary
		CCLTRYE ( (pRef = new visObjRef()) != NULL, E_OUTOFMEMORY );
		CCLTRYE	( (pRef->polydata = vtkSmartPointer<vtkPolyData>::New())
						!= NULL, E_OUTOFMEMORY );

		// Result
		CCLTRY	( pDct->store ( adtString(L"visObjRef"), adtIUnknown(pRef) ) );
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Set item to object
	else if (_RCP(Set))
		{
		visObjRef	*pvPly = NULL;
		visObjRef	*pvItm = NULL;
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL && pItm != NULL, ERROR_INVALID_STATE );

		// Retrieve references
		CCLTRY (pDct->load(adtString(L"visObjRef"),vL));
		CCLTRY (pItm->load(adtString(L"visObjRef"),vL));
		CCLTRYE( (pvPly = (visObjRef *)(IUnknown *)(adtIUnknown(vL)))
						!= NULL, ERROR_INVALID_STATE );
		CCLTRYE( (pvItm = (visObjRef *)(IUnknown *)(adtIUnknown(vL)))
						!= NULL, ERROR_INVALID_STATE );
		CCLOK  ( pvPly->AddRef(); )
		CCLOK  ( pvItm->AddRef(); )

		// State check
		CCLTRYE ( pvPly->polydata != NULL, E_UNEXPECTED );

		// Points ?
		if (hr == S_OK && pvItm->pts != NULL)
			{
			// Assign point to object
			pvPly->polydata->SetPoints ( pvItm->pts );			
			}	// if

		// Clean up
		_RELEASE(pvItm);
		_RELEASE(pvPly);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Item))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pItm);
		_QISAFE(unkV,IID_IDictionary,&pItm);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

