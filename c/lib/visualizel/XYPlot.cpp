////////////////////////////////////////////////////////////////////////
//
//								XYPlot.CPP
//
//				Implementation of the XY Plot node.
//
////////////////////////////////////////////////////////////////////////

#include "visualizel_.h"
#include <stdio.h>

// Globals
extern sysCS csVtk;

XYPlot :: XYPlot ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	pImg	= NULL;
	pRng	= NULL;
	pLbl	= NULL;
	pTbl	= NULL;
	}	// XYPlot

HRESULT XYPlot :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pTbl);
		_RELEASE(pLbl);
		_RELEASE(pRng);
		_RELEASE(pDct);
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT XYPlot :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// VTK Thread safety
	csVtk.enter();

	// Execute
	if (_RCP(Fire))
		{
		visObjRef	*pvRef	= NULL;
		S32			iH			= 0;
		S32			iW			= 0;
		adtValue		vL;
		ImageDct		imgDct;

		// State check
		CCLTRYE ( pDct != NULL && pImg != NULL, ERROR_INVALID_STATE );

		// Access image data
		CCLTRY ( imgDct.lock ( pImg ) );

		// For single channel data, the width and height are one for one
		if (hr == S_OK && imgDct.iCh == 1)
			{
			iH = imgDct.iH;
			iW = imgDct.iW;
			}	// if

		// For multi-channel data, the "height" is the number of channels
		else if (hr == S_OK)
			{
			iH = imgDct.iCh;
			iW = imgDct.iW;
			}	// else if

		// The 'height' of the image is the height for single channel data,
		// and the number of channels for multi-channel data.
		CCLTRYE( (iH > 1 && iW > 1), ERROR_INVALID_STATE );
		if (hr == S_OK)
			{
			//
			// Existing object ?
			//
			if (pDct->load ( adtString(L"visObjRef"), vL ) == S_OK)
				{
				CCLTRYE( (pvRef = (visObjRef *)(IUnknown *)(adtIUnknown(vL)))
								!= NULL, ERROR_INVALID_STATE );
				CCLOK  ( pvRef->AddRef(); )
				}	// if
			else
				{
				CCLTRYE	( (pvRef = new visObjRef()) != NULL, E_OUTOFMEMORY );
				CCLTRY	( pDct->store ( adtString(L"visObjRef"), adtIUnknown(pvRef) ) );
				}	// else

			//
			// Table
			//
			vtkSmartPointer<vtkTable>
			table;
			vtkSmartPointer<vtkChartXY>	
			chart;

			// New chart ?

			// Obtain table ptr. from existing actor or create new object chain
			if (pvRef->renderobj != NULL)
				{
				// Renderable actor object
				vtkSmartPointer<vtkContextActor>
				actor = vtkContextActor::SafeDownCast (  pvRef->renderobj );

				// First item is the chart
				chart = vtkChartXY::SafeDownCast ( actor->GetScene()->GetItem(0) );

				// Get table assigned to first plot line
				table = chart->GetPlot(0)->GetInput();
				}	// if

			// Brand new object chain
			else
				{
				// New table
				table = vtkSmartPointer<vtkTable>::New();

				// New chart and renderable
				chart = vtkSmartPointer<vtkChartXY>::New();
				vtkSmartPointer<vtkContextActor>
				actor = vtkSmartPointer<vtkContextActor>::New();
				actor->GetScene()->AddItem(chart);

				// Renderable
				pvRef->renderobj = actor;
				}	// else

			// Currently re-creating plot object every time due to problems updating
			// the scaling on future updates.
			chart->ClearPlots();
			bool bLegend = false;

			// All columns must exist and be the correct size.
			// When dynamically adding data to a table 'table->SetNumberOfRows' seems
			// to be insufficient by itself
			for (int r = 0;r < iH;++r)
				{
				// Active column
				vtkSmartPointer<vtkFloatArray>
				arr	= (vtkFloatArray *) table->GetColumn(r);

				// Need a new column ?
				bool bAdd = (arr == NULL);

				// Create new column
				if (bAdd)
					arr	= vtkSmartPointer<vtkFloatArray>::New();

				// Name column
				if (bAdd)
					{
					char	cCol[21];
					#ifdef	_WIN32
					sprintf_s ( cCol, "Column%d", r );
					#else
					sprintf ( cCol, "Column%d", r );
					#endif
					arr->SetName ( cCol );
					}	// if

				// Set size
				arr->SetNumberOfTuples(iW);

				// Add to table
				if (bAdd)
					table->AddColumn ( arr );
				}	// for

			//
			// Update points
			//
			table->SetNumberOfRows(iW);
			for (int r = 0,idx = 0;r < iH;++r)
				{

				// Add points
				for (int c = 0;c < iW;++c)
					{
					// Valid point ?
					float
					fVal = imgDct.getFloat(c,r);
//					if (isnan(fVal))
//						continue;

					// Add to list.  TODO: "Real" x/y coordinates
//					lprintf ( LOG_DBG, L"%d,%d : %g\r\n", c, r, fVal );
					table->SetValue(c,r,fVal);
					}	// for
				}	// for

			//
			// Plot lines for data.  One for each table plot.
			//
			IIt	*pIt	= NULL;
			U32	nplot = 1;
			if (pTbl != NULL)
				pTbl->iterate ( &pIt );

			// Default is a single plot
			if (pIt == NULL)
				{
				vtkPlot *line = chart->AddPlot(vtkChart::LINE);
				line->SetInputData ( table, 0, 1 );
				line->SetColor ( 0, 128, 0, 0 );
//					line->SetColor ( 0, 0, 128, 0 );
				line->SetWidth ( 2.0 );
				}	// if
			else
				{
				// Add plots
				while (pIt->read ( vL ) == S_OK)
					{
					IDictionary		*pDctTbl	= NULL;
					adtIUnknown		unkV(vL);
					if ((IUnknown *)NULL != unkV && _QI(unkV,IID_IDictionary,&pDctTbl) == S_OK)
						{
						adtInt	iX(0),iY(1),iClr(0xffffff);
						if (pDctTbl->load ( adtString(L"X"), vL ) == S_OK)
							iX = vL;
						if (pDctTbl->load ( adtString(L"Y"), vL ) == S_OK)
							iY = vL;
						if (pDctTbl->load ( adtString(L"Color"), vL ) == S_OK)
							iClr = vL;

						// Valid index ?
						if ((S32)iX >= 0 && (S32)iX < iH && (S32)iY >= 0 && (S32)iY < iH)
							{
							// Add plot
							vtkPlot *line = chart->AddPlot(vtkChart::LINE);
							line->SetInputData ( table, iX, iY );
							line->SetColor (	(iClr >> 16) & 0xff,
													(iClr >> 8) & 0xff, (iClr & 0xff) );
							line->SetWidth ( 2.0 );

							// Label ?
							if (pDctTbl->load ( adtString(L"Label"), vL ) == S_OK)
								{
								adtString	strLbl(vL);
								char			*paLbl = NULL;

								// Set label for plot
								if (strLbl.toAscii(&paLbl) == S_OK)
									line->SetLabel ( paLbl );
								bLegend = true;

								// Clean up
								_FREEMEM(paLbl);
								}	// if
							}	// if
						}	// if

					// Clean up
					_RELEASE(pDctTbl);
					pIt->next();
					}	// while

				// Clean up
				_RELEASE(pIt);
				}	// else

			//
			// Some reasonable defaults
			//
			chart->GetAxis(vtkAxis::BOTTOM)->SetGridVisible(true);
//			chart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetColor(255,255,255);
//			chart->GetAxis(vtkAxis::BOTTOM)->SetRange
			chart->GetAxis(vtkAxis::LEFT)->SetGridVisible(true);
//			chart->GetAxis(vtkAxis::LEFT)->GetTitleProperties()->SetColor(255,255,255);
			chart->SetShowLegend(bLegend);
			if (bLegend)
				{
				chart->GetLegend()->SetVerticalAlignment(vtkChartLegend::TOP);
				chart->GetLegend()->SetHorizontalAlignment(vtkChartLegend::CENTER);
				}	// if

			//
			// Update ranges
			//

			// X-axis
			if (pRng != NULL && pRng->load ( adtString(L"Left"), vL ) == S_OK)
				{
				// Limits
				adtDouble	dMin(vL);
				adtDouble	dMax(dMin+1);
				if (pRng->load ( adtString(L"Right"), vL ) == S_OK)
					dMax = vL;

				// Range
				chart->GetAxis(vtkAxis::BOTTOM)->SetBehavior(vtkAxis::FIXED);
				chart->GetAxis(vtkAxis::BOTTOM)->SetRange(dMin,dMax);
				}	// if
			else
				chart->GetAxis(vtkAxis::BOTTOM)->SetBehavior(vtkAxis::AUTO);
			if (pLbl != NULL && pLbl->load ( adtString(L"Bottom"), vL ) == S_OK)
				{
				char			*paLbl	= NULL;
				adtString	strLbl(vL);
				if (strLbl.toAscii(&paLbl) == S_OK)
					chart->GetAxis(vtkAxis::BOTTOM)->SetTitle ( paLbl );
				_FREEMEM(paLbl);
				}	// if
			else
				chart->GetAxis(vtkAxis::BOTTOM)->SetVisible(false);

			// Y-axis
			if (pRng != NULL && pRng->load ( adtString(L"Top"), vL ) == S_OK)
				{
				// Limits
				adtDouble	dMax(vL);
				adtDouble	dMin(dMax+1);
				if (pRng->load ( adtString(L"Bottom"), vL ) == S_OK)
					dMin = vL;

				// Range
				chart->GetAxis(vtkAxis::LEFT)->SetBehavior(vtkAxis::FIXED);
				chart->GetAxis(vtkAxis::LEFT)->SetRange(dMin,dMax);
				}	// if
			else
				chart->GetAxis(vtkAxis::LEFT)->SetBehavior(vtkAxis::AUTO);
			if (pLbl != NULL && pLbl->load ( adtString(L"Left"), vL ) == S_OK)
				{
				char			*paLbl	= NULL;
				adtString	strLbl(vL);
				if (strLbl.toAscii(&paLbl) == S_OK)
					chart->GetAxis(vtkAxis::LEFT)->SetTitle ( paLbl );
				_FREEMEM(paLbl);
				}	// if
			else
				chart->GetAxis(vtkAxis::LEFT)->SetVisible(false);

			// Ensure settings
			chart->GetAxis(vtkAxis::BOTTOM)->Modified();
			chart->GetAxis(vtkAxis::LEFT)->Modified();

			// Objects update
			table->Modified();
			chart->Modified();

			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pvRef);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Range))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pRng);
		_QISAFE(unkV,IID_IDictionary,&pRng);
		}	// else if
	else if (_RCP(Label))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pLbl);
		_QISAFE(unkV,IID_IDictionary,&pLbl);
		}	// else if
	else if (_RCP(Table))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pTbl);
		_QISAFE(unkV,IID_IList,&pTbl);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	// VTK Thread safety
	csVtk.leave();

	return hr;
	}	// receive

