////////////////////////////////////////////////////////////////////////
//
//							ColoredElevationMap.CPP
//
//				Implementation of the colored elevation map node.
//
////////////////////////////////////////////////////////////////////////

#include "visualizel_.h"
#include <stdio.h>

// Globals

ColoredElevationMap :: ColoredElevationMap ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	pImg	= NULL;
	}	// ColoredElevationMap

HRESULT ColoredElevationMap :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT ColoredElevationMap :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		visObjRef	*pvRef = NULL;
		adtValue		vL;
		ImageDct		imgDct;

		// State check
		CCLTRYE ( pDct != NULL && pImg != NULL, ERROR_INVALID_STATE );

		// Access image data
		CCLTRY ( imgDct.lock ( pImg ) );
		if (hr == S_OK)
			{
			//
			// Points
			//
			vtkSmartPointer<vtkPoints>
			points = vtkSmartPointer<vtkPoints>::New();

			// Assign points to point grid
			for (int r = 0,idx = 0;r < imgDct.iH;++r)
				for (int c = 0;c < imgDct.iW;++c)
					{
					// Valid point ?
					float
					fVal = imgDct.getFloat(c,r);
//					if (isnan(fVal))
//						continue;

					// Add to list.  TODO: "Real" x/y coordinates
					points->InsertPoint(idx++,c,r,fVal);
					}	// for

			//
			// Poly data
			//
			vtkSmartPointer<vtkPolyData>	
			inputPolyData = vtkSmartPointer<vtkPolyData>::New();
			inputPolyData->SetPoints(points);

			//
			// Trianulate the grid point
			//
			vtkSmartPointer<vtkDelaunay2D>
			delaunay = vtkSmartPointer<vtkDelaunay2D>::New();
			delaunay->SetInputData(inputPolyData);
			delaunay->Update();

			//
			// Data bounds
			//
			vtkPolyData	*
			outputPolyData = delaunay->GetOutput();
			double	bounds[6];
			outputPolyData->GetBounds(bounds);
			double 
			minz	= bounds[4];
			double
			maxz	= bounds[5];

			//
			// Create the color map covering the full range of values
			//
			vtkSmartPointer<vtkLookupTable>
			colorLookupTable = vtkSmartPointer<vtkLookupTable>::New();
			colorLookupTable->SetTableRange(minz,maxz);
			colorLookupTable->Build();

			//
			// Generate colors for each point
			//
			vtkSmartPointer<vtkUnsignedCharArray>
			colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
			colors->SetNumberOfComponents(3);
			colors->SetName("Colors");
			for (int i = 0;i < outputPolyData->GetNumberOfPoints();++i)
				{
				// Point value
				double p[3];
				outputPolyData->GetPoint(i,p);

				// Colors at value
				double dcolor[3];
				colorLookupTable->GetColor(p[2],dcolor);

				// Assign RGB color
				unsigned char color[3];
				for (unsigned int j = 0;j < 3;++j)
					{
					color[j] = static_cast<unsigned char>(255.0 * dcolor[j]);
					}	// for
//				colors->InsertNextTupleValue(color);
//				colors->InsertNextTypedTuple(color);
//				colors->InsertNextTupleValue(color);
				colors->InsertNextTuple3(color[0],color[1],color[2]);
				}	// for

			// Map scalars
			outputPolyData->GetPointData()->SetScalars(colors);

			//
			// Mapper
			//
			vtkSmartPointer<vtkPolyDataMapper>
			mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
			mapper->SetInputData(outputPolyData);

			//
			// Actor
			//
			vtkSmartPointer<vtkActor>
			actor = vtkSmartPointer<vtkActor>::New();
			actor->SetMapper(mapper);

			// Actor is stored in dictionary
			CCLTRYE	( (pvRef = new visObjRef()) != NULL, E_OUTOFMEMORY );
			CCLOK		( pvRef->renderobj = actor; )
			CCLTRY	( pDct->store ( adtString(L"visObjRef"), adtIUnknown(pvRef) ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pvRef);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

