////////////////////////////////////////////////////////////////////////
//
//										VISUALIZEL_.H
//
//		Implementation include file for visualization library
//
////////////////////////////////////////////////////////////////////////

#ifndef	VISUALIZEL__H
#define	VISUALIZEL__H

// Includes
#include	"visualizel.h"
#include "../imagel/imagel.h"

//
// Currently using the Visualization Toolkit (VTK)
//
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkGraphicsFactory.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPointSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkCamera.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkDelaunay2D.h>
#include <vtkLookupTable.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkTable.h>
#include <vtkFloatArray.h>
#include <vtkChartXY.h>
#include <vtkContextView.h>
#include <vtkContextScene.h>
#include <vtkContextActor.h>
#include <vtkPlot.h>
#include <vtkAxis.h>
#include <vtkTextProperty.h>
#include <vtkChartLegend.h>

///////////
// Objects
///////////

//
// Class - visObjRef.  Object reference container.
//

class visObjRef :
	public CCLObject										// Base class
	{
	public :
	visObjRef ( void ) { AddRef(); }					// Constructor

	// Run-time data
	vtkSmartPointer<vtkRenderer>		renderer;	// Renderer object
	vtkSmartPointer<vtkRenderWindow>	renderw;		// Rendere window object
	vtkSmartPointer<vtkWindowToImageFilter>
												wif;			// Window to image filter
	vtkSmartPointer<vtkPoints>			pts;			// Points collection
	vtkSmartPointer<vtkProp>			renderobj;	// Object that can exist in a rendered scene
	vtkSmartPointer<vtkPolyData>		polydata;	// Data structure
	vtkSmartPointer<vtkDelaunay2D>	delaunay;	// Data structure

	// CCL
	CCL_OBJECT_BEGIN_INT(visObjRef)
	CCL_OBJECT_END()
	};

/////////
// Nodes
/////////

//
// Class - ColoredElevationMap.  Implementation of a colored elevation map.
//

class ColoredElevationMap :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	ColoredElevationMap ( void );						// Constructor

	// Run-time data
	IDictionary		*pDct;								// Dictionary
	IDictionary		*pImg;								// Image data

	// CCL
	CCL_OBJECT_BEGIN(ColoredElevationMap)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	DECLARE_RCP(Image)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
		DEFINE_RCP(Image)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Delaunay.  Delaunay triagulation of data.
//

class Delaunay :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Delaunay ( void );									// Constructor

	// Run-time data
	IDictionary		*pDct;								// Dictionary
	IDictionary		*pItm;								// Current item
	
	// CCL
	CCL_OBJECT_BEGIN(Delaunay)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Fire)
	DECLARE_CON(Set)
	DECLARE_EMT(Error)
	DECLARE_RCP(Item)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Fire)
		DEFINE_CON(Set)
		DEFINE_EMT(Error)
		DEFINE_RCP(Item)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Points.  Point cloud generation.
//

class Points :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Points ( void );										// Constructor

	// Run-time data
	IDictionary	*pDct;									// Context
	IDictionary	*pImg;									// Image data
	
	// CCL
	CCL_OBJECT_BEGIN(Points)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Image)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Image)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - PolyData.  Visualization data set.
//

class PolyData :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	PolyData ( void );									// Constructor

	// Run-time data
	IDictionary		*pDct;								// Dictionary
	IDictionary		*pItm;								// Current item
	
	// CCL
	CCL_OBJECT_BEGIN(PolyData)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Fire)
	DECLARE_CON(Set)
	DECLARE_EMT(Error)
	DECLARE_RCP(Item)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Fire)
		DEFINE_CON(Set)
		DEFINE_EMT(Error)
		DEFINE_RCP(Item)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Render.  Rendering node.
//

class Render :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Render ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context
	IDictionary		*pImg;								// Rendered image
	IDictionary		*pItm;								// Current item
	IMemoryMapped	*pBits;								// Image bits
	adtInt			iW,iH;								// Size

	// CCL
	CCL_OBJECT_BEGIN(Render)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Item)
	DECLARE_RCP(Add)
	DECLARE_RCP(Close)
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	DECLARE_CON(Open)
	DECLARE_RCP(Remove)
	DECLARE_RCP(Width)
	DECLARE_RCP(Height)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Item)
		DEFINE_RCP(Add)
		DEFINE_RCP(Close)
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
		DEFINE_CON(Open)
		DEFINE_RCP(Remove)
		DEFINE_RCP(Width)
		DEFINE_RCP(Height)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Renderable.  Creates a 'renderable' object
//

class Renderable :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Renderable ( void );									// Constructor

	// Run-time data
	IDictionary	*pDct;									// Context
	IDictionary		*pItm;								// Current item
	
	// CCL
	CCL_OBJECT_BEGIN(Renderable)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Item)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Item)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - XYPlot.  Implementation of an XY plot.
//

class XYPlot :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	XYPlot ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Dictionary
	IDictionary		*pImg;								// Image data
	IDictionary		*pRng;								// Range dictionary
	IDictionary		*pLbl;								// Label dictionary
	IContainer		*pTbl;								// Table configuration dictionary

	// CCL
	CCL_OBJECT_BEGIN(XYPlot)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	DECLARE_RCP(Image)
	DECLARE_RCP(Label)
	DECLARE_RCP(Range)
	DECLARE_RCP(Table)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
		DEFINE_RCP(Image)
		DEFINE_RCP(Label)
		DEFINE_RCP(Range)
		DEFINE_RCP(Table)
	END_BEHAVIOUR_NOTIFY()
	};

#endif
