////////////////////////////////////////////////////////////////////////
//
//									PythonOp.CPP
//
//				Implementation of the python operations node
//
////////////////////////////////////////////////////////////////////////

#include "pythonl_.h"

// Globals
static U32 lRefCnt = 0;

PythonOp :: PythonOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the node
	//
	////////////////////////////////////////////////////////////////////////
	pDctMod	= NULL;
	pDctObj	= NULL;
	pDctAtr	= NULL;
	pyArgs	= NULL;
	bInitd	= false;
	}	// PythonOp

HRESULT PythonOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Initialize the Python interpreter
		if (lRefCnt == 0)
			{
			Py_Initialize();
			bInitd = Py_IsInitialized();
			if (bInitd) ++lRefCnt;
			}	// if

		// Debug
//		PyRun_SimpleString ( "import sys" );
//		PyRun_SimpleString ( "f = open (\"c:\\temp\\test.txt\",\"w\")" );
//		PyRun_SimpleString ( "f.write(\"YouCanByteMeNow\")" );
//		PyRun_SimpleString ( "import sys\n"
//									"print sys.path\n" );
//		PyRun_SimpleString ( "sys.stdout.flush()" );
//		Py_Finalize();

		// Defaults
		if (pnDesc->load(adtString(L"Name"),vL) == S_OK)
			hr = adtValue::toString(vL,strName);
		if (pnDesc->load(adtString(L"NameAs"),vL) == S_OK)
			hr = adtValue::toString(vL,strNameAs);
		}	// if

	// Detach
	else
		{
		if (--lRefCnt == 0)
			{
			if (bInitd)
				Py_Finalize();
			bInitd = false;
			}	// if

		// Clean up
		if (pyArgs != NULL)
			Py_DECREF(pyArgs);
		_RELEASE(pDctAtr);
		_RELEASE(pDctMod);
		_RELEASE(pDctObj);
		}	// else

	return S_OK;
	}	// onAttach

HRESULT PythonOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Import module into dictionary
	if (_RCP(Import))
		{
		PyObject *pModule = NULL;
		PyObject	*pName	= NULL;
		adtValue	vL;

		// State check
		CCLTRYE ( pDctMod != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// One module per dictionay
		CCLTRYE ( pDctMod->load ( adtString(L"Module"), vL ) != S_OK,
						ERROR_INVALID_STATE );

		// Current name into Python string
		CCLTRYE ( (pName = PyUnicode_FromWideChar ( strName, -1 ))
						!= NULL, E_UNEXPECTED );

		// Import the module
		CCLTRYE ( (pModule = PyImport_Import ( pName )) != NULL, E_UNEXPECTED );

		// Result
		CCLTRY ( pDctMod->store ( adtString(L"Module"),
											adtIUnknown(new pyRef(pModule)) ) );
		if (hr == S_OK)
			_EMT(Import,adtIUnknown(pDctMod));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		if (pName != NULL)
			Py_DECREF(pName);
		}	// else if

	// Create new object into dictionary
	else if (_RCP(Create))
		{
		pyRef			*pyMod	= NULL;
		PyObject		*pyDct	= NULL;
		PyObject		*pyCls	= NULL;
		PyObject		*pyName	= NULL;
		PyObject		*pyObj	= NULL;
		adtValue		vL;
		adtIUnknown	unkV;

		// State check
		CCLTRYE ( pDctMod != NULL && pDctObj != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// One object per dictionay
		CCLTRYE ( pDctObj->load ( adtString(L"Object"), vL ) != S_OK,
						ERROR_INVALID_STATE );

		// Extract module
		CCLTRY ( pDctMod->load ( adtString(L"Module"), vL ) );
		CCLTRYE( (pyMod = (pyRef *)(IUnknown *)adtIUnknown(vL)) != NULL, ERROR_INVALID_STATE );

		// Module information
		CCLTRYE ( (pyDct = PyModule_GetDict(*pyMod)) != NULL, E_UNEXPECTED );

		// Current name into Python string
		CCLTRYE ( (pyName = PyUnicode_FromWideChar ( strName, -1 ))
						!= NULL, E_UNEXPECTED );

		// Name is the class name
		CCLTRYE ( (pyCls = PyDict_GetItem(pyDct,pyName)) != NULL, E_UNEXPECTED );

		// Creste Python object
		CCLTRYE ( (pyObj = PyObject_CallObject(pyCls,NULL)) != NULL, E_UNEXPECTED );

		// Result
		CCLTRY ( pDctObj->store ( adtString(L"Object"),
											adtIUnknown(new pyRef(pyObj)) ) );
		if (hr == S_OK)
			_EMT(Create,adtIUnknown(pDctObj));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		if (pyDct != NULL)
			Py_DECREF(pyDct);
		if (pyCls != NULL)
			Py_DECREF(pyCls);
		if (pyName != NULL)
			Py_DECREF(pyName);
		}	// else if

	// Call method on object
	else if (_RCP(Method))
		{
		pyRef		*pyObj	= NULL;
		PyObject	*pyName	= NULL;
		PyObject *pyRet	= NULL;
		size_t	args		= 0;
		adtValue vL,vRet;

		// State check
		CCLTRYE ( pDctObj != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Extract object
		CCLTRY ( pDctObj->load ( adtString(L"Object"), vL ) );
		CCLTRYE( (pyObj = (pyRef *)(IUnknown *)adtIUnknown(vL)) != NULL, ERROR_INVALID_STATE );

		// Name is the moethod to call
		CCLTRYE ( (pyName = PyUnicode_FromWideChar ( strName, -1 ))
						!= NULL, E_UNEXPECTED );

		// There doesn't seem to be a way to call a method with a tuple
		// of arguments, there must be a way (?)
//		PyObject_CallMethodObjArgs ( *pyObj, pyName, pyArgs, NULL );
		if (!WCASECMP(strName,L"init_camera_feed"))
			lprintf ( LOG_DBG, L"Hi\r\n");
		if (!WCASECMP(strName,L"connect"))
			lprintf ( LOG_DBG, L"Hi\r\n");

		// Support required number of arguments
		if (hr == S_OK)
			{
			args = (pyArgs != NULL) ? PyTuple_Size(pyArgs) : 0;
			switch (args)
				{
				case 0 : 
					pyRet = PyObject_CallMethodObjArgs ( *pyObj, pyName, NULL );
					break;
				case 1 : 
					pyRet = PyObject_CallMethodObjArgs ( *pyObj, pyName, 
								PyTuple_GetItem(pyArgs,0), 
								NULL );
					break;
				case 2 : 
					pyRet = PyObject_CallMethodObjArgs ( *pyObj, pyName, 
								PyTuple_GetItem(pyArgs,0), 
								PyTuple_GetItem(pyArgs,1), 
								NULL );
					break;
				case 3 : 
					pyRet = PyObject_CallMethodObjArgs ( *pyObj, pyName, 
								PyTuple_GetItem(pyArgs,0), 
								PyTuple_GetItem(pyArgs,1), 
								PyTuple_GetItem(pyArgs,2), 
								NULL );
					break;
				case 4 : 
					pyRet = PyObject_CallMethodObjArgs ( *pyObj, pyName, 
								PyTuple_GetItem(pyArgs,0), 
								PyTuple_GetItem(pyArgs,1), 
								PyTuple_GetItem(pyArgs,2), 
								PyTuple_GetItem(pyArgs,3), 
								NULL );
					break;
				default :
					lprintf ( LOG_DBG, L"Unsupported number of arguments %d\r\n", args );
				}	// switch
			}	// if

		// Result
		if (hr == S_OK && pyRet != NULL)
			toValue ( pyRet, vRet );
		else
			adtValue::copy(adtInt(0),vRet);
		if (hr == S_OK)
			_EMT(Method,vRet);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		if (pyName != NULL)
			Py_DECREF(pyName);
		}	// else if

	// Set parameters
	else if (_RCP(Parameters))
		{
		IContainer	*pCnt	= NULL;
		IIt			*pIt	= NULL;
		adtIUnknown unkV(v);
		U32			sz;

		// Previous argument tuple
		if (pyArgs != NULL)
			{
			Py_DECREF(pyArgs);
			pyArgs = NULL;
			}	// if

		// Number of arguments
		CCLTRY (_QISAFE(unkV,IID_IContainer,&pCnt));
		CCLTRY(pCnt->size(&sz));

		// Only need tuple for arguments
		if (hr == S_OK && sz > 0)
			{
			U32		idx = 0;
			adtValue vL;

			// Create a tuple of the needed size
			CCLTRYE ( (pyArgs = PyTuple_New(sz)) != NULL, E_UNEXPECTED );

			// Iterate values
			CCLTRY (pCnt->iterate(&pIt));
			while (hr == S_OK && pIt->read ( vL ) == S_OK)
				{
				PyObject	*pyArg = NULL;

				// Convert value to python object based on type
				switch (adtValue::type(vL))
					{
					// String
					case VTYPE_STR :
						pyArg = PyUnicode_FromWideChar ( adtString(vL), -1 );
						break;
					// Integers
					case VTYPE_I4 :
						pyArg = PyLong_FromLong ( vL.vint );
						break;
					case VTYPE_I8 :
						pyArg = PyLong_FromLongLong ( vL.vlong );
						break;
					// Float
					case VTYPE_R4 :
						pyArg = PyFloat_FromDouble(vL.vflt);
						break;
					case VTYPE_R8 :
						pyArg = PyFloat_FromDouble(vL.vdbl);
						break;
					// Boolean
					case  VTYPE_BOOL :
//						pyArg = PyBool_FromLong((vL.vbool == 0) ? Py_Fals
						pyArg = (vL.vbool) ? Py_True : Py_False;
						break;
					default :
						lprintf ( LOG_DBG, L"Unhandled type %d\r\n", adtValue::type(vL) );
					}	// switch

				// Argument, call 'steals' reference
				PyTuple_SetItem(pyArgs,idx++,pyArg);

				// Proceed to next item
				pIt->next();
				}	// while

			}	// if

		// Clean up
		_RELEASE(pIt);
		_RELEASE(pCnt);
		}	// else if

	// Get attributes
	else if (_RCP(GetAttr))
		{
		pyRef		*pyObj	= NULL;
		PyObject	*pyName	= NULL;
		PyObject	*pyAttr	= NULL;
		PyObject *pyRet	= NULL;
		size_t	args		= 0;
		adtValue vL;
		adtValue	vAttr;

		// State check
		CCLTRYE ( pDctObj != NULL && pDctAtr != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Extract object
		CCLTRY ( pDctObj->load ( adtString(L"Object"), vL ) );
		CCLTRYE( (pyObj = (pyRef *)(IUnknown *)adtIUnknown(vL)) != NULL, ERROR_INVALID_STATE );

		// Name is the attribute to call
		CCLTRYE ( (pyName = PyUnicode_FromWideChar ( strName, -1 ))
						!= NULL, E_UNEXPECTED );

		// Get the attribute from the object
		CCLTRYE ( (pyAttr = PyObject_GetAttr(*pyObj,pyName))
							!= NULL, E_UNEXPECTED );
		CCLOK ( toValue ( pyAttr, vAttr ); )

		// Result
		CCLTRY ( pDctAtr->store (	(strNameAs.length() > 0) ? 
					strNameAs : adtString(L"Object"), vAttr ) );
		if (hr == S_OK)
			_EMT(GetAttr,adtIUnknown(pDctAtr));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
//		if (pyAttr != NULL)
//			Py_DECREF(pyAttr);
		if (pyName != NULL)
			Py_DECREF(pyName);
		}	// else if

	// Set attributes
	else if (_RCP(SetAttr))
		{
		pyRef		*pyObj	= NULL;
		PyObject	*pyName	= NULL;
		PyObject	*pyAttr	= NULL;
		PyObject *pyRet	= NULL;
		size_t	args		= 0;
		adtValue vL;
		adtValue	vAttr;

		// State check
		CCLTRYE ( pDctObj != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Extract object
		CCLTRY ( pDctObj->load ( adtString(L"Object"), vL ) );
		CCLTRYE( (pyObj = (pyRef *)(IUnknown *)adtIUnknown(vL)) != NULL, ERROR_INVALID_STATE );

		// Setting an attribute requires an argument

		// Name is the attribute to call
		CCLTRYE ( (pyName = PyUnicode_FromWideChar ( strName, -1 ))
						!= NULL, E_UNEXPECTED );

		// Support required number of arguments
		CCLOK		( args = (pyArgs != NULL) ? PyTuple_Size(pyArgs) : 0; )
		CCLTRYE	( (args == 1), ERROR_INVALID_STATE );

		// Set the attribute
		CCLTRYE ( PyObject_HasAttr ( *pyObj, pyName ) == 1, ERROR_NOT_FOUND );
//		CCLTRYE ( (PyObject_SetAttr ( *pyObj, pyName, 
//						PyTuple_GetItem(pyArgs,0) )) == 0, E_UNEXPECTED );
//		CCLTRYE ( (PyObject_SetItem ( *pyObj, pyName, 
//						PyTuple_GetItem(pyArgs,0) )) == 0, E_UNEXPECTED );

		// Result
		if (hr == S_OK)
			_EMT(SetAttr,adtInt(hr));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
//		if (pyAttr != NULL)
//			Py_DECREF(pyAttr);
		if (pyName != NULL)
			Py_DECREF(pyName);
		}	// else if

	// State
	else if (_RCP(Module))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctMod);
		_QISAFE(unkV,IID_IDictionary,&pDctMod);
		}	// else if
	else if (_RCP(Object))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctObj);
		_QISAFE(unkV,IID_IDictionary,&pDctObj);
		}	// else if
	else if (_RCP(Attribute))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctAtr);
		_QISAFE(unkV,IID_IDictionary,&pDctAtr);
		}	// else if
	else if (_RCP(Name))
		hr = adtValue::toString(v,strName);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT PythonOp :: toValue ( PyObject *pyObj, ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert Python object to a nSpac value.  If it cannot be
	//			conerted, it will be stored as a reference counted native 
	//			object.
	//
	//	PARAMETERS
	//		-	pyObj is the Python object
	//		-	v will receive the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Convert simple data types, better way to check data type ?
	if (!STRCASECMP(pyObj->ob_type->tp_name,"NoneType"))
		hr = adtValue::copy ( adtInt(0), v );
	else if (!STRCASECMP(pyObj->ob_type->tp_name,"int"))
		hr = adtValue::copy ( adtInt((S32)PyLong_AsLong(pyObj)), v );
	else if (!STRCASECMP(pyObj->ob_type->tp_name,"str"))
		hr = adtValue::copy ( adtString((const WCHAR *)PyUnicode_AS_UNICODE(pyObj)), v );
	else if (!STRCASECMP(pyObj->ob_type->tp_name,"bytes"))
		{
		IMemoryMapped	*pMem		= NULL;
		void				*pvMem	= NULL;
		U32				sz			= 0;

		// Number of bytes
		CCLTRYE( (sz = (U32)PyBytes_Size(pyObj)) > 0, E_UNEXPECTED );

		// Bytes will be placed into a memory mapped block
		CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pMem ) );
		CCLTRY ( pMem->setSize ( sz ) );
		CCLTRY ( pMem->getInfo ( &pvMem, NULL ) );

		// Copy bytes
		CCLOK ( memcpy ( pvMem, PyBytes_AsString(pyObj), sz ); )

		// Result
		CCLTRY ( adtValue::copy ( adtIUnknown(pMem), v ) );

		// Clean up
		_RELEASE(pMem);
		}	// else if
	else
		{
		// Save as native object
		hr = adtValue::copy ( adtIUnknown(new pyRef(pyObj)), v );
		lprintf ( LOG_DBG, L"Unconverted attribute type : %S\r\n",
									pyObj->ob_type->tp_name );
		}	// else

	return hr;
	}	// toValue

/*
{
PyObject *pyName;
PyObject	*pyItm	= NULL;

		// Current name into Python string
		CCLTRYE ( (pyName = PyUnicode_FromWideChar ( L"behavior", -1 ))
						!= NULL, E_UNEXPECTED );

		// Test
		lprintf ( LOG_DBG, L"Has %d", PyObject_HasAttrString(*pyObj,"behavior") );

		// Name is the class name
		CCLTRYE ( (pyItm = PyObject_GetAttrString(*pyObj,"behavior"))
							!= NULL, E_UNEXPECTED );


		if (pyName != NULL)
			Py_DECREF(pyName);
		CCLTRYE ( (pyName = PyUnicode_FromWideChar ( L"drive_off_charger", -1 ))
						!= NULL, E_UNEXPECTED );
		CCLOK ( PyObject_CallMethodObjArgs ( pyItm, pyName, NULL ); )

		// Clean up
		if (pyItm != NULL)
			Py_DECREF(pyItm);
		if (pyName != NULL)
			Py_DECREF(pyName);
}
*/
