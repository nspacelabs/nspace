////////////////////////////////////////////////////////////////////////
//
//										PYTHONL_.H
//
//				Implementation include file for the python library
//
////////////////////////////////////////////////////////////////////////

#ifndef	PYTHONL__H
#define	PYTHONL__H

// Includes
#include	"pythonl.h"

// Library.  Too many problems with debug version of Python DLL so
// ensure release version is always used.  For example PyImport_Import 
// always fails no matter what with debug DLL.
#ifdef	_DEBUG
#undef	_DEBUG
#include <Python.h>
#define	_DEBUG
#else
#include <Python.h>
#endif

///////////
// Objects
///////////

//
// Class - pyRef.  Object to cache reference counted Python matrices
//

class pyRef :
	public CCLObject									// Base class
	{
	public :
	pyRef ( void );									// Constructor
	pyRef ( PyObject * );							// Constructor
	virtual ~pyRef ( void );						// Destructor

	// Run-time data
	PyObject *pyObj;									// Python object

	// Operators
	operator PyObject *() { return pyObj; }

	// CCL
	CCL_OBJECT_BEGIN_INT(pyRef)
	CCL_OBJECT_END()
	};

/////////
// Nodes
/////////

//
// Class - PythonOp.  Python operations node.
//

class PythonOp :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	PythonOp ( void );									// Constructor

	// Run-time data
	IDictionary	*pDctMod,*pDctObj,*pDctAtr;		// Active context
	bool			bInitd;									// Python initialized ?
	adtString	strName;									// Active name
	adtString	strNameAs;								// Store name 'as'
	IDictionary	*pDctArgs;								// Arguments
	PyObject		*pyModule;								// Active module
	PyObject		*pyClass;								// Active class
	PyObject		*pyArgs;									// Argument tuples

	// CCL
	CCL_OBJECT_BEGIN(PythonOp)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Attribute)
	DECLARE_CON(Create)
	DECLARE_CON(GetAttr)
	DECLARE_CON(Import)
	DECLARE_EMT(Error)
	DECLARE_CON(Method)
	DECLARE_RCP(Module)
	DECLARE_RCP(Object)
	DECLARE_RCP(Name)
	DECLARE_RCP(Parameters)
	DECLARE_CON(SetAttr)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Attribute)
		DEFINE_CON(Create)
		DEFINE_CON(GetAttr)
		DEFINE_CON(Import)
		DEFINE_EMT(Error)
		DEFINE_CON(Method)
		DEFINE_RCP(Module)
		DEFINE_RCP(Object)
		DEFINE_RCP(Name)
		DEFINE_RCP(Parameters)
		DEFINE_CON(SetAttr)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Internal utilities
	HRESULT toValue ( PyObject *, ADTVALUE & );

	};

#endif
