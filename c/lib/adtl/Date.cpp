////////////////////////////////////////////////////////////////////////
//
//										DATE.CPP
//
//					Implementation of the 'date' class
//
////////////////////////////////////////////////////////////////////////

#include "adtl.h"
#include <time.h>
#include <math.h>
#include <stdio.h>
#if	__APPLE__ || __unix__
#include <sys/time.h>
#endif

// Convert character to number
#define	CHAR2NUM(a)		(((a) >= ('0') && (a) <= ('9')) ? (a)-'0' : 0)

// Days since Jan 1, 1970 to Jan 1, 2001
#define	DAY20011970		11323							

adtDate :: adtDate ( DATE _date )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Default constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	vtype		= VTYPE_DATE;
	vdate		= _date;
	}	// adtDate

adtDate :: adtDate ( const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	//	PARAMETERS
	//		-	v is the value to initialize with
	//
	////////////////////////////////////////////////////////////////////////
	vtype	= VTYPE_DATE;
	vdate	= 0;
	*this = v;
	}	// adtDate

HRESULT adtDate :: fromEpochSeconds ( time_t s, S32 us, DATE *d )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts the specified # of seconds since Jan 1, 1970 to the
	//			date format.
	//
	//	PARAMETERS
	//		-	s is the number of seconds since Jan 1, 1970
	//		-	us is the number of microseconds
	//		-	d will receive the date
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Convert to internal date
	(*d)	=	(s/SECINDAY) +
				((us/1000000.0)/SECINDAY) -
				DAY20011970;

	return S_OK;
	}	// fromEpochSeconds

HRESULT adtDate :: fromSeconds ( double s, DATE *d )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts the specified # of seconds to date format.  Useful
	//			of measureing 'delta' times.
	//
	//	PARAMETERS
	//		-	s is the number of seconds to use
	//		-	d will receive the date
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Convert from seconds to days
	*d = s/SECINDAY;
	
	return S_OK;
	}	// fromSeconds

HRESULT adtDate :: fromString ( const WCHAR *s, ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts the string to its value.
	//
	//	PARAMETERS
	//		-	s is the string
	//		-  v will receive the date value 
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr	= S_OK;
	adtString	str(s);
	int			i;

	// Support loading doubles as dates as well.  We will use the colon (:) or slash (/)
	// to determine if the format is a date or a double
	for (i = 0;str.at(i) != WCHAR('\0');++i)
		if (	str.at(i) == WCHAR(':') ||
				str.at(i) == WCHAR('/') ) break;

	// Convert
	v.vtype	= VTYPE_DATE;
	v.vdate	= 0;
	if (str.at(i) != WCHAR('\0'))
		{
		time_t		t	= 0;
		S32			us	= 0;
		int			i;
		struct tm	tm;
		const WCHAR	*c;

		// Setup.  The default should be the system's 'zero' date, Jan 1, 2001.
		memset ( &tm, 0, sizeof(tm) );
		tm.tm_mon	= 0;										// Jan
		tm.tm_mday	= 1;										// 1
		tm.tm_year	= 101;									// 2001
		v.vdate		= 0.0;

		// Parse optional date
		for (i = 0;hr == S_OK && str[i] != WCHAR('\0');++i)
			if (str[i] == WCHAR('/'))
				break;

		// If a 'slash' was found then assume date has been specified
		if (hr == S_OK && str[i] != WCHAR('\0'))
			{
			// Month
			c = &(str[i]);
			if ((c-2) >= str)
				tm.tm_mon = wcstoul ( c-2, NULL, 10 );
			else if ((c-1) >= str)
				tm.tm_mon = wcstoul ( c-1, NULL, 10 );
			else
				hr = E_INVALIDARG;

			// Month is 0-11 for API
			CCLOK ( tm.tm_mon--; )

			// Day
			CCLOK ( tm.tm_mday = wcstoul ( ++c, NULL, 10 ); )

			// Next '/' (required)
			for (;hr == S_OK && (*c) != WCHAR('\0');++c)
				if ((*c) == WCHAR('/')) break;
			if (hr == S_OK && (*c) == WCHAR('\0'))
				hr = E_INVALIDARG;
			if (hr == S_OK && *(c+1) == WCHAR('\0'))
				hr = E_INVALIDARG;

			// Year
			CCLOK ( tm.tm_year = wcstoul ( c+1, NULL, 10 ) - 1900; )
			}	// if

		// Parse optional time
		for (i = 0;hr == S_OK && str[i] != WCHAR('\0');++i)
			if (str[i] == WCHAR(':')) break;

		// If a 'colon' was found then assume time has been specified
		if (hr == S_OK && str[i] != WCHAR('\0'))
			{
			// Hours
			c = &(str[i]);
			if ((c-2) >= str)
				tm.tm_hour = wcstoul ( c-2, NULL, 10 );
			else if ((c-1) >= str)
				tm.tm_hour = wcstoul ( c-1, NULL, 10 );
			else
				hr = E_INVALIDARG;

			// Minutes
			CCLOK ( tm.tm_min = wcstoul ( ++c, NULL, 10 ); )

			// Next ':' (required)
			for (;hr == S_OK && (*c) != WCHAR('\0');++c)
				if ((*c) == WCHAR(':')) break;
			if (hr == S_OK && (*c) == WCHAR('\0'))
				hr = E_INVALIDARG;
			if (hr == S_OK && *(c+1) == WCHAR('\0'))
				hr = E_INVALIDARG;

			// Seconds
			CCLOK ( tm.tm_sec = wcstoul ( c+1, NULL, 10 ); )

			// Fractions of a second (after decimal point) (optional)
			for (;hr == S_OK && (*c) != WCHAR('\0');++c)
				if ((*c) == WCHAR('.')) break;
			if (hr == S_OK && (*c) == WCHAR('.'))
				{
				// Convert each place to milliseconds
				if (*(c+1) != WCHAR('\0'))
					{
					us += (100000 * CHAR2NUM(*(c+1)));
					if (*(c+2) != WCHAR('\0'))
						{
						us += (10000 * CHAR2NUM(*(c+2)));
						if (*(c+3) != WCHAR('\0'))
							{
							us += (1000*CHAR2NUM(*(c+3)));

							// Sub millisecond
							if (*(c+4) != WCHAR('\0'))
								{
								us += (100*CHAR2NUM(*(c+4)));
								if (*(c+5) != WCHAR('\0'))
									us += (10*CHAR2NUM(*(c+5)));
								}	// if
							}	// if
						}	// if
					}	// if

				// Debug
//				lprintf ( LOG_DBG, L"Fractions of a second : %s : us %d\r\n", c, us );
				}	// if

			}	// if

		// Debug
//		lprintf ( LOG_DBG, L"tm : %d/%d/%d %d:%02d:%02d\n",
//					tm.tm_mon, tm.tm_mday, tm.tm_year,
//					tm.tm_hour, tm.tm_min, tm.tm_sec );

		// Convert # of seconds in Jan. 1, 1970
		CCLTRYE ( ((t = mktime ( &tm )) != -1), E_INVALIDARG );

		// GMT
		#ifdef	_WIN32
		long	timezone = 0;
		CCLOK ( _get_timezone ( &timezone ); )
		#endif
		CCLOK ( t -= timezone; )

		// Convert from epoch to system zero
		CCLTRY ( adtDate::fromEpochSeconds ( t, us, &(v.vdate) ) );

		// Debug
//		lprintf ( LOG_DBG, L"0x%x : %d/%d/%d %d:%02d:%02d : %ld seconds %d us (%lf)\n",
//					hr, tm.tm_mon, tm.tm_mday, tm.tm_year,
//					tm.tm_hour, tm.tm_min, tm.tm_sec, t, us, v.vdate );

		}	// if
	else
		SWSCANF ( str, L"%lg", &(v.vdate) );

	// Debug ?
	if (hr != S_OK)
		lprintf ( LOG_WARN, L"Unable to convert string to date %s\r\n", s );

	return hr;
	}	// fromString

#ifdef	_WIN32
HRESULT adtDate :: fromSystemTime ( SYSTEMTIME *st, DATE *date )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts the SYSTEM time structure to a variant date.	We cannot use
	//			Variant functions only because they ignore milliseconds.
	//
	//	PARAMETERS
	//		-	st is the system time
	//		-	date will receive the date
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	struct tm	tm;
	time_t		t;

	// Fill out time structure from system time structure
	memset ( &tm, 0, sizeof(tm) );
	tm.tm_sec	= st->wSecond;
	tm.tm_min	= st->wMinute;
	tm.tm_hour	= st->wHour;
	tm.tm_mday	= st->wDay;
	tm.tm_mon	= st->wMonth-1;
	tm.tm_year	= st->wYear - 1900;

	// Convert # of seconds in Jan. 1, 1970
	CCLTRYE ( ((t = mktime ( &tm )) != -1), E_INVALIDARG );

	// GMT
	#ifdef	_WIN32
	long	timezone = 0;
	CCLOK ( _get_timezone ( &timezone ); )
	#endif
	CCLOK ( t -= timezone; )

	// Convert from epoch to system zero
	CCLTRY ( adtDate::fromEpochSeconds ( t, st->wMilliseconds*1000, date ) );

	// Add in milliseconds
	CCLOK ( *date += (((double)(st->wMilliseconds))/(1000.0*SECINDAY)); )

	return hr;
	}	// fromSystemTime
#endif

adtDate &adtDate :: now ( bool bLocal )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Initialize object with current timestamp.
	//
	//	PARAMETERS
	//		-	bLocal to get the local time, false for GMT
	//
	//	RETURN VALUE
	//		Reference to the value
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef _WIN32
	SYSTEMTIME	st;

	// Current time
	if (bLocal == false) GetSystemTime ( &st );
	else						GetLocalTime ( &st );

	// Convert to date
	adtDate::fromSystemTime ( &st, &vdate );

	#elif __APPLE__ || __unix__
	struct timespec 	ts;

	// System wide clock
	clock_gettime ( CLOCK_REALTIME, &ts );

	// Need local time ?
	if (bLocal)
		{
		struct tm	tm;

		// Re-entrant/re-startable version, possible issues with 'localtime'
		// sometimes not giving the correct time in a system (not thread safe ?)
		localtime_r ( &(ts.tv_sec), &tm );

		// Convert to seconds
		ts.tv_sec = timegm(&tm);
		}	// if

	// Convert from epoch seconds to system zero
	adtDate::fromEpochSeconds ( ts.tv_sec, ts.tv_nsec/1000, &vdate );
	#endif
	
	return *this;
	}	// now

HRESULT adtDate :: toEpochSeconds ( DATE d, time_t *ps, S32 *pus )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts a date to # of seconds (and microseconds) 
	//			since Jan 1, 1970.
	//
	//	PARAMETERS
	//		-	d is the date
	//		-	ps will receive the number of seconds since Jan 1, 1970
	//		-	pus will receive the number of microseconds
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Offset from 2001 days into 1970 days
	d += DAY20011970;

	// Calculate whole # of seconds
	*ps = (time_t) (d*SECINDAY);

	// Sub-second remainder
	d = (d - (*ps/SECINDAY));

	// To seconds
	d *= SECINDAY;

	// Invert if negative for proper calculation (for dates < Jan 1, 2001)
	if (d < 0)
		d += 1.0;

	// To microseconds
	*pus = (S32) (d*1000000.0);

	return S_OK;
	}	// toEpochSeconds

HRESULT adtDate :: toString ( const ADTVALUE &v, adtString &s )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts the value to its string representation.
	//
	//	PARAMETERS
	//		-	v is the value to convert
	//		-	s will receive the representation
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Date ?
	if (v.vtype != VTYPE_DATE)
		return E_INVALIDARG;

	// Format is DD/MM/YYYY HH:MM:SS.FFF
	#define		DATESTRLEN		24

	#if	_WIN32
	SYSTEMTIME	st;
//	double		dbl;

	// Convert to internal reference (toSystemTime does this)
//	dbl = refToVar(v.vdate);

	// VarBstrFromDate is not flexible for us due to needing fractions
	// of a second.
	if (hr == S_OK) hr = s.allocate ( DATESTRLEN );
//	if (hr == S_OK) hr = adtDate::toSystemTime ( dbl, &st );
	if (hr == S_OK) hr = adtDate::toSystemTime ( v.vdate, &st );
	if (hr == S_OK)
		{
		swprintf ( &s.at(0), DATESTRLEN,
						L"%02d/%02d/%04d %02d:%02d:%02d.%03d",
						st.wMonth, st.wDay, st.wYear,
						st.wHour, st.wMinute, st.wSecond,
						st.wMilliseconds );
		}	// if
	#elif defined(__APPLE__) || defined(__unix__)
	time_t		sec;
	S32			usec;
	struct tm	*ptm 	= NULL;
	time_t		tt		= 0;

	// Space for the string
	CCLTRY ( s.allocate ( DATESTRLEN ) );

	// Obtain time_t value (Epoch seconds)
	CCLTRY ( adtDate::toEpochSeconds ( v.vdate, &sec, &usec ) );

	// Break time into its components
	CCLOK ( tt = sec; )
	CCLTRYE ( ((ptm = gmtime ( &tt )) != NULL),
					E_UNEXPECTED );
	if (hr == S_OK)
		{
		swprintf ( &s.at(0), DATESTRLEN,
						L"%02d/%02d/%04d %02d:%02d:%02d.%03d",
						ptm->tm_mon+1, ptm->tm_mday, ptm->tm_year+1900,
						ptm->tm_hour, ptm->tm_min, ptm->tm_sec,
						(usec/1000) );

//swprintf ( &s.at(0), DATESTRLEN, L"%g", v.vdate );
		}	// if
	#endif

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Error converting to string 0x%x,%g\r\n", hr, v.vdate );

	return hr;
	}	// toString

#ifdef	_WIN32
HRESULT adtDate :: toSystemTime ( double date, SYSTEMTIME *st )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Converts the date to a SYSTEM time structure.  We cannot use
	//			Variant functions only because they ignore milliseconds.
	//
	//	PARAMETERS
	//		-	date is the date to use
	//		-	st will receive the system time
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	struct tm	tm;
	time_t		s;
	S32			us;
	int			err;

	// To epoch secconds
	CCLTRY ( adtDate::toEpochSeconds ( date, &s, &us ) );

	// Break epoch seconds into components
	CCLTRYE ( ((err = gmtime_s ( &tm, &s )) == 0), err );

	// Fill out system time structure
	memset ( st, 0, sizeof(SYSTEMTIME) );
	if (hr == S_OK)
		{
		st->wSecond			= tm.tm_sec;
		st->wMinute			= tm.tm_min;
		st->wHour			= tm.tm_hour;
		st->wDay				= tm.tm_mday;
		st->wMonth			= tm.tm_mon+1;
		st->wYear			= tm.tm_year + 1900;
		st->wDayOfWeek		= tm.tm_wday;
		st->wMilliseconds	= (WORD)(us/1000);
		}	// if

	return hr;
	}	// toSystemTime
#endif

//
// Operators
//

adtDate& adtDate::operator= ( const ADTVALUE &v )
	{
	adtValue::clear(*this);
	vtype	= VTYPE_DATE;
	if			(v.vtype == VTYPE_DATE)					vdate	= v.vdate;
	else if	(v.vtype == VTYPE_R4)					vdate	= v.vflt;
	else if	(v.vtype == VTYPE_R8)					vdate	= v.vdbl;
	else if	(adtValue::type(v) == VTYPE_STR)		fromString ( v.pstr, *this );
	else														vdate	= 0;
	return *this;
	}	// operator=


