////////////////////////////////////////////////////////////////////////
//
//								Display.CPP
//
//				Implementation of the display node
//
////////////////////////////////////////////////////////////////////////

#include "medial_.h"

Display :: Display ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////

	// Setup
	iIdx	= 0;
	memset ( &dd, 0, sizeof(dd) );
	}	// Display

HRESULT Display :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
//		adtValue vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"Index"), vL ) == S_OK)
//			adtValue::toString(vL,strLoc);
		}	// if

	// Detach
	else
		{
		// Clean up
		}	// else

	return hr;
	}	// onAttach

HRESULT Display :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// First/next display
	if (_RCP(First))
		{
		// Reset enumeration index
		iIdx = 0;

		// Proceed to next monitor
		onReceive(prNext,v);
		}	// if
	else if (_RCP(Next))
		{
		adtString	strName;
		DEVMODE		mode;
		U32			i;

		// Next index available ?
		memset ( &dd, 0, sizeof(dd) );
		dd.cb = sizeof(dd);
		CCLTRYE ( EnumDisplayDevices ( NULL, iIdx, &dd, 0 ) == TRUE,
						ERROR_NOT_FOUND );
		CCLOK ( lprintf ( LOG_DBG, L"Device %d) : %s\r\n", iIdx, dd.DeviceName ); )

		// DEBUG.  Dump possible mods to debug.
		for (i = 0;hr == S_OK;++i)
			{
			// Next display mode
			memset ( &mode, 0, sizeof(mode) );
			mode.dmSize = sizeof(mode);
			if (!EnumDisplaySettingsEx ( dd.DeviceName, i, &mode, EDS_RAWMODE ))
				break;
			CCLOK ( lprintf ( LOG_DBG, L"%d) %dx%dx%d@%d\r\n", i, 
									mode.dmPelsWidth, mode.dmPelsHeight, mode.dmBitsPerPel, 
									mode.dmDisplayFrequency ); )
			}	// for

		// Next display
		if (hr == S_OK)
			iIdx = iIdx+1;

		// Result
		if (hr == S_OK)
			_EMT(Next,(strName = dd.DeviceName));
		else 
			_EMT(End,adtInt());
		}	// else if

	// Change
	else if (_RCP(Change))
		{
		DEVMODE	mode;
		U32		i;

		// State check
		CCLTRYE ( (wcslen(dd.DeviceName) > 0), ERROR_INVALID_STATE );

		// Search the display modes that most closely matches
		// the current parameters
		for (i = 0;hr == S_OK;++i)
			{
			// Next display mode
			memset ( &mode, 0, sizeof(mode) );
			mode.dmSize = sizeof(mode);
			CCLTRYE ( EnumDisplaySettingsEx ( dd.DeviceName, i, &mode, EDS_RAWMODE ) == TRUE,
							ERROR_NOT_FOUND );

			// Does this mode match current state ?
			if (	(iW == 0		|| (iW == mode.dmPelsWidth)) &&
					(iH == 0		|| (iH == mode.dmPelsHeight)) &&
					(iBpp == 0	|| (iBpp == mode.dmBitsPerPel)) &&
					(fRate == 0 || (fRate == mode.dmDisplayFrequency)) )
				{
				CCLOK ( lprintf ( LOG_DBG, L"Selected %d) %dx%dx%d@%d\r\n", i, 
										mode.dmPelsWidth, mode.dmPelsHeight, mode.dmBitsPerPel, 
										mode.dmDisplayFrequency ); )
				break;
				}	// if
			}	// for

		// Valid mode found ?
		if (hr == S_OK)
			{
			// Set display mode
			LONG lRet = ChangeDisplaySettingsEx ( dd.DeviceName, &mode, 
//								NULL, CDS_ENABLE_UNSAFE_MODES, NULL );
								NULL, CDS_RESET, NULL );

			// Debug
			CCLOK ( lprintf ( LOG_DBG, L"%s mode change %dx%dx%d@%d = %d\r\n",
									dd.DeviceName,
									mode.dmPelsWidth, mode.dmPelsHeight, mode.dmBitsPerPel, 
									mode.dmDisplayFrequency, lRet ); )

			// Success ?
			hr = (lRet == DISP_CHANGE_SUCCESSFUL) ? S_OK : lRet;
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Error changing display mode %d\r\n", lRet );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Change,adtInt(i));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else if (_RCP(Width))
		iW = v;
	else if (_RCP(Height))
		iH = v;
	else if (_RCP(Bpp))
		iBpp = v;
	else if (_RCP(Rate))
		fRate = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

