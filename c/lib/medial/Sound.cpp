////////////////////////////////////////////////////////////////////////
//
//								Sound.CPP
//
//						Implementation of the 'Sound' node
//
////////////////////////////////////////////////////////////////////////

#include "medial_.h"

#ifdef	_WIN32

Sound :: Sound ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////

	// Setup
	strLoc	= L"";
	bSync		= false;
	}	// Sound

HRESULT Sound :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Location"), vL ) == S_OK)
			adtValue::toString(vL,strLoc);
		if (pnDesc->load ( adtString(L"Async"), vL ) == S_OK)
			bSync = !adtBool(vL);
		}	// if

	// Detach
	else
		{
		// Clean up
		}	// else

	return hr;
	}	// onAttach

HRESULT Sound :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Sound
	if (_RCP(Fire))
		{
		// State check
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// Play the sound
		CCLOK ( PlaySound ( strLoc, NULL, (bSync) ?  SND_FILENAME : 
									(SND_FILENAME|SND_ASYNC) ); )
		}	// if

	// State
	else if (_RCP(Location))
		hr = adtValue::toString(v,strLoc);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

#endif

