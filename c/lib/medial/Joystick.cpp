////////////////////////////////////////////////////////////////////////
//
//								Joystick.CPP
//
//				Implementation of the joystic/game pad node
//
////////////////////////////////////////////////////////////////////////

#include "medial_.h"
#ifndef	_WIN32
#include	<unistd.h>
#include <sys/time.h>
#endif

//#define SDL2_FOUND

Joystick :: Joystick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDctJoy	= NULL;
	iIdx		= 0;
	iRate		= 1000;
	pThrd		= NULL;
	bRun		= false;
	#ifdef  	SDL2_FOUND
	pJoy		= NULL;
	#endif
	}	// Joystick

HRESULT Joystick :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Index"), vL ) == S_OK)
			iIdx = vL;
		if (pnDesc->load ( adtString(L"Rate"), vL ) == S_OK)
			iRate = vL;

		// State dictionary
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctJoy ) );
		}	// if

	// Detach
	else
		{
		// Shutdown worker thread if necessary
		onReceive(prStop,adtInt());
		_RELEASE(pDctJoy);
		}	// else

	return hr;
	}	// onAttach

HRESULT Joystick :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Start polling
	if (_RCP(Start))
		{
		// State check
		CCLTRYE ( pThrd == NULL, ERROR_INVALID_STATE );

		// Start asynchronous polling
		CCLOK(bRun = true;)
		CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
		CCLTRY(pThrd->threadStart ( this, 5000 ));
		}	// if

	// Stop polling
	else if (_RCP(Stop))
		{
		// Shutdown worker thread if necessary
		if (pThrd != NULL)
			{
			pThrd->threadStop(10000);
			_RELEASE(pThrd);
			}	// if
		}	// else if

	// State
	else if (_RCP(Rate))
		iRate = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Joystick :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	bRun = false;
	return S_OK;
	}	// tickAbort

HRESULT Joystick :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	DWORD		dwNow,dwThen,dwPkt,dwDiff;

	// Poll/emit joystick input
	dwPkt		= 0;
	dwThen	= tickCount();
	while (bRun)
		{
		// Poll
		#ifdef 	_WIN32
		// Wait for remaining poll time
		dwNow = tickCount();
		if (dwNow > dwThen)
			{
			// Delta time
			dwDiff = (dwNow-dwThen);

			// Wait if necessary
			if (dwDiff < iRate)
				{
				#ifdef 	_WIN32
				Sleep(iRate-dwDiff);
				#else
				usleep((iRate-dwDiff)*1000);
				#endif
				}	// if
			}	// if

		XINPUT_STATE	state;
		XINPUT_GAMEPAD	*pad = &(state.Gamepad);
		memset ( &state, 0, sizeof(state) );
		if (	XInputGetState ( iIdx, &state ) == ERROR_SUCCESS &&
				state.dwPacketNumber != dwPkt)
			{
			// Fill in state information

			// DPAD
			pDctJoy->store ( adtString(L"PadUp"), adtBool( (pad->wButtons & 0x1) != 0 ) );
			pDctJoy->store ( adtString(L"PadDown"), adtBool( (pad->wButtons & 0x2) != 0 ) );
			pDctJoy->store ( adtString(L"PadLeft"), adtBool( (pad->wButtons & 0x4) != 0 ) );
			pDctJoy->store ( adtString(L"PadRight"), adtBool( (pad->wButtons & 0x8) != 0 ) );
			pDctJoy->store ( adtString(L"PadStart"), adtBool( (pad->wButtons & 0x10) != 0 ) );
			pDctJoy->store ( adtString(L"PadBack"), adtBool( (pad->wButtons & 0x20) != 0 ) );
			pDctJoy->store ( adtString(L"ThumbLeft"), adtBool( (pad->wButtons & 0x40) != 0 ) );
			pDctJoy->store ( adtString(L"ThumbRight"), adtBool( (pad->wButtons & 0x80) != 0 ) );
			pDctJoy->store ( adtString(L"ShoulderLeft"), adtBool( (pad->wButtons & 0x100) != 0 ) );
			pDctJoy->store ( adtString(L"ShoulderRight"), adtBool( (pad->wButtons & 0x200) != 0 ) );
			pDctJoy->store ( adtString(L"PadA"), adtBool( (pad->wButtons & 0x1000) != 0 ) );
			pDctJoy->store ( adtString(L"PadB"), adtBool( (pad->wButtons & 0x2000) != 0 ) );
			pDctJoy->store ( adtString(L"PadC"), adtBool( (pad->wButtons & 0x4000) != 0 ) );
			pDctJoy->store ( adtString(L"PadD"), adtBool( (pad->wButtons & 0x8000) != 0 ) );

			pDctJoy->store ( adtString(L"TriggerLeft"),	adtFloat(pad->bLeftTrigger/255.0f) );
			pDctJoy->store ( adtString(L"TriggerRight"), adtFloat(pad->bRightTrigger/255.0f) );

			pDctJoy->store ( adtString(L"ThumbLeftX"), adtFloat(pad->sThumbLX/32768.0f) );
			pDctJoy->store ( adtString(L"ThumbLeftY"), adtFloat(pad->sThumbLY/32768.0f) );
			pDctJoy->store ( adtString(L"ThumbRightX"), adtFloat(pad->sThumbRX/32768.0f) );
			pDctJoy->store ( adtString(L"ThumbRightY"), adtFloat(pad->sThumbRY/32768.0f) );

			// Result
			_EMT(Fire,adtIUnknown(pDctJoy));

			// Active packet number
			dwPkt = state.dwPacketNumber;
			}	// if
		#endif
		#ifdef 	SDL2_FOUND
		SDL_Event	event;
		int			ret;

		// SDL polling, again need to share with other nodes using SDL (none yet)
		while ((ret = SDL_WaitEventTimeout(&event,1000)))
//		while (SDL_PollEvent(&event))
			{
			// Start with clear dictionary
			CCLTRY ( pDctJoy->clear() );

			// Process event
			switch (event.type)
				{
				// Analog controls
				case SDL_JOYAXISMOTION :
					{
					// Dealing with minimum threshold for analog movement is the
					// responsibility of the graph

					// Normalize to -1 to +1
					// Values coming from SDL are -32768 to +32767
					float 		fVal = (event.jaxis.value/32768.0);
					float 		fMul = +1.0f;
					adtString 	strKey;

					// Copy event names used above, need to make it more general.
					// Y-axis seems inverted to Windows.
					switch (event.jaxis.axis)
						{
						case 0 : strKey = L"ThumbLeftX"; break;
						case 1 : strKey = L"ThumbLeftY"; fMul = -1.0f; break;
						case 2 : strKey = L"ThumbLeft"; break;
						case 3 : strKey = L"ThumbRightX"; break;
						case 4 : strKey = L"ThumbRightY"; fMul = -1.0f; break;
						case 5 : strKey = L"ThumbRight"; break;
						}	// switch

					// Update event dictionary
					if (strKey.length() > 0)
						hr = pDctJoy->store(strKey,adtFloat(fMul*fVal));

//					lprintf ( LOG_DBG, L"Joystick motion, axis %d, value %d\r\n", 
//									event.jaxis.axis, event.jaxis.value );
					}	// SDL_JOYAXISMOTION
					break;					


				// Button events
				case SDL_JOYBUTTONDOWN :
				case SDL_JOYBUTTONUP :
					{
					adtString 	strKey;

					// Map to names
					switch (event.jbutton.button)
						{
						case 0 : strKey = L"PadA"; break;
						case 1 : strKey = L"PadB"; break;
						case 2 : strKey = L"PadC"; break;
						case 3 : strKey = L"PadD"; break;
						case 4 : strKey = L"ShoulderLeft"; break;
						case 5 : strKey = L"ShoulderRight"; break;
						case 6 : strKey = L"PadBack"; break;
						case 7 : strKey = L"PadStart"; break;
						case 9 : strKey = L"ThumbLeft"; break;
						case 10 : strKey = L"ThumbRight"; break;
						}	// switch

					// Update event dictionary
					if (strKey.length() > 0)
						hr = pDctJoy->store(strKey,adtBool(event.type == SDL_JOYBUTTONDOWN));

//					lprintf ( LOG_DBG, L"Joystick button, %d, button %d\r\n", 
//									event.type, event.jbutton.button );
					}	// case SDL_JOYBUTTONXX
					break;

				// Joystick devices
				case SDL_JOYDEVICEADDED :
					// Index match joystick node is using ? (TODO: Other indicies)
					lprintf ( LOG_DBG, L"Joystick added, index %d\r\n", event.jdevice.which );
					if (event.jdevice.which == 0)
						{
						// Previous joystick
						if (pJoy != NULL)
							SDL_JoystickClose(pJoy);

						// New joystick
						if ( (pJoy = SDL_JoystickOpen(event.jdevice.which)) != NULL )
							lprintf ( LOG_DBG, L"Joystick %d opened\r\n", event.jdevice.which );
						else
							lprintf ( LOG_DBG, L"Unable to open joystick %d\r\n", event.jdevice.which );
						}	// if
					break;
				case SDL_JOYDEVICEREMOVED :
					{
					// Instance Id match joystick node is using ? (TODO: Other indicies)
					lprintf ( LOG_DBG, L"Joystick removed, instance %d\r\n", event.jdevice.which );
					if (event.jdevice.which == 0 && pJoy != NULL)
						{
						// Clean up own joystick
						SDL_JoystickClose(pJoy);
						pJoy = NULL;
						}	// if
					}	// SDL_JOYDEVICEREMOVED
					break;

				default :
					lprintf ( LOG_DBG, L"Unhandled event 0x%x(%d)\r\n", event.type, event.type );
				}	// switch

			// Event
			if (hr == S_OK && pDctJoy->isEmpty() == S_FALSE)
				_EMT(Fire,adtIUnknown(pDctJoy));
			}	// while
		#endif

		// Next
		dwThen = dwNow;
		}	// while

	// More ?
	CCLTRYE ( bRun == true, S_FALSE );

	return hr;
	}	// tick

HRESULT Joystick :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	#ifdef 	SDL2_FOUND
	int ret;

	// How to work with other nodes using SDL if init is supposed to be
	// called only once ?  Reference counting ?
	CCLTRYE ( ((ret = SDL_Init ( SDL_INIT_JOYSTICK )) >= 0), E_UNEXPECTED );

	// Enable joystick event polling
	CCLOK ( SDL_JoystickEventState(SDL_ENABLE); )

	// Debug
	if (hr == S_OK)
		lprintf ( LOG_DBG, L"SDL_NumJoysticks : %d\r\n", SDL_NumJoysticks() );

	// Attempt to access pre-existing joystick, ok to fail, main tick
	// will handle arrival of new joysticks
	CCLOK ( pJoy = SDL_JoystickOpen(0); )

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Unable to initialize SDL, ret %d\r\n", ret );
	#endif

	return hr;
	}	// tickBegin

U32 Joystick :: tickCount ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Returns the # of milliseconds elapsed since an arbitrary time.
	//
	//	RETURN VALUE
	//		# of milliseconds since 't zero'
	//
	////////////////////////////////////////////////////////////////////////
	U32	ret = 0;

	#ifdef	_WIN32
	ret = GetTickCount();
	#endif

	#if	defined(__unix__) || defined(__APPLE__)
	struct timeval		now;

	// Compute relative elapsed time since t0
	if (gettimeofday ( &now, NULL ) == 0)
		{
		// Watch for negative usec
		if (now.tv_usec > t0.tv_usec)
			ret = (now.tv_usec - t0.tv_usec) / 1000;
		else
			ret = 1000 - ((t0.tv_usec - now.tv_usec)/1000);

		// Seconds
		ret += (now.tv_sec - t0.tv_sec) * 1000;
		}	// if
	#endif

	return ret;
	}	// tickCount

HRESULT Joystick :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef 	SDL2_FOUND
	if (pJoy != NULL)
		{
		SDL_JoystickClose(pJoy);
		pJoy = NULL;
		}
	SDL_Quit();
	#endif

	return S_OK;
	}	// tickEnd

