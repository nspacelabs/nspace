////////////////////////////////////////////////////////////////////////
//
//										MEDIAL_.H
//
//				Implementation include file for the media library
//
////////////////////////////////////////////////////////////////////////

#ifndef	MEDIAL__H
#define	MEDIAL__H

// Includes
#include "medial.h"

#ifdef	_WIN32
// SAPI
#include	<sapi.h>

// DirectX
#include	<xinput.h>
#endif

// SDL2
#ifdef 	SDL2_FOUND
#include	<SDL2/SDL.h>
#endif

///////////
// Objects
///////////

/////////
// Nodes
/////////

#ifdef	_WIN32

//
// Class - Display.  Node for querying and manipulating dispays
//

class Display :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Display ( void );										// Constructor

	// Run-time data
	DISPLAY_DEVICE dd;									// Lastest device info
	U32				iIdx;									// Enumeration index
	adtInt			iW,iH,iBpp;							// Display mode
	adtFloat			fRate;								// Display mode

	// CCL
	CCL_OBJECT_BEGIN(Display)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(First)
	DECLARE_CON(Next)
	DECLARE_EMT(End)
	DECLARE_CON(Change)
	DECLARE_EMT(Error)
	DECLARE_RCP(Width)
	DECLARE_RCP(Height)
	DECLARE_RCP(Bpp)
	DECLARE_RCP(Rate)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(First)
		DEFINE_CON(Next)
		DEFINE_EMT(End)
		DEFINE_CON(Change)
		DEFINE_EMT(Error)
		DEFINE_RCP(Width)
		DEFINE_RCP(Height)
		DEFINE_RCP(Bpp)
		DEFINE_RCP(Rate)
	END_BEHAVIOUR_NOTIFY()
	};

#endif

//
// Class - Joystick.  Node for a joystick/gamepad.
//

class Joystick :
	public CCLObject,										// Base class
	public Behaviour,										// Interface
	public ITickable										// Interface
	{
	public :
	Joystick ( void );									// Constructor

	// Run-time data
	adtInt		iIdx;										// Joystick/pad index
	adtInt		iRate;									// Polling rate
	IThread		*pThrd;									// Asynchronous read thread
	bool			bRun;										// Async should run ?
	IDictionary	*pDctJoy;								// State

	// 'ITickable' members
	STDMETHOD(tick)		( void );
	STDMETHOD(tickAbort)	( void );
	STDMETHOD(tickBegin)	( void );
	STDMETHOD(tickEnd)	( void );

	// CCL
	CCL_OBJECT_BEGIN(Joystick)
		CCL_INTF(IBehaviour)
		CCL_INTF(ITickable)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Start)
	DECLARE_RCP(Stop)
	DECLARE_RCP(Rate)
	DECLARE_EMT(Error)
	DECLARE_EMT(Fire)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Start)
		DEFINE_RCP(Stop)
		DEFINE_RCP(Rate)
		DEFINE_EMT(Error)
		DEFINE_EMT(Fire)
	END_BEHAVIOUR_NOTIFY()

	private :

	// Run-time data
	#if					__unix__ || __APPLE__
	struct timeval		t0;								// Reference time
	#endif
	#if  SDL2_FOUND
	SDL_Joystick 	*pJoy;								// Open joystick
	#endif

	// Internal utlities
	U32 tickCount ( void );
	};

#ifdef	_WIN32

//
// Class - Sound.  Node for playing sounds.
//

class Sound :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Sound ( void );										// Constructor

	// Run-time data
	adtString	strLoc;									// Filename location
	adtBool		bSync;									// Synchronous playback ?

	// CCL
	CCL_OBJECT_BEGIN(Sound)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	DECLARE_RCP(Location)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
		DEFINE_RCP(Location)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Speak.  Client node for generating plot Speaks.
//								(Currently uses Speak).
//

class Speak :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Speak ( void );										// Constructor

	// Run-time data
	adtValue		vSpk;										// Speak value
	ISpVoice		*pVoice;									// SAPI voice

	// CCL
	CCL_OBJECT_BEGIN(Speak)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	DECLARE_RCP(Value)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
		DEFINE_RCP(Value)
	END_BEHAVIOUR_NOTIFY()
	};
#endif

#endif
