////////////////////////////////////////////////////////////////////////
//
//									MAINDEF.CPP
//
//						Default main for nSpace.
//
////////////////////////////////////////////////////////////////////////

#include "nshl_.h"
#ifdef	_WIN32
#include <commctrl.h>
#endif
#if     __unix__ || __APPLE__
#include <stdio.h>
#include <signal.h>
#include <execinfo.h>
#include <fcntl.h>
#endif

// Globals
bool bShellTick	= false;								// Global ticking enable

// Prototypes
#if	__unix__ || __APPLE__
void handler_sigint	( int );
void handler_sigsegv ( int );
void handler_sigabrt ( int );
void handler_sigbus	( int );
#endif

HRESULT WINAPI defMain ( IDictionary *pDctCmd )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Entry point into program.
	//
	//	PARAMETERS
	//		-	pDctCmd contains command line information
	//
	//	RETURN VALUE	
	//		0 on failure
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	Shell		*pShell	= NULL;

	// Initialize common controls for Win32
	#if	!defined(__NOGDI__) || (UNDER_CE >= 400)
	INITCOMMONCONTROLSEX	icce;
	memset ( &icce, 0, sizeof(icce) );
	icce.dwSize = sizeof(icce);
	icce.dwICC	= ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx ( &icce );
	#endif

	#if	__unix__ || __APPLE__
	sigset_t		set;

	// Ignore some pointless unix signals so it does not exit over a simple error
	// (e.g. a TCP connection is closed).
	sigemptyset ( &set );
	sigaddset	( &set, SIGPIPE );
	sigprocmask	( SIG_BLOCK, &set, NULL );
	signal 		( SIGPIPE, SIG_IGN );

	// Install the handler for Ctrl-C and terminiation.  This is how to exit.
	signal ( SIGINT,	handler_sigint );
	signal ( SIGTERM, handler_sigint );

	// Install handler for segmentation fault.
	signal ( SIGSEGV, handler_sigsegv );
	signal ( SIGABRT, handler_sigabrt );
	signal ( SIGBUS, handler_sigbus );
	#endif

	// Shell object
	CCLTRYE	( (pShell = new Shell(pDctCmd)) != NULL, E_OUTOFMEMORY );
	CCLOK		( pShell->AddRef(); )
	CCLTRY	( pShell->construct() );
	CCLOK		( bShellTick = true; )

	// Run shell
	if (hr == S_OK && pShell->tickBegin() == S_OK)
		{
		// Continue until shell exits
		while (pShell->tick() == S_OK && bShellTick)
			{
//				lprintf ( LOG_DBG, L"Tick!" );
			}	// while
		}	// if

	// Clean up
	if (pShell != NULL)
		{
		pShell->tickEnd();
		_RELEASE(pShell);
		}	// if

	return hr;
	}	// defMain

// Signal handler
#if	__unix__ || __APPLE__
void handler_sigint ( int signum )
	{
	lprintf ( LOG_INFO, L"Received SIG:%d, signaling graph shutdown\n", signum );
	bShellTick = false;
	}	// handler_sigint

void handler_sigsegv ( int signum )
	{
	void 		*array[100];
	size_t	size;

	// Debug
	lprintf ( LOG_ERR, L"Received SIGSEGV (segmentation fault), backtrace follows\n", signum );

	// get void *'s for all entries on the stack and print out all the frames
	size = backtrace ( array, 100 );
	backtrace_symbols_fd ( array, size, STDERR_FILENO );

	// Attempt to write to file as well
	int fd = open ( "sigsegv.txt", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR );
	if (fd != -1)
		{
		backtrace_symbols_fd ( array, size, fd );
		close(fd);
		}	// if

	exit(1);
	}	// handler_sigsegv

void handler_sigabrt ( int signum )
	{
	void 		*array[100];
	size_t	size;

	// Debug
	lprintf ( LOG_ERR, L"Received SIGABRT (Abort), backtrace follows\n", signum );

	// get void *'s for all entries on the stack and print out all the frames
	size = backtrace ( array, 100 );
	backtrace_symbols_fd ( array, size, STDERR_FILENO );

	// Attempt to write to file as well
	int fd = open ( "sigabrt.txt", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR );
	if (fd != -1)
		{
		backtrace_symbols_fd ( array, size, fd );
		close(fd);
		}	// if

	exit(1);
	}	// handler_sigabrt

void handler_sigbus ( int signum )
	{
	void 		*array[100];
	size_t	size;

	// Debug
	lprintf ( LOG_ERR, L"Received SIGBUS (Bus error), backtrace follows\n", signum );

	// get void *'s for all entries on the stack and print out all the frames
	size = backtrace ( array, 100 );
	backtrace_symbols_fd ( array, size, STDERR_FILENO );

	// Attempt to write to file as well
	int fd = open ( "sigbus.txt", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR );
	if (fd != -1)
		{
		backtrace_symbols_fd ( array, size, fd );
		close(fd);
		}	// if

	exit(1);
	}	// handler_sigbus

#endif

