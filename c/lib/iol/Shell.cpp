////////////////////////////////////////////////////////////////////////
//
//									SHELL.CPP
//
//					Implementation of the shell node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>
#ifdef	_WIN32
#include <shellapi.h>
#include <ShlObj.h>
#endif

Shell :: Shell ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	strFile = L"";
	}	// Shell

HRESULT Shell :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	adtIUnknown		unkV;

	// Attaching to graph
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"File"), vL ) == S_OK)
			adtValue::toString ( vL, strFile );
		}	// if

	// Detaching from graph
	else
		{
		}	// else

	return hr;
	}	// onAttach

HRESULT Shell :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute file
	if (_RCP(Execute))
		{
		// State check
		CCLTRYE( strFile.length() > 0, ERROR_INVALID_STATE);

		// Execute
		#ifdef	_WIN32
		HINSTANCE ret = 0;
		CCLOK ( ret = ShellExecute ( NULL, L"open", strFile, NULL, 
						NULL, SW_SHOWNORMAL ); )
		CCLTRYE ( ((S64)ret) > 32, (HRESULT)((S64)ret) );
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Execute,strFile);
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Get known folder path
	else if (_RCP(QueryPath))
		{
		adtString	strF;
		#ifdef	_WIN32
		int			csidl = 0;
		WCHAR			wFolder[MAX_PATH];

		// State check
		CCLTRYE ( strFile.length() > 0, ERROR_INVALID_STATE );

		// Map to Windows specific Id, add as needed
		CCLOK (	csidl =	(!WCASECMP(strFile,L"Documents")) ? CSIDL_COMMON_DOCUMENTS :
								(!WCASECMP(strFile,L"Personal")) ? CSIDL_PERSONAL :
								(!WCASECMP(strFile,L"Program Files")) ? CSIDL_PROGRAM_FILES :
								(!WCASECMP(strFile,L"Common App Data")) ? CSIDL_COMMON_APPDATA : 0; )
		CCLTRYE ( (csidl != 0), E_NOTIMPL );

		// Retrieve the path
		CCLTRY ( SHGetFolderPath ( NULL, csidl, NULL,
											SHGFP_TYPE_DEFAULT, wFolder ) );

		// nSpace locations use forward slashes and end in a slash
		CCLOK ( strF = wFolder; )
		CCLTRY( strF.append ( L"\\" ) );
		CCLOK ( strF.replace ( '\\', '/' ); )
		#else
		hr = E_NOTIMPL;
		#endif

		// Result
		if (hr == S_OK)
			_EMT(QueryPath,strF);
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else if (_RCP(File))
		hr = adtValue::copy(v,strFile);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

