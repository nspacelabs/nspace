////////////////////////////////////////////////////////////////////////
//
//									MEMBLCK.CPP
//
//					Implementation of the memory block object
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#include "iol_.h"
#include <stdio.h>

#define	MEMBLOCK_DEFBLKSIZE		0x01000			// 'In process' memory (4k)
#define	MEMBLOCK_BIGBLKSIZE		0x10000			// 'File mapped' memory (64K)

// Debug
static U64 uAllocdTot = 0;

MemoryBlock :: MemoryBlock ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pcBlk		= NULL;
	szBlk		= 0;
	szAllocd	= 0;
	hMap		= NULL;
	pParent	= NULL;
	iOff		= 0;
	}	// MemoryBlock

MemoryBlock :: MemoryBlock ( IMemoryMapped *pMap, U64 off, U32 sz ) :
	MemoryBlock()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Internal constructor for the object.  Creates a static memory
	//			map around the specified ptr.
	//
	//	PARAMETERS
	//		-	pMap is the parent map this block will depend on.
	//		-	off is the offset into the parent memory
	//		-	sz is the size of the block
	//
	////////////////////////////////////////////////////////////////////////

	// Remember parent
	pParent	= pMap; _ADDREF(pParent);

	// Cache specific setup for constructor
	iOff	= (U32)off;
	szBlk	= sz;
	}	// MemoryBlock

HRESULT MemoryBlock :: clone ( IUnknown **ppUnk )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ICloneable
	//
	//	PURPOSE
	//		-	Clones the object.
	//
	//	PARAMETERS
	//		-	ppUnk will receive the cloned object
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	MemoryBlock	*pDst		= NULL;
	void			*pvDst	= NULL;

	// Cloning a memory block creates a copy of the data.
//	lprintf ( LOG_DBG, L"pParent %p szBlk %d pcBlk %p\r\n", pParent, szBlk, pcBlk );

	// Create another instance of this object
	if (pParent == NULL)
		{
		CCLTRYE	( (pDst = new MemoryBlock()) != NULL, E_OUTOFMEMORY );
		}	// if
	else
		{
		CCLTRYE	( (pDst = new MemoryBlock(pParent,iOff,szBlk)) != NULL, E_OUTOFMEMORY );
		}	// else
	CCLOK		( pDst->AddRef(); )
	CCLTRY	( pDst->construct(); )

	// Valid size ?
	if (hr == S_OK && szBlk > 0 && pParent == NULL)
		{
		// Copy bits
		CCLTRY	( pDst->setSize ( szBlk ) );
		CCLTRY	( pDst->getInfo ( &pvDst, NULL ) );
		CCLOK		( memcpy ( pvDst, pcBlk, szBlk ); )
		}	// if

	// Result
	if (hr == S_OK)
		{
		(*ppUnk) = (IMemoryMapped *) pDst;
		_ADDREF((*ppUnk));
		}	// if

	// Clean up
	_RELEASE(pDst);

	return hr;
	}	// clone

HRESULT MemoryBlock :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// If memory block has a parent, perfirm setup
	if (pParent != NULL)
		{
		U8 	*pcBlkP	= NULL;
		U32	sz			= 0;

		// Obtain information about parent
		CCLTRY ( pParent->getInfo ( (void **) &pcBlkP, &sz ) );

		// Starting address
		CCLOK ( pcBlk = &pcBlkP[iOff]; )
		}	// if

	// Error state
	if (hr != S_OK)
		{
		pcBlk = NULL;
		szBlk	= 0;
		}	// if

	return hr;
	}	// construct

void MemoryBlock :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (hMap == NULL)
		{
		if (pParent == NULL)
			{
			// Dynamically allocated ?
			if (szAllocd > 0)
				{
				uAllocdTot -= szAllocd;
	//			lprintf ( LOG_DBG, L"Reduced %d:uAllocdTot %d\r\n", szAllocd, uAllocdTot );
				_FREEMEM(pcBlk);
				}	// if
			}	// if
		}	// if
	else
		{
		#if	defined(_WIN32)
		if (pcBlk != NULL) UnmapViewOfFile ( pcBlk );
		CloseHandle ( hMap );
		#endif
		}	// else
	_RELEASE(pParent);
	}	// destruct

HRESULT MemoryBlock :: getPersistId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IValuePersist
	//
	//	PURPOSE
	//		-	Return an object Id to use for persistence.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return adtValue::copy ( adtString(L"Io.MemoryBlock"), vId );
	}	// getResId

HRESULT MemoryBlock :: getInfo ( void **ppv, U32 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Retrieve pointer and/or the size of the region.
	//
	//	PARAMETERS
	//		-	ppv will receive the ptr.
	//		-	puSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr				= S_OK;

	// Pointer
	if (ppv != NULL)
		{
		// Error on NULL ptr.
		CCLTRYE	( (pcBlk != NULL), E_OUTOFMEMORY );

		// Result
		*ppv = (hr == S_OK) ? pcBlk : NULL;
		}	// if

	// Size
	if (puSz != NULL)	
		*puSz = (hr == S_OK) ? szBlk : 0;

	return hr;
	}	// getInfo

HRESULT MemoryBlock :: setInfo ( void *pv, U32 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Set static information about the memory block, can be
	//			called only one time.
	//
	//	PARAMETERS
	//		-	pv is the ptr. to the memory location
	//		-	uSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr				= S_OK;

	// Current state must be empty.
//	lprintf ( LOG_DBG, L"setInfo %p, %d\r\n", pv, uSz );
	if (pcBlk != NULL || szBlk != 0)
		return ERROR_INVALID_STATE;

	// Cache info.
	pcBlk		= (U8 *) pv;
	szBlk		= uSz;
	szAllocd = 0;											// Sign of a 'static' block

	return hr;
	}	// setInfo

HRESULT MemoryBlock :: setSize ( U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Specifies what size the memory block should be sized too.
	//
	//	PARAMETERS
	//		-	sz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr				= S_OK;
	U32		szAllocdNew	= 0;

	// Allow a 'harmless' call to set the size to the same as current size.
	if (sz == szBlk)
		{
//		lprintf ( LOG_DBG, L"Harmless setSize %d\r\n", sz );
		return S_OK;
		}	// if

	// Not allowed for static map
	if (pParent != NULL || (pcBlk != NULL && szAllocd == 0))
		{
		lprintf ( LOG_DBG, L"Invalid attempt to set size on static map\r\n" );
		return ERROR_INVALID_STATE;
		}	// if

	// Compute the needed size in blocks.  This filters many small increases in requested size.
	szAllocdNew = (sz) ? (((sz/MEMBLOCK_DEFBLKSIZE)+1)*MEMBLOCK_DEFBLKSIZE) : 0;

	// Difference ?
//	lprintf ( LOG_DBG, L"szAllocd %d azAllocdNew %d sz %d\r\n", szAllocd, szAllocdNew, sz );
	if (szAllocdNew != szAllocd)
		{
		U8	*pcBlkNew = NULL;

		// Debug
		uAllocdTot 	-= szAllocd;

		// Reallocate buffer
		if (szAllocdNew > 0)
			{
			CCLTRYE ( (pcBlkNew = (U8 *) _REALLOCMEM ( (hMap == NULL) ? pcBlk : NULL, 
							szAllocdNew )) != NULL, E_OUTOFMEMORY );
			}	// if
		else
			{
//			lprintf ( LOG_DBG, L"setSize 0, freeing memory %p (was %d bytes)\r\n", pcBlk, szAllocd );
			_FREEMEM(pcBlk);
			}	// else

		// New allocated amount
		szAllocd = (hr == S_OK) ? szAllocdNew : 0;

		// New ptr.
		pcBlk 		= pcBlkNew;
		uAllocdTot 	+= szAllocd;

		// Debug
//		lprintf ( LOG_DBG, L"MemoryBlock::setSize:szAllocd %d:szBlk %d:uAllocdTot %d\r\n",
//									szAllocd, szBlk, uAllocdTot );
		}	// if

	// New information
	szBlk = (hr == S_OK)	? sz : 0;

	return hr;
	}	// setSize

HRESULT MemoryBlock :: stream ( IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Creates a byte stream in front of the memory block.
	//
	//	PARAMETERS
	//		-	ppStm is the new stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	StmMemory		*pStm	= NULL;

	// Setup
	(*ppStm) = NULL;

	// Create new stream and assign this object as the block
	CCLTRYE	( (pStm = new StmMemory()) != NULL, E_OUTOFMEMORY );
	CCLOK		( pStm->AddRef(); )
	CCLOK		( pStm->pBlock = this; )
	CCLTRY	( pStm->construct(); )

	// Done
	if (hr == S_OK)
		{
		(*ppStm) = pStm;
		_ADDREF((*ppStm));
		}	// if

	// Clean up
	_RELEASE(pStm);

	return hr;
	}	// stream

		/*
		// NOTE: For Windows CE each process is limited to 32 MB.  Only way
		// around this limitation is to use file-mapping objects.  For 'large'
		// allocations, put memory outside process.  'Large' is arbitrary.
		// WARNING: Resizing large blocks slow due to non-resizable map objects.
		// TODO: Makes sense to do this for large blocks for 'any' OS ?
		#if	defined(_WIN32)
		if (sz >= MEMBLOCK_BIGBLKSIZE)
			{
			HANDLE	hMapNew		= NULL;
			U8			*pcBlkNew	= NULL;

			// Allocate in blocks
			szAllocd = ((sz/MEMBLOCK_BIGBLKSIZE)+1)*MEMBLOCK_BIGBLKSIZE;

			// Create a new file mapping backed by memory
			CCLTRYE ( (hMapNew = CreateFileMapping ( INVALID_HANDLE_VALUE,
							NULL, PAGE_READWRITE, 0, szAllocd, NULL ))
							!= NULL, GetLastError() );

			// Map a view info the file
			CCLTRYE ( (pcBlkNew = (U8 *) MapViewOfFile ( hMapNew,
							FILE_MAP_WRITE, 0, 0, 0 )) != NULL, GetLastError() );

			// If previous buffer exists, copy into new buffer
			if (hr == S_OK && pcBlk != NULL && szBlk)
				memcpy ( pcBlkNew, pcBlk, szBlk );

			// Release previous memory based on type
			if (hMap == NULL)
				{
				_FREEMEM(pcBlk);
				}	// if
			else
				{
				if (pcBlk != NULL) { UnmapViewOfFile ( pcBlk ); pcBlk = NULL; }
				CloseHandle ( hMap );
				hMap = NULL;
				}	// else

			// New block information
			if (hr == S_OK)
				{
				hMap	= hMapNew;
				pcBlk	= pcBlkNew;
				}	// if
			}	// if
		else
			{
			#endif

			#if	defined(_WIN32)
			// If decreasing in size from a large memory mapped file, release it
			if (hr == S_OK && hMap != NULL)
				{
				// Keep contents of block
				if (pcBlk != NULL)
					{
					if (szBlk) memcpy ( pcBlkNew, pcBlk, szBlk );
					UnmapViewOfFile ( pcBlk );
					}	// if
				CloseHandle ( hMap );
				hMap	= NULL;
				}	// if
			#endif
			#if	defined(_WIN32)	
			}	// else
			#endif


*/
