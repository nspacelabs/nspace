////////////////////////////////////////////////////////////////////////
//
//									FILE.CPP
//
//				Implementation of the file I/O node.
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

// Globals
#define	SIZE_FILE_BFR		8192

File :: File ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	#ifdef		_WIN32
	hFile		= INVALID_HANDLE_VALUE;
	hevWr		= NULL;
	hevRd		= NULL;
	hevStop	= NULL;
	#else
	fd			= -1;
	#endif
	pThrd		= NULL;
	pStmIo	= NULL;
	iSzIo		= 0;
	bAsync	= false;
	pcBfr		= NULL;
	iSzBfr	= SIZE_FILE_BFR;
	bRun		= false;
	bWrChk	= true;
	bRdChk	= true;
	uTo		= 1000;
	bDbg		= false;
	}	// File

HRESULT File :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Buffer memory
	CCLTRYE ( (pcBfr = (U8 *) _ALLOCMEM ( iSzBfr )) != NULL, E_OUTOFMEMORY );

	return hr;
	}	// construct

void File :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_FREEMEM(pcBfr);
	}	// destruct

HRESULT File :: fileIo  ( BOOL bWr, DWORD uIo, DWORD uTo, DWORD *puIo )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Unified asynchronous I/O for packet buffer.
	//
	//	PARAMETERS
	//		-	bWr is TRUE to write, FALSE to read.
	//		-	uIo is the size of the requested transfer
	//		-	uTo is the timeout in milliseconds
	//		-	puIo will receive the actual amount transferred.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr 	= S_OK;
	DWORD 		uXfer	= 0;
	#ifdef		_WIN32
	HANDLE		hevs[2]	= { (bWr) ? hevWr : hevRd, hevStop };
	OVERLAPPED	ov;
	DWORD			dwRet;

	// Debug
//	lprintf ( LOG_DBG, L"bWr %d:uIo %d\r\n", bWr, uIo );

	// I/O is always overlapped since that it the way USB devices are opened.
	memset ( &ov, 0, sizeof(ov) );
	ov.hEvent = hevs[0];
	if (hr == S_OK && bWr)
		{
		// Begin a write
		CCLTRYE ( WriteFile ( hFile, pcBfr, uIo, NULL, &ov ) == TRUE, GetLastError() );
		}	// if
	else if (hr == S_OK)
		{
		// Begin a read
		CCLTRYE ( ReadFile ( hFile, pcBfr, uIo, NULL, &ov ) == TRUE, GetLastError() );
		}	// else if

	// I/O is still pending, must wait
	if (hr == ERROR_IO_PENDING)
		{
		// Wait for completion or signal to stop
		hr		= S_OK;
		dwRet = WaitForMultipleObjects ( 2, hevs, FALSE, uTo );

		// Success ?
		if (dwRet != WAIT_OBJECT_0)
			{
			// Stop event detected
			if (dwRet == WAIT_OBJECT_0+1)
				hr = S_FALSE;

			// Timeout
			else if (dwRet == WAIT_TIMEOUT)
				hr = ERROR_TIMEOUT;

			// ??
			else
				hr = GetLastError();
			}	// if

		}	// if

	// Amount transfered
	CCLTRYE ( GetOverlappedResult ( hFile, &ov, &uXfer, TRUE ) == TRUE,
					GetLastError() );

	// Debug
//	dbgprintf ( L"File::fileIo:bWr %d:uXfer %d:hr 0x%x\r\n", bWr, uXfer, hr );
	#else
	int 	ret;

	// Check for read/writeability
	if ((!bWr && bRdChk) || (bWr && bWrChk))
		{
		fd_set 			fds;
		struct timeval	to;

		// Must use 'select' for async, read waiting in order for writes to not 
		// be blocked.
		FD_ZERO	(&fds);
		FD_SET	(fd,&fds);
			
		// Setup timeout.  Timeout the select every so often to catch
		// 'stop' requests.
		to.tv_sec	= (uTo/1000);
		to.tv_usec	= (uTo % 1000) * 1000;

		// Wait for I/O to be available
		ret = select ( (int)fd+1, (!bWr) ? &fds : NULL,
											(bWr) ? &fds : NULL, NULL, &to );

		// Valid ?
		if (ret < 0)
			{
			lprintf ( LOG_DBG, L"select : ret %d errno %d\r\n", ret, errno );
			hr = S_FALSE;
			}	// if
		else if (!FD_ISSET(fd,&fds))
			hr = ERROR_TIMEOUT;
		}	// if

	// Perform I/O
	if (hr == S_OK && bWr)
		{
		// Debug
//		lprintf ( LOG_DBG, L"Wa: %d bytes", uIo );
//		for (int i = 0;i < uIo;++i)
//			lprintf ( LOG_DBG, L"0x%x", pcBfr[i] );
		
		// Begin a write
		ret = write ( fd, pcBfr, uIo );
		if (ret <= 0)
			lprintf ( LOG_DBG, L"Wr: Error fd %d pcBfr %p uIo %d ret %d errno %d", 
										fd, pcBfr, uIo, ret, errno );
		}	// if
	else if (hr == S_OK)
		{
		// Begin a read
		ret = read ( fd, pcBfr, uIo );
		if (ret <= 0)
			lprintf ( LOG_DBG, L"Rd: Error fd %d pcBfr %p uIo %d ret %d  errno %d", 
										fd, pcBfr, uIo, ret, errno );
	
		// Debug
//		lprintf ( LOG_DBG, L"R: %d bytes", ret );
//		for (int i = 0;i < ret;++i)
//			lprintf ( LOG_DBG, L"0x%x", pcBfr[i] );
		}	// else if
		
	// Return of 0 means read timeout
	if (ret == 0)
		hr = ERROR_TIMEOUT;
	else if (ret < 0)
		{
//		lprintf ( LOG_DBG, L"fileIo : ret %d\r\n", ret );
		hr = S_FALSE;
		}	// else if
	else
		uXfer = ret;
	
	#endif

	// Result
	if (hr == S_OK && puIo != NULL)
		*puIo = uXfer;

	// Debug
//	lprintf ( LOG_DBG, L"bWr %d:uIo %d:uXfer %d:hr 0x%x\r\n", bWr, uIo, uXfer, hr );

	return hr;
	}	// fileIo

HRESULT File :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"AsyncRead"), vL ) == S_OK)
			bAsync = vL;
		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			iSzIo = adtInt(vL);
		if (pnDesc->load ( adtString(L"WriteCheck"), vL ) == S_OK)
			bWrChk = vL;
		if (pnDesc->load ( adtString(L"ReadCheck"), vL ) == S_OK)
			bRdChk = vL;
		if (pnDesc->load ( adtString(L"Timeout"), vL ) == S_OK)
			uTo = vL;
		if (pnDesc->load ( adtString(L"Debug"), vL ) == S_OK)
			bDbg = vL;

		// I/O events
		#ifdef		_WIN32
		CCLTRYE ( (hevWr = CreateEvent ( NULL, FALSE, FALSE, NULL )) != NULL,
						GetLastError() );
		CCLTRYE ( (hevRd = CreateEvent ( NULL, FALSE, FALSE, NULL )) != NULL,
						GetLastError() );
		CCLTRYE ( (hevStop = CreateEvent ( NULL, TRUE, FALSE, NULL )) != NULL,
						GetLastError() );
		#endif
		}	// if

	// Detach
	else
		{
		// Shutdown worker thread if necessary
		if (pThrd != NULL)
			{
			pThrd->threadStop(10000);
			_RELEASE(pThrd);
			}	// if

		// Clean up
		#ifdef		_WIN32
		if (hevWr != NULL)
			{
			CloseHandle ( hevWr );
			hevWr = NULL;
			}	// if
		if (hevRd != NULL)
			{
			CloseHandle ( hevRd );
			hevRd = NULL;
			}	// if
		if (hevStop != NULL)
			{
			CloseHandle ( hevStop );
			hevStop = NULL;
			}	// if
		#endif
		_RELEASE(pStmIo);
		}	// else

	return hr;
	}	// onAttach

HRESULT File :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Write
	if (_RCP(Write))
		{
		U32			uLeft		= 0;
		U64			uXferd	= 0;
		ULONG			uXfer		= 0;

		// State check
		#ifdef		_WIN32
		CCLTRYE ( hFile != INVALID_HANDLE_VALUE,	ERROR_INVALID_STATE );
		#else
		CCLTRYE ( fd != -1,								ERROR_INVALID_STATE );
		#endif
		CCLTRYE ( pStmIo != NULL,						ERROR_INVALID_STATE );

		// Size of transfer, size of 0 means entire stream
		if (hr == S_OK && iSzIo == 0)
			{
			U64	uAv = 0;
			CCLTRY ( pStmIo->available ( &uAv ) );
			CCLOK  ( uLeft = (U32) uAv; )
			}	// if
		else if (hr == S_OK)
			uLeft = iSzIo;

		// Write packet size data from stream
		while (hr == S_OK && uLeft > 0)
			{
			// Amount to write on next transaction
			uXfer = (uLeft < iSzBfr) ? uLeft : (ULONG)iSzBfr;

			// Read from source stream
			CCLTRY ( pStmIo->read ( pcBfr, uXfer, &uXferd ) );

			// Debug mode
			if (bDbg)
				{
				lprintf ( LOG_DBG, L"Writing %d bytes (0x%x)\r\n", uXferd, hr );
				for (U32 i = 0;i < uXferd;++i)
					lprintf ( LOG_DBG, L"%d) 0x%x\r\n", i, pcBfr[i] );
				}	// if

			// Write out File
			CCLTRY ( fileIo ( TRUE, (DWORD)uXferd, uTo, &uXfer ) );

			// Next block
			CCLOK ( uLeft -= uXfer; )
			}	// while

		// Result
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"File::Write:hr 0x%x, I/O %d/%d pStmIo %p uXfer %d uXferd %d",
					hr, uLeft, (U32)iSzIo, pStmIo, (U32)uXfer, (U32) uXferd ); 
		if (hr == S_OK)
			_EMT(Write,adtIUnknown(pStmIo));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Read
	else if (_RCP(Read))
		{
		U32			uLeft	= 0;
		ULONG			uXfer;

		// State check
		#ifdef		_WIN32
		CCLTRYE ( hFile != INVALID_HANDLE_VALUE,	ERROR_INVALID_STATE );
		#else
		CCLTRYE ( fd != -1,								ERROR_INVALID_STATE );
		#endif
		CCLTRYE ( pStmIo != NULL && iSzIo > 0,		ERROR_INVALID_STATE );

		// If asynchronous reads are enabled, reads happen in thread
		CCLTRYE ( bAsync == false, ERROR_INVALID_STATE );

		// Read packet size data from File
		CCLOK ( uLeft = iSzIo; )
		while (hr == S_OK && uLeft > 0)
			{
			// Amount to read on next transaction
			uXfer = (uLeft < iSzBfr) ? uLeft : (ULONG)iSzBfr;

			// Read from File.
			CCLTRY ( fileIo ( FALSE, uXfer, uTo, &uXfer ) );

			// Debug mode
			if (bDbg)
				{
				lprintf ( LOG_DBG, L"Read %d bytes (0x%x)\r\n", uXfer, hr );
				for (U32 i = 0;i < uXfer;++i)
					lprintf ( LOG_DBG, L"%d) 0x%x\r\n", i, pcBfr[i] );
				}	// if

			// Write to destination stream
			CCLTRY ( pStmIo->write ( pcBfr, uXfer, NULL ) );

			// Next block
			CCLOK ( uLeft -= uXfer; )

			// A short packet means end of transfer
			if (hr == S_OK && uXfer < iSzBfr)
				break;
			}	// while

		// Result
		if (hr == S_OK)
			_EMT(Read,adtIUnknown(pStmIo));
		else if (hr == ERROR_TIMEOUT)
			{
			lprintf ( LOG_DBG, L"File::Read:Timeout uXfer %d bytes\r\n", uXfer );
			_EMT(Timeout,adtIUnknown(pStmIo));
			}	// else if
		else
			{
			lprintf ( LOG_DBG, L"File::Read:hr 0x%x, I/O %d/%d\r\n", hr, uLeft, (U32)iSzIo );
			_EMT(Error,adtInt(hr));
			}	// else
		}	// if

	// State
	else if (_RCP(File))
		{
		IResource		*pRes = NULL;
		adtIUnknown		unkV(v);
		adtValue			vId;

		// Shutdown current state
		if (pThrd != NULL)
			{
			pThrd->threadStop(10000);
			_RELEASE(pThrd);
			}	// if
		#ifdef	_WIN32
		hFile = INVALID_HANDLE_VALUE;
		#else
		fd = -1;
		#endif
		
		// New resource/file handle
		CCLTRY ( _QISAFE(unkV,IID_IResource,&pRes) );
		CCLTRY ( pRes->getResId ( vId ) );
		#ifdef	_WIN32
		CCLOK  ( hFile = (HANDLE)(U64)adtLong(vId); )
		#else
		CCLOK  ( fd = (int)adtLong(vId); )
		#endif
		_RELEASE(pRes);
		
		// If async reads are specified, fire up thread
		if (hr == S_OK &&
			#ifdef	_WIN32
			hFile != INVALID_HANDLE_VALUE &&
			#else
			fd != -1 &&
			#endif
			bAsync == true)
			{
			// Create read thread
			#ifdef	_WIN32
			CCLOK(ResetEvent(hevStop);)
			#endif
			CCLOK(bRun = true;)
			CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
			CCLTRY(pThrd->threadStart ( this, 5000 ));
			}	// if

		}	// else if
	else if (_RCP(Stream))
		{
		adtIUnknown		unkV(v);
		_RELEASE(pStmIo);
		_QISAFE(unkV,IID_IByteStream,&pStmIo);
		}	// else if
	else if (_RCP(Size))
		iSzIo = adtInt(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT File :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Signal stop
	bRun = false;
	#ifdef	_WIN32
	if (hevStop != NULL)
		SetEvent ( hevStop );
	#endif

	return S_OK;
	}	// tickAbort

HRESULT File :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	DWORD		uXfer,uLeft;

	// Debug
//	dbgprintf ( L"File::tick {\n" );

	// Sanity check to avoid fast looping
	if (hr == S_OK && iSzIo == 0)
		{
		lprintf ( LOG_DBG, L"File::tick:WARNING no transfer size specified\r\n" );
		#ifdef 	_WIN32
		Sleep(1000);
		#else
		sleep(1);
		#endif
		}	// if

	// Perform transfer
	uLeft = iSzIo;
	while (hr == S_OK && uLeft > 0 && bRun)
		{
		// Read from end point
		CCLOK ( uXfer = 0; )
		CCLOK ( fileIo ( FALSE, (uLeft < iSzBfr) ? (DWORD)uLeft : (DWORD)iSzBfr, 1000, &uXfer ); )

		// Ok if no data was read as long as there is not an error.
		// For example this happens on a serial port with a timeout.
		if (hr == S_OK && uXfer > 0 && pStmIo != NULL)
			{
			// Write to destination stream
			CCLTRY ( pStmIo->write ( pcBfr, uXfer, NULL ) );

			// Debug mode
			if (bDbg)
				{
				lprintf ( LOG_DBG, L"Read async %d bytes (0x%x)\r\n", uXfer, hr );
				for (U32 i = 0;i < uXfer;++i)
					lprintf ( LOG_DBG, L"%d) 0x%x\r\n", i, pcBfr[i] );
				}	// if

			// Next block
			CCLOK ( uLeft -= uXfer; )

			// Assume a short read means end of transfer
			if (hr == S_OK && uXfer < iSzBfr)
				break;
			}	// if

		}	// while

	// Send out read stream
//	lprintf ( LOG_DBG, L"File::tick:hr 0x%x uLeft %d uXfer %d bRun %d\r\n", hr, uLeft, uXfer, bRun );
	if (bRun)
		{
		if (hr == S_OK)
			_EMT(Read,adtIUnknown(pStmIo));
		else
			_EMT(Error,adtInt(hr));
		}	// if
		
	// Keep running ?
	CCLTRYE ( bRun == true, S_FALSE );

	// Debug
//	dbgprintf ( L"} File::tick (0x%x)\n", hr );

	return hr;
	}	// tick

HRESULT File :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"{}\r\n" );

	return S_OK;
	}	// tickBegin

HRESULT File :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"{}\r\n" );
	
	return S_OK;
	}	// tickEnd
/*
void File :: update ( IDictionary *pDesc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Update internal state of node to handle new configuration.
	//
	//	PARAMETERS
	//		-	pDesc contains the File information
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	adtValue	vL;

	// Previous state
	iSzBfr = 0;
	iPipe  = -1;
	_FREEMEM(pcBfr);

	// Query the pipe information
	CCLTRY ( pDesc->load ( adtString(L"Id"), vL ) );
	CCLOK  ( iPipe = vL; )
	CCLTRY ( pDesc->load ( adtString(L"MaximumPacketSize"), vL ) );
	CCLOK  ( iSzBfr = vL; )

	// Allocate enough space for a full packet
	CCLTRYE ( iSzBfr > 0, E_UNEXPECTED );
	CCLTRYE ( (pcBfr = (U8 *) _ALLOCMEM(iSzBfr)) != NULL, E_OUTOFMEMORY );
	}	// update

*/
