////////////////////////////////////////////////////////////////////////
//
//									NotifyPath.CPP
//
//				Implementation of the path change notification node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"

#define	LENGTH_INFOBFR		0x10000

NotifyPath :: NotifyPath ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	hDir		= INVALID_HANDLE_VALUE;
	bWork		= false;
	pThrd		= NULL;
	pbBfr		= NULL;
	evWork.init();
	}	// NotifyPath

HRESULT NotifyPath :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Attach
	if (bAttach)
		{
		// Create a dictionary to hold results
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctRes ) );
		}	// if

	// Detach
	else
		{
		// Verify shutdown
		onReceive(prStop,adtInt());

		// Clean up
		_RELEASE(pDctRes);
		}	// else

	return hr;
	}	// onAttach

HRESULT NotifyPath :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Start
	if (_RCP(Start))
		{
		// Start worker thread
		if (pThrd == NULL)
			{
			CCLOK (evWork.reset();)
			CCLOK (bWork = true;)
			CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
			CCLOK (pThrd->threadStart ( this, 5000 );)
			}	// if
		}	// if

	// Stop
	else if (_RCP(Stop))
		{
		// Shutdown thread
		if (pThrd != NULL)
			{
			pThrd->threadStop(5000);
			pThrd->Release();
			pThrd = NULL;
			}	// if

		}	// if

	// State
	else if (_RCP(Location))
		hr = adtValue::toString ( v, sLoc );

	return hr;
	}	// receive

HRESULT NotifyPath :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT						hr		= S_OK;
	DWORD							dwOff,dwRet	= 0;
	FILE_NOTIFY_INFORMATION	*fni;

	// Issue an asynchronous read for directory changes
	memset ( &ov, 0, sizeof(ov) );
	ov.hEvent = evWork;
	if (!ReadDirectoryChangesW ( hDir, pbBfr, LENGTH_INFOBFR, TRUE, 
			FILE_NOTIFY_CHANGE_FILE_NAME|FILE_NOTIFY_CHANGE_DIR_NAME|
			FILE_NOTIFY_CHANGE_SIZE|FILE_NOTIFY_CHANGE_LAST_WRITE,
			NULL, &ov, NULL ))
		hr = GetLastError();

	// Wait until directory change or exiting
	if (hr == S_OK)
		{
		// Wait for work
		dwRet = WaitForSingleObject ( evWork, INFINITE );

		// Still running ?
		CCLTRYE ( bWork == true, S_FALSE );

		// Should be...
		CCLTRYE ( dwRet == WAIT_OBJECT_0, E_UNEXPECTED );

		// Retrieve the result
		CCLTRYE ( GetOverlappedResult ( hDir, &ov, &dwRet, FALSE ) == TRUE,
						GetLastError() );
		lprintf ( LOG_DBG, L"hr 0x%x,dwRet %d", hr, dwRet );
		}	// if

	// Process all of the entries
	for (	dwOff = 0,fni	= (FILE_NOTIFY_INFORMATION *)pbBfr;
			hr		== S_OK && dwOff+1 < LENGTH_INFOBFR;
			dwOff	+= fni->NextEntryOffset,fni = (FILE_NOTIFY_INFORMATION *)&pbBfr[dwOff] )
		{
		adtString	strFile;

		// Debug
//		lprintf ( LOG_DBG, L"Action 0x%x,Offset %d,FileNameLength %d,FileName %s", 
//						fni->Action, fni->NextEntryOffset, fni->FileNameLength, fni->FileName );

		// Convert action to string
		adtString	strAction = (fni->Action == FILE_ACTION_ADDED)					? L"Added" :
										(fni->Action == FILE_ACTION_REMOVED)				? L"Removed" :
										(fni->Action == FILE_ACTION_MODIFIED)				? L"Modified" :
										(fni->Action == FILE_ACTION_RENAMED_OLD_NAME)	? L"RenamedFrom" :
										(fni->Action == FILE_ACTION_RENAMED_NEW_NAME)	? L"RenamedTo" : L"Unknown";

		// String in structure is unterminated, generate full string
		dwRet = fni->FileNameLength/sizeof(WCHAR);
		CCLTRY ( strFile.allocate ( dwRet ) );
		CCLOK  ( WCSNCPY ( &strFile.at(0), dwRet+1, fni->FileName, dwRet ); )

		// To nSpace locations
		CCLOK  ( strFile.replace('\\','/'); )

		// Emit results
		CCLTRY ( pDctRes->store ( adtString(L"Filename"), strFile ) );
		CCLTRY ( pDctRes->store ( adtString(L"Action"), strAction ) );
		_EMT(Fire,adtIUnknown(pDctRes));

		// Done ?
		if (fni->NextEntryOffset == 0)
			break;
		}	// for

	return hr;
	}	// tick

HRESULT NotifyPath :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// End running
	bWork = false;
	evWork.signal();

	return S_OK;
	}	// tickAbort

HRESULT NotifyPath :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Access directory (see documentation for ReadDirectoryChangesW)
	CCLTRYE( (hDir = CreateFile ( sLoc, FILE_LIST_DIRECTORY, 
				FILE_SHARE_WRITE|FILE_SHARE_READ|FILE_SHARE_DELETE,
				NULL, OPEN_EXISTING, 
				FILE_FLAG_BACKUP_SEMANTICS|FILE_FLAG_OVERLAPPED,
				NULL )) != NULL, GetLastError() );

	// Allocate memory for the notification buffer
	CCLTRYE ( (pbBfr = (BYTE *) _ALLOCMEM(LENGTH_INFOBFR)) != NULL,
					E_OUTOFMEMORY );

	return hr;
	}	// tickBegin

HRESULT NotifyPath :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (hDir != INVALID_HANDLE_VALUE)
		{
		CloseHandle ( hDir );
		hDir = INVALID_HANDLE_VALUE;
		}	// if
	_FREEMEM(pbBfr);

	return S_OK;
	}	// tickEnd
