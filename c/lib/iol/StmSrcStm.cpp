////////////////////////////////////////////////////////////////////////
//
//									StmSrcStm.CPP
//
//			Single stream from inside stream-based stream source
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"

StmSrcStm :: StmSrcStm ( StmSrcStms *_pParent, U64 _uAtHdr, bool _bRead )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	//	PARAMETERS
	//		-	_pParent is the parent object
	//		-	_uAt is the position of this stream header within 
	//			the master stream
	//		-	_bRead is true for read-only access
	//
	////////////////////////////////////////////////////////////////////////
	pParent	= _pParent; _ADDREF(pParent);
	uAtHdr	= _uAtHdr;
	uPos		= 0;
	bRead		= _bRead;
	pDct		= NULL;
	}	// StmSrcStm

HRESULT StmSrcStm :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;
	return E_NOTIMPL;
	}	// available

HRESULT StmSrcStm :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Close the resource.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Closed
	if (pParent != NULL)
		{
		HRESULT	hr = S_OK;
		U64		szTot,szStm;

		// Update size if necessary
		if (!bRead)
			{
			// Total size of stream
			pParent->csStm.enter();
			CCLTRY ( pParent->pStmSrc->seek ( 0, STREAM_SEEK_END, &szTot ) );

			// Size of closing stream
			CCLOK ( szStm = (szTot - uAt); )
			CCLTRY( pDct->store ( adtString(L"Size"), adtLong(szStm) ) );

			// Write size directly to header in stream
			// This header can be re-written because the variable length fields
			// in the dictionary have not changed size.
			CCLTRY ( pParent->pStmSrc->seek ( uAtHdr, STREAM_SEEK_SET, NULL ) );
			CCLTRY ( pParent->pStmPer->save ( pParent->pStmSrc, adtIUnknown(pDct) ) );
			pParent->csStm.leave();
			}	// if

		// Notify parent
		pParent->closed ( this );

		// Clean up
		_RELEASE(pParent);
		}	// if

	return S_OK;
	}	// close

HRESULT StmSrcStm :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	adtValue		vL;
	adtIUnknown	unkV;

	// Read own header for reference
	pParent->csStm.enter();
	CCLTRY ( pParent->pStmSrc->seek ( uAtHdr, STREAM_SEEK_SET, NULL ) );
	CCLTRY ( pParent->pStmPer->load ( pParent->pStmSrc, vL ) );
	CCLTRY ( _QISAFE((unkV=vL),IID_IDictionary,&pDct) );
	CCLTRY ( pDct->load ( adtString(L"Size"), vL ) );
	CCLOK  ( uSz = adtLong(vL); )
	CCLTRY ( pParent->pStmSrc->seek ( 0, STREAM_SEEK_CUR, &uAt ) );
	pParent->csStm.leave();

	return hr;
	}	// construct

HRESULT StmSrcStm :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// copyTo

void StmSrcStm :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	close();
	_RELEASE(pDct);
//	dbgprintf ( L"StmSrcStm::destruct:%p\r\n", this );
	}	// destruct

HRESULT StmSrcStm :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Flush master stream
	pParent->csStm.enter();
	hr = pParent->pStmSrc->flush();
	pParent->csStm.leave();

	return hr;
	}	// flush

HRESULT StmSrcStm :: getResId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Return an identifier for the resource.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Setup
	adtValue::clear ( vId );

	// What makes sense here ?  Position ?
	hr = adtValue::copy ( adtLong(uAt), vId );
	
	return hr;
	}	// getResId

HRESULT StmSrcStm :: open ( IDictionary *pOpts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Open a byte stream on top of a file.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the file.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Stream is already open on construct
	return ERROR_INVALID_STATE;

	}	// open

HRESULT StmSrcStm :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr = S_OK;
	U64				ntor,nr;

	// Adjust bytes to ensure not going off the end
	ntor = (nio <= (uSz - uPos)) ? nio : (uSz - uPos);

	// Seek and read
	pParent->csStm.enter();
	CCLTRY ( pParent->pStmSrc->seek ( uAt+uPos, STREAM_SEEK_SET, NULL ) );
	CCLTRY ( pParent->pStmSrc->read ( pvBfr, ntor, &nr ) );
	pParent->csStm.leave();

	// Results
	if (hr == S_OK)
		{
		uPos += nr;
		if (pnio != NULL)
			*pnio = nr;
		}	// if

	return hr;
	}	// read

HRESULT StmSrcStm :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	S64		pos;

	// Internal seek only
	pos = (uFrom == STREAM_SEEK_SET) ?	sPos :
			(uFrom == STREAM_SEEK_CUR) ?	(uPos + sPos) :
													(uSz - sPos);

	// Rail to size of stream
	if			(pos < 0)					pos = 0;
	else if	(pos > (S64)uSz)	pos = uSz;

	// Result
	if (hr == S_OK)
		{
		uPos = (U64) pos;
		if (puPos != NULL)
			*puPos = uPos;
		}	// if

	return hr;
	}	// seek

HRESULT StmSrcStm :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Writable stream ?
	if (bRead)
		return E_ACCESSDENIED;

	return hr;
	}	// setSize

HRESULT StmSrcStm :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	U64		nw;

	// Writable stream ?
	if (bRead)
		return E_ACCESSDENIED;

	// Seek and write
	pParent->csStm.enter();
	CCLTRY ( pParent->pStmSrc->seek ( uAt+uPos, STREAM_SEEK_SET, NULL ) );
	CCLTRY ( pParent->pStmSrc->write ( pcvBfr, nio, &nw ) );
	pParent->csStm.leave();

	// Did this new write result in a larger size ?
	if (hr == S_OK && (uPos + nw) > uSz)
		uSz = uPos + nw;

	// Result
	if (hr == S_OK)
		{
		uPos += nw;
		if (pnio != NULL)
			*pnio = nw;
		}	// if

	return hr;
	}	// write

