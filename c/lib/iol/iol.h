////////////////////////////////////////////////////////////////////////
//
//										IOL.H
//
//									I/O library
//
////////////////////////////////////////////////////////////////////////

#ifndef	IOL_H
#define	IOL_H

// System includes
#include "../adtl/adtl.h"

// Definitions/macros
#ifndef	STREAM_SEEK_SET
#define	STREAM_SEEK_SET		0
#define	STREAM_SEEK_CUR		1
#define	STREAM_SEEK_END		2
#endif

//////////////
// Interfaces
//////////////

//
// Interface - IByteStream.  Base interface for a byte stream.
//

DEFINE_GUID	(	IID_IByteStream, 0x2534d006, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IByteStream,IUnknown)
	{
	public :
	STDMETHOD(available)	( U64 * )								PURE;
	STDMETHOD(copyTo)		( IByteStream *, U64, U64 * )		PURE;
	STDMETHOD(flush)		( void )									PURE;
	STDMETHOD(read)		( void *, U64, U64 * )				PURE;
	STDMETHOD(seek)		( S64, U32, U64 * )					PURE;
	STDMETHOD(setSize)	( U64 )									PURE;
	STDMETHOD(write)		( void const *, U64, U64 * )		PURE;
	};

//
// Interface - ILocations.  Base interface for a source of streams.
//

DEFINE_GUID	(	IID_ILocations, 0x2534d010, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(ILocations,IUnknown)
	{
	public :
	STDMETHOD(copy)		( const WCHAR *, const WCHAR * )			PURE;
	STDMETHOD(flush)		( ADTVALUE & )									PURE;
	STDMETHOD(link)		( const WCHAR *, const WCHAR * )			PURE;
	STDMETHOD(locations)	( const WCHAR *,	IIt ** )					PURE;
	STDMETHOD(move)		( const WCHAR *, const WCHAR * )			PURE;
	STDMETHOD(open)		( IDictionary *,	IUnknown ** )			PURE;
	STDMETHOD(remove)		( const WCHAR * )								PURE;
	STDMETHOD(resolve)	( const WCHAR *,	bool, ADTVALUE & )	PURE;
	STDMETHOD(status)		( const WCHAR *,	IDictionary * )		PURE;
	};

//
// Interface - IResource.  Interface to a generic I/O resource object that can
//										be opened and closed.
//

DEFINE_GUID	(	IID_IResource, 0x2534d007, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IResource,IUnknown)
	{
	public :
	STDMETHOD(close)		( void )				PURE;
	STDMETHOD(getResId)	( ADTVALUE & )		PURE;
	STDMETHOD(open)		( IDictionary * )	PURE;
	};

//
// Interface - IStreamPersist.  Interface to an object that can persist
//		values from and to a stream.
//

DEFINE_GUID	(	IID_IStreamPersist, 0x2534d015, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IStreamPersist,IUnknown)
	{
	public :
//	STDMETHOD(iterate)	( IByteStream *, IIt ** )				PURE;
	STDMETHOD(load)		( IByteStream *, ADTVALUE & )			PURE;
	STDMETHOD(save)		( IByteStream *, const ADTVALUE & )	PURE;
	};

//
// Interface - IMemoryMapped.  Interface to an object that is
//		memory mapped.
//

DEFINE_GUID	(	IID_IMemoryMapped, 0x2534d00c, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IMemoryMapped,IUnknown)
	{
	public :
	STDMETHOD(getInfo)	( void **, U32 * )	PURE;
	STDMETHOD(setInfo)	( void *, U32 )		PURE;
	STDMETHOD(setSize)	( U32 )					PURE;
	STDMETHOD(stream)		( IByteStream ** )	PURE;
	};

//
// Interface - IPersistId.  Interface to retrieve persistence Id of object.
//

DEFINE_GUID	(	IID_IPersistId, 0x2534d119, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IPersistId,IUnknown)
	{
	public :
	STDMETHOD(getPersistId)	( ADTVALUE & )		PURE;
	};

//
// Interface - IPersistValue.  Interface to an object that can persist
//		its own values.
//

DEFINE_GUID	(	IID_IPersistValue, 0x2534d111, 0x8628, 0x11d2, 0x86, 0x8c,
					0x00, 0x60, 0x08, 0xad, 0xdf, 0xed );

DECLARE_INTERFACE_(IPersistValue,IPersistId)
	{
	public :
	STDMETHOD(load)	( IStreamPersist *, IByteStream * )	PURE;
	STDMETHOD(save)	( IStreamPersist *, IByteStream * )	PURE;
	};

#endif
