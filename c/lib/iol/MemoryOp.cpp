////////////////////////////////////////////////////////////////////////
//
//									MemoryOp.CPP
//
//			Implementation of the memory operations node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

MemoryOp :: MemoryOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pMap	= NULL;
	lOff	= 0;
	lSz	= 0;
	}	// MemoryOp

void MemoryOp :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pMap);
	}	// destruct

HRESULT MemoryOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Offset"), vL ) == S_OK)
			lOff = vL;
		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			lSz = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pMap);
		}	// if

	return S_OK;
	}	// onAttach

HRESULT MemoryOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Submap
	if (_RCP(Submap))
		{
		MemoryBlock	*pBlk 	= NULL;

		// State check
		CCLTRYE ( pMap != NULL, ERROR_INVALID_STATE );

		// Create a static memory block for the region
		CCLTRYE( (pBlk = new MemoryBlock(pMap,lOff,(U32)lSz)) != NULL,
						E_OUTOFMEMORY );
		CCLOK  ( pBlk->AddRef(); )
		CCLTRY ( pBlk->construct() );

		// Result
		if (hr == S_OK)
			_EMT(Submap,adtIUnknown((IMemoryMapped *)pBlk));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pBlk);
		}	// if

	// Stream on stop of memory mapped onject
	else if (_RCP(Stream))
		{
		IByteStream	*pStm = NULL;

		// State check
		CCLTRYE ( pMap != NULL, ERROR_INVALID_STATE );

		// Obtain a stream
		CCLTRY ( pMap->stream ( &pStm ) );

		// Result
		if (hr == S_OK)
			_EMT(Stream,adtIUnknown(pStm));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pStm);
		}	// else if

	// Resize
	else if (_RCP(Resize))
		{
		// State check
		CCLTRYE ( pMap != NULL, ERROR_INVALID_STATE );

		// Debug
//		lprintf ( LOG_DBG, L"%s) Resize pMap %p lSz %d\r\n", (LPCWSTR)strnName, pMap, (U32)lSz );

		// Resize the map
		CCLTRY ( pMap->setSize ( (U32)lSz ) );

		// Result 
		if (hr == S_OK)
			_EMT(Resize,adtIUnknown(pMap));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Query info.
	else if (_RCP(Query))
		{
		U32	sz;

		// State check
		CCLTRYE ( pMap != NULL, ERROR_INVALID_STATE );

		// Do ptr too ?
		CCLTRY ( pMap->getInfo ( NULL, &sz ) );

		// Result
		if (hr == S_OK)
			_EMT(Size,adtInt(sz));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else if (_RCP(Memory))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pMap);
		hr = _QI(unkV,IID_IMemoryMapped,&pMap);
		}	// else if

	else if (_RCP(Offset))
		lOff = v;
	else if (_RCP(Size))
		lSz = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// onReceive


