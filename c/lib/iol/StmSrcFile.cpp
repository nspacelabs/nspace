////////////////////////////////////////////////////////////////////////
//
//									StmSrcFile.CPP
//
//						File system based stream source
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#if defined(__APPLE__) || defined(__unix__)
#include <dirent.h>
#include <sys/statvfs.h>
#include <stdio.h>
#endif
#ifdef	_WIN32
#include	<shlobj_core.h>
#endif

StmSrcFile :: StmSrcFile ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	punkDct	= NULL;
	pDct		= NULL;
	pStmOpts	= NULL;
	strRoot	= L"";

	// Frequently used keys
	strkLoc		= L"Location";
	strkRO		= L"ReadOnly";
	strkCr		= L"Create";
	strkTr		= L"Truncate";
	strkAsync	= L"Async";
	}	// SysStmsFile

HRESULT StmSrcFile :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Aggregrate dictionary
	CCLTRY ( COCREATEA ( L"Adt.Dictionary", (ILocations *) this, &punkDct ) );

	// Interfaces
	CCLTRY ( _QI(punkDct,IID_IDictionary,&pDct) );
	CCLOK  ( pDct->Release(); )						// Cancel 'QI'

	// For stream options
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pStmOpts ) );

	// Default root location
	CCLTRY(defaultRoot());

	return hr;
	}	// construct

HRESULT StmSrcFile :: copy ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Copy a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtString	strF,strT;

	// Generate full paths
	CCLTRY ( toPath ( pwF, strF ) );
	CCLTRY ( toPath ( pwT, strT ) );

	// Windows
	#ifdef	_WIN32

	// Execute
	CCLTRYE ( CopyFile ( strF, strT, FALSE ) == TRUE, GetLastError() );

	#elif __APPLE__ || __unix__
	char	*pcF 	= NULL;
	char 	*pcT 	= NULL;
	char	*pcX	= NULL;
	int 	ret	= 0;

	// No Unix copy command.  Use shell to copy files
	// TODO: Use own byte streams to copy ?

//		CCLOK ( ret = system ( pcFull ); )

	// Calls need ASCII
	CCLTRY ( strF.toAscii ( &pcF ) );
	CCLTRY ( strT.toAscii ( &pcT ) );

	// Allocate memory for command string since it is not
	// known how long the file names are
	CCLTRYE ( (pcX = (char *) _ALLOCMEM ( strlen(pcF) + strlen(pcT) + 20 ))
					!= NULL, E_OUTOFMEMORY );

	// Run the command
	CCLOK ( sprintf ( pcX, "cp \"%s\" \"%s\"", pcF, pcT ); )
//	lprintf ( LOG_DBG, L"pcX %S\r\n", pcX );
	CCLOK ( ret = system ( pcX ); )
	if (ret != 0)
		lprintf ( LOG_DBG, L"System returned : %d for %S\r\n", ret, pcX );


	// Clean up
	_FREEMEM(pcX);
	_FREEMEM(pcF);
	_FREEMEM(pcT);
	#endif

//	lprintf ( LOG_DBG, L"Copy : %s : %s\r\n", pwF, pwT );

	return hr;
	}	// copy

HRESULT StmSrcFile :: createLoc ( const WCHAR *pLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to create a stream location.
	//
	//	PARAMETERS
	//		-	pLoc is the location.  A filename at the end is ignored.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	int				j,i	= -1;
	adtString		strLoc(pLoc),strDir;

	// Verify/create location one folder at a time
	while (	hr == S_OK &&
				(	(j = strLoc.indexOf ( '/', i+1 ))	!= -1 ||
					(j = strLoc.indexOf ( '\\', i+1 ))	!= -1 ) )
		{
		// Generate path up to this point (allow for extra long path names)
		// Do NOT include trailing slash for Windows or it will fail on
		// folders that are symbolic links
		CCLOK  ( i = j; )
		CCLTRY ( strLoc.substring ( 0, i, strDir ) );

		#ifdef			_WIN32
      U32				fa;

		// Windows wants backslashes
		CCLOK ( strDir.replace ( '/', '\\' ); )
      
		// Does the directory already exist ?
		CCLOK ( fa = GetFileAttributes ( strDir ); )

		// Does not exist
		if (hr == S_OK && 
				(fa == INVALID_FILE_ATTRIBUTES ||
				!(fa & FILE_ATTRIBUTE_DIRECTORY) ) )
			{
			// Create the location 
			CCLTRYE ( CreateDirectory ( strDir, NULL ) == TRUE, GetLastError() );

			// Update file attributes
			CCLOK ( fa = GetFileAttributes ( strDir ); )
			}	// if

		// Proceed ?
		CCLTRYE ((fa & FILE_ATTRIBUTE_DIRECTORY) || 
					(fa & FILE_ATTRIBUTE_REPARSE_POINT), E_UNEXPECTED );
		#elif __APPLE__ || __unix__
      {
      char        *pc = NULL;
      struct stat s;
      
      // Convert to ASCII for system calls
      CCLTRY ( strDir.toAscii(&pc) );
      
      // Get status of location
      if (stat ( pc, &s ) != 0)
         {
         // Create the directory with max. permissions allowed 
			// TODO: Use mode as with opening files
         mkdir ( pc, S_IRWXU|S_IRWXG|S_IRWXO );
         }  // if
         
      // Clean up
      _FREEMEM(pc);
      }
		#endif
		}	// while
	
	// Debug
	if (hr != S_OK)
		dbgprintf ( L"StmSrcFile::createLoc:Failed:0x%x\r\n", hr );

	return hr;
	}	// createLoc

HRESULT StmSrcFile :: defaultRoot ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Generate a system-specific default root location for streams.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	#ifdef	_WIN32
	HKEY		hKey	= NULL;
	LONG		lRes;
	DWORD		dwType,dwSz,len;
	WCHAR		strBfr[1024];

	///////////////////////////////////////////////////////////////////
	// Multiple options for setting the default root directory.  Avoid
	// using 'working directory' since it changes so much between OSes
	///////////////////////////////////////////////////////////////////

	// Option 1 - Default is to use the same path where the process EXE is located
	if (hr == S_OK)
		{
		WCHAR		*pwSlash = NULL;
		DWORD		len;

		// Get path to running executable
		CCLOK   ( len = sizeof(strBfr)/sizeof(strBfr[0]); )
		CCLTRYE ( (len = GetModuleFileName ( NULL, strBfr, len )) != 0, GetLastError() );

		// Path only
		CCLTRYE ( (pwSlash = wcsrchr ( strBfr, WCHAR('\\') )) != NULL, E_UNEXPECTED );
		CCLOK   ( *(pwSlash+1) = WCHAR('\0'); )

		// Paths in nSpace are always forward slash
		for (size_t i = 0;hr == S_OK && i < wcslen(strBfr);++i)
			if (strBfr[i] == '\\')
				strBfr[i] = '/';

		// New root
		strRoot = strBfr;

		// Windows specific.  The directory "Program Files" is protected and
		// set as read-only.  If this instance of nSpace is run from inside that
		// assume the "Program Data" location is used for non-executable files.  
		// Use the same 'sub' folder name as the detected root
		#ifdef _WIN32
		if (hr == S_OK)
			{
			WCHAR			wPf[MAX_PATH],wAd[MAX_PATH];
			adtString	strRootNew;

			// Retrieve the current program files location
			CCLTRY ( SHGetFolderPath ( NULL, CSIDL_PROGRAM_FILES, NULL,
												SHGFP_TYPE_DEFAULT, wPf ) );

			// nSpace uses forward slashes
			CCLOK ( strRootNew = wPf; )
			CCLOK ( strRootNew.replace ( WCHAR('\\'), WCHAR('/') ); )

			// Does the root have the same prefix ?
//			lprintf ( LOG_DBG, L"strRoot %s strRootNew %s\r\n",
//							(LPCWSTR)strRoot, (LPCWSTR)strRootNew );
			if (hr == S_OK && !WCASENCMP(strRoot,strRootNew,wcslen(wPf)))
				{
				// Program data location
				CCLTRY ( SHGetFolderPath ( NULL, CSIDL_COMMON_APPDATA, NULL,
													SHGFP_TYPE_DEFAULT, wAd ) );

				// Replace program files with path to program data
				CCLOK	( strRootNew = wAd; )
				CCLTRY( strRootNew.append ( &strRoot[wcslen(wPf)] ) );
				CCLOK	( strRootNew.replace ( WCHAR('\\'), WCHAR('/') ); )

				// Debug
				lprintf ( LOG_DBG, L"Program files install detected, storage root now %s\r\n",
								(LPCWSTR)strRootNew );

				// New root
				CCLTRY ( adtValue::copy ( strRootNew, strRoot ) );
				}	// if

			}	// if

		#endif
		}	// if
	/*
	// Option 2 - For development the 'nsh.exe' is put into a 'debug' folder but
	#ifdef	_DEBUG
	if (	hr == S_OK										&& 
			(len = strRoot.length()) > wcslen(L"Debug/")	&&
			!WCASECMP ( strRoot, L"Debug/" ) )
		{
		// Previous path
		CCLOK ( wcscpy_s ( strBfr, sizeof(strBfr)/sizeof(strBfr[0]), strRoot ); )
		CCLOK ( strBfr[len-wcslen(L"Debug/")] = '\0'; )

		// Allocate memory for new path
		CCLTRY ( strRoot.allocate ( wcslen(strBfr)+10 ) );

		// New path
		CCLOK ( wcscpy_s ( &strRoot.at(), wcslen(strBfr)+10, strBfr ); )
		CCLOK ( wcscat_s ( &strRoot.at(), wcslen(strBfr)+10, L"graphs/" ); )
		}	// if
	#endif
	*/

	// Option 3 - Environment variable.  This is useful for using the debugger
	// or setting a root via the command line.
	if (hr == S_OK && GetEnvironmentVariable ( L"NSPACE_ROOT", 
			strBfr, sizeof(strBfr)/sizeof(strBfr[0]) ) > 0)
		strRoot = strBfr;

	// Option 4 - Hard coded root path in registry for all nSpace executions.
	if (	(hr == S_OK)
			&& 
			((lRes = RegCreateKeyEx ( HKEY_LOCAL_MACHINE, L"SOFTWARE\\nSpace",
												0, NULL, REG_OPTION_NON_VOLATILE, 
												KEY_READ, NULL,
												&hKey, NULL )) == ERROR_SUCCESS)
			&&
			((lRes = RegQueryValueEx ( hKey, L"Root", NULL, &dwType, NULL, &dwSz ))
													== ERROR_SUCCESS) )
		{
		// Allocate (+1 for possible slash)
		CCLTRY ( strRoot.allocate ( dwSz/sizeof(WCHAR) ) );

		// Read
		CCLTRYE ( ((lRes = RegQueryValueEx ( hKey, L"Root", NULL, &dwType,
						(BYTE *) &strRoot.at(), &dwSz )) == ERROR_SUCCESS), lRes );

		// Our paths always end in '\\'
		CCLOK ( len = strRoot.length(); )
		if (	hr == S_OK && strRoot[len-1] != WCHAR('/') &&
				strRoot[len-1] != WCHAR('\\') )
			{
			strRoot.at(len)	= WCHAR('\\');		// Add a slash
			strRoot.at(len+1)	= WCHAR('\0');		// Terminate at new position
			}	// if

		// Debug
//		dbgprintf ( L"SysStmsFile::open:Root nSpace Path:%s\r\n", (LPWSTR)strRoot );
		}	// if

	// Clean up
	if (hKey != NULL) RegCloseKey ( hKey );

	// Unix
	#elif	__unix__ || __APPLE__
	if ( hr == S_OK )
		{
		char	cwd[1024];
		char	*pcEnv;

		// Option 1 - Default is to use the working directory

		// Working directory
		CCLTRYE ( getcwd ( cwd, sizeof(cwd) ) != NULL, errno );

		// Paths always end in '/'
		CCLOK ( strcat ( cwd, "/" ); )

		// New root
		strRoot = cwd;

		// Option 2 - Environment variable.  This is useful for using the debugger
		// or setting a root via the command line.
		if (hr == S_OK && 
				(pcEnv = getenv ( "NSPACE_ROOT" )) != NULL )
			strRoot = pcEnv;
		}	// else if
	#endif

	// the graphs are somewhere else
	// Default root
	CCLTRY ( pDct->store ( adtString(L"Root"), strRoot ) );
//	CCLOK  ( lprintf ( LOG_DBG, L"%s", (const WCHAR *)strRoot ); )

	return hr;
	}	// defaultRoot

void StmSrcFile :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pStmOpts);
	_RELEASE(punkDct);
	}	// destruct

HRESULT StmSrcFile :: flush ( ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Flushes or synchronous the stream source.
	//
	//	PARAMETERS
	//		-	v is a value for possible future use
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// On Unix, 'sync' the file systems
	#if defined(__APPLE__) || defined(__unix__)
	int ret = system ( "sync" );
	if (ret != 0)
		lprintf ( LOG_DBG, L"Result %d\r\n", ret );
	#endif

	return hr;
	}	// flush

HRESULT StmSrcFile :: link ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Create a symbolic link to a location.
	//
	//	PARAMETERS
	//		-	pwF is the source/link location
	//		-	pwT is the destination/target location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtString	strF,strT;

	// Generate full paths
	CCLTRY ( toPath ( pwF, strF ) );
	CCLTRY ( toPath ( pwT, strT ) );

	// Windows
	#ifdef	_WIN32
	bool	bDir = (	strT[strT.length()-1] == '/' ||
						strT[strT.length()-1] == '\\' );

	// Execute.  CreateSymbolicLink is admin only ???
	CCLTRYE ( CreateSymbolicLink ( strF, strT, (bDir) ? 
					SYMBOLIC_LINK_FLAG_DIRECTORY : 0 ) == TRUE, GetLastError() );

	#elif __APPLE__ || __unix__
	char *pcF = NULL;
	char *pcT = NULL;

	// Calls need ASCII
	CCLTRY ( strF.toAscii ( &pcF ) );
	CCLTRY ( strT.toAscii ( &pcT ) );

	//		-	pwF is the source/link location
	//		-	pwT is the destination/target location

	// Execute
	CCLTRYE ( (symlink ( pcT, pcF ) == 0), errno );

	// Clean up
	_FREEMEM(pcF);
	_FREEMEM(pcT);
	#endif

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Link failed:0x%x:From:%s:To:%s\r\n", 
						hr, pwF, pwT );

	return hr;
	}	// link

HRESULT StmSrcFile :: locations ( const WCHAR *wLoc, IIt **ppIt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns an iterator for the stream locations at the specified
	//			location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	ppIt will receive a stream location iterator.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr		= S_OK;
	IList					*pLst	= NULL;
	adtString			strLoc;

	// Create a list of locations and use that lists iterator
	CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLst ) );

	// Generate full path to strLeam using source options
	CCLTRY ( toPath ( wLoc, strLoc ) );

	// Iterate the files in the specified location
	#ifdef	_WIN32
		{
		HANDLE				hFind	= INVALID_HANDLE_VALUE;
		BOOL					bMore;
		WIN32_FIND_DATA	wfd;
		CCLTRY ( strLoc.append ( L"*" ) );
		for (	bMore = ((hFind = FindFirstFile ( strLoc, &wfd )) != INVALID_HANDLE_VALUE);
				bMore;
				bMore = FindNextFile ( hFind, &wfd ) )
			{
			// Ignore directory entries
			if (!WCASECMP(wfd.cFileName,L".") || !WCASECMP(wfd.cFileName,L".."))
				continue;

			// nSpace locations end with a slash
			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
				// Add slash
				wfd.cFileName[wcslen(wfd.cFileName)+1] = '\0';
				wfd.cFileName[wcslen(wfd.cFileName)] = '/';
				}	// if

			// Add name to list
			CCLTRY ( pLst->write ( adtString(wfd.cFileName) ) );
			}	// for
			
		// Clean up
		if (hFind != INVALID_HANDLE_VALUE)
			FindClose ( hFind );
		}
	#elif defined(__APPLE__) || defined(__unix__)
      {
		DIR				*dir		= NULL;
		char				*pcLoc	= NULL;
		struct dirent	*ent		= NULL;
		adtString		strName;
		struct stat		lstat;
		
		// ASCII version of location
		CCLTRY ( strLoc.toAscii ( &pcLoc ) );
//		lprintf ( LOG_DBG, L"pcLoc %S\r\n", pcLoc );
		
		// Access location
		if (hr == S_OK && (dir = opendir ( pcLoc )) != NULL)
			{	
			// Read entries in directory
			while (hr == S_OK && (ent = readdir ( dir )) != NULL)
				{
//				lprintf ( LOG_DBG, L"d_name %S dtype 0x%x (0x%x)\r\n", ent->d_name, ent->d_type, DT_DIR );

				// Ignore directory entries
				if (	ent->d_name[0] == '.' &&
						(	ent->d_name[1] == '\0' ||
							(ent->d_name[1] == '.' && ent->d_name[2] == '\0') ) )
					continue;

				// Not all file systems under Linux have to fill in the 'd_type' so
				// the only way to verify a directory or not is to 'stat' it directly
				if (hr == S_OK)
					{
					adtString	strLocFull(strLoc);
					adtString 	strName(ent->d_name);
					char			*pcLocFull	= NULL;

					// ASCII version of full path
					CCLTRY ( strLocFull.append ( strName ) );
					CCLTRY ( strLocFull.toAscii ( &pcLocFull ) );

					// Stat the location
					CCLTRYE ( (stat ( pcLocFull, &lstat ) == 0), GetLastError() );
//					lprintf ( LOG_DBG, L"st_mode 0x%x (0x%x) %S (hr 0x%x)\r\n", 
//								lstat.st_mode, S_IFDIR, pcLocFull, hr );

					// Clean up
					_FREEMEM(pcLocFull);
					}	// if

				// Location name
				CCLOK ( strName = ent->d_name; )

				// nSpace locations end with a slash
 				if (hr == S_OK && (lstat.st_mode & S_IFMT) == S_IFDIR)
					strName.append ( L"/" );

				// Add file to list
				CCLTRY ( pLst->write ( strName ) );
				}	// while
			}	// if

		// Clean up
		if (dir != NULL)
			closedir ( dir );
		_FREEMEM(pcLoc);
      }
	#endif

	// Create iterator for locations
	CCLTRY ( pLst->iterate ( ppIt ) );

	// Clean up
	_RELEASE(pLst);

	return hr;
	}	// locations

HRESULT StmSrcFile :: open ( IDictionary *pOpts, IUnknown **ppLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Open a a location inside the locations.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the stream.
	//		-	ppLoc will receive the location object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IResource	*pRes	= NULL;
	adtValue		v;
	adtString	strLoc,strLocFull;
	adtBool		bRO(true),bCr(false);
	
	// Setup
	*ppLoc	= NULL;

	// Stream location (required)
	CCLTRY ( pOpts->load ( strkLoc, v ) );

	// Generate full path to stream using source options
	CCLTRY ( toPath ( (strLoc = v), strLocFull ) );

	// Copy relevant options for stream
	CCLOK  ( pStmOpts->clear(); )
	CCLTRY ( pStmOpts->store ( strkLoc, strLocFull ) );
	if (pOpts->load ( strkRO, bRO ) == S_OK)
		pStmOpts->store ( strkRO, bRO );
	if (pOpts->load ( adtString(L"WriteOnly"), v ) == S_OK)
		pStmOpts->store ( adtString(L"WriteOnly"), v );
	if (pOpts->load ( strkCr, bCr ) == S_OK)
		pStmOpts->store ( strkCr, bCr );
	if (pOpts->load ( strkTr, v ) == S_OK)
		pStmOpts->store ( strkTr, v );
	if (pOpts->load ( strkAsync, v ) == S_OK)
		pStmOpts->store ( strkAsync, v );
	if (pOpts->load ( adtString(L"Mode"), v ) == S_OK)
		pStmOpts->store ( adtString(L"Mode"), v );
	if (pOpts->load ( adtString(L"Sync"), v ) == S_OK)
		pStmOpts->store ( adtString(L"Sync"), v );
	if (pOpts->load ( adtString(L"SyncData"), v ) == S_OK)
		pStmOpts->store ( adtString(L"SyncData"), v );

	// Create a file stream resource
	CCLTRY ( COCREATE ( L"Io.StmFile", IID_IResource, &pRes ) );

	// Attempt to open stream
	if (hr == S_OK && strLocFull[strLocFull.length()-1] != '/')
		{
		// Attempt open
		hr = pRes->open ( pStmOpts );

		// It turns out verifying the existing of the full path every time is slow,
		// so only do it if opening the file fails.
		if (hr != S_OK && bRO == false && bCr == true)
			{
			// Verify path exists
			hr = createLoc ( strLocFull );

			// Attempt open again
			CCLTRY ( pRes->open ( pStmOpts ) );
			}	// if
		}	// if

	// Just verify path exists
	else if (hr == S_OK)
		hr = createLoc ( strLocFull );

	// Attempt to 'open' a location, generally used to ensure location exists

	// Ensure that the location of the stream exists within the source
//	dbgprintf ( L"StmSrcFile::open:0x%x:%s:RO:%d:Cr:%d\r\n", hr, (LPCWSTR)strLocFull, 
//					(bool) bRO, (bool) bCr );
//	if (hr == S_OK && bRO == false && bCr == true)
//		hr = createLoc ( strLocFull );

	// Result
	CCLTRY ( _QISAFE(pRes,IID_IByteStream,ppLoc) );

	// Clean up
	_RELEASE(pRes);

	return hr;
	}	// open

HRESULT StmSrcFile :: move ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Move a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtString	strF,strT;

	// Generate full paths
	CCLTRY ( toPath ( pwF, strF ) );
	CCLTRY ( toPath ( pwT, strT ) );

	// Windows
	#ifdef	_WIN32
	// Execute
	CCLTRYE ( MoveFile ( strF, strT ) == TRUE, GetLastError() );
	#elif __APPLE__ || __unix__
	char *pcF = NULL;
	char *pcT = NULL;

	// Calls need ASCII
	CCLTRY ( strF.toAscii ( &pcF ) );
	CCLTRY ( strT.toAscii ( &pcT ) );

	// Execute
	CCLTRYE ( (rename ( pcF, pcT ) == 0), errno );

	// Clean up
	_FREEMEM(pcF);
	_FREEMEM(pcT);
	#endif

	lprintf ( LOG_DBG, L"Move : %s : %s\r\n", pwF, pwT );
	return hr;
	}	// move

HRESULT StmSrcFile :: remove ( const WCHAR *pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Remove/delete the location from the collection
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	bool			bDir	= (pwLoc[0] != WCHAR('\0') && pwLoc[wcslen(pwLoc)-1] == '/');
	adtString	strDel;

	// Generate full path to stream using source options
	CCLTRY ( toPath ( pwLoc, strDel ) );

	#ifdef	_WIN32

	// Attempt removal of directory
	if (hr == S_OK && bDir)
		hr = ( RemoveDirectory ( strDel ) == TRUE ) ? S_OK : GetLastError();

	// Attempt removal of file
	else if (hr == S_OK && !bDir)
		hr = ( DeleteFile ( strDel ) == TRUE ) ? S_OK : GetLastError();

	#elif defined(__APPLE__) || defined(__unix__)
	char *pcLoc = NULL;

	// Convert to ASCII version
	CCLTRY ( strDel.toAscii ( &pcLoc ) );

	// Attempt removal of directory
	if (hr == S_OK && bDir)
		hr = ( ::rmdir ( pcLoc ) == 0 ) ? S_OK : errno;

	// Attempt removal of file
	else if (hr == S_OK && !bDir)
		hr = ( ::remove ( pcLoc ) == 0 ) ? S_OK : errno;

	// Clean up
	_FREEMEM(pcLoc);
	#endif

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Unable to delete file %s (0x%x)\r\n", 
						(LPCWSTR)strDel, hr );

	return hr;
	}	// remove

HRESULT StmSrcFile :: resolve ( const WCHAR *pwLoc, bool bAbs, 
											ADTVALUE &vLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Resolve provided location to an absolute or relative path
	//			within the stream source.
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//		-	bAbs is true to produce an absolute path, or false to produce
	//			a relative path
	//		-	vLoc will receive the new location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;

	// To absolute
	if (bAbs)
		{
		adtString strLocFull;

		// Use default behaviour
		CCLTRY ( toPath ( pwLoc, strLocFull ) );

		// Result
		CCLTRY ( adtValue::copy ( strLocFull, vLoc ) );
		}	// if

	// To relative
	else
		{
		adtValue		vL;
		adtString	strRoot,strLocRel;
		U32			len;

		// A relative path make sense only if the prefix matches the
		// default root.

		// Load root in case it has changed
		CCLTRY ( pDct->load ( adtString(L"Root"), vL ) );
		CCLTRYE( (strRoot = vL).length() > 0, E_UNEXPECTED );

		// Prefix the same ?
		if (	hr == S_OK && 
				wcslen(pwLoc) > (len = strRoot.length()) &&
				!WCASENCMP(pwLoc,strRoot,len) )
			strLocRel = &(pwLoc[len]);
		else
			strLocRel = pwLoc;

		// Result
		CCLTRY ( adtValue::copy ( strLocRel, vLoc ) );			
		}	// else

	return hr;
	}	// resolve

HRESULT StmSrcFile :: status ( const WCHAR *wLoc, IDictionary *pSt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns information about the stream at the specified location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	pSt will receive the information about the stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	adtString	strLoc;
	adtDate		dC,dA,dM;
	
	// Generate full path to stream using source options
	CCLTRY ( toPath ( wLoc, strLoc ) );

	#ifdef	_WIN32
		{
		HANDLE							hFile	= INVALID_HANDLE_VALUE;
		BY_HANDLE_FILE_INFORMATION	hfi;
		SYSTEMTIME						sct,sat,smt;
		
		// Access file
		CCLTRYE( (hFile = CreateFile ( strLoc, GENERIC_READ,
												FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,
												NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL )) != INVALID_HANDLE_VALUE,
						GetLastError() );

		// File attributes
		CCLTRYE ( GetFileInformationByHandle ( hFile, &hfi ) == TRUE,  GetLastError() );

		// Convert file times to system date
		CCLTRYE ( FileTimeToSystemTime ( &(hfi.ftCreationTime), &sct ) == TRUE, GetLastError() );
		CCLTRY  ( adtDate::fromSystemTime ( &sct, &(dC.vdate) ) );
		CCLTRYE ( FileTimeToSystemTime ( &(hfi.ftLastAccessTime), &sat ) == TRUE, GetLastError() );
		CCLTRY  ( adtDate::fromSystemTime ( &sat, &(dA.vdate) ) );
		CCLTRYE ( FileTimeToSystemTime ( &(hfi.ftLastWriteTime), &smt ) == TRUE, GetLastError() );
		CCLTRY  ( adtDate::fromSystemTime ( &smt, &(dM.vdate) ) );

		// Store information
		CCLTRY ( pSt->store ( adtString(L"Size"), adtInt(hfi.nFileSizeLow) ) );
		
		// Directory ?
		if (hr == S_OK && (hfi.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
			ULARGE_INTEGER	tb,tf;

			// Is a folder
			CCLTRY ( pSt->store ( adtString(L"Folder"), adtBool(true) ) );

			// Total/Free
			if (hr == S_OK && GetDiskFreeSpaceEx ( strLoc, NULL, &tb, &tf ))
				{
				CCLTRY ( pSt->store ( adtString(L"TotalBytes"), adtLong(tb.QuadPart) ) );
				CCLTRY ( pSt->store ( adtString(L"TotalFree"), adtLong(tf.QuadPart) ) );
				}	// if

			}	// if

		// Clean up
		if (hFile != INVALID_HANDLE_VALUE)
			CloseHandle ( hFile );
		}
	#elif __APPLE__ || __unix__
	char			*pcLoc	= NULL;
	struct stat	lstat;
	
	// ASCII version of location
	CCLTRY ( strLoc.toAscii ( &pcLoc ) );

	// File status
	CCLTRYE ( (stat ( pcLoc, &lstat ) == 0), GetLastError() );

	// Convert times to dates
	#if defined(__APPLE__) 
	CCLTRY ( adtDate::fromEpochSeconds ( (S32)lstat.st_mtimespec.tv_sec, lstat.st_mtimespec.tv_nsec/1000, &(dM.vdate) ) );
	CCLTRY ( adtDate::fromEpochSeconds ( (S32)lstat.st_ctimespec.tv_sec, lstat.st_ctimespec.tv_nsec/1000, &(dC.vdate) ) );
	CCLTRY ( adtDate::fromEpochSeconds ( (S32)lstat.st_atimespec.tv_sec, lstat.st_atimespec.tv_nsec/1000, &(dA.vdate) ) );
	#else
	CCLTRY ( adtDate::fromEpochSeconds ( (S32)lstat.st_mtim.tv_sec, lstat.st_mtim.tv_nsec/1000, &(dM.vdate) ) );
	CCLTRY ( adtDate::fromEpochSeconds ( (S32)lstat.st_atim.tv_sec, lstat.st_atim.tv_nsec/1000, &(dA.vdate) ) );
	CCLTRY ( adtDate::fromEpochSeconds ( (S32)lstat.st_mtim.tv_sec, lstat.st_mtim.tv_nsec/1000, &(dM.vdate) ) );
	#endif

	// File size
	CCLTRY ( pSt->store ( adtString(L"Size"), adtLong(lstat.st_size) ) );

	// Directory ?
	if (hr == S_OK && S_ISDIR(lstat.st_mode))
		{
		struct statvfs statfs;
		
		// Is a folder
		CCLTRY ( pSt->store ( adtString(L"Folder"), adtBool(true) ) );

		// Total/Free
		if (hr == S_OK && statvfs ( pcLoc, &statfs ) == 0)
			{
			CCLTRY ( pSt->store ( adtString(L"TotalBytes"), adtLong(statfs.f_bsize*statfs.f_blocks) ) );
			CCLTRY ( pSt->store ( adtString(L"TotalFree"), adtLong(statfs.f_bsize*statfs.f_bfree) ) );
			}	// if

		}	// if

	// Clean up
	_FREEMEM(pcLoc);
	#endif
	
	// Store information
	CCLTRY ( pSt->store ( adtString(L"Creation"), dC ) );
	CCLTRY ( pSt->store ( adtString(L"Accessed"), dA ) );
	CCLTRY ( pSt->store ( adtString(L"Modified"), dM ) );

	return hr;
	}	// status

HRESULT StmSrcFile :: toPath ( const WCHAR *wLoc, adtString &sLocFull )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocations
	//
	//	PURPOSE
	//		-	Generates a 'full path' to the specified file.
	//
	//	PARAMETERS
	//		-	wLoc contains the file or partial path to a file.
	//		-	sFull will receive the full path to the file
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr 		= S_OK;

	// Generate a full path if not already an absolute path
	if (	hr == S_OK					&& 
			wcslen(wLoc) > 0			&&
			wLoc[0] != WCHAR('\\')	&&
			wLoc[0] != WCHAR('/')
			#if	defined(_WIN32) && !defined(UNDER_CE)
			&& wLoc[1] != WCHAR(':')					// Drive letter ?
			#endif
		)
		{
		// Load root in case it has changed
		CCLTRY ( pDct->load ( adtString(L"Root"), strRoot ) );

		// Generate path that includes the default root
		U32
		len = strRoot.length()+(U32)wcslen(wLoc)+1;
		CCLTRY ( sLocFull.allocate ( len )  );
		CCLOK	 ( WCSCPY ( &sLocFull.at(), len, strRoot ); )
		CCLOK	 ( WCSCAT ( &sLocFull.at(), len, wLoc ); )
		}	// if

	// Using root directly ?
	else if (wcslen(wLoc) == 0)
		hr = pDct->load ( adtString(L"Root"), sLocFull );

	// Absolute paths are unmodified
	else if (hr == S_OK)
		sLocFull = wLoc;

	return hr;
	}	// toPath

