////////////////////////////////////////////////////////////////////////
//
//									StmSrcStms.CPP
//
//						Stream source in single stream.
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"

StmSrcStms :: StmSrcStms ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	punkDct		= NULL;
	pDct			= NULL;
	pStmSrc		= NULL;
	pStmSrcSrc	= NULL;
	pStmOpts		= NULL;
	pStmPer		= NULL;
	strRoot		= L"";
	bWrOp			= false;
	uStmCnt		= 0;
	}	// SysStmsFile

HRESULT StmSrcStms :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Aggregrate dictionary
	CCLTRY ( COCREATEA ( L"Adt.Dictionary", (ILocations *) this, &punkDct ) );

	// Interfaces
	CCLTRY ( _QI(punkDct,IID_IDictionary,&pDct) );
	CCLOK  ( pDct->Release(); )						// Cancel 'QI'

	// For persisting stream headers
	CCLTRY ( COCREATE ( L"Io.StmPrsBin", IID_IStreamPersist, &pStmPer ) );

	// For stream options
	CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pStmOpts ) );

	return hr;
	}	// construct

void StmSrcStms :: closed ( StmSrcStm *pStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when an internal stream has been closed.
	//
	//	PARAMETERS
	//		-	pStm is the stream object
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;

	// State check
	if (pStm == NULL)
		return;

	// If writable stream is closed, the size must be updated.
	// NOTE: Only the last stream can be writable to allow for growth.
	if (!pStm->bRead)
		bWrOp = false;

	// One less open stream
	if (uStmCnt > 0 && --uStmCnt == 0)
		{
		// Auto-close the archive file on the last stream.
		_RELEASE(pStmSrc);
		_RELEASE(pStmSrcSrc);
		}	// if

	}	// closed

HRESULT StmSrcStms :: copy ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Copy a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// If necessary to support, copy existing stream to a new stream 
	// at the end ?
	return ERROR_NOT_SUPPORTED;

	}	// copy

void StmSrcStms :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	_RELEASE(pStmPer);
	_RELEASE(pStmSrc);
	_RELEASE(pStmSrcSrc);
	_RELEASE(pStmOpts);
	_RELEASE(punkDct);
	}	// destruct

HRESULT StmSrcStms :: flush ( ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Flushes or synchronous the stream source.
	//
	//	PARAMETERS
	//		-	v is a value for possible future use
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// State check
	CCLTRYE ( pStmSrc != NULL, ERROR_INVALID_STATE );

	// Commit changes
	CCLTRY ( pStmSrcSrc->flush(v) );

	return hr;
	}	// flush

HRESULT StmSrcStms :: link ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Create a symbolic link to a location.
	//
	//	PARAMETERS
	//		-	pwF is the source/link location
	//		-	pwT is the destination/target location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// link

HRESULT StmSrcStms :: locations ( const WCHAR *wLoc, IIt **ppIt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns an iterator for the stream locations at the specified
	//			location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	ppIt will receive a stream location iterator.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// locations

HRESULT StmSrcStms :: open ( IDictionary *pOpts, IUnknown **ppLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Open a a location inside the locations.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the stream.
	//		-	ppLoc will receive the location object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	IDictionary	*pDctLoc	= NULL;
	StmSrcStm	*pStm		= NULL;
	bool			bFound	= false;
	U64			uStmAt	= 0;
	adtString	strLoc;
	adtBool		bRead,bCreate,bTrunc;
	adtValue 	vL;
	adtIUnknown	unkV;

	// Options.
	if (hr == S_OK && pOpts->load ( adtString(L"ReadOnly"), vL ) == S_OK)
		bRead = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Create"), vL ) == S_OK)
		bCreate = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Truncate"), vL ) == S_OK)
		bTrunc = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Location"), vL ) == S_OK)
		{
		adtString	strLocTmp;

		// Extract string
		hr = adtValue::toString ( vL, strLocTmp );

		// Remove any root identifier, everything relative but support root.
		if (strLocTmp[0] == '/')
			strLoc = &strLocTmp.at(1);
		else
			hr = adtValue::copy ( strLocTmp, strLoc );

		// Local ownership
		strLoc.at();
		}	// if

	// The master byte stream in the provided source must be
	// access first before any activity.
	if (hr == S_OK && pStmSrcSrc == NULL)
		{
		adtString	strLocSrc;

		// Required are the stream source and the source location name
		CCLTRY ( pDct->load ( adtString(L"Source"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_ILocations,&pStmSrcSrc) );
		CCLOK  ( unkV = NULL; )
		CCLTRY ( pDct->load ( adtString(L"LocationSource"), vL ) );
		CCLTRYE( (strLocSrc = vL).length() > 0, ERROR_NOT_FOUND );

		// Access the source stream with the same options to ensure
		// proper creation and access control
		// NOTE: Truncation/re-do will have to be done external, cannot
		// truncate every time the master file opens.
		CCLTRY ( pStmOpts->clear() );
		CCLTRY ( pStmOpts->store ( adtString(L"Location"), strLocSrc ) );
		CCLTRY ( pStmOpts->store ( adtString(L"ReadOnly"), bRead ) );
		CCLTRY ( pStmOpts->store ( adtString(L"Create"), bCreate ) );
		CCLTRY ( pStmSrcSrc->open ( pStmOpts, &(unkV.punk) ) );
		CCLTRY ( _QISAFE(unkV,IID_IByteStream,&pStmSrc) );

		// Clean up
		if (hr != S_OK)
			{
			_RELEASE(pStmSrc);
			_RELEASE(pStmSrcSrc);
			}	// if

		}	// if

	// If read only, the location must already exist,
	// if writable, the location cannot already exist.
//	CCLTRYE (	(bRead == true && pDctLocs->load ( strLoc, vL ) == S_OK) ||
//					(bRead == false && pDctLocs->load ( strLoc, vL ) != S_OK),
//					ERROR_NOT_SUPPORTED );

	// Attempt to find the stream inside the container
	if (hr == S_OK)
		{
		// Start from beginning 
		CCLTRY ( pStmSrc->seek ( 0, STREAM_SEEK_SET, NULL ) );
		while (!bFound && pStmPer->load ( pStmSrc, vL ) == S_OK)
			{
			IDictionary	*pDctHdr	= NULL;
			adtIUnknown unkV(vL);
			adtString	strLocHdr;
			adtLong		lSz;

			// Extract parameters
			CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctHdr) );
			CCLTRY ( pDctHdr->load ( adtString(L"Path"), vL ) );
			CCLTRYE( (strLocHdr = vL).length () > 0, E_UNEXPECTED );
			CCLTRY ( pDctHdr->load ( adtString(L"Size"), vL ) );
			if (hr == S_OK)
				{
				// Matching name ?
				bFound = (!WCASECMP(strLocHdr,strLoc));

				// If not found, proceed to next header
				if (!bFound)
					hr = pStmSrc->seek ( (lSz = vL), STREAM_SEEK_CUR, &uStmAt );
				}	// else

			// Clean up
			_RELEASE(pDctHdr);
			}	// while

		}	// if

	// Read only means the stream must already exist
	if (hr == S_OK && bRead == true && !bFound)
		hr = ERROR_NOT_FOUND;

	// Not read-only
	else if (hr == S_OK && !bRead)
		{
		// Cannot truncate an existing stream
		CCLTRYE ( bTrunc == false || (bTrunc == true && !bFound), 
						ERROR_NOT_SUPPORTED );

		// One writable location at a time
		CCLTRYE ( bWrOp == false, ERROR_ALREADY_EXISTS );

		// New location ?
		if (hr == S_OK && !bFound && bCreate == true)
			{
			IDictionary	*pDctHdr	= NULL;

			// Create header dictionary for new stream
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctHdr ) );
			CCLTRY ( pDctHdr->store ( adtString(L"Path"), strLoc ) );
			CCLTRY ( pDctHdr->store ( adtString(L"Size"), adtLong(0) ) );

			// Seek to the end of the stream and save header
			csStm.enter();
			CCLTRY ( pStmSrc->seek ( 0, STREAM_SEEK_END, &uStmAt ) );
			CCLTRY ( pStmPer->save ( pStmSrc, adtIUnknown(pDctHdr) ) );
			csStm.leave();

			// Clean up
			_RELEASE(pDctHdr);
			}	// if

		// Write access can only occur on the last stream
		}	// else if

	// If successful, open byte stream on location
	CCLTRYE	( (pStm = new StmSrcStm ( this, uStmAt, bRead )) != NULL, 
					E_OUTOFMEMORY );
	CCLOK		( pStm->AddRef(); )
	CCLTRY	( pStm->construct() );
	CCLTRY	( _QI(pStm,IID_IUnknown,ppLoc) );
	CCLOK		( ++uStmCnt; )
	if (hr == S_OK && !bRead)
		bWrOp = true;

	// Clean up
	_RELEASE(pStm);
	_RELEASE(pDctLoc);

	return hr;
	}	// open

HRESULT StmSrcStms :: move ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Move a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return ERROR_NOT_SUPPORTED;
	}	// move

HRESULT StmSrcStms :: remove ( const WCHAR *pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Remove/delete the location from the collection
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return ERROR_NOT_SUPPORTED;
	}	// remove

HRESULT StmSrcStms :: resolve ( const WCHAR *pwLoc, bool bAbs, 
											ADTVALUE &vLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Resolve provided location to an absolute or relative path
	//			within the stream source.
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//		-	bAbs is true to produce an absolute path, or false to produce
	//			a relative path
	//		-	vLoc will receive the new location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// resolve

HRESULT StmSrcStms :: status ( const WCHAR *wLoc, IDictionary *pSt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns information about the stream at the specified location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	pSt will receive the information about the stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// status

HRESULT StmSrcStms :: toPath ( const WCHAR *wLoc, adtString &sLocFull )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocations
	//
	//	PURPOSE
	//		-	Generates a 'full path' to the specified file.
	//
	//	PARAMETERS
	//		-	wLoc contains the file or partial path to a file.
	//		-	sFull will receive the full path to the file
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr 		= S_OK;

	// Generate a full path if not already an absolute path
	if (	hr == S_OK					&& 
			wcslen(wLoc) > 0			&&
			wLoc[0] != WCHAR('\\')	&&
			wLoc[0] != WCHAR('/')
			#if	defined(_WIN32) && !defined(UNDER_CE)
			&& wLoc[1] != WCHAR(':')					// Drive letter ?
			#endif
		)
		{
		// Load root in case it has changed
		CCLTRY ( pDct->load ( adtString(L"Root"), strRoot ) );

		// Generate path that includes the default root
		U32
		len = strRoot.length()+(U32)wcslen(wLoc)+1;
		CCLTRY ( sLocFull.allocate ( len )  );
		CCLOK	 ( WCSCPY ( &sLocFull.at(), len, strRoot ); )
		CCLOK	 ( WCSCAT ( &sLocFull.at(), len, wLoc ); )
		}	// if

	// Using root directly ?
	else if (wcslen(wLoc) == 0)
		hr = pDct->load ( adtString(L"Root"), sLocFull );

	// Absolute paths are unmodified
	else if (hr == S_OK)
		sLocFull = wLoc;

	return hr;
	}	// toPath
