////////////////////////////////////////////////////////////////////////
//
//									StreamBlock.CPP
//
//					Implementation of the stream block node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

StreamBlock :: StreamBlock ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	bBegin	= false;
	bEnd		= false;
	bEsc		= false;
	szWrite	= 0;
	pStmSrc	= NULL;
	pStmDst	= NULL;
	iBegin	= (U32)0x100;
	iEnd		= (U32)0x100;
	iSzMrk	= 0;
	uMarkCnt	= 0;
	iEsc		= 0;
	pcMark	= NULL;
	bDbg		= false;
	}	// StreamBlock

void StreamBlock :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pStmSrc);
	_RELEASE(pStmDst);
	}	// destruct

HRESULT StreamBlock :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Begin"), vL ) == S_OK)
			iBegin = vL;

		// Size of marker (1,2,4 bytes) (0 means to start marker)
		if (pnDesc->load ( adtString(L"SizeMark"), vL ) == S_OK)
			iSzMrk = vL;

		// Ensure enough room for marke
		CCLTRYE ( (pcMark = (BYTE *) _ALLOCMEM ( iSzMrk + 1 )) != NULL, E_OUTOFMEMORY );

		// End byte or size (mutually exclusive)
		if (pnDesc->load ( adtString(L"End"), vL ) == S_OK)
			iEnd = vL;
		else if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			iSize = vL;

		// For escaping begin/end markers
		if (pnDesc->load ( adtString(L"Escape"), vL ) == S_OK)
			iEsc = vL;

		if (pnDesc->load ( adtString(L"Debug"), vL ) == S_OK)
			bDbg = vL;
		}	// if

	// Detach
	else
		{
		_RELEASE(pStmSrc);
		_RELEASE(pStmDst);
		}	// if

	return hr;
	}	// onAttach

HRESULT StreamBlock :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Perform blocking
	if (_RCP(Fire))
		{
		BYTE	b;

		// State check
		CCLTRYE ( (pStmSrc != NULL && pStmDst != NULL), ERROR_INVALID_STATE );

		// Read bytes from source stream transfering them to the destination stream
		while (hr == S_OK && pStmSrc->read ( &b, 1, NULL ) == S_OK)
			{
			bool bMark = false;

			// Debug
			if (bDbg)
				{
				lprintf ( LOG_DBG, L"%s : bBegin %s iSzMrk %d uMarkCnt %d iBegin 0x%x szWrite %d nextb 0x%x (%d)\r\n", 
					(LPCWSTR)strnName, (bBegin == true) ? L"true" : L"false", iSzMrk.vint, uMarkCnt, (U32)iBegin,
					 szWrite, b, b );
				for (int i = 0;i < (int)uMarkCnt;++i)
					lprintf ( LOG_DBG, L"pcMark[%d] = 0x%x (%d)\r\n", i, pcMark[i], pcMark[i] );
				}	// if

			// If no marker is specified, no begin marker necessary.
			if (bBegin == false && iSzMrk == 0)
				bBegin = true;

			// Marker check ?
			if (bBegin == false || iSize == 0)
				{
				// Add byte to marker search.
				pcMark[uMarkCnt++] = b;

				// Marker to look for 
				adtInt	iMrkSrch = (!bBegin) ? iBegin : iEnd;

				// Correct size ?
				if (uMarkCnt == iSzMrk)
					{
					// Debug
//					lprintf ( LOG_DBG, L"Marker size reached (%d)\r\n", uMarkCnt );

					// Marker match ?
					if (	((iSzMrk == 1) && pcMark[0] == iMrkSrch) 			||
							((iSzMrk == 2) && *((U16 *)pcMark) == iMrkSrch) 	||
							((iSzMrk == 4) && *((U32 *)pcMark) == iMrkSrch) 	||
							((iSzMrk == 8) && *((U64 *)pcMark) == iMrkSrch)  )
						{
						// Marker found
						bMark 	= true;
						
						// Reset marker search
						uMarkCnt	= 0;
//						lprintf ( LOG_DBG, L"Marker matched (0x%x)\r\n", (U32)iMrkSrch );
						}	// if
					else
						{
						// Shift bytes over for continued marker search
						--uMarkCnt;
						if (uMarkCnt)
							memmove ( pcMark, &pcMark[1], uMarkCnt );
						}	// else
						
					}	// if
															
				}	// if

			// Marker found ?
			if (bMark == true)
				{
				// Waiting for beginning ?
				if (bBegin == false)
					bBegin = true;

				// Waiting for ending ?
				else if (bEnd == false)
					bEnd = true;
				}	// if

			// Inside beginning marker but not at end
			else if (bMark == false && bBegin == true)
				{
				// Write the byte to the output stream
				CCLTRY ( pStmDst->write ( &b, 1, NULL ) );
				CCLOK  ( ++szWrite; )
//				lprintf ( LOG_DBG, L"Byte saved 0x%x total %d\r\n", b, szWrite );
				}	// else if

			// Inside beginning and ending marker or size reached
			if (	bBegin == true && 
					(bEnd == true || (iSize != 0 && iSize == szWrite)) )
				{
				// Debug
//				lprintf ( LOG_DBG, L"Result szWrite %d uMarkCnt %d pStmDst %p\r\n", szWrite, uMarkCnt, pStmDst );

				// Reset state for next block
				bBegin 	= false;
				bEnd		= false;
				szWrite	= 0;
				uMarkCnt	= 0;
				bEsc		= false;
				
				// Block is complete
				_EMT(Fire,adtIUnknown(pStmDst));
				break;
				} // if				

			}	// while

		}	// if

	// Reset
	else if (_RCP(Reset))
		{
		// Start over
		bBegin 	= false;
		bEnd		= false;
		szWrite	= 0;
		uMarkCnt	= 0;
		bEsc		= false;
		}	// else if

	// State
	else if (_RCP(Source))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStmSrc);
		hr = _QI(unkV,IID_IByteStream,&pStmSrc);
		}	// else if
	else if (_RCP(Destination))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStmDst);
		hr = _QI(unkV,IID_IByteStream,&pStmDst);

		// Assume a new destination means new blocks (?)
		bBegin 	= false;
		bEnd		= false;
		szWrite	= 0;
		uMarkCnt	= 0;
		bEsc		= false;
		}	// else if
	else if (_RCP(Size))
		iSize = adtInt(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

