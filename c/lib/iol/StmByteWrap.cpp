////////////////////////////////////////////////////////////////////////
//
//									StmByteWrap.CPP
//
//				Implements a stream based circular byte buffer
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"

// Definitions
#define	SIZE_COPY_BFR	0x100000

StmByteWrap :: StmByteWrap ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pQ		= NULL;
	pcQ	= NULL;
	qn		=	0;
	qoff	=	0;
	sz		=	0;
	puCpy	= NULL;
	}	// StmByteWrap

HRESULT StmByteWrap :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// For this queue, there is always 'sz' amount of bytes available.
	if (puAv != NULL)
		*puAv = sz;

	return hr;
	}	// available

HRESULT StmByteWrap :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Initial state
	sz	= 512;
	qn	= 0;

	// Create a memory mapped object to use for the queue with a default size.
	CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pQ ) );
	CCLTRY ( pQ->setSize ( sz ) );
	CCLTRY ( pQ->getInfo ( (void **) &pcQ, NULL ) );

	return hr;
	}	// construct

HRESULT StmByteWrap :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
	U64		nio,nw,nr;

	// Amount of data available to read
	CCLTRY ( available(&nio) );

	// Amount to copy based on requested amount
	CCLOK ( uSz = (uSz != 0 && uSz < nio) ? uSz : nio; )

	// Using on demand, large, internal copy buffer for speed
	if (hr == S_OK && puCpy == NULL)
		{
		CCLTRYE ( (puCpy = (U8 *) _ALLOCMEM ( SIZE_COPY_BFR )) != NULL,
						E_OUTOFMEMORY );
		}	// if

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK ( nio = (SIZE_COPY_BFR < uSz) ? SIZE_COPY_BFR : uSz; )
		CCLTRY( read ( puCpy, nio, &nr ) );

		// Write full block to stream
		CCLOK ( fp		= puCpy; )
		CCLOK ( nleft	= nr; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nr; )
		if (hr == S_OK && puSz != NULL)
			*puSz += nr;
		}	// while

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"Failed:0x%x\r\n", hr );

	return hr;
	}	// copyTo

void StmByteWrap :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(pQ);
	_FREEMEM(puCpy);
//	dbgprintf ( L"StmByteWrap::destruct:%p\r\n", this );
	}	// destruct

HRESULT StmByteWrap :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Nothing to do for this object
	return S_OK;
	}	// flush

HRESULT StmByteWrap :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U8			*pcBfr	= (U8 *)pvBfr;
	S32		at			= 0;
	U64		nt,nrf,nr;

	// Total amount of available bytes
	CCLTRY ( available(&nt) );

	// Total amount to read
	CCLOK ( nt = (nio < nt) ? nio : nt; )

	// Starting position of read taking into account the current offset.
	CCLOK ( at = ( (qn + qoff + sz) - nt ) % sz; );

	// Amount to read first ( to avoid copying past end )
	nrf = (at + nt > sz) ? (sz-at) : nt;

	// Copy out available bytes
	nr = 0;
	if (hr == S_OK && nt > 0)
		{
		// Copy portion of cache
		memcpy ( pcBfr, &pcQ[at], nrf );

		// Bytes consumed
		at	= ( (at + nrf ) % sz );
		nr	= nrf;

		// More to read ?
		if (hr == S_OK && nrf < nt)
			{
			// Copy portion of cache
			memcpy ( &pcBfr[nr], &pcQ[at], (nt-nrf) );

			// Bytes consumed
			at		= ( (at + (nt-nrf) ) % sz );
			nr		+= (nt-nrf);
			}	// if

		}	// if

	// Results
	if (hr == S_OK && pnio != NULL)
		*pnio = nr;

	return hr;
	}	// read

HRESULT StmByteWrap :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// This just seeks the read offsets and STREAM_SEEK_CUR are supported.
	qoff = (uFrom == STREAM_SEEK_CUR && sPos < 0) ? (S32) sPos : 0;

	// Result
	if (puPos != NULL)
		*puPos = qoff;

	return hr;
	}	// seek

HRESULT StmByteWrap :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;

	// Reset state
	sz	= (U32)uSz;
	qn	= 0;

	// Create a memory mapped object to use for the queue with a default size.
	CCLTRY ( pQ->setSize ( sz ) );
	CCLTRY ( pQ->getInfo ( (void **) &pcQ, NULL ) );

	return hr;
	}	// setSize

HRESULT StmByteWrap :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	const U8	*pcBfr	= (const U8 *)pcvBfr;
	U64		nw,at;

	// Set result now
	if (pnio != NULL)
		*pnio = nio;

	// Write all available bytes.  If amount to write is larger than queue
	// size, it will simply get overwritten when it wraps around.
	at = 0;
	while (hr == S_OK && nio > 0)
		{
		// Amount to write next
		nw = (qn + nio <= sz) ? nio : (sz - nio);

		// Copy next block
		memcpy ( &pcQ[qn], &pcBfr[at], nw );

		// Next block
		qn		= ( ( qn + nio ) % sz );
		nio	-= nw;
		at		+= nw;
		}	// while

	return hr;
	}	// write

