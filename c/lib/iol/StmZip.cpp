////////////////////////////////////////////////////////////////////////
//
//									StmZip.CPP
//
//						A stream inside a ZIP archive
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include "../ext/minizip/zip.h"
#include "../ext/minizip/unzip.h"

StmZip :: StmZip ( StmSrcZip *_pOwner )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	//	PARAMETERS
	//		-	_pOwner is the owner archive
	//
	////////////////////////////////////////////////////////////////////////
	pOwner	= _pOwner;
	_ADDREF(pOwner);
	}	// StmZip

HRESULT StmZip :: available ( U64 *puAv )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Retrieve the number of bytes available for reading.
	//
	//	PARAMETERS
	//		-	puAv will receive the available bytes
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = E_NOTIMPL;
	// Way to get full uncompressed size but not how many bytes remain ?
	return hr;
	}	// available

HRESULT StmZip :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Close the resource.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up, only one file allowed to be open at a time
	if (pOwner->pzip != NULL)
		{
		zipCloseFileInZip	( pOwner->pzip );
		zipClose				( pOwner->pzip, NULL );
		pOwner->pzip		= NULL;
		}	// if
	else if (pOwner->punzip != NULL)
		{
		unzCloseCurrentFile	( pOwner->punzip );
		unzClose ( pOwner->punzip );
		pOwner->punzip	= NULL;
		}	// if

	return S_OK;
	}	// close

HRESULT StmZip :: copyTo ( IByteStream *pStmDst, U64 uSz, U64 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Copies the specified # of bytes to another stream.
	//
	//	PARAMETERS
	//		-	pStmDst is the target stream
	//		-	uSz is the amount to copy
	//		-	puSz is the amount copied
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr 	= S_OK;
	U8			*fp	= NULL;
	U64		nleft	= 0;
	U64		nio,nw;
	int		err;

	// Setup
	if (puSz != NULL) *puSz = 0;

	// State check
	CCLTRYE ( pOwner->punzip != NULL,	ERROR_INVALID_STATE );

	// If size is not specified, assume entire stream is to be copied
	if (hr == S_OK && uSz == 0)
		{
		unz_file_info	ufi;

		// Obtain information about current file so the size is known
		CCLTRYE ( (err = unzGetCurrentFileInfo ( pOwner->punzip, &ufi, 
						NULL, 0, NULL, 0, NULL, 0 )) == UNZ_OK, err );

		// Copy uncompressed size
		CCLOK ( uSz = ufi.uncompressed_size; )
		}	// if

	// Read/write file
	while (hr == S_OK && uSz)
		{
		// Read next block
		CCLOK		( nio = (sizeof(filebufr) < uSz) ? sizeof(filebufr) : uSz; )
		CCLTRYE	( (err = unzReadCurrentFile ( pOwner->punzip, filebufr, (U32)nio )) > 0, err );
		CCLOK		( nio = err; )

		// End of stream ?
		if (hr == S_OK && nio == 0)
			break;

		// Write full block to stream
		CCLOK ( fp		= filebufr; )
		CCLOK ( nleft	= nio; )
		while (hr == S_OK && nleft)
			{
			// Write next block
			CCLTRY ( pStmDst->write ( fp, nleft, &nw ) );

			// Next block
			CCLOK ( nleft -= nw; )
			CCLOK ( fp += nw; )
			}	// while

		// Next block
		CCLOK ( uSz -= nio; )
		if (puSz != NULL)
			*puSz += nio;
		}	// while

	return hr;
	}	// copyTo

void StmZip :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	close();
	_RELEASE(pOwner);
//	dbgprintf ( L"StmZip::destruct:%p\r\n", this );
	}	// destruct

HRESULT StmZip :: flush ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Flush the stream state.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return S_OK;
	}	// flush

HRESULT StmZip :: getResId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Return an identifier for the resource.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Setup
	adtValue::clear ( vId );

	// Zip structure ?
	if (pOwner->pzip != NULL)
		adtValue::copy ( adtLong((U64)pOwner->pzip), vId );
	else if (pOwner->punzip != NULL)
		adtValue::copy ( adtLong((U64)pOwner->punzip), vId );
	
	return hr;
	}	// getResId

HRESULT StmZip :: open ( IDictionary *pOpts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Open a byte stream on top of a file.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the file.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	char			*paLoc	= NULL;
	StmZip		*pStmZip	= NULL;
	adtString	strLoc,strFile,strPath;
	adtBool		bRead,bCreate;
	adtValue 	vL;
	U32			len;

	// Options.  File = zip file, location = location within zip
	CCLTRY ( pOpts->load ( adtString(L"Location"), vL ) );
	CCLTRYE( (len = (strLoc = vL).length()) > 0, ERROR_INVALID_STATE );
	if (hr == S_OK && pOpts->load ( adtString(L"ReadOnly"), vL ) == S_OK)
		bRead = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Create"), vL ) == S_OK)
		bCreate = vL;

	// State check
	CCLTRYE (	(bRead == true && pOwner->punzip != NULL) ||
					(bRead == false && pOwner->pzip != NULL), ERROR_PATH_NOT_FOUND );

	// Access stream inside file
	if (hr == S_OK && bRead == true)
		{
		int	err;

		// State check
		CCLTRYE ( pOwner->punzip != NULL,		ERROR_INVALID_STATE );

		// Relative paths are supported inside a ZIP archive.  No root paths,
		// no drive letters and forward slashes only.
		CCLOK  ( strLoc.replace ( WCHAR('\\'), WCHAR('/') ); )

		// Ascii version of string
		CCLTRY ( strLoc.toAscii ( &paLoc ) );

		// Locate file withing archive
		CCLTRYE ( (err = unzLocateFile ( pOwner->punzip, paLoc, 0 )) == UNZ_OK, ERROR_FILE_NOT_FOUND );

		// Access file
		CCLTRYE ( (err = unzOpenCurrentFile ( pOwner->punzip )) == UNZ_OK, err );
		}	// if
	else if (hr == S_OK && bRead == false)
		{
		int				err;
		zip_fileinfo	zi;

		// State check
		CCLTRYE ( pOwner->pzip != NULL, ERROR_INVALID_STATE );

		// Relative paths are supported inside a ZIP archive.  No root paths,
		// no drive letters and forward slashes only.
		CCLOK  ( strLoc.replace ( WCHAR('\\'), WCHAR('/') ); )

		// Ascii version of string
		CCLTRY ( strLoc.toAscii ( &paLoc ) );

		// File information
		if (hr == S_OK)
			{
			// TODO: Perhaps allow real-time changes to options to allow a date/time
			// to be specified for a file ?  For now zero out the time and date so the
			// archive is byte equivalent to itself with the same source files.  This is to ensure
			// hashes calculated on archive with same source files will always be the same.
//			SYSTEMTIME	st;
			memset ( &zi, 0, sizeof(zi) );
//			GetLocalTime ( &st );
//			zi.tmz_date.tm_sec	= st.wSecond;
//			zi.tmz_date.tm_min	= st.wMinute;
//			zi.tmz_date.tm_hour	= st.wHour;
//			zi.tmz_date.tm_mday	= st.wDay;
//			zi.tmz_date.tm_mon	= st.wMonth-1;
//			zi.tmz_date.tm_year	= st.wYear;
			}	// if

		// Create a new file within the ZIP archive
		CCLTRYE ( (err = zipOpenNewFileInZip3 ( pOwner->pzip, 
						(paLoc[0] != '/') ? paLoc : &(paLoc[1]), &zi,
//						NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION, 0,
						NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_BEST_SPEED, 0,
						-MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY, NULL, 0 ))
						== UNZ_OK, err );
//		lprintf ( LOG_DBG, L"Open new file in zip : %S, hr 0x%x\r\n", paLoc, hr );

		// Debug
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"SysStmsZip::open:Unable to open %s\r\n", (PCWSTR) strLoc );
		}	// else if

	// Clean up
	_FREEMEM(paLoc);
	if (hr != S_OK)
		close();

	return hr;
	}	// open

HRESULT StmZip :: read ( void *pvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Reads the specified # of bytes from the stream.
	//
	//	PARAMETERS
	//		-	pvBfr will receive the data
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	int		err	= 0;

	// State check
	CCLTRYE ( pOwner->punzip != NULL,		ERROR_INVALID_STATE );

	// Read block
	CCLTRYE ( (err = unzReadCurrentFile ( pOwner->punzip, pvBfr, (U32)nio ))
					> 0, err );

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"SysStmZip::read:Error reading from file %d\r\n", err );

	// Result
	if (hr == S_OK && pnio != NULL) *pnio = err;

	return hr;
	}	// read

HRESULT StmZip :: seek ( S64 sPos, U32 uFrom, U64 *puPos )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Places the current byte position at the specified location.
	//
	//	PARAMETERS
	//		-	sPos is the new position
	//		-	uFrom specified where to start seek from
	//		-	puPos will receive the new position
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr = S_OK;
	unz_file_info	ufi;
	int				err;

	// Partial support for seek.  Seek is sometimes used to size the stream
	// by seeking to the end.  Support just that mode.
	if (hr == S_OK)
		{
		// From beginning
		if (uFrom == STREAM_SEEK_SET && sPos == 0)
			{
			if (puPos != NULL) *puPos = 0;
			}	// if

		// From current
		else if (uFrom == STREAM_SEEK_CUR && sPos == 0)
			{
			if (puPos != NULL) *puPos = 0;
			}	// if

		// From end
		else if (uFrom == STREAM_SEEK_END && sPos == 0)
			{
			// Obtain information about current file so the size is known
			CCLTRYE ( (err = unzGetCurrentFileInfo ( pOwner->punzip, &ufi, 
							NULL, 0, NULL, 0, NULL, 0 )) == UNZ_OK, err );

			// Result
			if (hr == S_OK && puPos != NULL)
				*puPos = ufi.uncompressed_size;
			}	// if

		// Not supported
		else
			hr = E_NOTIMPL;
		}	// if

	return hr;
	}	// seek

HRESULT StmZip :: setSize ( U64 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Sets the size of the stream.
	//
	//	PARAMETERS
	//		-	uSz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	// Undefined for ZIP ?
	return S_OK;
	}	// setSize

HRESULT StmZip :: write ( void const *pcvBfr, U64 nio, U64 *pnio )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IByteStream
	//
	//	PURPOSE
	//		-	Writes the specified # of bytes to the stream.
	//
	//	PARAMETERS
	//		-	pvBfr contains the data to write
	//		-	nio is the # of bytes to transfer
	//		-	pnio is the # of bytes transferred
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	int		err	= 0;

	// State check
	CCLTRYE ( pOwner->pzip != NULL,			ERROR_INVALID_STATE );

	// Write block
	CCLTRYE ( (err = zipWriteInFileInZip ( pOwner->pzip, pcvBfr, (U32)nio ))
					== ZIP_OK, err );

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"SysStmZip::write:Error writing to file %d,0x%x\r\n", err, hr );

	// Result
	if (hr == S_OK && pnio != NULL) *pnio = nio;

	return hr;
	}	// write

