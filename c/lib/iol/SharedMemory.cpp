////////////////////////////////////////////////////////////////////////
//
//									SharedMemory.CPP
//
//					Implementation of the shared memory object
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

SharedMemory :: SharedMemory ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	hMap		= NULL;
	pvMap		= NULL;
	uSzMap	= 0;
	}	// SharedMemory

HRESULT SharedMemory :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Close the resource.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (pvMap != NULL)
		{
		UnmapViewOfFile(pvMap);
		pvMap = NULL;
		}	// if
	if (hMap != NULL)
		{
		CloseHandle(hMap);
		hMap = NULL;
		}	// if
	uSzMap = 0;

	return S_OK;
	}	// close

void SharedMemory :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////
	close();
	}	// destruct

HRESULT SharedMemory :: getPersistId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IPersistValue
	//
	//	PURPOSE
	//		-	Return an object Id to use for persistence.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return adtValue::copy ( adtString(L"Io.SharedMemory"), vId );
	}	// getResId

HRESULT SharedMemory :: getResId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Return an identifier for the resource.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Underlying stream
	CCLTRY ( adtValue::copy ( adtLong((U64)hMap), vId ) );
	
	return hr;
	}	// getResId

HRESULT SharedMemory :: getInfo ( void **ppv, U32 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Retrieve pointer and/or the size of the region.
	//
	//	PARAMETERS
	//		-	ppv will receive the ptr.
	//		-	puSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr				= S_OK;

	// Valid block ?
	CCLTRYE	( (pvMap != NULL), ERROR_INVALID_STATE );

	// Pointer
	if (ppv != NULL)
		*ppv = (hr == S_OK) ? pvMap : NULL;

	// Size
	if (puSz != NULL)	
		*puSz = (hr == S_OK) ? uSzMap : 0;

	return hr;
	}	// getInfo

HRESULT SharedMemory :: load ( IStreamPersist *pPer, IByteStream *pStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IPersistValue
	//
	//	PURPOSE
	//		-	Load own values from persistence to initialize object.
	//
	//	PARAMETERS
	//		-	pPer is the value persistence interface
	//		-	pStm is the active byte stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;
	adtValue	vL;

	// Current state
	CCLOK ( close(); )

	// Load the location/name of the shared memory object.
	CCLTRY ( pPer->load(pStm,vL) );
	CCLTRY ( adtValue::toString(vL,strLoc) );

	// Open object, for now only accessing pre-existing regions are
	// supported from persistence
	CCLOK ( open ( strLoc, true, false, 0 ); )

	return hr;
	}	// load

HRESULT SharedMemory :: open ( IDictionary *pOpts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Open a byte stream on top of a file.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the file.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	bool			bCreated	= false;
	U32			*puMem	= NULL;
	adtString	strLoc;
	adtValue		vL;
	adtBool		bCr,bRo;
	adtInt		iSz;

	// Options
	CCLTRY ( pOpts->load ( adtString(L"Location"), vL ) );
	CCLTRYE ( ((strLoc = vL).length()) > 0, ERROR_INVALID_STATE );
	if (hr == S_OK && pOpts->load ( adtString(L"ReadOnly"), vL ) == S_OK)
		bRo = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Create"), vL ) == S_OK)
		bCr = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Size"), vL ) == S_OK)
		iSz = vL;

	// Access mapping
	CCLTRY ( open ( strLoc, bRo, bCr, iSz ) );

	return hr;
	}	// open

HRESULT SharedMemory :: open ( const WCHAR *pwLoc, bool bReadOnly, 
											bool bCreate, U32 iSize )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Open a byte stream on top of a file.
	//
	//	PARAMETERS
	//		-	pwLoc is the location/name to open
	//		-	bReadOnly is false for writable
	//		-	bCreate is true to create region if it does not exist
	//		-	iSize is the size of the region if creating
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	bool			bCreated	= false;
	adtValue		vL;

	// Access mapping
	if (hr == S_OK)
		{
		// Attempt to open an existing map
		CCLTRYE( (hMap = OpenFileMapping ( 
					(bReadOnly) ? FILE_MAP_READ : FILE_MAP_READ|FILE_MAP_WRITE,
					FALSE, pwLoc )) != NULL, GetLastError() );

		// If location did not exist, attempt to create if specified
		if (hr != S_OK && bCreate)
			{
			// Trying again
			hr = S_OK;

			// Size required on creation
			CCLTRYE( (iSize > 0), E_INVALIDARG );

			// Create new location (leave room for size stamp)
			CCLTRYE ( (hMap = CreateFileMapping ( INVALID_HANDLE_VALUE,
							NULL, (bReadOnly) ? PAGE_READONLY : PAGE_READWRITE, 0, 
							iSize+4, pwLoc )) != NULL, GetLastError() );

			// Map was created here
			CCLOK ( bCreated = true; )
			CCLOK ( uSzMap = iSize+4; )
			}	// if

		}	// if

	// Obtain ptr. to region
	CCLTRYE ( (pvMap = MapViewOfFile ( hMap,
					(bReadOnly) ? FILE_MAP_READ : FILE_MAP_READ|FILE_MAP_WRITE, 
					0, 0, 0 )) != NULL, GetLastError() );

	// If region was created, write size to beginning, this needs to be known
	// by all users of region
	if (hr == S_OK)
		{
		strLoc = pwLoc;
		if (bCreated)
			*((U32 *)pvMap) = uSzMap;
		else
			uSzMap = *((U32 *)pvMap);
		}	// if
	bRo = (hr == S_OK) ? bReadOnly : true;

	return hr;
	}	// open

HRESULT SharedMemory :: save ( IStreamPersist *pPer, IByteStream *pStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IPersistValue
	//
	//	PURPOSE
	//		-	Save own values to persistence.
	//
	//	PARAMETERS
	//		-	pPer is the value persistence interface
	//		-	pStm is the active byte stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Save the location/name of the shared memory object.  Obviously
	// persisting shared memory ony works at run-time and on the same machine.
	CCLTRY ( pPer->save(pStm,strLoc) );

	return hr;
	}	// save

HRESULT SharedMemory :: setInfo ( void *pv, U32 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Set static information about the memory block, can be
	//			called only one time.
	//
	//	PARAMETERS
	//		-	pv is the ptr. to the memory location
	//		-	uSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Not supported
	return E_NOTIMPL;
	}	// setInfo

HRESULT SharedMemory :: setSize ( U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Specifies what size the memory block should be sized too.
	//
	//	PARAMETERS
	//		-	sz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Setting the size may be the first called received if this object
	// is treated as a 'blind' memory block.  If this is the case, generate
	// a run-time name and attempt to create the block of the specified size.
	if (hMap == NULL)
		{
		WCHAR		wBfr[41];

		// Generate a unique run-time name for the block
		strLoc = L"nSpace_Io_SharedMemory_";

		// Use memory ptr. and Pid for uniqueness
		// TODO: Something else, ick
		swprintf ( SWPF(wBfr,40), L"%p", this );
		strLoc.append ( wBfr );
		strLoc.append ( L"_" );
		swprintf ( SWPF(wBfr,40), L"%x", GetCurrentProcessId() );
		strLoc.append ( wBfr );
		lprintf ( LOG_INFO,
						L"Creating new location '%s' of %d bytes\r\n",
						(LPCWSTR)strLoc, sz );

		// Open/create region under name, writable, create
		hr = open ( strLoc, false, true, sz );
		}	// if

	// At the moment increasing size is not supported
	else if (sz > uSzMap)
		{
		lprintf ( LOG_WARN, L"Unable to re-size existing shared memory object (%d,%d)\r\n",
									uSzMap, sz );
		hr = E_NOTIMPL;
		}	// else if

	// Otherwise resizing to smaller is fine (no-op)

	return hr;
	}	// setSize

HRESULT SharedMemory :: stream ( IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Creates a byte stream in front of the memory block.
	//
	//	PARAMETERS
	//		-	ppStm is the new stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	if (ppStm != NULL) *ppStm = NULL;
	return E_NOTIMPL;
	}	// stream

