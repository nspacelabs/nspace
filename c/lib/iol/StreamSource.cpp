////////////////////////////////////////////////////////////////////////
//
//									STMSRC.CPP
//
//					Implementation of the stream source node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

StreamSource :: StreamSource ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pOpt			= NULL;	
	pSrc			= NULL;
	pStm			= NULL;
	pStmsIt		= NULL;
	strRefLoc	= L"Location";
	}	// StreamSource

void StreamSource :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	onAttach(false);
	}	// destruct

HRESULT StreamSource :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Default location
		pnDesc->load ( strRefLoc, strLoc );

		// Default options
		if (pnDesc->load ( adtString(L"Options"), vL ) == S_OK)
			{
			adtIUnknown unkV(vL);
			CCLTRY(_QISAFE(unkV,IID_IDictionary,&pOpt));
			}	// if
		else
			{
			CCLTRY(COCREATE(L"Adt.Dictionary",IID_IDictionary,&pOpt));
			}	// else
		if (pnDesc->load ( adtString(L"ResolveNative"), vL) == S_OK)
			bResNat = adtBool(vL);
		}	// if
	
	// Detach
	else
		{
		_RELEASE(pStmsIt);
		_RELEASE(pStm);
		_RELEASE(pSrc);
		_RELEASE(pOpt);
		}	// else

	return S_OK;
	}	// onAttach

HRESULT StreamSource :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open
	if (_RCP(Open))
		{
		IUnknown	*pStmOp	= NULL;

		// State check
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );
		CCLTRYE ( pOpt != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );

		// Store location in options and access stream
		CCLTRY ( pOpt->store ( strRefLoc, strLoc ) );
		CCLTRY ( pSrc->open ( pOpt, &pStmOp ) );

		// Result
		if (hr == S_OK)	_EMT(Open,adtIUnknown(pStmOp) );
		else					_EMT(Error,adtInt(hr) );

		// Clean up
		_RELEASE(pStmOp);
		}	// if

	// First/next stream
	else if (_RCP(First) || _RCP(Next))
		{
		adtValue	vIt;

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// First time ?
		if (_RCP(First))
			{
			// New iterate
			_RELEASE(pStmsIt);
			CCLTRY(pSrc->locations(strLoc,&pStmsIt));

			// Ensure beginning
			CCLTRY(pStmsIt->begin());
			}	// if

		// State check
		CCLTRYE ( pStmsIt != NULL, ERROR_INVALID_STATE );

		// Emit next stream info.
		if (hr == S_OK && pStmsIt->read ( vIt ) == S_OK)
			{
			// Result
			_EMT(Next,vIt);

			// Move to next stream
			pStmsIt->next();
			}	// if
		else
			_EMT(End,adtInt());
		}	// else if

	// Status
		else if (_RCP(Status))
		{
		IDictionary	*pDct	= NULL;

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// Create dictionary to receive status
		CCLTRY ( COCREATE(L"Adt.Dictionary", IID_IDictionary, &pDct ) );

		// Obtain status
//		CCLOK  ( lprintf ( LOG_DBG, L"StreamSource::receive:Status:%s\r\n", (LPCWSTR)strLoc ); )
		CCLTRY ( pSrc->status ( strLoc, pDct ) );

		// Result
		if (hr == S_OK)
			_EMT(Status,adtIUnknown(pDct));
		else
			_EMT(Error,strLoc);

		// Clean up
		_RELEASE(pDct);
		}	// else if

	// Resolve location
	else if (_RCP(Resolve))
		{
		adtString	strLocRes,strLocNow;

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );

		// If location is empty, allow string to be passed in directly
		CCLTRY ( adtValue::copy ( strLoc, strLocNow ) );
		if (hr == S_OK && strLocNow.length() == 0)
			hr = adtValue::toString ( v, strLocNow );

		// Must have location
		CCLTRYE ( strLocNow.length() > 0, ERROR_INVALID_STATE );

		// Have stream source resolve location
		CCLTRY ( pSrc->resolve ( strLocNow, true, strLocRes ) );

		// Resolve to native format ?  Windows uses back slashes
		#ifdef	_WIN32
		if (hr == S_OK && bResNat == true)
			strLocRes.replace ( '/', '\\' );
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Resolve,strLocRes);
		else
			_EMT(Error,strLoc);
		}	// else if

	// Flush
	else if (_RCP(Flush))
		{
		adtValue vF;

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );

		// TODO: Support going relative
		CCLTRY ( pSrc->flush(vF) );
		}	// else if

	// Close
	else if (_RCP(Close))
		{
		IResource	*pRes = NULL;
		adtIUnknown unkV(v);

		// State check
		CCLTRY(_QISAFE(unkV,IID_IResource,&pRes));

		// Close it
		CCLTRY ( pRes->close() );

		// Clean up
		_RELEASE(pRes);
		}	// else if

	// Move/rename
	else if (_RCP(Move))
		{
		adtString	strTo(v);

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );
		CCLTRYE ( strTo.length() > 0, ERROR_INVALID_STATE );

		// Rename item
		CCLTRY ( pSrc->move(strLoc,strTo) );
		}	// else if

	// Remove/delete
	else if (_RCP(Remove))
		{
		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// Delete item
		CCLTRY ( pSrc->remove(strLoc) );

		// Result
		if (hr == S_OK)
			_EMT(Remove,strLoc);
		else
			_EMT(Error,strLoc);
		}	// else if

	// Copy
	else if (_RCP(Copy))
		{
		adtString	strTo(v);

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );
		CCLTRYE ( strTo.length() > 0, ERROR_INVALID_STATE );

		// Copy item
		CCLTRY ( pSrc->copy(strLoc,strTo) );

		// Result
		if (hr == S_OK)
			_EMT(Copy,strTo);
		else
			_EMT(Error,strLoc);
		}	// else if

	// Link
	else if (_RCP(Link))
		{
		adtString	strTo(v);

		// State check
		CCLTRYE ( pSrc != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );
		CCLTRYE ( strTo.length() > 0, ERROR_INVALID_STATE );

		// Link item
		CCLTRY ( pSrc->link(strLoc,strTo) );

		// Result
		if (hr == S_OK)
			_EMT(Link,strTo);
		else
			_EMT(Error,strLoc);
		}	// else if

	// State
	else if (_RCP(Options))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pOpt);
		hr = _QI(unkV,IID_IDictionary,&pOpt);
		}	// else if
	else if (_RCP(Source))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pSrc);
		hr = _QISAFE(unkV,IID_ILocations,&pSrc);
		}	// else if
	else if (_RCP(Location))
		hr = adtValue::copy ( adtString(v), strLoc );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

