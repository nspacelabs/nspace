////////////////////////////////////////////////////////////////////////
//
//									Execute.CPP
//
//					Implementation of the Execute node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

#ifdef	_WIN32
#include <TlHelp32.h>
#else
#include <sys/wait.h>

// Helpful defines for pipes
#define PARENT_WRITE_PIPE  0
#define PARENT_READ_PIPE   1
#define READ_FD  				0
#define WRITE_FD 				1

#define PARENT_READ_FD  	( pipes[PARENT_READ_PIPE][READ_FD]   )
#define PARENT_WRITE_FD 	( pipes[PARENT_WRITE_PIPE][WRITE_FD] )
 
#define CHILD_READ_FD   	( pipes[PARENT_WRITE_PIPE][READ_FD]  )
#define CHILD_WRITE_FD  	( pipes[PARENT_READ_PIPE][WRITE_FD]  )
#endif

Execute :: Execute ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	pDctEnv	= NULL;
	pParam	= NULL;
	#ifdef	_WIN32
	iX			= CW_USEDEFAULT;
	iY			= CW_USEDEFAULT;
	iW			= CW_USEDEFAULT;
	iH			= CW_USEDEFAULT;
	bWnd		= true;
	#endif
	bDbg		= false;
	bInherit	= false;
	}	// Execute

HRESULT Execute :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	adtIUnknown		unkV;

	// Attaching to graph
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Directory"), vL ) == S_OK)
			onReceive(prDirectory,vL);
		if (pnDesc->load ( adtString(L"Parameters"), vL ) == S_OK)
			onReceive(prParameters,vL);
		if (pnDesc->load ( adtString(L"X"), vL ) == S_OK)
			iX = vL;
		if (pnDesc->load ( adtString(L"Y"), vL ) == S_OK)
			iY = vL;
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			iW = vL;
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			iH = vL;
		if (pnDesc->load ( adtString(L"UseStdIn"), vL ) == S_OK)
			bStdIn = vL;
		if (pnDesc->load ( adtString(L"UseStdOut"), vL ) == S_OK)
			bStdOut = vL;
		if (pnDesc->load ( adtString(L"UseStdErr"), vL ) == S_OK)
			bStdErr = vL;
		if (pnDesc->load ( adtString(L"Show"), vL ) == S_OK)
			bShow = vL;
		if (pnDesc->load ( adtString(L"Console"), vL ) == S_OK)
			bConsole = vL;
		if (pnDesc->load ( adtString(L"Window"), vL ) == S_OK)
			bWnd = vL;
		if (pnDesc->load ( adtString(L"Debug"), vL ) == S_OK)
			bDbg = vL;
		if (pnDesc->load(adtString(L"Inherit"), vL) == S_OK)
			bInherit = adtBool(vL);
		}	// if

	// Detaching from graph
	else
		{
		// Clean up
		_RELEASE(pParam);
		_RELEASE(pDctEnv);
		_RELEASE(pDct);
		}	// else

	return S_OK;
	}	// onAttach

#ifdef	_WIN32
static
BOOL CALLBACK SendClose ( HWND hWnd, LPARAM lParam )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Window enumeration callback
	//
	//	PARAMETERS
	//		-	hWnd is the next top level window
	//		-	lParam will be the process Id
	//
	//	RETURN VALUE
	//		TRUE to continue enumeration
	//
	////////////////////////////////////////////////////////////////////////
	DWORD	dwPid = 0;

	// Retrieve the process Id for the top level window
	if (	GetWindowThreadProcessId(hWnd,&dwPid) != 0 && 
			dwPid == (DWORD)lParam)
		{
		// Close window
		lprintf ( LOG_DBG, L"Found : SendClose hWnd %d dwProcessId %d/%d", 
						hWnd,  dwPid, lParam );
		SendMessageTimeout ( hWnd, WM_CLOSE, 0, 0, SMTO_ABORTIFHUNG, 20000, NULL );
//		PostMessage ( hWnd, WM_CLOSE, 0, 0 );
		return FALSE;
		}	// if

	// Not found, continue
	return TRUE;
	}	// SendClose

static
BOOL CALLBACK ToFront ( HWND hWnd, LPARAM lParam )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Window enumeration callback
	//
	//	PARAMETERS
	//		-	hWnd is the next top level window
	//		-	lParam will be the process Id
	//
	//	RETURN VALUE
	//		TRUE to continue enumeration
	//
	////////////////////////////////////////////////////////////////////////
	DWORD	dwPid = 0;

	// Retrieve the process Id for the top level window
	GetWindowThreadProcessId(hWnd,&dwPid);
//	lprintf ( LOG_DBG, L"ToFront hWnd %d dwProcessId %d/%d", 
//					hWnd,  dwPid, lParam );
	if (	GetWindowThreadProcessId(hWnd,&dwPid) != 0 && 
			dwPid == (DWORD)lParam)
		{
		// Set focus/bring to front
		lprintf ( LOG_DBG, L"Found : ToFront hWnd %d dwProcessId %d/%d", 
						hWnd,  dwPid, lParam );

		// Works better but still not ideal, Windows start menu still shows
		HWND	hWndNow	= GetForegroundWindow();
		DWORD dwNow		= GetWindowThreadProcessId ( hWndNow, NULL );
		DWORD dwNew		= GetWindowThreadProcessId ( hWnd, NULL );
		AttachThreadInput ( dwNow, dwNew, TRUE );
		SetFocus(hWnd);
		SetWindowPos (hWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE );
		SetForegroundWindow(hWnd);
		AttachThreadInput ( dwNow, dwNew, FALSE );
		return FALSE;
		}	// if

	// Not found, continue
	return TRUE;
	}	// ToFront
#endif

HRESULT Execute :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute 
	if (_RCP(Open))
		{
		adtString	strFull;

		// State check
		CCLTRYE( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE( pParam != NULL, ERROR_INVALID_STATE );

		// Generate the full command line
//		CCLTRY ( adtValue::copy ( strLoc, strFull ) );
//		if (hr == S_OK && strParam.length() > 0)
//			{
//			CCLTRY ( strFull.append(L" ") );
//			CCLTRY ( strFull.append(strParam) );
//			}	// if

		#ifdef	_WIN32
		PROCESS_INFORMATION	pi;
		STARTUPINFO				si;

		// Create process
		if (hr == S_OK)
			{
			U64			pidWnd = 0;
			adtString	strEnv;

			// Prepare
			memset ( &pi, 0, sizeof(pi) );
			memset ( &si, 0, sizeof(si) );
			si.cb				= sizeof(si);
			si.wShowWindow	= (bShow) ? SW_SHOWNORMAL : SW_HIDE;
			si.dwFlags		= STARTF_USESHOWWINDOW;

			// Position/size
			if (iX != CW_USEDEFAULT || iY != CW_USEDEFAULT)
				{
				si.dwX		= iX;
				si.dwY		= iY;
				si.dwFlags	|= STARTF_USEPOSITION;
				}	// if
			if (iW != CW_USEDEFAULT || iH != CW_USEDEFAULT)
				{
				si.dwXSize	= iW;
				si.dwYSize	= iH;
				si.dwFlags	|= STARTF_USESIZE;
				}	// if

			//
			// Standard I/O
			//

			// Use standard I/O ?
			if (hr == S_OK && (bStdIn || bStdOut || bStdErr))
				{
				// Fill in defaults for unused I/O
				si.hStdInput	= GetStdHandle(STD_INPUT_HANDLE);
				si.hStdOutput	= GetStdHandle(STD_OUTPUT_HANDLE);
				si.hStdError	= GetStdHandle(STD_ERROR_HANDLE);

				// Handles will be used
				si.dwFlags		|= STARTF_USESTDHANDLES;

				// Setup standard I/O pipes
				for (int p = 0;hr == S_OK && p < 3;++p)
					{
					HANDLE		hIn		= INVALID_HANDLE_VALUE;
					HANDLE		hOut		= INVALID_HANDLE_VALUE;
					StmFile		*pRes		= NULL;
					IResource	*piRes	= NULL;
					adtString	strType	=	(p == 0) ? L"In" :
													(p == 1) ? L"Out" : L"Err";

					// Use I/O ?
					if (	(p == 0 && bStdIn == false)	||
							(p == 1 && bStdOut == false)	||
							(p == 2 && bStdErr == false) )
						break;

					// Create anonymous pipe for I/O
					if (hr == S_OK)
						{
						SECURITY_ATTRIBUTES	attr;

						// Required attributes for pipe so that process inherits the same handles
						memset ( &attr, 0, sizeof(attr) );
						attr.nLength			= sizeof(attr);
						attr.bInheritHandle	= TRUE;
						CCLTRYE ( CreatePipe ( &hIn, &hOut, &attr, 0 ) == TRUE, GetLastError() );
						}	// if

					// Pipe handles are for both ends of a single pipe, assign pipe to startup info.
					// The process's stdin, for example, gets the 'input' handle for reading the
					// graph gets the 'output' handle for writing to the input.
					if (hr == S_OK)
						{
						// Process gets input handle for stdin, otherwise output handle
						if			(p == 0)	si.hStdInput	= hIn;
						else if	(p == 1)	si.hStdOutput	= hOut;
						else					si.hStdError	= hOut;
						}	// if

					// Create file resource for 'other' end of pipe
					if (hr == S_OK)
						{
						HANDLE		hPipe = (p == 0) ? hOut : hIn;
						adtString	strName (L"Std");

						// Ensure the local handles are not inherited
						CCLTRYE ( SetHandleInformation ( hPipe, HANDLE_FLAG_INHERIT, 0 )
										== TRUE, GetLastError() );

						// Standard input file resource
						CCLTRYE( ((pRes = new StmFile ( hPipe )) != NULL), E_OUTOFMEMORY );
						CCLTRY ( pRes->construct() );
						CCLOK  ( pRes->AddRef(); )

						// Store in context
						CCLTRY ( strName.append(strType) );
						CCLTRY ( pDct->store ( strName, adtIUnknown(piRes = pRes) ) );
						_RELEASE(pRes);
						}	// if

					}	// for

				}	// if

			//
			// Environment
			//
			if (hr == S_OK && pDctEnv != NULL)
				{
				IIt		*pKeys	= NULL;
				adtValue	vK,vV;

				// Iterate the key = value environment variables
				CCLTRY ( pDctEnv->keys ( &pKeys ) );
				while (hr == S_OK && pKeys->read ( vK ) == S_OK)
					{
					adtString strKey(vK);

					// Value for key
					if (pDctEnv->load ( vK, vV ) == S_OK)
						{
						adtString strV(vV);

						// Append to enviromment variables.
						// Win32 wants null terminated strings after each other
						CCLTRY ( strEnv.append ( strKey ) );
						CCLTRY ( strEnv.append ( L"=" ) );
						CCLTRY ( strEnv.append ( strV ) );
						CCLTRY ( strEnv.append ( L"|" ) );
						}	// if

					// Proceed to next pair
					pKeys->next();
					}	// while

				// Entire block must be null terminated
				CCLTRY ( strEnv.append ( L"|" ) );

				// Now, replace all separators with nulls for Win32
				CCLOK ( strEnv.replace ( '|', '\0' ); )

				// Clean up
				_RELEASE(pKeys);
				}	// if

			//
			// Command line
			//
			if (hr == S_OK && pParam != NULL)
				{
				IIt		*pVals	= NULL;
				adtValue	vV;

				// Iterate the list of command line parameters, skip first entry (for Unix)
				strFull = L"";
				CCLTRY ( pParam->iterate ( &pVals ) );
				CCLTRY ( pVals->next() );
				while (hr == S_OK && pVals->read ( vV ) == S_OK)
					{
					adtString strKey(vV);

					// Append to string
					if (strFull.length() > 0)
						hr = strFull.append(L" ");
					CCLTRY ( strFull.append ( strKey ) );

					// Proceed to next pair
					pVals->next();
					}	// while

				// Clean up
				_RELEASE(pVals);
				}	// if

			// Create
			CCLTRYE ( CreateProcess ( NULL, &strFull.at(), NULL, NULL,
							bInherit || ((si.dwFlags & STARTF_USESTDHANDLES) != 0),
							(strEnv.length() > 0) ? CREATE_UNICODE_ENVIRONMENT : 0,
							(strEnv.length() > 0) ? (LPVOID)strEnv.pstr : NULL,
							(strDir.length() > 0) ? strDir : (LPCWSTR)NULL, 
							&si, &pi ) == TRUE, GetLastError() );

			// Debug
			lprintf ( LOG_DBG, L"CreateProcess %s : pid 0x%x : hr 0x%x", 
								&strFull.at(), pi.dwProcessId, hr );

			// Unneeded handles
//			if (pi.hProcess != NULL)
//				CloseHandle(pi.hProcess);
			if (pi.hThread != NULL)
				CloseHandle(pi.hThread);

			// Do not need local version of child process stdio
			if (bStdIn == true)
				CloseHandle(si.hStdInput);
			if (bStdOut == true)
				CloseHandle(si.hStdOutput);
			if (bStdErr == true)
				CloseHandle(si.hStdError);

			// Wait a bit for process window to exist (timeout option ?)
			// (only valid for Windowed processes, option for node ?)
			if (bWnd)
				{
				for (int i = 0;hr == S_OK && i < 10;++i)
					{
					HANDLE			hSnap = INVALID_HANDLE_VALUE;
					BOOL				bRes	= TRUE;
					PROCESSENTRY32	pent;

					// Attempt to bring process to front.  Looped because sometimes the main window
					// comes up after CreateProcess returns TODO: Option for windowed app ?
					if (!EnumWindows(ToFront, pi.dwProcessId))
						break;

					// NOTE: Some programs launch a child process that actually contains the
					// main window.  Check child processes as well.
					// Process information
					if ((hSnap = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 ))
							!= INVALID_HANDLE_VALUE )
						{
						// Find if process has parent processes
						pent.dwSize = sizeof(pent);
						for (	bRes = Process32First ( hSnap, &pent );
								bRes == TRUE;
								bRes = Process32Next ( hSnap, &pent ) )
							{
							// If parent process matches the just launch one,
							// run logic on that process as well.
							if (pent.th32ParentProcessID == pi.dwProcessId)
								{
								// Process child
								if (!EnumWindows ( ToFront, pent.th32ProcessID ))
									pidWnd = pent.th32ProcessID;
								}	// if

							}	// for

						// Clean up
						CloseHandle(hSnap);
						}	// if

					// If window was found, exit
					if (pidWnd != 0)
						break;

					// Give it time
					Sleep(500);
					}	// for
				}	// if

			// Store process information in context
			CCLTRY ( pDct->store ( adtString(L"ProcessHandle"), adtLong((U64)pi.hProcess) ) );
			CCLTRY ( pDct->store ( adtString(L"ProcessId"), adtLong((U64)pi.dwProcessId) ) );
			if (hr == S_OK && pidWnd != 0)
				hr = pDct->store ( adtString(L"ProcessIdWnd"), adtLong((U64)pidWnd) );

			// Clean up
			}	// if

		// Linux
		#elif	defined(__unix__)
		char 			*pcFull		= NULL;
		char 			*pcLoc 		= NULL;
		char 			*pcParam		= NULL;
		char			**ppargv		= NULL;
		IIt			*pIt			= NULL;
		int			i,ret			= 0;
		U32			cnt			= 0;
		int			pipes[2][2];
		adtValue		vK,vV;

		// Generate an array of 'argvs' for the execution from the parameter list
		// It is assume the first entry is the command to execute
		CCLTRY ( pParam->iterate ( &pIt ) );
		CCLTRY ( pParam->size ( &cnt ) );
		CCLTRYE( cnt > 0, ERROR_INVALID_STATE );
//		lprintf ( LOG_DBG, L"Parameter count %d\r\n", cnt );
		CCLTRYE( (ppargv = (char **) _ALLOCMEM ( (cnt+1) * sizeof(char *) )) != NULL,
						E_OUTOFMEMORY );
		for (i = 0;hr == S_OK && i < cnt;++i)
			{
			char			*pcStr	= NULL;
			adtString	strV;

			// Read the next value
			CCLTRY ( pIt->read ( vV ) );
			CCLTRY ( adtValue::toString ( vV, strV ) );
			CCLTRY ( strV.toAscii ( &pcStr ) );

			// Allocate memory for string and put in list
			CCLTRYE( (ppargv[i] = (char *) _ALLOCMEM ( strlen(pcStr)+1*sizeof(char) )) != NULL,
							E_OUTOFMEMORY );
			CCLOK  ( strcpy ( ppargv[i], pcStr ); )
//			CCLOK  ( lprintf ( LOG_DBG, L"%d) %S\r\n", i, pcStr ); )

			// Clean up
			_FREEMEM(pcStr);
			pIt->next();
			}	// for

		// List must be null terminated
		CCLOK ( ppargv[i] = NULL; )

		// Parameters 
		// Create a pair of pipes for I/O with child process
		CCLOK   ( memset ( pipes, 0, sizeof(pipes) ); )
		CCLTRYE ( (pipe(pipes[PARENT_READ_PIPE]) == 0), errno );
		CCLTRYE ( (pipe(pipes[PARENT_WRITE_PIPE]) == 0), errno );

		// Need ASCII versions
//		CCLTRY ( strFull.toAscii(&pcFull) );
//		CCLTRY ( strLoc.toAscii(&pcLoc) );
//CCLOK ( strParam.prepend ( L"\"" ); )
//		CCLTRY ( strParam.toAscii(&pcParam) );
//CCLOK ( strParam.append ( L"\"" ); )
		if (hr == S_OK)
			{
			// Fork the process to execute the command
			ret = fork();

			// This process
			if (ret > 0)
				{
				// Close descriptors not needed by parent
				close(CHILD_READ_FD);
				close(CHILD_WRITE_FD);

				// Process Id of child
//				lprintf ( LOG_DBG, L"fork() child process pid : %d, hr 0x%x\r\n", ret, hr );
				CCLTRY ( pDct->store ( adtString(L"ProcessId"), adtLong(ret) ) );
				}	// else if

			// Child process
			else if (ret == 0)
				{
				// Assign pipes to std out and std in for communication with child process.
				// If not requested, leave untouched for default usage.
				if (bStdIn)
					dup2 ( CHILD_READ_FD, STDIN_FILENO );
				if (bStdOut)
					dup2 ( CHILD_WRITE_FD, STDOUT_FILENO );

				// Close file descriptors not required by child, they are duplicated during fork.
				close ( CHILD_READ_FD );
				close ( CHILD_WRITE_FD );
				close ( PARENT_READ_FD );
				close ( PARENT_WRITE_FD );

				// Set environment
//				lprintf ( LOG_DBG, L"pDctEnv %p\r\n", pDctEnv );
				if (pDctEnv != NULL)
					{
					IIt	*pKeys	= NULL;

					// Iterate the key = value environment variables
					CCLTRY ( pDctEnv->keys ( &pKeys ) );
					while (hr == S_OK && pKeys->read ( vK ) == S_OK)
						{
						adtString strKey(vK);

						// Value for key
						if (pDctEnv->load ( vK, vV ) == S_OK)
							{
							const char	*pcE	= NULL;
							char			*pcK	= NULL;
							char 			*pcV	= NULL;
							adtString	strV(vV);

							// ASCII versions
							CCLTRY ( strKey.toAscii(&pcK) );
							CCLTRY ( strV.toAscii(&pcV) );

							// Set the environment variable
							CCLOK ( ret = setenv ( pcK, pcV, 1 ); )
//							lprintf ( LOG_DBG, L"Environment %s = %s (%d)\r\n", (LPCWSTR)strKey, (LPCWSTR)strV, ret );

							// Clean up
							_FREEMEM(pcV);
							_FREEMEM(pcK);
							}	// if

						// Proceed to next pair
						pKeys->next();
						}	// while

					// Clean up
					_RELEASE(pKeys);
					}	// if

				// Debug
				if (bDbg)
					{
					lprintf ( LOG_DBG, L"execvp : %S\r\n", ppargv[0] );
					for (i = 1;ppargv[i] != NULL;++i)
						lprintf ( LOG_DBG, L"%S\r\n", ppargv[i] );
					}	// if

				// Execute command line.  If succesful, it replaces this process.
//				lprintf ( LOG_DBG, L"execlp %S, %S, %S\r\n", pcLoc, pcParam, pcFull );
//				ret = execlp ( pcLoc, pcLoc, "-c", pcParam, NULL );
//				ret = execlp ( pcLoc, pcLoc, pcParam, NULL );
//				lprintf ( LOG_DBG, L"execlp %S\r\n", pcFull );
//				ret = execlp ( "/bin/sh", "sh", "-c", pcFull, NULL );
				ret = execvp ( ppargv[0], &(ppargv[1]) );
				if (bDbg)
					lprintf ( LOG_DBG, L"NOTE: execvp : ret %d\r\n", ret );

				// If still here, error
				lprintf ( LOG_DBG, L"execlp failed with return code %d, exiting child fork\r\n", ret );
				exit(0);
				}	// if

			// Error
			else
				{
				lprintf ( LOG_DBG, L"fork() failed, return code %d\r\n", ret );
				hr = E_UNEXPECTED;
				}	// else

			}	// if

		// Standard I/O
//		lprintf ( LOG_DBG, L"Stdio processing hr 0x%x\r\n", hr );
		for (int p = 0;hr == S_OK && p < 2;++p)
			{
			StmFile		*pRes		= NULL;
			IResource	*piRes	= NULL;
			adtString	strType	= (p == 0) ? L"In" : L"Out";
			int			pipe		= (p == 0) ? PARENT_WRITE_FD : PARENT_READ_FD;

			// Use I/O ?
//			lprintf ( LOG_DBG, L"strType %s pipe %d bStdIn %d bStdOut %d\r\n",
//										(LPCWSTR)strType, pipe, (bool)bStdIn, (bool)bStdOut );
			if (	(p == 0 && bStdIn == false)	||
					(p == 1 && bStdOut == false) )
				{
				// Close unused pipe
				close(pipe);
				}	// if

			// Create file resource for 'other' end of pipe
			else if (hr == S_OK)
				{
				adtString	strName (L"Std");

				// Standard file resource
				CCLTRYE( ((pRes = new StmFile ( pipe )) != NULL), E_OUTOFMEMORY );
				CCLTRY ( pRes->construct() );
				CCLOK  ( pRes->AddRef(); )

				// Store in context
				CCLTRY ( strName.append(strType) );
				CCLTRY ( pDct->store ( strName, adtIUnknown(piRes = pRes) ) );
				_RELEASE(pRes);
				}	// if

			}	// for

/*
		// NOTE: Currently just calling 'system' on the command line.
		// Will need fork()/exec() to get PID for management.
//		CCLTRYE ( (ret = system ( pcFull )) > 0, E_UNEXPECTED );
//		lprintf ( LOG_DBG, L"system : %S\r\n", pcFull );
		CCLOK ( ret = system ( pcFull ); )


		// Set environment
//		lprintf ( LOG_DBG, L"pDctEnv %p\r\n", pDctEnv );
		if (hr == S_OK && pDctEnv != NULL)
			{
			// Dictionary for previous environment
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctOld ) );

			// Iterate the key = value environment variables
			CCLTRY ( pDctEnv->keys ( &pKeys ) );
			while (hr == S_OK && pKeys->read ( vK ) == S_OK)
				{
				adtString strKey(vK);

				// Value for key
				if (pDctEnv->load ( vK, vV ) == S_OK)
					{
					const char	*pcE	= NULL;
					char			*pcK	= NULL;
					char 			*pcV	= NULL;
					adtString	strV(vV);

					// ASCII versions
					CCLTRY ( strKey.toAscii(&pcK) );
					CCLTRY ( strV.toAscii(&pcV) );

					// Current environment
					if (hr == S_OK)
						{
						if ((pcE = getenv ( pcK )) != NULL)
							hr = pDctOld->store ( strKey, (strV = pcE) );
						else
							hr = pDctOld->store ( strKey, adtInt(0) );
						}	// if

					// Set the environment variable
					CCLOK ( ret = setenv ( pcK, pcV, 1 ); )
//					lprintf ( LOG_DBG, L"Environment %s = %s (%d)\r\n", (LPCWSTR)strKey, (LPCWSTR)strV, ret );

					// Clean up
					_FREEMEM(pcV);
					_FREEMEM(pcK);
					}	// if

				// Proceed to next pair
				pKeys->next();
				}	// while

			// Clean up
			_RELEASE(pKeys);
			}	// if

		// Need ASCII version
		CCLTRY ( strFull.toAscii(&pcFull) );

		// NOTE: Currently just calling 'system' on the command line.
		// Will need fork()/exec() to get PID for management.
//		CCLTRYE ( (ret = system ( pcFull )) > 0, E_UNEXPECTED );
//		lprintf ( LOG_DBG, L"system : %S\r\n", pcFull );
		CCLOK ( ret = system ( pcFull ); )
		if (ret != 0)
			lprintf ( LOG_DBG, L"System returned : %d for pcFull %S\r\n", ret, pcFull );

		// Restore environment variables
		if (hr == S_OK && pDctOld != NULL)
			{
			// Iterate the key = value environment variables
			CCLTRY ( pDctOld->keys ( &pKeys ) );
			while (hr == S_OK && pKeys->read ( vK ) == S_OK)
				{
				adtString strKey(vK);

				// Value for key
				if (pDctEnv->load ( vK, vV ) == S_OK)
					{
					char			*pcK	= NULL;

					// ASCII version
					CCLTRY ( strKey.toAscii(&pcK) );

					// Previous key ?
					if (hr == S_OK && adtValue::type(vV) == VTYPE_STR)
						{
						char 			*pcV	= NULL;
						adtString	strV(vV);

						// ASCII version
						CCLTRY ( strV.toAscii(&pcV) );

						// Restore
						lprintf ( LOG_DBG, L"Restore %s = %s\r\n", (LPCWSTR)strKey, (LPCWSTR)strV );
						CCLOK ( setenv ( pcK, pcV, 1 ); )

						// Clean up
						_FREEMEM(pcV);
						}	// if
					else if (hr == S_OK)
						{
						// Unset the variable
						lprintf ( LOG_DBG, L"Unset %s\r\n", (LPCWSTR)strKey );
						unsetenv ( pcK );
						}	// else if

					// Clean up
					_FREEMEM(pcK);
					}	// if

				// Proceed to next pair
				pKeys->next();
				}	// while

			// Clean up
			_RELEASE(pKeys);
			}	// if
*/
		// Clean up
		_FREEMEM(pcLoc);
		_FREEMEM(pcParam);
		_FREEMEM(pcFull);
		if (ppargv != NULL)
			{
			for (i = 0;i < cnt;++i)
				{
				_FREEMEM(ppargv[i]);
				}	// for
			_FREEMEM(ppargv);
			}	// if
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Open,adtIUnknown(pDct));
		else
			{
			lprintf ( LOG_DBG, L"%s: Failed 0x%x : pDct %p : pParam %p\r\n", 
							(LPCWSTR)strnName, hr, pDct, pParam );
			_EMT(Error,adtInt(hr));
			}	// else
		}	// if

	// Close
	else if (_RCP(Close))
		{
		HANDLE	hProc		= 0;
		DWORD		dwId		= 0;
		DWORD		dwIdWnd	= 0;
		bool		bAttach	= false;
		bool		bHandler	= false;
		adtValue	vL;

		// State check
		CCLTRYE(pDct != NULL, ERROR_INVALID_STATE);

		// Access process information
		if (hr == S_OK && pDct->load ( adtString(L"ProcessId"), vL ) == S_OK)
			{
			CCLTRYE	( (dwId = (DWORD)(U64)adtLong(vL)) != 0, ERROR_INVALID_STATE );
			}	// if
		if (hr == S_OK && pDct->load ( adtString(L"ProcessHandle"), vL ) == S_OK)
			{
			CCLTRYE	( (hProc = (HANDLE)(U64)adtLong(vL)) != 0, ERROR_INVALID_STATE );
			}	// if

		// Windows specific
		#ifdef	_WIN32
		if (hr == S_OK && pDct->load ( adtString(L"ProcessIdWnd"), vL ) == S_OK)
			dwIdWnd = (DWORD)(U64)adtLong(vL);

		// State check
		CCLTRYE ( (dwId != 0 && hProc != 0), ERROR_INVALID_STATE );

		// Close process
//		lprintf ( LOG_DBG, L"Close dwId %d dwIdWnd %d", dwId, dwIdWnd );
		#endif

		// Close any created pipe handles
		for (int p = 0;hr == S_OK && p < 3;++p)
			{
			IResource	*pRes		= NULL;
			adtIUnknown	unkV;

			// Name of current pipe
			adtString strName =	(p == 0) ? L"StdIn" :
										(p == 1) ? L"StdOut" : L"StdErr";

			// Pipe created ?
			if (	pDct->load ( strName, vL ) != S_OK	||
					((IUnknown *)(NULL)) == (unkV = vL) ||
					_QI(unkV,IID_IResource,&pRes) != S_OK)
				continue;

			// Clean up
			pRes->close();
			_RELEASE(pRes);
			}	// for

		// Send close request.  NOTE: It appears if stdio is used these handles
		// have to be closed before the child process will close properly.
		#ifdef	_WIN32
		if (hr == S_OK)
			{
			// Windows program
			if (bConsole == false)
				EnumWindows(SendClose, (dwIdWnd != NULL) ? dwIdWnd : dwId);

			// Console program.  Programs like 'ffmpeg' need a Ctrl-C in order
			// to exit cleanly.  Even though WM_CLOSE seems to close the app.
			// it is not clear if it is always 'clean'.
			else
				{
				// If console app, use Ctrl-C signaling.
				// NOTE: In order to stop the debugger from throwing an
				// expection, disable in Debug->Exceptions (Visual Studio)

				// Attach this process to the console
				bAttach = (AttachConsole ( dwId ) == TRUE);
				if (!bAttach)
					lprintf ( LOG_DBG, L"AttachConsole failed 0x%x\r\n", GetLastError() );

				// Turn off control-c handler for this process to prevent it from stopping
				if (bAttach)
					{
					bHandler = (SetConsoleCtrlHandler(NULL,TRUE) == TRUE);
					if (!bHandler)
						lprintf ( LOG_DBG, L"SetConsoleCtrlHandler failed 0x%x\r\n", GetLastError() );
					}	// if

				// Send the Cltr-C signal
				if (bAttach && bHandler)
					{
					if (!GenerateConsoleCtrlEvent ( CTRL_C_EVENT, 0 ))
						lprintf ( LOG_DBG, L"GenerateConsoleCtrlEvent failed 0x%x\r\n", GetLastError() );
					}	// if

				}	// else

			// Wait for shutdown
			DWORD dwRet	= 0;
			DWORD dwT0	= GetTickCount();
			while (1)
				{
				MSG msg;

				// Perform wait 
				dwRet = MsgWaitForMultipleObjects ( 1, &hProc, FALSE, 10000, QS_ALLINPUT );

				// Message
				if (	dwRet == WAIT_OBJECT_0 + 1 &&
						PeekMessage ( &msg, NULL, 0, 0, PM_REMOVE ) )
					{
					// Process message
					TranslateMessage ( &msg );
					DispatchMessage ( &msg );
					}	// else

				// Other event
				else
					{
					break;
					}	// else

				// Overall wait timeout
				if (GetTickCount()-dwT0 > 10000)
					{
					lprintf ( LOG_DBG, L"Timeout waiting for application to exit dwId %d\r\n", dwId );
					break;
					}	// if
				}	// while

			// Clean up

			// Detach console
			// NOTE: "FreeConsole" messes up future output from other console
			// programs even though they still seem to work, better way to do this ?
			if (bAttach) 
				FreeConsole();

			// Restore Ctrl-C handler
			if (bHandler)
				SetConsoleCtrlHandler(NULL,FALSE);
			}	// if
		#else

		// Wait for child process to exit
//		lprintf ( LOG_DBG, L"Close dwId %d hr 0x%x\r\n", dwId, hr );
		if (hr == S_OK && dwId > 0)
			{
			int ret,status,i;

			// Wait with timeout
//			lprintf ( LOG_DBG, L"Waiting for process %d to exit\r\n", dwId );
			for (i = 0;i < 100;++i)
				{
				// Done ?
				if ((ret = waitpid ( (int)dwId, &status, WNOHANG )) > 0)
					break;

				// Wait (100ms)
				usleep(100000);
				}	// for

			// Timeout ?
			if (i == 100)
				{
				lprintf ( LOG_DBG, L"Timeout waiting for process to exit, status %d\r\n", status );
				hr = ERROR_TIMEOUT;
				}	// if

			// Done
//			else
//				lprintf ( LOG_DBG, L"Process exited, status %d\r\n", status );
			}	// if

		#endif

		// Clean up
		if (pDct != NULL)
			{
			pDct->remove ( adtString(L"ProcessId") );
			pDct->remove ( adtString(L"ProcessIdWnd") );
			pDct->remove ( adtString(L"StdIn") );
			pDct->remove ( adtString(L"StdOut") );
			pDct->remove ( adtString(L"StdErr") );
			}	// if

		// Result
		_EMT(Close,adtIUnknown(pDct));
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Environment))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctEnv);
		_QISAFE(unkV,IID_IDictionary,&pDctEnv);
		}	// else if
	else if (_RCP(Directory))
		hr = adtValue::toString(v,strDir);
	else if (_RCP(Parameters))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pParam);
		_QISAFE(unkV,IID_IContainer,&pParam);
//		hr = adtValue::copy(v,strParam);
		}	// else if

	else if (_RCP(Show))
		bShow = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive


/*
		if (hr == S_OK)
			{
			bool bAttach	= false;
			bool bHandler	= false;

			// Attempt to close process window 'normally'
			if (bConsole == false)
				{
				// Close main window
//				EnumWindows(SendClose, (dwIdWnd != NULL) ? dwIdWnd : dwId);

				}	// if

			// Console app
			else
				{
				// If console app, use Ctrl-C signaling.
				// NOTE: In order to stop the debugger from throwing an
				// expection, disable in Debug->Exceptions (Visual Studio)

				// Attach this process to the console
				bAttach = (AttachConsole ( dwId ) == TRUE);
				if (!bAttach)
					lprintf ( LOG_DBG, L"AttachConsole failed 0x%x\r\n", GetLastError() );

				// Turn off control-c handler for this process to prevent it from stopping
				if (bAttach)
					{
					bHandler = (SetConsoleCtrlHandler(NULL,TRUE) == TRUE);
					if (!bHandler)
						lprintf ( LOG_DBG, L"SetConsoleCtrlHandler failed 0x%x\r\n", GetLastError() );
					}	// if

				// Send the Cltr-C signal
				if (bAttach && bHandler)
					{
					if (!GenerateConsoleCtrlEvent ( CTRL_C_EVENT, 0 ))
						lprintf ( LOG_DBG, L"GenerateConsoleCtrlEvent failed 0x%x\r\n", GetLastError() );
					}	// if

				// Wait ?
//				if (bAttach && bHandler)
//					Sleep(1000);

				// Detach console
				if (bAttach) FreeConsole();

				}	// else

			// Wait for shutdown
			DWORD dwRet = 0;
			while (1)
				{
				MSG msg;

				// Perform wait 
				dwRet = MsgWaitForMultipleObjects ( 1, &hProc, FALSE, 10000, QS_ALLINPUT );

				// Message
				if (	dwRet == WAIT_OBJECT_0 + 1 &&
						PeekMessage ( &msg, NULL, 0, 0, PM_REMOVE ) )
					{
					// Process message
					TranslateMessage ( &msg );
					DispatchMessage ( &msg );
					}	// else

				// Other event
				else
					break;
				}	// while


//			lprintf ( LOG_DBG, L"Waiting for shutdown hProc 0x%x\r\n", hProc );
//			if (WaitForSingleObject(hProc,5000) == WAIT_TIMEOUT)
//			if (WaitForSingleObject(hProc,20000) == WAIT_TIMEOUT)
			if (dwRet == WAIT_TIMEOUT)
				{
				HANDLE hProcOp = NULL;

				// Application won't shutdown, force the issue
lprintf ( LOG_DBG, L"TIMEOUT!!!!!!!!\r\n" );
				if ((hProcOp = OpenProcess(PROCESS_TERMINATE,FALSE,dwId)) != NULL)
					{
					// Shutdown
					TerminateProcess(hProcOp,0);

					// Clean up
					CloseHandle(hProcOp);
					}	// if

				}	// if
lprintf ( LOG_DBG, L"Done\r\n" );

			// Restore Ctrl-C handler
			if (bHandler) SetConsoleCtrlHandler(NULL,FALSE);
			}	// if

		// Clean up
		if (hProc != NULL)
			CloseHandle(hProc);
*/
