////////////////////////////////////////////////////////////////////////
//
//									MappedFile.CPP
//
//					Implementation of the mapped file object
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>
#if defined(__APPLE__) || defined(__unix__)
#include <sys/mman.h>
#endif

MappedFile :: MappedFile ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	fd		= -1;
	pvMap	= NULL;
	uOff	= 0;
	uSz	= 0;
	}	// MappedFile

HRESULT MappedFile :: close ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Close the resource.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Debug
//	lprintf ( LOG_DBG, L"pvMap %p:uSz %d\r\n", pvMap, uSz );

	// Clean up
	if (pvMap != NULL)
		{
		munmap ( pvMap, uSz );
		pvMap = NULL;
		uSz	= 0;
		}	// if
	
	return S_OK;
	}	// close

void MappedFile :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////
	close();
	}	// destruct

HRESULT MappedFile :: getResId ( ADTVALUE &vId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Return an identifier for the resource.
	//
	//	PARAMETERS
	//		-	vId will receive the identifer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;

	// Underlying stream
	CCLTRY ( adtValue::copy ( adtLong((U64)pvMap), vId ) );
	
	return hr;
	}	// getResId

HRESULT MappedFile :: getInfo ( void **ppv, U32 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Retrieve pointer and/or the size of the region.
	//
	//	PARAMETERS
	//		-	ppv will receive the ptr.
	//		-	puSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// Valid lock ?
	CCLTRYE ( pvMap != NULL && uSz > 0, ERROR_INVALID_STATE );

	// Debug
//	lprintf ( LOG_DBG, L"mmap uOff 0x%x uSz %d pvMap %p {\r\n", 
//					uOff, uSz, pvMap, hr );

	// Result
	if (ppv != NULL)
		*ppv 	= (hr == S_OK) ? pvMap : NULL;
	if (puSz != NULL)
		*puSz = (hr == S_OK) ? uSz : 0;

	// Debug
//	lprintf ( LOG_DBG, L"} mmap pvMap %p hr 0x%x\r\n", pvMap, hr );
	
	return hr;
	}	// getInfo

HRESULT MappedFile :: open ( IDictionary *pOpts )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IResource
	//
	//	PURPOSE
	//		-	Open a byte stream on top of a file.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the file.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
	IResource	*pRes	= NULL;
	int 			fd		= -1;
	adtValue		vL;
	adtIUnknown	unkV;

	// Existing mapping ?
	close();
	
	// File resource to map (required)
	CCLTRY ( pOpts->load ( adtString(L"File"), vL ) );
	CCLTRY ( _QISAFE((unkV=vL),IID_IResource,&pRes) );
	CCLTRY ( pRes->getResId(vL) );
	CCLTRYE( (fd = adtInt(vL)) != -1, E_INVALIDARG );
	_RELEASE(pRes);

	// A default offset and size can be specified that will be added to
	// any 'lock' calls.  This makes it easier in the rest of the system
	// when someone wants to just lock everything they can use 0,0 for 
	// the offset and size.
	if (hr == S_OK && pOpts->load ( adtString(L"Offset"), vL ) == S_OK)
		uOff = adtInt(vL);
	if (hr == S_OK && pOpts->load ( adtString(L"Size"), vL ) == S_OK)
		uSz = adtInt(vL);

	// New mapping
	CCLTRYE ( (pvMap = mmap ( 	NULL, uSz, PROT_READ | PROT_WRITE, MAP_SHARED,
										fd, uOff )) != (void *) -1, errno );

	// Zero memory on open request ?
	if (	hr == S_OK 													&& 
			pOpts->load ( adtString(L"Zero"), vL ) == S_OK 	&&
			adtBool(vL) == true )
		memset ( pvMap, 0, uSz );

	// Debug
//	CCLOK ( lprintf ( LOG_DBG, L"mmap fd %d pvMap %p uOff %d uSz %d hr 0x%x\r\n", 
//										fd, pvMap, uOff, uSz, hr ); )
//	if (hr == S_OK)
//		{
//		U32 *piMap = (U32 *)pvMap;
//		for (int i = 0;i < 8;++i)
//			lprintf ( LOG_DBG, L"0x%x 0x%x 0x%x 0x%x\r\n",
//							piMap[i*4+0], piMap[i*4+1], piMap[i*4+2], piMap[i*4+3] );
//		}	// if

	// Clearn up
	if (hr != S_OK)
		{
		lprintf ( LOG_DBG, L"Error:mmap fd %d pvMap %p uOff %d uSz %d hr 0x%x\r\n", 
						fd, pvMap, uOff, uSz, hr );
		// Undo
		close();
		}	// if

	return hr;
	}	// open

HRESULT MappedFile :: setInfo ( void *pv, U32 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Set static information about the memory block, can be
	//			called only one time.
	//
	//	PARAMETERS
	//		-	pv is the ptr. to the memory location
	//		-	uSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// setInfo

HRESULT MappedFile :: setSize ( U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Specifies what size the memory block should be sized too.
	//
	//	PARAMETERS
	//		-	sz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Allow 'harmless' calls to set the size as long as it is less than
	// or equal to current size.
	return (sz <= uSz) ? S_OK : E_OUTOFMEMORY;
	}	// setSize

HRESULT MappedFile :: stream ( IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Creates a byte stream in front of the memory block.
	//
	//	PARAMETERS
	//		-	ppStm is the new stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	MappedFileStm	*pStm	= NULL;

	// Setup
	(*ppStm) = NULL;

	// Create new stream and assign this object as the block
	CCLTRYE	( (pStm = new MappedFileStm()) != NULL, E_OUTOFMEMORY );
	CCLOK		( pStm->AddRef(); )
	CCLOK		( pStm->pMap = this; )
	CCLTRY	( pStm->construct(); )

	// Done
	if (hr == S_OK)
		{
		(*ppStm) = pStm;
		_ADDREF((*ppStm));
		}	// if

	// Clean up
	_RELEASE(pStm);

	return hr;
	}	// stream


