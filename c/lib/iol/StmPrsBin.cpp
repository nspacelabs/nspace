////////////////////////////////////////////////////////////////////////
//
//									STMPRBIN.CPP
//
//							Binary value stream parser
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"

// Markers
#define	MARKER_BEGIN		0x3141
#define	MARKER_END			0x5926

// Endian macros

#define	SWAPS(a)										\
				(											\
				(((a) << 8)	& 0xff00) |				\
				(((a) >> 8) & 0x00ff)				\
				)
#define	SWAPI(a)													\
				(														\
				(SWAPS(((a) >>  0) & 0x0000ffff) << 16) |	\
				(SWAPS(((a) >> 16) & 0x0000ffff) <<  0)	\
				)
#define	SWAPL(a)													\
				(														\
				(SWAPI(((a) >>  0) & 0xffffffff) << 32) |	\
				(SWAPI(((a) >> 32) & 0xffffffff) <<  0)	\
				)

StmPrsBin :: StmPrsBin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	punkDct	= NULL;
	pDct		= NULL;
	iChrSz 	= sizeof(WCHAR);
	}	// StmPrsBin

HRESULT StmPrsBin :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Aggregrate dictionary
	CCLTRY ( COCREATEA ( L"Adt.Dictionary", (IStreamPersist *) this, &punkDct ) );

	// Interfaces
	CCLTRY ( _QI(punkDct,IID_IDictionary,&pDct) );
	CCLOK  ( pDct->Release(); )						// Cancel 'QI'

	return hr;
	}	// construct

void StmPrsBin :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(punkDct);
	}	// destruct

HRESULT StmPrsBin :: load ( IByteStream *pStm, ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IStreamPersist
	//
	//	PURPOSE
	//		-	Load a value from the stream.
	//
	//	PARAMETERS
	//		-	pStm is the source stream
	//		-	v will receive the loaded value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// Options
	if (pDct->load ( adtString(L"CharSize"), vL ) == S_OK)
		iChrSz = vL;

	// Load value
	return loadValue(pStm,v);
	}	// load

HRESULT StmPrsBin :: loadValue ( IByteStream *pStm, ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IStreamPersist
	//
	//	PURPOSE
	//		-	Load a value from the stream.
	//
	//	PARAMETERS
	//		-	pStm is the source stream
	//		-	v will receive the loaded value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	U16		vt		= VTYPE_TYPEMASK;

	// Setup
	adtValue::clear(v);

	// Initialize marker
	CCLTRY	( read ( pStm, &vt ) );

	// Marker should be a 'begin'
	if (hr == S_OK)
		{
		CCLTRYE ( (vt == MARKER_BEGIN), E_UNEXPECTED );
		if (hr != S_OK)
			{
			U64 at	= 0;

			// Read stream position for debug
			pStm->seek ( 0, STREAM_SEEK_CUR, &at );

			// Debug
			lprintf ( LOG_DBG, L"Invalid beginning marker (0x%x != 0x%x) at 0x%x\r\n",
									vt, MARKER_BEGIN, (at-sizeof(U16)) );

			// DEBUG.  Temporary code to recover some temporal databases from a
			// data corruption bug, is this is good thing to leave in here all the time ?
			// Read until a begin marker is found, this may end up skipping over corrupted
			// values.
			#if 0
			hr = S_OK;
			while (hr == S_OK)
				{
				// Check next bytes
				hr = read ( pStm, &vt );
				if (hr == S_OK && vt == MARKER_BEGIN)
					{
					lprintf ( LOG_DBG, L"Scanning found the possible beginning of a new value\r\n" );
					break;
					}	// if
				}	// while

			// Valid ?
			CCLTRYE ( (vt == MARKER_BEGIN), E_UNEXPECTED );
			#endif
			}	// if

		}	// if

	// Value type
	CCLTRY	( read ( pStm, &vt ) );

	// Process value
	if (hr == S_OK)
		{
		switch (vt)
			{
			// Strings
			case VTYPE_STR :
				{
				adtString	vStr;

				// Read and store string
				CCLTRY ( readStr ( pStm, vStr ) );
				CCLTRY ( adtValue::copy ( vStr, v ) );
				}
				break;

			// Object
			case VTYPE_UNK :
				{
				IDictionary		*pDct		= NULL;
				IList				*pLst		= NULL;
				IUnknown			*pUnk		= NULL;
				IByteStream		*pStmV	= NULL;
				IMemoryMapped	*pMap		= NULL;
				IPersistValue	*pPer		= NULL;
				adtIUnknown		vUnk;
				adtValue			vK,vV;
				adtString		strId;
				U32				i,sz     = 0;

				// Object ID
				CCLTRY ( readStr ( pStm, strId ) );

				// Create object.
				if (hr == S_OK && strId.length() > 0)
					{
					CCLTRY ( cclCreateObject ( strId, NULL, IID_IUnknown, (void **) &pUnk ) );
					}	// if

				// Null object ok
				if (hr == S_OK && pUnk == NULL)
					{
					// NULL object
					vUnk	= (IUnknown *)NULL;
					}	// if

				// Does object persist itself ?
				else if (hr == S_OK && _QI(pUnk,IID_IPersistValue,&pPer) == S_OK)
					{
					// Allow object to load itself
					CCLTRY ( pPer->load ( this, pStm ) );

					// Clean up
					_RELEASE(pPer);
					}	// else if

				// List - Check 'List' first since it also support the dictionary interface
				else if (hr == S_OK && _QI(pUnk,IID_IList,&pLst) == S_OK)
					{
					// Read count of key/value pairs
					CCLTRY ( read ( pStm, &sz ) );

					// Read values until complete
					for (i = 0;hr == S_OK && i < sz;++i)
						{
						// Value
						CCLTRY ( loadValue ( pStm, vV ) );

						// Store value
						CCLTRY ( pLst->write ( vV ) );
						}	// for

					// Clean up
					_RELEASE(pLst);
					}	// if

				// Dictionary
				else if (hr == S_OK && _QI(pUnk,IID_IDictionary,&pDct) == S_OK)
					{
					// Read count of key/value pairs
					CCLTRY ( read ( pStm, &sz ) );

					// Read key/value pairs until complete
					for (i = 0;hr == S_OK && i < sz;++i)
						{
						// Key/value
						CCLTRY ( loadValue ( pStm, vK ) );
						CCLTRY ( loadValue ( pStm, vV ) );

						// Store pair
						CCLTRY ( pDct->store ( vK, vV ) );
						}	// for

					// Clean up
					_RELEASE(pDct);
					}	// if

				// Byte stream ?
				else if (hr == S_OK && _QI(pUnk,IID_IByteStream,&pStmV) == S_OK)
					{
					// Size of byte stream
					CCLTRY ( read ( pStm, &sz ) );
					CCLTRY ( pStmV->setSize ( sz ) );

					// Stream in bytes
					if (hr == S_OK && sz > 0)
						hr = pStm->copyTo ( pStmV, sz, NULL );

					// Reset position of stream
					CCLTRY ( pStmV->seek ( 0, STREAM_SEEK_SET, NULL ) );

					// Clean up
					_RELEASE(pStmV);
					}	// else if

				// Memory block
				else if (hr == S_OK && _QI(pUnk,IID_IMemoryMapped,&pMap) == S_OK)
					{
					void	*pv	= NULL;

					// Size of region
					CCLTRY ( read ( pStm, &sz ) );
					CCLTRY ( pMap->setSize ( sz ) );

					// Access the region
					CCLTRY ( pMap->getInfo ( &pv, NULL ) );

					// Read bytes
	//				dbgprintf ( L"-> StmPrsBin::load:Memory Block:0x%x:%d bytes\r\n", hr, sz );
					CCLTRY ( read ( pStm, pv, sz ) );
	//				dbgprintf ( L"<- StmPrsBin::load:Memory Block:0x%x:%d bytes\r\n", hr, sz );

					// Clean up
					_RELEASE(pMap);
					}	// else if

				// Unhandled object
				else if (hr == S_OK)
					{
					hr = E_NOTIMPL;
					lprintf ( LOG_DBG, L"Object not implemented\r\n" );
					}	// else

				// Result
				CCLTRY ( adtValue::copy ( adtIUnknown(pUnk), v ) );

				// Clean up
				_RELEASE(pUnk);
				}	// TYPE_UNKNOWN
				break;

			// Simple types

			// Integer
			case VTYPE_I4 :
				CCLTRY ( read ( pStm, (U32 *) &(v.vint) ) );
				break;
			// Long
			case VTYPE_I8 :
				CCLTRY ( read ( pStm, (U64 *) &(v.vlong) ) );
				break;
			// Float
			case VTYPE_R4 :
				CCLTRY ( read ( pStm, (U32 *) &(v.vflt) ) );
				break;
			// Double
			case VTYPE_R8 :
				CCLTRY ( read ( pStm, (U64 *) &(v.vdbl) ) );
				break;
			// Date
			case VTYPE_DATE :
				CCLTRY ( read ( pStm, (U64 *) &(v.vdate) ) );
				break;
			// Boolean
			case VTYPE_BOOL :
				CCLTRY ( read ( pStm, &(v.vbool) ) );
				break;
			// Empty
			case VTYPE_EMPTY :
				// Empty values ok
				break;
			default :
				{
				U64 at	= 0;
				pStm->seek ( 0, STREAM_SEEK_CUR, &at );
				lprintf ( LOG_ERR, L"StmPrsBin::load:Unhandled value type 0x%x @ 0x%x\r\n", vt, at );
				}	// default
				break;
			}	// switch
		}	// if

	// Set type if successful
	CCLOK ( v.vtype = vt; )

	// Termination marker for value
	CCLTRY ( read ( pStm, &vt ) );
	if (hr == S_OK)
		{
		CCLTRYE ( (vt == MARKER_END), E_UNEXPECTED );
		if (hr != S_OK)
			{
			lprintf ( LOG_DBG, L"Invalid ending marker (0x%x != 0x%x)\r\n",
									vt, MARKER_END );

			#if 0
			//
			// Additional debug code
			//
			hr = S_OK;
			for (int i = 0;i < 100 && hr == S_OK;++i)
				{
				hr = read ( pStm, &vt );
				lprintf ( LOG_DBG, L"u16 : 0x%x\r\n", vt );
				}	// for
			hr = S_OK;
			#endif
			}	// if

		}	// if

	return hr;
	}	// loadValue

HRESULT StmPrsBin :: read ( IByteStream *pStm, U16 *pBfr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Read a 2 byte value to the stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pBfr will receive the data.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U16		v;

	// Read
	hr = read ( pStm, &v, sizeof(v) );

	// To big endian
	*pBfr = SWAPS(v);

	return hr;
	}	// read

HRESULT StmPrsBin :: read ( IByteStream *pStm, U32 *pBfr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Read a 4 byte value to the stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pBfr will receive the data.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U32		v;

	// Read
	hr = read ( pStm, &v, sizeof(v) );

	// To big endian
	*pBfr = SWAPI(v);

	return hr;
	}	// read

HRESULT StmPrsBin :: read ( IByteStream *pStm, U64 *pBfr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Read a 8 byte value to the stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pBfr will receive the data.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U64		v;

	// Read
	hr = read ( pStm, &v, sizeof(v) );

	// To big endian
	*pBfr = SWAPL(v);

	return hr;
	}	// read

HRESULT StmPrsBin :: read ( IByteStream *pStm, void *pvBfr, U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Reads from stream until error or complete.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pvBfr will receive the data.
	//		-	sz is the size of the transfer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U8			*pcBfr	= (U8 *) pvBfr;
	U64		nleft		= sz;
	U64		nx;

	// Continue until error or done
	while (hr == S_OK && nleft)
		{
		CCLTRY( pStm->read ( pcBfr, nleft, &nx ) );
		CCLOK	( nleft -= nx; )
		CCLOK	( pcBfr += nx; )
		}	// while

	// Debug
//	if (hr != S_OK)
//		lprintf ( LOG_DBG, L"hr 0x%x pcBfr %p nleft %d nx %d\r\n",
//									hr, pcBfr, nleft, nx );
 
	return hr;
	}	// read

HRESULT StmPrsBin :: readStr ( IByteStream *pStm, adtString &str )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Read string from stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	str will receive the string
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U32		len;
	U16		wsz;
			
	// Length of string
	CCLTRY ( read ( pStm, &len ) );

	// Allocate for string
	CCLTRY ( str.allocate ( len ) );

	// Size of local 'WCHAR' when written
	CCLTRY ( read ( pStm, &wsz ) );

	// Different environments have different size unicode characters.
	// Example: Windows sizeof(WCHAR) = 2, OSX sizeof(WCHAR) = 4
	if (hr == S_OK)
		{
		// Same size, read directly
		if (wsz == sizeof(WCHAR))
			hr = read ( pStm, &str.at(), len*wsz );

		// 1 byte per character
		else if (wsz == 1)
			{
			char	*pc	= NULL;

			// Allocate byte array for string
			CCLTRYE ( (pc = (char *) _ALLOCMEM((len+1)*sizeof(char))) != NULL,
							E_OUTOFMEMORY );
			CCLOK   ( pc[len] = '\0'; )

			// Read string
			CCLTRY ( read ( pStm, pc, len*sizeof(char) ) );

			// Convert to local format
			CCLOK ( str = pc; )

			// Clean up
			_FREEMEM(pc);
			}	// else if

		// Remote 32 bits, Local 16 bits
		else if (wsz == 4 && sizeof(WCHAR) == 2)
			{
			U32	uc;

			// Read all characters
			for (U32 i = 0;hr == S_OK && i < len;++i)
				{
				// Read 4 byte char.
				CCLTRY ( read ( pStm, &uc, sizeof(uc) ) );

				// Store two bytes
				CCLOK  ( str.at(i) = (U16) (uc & 0xffff); )
				}	// for

			}	// else if

		// Remote 16 bits, Local 32 bits
		else if (wsz == 2 && sizeof(WCHAR) == 4)
			{
			U16	uc;

			// Read all characters
			for (U32 i = 0;hr == S_OK && i < len;++i)
				{
				// Read 2 byte char.
				CCLTRY ( read ( pStm, &uc, sizeof(uc) ) );

				// Store four bytes
				CCLOK  ( str.at(i) = (U32) (uc); )
				}	// for

			}	// else if
					
		// TODO:
		else
			{
			lprintf ( LOG_DBG, L"Different size WCHARs wsz %d, local %d!\r\n", wsz, sizeof(WCHAR) );
//			DebugBreak();
//			MessageBeep(0);
			hr = E_NOTIMPL;
			}	// else
					
		}	// if
				
	// Terminate string
	CCLOK  ( str.at(len) = '\0'; )

	return hr;
	}	// readStr

HRESULT StmPrsBin :: save ( IByteStream *pStm, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IStreamPersist
	//
	//	PURPOSE
	//		-	Save a value to the stream.
	//
	//	PARAMETERS
	//		-	pStm will receive the data
	//		-	v contains the value to save
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	adtValue	vL;

	// Options
	if (pDct->load ( adtString(L"CharSize"), vL ) == S_OK)
		iChrSz = vL;

	// Save value
	return saveValue(pStm,v);
	}	// save

HRESULT StmPrsBin :: saveValue ( IByteStream *pStm, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	IStreamPersist
	//
	//	PURPOSE
	//		-	Save a value to the stream.
	//
	//	PARAMETERS
	//		-	pStm will receive the data
	//		-	v contains the value to save
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	U16		v2;
	U32		v4;

	// Value begin
	CCLTRY ( write ( pStm, &(v2 = MARKER_BEGIN) ) );
	CCLTRY ( write ( pStm, &(v2 = v.vtype & VTYPE_TYPEMASK) ) );

	// Process value based on type
	switch ( adtValue::type(v) )
		{
		// Strings
		case VTYPE_STR :
			{
			// Ptr. to string
			const WCHAR *str = (adtValue::type(v) == VTYPE_STR) ? v.pstr : NULL;

			// Valid string ?
			CCLTRYE	( str != NULL, E_UNEXPECTED );

			// Write to stream
			CCLTRY ( writeStr ( pStm, str ) );
			}
			break;

		// Object
		case VTYPE_UNK :
			{
			IDictionary		*pDct		= NULL;
			IList				*pLst		= NULL;
			IByteStream		*pStmV	= NULL;
			IIt				*pIt		= NULL;
			IUnknown			*pUnk		= NULL;
			IMemoryMapped	*pMap		= NULL;
			IPersistValue	*pPer		= NULL;
			adtValue			vK,vV;

			// IUnknown ptr.
			pUnk	=	v.punk;

			// Null object ok
			if (hr == S_OK && pUnk == NULL)
				{
				// Store NULL object ID
				CCLTRY ( writeStr ( pStm, L"" ) );
				}	// if

			// Does object persist itself ?
			else if (hr == S_OK && _QI(pUnk,IID_IPersistValue,&pPer) == S_OK)
				{
				// Get/write Id object
				CCLTRY ( pPer->getPersistId ( vV ) );
				CCLTRY ( writeStr ( pStm, adtString(vV) ) );

				// Allow object to save itself
				CCLTRY ( pPer->save ( this, pStm ) );
				}	// else if

			// List
			else if (hr == S_OK && _QI(pUnk,IID_IList,&pLst) == S_OK)
				{
				U32	i,sz;

				// Write Id of object
				CCLTRY ( writeStr ( pStm, L"Adt.List" ) );

				// Write count of values
				CCLTRY ( pLst->size ( &sz ) );
				CCLTRY( write ( pStm, &sz ) );

				// Iterate and save values in dictionary
				CCLTRY ( pLst->iterate ( &pIt ) );
				for (i = 0;hr == S_OK && i < sz;++i)
					{
					// Read the next value
					CCLTRY ( pIt->read ( vV ) );

					// Save value 
					CCLTRY ( saveValue ( pStm, vV ) );

					// Next item in list
					pIt->next();
					}	// for

				}	// if

			// Dictionary
			else if (hr == S_OK && _QI(pUnk,IID_IDictionary,&pDct) == S_OK)
				{
				U32	i,sz;

				// Write Id of object
				CCLTRY ( writeStr ( pStm, L"Adt.Dictionary" ) );

				// Write count of value pairs
				CCLTRY( pDct->size ( &sz ) );
				CCLTRY( write ( pStm, &sz ) );

				// Iterate and save values in dictionary
				CCLTRY ( pDct->keys ( &pIt ) );
				for (i = 0;hr == S_OK && i < sz;++i)
					{
					// Read the next key
					CCLTRY ( pIt->read ( vK ) );

					// Load value for key and save both
					CCLTRY ( pDct->load ( vK, vV ) );
					CCLTRY ( saveValue ( pStm, vK ) );
					CCLTRY ( saveValue ( pStm, vV ) );

					// Next pair
					pIt->next();
					}	// while

				}	// if

			// Stream
			else if (hr == S_OK && _QI(pUnk,IID_IByteStream,&pStmV) == S_OK)
				{
				U64 opos,sz;

				// Size the stream from current position.
				CCLTRY ( pStmV->available ( &sz ) );

				// See if object has class ID.  If not default to a memory-based
				// stream.  Some objects can stream but need to be created on the other side
/*				if (hr == S_OK)
					{
					IPersist	*pPersist	= NULL;
					if (_QI(pStm,IID_IPersist,&pPersist) == S_OK)
						hr = pPersist->GetClassID ( &clsid );
					else
						clsid = CLSID_StmMemory;
					_RELEASE(pPersist);
					}	// if
*/
				// Write class id
				CCLTRY ( writeStr ( pStm, L"Io.StmMemory" ) );

				// Write size
				CCLTRY( write ( pStm, &(v4 = (U32)sz) ) );

				// Current position
				CCLTRY ( pStmV->seek ( 0, STREAM_SEEK_CUR, &opos ) );

				// Write stream
				if (hr == S_OK && sz > 0)
					hr = pStmV->copyTo ( pStm, sz, NULL );

				// Restore position
				CCLTRY ( pStmV->seek ( opos, STREAM_SEEK_SET, NULL ) );
				}	// else if

			// Memory block
			else if (hr == S_OK && _QI(pUnk,IID_IMemoryMapped,&pMap) == S_OK)
				{
				void	*pv	= NULL;
				U32	sz		= 0;

				// Lock/size the region
				CCLTRY ( pMap->getInfo ( &pv, &sz ) );

				// Write class id
				CCLTRY ( writeStr ( pStm, L"Io.MemoryBlock" ) );

				// Write size of region
				CCLTRY( write ( pStm, &(v4 = sz) ) );

				// Write region
//				dbgprintf ( L"-> StmPrsBin::save:Memory Block:0x%x:%d bytes\r\n", hr, sz );
				CCLTRY ( write ( pStm, pv, sz ) );
//				dbgprintf ( L"<- StmPrsBin::save:Memory Block:0x%x:%d bytes\r\n", hr, sz );
				}	// else if

			// Unhandled object
			else if (hr == S_OK)
				{
				hr = E_NOTIMPL;
				lprintf ( LOG_DBG, L"Object not implemented hr 0x%x,%p\r\n", hr, pUnk );
				}	// else

			// Clean up
			_RELEASE(pPer);
			_RELEASE(pMap);
			_RELEASE(pStmV);
			_RELEASE(pIt);
			_RELEASE(pDct);
			_RELEASE(pLst);
			}	// VTYPE_UNK
			break;

		// Simple types

		// Integer
		case VTYPE_I4 :
			CCLTRY( write ( pStm, (U32 *) &v.vint ) );
			break;
		// Long
		case VTYPE_I8 :
			CCLTRY( write ( pStm, (U64 *) &v.vlong ) );
			break;
		// Float
		case VTYPE_R4 :
			CCLTRY ( write ( pStm, (U32 *) &v.vflt ) );
			break;
		// Double
		case VTYPE_R8 :
			CCLTRY( write ( pStm, (U64 *) &v.vdbl ) );
			break;
		// Date
		case VTYPE_DATE :
			CCLTRY( write ( pStm, (U64 *) &v.vdate ) );
			break;
		// Boolean
		case VTYPE_BOOL :
			CCLTRY( write ( pStm, &v.vbool ) );
			break;
		// Empty
		case VTYPE_EMPTY :
			// Empty values ok
			break;
		default :
			dbgprintf ( L"StmPrsBin::save:Unhandled value type 0x%x\r\n", v.vtype );
			break;
		}	// switch

	// Value end
	CCLTRY ( write ( pStm, &(v2 = MARKER_END) ) );

	return hr;
	}	// saveValue

HRESULT StmPrsBin :: write ( IByteStream *pStm, const U16 *pBfr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Write a 2 byte value to the stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pBfr contains the data.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U16		v;

	// To big endian
	v = SWAPS(*pBfr);

	// Write
	CCLTRY ( pStm->write ( &v, sizeof(v), NULL ) );

	return hr;
	}	// write

HRESULT StmPrsBin :: write ( IByteStream *pStm, const U32 *pBfr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Write a 4 byte value to the stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pBfr contains the data.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U32		v;

	// To big endian
	v = SWAPI(*pBfr);

	// Write
	CCLTRY ( pStm->write ( &v, sizeof(v), NULL ) );

	return hr;
	}	// write

HRESULT StmPrsBin :: write ( IByteStream *pStm, const U64 *pBfr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Write a 8 byte value to the stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pBfr contains the data.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U64		v;

	// To big endian
	v = SWAPL(*pBfr);

	// Write
	CCLTRY ( pStm->write ( &v, sizeof(v), NULL ) );

	return hr;
	}	// write

HRESULT StmPrsBin :: write ( IByteStream *pStm, const void *pcvBfr, U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Writes entire buffer to stream until error or complete.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	pcvBfr contains the data.
	//		-	sz is the size of the transfer.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	const U8	*pccBfr	= (U8 *) pcvBfr;
	U64		nleft		= sz;
	U64		nx;

	// Continue until error or done
	while (hr == S_OK && nleft)
		{
		CCLTRY( pStm->write ( pccBfr, nleft, &nx ) );
		CCLOK	( nleft	-= nx; )
		CCLOK	( pccBfr += nx; )
		}	// while

	return hr;
	}	// write

HRESULT StmPrsBin :: writeStr ( IByteStream *pStm, const WCHAR *pstr )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Write string to stream.
	//
	//	PARAMETERS
	//		-	pStm is the stream
	//		-	str is the string
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	U32		len;
	U16		wsz;
			
	// Valid string ?
	CCLTRYE	( pstr != NULL, E_UNEXPECTED );

	// Write length of string
	CCLOK		( len = (U32)wcslen(pstr); )
	CCLTRY	( write ( pStm, &len ) );

	// Size of local char
	CCLOK		( wsz = iChrSz; )
	CCLTRY	( write ( pStm, &wsz ) );

	// Write string according to selected size
	if (hr == S_OK)
		{
		// System default
		if (iChrSz == sizeof(WCHAR))
			hr = write ( pStm, pstr, len*sizeof(WCHAR) );
		else if (iChrSz == 1)
			{
			char 			*pcStr = NULL;
			adtString 	str(pstr);

			// Save as ASCII Allocate/Free faster than writing one byte at a time ?
//			lprintf ( LOG_DBG, L"wsz %d\r\n", wsz);
			CCLTRY ( str.toAscii ( &pcStr ) );
			CCLTRY ( write ( pStm, pcStr, len*sizeof(char) ) );

			// Clean up
			_FREEMEM(pcStr);
			}	// else if
		else
			{
			hr = E_NOTIMPL;		
			lprintf ( LOG_DBG, L"Unsupported char size %d\r\n", wsz );
			}	// else
		}			
	// Write string

	return hr;
	}	// writeStr
