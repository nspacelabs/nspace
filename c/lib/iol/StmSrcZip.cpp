////////////////////////////////////////////////////////////////////////
//
//										StmSrcZip.CPP
//
//								ZIP archive stream source
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include "../ext/minizip/zip.h"
#include "../ext/minizip/unzip.h"
#ifdef	_WIN32
#include "../ext/minizip/iowin32.h"
#endif

StmSrcZip :: StmSrcZip ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	punkDct	= NULL;
	pDct		= NULL;
	strFile	= L"";
	strRoot	= L"";
	punzip	= NULL;
	pzip		= NULL;
	}	// SysStmsFile

HRESULT StmSrcZip :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to construct the object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Aggregrate dictionary
	CCLTRY ( COCREATEA ( L"Adt.Dictionary", (ILocations *) this, &punkDct ) );

	// Interfaces
	CCLTRY ( _QI(punkDct,IID_IDictionary,&pDct) );
	CCLOK  ( pDct->Release(); )						// Cancel 'QI'

	// Default root location
	CCLTRY(defaultRoot());

	return hr;
	}	// construct

HRESULT StmSrcZip :: copy ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Copy a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// copy

HRESULT StmSrcZip :: defaultRoot ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Generate a system-specific default root location for streams.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	#ifdef	_WIN32
	HKEY		hKey	= NULL;
	LONG		lRes;
	DWORD		dwType,dwSz,len;
	WCHAR		strBfr[1024];

	///////////////////////////////////////////////////////////////////
	// Multiple options for setting the default root directory.  Avoid
	// using 'working directory' since it changes so much between OSes
	///////////////////////////////////////////////////////////////////

	// Option 1 - Default is to use the same path where the process EXE is located
	if (hr == S_OK)
		{
		WCHAR		*pwSlash = NULL;
		DWORD		len;

		// Get path to running executable
		CCLOK   ( len = sizeof(strBfr)/sizeof(strBfr[0]); )
		CCLTRYE ( (len = GetModuleFileName ( NULL, strBfr, len )) != 0, GetLastError() );

		// Path only
		CCLTRYE ( (pwSlash = wcsrchr ( strBfr, WCHAR('\\') )) != NULL, E_UNEXPECTED );
		CCLOK   ( *(pwSlash+1) = WCHAR('\0'); )

		// Paths in nSpace are always forward slash
		for (size_t i = 0;hr == S_OK && i < wcslen(strBfr);++i)
			if (strBfr[i] == '\\')
				strBfr[i] = '/';

		// New root
		strRoot = strBfr;
		}	// if
	/*
	// Option 2 - For development the 'nsh.exe' is put into a 'debug' folder but
	#ifdef	_DEBUG
	if (	hr == S_OK										&& 
			(len = strRoot.length()) > wcslen(L"Debug/")	&&
			!WCASECMP ( strRoot, L"Debug/" ) )
		{
		// Previous path
		CCLOK ( wcscpy_s ( strBfr, sizeof(strBfr)/sizeof(strBfr[0]), strRoot ); )
		CCLOK ( strBfr[len-wcslen(L"Debug/")] = '\0'; )

		// Allocate memory for new path
		CCLTRY ( strRoot.allocate ( wcslen(strBfr)+10 ) );

		// New path
		CCLOK ( wcscpy_s ( &strRoot.at(), wcslen(strBfr)+10, strBfr ); )
		CCLOK ( wcscat_s ( &strRoot.at(), wcslen(strBfr)+10, L"graphs/" ); )
		}	// if
	#endif
	*/

	// Option 3 - Environment variable.  This is useful for using the debugger
	// or setting a root via the command line.
	if (hr == S_OK && GetEnvironmentVariable ( L"NSPACE_ROOT", 
			strBfr, sizeof(strBfr)/sizeof(strBfr[0]) ) > 0)
		strRoot = strBfr;

	// Option 4 - Hard coded root path in registry for all nSpace executions.
	if (	(hr == S_OK)
			&& 
			((lRes = RegCreateKeyEx ( HKEY_LOCAL_MACHINE, L"SOFTWARE\\nSpace",
												0, NULL, REG_OPTION_NON_VOLATILE, 
												KEY_READ, NULL,
												&hKey, NULL )) == ERROR_SUCCESS)
			&&
			((lRes = RegQueryValueEx ( hKey, L"Root", NULL, &dwType, NULL, &dwSz ))
													== ERROR_SUCCESS) )
		{
		// Allocate (+1 for possible slash)
		CCLTRY ( strRoot.allocate ( dwSz/sizeof(WCHAR) ) );

		// Read
		CCLTRYE ( ((lRes = RegQueryValueEx ( hKey, L"Root", NULL, &dwType,
						(BYTE *) &strRoot.at(), &dwSz )) == ERROR_SUCCESS), lRes );

		// Our paths always end in '\\'
		CCLOK ( len = strRoot.length(); )
		if (	hr == S_OK && strRoot[len-1] != WCHAR('/') &&
				strRoot[len-1] != WCHAR('\\') )
			{
			strRoot.at(len)	= WCHAR('\\');		// Add a slash
			strRoot.at(len+1)	= WCHAR('\0');		// Terminate at new position
			}	// if

		// Debug
//		dbgprintf ( L"SysStmsFile::open:Root nSpace Path:%s\r\n", (LPWSTR)strRoot );
		}	// if

	// Clean up
	if (hKey != NULL) RegCloseKey ( hKey );

	// Unix
	#elif	__unix__ || __APPLE__
	if ( hr == S_OK )
		{
		char	cwd[1024];

		// Working directory
		CCLTRYE ( getcwd ( cwd, sizeof(cwd) ) != NULL, errno );

		// Paths always end in '/'
		CCLOK ( strcat ( cwd, "/" ); )

		// New root
		strRoot = cwd;
		}	// else if
	#endif

	// the graphs are somewhere else
	// Default root
	CCLTRY ( pDct->store ( adtString(L"Root"), strRoot ) );
	CCLOK  ( lprintf ( LOG_DBG, L"%s", (const WCHAR *)strRoot ); )

	return hr;
	}	// defaultRoot

void StmSrcZip :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (pzip != NULL)
		{
		zipCloseFileInZip	( pzip );
		zipClose				( pzip, NULL );
		pzip		= NULL;
		}	// if
	else if (punzip != NULL)
		{
		unzCloseCurrentFile	( punzip );
		unzClose ( punzip );
		punzip	= NULL;
		}	// if

	_RELEASE(punkDct);
	}	// destruct

HRESULT StmSrcZip :: flush ( ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Flushes or synchronous the stream source.
	//
	//	PARAMETERS
	//		-	v is a value for possible future use
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;
	/*
	// State check
	CCLTRYE ( pStg != NULL, ERROR_INVALID_STATE );

	// Commit changes
	CCLTRY ( pStg->Commit(STGC_DEFAULT) );
	*/
	return hr;
	}	// flush

HRESULT StmSrcZip :: link ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Create a symbolic link to a location.
	//
	//	PARAMETERS
	//		-	pwF is the source/link location
	//		-	pwT is the destination/target location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// link

HRESULT StmSrcZip :: locations ( const WCHAR *wLoc, IIt **ppIt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns an iterator for the stream locations at the specified
	//			location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	ppIt will receive a stream location iterator.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr			= S_OK;
	void					*punz		= NULL;
	char					*paFile	= NULL;
	IList					*pLst		= NULL;
	adtString			strPath,strFile;
	zlib_filefunc_def	ff;
	unz_global_info	gi;
	unz_file_info		fi;
	int					ret;
	char					filename[256];

	// Create a list of locations and use that lists iterator
	CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLst ) );

	// Generate full path
	CCLTRY ( toPath ( wLoc, strPath ) );

	// Convert to ASCII for ZIP API
	CCLTRY ( strPath.toAscii ( &paFile ) );

	// Using Win32 file I/O
	#ifdef	_WIN32
	CCLOK ( fill_win32_filefunc ( &ff ); )
	#else
	CCLOK ( fill_fopen_filefunc ( &ff ); )
	#endif

	// Access file
	CCLTRYE ( (punz = unzOpen2 ( paFile, &ff )) != NULL, ERROR_FILE_NOT_FOUND );

	// Obtain information about ZIP file
	CCLTRYE ( (ret = unzGetGlobalInfo ( punz, &gi )) == UNZ_OK, ret );

	// Iterate all of the entries
	for (uLong idx = 0;hr == S_OK && idx < gi.number_entry;++idx)
		{
		// Obtain the entries 'filename'
		CCLTRYE ( (ret = unzGetCurrentFileInfo ( punz, &fi, filename, sizeof(filename), 
						nullptr, 0, nullptr, 0 ) == UNZ_OK), ret );

		// ZIPs store 'paths' but only has names so just report the names.
//		lprintf ( LOG_DBG, L"ret %d hr 0x%x filename %S\r\n", ret, hr, filename );
		CCLTRY ( pLst->write ( (strFile = filename) ) );

		// Proceed to the next file
		if (hr == S_OK && idx+1 < gi.number_entry)
			hr = ((ret = unzGoToNextFile ( punz )) == UNZ_OK) ? S_OK : ret;
		}	// for

	// Clean up
	if (punz != NULL)
		unzClose ( punz );

	// Create iterator for locations
	CCLTRY ( pLst->iterate ( ppIt ) );

	// Clean up
	_RELEASE(pLst);

	return hr;
	}	// locations

HRESULT StmSrcZip :: open ( IDictionary *pOpts, IUnknown **ppLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Open a a location inside the locations.
	//
	//	PARAMETERS
	//		-	pOpts contain options for the stream.
	//		-	ppLoc will receive the location object.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	char			*paFile	= NULL;
	StmZip		*pStmZip	= NULL;
	adtString	strLoc,strFile,strPath;
	adtBool		bRead,bCreate,bTrunc;
	adtValue 	vL;
	U32			len;

	// Options.
	if (hr == S_OK && pOpts->load ( adtString(L"ReadOnly"), vL ) == S_OK)
		bRead = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Create"), vL ) == S_OK)
		bCreate = vL;
	if (hr == S_OK && pOpts->load ( adtString(L"Truncate"), vL ) == S_OK)
		bTrunc = vL;

	// Read-only mode assume unzip.
	if (bRead == true && punzip == NULL)
		{
		zlib_filefunc_def	ff;

		// State check
		CCLTRYE ( pzip == NULL, E_UNEXPECTED );

		// Zip file required
		CCLTRY ( pOpts->load ( adtString(L"File"), vL ) );
		CCLTRYE( (len = (strFile = vL).length()) > 0, ERROR_INVALID_STATE );

		// Generate full path
		CCLTRY ( toPath ( strFile, strPath ) );

		// Convert to ASCII for ZIP API
		CCLTRY ( strPath.toAscii ( &paFile ) );

		// Using Win32 file I/O
		#ifdef	_WIN32
		CCLOK ( fill_win32_filefunc ( &ff ); )
		#else
		CCLOK ( fill_fopen_filefunc ( &ff ); )
		#endif

		// Access file
		CCLTRYE ( (punzip = unzOpen2 ( paFile, &ff )) != NULL, ERROR_FILE_NOT_FOUND );
		}	// if

	// Writeable assumes zip
	else if (bRead == false && pzip == NULL)
		{
		zipFile				zf	= NULL;
		zlib_filefunc_def	ff;

		// State check
		CCLTRYE ( punzip == NULL, E_UNEXPECTED );

		// Zip file required
		CCLTRY ( pOpts->load ( adtString(L"File"), vL ) );
		CCLTRYE( (len = (strFile = vL).length()) > 0, ERROR_INVALID_STATE );

		// Generate full path
		CCLTRY ( toPath ( strFile, strPath ) );

		// Convert to ASCII for ZIP API
		CCLTRY ( strPath.toAscii ( &paFile ) );

		// Using Win32 file I/O
		#ifdef	_WIN32
		CCLOK ( fill_win32_filefunc ( &ff ); )
		#else
		CCLOK ( fill_fopen_filefunc ( &ff ); )
		#endif

		// Access file
		if (hr == S_OK)
			{
			// Truncate existing file ?  Not clear if 'APPEND_STATUS_CREATE' does this so
			// delete file before proceding
//			if (bCreate && bTrunc)
//				{
//				#ifdef _WIN32
//				DeleteFile ( strPath );
//				#else
//				::remove ( paFile );
//				#endif
//				}	// if

			// Open existing file
			if (bTrunc == false)
				{
				hr = ((zf = zipOpen2 ( paFile, APPEND_STATUS_ADDINZIP, NULL, &ff ) ) != NULL)
											? S_OK : ERROR_FILE_NOT_FOUND;
//				lprintf ( LOG_DBG, L"Open existing zip attempt : %S, hr 0x%x\r\n", paFile, hr );
				}	// if

			// Create/truncate
			if (	(hr == ERROR_FILE_NOT_FOUND && bCreate) ||
					(hr == S_OK && bTrunc) )
				{
				hr = ((zf = zipOpen2 ( paFile, APPEND_STATUS_CREATE, NULL, &ff )) != NULL)
						? S_OK : E_UNEXPECTED;
//				lprintf ( LOG_DBG, L"Create zip attempt : %S, hr 0x%x\r\n", paFile, hr );
				}	// if
			}	// if

		// Result
		CCLOK ( pzip = zf; )
		}	// else if

	// Clean up
	if (hr != S_OK)
		lprintf ( LOG_DBG, L"(UN)ZIP : %d,%d,%d hr 0x%x paFile %S\r\n", 
						(bRead == true) ? 1 : 0, 
						(bCreate == true) ? 1 : 0, 
						(bTrunc == true) ? 1 : 0, hr, paFile );
	_FREEMEM(paFile);

	// State check
	CCLTRYE (	(bRead == true && punzip != NULL) ||
					(bRead == false && pzip != NULL), ERROR_PATH_NOT_FOUND );

	// Open individual stream on zip file
	CCLTRYE	( (pStmZip = new StmZip ( this )) != NULL, E_OUTOFMEMORY );
	CCLOK		( pStmZip->AddRef(); )
	CCLTRY	( pStmZip->open ( pOpts ) );
	CCLTRY	( _QI(pStmZip,IID_IUnknown,ppLoc) );

	// Clean up
	_RELEASE(pStmZip);

	return hr;
	}	// open

HRESULT StmSrcZip :: move ( const WCHAR *pwF, const WCHAR *pwT )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Move a location to another location.
	//
	//	PARAMETERS
	//		-	pwF is the source location
	//		-	pwT is the destination location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// rename

HRESULT StmSrcZip :: remove ( const WCHAR *pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Remove/delete the location from the collection
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr		= S_OK;
/*	adtString	strLoc;

	// State check
	CCLTRYE ( pStg != NULL, ERROR_INVALID_STATE );

	// Generate full path
	CCLTRY ( toPath ( pwLoc, strLoc ) );

	// Delete it
	CCLTRY ( pStg->DestroyElement ( strLoc ) );
*/
	return hr;
	}	// move

HRESULT StmSrcZip :: resolve ( const WCHAR *pwLoc, bool bAbs, 
											ADTVALUE &vLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Resolve provided location to an absolute or relative path
	//			within the stream source.
	//
	//	PARAMETERS
	//		-	pwLoc specifies the stream location
	//		-	bAbs is true to produce an absolute path, or false to produce
	//			a relative path
	//		-	vLoc will receive the new location
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// resolve

HRESULT StmSrcZip :: status ( const WCHAR *wLoc, IDictionary *pSt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	FROM	ILocations
	//
	//	PURPOSE
	//		-	Returns information about the stream at the specified location.
	//
	//	PARAMETERS
	//		-	wLoc specifies the stream location
	//		-	pSt will receive the information about the stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// status

HRESULT StmSrcZip :: toPath ( const WCHAR *wLoc, adtString &sLocFull )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ILocations
	//
	//	PURPOSE
	//		-	Generates a 'full path' to the specified file.
	//
	//	PARAMETERS
	//		-	wLoc contains the file or partial path to a file.
	//		-	sFull will receive the full path to the file
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr 		= S_OK;

	// Generate a full path if not already an absolute path
	if (	hr == S_OK					&& 
			wcslen(wLoc) > 0			&&
			wLoc[0] != WCHAR('\\')	&&
			wLoc[0] != WCHAR('/')
			#if	defined(_WIN32) && !defined(UNDER_CE)
			&& wLoc[1] != WCHAR(':')					// Drive letter ?
			#endif
		)
		{
		// Load root in case it has changed
		CCLTRY ( pDct->load ( adtString(L"Root"), strRoot ) );

		// Generate path that includes the default root
		U32
		len = strRoot.length()+(U32)wcslen(wLoc)+1;
		CCLTRY ( sLocFull.allocate ( len )  );
		CCLOK	 ( WCSCPY ( &sLocFull.at(), len, strRoot ); )
		CCLOK	 ( WCSCAT ( &sLocFull.at(), len, wLoc ); )
		}	// if

	// Using root directly ?
	else if (wcslen(wLoc) == 0)
		hr = pDct->load ( adtString(L"Root"), sLocFull );

	// Absolute paths are unmodified
	else if (hr == S_OK)
		sLocFull = wLoc;

	return hr;
	}	// toPath

