////////////////////////////////////////////////////////////////////////
//
//									STMCOPY.CPP
//
//					Implementation of the copy StreamCopy node
//
////////////////////////////////////////////////////////////////////////

#include "iol_.h"
#include <stdio.h>

StreamCopy :: StreamCopy ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	punkSrc	= NULL;
	punkDst	= NULL;
	iSz		= 0;
	bVol		= false;
	}	// StreamCopy

void StreamCopy :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed
	//
	////////////////////////////////////////////////////////////////////////
	_RELEASE(punkSrc);
	_RELEASE(punkDst);
	}	// destruct

volatile void *StreamCopy :: memcpyv ( volatile void *dst, 
													const volatile void *src, size_t len )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Custom 'memcpy' function using volatile ptrs.
	//
	//	PARAMETERS
	//		-	dst is the destination
	//		-	src is the source
	//		-	len is the amount to copy
	//
	//	RETURN VALUE
	//		Ptr to destination
	//
	////////////////////////////////////////////////////////////////////////

	// Any bytes to copy ?
	if (len > 0)
		{
		size_t i;

		// Is source and destination 4 byte aligned for faster copy ?
		if (	(U64)dst % sizeof(U32) == 0 &&
				(U64)src % sizeof(U32) == 0 )
			{
			volatile 		U32 *d	= (volatile U32 *) dst;
			const volatile U32 *s 	= (const volatile U32 *) src;

			// Copying U32 at a time
			len = len/sizeof(U32);

//			if (len >= 8)
//				lprintf ( LOG_DBG, L"s %p 0x%x 0x%x\r\n", 
//								s, ((U32 *)s)[0], ((U32 *)s)[1] );

			// Copy
			for (i = 0; i < len; ++i)
				d[i] = s[i];

//			lprintf ( LOG_DBG, L"Volatile copied %d bytes (0x%x,0x%x)\r\n", 
//									len, ((U32 *)s)[0], ((U32 *)s)[1] );
			}	// if

		// Byte copy
		else
			{
			volatile 		U8 *d	= (volatile U8 *) dst;
			const volatile U8 *s	= (const volatile U8 *) src;

			// Copy
			for (i = 0; i < len; ++i)
				d[i] = s[i];
			}	// else

		}	// if

	return dst;
	}	// memcpyv

HRESULT StreamCopy :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Attach
	if (bAttach)
		{
		adtValue	v;

		// Defaults
		if (pnDesc->load ( adtString(L"Size"), v ) == S_OK)
			iSz = v;
		if (pnDesc->load ( adtString(L"Volatile"), v ) == S_OK)
			bVol = adtBool(v);
		}	// if

	// Detach
	else
		{
		_RELEASE(punkSrc);
		_RELEASE(punkDst);
		}	// if

	return S_OK;
	}	// onAttach

HRESULT StreamCopy :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Copy
	if (_RCP(Fire))
		{
		IByteStream		*pStmSrc		= NULL;
		IByteStream		*pStmDst		= NULL;
		IMemoryMapped	*pMemSrc		= NULL;
		IMemoryMapped	*pMemDst		= NULL;
		void				*pvMemSrc	= NULL;
		void				*pvMemDst	= NULL;
		U64				sz;

		// State check
		CCLTRYE ( (punkDst != NULL), ERROR_INVALID_STATE );
		CCLTRYE ( (punkSrc != NULL), ERROR_INVALID_STATE );

		// Determine which interface each object supports
		CCLOK ( _QI(punkSrc,IID_IMemoryMapped,&pMemSrc); )
		CCLOK ( _QI(punkSrc,IID_IByteStream,&pStmSrc); )
		CCLOK ( _QI(punkDst,IID_IMemoryMapped,&pMemDst); )
		CCLOK ( _QI(punkDst,IID_IByteStream,&pStmDst); )

		// Perform copy
		if (hr == S_OK)
			{
			// Source is a stream
			if (pStmSrc != NULL)
				{
				// Stream to stream
				if (pStmDst != NULL)
					hr = pStmSrc->copyTo ( pStmDst, iSz, &sz );

				// Stream to memory mapped
				else if (pMemDst != NULL)
					{
					U64		nleft = iSz;

					// Obtain full size of remaining bytes in stream if necessary
					if (nleft == 0)
						hr = pStmSrc->available(&nleft);

					// Valid size ?
					if (hr == S_OK && nleft > 0)
						{
						U8		*pcDst	= NULL;
						U64	ncpy;

						// Preset size needed for stream and get ptr.
						CCLTRY ( pMemDst->setSize ( (U32) nleft ) );
						CCLTRY ( pMemDst->getInfo ( (void **) &pcDst, NULL ) );

						// Copy into memory block
						while (hr == S_OK && nleft > 0)
							{
							// Next amount to copy
							ncpy = (nleft < sizeof(cBfr)) ? nleft : sizeof(cBfr);

							// Read block
							CCLTRY ( pStmSrc->read ( cBfr, ncpy, &ncpy ) );

							// Write to memory
							if (hr == S_OK && ncpy > 0)
								{
								if (!bVol)
									memcpy ( pcDst, cBfr, ncpy );
								else
									memcpyv ( (volatile void *) pcDst, (volatile void *)cBfr, ncpy );
								pcDst += ncpy;
								nleft -= ncpy;
								}	// if

							}	// while

						}	// if

					}	// else if

				// ?
				else
					hr = E_NOTIMPL;
				}	// if

			// Source is memory mapped
			else if (pMemSrc != NULL)
				{
//				lprintf ( LOG_DBG, L"pMemSrc %p\r\n", pMemSrc );

				// Memory mapped to stream
				if (pStmDst != NULL)
					{
					U32	nCpy = iSz;
					U32	nChk;

					// Size of source data
					CCLTRY ( pMemSrc->getInfo(&pvMemSrc,&nChk) );

					// Adjust
					if (hr == S_OK && (nCpy == 0 || nCpy > nChk))
						nCpy = nChk;

					// To support 'volatile' memory, copying to a local buffer must happen first.
					if (bVol)
						{
						volatile U8	*pcSrc	= (volatile U8 *) pvMemSrc;
						U32			nLeft		= nCpy;
						U32			nCpyNow;

						// Copy volatile blocks and write to stream
//						lprintf ( LOG_DBG, L"Volatile copy from memory block to stream %d bytes (0x%x,0x%x)\r\n", 
//										nLeft, ((U32 *)pcSrc)[0], ((U32 *)pcSrc)[1] );
						while (hr == S_OK && nLeft > 0)
							{
							// Amount to copy
							nCpyNow = (nLeft < sizeof(cBfr)) ? nLeft : sizeof(cBfr);

							// Copy to local buffer
							memcpyv ( cBfr, pcSrc, nCpyNow );

							// Write to stream
							CCLTRY ( pStmDst->write ( cBfr, nCpyNow, NULL ) );

							// Next block
							nLeft -= nCpyNow;
							}	// while

						}	// if

					else
						{
						// Write block to stream
//						lprintf ( LOG_DBG, L"hr 0x%x pStmDst %p pvMemSrc %p nChk %d nCpy %d\r\n", 
//												hr, pStmDst, pvMemSrc, nChk, nCpy );
						CCLTRY ( pStmDst->write ( pvMemSrc, nCpy, NULL ) );
//						lprintf ( LOG_DBG, L"hr 0x%x\r\n", hr );
						}	// else

					}	// if

				// Memory mapped to memory mapped
				else if (pMemDst != NULL)
					{
					U32	nChk,nCpy = iSz;

					// Size of source data
					CCLTRY ( pMemSrc->getInfo(&pvMemSrc,&nChk) );
//					lprintf ( LOG_DBG, L"Memory to memory pvMemSrc %p nCpy %d nChk %d\r\n", pvMemSrc, nCpy, nChk );

					// Adjust
					if (hr == S_OK && (nCpy == 0 || nCpy > nChk))
						nCpy = nChk;

					// Ensure destination has enough room
					CCLTRY ( pMemDst->getInfo ( NULL, &nChk ) );
//					lprintf ( LOG_DBG, L"Memory to memory pvMemDst %p nChk %d\r\n", pvMemDst, nChk );
					if (hr == S_OK && nChk < nCpy)
						hr = pMemDst->setSize ( nCpy );

					// Retrieve destination info
					CCLTRY ( pMemDst->getInfo ( &pvMemDst, NULL ) );

					// Copy data
//					CCLOK ( memcpy ( pvMemDst, pvMemSrc, nCpy ); )
					if (hr == S_OK)
						{
						if (!bVol)
							memcpy ( pvMemDst, pvMemSrc, nCpy );
						else
							memcpyv ( (volatile void *) pvMemDst, (volatile void *) pvMemSrc, nCpy );
						}	// if

					}	// else if
				}	// else if
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(punkDst) );
		else
			{
			lprintf ( LOG_DBG, L"%s: Unable to copy memory/streams Dst %p/%p Src %p/%p hr 0x%x\r\n",
							(LPCWSTR)strnName, pMemDst, pStmDst, pMemSrc, pStmSrc, hr );
			_EMT(Error,adtInt(hr) );
			}	// else

		// Clean up
		_RELEASE(pStmDst);
		_RELEASE(pMemDst);
		_RELEASE(pStmSrc);
		_RELEASE(pMemSrc);
		}	// if

	// State
	else if (_RCP(Source))
		{
		adtIUnknown	unkV(v);
		_RELEASE(punkSrc);
		hr = _QISAFE(unkV,IID_IUnknown,&punkSrc);
		}	// else if
	else if (_RCP(Destination))
		{
		adtIUnknown	unkV(v);
		_RELEASE(punkDst);
		hr = _QISAFE(unkV,IID_IUnknown,&punkDst);
		}	// else if
	else if (_RCP(Size))
		iSz = adtInt(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

