////////////////////////////////////////////////////////////////////////
//
//									PersistImage.CPP
//
//				Implementation of the image PersistImageence node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

PersistImage :: PersistImage ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctImg	= NULL;
	pDctSrc	= NULL;
	strLoc	= L"";
	bGlb		= false;
	}	// PersistImage

HRESULT PersistImage :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Location"), vL ) == S_OK)
			adtValue::toString ( vL, strLoc );
		if (pnDesc->load ( adtString(L"Global"), vL ) == S_OK)
			onReceive(prGlobal,vL);
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDctSrc);
		_RELEASE(pDctImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT PersistImage :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Load
	if (_RCP(Load))
		{
		// State check
		CCLTRYE ( (pDctImg != NULL), ERROR_INVALID_STATE );

		// Image source specified ?
		if (hr == S_OK && pDctSrc != NULL)
			{
			IDictionary	*pImgSrcUse = NULL;
			cvMatRef		*pMatSrc		= NULL;
			cvMatRef		matRef;
			cv::Mat		matDst;

			// Obtain image refence and result image
			CCLTRY ( Prepare::extract ( pDctSrc, adtValue(), &pImgSrcUse, &pMatSrc ) );

			// Attempt to decode the image directly from memory
			CCLOK ( matDst = cv::imdecode ( *(pMatSrc->mat), cv::IMREAD_UNCHANGED ); )

			// Results
			CCLOK  ( matRef.mat = &matDst; )
			CCLTRY ( image_from_mat ( &matRef, pDctImg, bGlb ) );
			
			// Clean up
			matRef.mat = NULL;
			_RELEASE(pImgSrcUse);
			_RELEASE(pMatSrc);
			}	// if

		// Assume location
		else if (hr == S_OK)
			{
			// State check
			CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

			// Load image from file
			CCLTRY ( image_load ( strLoc, pDctImg, bGlb ) );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Load,adtIUnknown(pDctImg));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Save
	else if (_RCP(Save))
		{
		// State check
		CCLTRYE ( (pDctImg != NULL), ERROR_INVALID_STATE );
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );

		// Save image to destination
		CCLTRY ( image_save ( pDctImg, strLoc ) );

		// Result
		if (hr == S_OK)
			_EMT(Save,adtIUnknown(pDctImg));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctImg);
		_QISAFE(unkV,IID_IDictionary,&pDctImg);
		}	// else if
	else if (_RCP(Source))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctSrc);
		_QISAFE(unkV,IID_IDictionary,&pDctSrc);
		}	// else if
	else if (_RCP(Location))
		hr = adtValue::toString ( v, strLoc );
	else if (_RCP(Global))
		bGlb = adtBool(v);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

