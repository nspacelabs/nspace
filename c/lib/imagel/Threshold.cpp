////////////////////////////////////////////////////////////////////////
//
//									THRESH.CPP
//
//				Implementation of the image threshold node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Threshold :: Threshold ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg		= NULL;
	adtValue::copy(adtInt(0),vT);
	strOp		= L"Zero";
	strAdapt	= L"";
	}	// Threshold

HRESULT Threshold :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Op"), vL ) == S_OK)
			adtValue::toString ( vL, strOp );
		if (pnDesc->load ( adtString(L"Adaptive"), vL ) == S_OK)
			adtValue::toString ( vL, strAdapt );
		pnDesc->load ( adtString(L"Value"), vT );
		pnDesc->load ( adtString(L"Maximum"), vMax );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Threshold :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Perform operation
//		CCLOK ( image_to_debug ( pMat, L"Threshold 1", L"c:/temp/thresh1.png" ); )
		if (hr == S_OK)
			{
			try
				{
				//
				// Mat
				//
				if (pMat->isMat())
					{
					if (!WCASECMP(strOp,L"Zero"))
						cv::threshold ( *(pMat->mat), *(pMat->mat), adtDouble(vT), 0, cv::THRESH_TOZERO );
					else if (!WCASECMP(strOp,L"Truncate"))
						cv::threshold ( *(pMat->mat), *(pMat->mat), adtDouble(vT), 0, cv::THRESH_TRUNC );
					else if (!WCASECMP(strOp,L"Binary"))
						{
						if (strAdapt.same(L"Mean"))
							cv::adaptiveThreshold ( *(pMat->mat), *(pMat->mat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY, 111, adtDouble(vT) );
						else if (strAdapt.same(L"Gaussian"))
							cv::adaptiveThreshold ( *(pMat->mat), *(pMat->mat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY, 111, adtDouble(vT) );
						else
							cv::threshold ( *(pMat->mat), *(pMat->mat), adtDouble(vT), adtDouble(vMax), cv::THRESH_BINARY );
						}	// if
					else if (!WCASECMP(strOp,L"BinaryInv"))
						{
						if (strAdapt.same(L"Mean"))
							cv::adaptiveThreshold ( *(pMat->mat), *(pMat->mat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY_INV, 3, adtDouble(vT) );
						else if (strAdapt.same(L"Gaussian"))
							cv::adaptiveThreshold ( *(pMat->mat), *(pMat->mat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY_INV, 3, adtDouble(vT) );
						else
							cv::threshold ( *(pMat->mat), *(pMat->mat), adtDouble(vT), adtDouble(vMax), cv::THRESH_BINARY_INV );
						}	// else if
					}	// else

				//
				// UMat
				//
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					{
					if (!WCASECMP(strOp,L"Zero"))
						cv::threshold ( *(pMat->umat), *(pMat->umat), adtDouble(vT), 0, cv::THRESH_TOZERO );
					else if (!WCASECMP(strOp,L"Truncate"))
						cv::threshold ( *(pMat->umat), *(pMat->umat), adtDouble(vT), 0, cv::THRESH_TRUNC );
					else if (!WCASECMP(strOp,L"Binary"))
						{
						if (strAdapt.same(L"Mean"))
							cv::adaptiveThreshold ( *(pMat->umat), *(pMat->umat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY, 3, adtDouble(vT) );
						else if (strAdapt.same(L"Gaussian"))
							cv::adaptiveThreshold ( *(pMat->umat), *(pMat->umat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY, 3, adtDouble(vT) );
						else
							cv::threshold ( *(pMat->umat), *(pMat->umat), adtDouble(vT), adtDouble(vMax), cv::THRESH_BINARY );
						}	// if
					else if (!WCASECMP(strOp,L"BinaryInv"))
						{
						if (strAdapt.same(L"Mean"))
							cv::adaptiveThreshold ( *(pMat->umat), *(pMat->umat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY_INV, 3, adtDouble(vT) );
						else if (strAdapt.same(L"Gaussian"))
							cv::adaptiveThreshold ( *(pMat->umat), *(pMat->umat), adtDouble(vMax), cv::ADAPTIVE_THRESH_MEAN_C,
															cv::THRESH_BINARY_INV, 3, adtDouble(vT) );
						else
							cv::threshold ( *(pMat->umat), *(pMat->umat), adtDouble(vT), adtDouble(vMax), cv::THRESH_BINARY_INV );
						}	// else if
					}	// else if
				#endif

				//
				// GPU
				//
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					{
					if (!WCASECMP(strOp,L"Zero"))
						cv::cuda::threshold ( *(pMat->gpumat), *(pMat->gpumat), adtDouble(vT), 0, cv::THRESH_TOZERO );
					else if (!WCASECMP(strOp,L"Truncate"))
						cv::cuda::threshold ( *(pMat->gpumat), *(pMat->gpumat), adtDouble(vT), 0, cv::THRESH_TRUNC );
					else if (!WCASECMP(strOp,L"Binary"))
						cv::cuda::threshold ( *(pMat->gpumat), *(pMat->gpumat), adtDouble(vT), adtDouble(vMax), cv::THRESH_BINARY );
					else if (!WCASECMP(strOp,L"BinaryInv"))
						cv::cuda::threshold ( *(pMat->gpumat), *(pMat->gpumat), adtDouble(vT), adtDouble(vMax), cv::THRESH_BINARY_INV );
					}	// if
				#endif

				}	// try
			catch ( cv::Exception &ex )
				{
				lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = E_UNEXPECTED;
				}	// catch


			}	// if

		// Debug
//		CCLOK ( image_to_debug ( pMat, L"Threshold 2", L"c:/temp/thresh2.png" ); )

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// Range check
	else if (_RCP(InRange))
		{
		IDictionary	*pDctOut	= NULL;
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		IThis			*pMatT	= NULL;
		adtInt		iV(vT),iMax(vMax);
		cv::Scalar	clrV,clrMax;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Colors.  Match the channel count of the source image.
		if (hr == S_OK)
			{
			double dL[4] = {	(double)((iV >> 0) & 0xff),		(double)((iV >> 8) & 0xff),
									(double)((iV >> 16) & 0xff),		(double)((iV >> 24) & 0xff) };
			double dU[4] = {	(double)((iMax >> 0) & 0xff),		(double)((iMax >> 8) & 0xff),
									(double)((iMax >> 16) & 0xff),	(double)((iMax >> 24) & 0xff) };
			switch (pMat->channels())
				{
				case 1 :
					clrV		= cv::Scalar	( dL[0] );
					clrMax	= cv::Scalar	( dU[0] );
					break;
				case 2 :
					clrV		= cv::Scalar	( dL[0], dL[1] );
					clrMax	= cv::Scalar	( dU[0], dU[1] );
					break;
				case 3 :
					clrV		= cv::Scalar	( dL[0], dL[1], dL[2] );
					clrMax	= cv::Scalar	( dU[0], dU[1], dU[2] );
					break;
				case 4 :
					clrV		= cv::Scalar	( dL[0], dL[1], dL[2], dL[3] );
					clrMax	= cv::Scalar	( dU[0], dU[1], dU[2], dU[3] );
					break;
				default : 
					hr = ERROR_NOT_SUPPORTED;
				}	// switch
			}	// if

		// Perform operation
		try
			{
			// Wrap matrix reference object in an image dictionary for external use
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctOut ) );

			//
			// Mat
			//
			if (pMat->isMat())
				{
				cv::Mat	matOut;

				// Compute output
				CCLOK ( cv::inRange ( *(pMat->mat), clrV, clrMax, matOut ); )

				// Result
				CCLTRY ( pDctOut->store ( adtString(L"cvMatRef"), 
							adtIUnknown ( (pMatT = new cvMatRef ( matOut ) ) ) ) );

				// Clean up
				_RELEASE(pMatT);
				}	// else

			//
			// UMat
			//
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				{
				cv::UMat	matOut;

				// Example RGB cubes, show blue
//				clrV = cv::Scalar(64, 0, 0, 0);
//				clrMax = cv::Scalar(255, 128, 128, 255);

				// Compute output
				CCLOK ( cv::inRange ( *(pMat->umat), clrV, clrMax, matOut ); )

				// Result
				CCLTRY ( pDctOut->store ( adtString(L"cvMatRef"), 
							adtIUnknown ( (pMatT = new cvMatRef ( matOut ) ) ) ) );

				// Clean up
				_RELEASE(pMatT);
				}	// else if
			#endif

			//
			// GPU
			//
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				{
				cv::cuda::GpuMat	matOut;

				// Compute output
				CCLOK ( cv::cuda::inRange ( *(pMat->gpumat), clrV, clrMax, matOut ); )

				// Result
				CCLTRY ( pDctOut->store ( adtString(L"cvMatRef"), 
							adtIUnknown ( (pMatT = new cvMatRef ( matOut ) ) ) ) );

				// Clean up
				_RELEASE(pMatT);
				}	// if
			#endif
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = E_UNEXPECTED;
			}	// catch

		// Result
		if (hr == S_OK)
			_EMT(InRange,adtIUnknown(pDctOut));
		else
			{
			lprintf ( LOG_DBG, L"%s:hr 0x%x\r\n", (LPCWSTR)strnName, hr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pDctOut);
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Maximum))
		adtValue::copy ( v, vMax );
	else if (_RCP(Value))
		adtValue::copy ( v, vT );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

