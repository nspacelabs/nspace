////////////////////////////////////////////////////////////////////////
//
//									PREPARE.CPP
//
//				Implementation of the image preparation node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals
static bool	bGPUInit = false;							// GPU detectiion/initialization has occured
bool	bCuda				= false;							// CUDA enabled
bool	bUMat				= false;							// UMat/OpenCL enabled

Prepare :: Prepare ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg	= NULL;
	bCPU	= false;
	bRel	= false;
	bGlb	= false;
	}	// Prepare

HRESULT Prepare :: extract (	IDictionary *pDct, const ADTVALUE &vAlt,
										IDictionary **ppDct, cvMatRef **ppMat )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Prepare image dictionary ptr and optionally the embedded
	//			cvMatRef object.
	//
	//	PARAMETERS
	//		-	pDct is the default image dictionary to use
	//		-	vAlt is checked for a dictionary if 'pDct' is NULL.
	//		-	ppDct will receive the image dictionary
	//		-	ppMat (if non-NULL) will receive the embedded cvMatRef object
	//			inside the dictionary
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT hr = S_OK;

	// Image dictionary to use
	if ( (*ppDct = pDct) == NULL )
		{
		adtIUnknown unkV(vAlt);
		CCLTRY(_QISAFE(unkV,IID_IDictionary,ppDct));
		}	// if
	else
		(*ppDct)->AddRef();

	// Caller want matrix object ?
	if (hr == S_OK && ppMat != NULL)
		{
		IThis				*pThis	= NULL;
		adtIUnknown		unkV;
		adtValue			vL;

		// Reference counted object
		CCLTRY ( (*ppDct)->load (	adtString(L"cvMatRef"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IThis,&pThis) );
		CCLTRY ( pThis->getThis ( (void **) ppMat ) );

		// NOTE: Leaving 'pThis' referenced so that caller receives
		// an 'AddRef' ptr. that needs to be released.
		}	// if

	return hr;
	}	// extract

HRESULT Prepare :: gpuInit ( bool bGPU, bool bOcl, bool bReInit )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Initialize any GPU support on the local machine.
	//
	//	PARAMETERS
	//		-	bGPU is true to attempt to use Cuda
	//		-	bOcl is true to attempt to use OpenCL (Umat)
	//		-	bReInit is true to switch acceleration preference
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
//	static LONG		lGpuRefCnt;

	// Since performance is not an issue when debugging default to 
	// CPU only.  The only time to enable this in debug mode is if this 
	// specifically is being debugged.
	// NOTE: Running from the debugger can be very slow to start cuda/open CL.
	// CUDA 8/OpenCV 3.3/Visual Studio 15 combo seems to have improved this 
	// so for now allow auto-detection.
//	#if 0
//	#ifndef	_DEBUG
//	#ifdef	_DEBUG
//	bCuda		= false;
//	bUMat		= false;
//	bGPUInit = true;
//	#endif

	// Re-initialize ?
	if (bReInit)
		{
		bGPUInit = false;
		bUMat		= false;
		bCuda		= false;
		}	// if

	// Already initialized ?
	if (!bGPUInit)
		{
		// Any CUDA-enabled devices ?
		bCuda = false;
		#ifdef	HAVE_OPENCV_CUDA
		int	ret;
		if (bGPU && (ret = cv::cuda::getCudaEnabledDeviceCount()) > 0)
			{
			// Perform some sort of GPU operation to intiailize Cuda for the
			// first time which can take while (many seconds on a fresh system)
			cv::cuda::GpuMat	gpumat;
			cv::Mat				mat(10,10,CV_8UC1);
			gpumat.upload(mat);

			// Enable Cuda
			lprintf ( LOG_INFO, L"Cuda enabled devices : %d\r\n", ret );
			bCuda = true;

			// For debug to force CPU mode
	//		bCuda = false;

			// Do not use OpenCL
			bUMat = false;
			}	// if
		else 
		#endif
		if (bOcl)
			{
			// Debug
			lprintf ( LOG_INFO, L"No Cuda enabled devices\r\n" );
			bUMat = false;

			// Open CV 3.X has automatic OpenCL support via the new "UMat" object
			#ifdef	HAVE_OPENCV_UMAT
			if (cv::ocl::haveOpenCL())
				bUMat = true;

			// Retrieving the OpenCL information below seems awfully crashy,
			// not sure what is going, disabled for now.  Sometimes it works fine
			// others, not.

			// Debug information
//			cv::ocl::Device	
//			dev = cv::ocl::Device::getDefault();
//			cv::String
//			name		= dev.name();
//			cv::String 
//			vendor	= dev.vendorName();

			// Debug
			lprintf ( LOG_INFO, L"OpenCL %s\r\n", (bUMat) ? L"enabled" : L"disabled" );
//			if (bUMat)
//				lprintf ( LOG_DBG, L"Name : %S : %S : %d.%d\r\n", name.c_str(), vendor.c_str(),
//								dev.deviceVersionMajor(), dev.deviceVersionMinor() );

			// Perform some sort of GPU operation to intiailize Cuda for the
			// first time which can take while (many seconds on a fresh system)
			cv::ocl::setUseOpenCL(bUMat);
			if (bUMat)
				{
				cv::UMat	umat;
				cv::Mat	mat(10,10,CV_8UC1);
				mat.copyTo ( umat );
				}	// if
			#endif
			}	// else

		// GPU initialized
		bGPUInit = true;
		}	// if

	return S_OK;
	}	// gpuInit

HRESULT Prepare :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"CPU"), vL ) == S_OK)
			bCPU = vL;
		if (pnDesc->load ( adtString(L"Release"), vL ) == S_OK)
			bRel = vL;
		if (pnDesc->load ( adtString(L"Global"), vL ) == S_OK)
			bGlb = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Prepare :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Upload
	if (_RCP(Upload))
		{
		IDictionary		*pImgUse = NULL;
		cvMatRef			*pMat		= NULL;
		cv::Mat			*mat		= NULL;
		IMemoryMapped	*pBits	= NULL;
		void *			pvBits	= NULL;
		adtValue			vL;

		// Obtain image refence
		CCLTRY ( extract ( pImg, v, &pImgUse, NULL ) );

		// Since nothing in the image library can be done without 'uploading' an
		// image first, initialize any GPU support here
		if (hr == S_OK && !bGPUInit)
			gpuInit();

		// Allow already prepared images to just pass through
		if (hr == S_OK && pImgUse->load ( adtString(L"cvMatRef"), vL ) != S_OK)
			{
			// Wrap image bits in OpenCv matrix
			CCLTRY ( image_to_mat ( pImgUse, &mat, &pBits, &pvBits ) );

			// Create a blank matrix type
			CCLTRY ( Create::create ( pImgUse, mat->cols, mat->rows, mat->type(), &pMat, bCPU ) );

			// Debug
//			CCLOK ( lprintf ( LOG_DBG, L"%s) Upload, type %s\r\n",
//						(LPCWSTR)strnName,
//						(pMat->isMat()) ? L"CPU" :
//						(pMat->isUMat()) ? L"UMAT" :
//						(pMat->isGPU()) ? L"GPU" : L"Unknown" ); )

			// CPU
			if (hr == S_OK && pMat->isMat())
				{
				// Shallow copy.  Image is already in CPU memory, just keep bits wrapped
				// instead of allocating and copying to a new image.  Originally changed to
				// reduce memory usage on an embedded system dealing with large images.
				*(pMat->mat) = *mat;

				// Copy the wrapper bits into the matrix
//				mat->copyTo(*(pMat->mat));
//				*(pMat->mat) = mat->clone();

				// DEBUG
//				pImgUse->remove ( adtString(L"Bits") );
				}	// else

			// OCL
			#ifdef	HAVE_OPENCV_UMAT
			else if (hr == S_OK && pMat->isUMat())
				mat->copyTo ( *(pMat->umat) );
			#endif

			// GPU
			#ifdef	HAVE_OPENCV_CUDA
			else if (hr == S_OK && pMat->isGPU())
				pMat->gpumat->upload ( *mat );
			#endif

			// Store 'uploaded' image in image dictionary
			CCLTRY ( pImgUse->store (	adtString(L"cvMatRef"), adtIUnknown(*pMat) ) );
			}	// if

		// Clean up
		if (mat != NULL)
			delete mat;
		_RELEASE(pMat);
		_RELEASE(pBits);

		// Result
		if (hr == S_OK)
			_EMT(Upload,adtIUnknown(pImgUse));
		else
			{
			lprintf ( LOG_ERR, L"%s : Unable to prepare image %d", (LPCWSTR)strnName, hr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pImgUse);

		}	// if

	// Download
	else if (_RCP(Download))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;

		// Obtain image refence
		CCLTRY ( extract ( pImg, v, &pImgUse, &pMat ) );

		// Copy image from source
		if (hr == S_OK && bRel == false)
			{
			// Store resulting image in dictionary
			hr = image_from_mat ( pMat, pImgUse, bGlb );
			}	// if

		// Ensure any matrix object is removed
		if (pImgUse != NULL)
			pImgUse->remove ( adtString(L"cvMatRef") );

		// Result
		if (hr == S_OK)
			_EMT(Download,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

