////////////////////////////////////////////////////////////////////////
//
//									CONVERT.CPP
//
//				Implementation of the image format conversion node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Convert :: Convert ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg 		= NULL;
	bClrUse	= true;
	}	// Convert

HRESULT Convert :: convertTo (	cvMatRef *pMatSrc, cvMatRef *pMatDst, 
											U32 fmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Global utility to convert data to a particular format.
	//
	//	PARAMETERS
	//		-	pMatSrc is the source data
	//		-	pMatDst will receive the converted data
	//		-	fmt is the new OpenCV format
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	bool		bMatch	= (	(pMatSrc->isUMat() && pMatDst->isUMat()) ||
									(pMatSrc->isGPU() && pMatDst->isGPU()) ||
									(pMatSrc->isMat() && pMatDst->isMat()) );

	// Both CPU bound or non matching types
	if (!bMatch || pMatSrc->isMat())
		{
		cv::Mat	matCnv;

		// Copy to CPU
		if (pMatSrc->isMat())
			matCnv = *(pMatSrc->mat);
		#ifdef	HAVE_OPENCV_UMAT
		else if (pMatSrc->isUMat())
			pMatSrc->umat->copyTo ( matCnv );
		#endif
		#ifdef	HAVE_OPENCV_CUDA
		else if (pMatSrc->isGPU())
			pMatSrc->gpumat->download ( matCnv );
		#endif

		// Convert 'to' specified format.
		matCnv.convertTo ( matCnv, fmt );

		// Copy back
		if (pMatDst->isMat())
			matCnv.copyTo ( *(pMatDst->mat) );
		#ifdef	HAVE_OPENCV_UMAT
		else if (pMatDst->isUMat())
			matCnv.copyTo ( *(pMatDst->umat) );
		#endif
		#ifdef	HAVE_OPENCV_CUDA
		else if (pMatDst->isGPU())
			pMatDst->gpumat->upload ( matCnv );
		#endif
		}	// if
	#ifdef	HAVE_OPENCV_UMAT
	else if (pMatSrc->isUMat() && pMatDst->isUMat())
		hr = convertTo ( pMatSrc->umat, pMatDst->umat, fmt );
	#endif
	#ifdef	HAVE_OPENCV_CUDA
	else if (pMatSrc->isGPU() && pMatDst->isGPU())
		hr = convertTo ( pMatSrc->gpumat, pMatDst->gpumat, fmt );
	#endif
	else
		hr = E_NOTIMPL;

	return hr;
	}	// convertTo

#ifdef	HAVE_OPENCV_UMAT
HRESULT Convert :: convertTo (	cv::UMat *pMatSrc, 
											cv::UMat *pMatDst, U32 fmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Global utility to convert data to a particular format.
	//
	//	PARAMETERS
	//		-	pMatSrc is the source data
	//		-	pMatDst will receive the converted data
	//		-	fmt is the new OpenCV format
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr = S_OK;

	// Direct in-memory conversion (?)
	pMatSrc->convertTo ( *(pMatDst), fmt );

	return hr;
	}	// convertTo
#endif

#ifdef	HAVE_OPENCV_CUDA
HRESULT Convert :: convertTo (	cv::cuda::GpuMat *pMatSrc, 
											cv::cuda::GpuMat *pMatDst, U32 fmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Global utility to convert data to a particular format.
	//
	//	PARAMETERS
	//		-	pMatSrc is the source data
	//		-	pMatDst will receive the converted data
	//		-	fmt is the new OpenCV format
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr = S_OK;
	cv::cuda::GpuMat	gpuCnv;

	// Cannot convert into the same array, use intermediate array

	// Convert to temporary location
	pMatSrc->convertTo ( gpuCnv, fmt );

	// Copy to destination
	gpuCnv.copyTo ( *pMatDst );

	return hr;
	}	// convertTo
#endif
/*
float Convert :: floatFromHalf ( S16 half )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert from a 16-bit half floating point number to full float
	//
	//	PARAMETERS
	//		-	half is the half floating point encoded in 2 byte integer
	//
	//	RETURN VALUE
	//		Full float value
	//
	////////////////////////////////////////////////////////////////////////
	static const FP32 magic = { (254 - 15) << 23 };
	static const FP32 was_infnan = { (127 + 16) << 23 };
	FP32 h;
	FP32 o;

	h.f = half;												// Original value
	o.u = (h.u & 0x7fff) << 13;						// exponent/mantissa bits
	o.f *= magic.f;										// exponent adjust
	if (o.f >= was_infnan.f)							// make sure Inf/NaN survive
		o.u |= 255 << 23;
	o.u |= (h.u & 0x8000) << 16;						// sign bit

	return o.f;
	}	// floatFromHalf
*/

FP32 Convert :: floatFromHalf ( FP16 h )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert from a 16-bit half floating point number to full float
	//
	//	PARAMETERS
	//		-	h is the half floating point encoded in 2 byte integer
	//
	//	RETURN VALUE
	//		Full float value
	//
	////////////////////////////////////////////////////////////////////////
	FP32 o = { 0 };

	// From ISPC ref code
	if (h.Exponent == 0 && h.Mantissa == 0) // (Signed) zero
		o.Sign = h.Sign;
	else
		{
		if (h.Exponent == 0) // Denormal (will convert to normalized)
			{
			// Adjust mantissa so it's normalized (and keep track of exp adjust)
			int e = -1;
			uint m = h.Mantissa;
			do
				{
				e++;
				m <<= 1;
				} while ((m & 0x400) == 0);

			o.Mantissa = (m & 0x3ff) << 13;
			o.Exponent = 127 - 15 - e;
			o.Sign = h.Sign;
			}
		else if (h.Exponent == 0x1f) // Inf/NaN
			{
			// NOTE: It's safe to treat both with the same code path by just truncating
			// lower Mantissa bits in NaNs (this is valid).
			o.Mantissa = h.Mantissa << 13;
			o.Exponent = 255;
			o.Sign = h.Sign;
			}
		else // Normalized number
			{
			o.Mantissa = h.Mantissa << 13;
			o.Exponent = 127 - 15 + h.Exponent; 
			o.Sign = h.Sign;
			}	// else
		}	// else

	return o;
	}	// floatFromHalf

HRESULT Convert :: FmtToCvFmt ( const WCHAR *pwFmt, U32 *pfmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert from a system image format string to OpenCV format.
	//
	//	PARAMETERS
	//		-	pwFmt is the format string (such as RGB24)
	//		-	pfmt will receive the CV_XXX format
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Mapping

	// Arrays
	if (!WCASECMP(pwFmt,L"F32"))
		*pfmt = CV_32FC1;
	else if (!WCASECMP(pwFmt,L"F64"))
		*pfmt = CV_64FC1;
	else if (!WCASECMP(pwFmt,L"U16"))
		*pfmt = CV_16UC1;
	else if (!WCASECMP(pwFmt,L"S16"))
		*pfmt = CV_16SC1;
	else if (!WCASECMP(pwFmt,L"U8"))
		*pfmt = CV_8UC1;
	else if (!WCASECMP(pwFmt,L"S8"))
		*pfmt = CV_8SC1;
	else if (!WCASECMP(pwFmt,L"F32F32"))
		*pfmt = CV_32FC2;
	else if (!WCASECMP(pwFmt,L"U8U8"))
		*pfmt = CV_8UC2;
	else if (!WCASECMP(pwFmt,L"S8S8"))
		*pfmt = CV_8SC2;

	// Images
	else if (!WCASECMP(pwFmt,L"B8G8R8"))
		*pfmt = CV_8UC3;
	else if (!WCASECMP(pwFmt,L"B8G8R8A8"))
		*pfmt = CV_8UC4;
	else if (!WCASECMP(pwFmt,L"B16G16R16"))
		*pfmt = CV_16UC3;
	else if (!WCASECMP(pwFmt,L"BGRf"))
		*pfmt = CV_32FC3;
	else
		hr = E_NOTIMPL;

	return hr;
	}	// FmtToCvFmt

HRESULT Convert :: fromHalfFloat ( cv::Mat *pmat, U32 nFrom, U32 nTo )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert from half-float format to specified nspace format.
	//
	//	PARAMETERS
	//		-	pmat contains and will receive the new image data
	//		-	nFrom is the source nSpace format (IMGFMT_XXX)
	//		-	nTo is the destination nSpace image format.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	void		*pvBits	= NULL;	
	U8			*pcBits	= NULL;	
	S16		*psBits	= NULL;	
	float		fr,fg,fb,fa;
	U8			ur,ug,ub,ua;
	FP16		half;
	cv::Mat	matDst;

	// Create destination image
	switch (nTo)
		{
		case IMGFMT_B8G8R8 :
			matDst.create ( pmat->rows, pmat->cols, CV_8UC3 );
			break;
		default :
			hr = E_NOTIMPL;
		}	// switch

	// Castings
	if (hr == S_OK)
		{
		// Conversion loop, slow
		for (int r = 0;r < pmat->rows;++r)
			{
			for (int c = 0;c < pmat->cols;++c)
				{
				// Source pixel
				fr = fb = fg = fa = 0.0f;
				switch ( nFrom )
					{
					case IMGFMT_RGBHF :
						{
						cv::Vec3s	v;
						v			= pmat->at<cv::Vec3s>(r,c);
						half.u	= v[0];
						fr			= floatFromHalf ( half ).f;
						half.u	= v[1];
						fg			= floatFromHalf ( half ).f;
						half.u	= v[2];
						fb			= floatFromHalf ( half ).f;
						}	// IMGFMT_RGBHF
						break;
					case IMGFMT_RGBAHF :
						{
						cv::Vec4s	v;
						v			= pmat->at<cv::Vec4s>(r,c);
						half.u	= v[0];
						fr			= floatFromHalf ( half ).f;
						half.u	= v[1];
						fg			= floatFromHalf ( half ).f;
						half.u	= v[2];
						fb			= floatFromHalf ( half ).f;
						half.u	= v[3];
						fa			= floatFromHalf ( half ).f;
						}	// IMGFMT_RGBAHF
						break;
					default :
						fr = fg = fb = 0.0f;
					}	// switch

				// NOTE: Cannot handle HDR for 8-bit formats, values are normalized 0-255;
				// Intensity is assumed to be [0.0...1.0] (LDR)
				fr = (MAX ( 0.0f, MIN ( 255.0f , 255.0f*fr ) ));
				fg = (MAX ( 0.0f, MIN ( 255.0f , 255.0f*fg ) ));
				fb = (MAX ( 0.0f, MIN ( 255.0f , 255.0f*fb ) ));
				fa = (MAX ( 0.0f, MIN ( 255.0f , 255.0f*fa ) ));
				ur = (U8) nearbyintf(fr);
				ug = (U8) nearbyintf(fg);
				ub = (U8) nearbyintf(fb);
				ua = (U8) nearbyintf(fa);

				// Destination format
				switch ( nTo )
					{
					case IMGFMT_B8G8R8 :
						{
						// Byte 1 = Blue
						cv::Vec3b	v(ub,ug,ur);
						matDst.at<cv::Vec3b>(r,c) = v;
						}	// IMGFMT_RbG8R8
						break;
					}	// switch
				}	// for (int c = 0...

			}	// for (int r = 0...
		}	// if

	// Replace original image
	CCLOK ( *pmat = matDst; )

	return hr;
	}	// fromHalfFloat

HRESULT Convert :: fromYuv ( cv::Mat *pmat, U32 nFrom, U32 nTo )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert from specialized YUV format to specified nspace format.
	//
	//	PARAMETERS
	//		-	pmat contains and will receive the new image data
	//		-	nFrom is the source nSpace format (IMGFMT_XXX)
	//		-	nTo is the destination nSpace image format.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr				= S_OK;
	U8			*pcSrcBits	= pmat->data;
	U16		*puSrcBits	= (U16 *) pmat->data;
	U32		srcStride	= (U32)(pmat->cols*pmat->elemSize());;
	U32		dstStride	= 0;
	U8			*pcDstBits	= NULL;
	U16		*puDstBits	= NULL;
	U16		uSrc[4];
	float 	b,g,r,cr,cb,y;
	cv::Mat	matDst;

	// Create destination image
	switch (nTo)
		{
		case IMGFMT_B16G16R16 :
			// Destination image
			matDst.create ( pmat->rows, pmat->cols, CV_16UC3 );

			// Setup
			pcDstBits 	= (U8 *) matDst.data;
			dstStride	= pmat->cols*(3*2);
			break;
		default :
			hr = E_NOTIMPL;
		}	// switch

	// Convert
//	lprintf ( LOG_DBG, L"pcSrcBits %p srcStride %d pcDstBits %p dstStride %d %dX%d\r\n",
//					pcSrcBits, srcStride, pcDstBits, dstStride, pmat->cols, pmat->rows );
	if (hr == S_OK)
		{
		// Conversion loop, slow
		for (int row = 0;row < pmat->rows;++row)
			{
			// Debug
//			if (row < 25)
//				lprintf ( LOG_DBG, L"row %d) 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\r\n",
//								row, 
//								pcSrcBits[0], pcSrcBits[1], pcSrcBits[2], pcSrcBits[3],
//								pcSrcBits[4], pcSrcBits[5], pcSrcBits[6], pcSrcBits[7] );

			// Columns
			puDstBits = (U16 *) pcDstBits;
			puSrcBits = (U16 *) pcSrcBits;
			for (int col = 0;col < pmat->cols;++col)
				{
				// Source pixel
				switch ( nFrom )
					{
					case IMGFMT_YUV422P16 :
						{
						// Raw values
						memcpy ( uSrc, puSrcBits, sizeof(uSrc) );

						// Format is YUYV so color channels are sampled on even pixels
						if (!(col & 0x1))
							{
							y	= uSrc[0]/65535.0f;
							cb	= uSrc[1]/65535.0f;
							cr = uSrc[3]/65535.0f;
							}	// if
						else
							{
							y 				= uSrc[2]/65535.0f;
							puSrcBits	+= 4;
							}	// else
						}	// IMGFMT_RGBHF
						break;
					default :
						y = cb = cr = 0;
					}	// switch

				// Use color information ?
				if (!bClrUse)
					{
					// "Zero" the color components
					cr = 0.5f;
					cb = 0.5f;
					}	// if

				// Apply formula for each color component
				r = y + 1.140f*(cr - 0.5f);
				g = y - 0.344f*(cr - 0.5f) - 0.714f*(cb - 0.5f);
				b = y + 1.773f*(cb - 0.5f);

				// Clamp output to valid ranges
				if 		(r < 0.0f) r = 0.0f;
				else if 	(r > 1.0f) r = 1.0f;
				if 		(g < 0.0f) g = 0.0f;
				else if 	(g > 1.0f) g = 1.0f;
				if 		(b < 0.0f) b = 0.0f;
				else if 	(b > 1.0f) b = 1.0f;

				// Destination format
				switch ( nTo )
					{
					case IMGFMT_B16G16R16 :
						{
						// Debug
//						r = g = b = y;

						// Debug
//						if (row < 25 && col < 16)
//							lprintf ( LOG_DBG, L"%d,%d) y %g cr %g cb %g r %g/%g g %g/%g b %g/%g\r\n",
//								col, row, y, cr, cb, r, (r*(1<<16)), g, (g*(1<<16)), b, (b*(1<<16)) );

						// Store pixel
						*puDstBits++ = (U16)(b*((1<<16)-1));
						*puDstBits++ = (U16)(g*((1<<16)-1));
						*puDstBits++ = (U16)(r*((1<<16)-1));

						// Debug - Gradient
//						*puDstBits++ = col;
//						*puDstBits++ = col;
//						*puDstBits++ = col;
						}	// IMGFMT_RbG8R8
						break;
					}	// switch
				}	// for (int col = 0...

			// Stride ptrs.
			pcSrcBits += srcStride;
			pcDstBits += dstStride;
			}	// for (int row = 0...
		}	// if

	// Transfer result to original matrix
	CCLOK ( matDst.copyTo(*pmat); )

	return hr;
	}	// fromYuv

HRESULT Convert :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Format"), vL ) == S_OK)
			onReceive ( prFormat, vL );
		if (pnDesc->load ( adtString(L"UseColor"), vL ) == S_OK)
			onReceive ( prUseColor, vL );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Convert :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Convert formats
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		U32			fmt		= 0;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Convert 'to' specified format. Add formats as needed.
//		CCLOK ( image_to_debug ( pMat, L"Convert", L"c:/temp/convert1.png" ); )
		CCLTRY ( FmtToCvFmt ( strTo, &fmt ) );
		CCLTRY ( convertTo ( pMat, pMat, fmt ) );
//		CCLOK ( image_to_debug ( pMat, L"Convert", L"c:/temp/convert2.png" ); )

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// Color space conversion
	else if (_RCP(Color))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		bool			bFromG	= false;
		bool			bToG		= false;
		bool			bAutoCnv	= true;
		S32			spc		= -1;
		adtString	strFmt;
		adtValue		vL;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		try
			{
			// Format of image
			CCLTRY ( image_format ( pMat, strFmt ) );

			// Use override format is previously converted
			if (hr == S_OK && pImgUse->load ( adtString(L"ColorFormat"), vL ) == S_OK)
				hr = adtValue::toString(vL,strFmt);

			// From/To grayscale
			CCLOK ( bFromG =	(	!WCASECMP(strFmt,L"U8")	||
										!WCASECMP(strFmt,L"S8")	||
										!WCASECMP(strFmt,L"U16")	||
										!WCASECMP(strFmt,L"S16") ); )
			CCLOK ( bToG	=	(	!WCASECMP(strTo,L"U8")	||
										!WCASECMP(strTo,L"S8")	||
										!WCASECMP(strTo,L"U16") ||
										!WCASECMP(strTo,L"S16") ); )

			// Conversion type.  Add as needed.
//			lprintf ( LOG_DBG, L"From %s To %s (%d,%d)\r\n",
//							(LPCWSTR) strFmt, (LPCWSTR) strTo, bFromG, bToG );

			// Type different ?
			if (hr == S_OK && WCASECMP(strFmt,strTo))
				{
				// From grayscale
				if (bFromG)
					{
					if (!WCASECMP(strTo,L"R8G8B8"))
						spc = cv::COLOR_GRAY2RGB;
					else if (!WCASECMP(strTo,L"B8G8R8") ||
								!WCASECMP(strTo,L"B16G16R16"))
						spc = cv::COLOR_GRAY2BGR;
					else if (!WCASECMP(strTo,L"R8G8B8A8"))
						spc = cv::COLOR_GRAY2RGBA;
					else if (!WCASECMP(strTo,L"B8G8R8A8"))
						spc = cv::COLOR_GRAY2BGRA;
					else
						hr = E_NOTIMPL;
					}	//if

				// From color
				else if (!WCASECMP(strFmt,L"R8G8B8") )
					{
					if (bToG)
						spc = cv::COLOR_RGB2GRAY;
					else if (!WCASECMP(strTo,L"B8G8R8") )
						spc = cv::COLOR_RGB2BGR;
					else if (!WCASECMP(strTo,L"R8G8B8A8"))
						spc = cv::COLOR_RGB2RGBA;
					else if (!WCASECMP(strTo,L"B8G8R8A8"))
						spc = cv::COLOR_RGB2BGRA;

					// XYZ color space
					else if (!WCASECMP(strTo,L"XYZ"))
						spc = cv::COLOR_RGB2XYZ;

					else
						hr = E_NOTIMPL;
					}	// if
				else if (!WCASECMP(strFmt,L"B8G8R8") )
					{
					if (bToG)
						spc = cv::COLOR_BGR2GRAY;
					else if (!WCASECMP(strTo,L"R8G8B8") )
						spc = cv::COLOR_BGR2RGB;
					else if (!WCASECMP(strTo,L"R8G8B8A8"))
						spc = cv::COLOR_BGR2RGBA;
					else if (!WCASECMP(strTo,L"B8G8R8A8"))
						spc = cv::COLOR_BGR2BGRA;

					// XYZ color space
					else if (!WCASECMP(strTo,L"XYZ"))
						spc = cv::COLOR_BGR2XYZ;

					else
						hr = E_NOTIMPL;
					}	// if

				else if (!WCASECMP(strFmt,L"R8G8B8A8") )
					{
					if (bToG)
						spc = cv::COLOR_RGBA2GRAY;
					else if (!WCASECMP(strTo,L"B8G8R8") )
						spc = cv::COLOR_RGBA2BGR;
					else if (!WCASECMP(strTo,L"R8G8B8"))
						spc = cv::COLOR_RGBA2RGB;
					else if (!WCASECMP(strTo,L"B8G8R8A8"))
						spc = cv::COLOR_RGBA2BGRA;
					else
						hr = E_NOTIMPL;
					}	// if
				else if (!WCASECMP(strFmt,L"B8G8R8A8") )
					{
					if (bToG)
						spc = cv::COLOR_BGRA2GRAY;
					else if (!WCASECMP(strTo,L"R8G8B8") )
						spc = cv::COLOR_BGRA2RGB;
					else if (!WCASECMP(strTo,L"R8G8B8A8"))
						spc = cv::COLOR_BGRA2RGBA;
					else if (!WCASECMP(strTo,L"B8G8R8"))
						spc = cv::COLOR_BGRA2BGR;
					else
						hr = E_NOTIMPL;
					}	// if
				else if (!WCASECMP(strFmt,L"B16G16R16") )
					{
					if (bToG)
						spc = cv::COLOR_BGRA2GRAY;
					else if (!WCASECMP(strTo,L"XYZ"))
						spc = cv::COLOR_BGR2XYZ;
					else
						hr = E_NOTIMPL;
					}	// if

				// Floating point
				else if (!WCASECMP(strFmt,L"BGRf") )
					{
					if (bToG)
						spc = cv::COLOR_BGR2GRAY;
					else if (!WCASECMP(strTo,L"RGBAf"))
						spc = cv::COLOR_BGR2RGBA;
					else if (!WCASECMP(strTo,L"BGRAf"))
						spc = cv::COLOR_BGR2BGRA;
					else if (!WCASECMP(strTo,L"HSV"))
						spc = cv::COLOR_BGR2HSV;
					else if (!WCASECMP(strTo,L"RGBf"))
						spc = cv::COLOR_BGR2RGB;
					else
						hr = E_NOTIMPL;
					}	// if

				else if (!WCASECMP(strFmt,L"RGBf") )
					{
					if (!WCASECMP(strTo,L"BGRf"))
						spc = cv::COLOR_RGB2BGR;
					else
						hr = E_NOTIMPL;
					}	// else if

				// Half floating point
				else if (!WCASECMP(strFmt,L"RGBAhf") )
					{
					// NOTE: No OpenCV support for half floats,
					// convert manually.
					bAutoCnv = false;

					// Must be local, support for read/modify/write ?
					if (!pMat->isMat())
						hr = E_NOTIMPL;
					else if (!WCASECMP(strTo,L"B8G8R8"))
						hr = fromHalfFloat ( pMat->mat, IMGFMT_RGBAHF, IMGFMT_B8G8R8 );
					else
						hr = E_NOTIMPL;
					}	// else if

				// YUV
				else if (!WCASECMP(strFmt,L"YUV422P16") )
					{
					// OpenCV does not support 32bpp YUV so use own converter
					bAutoCnv	= false;

					// Basically 32bpp YUY2 format (Y0Cb0Y1Cr0) where each component is 16bpp
					if (!WCASECMP(strTo,L"B16G16R16"))
						hr = fromYuv ( pMat->mat, IMGFMT_YUV422P16, IMGFMT_B16G16R16 );
					else
						hr = E_NOTIMPL;
					}	// else if

				// HSV
				else if (!WCASECMP(strFmt,L"HSV") )
					{
					if (!WCASECMP(strTo,L"BGR"))
						spc = cv::COLOR_HSV2BGR;
					else if (!WCASECMP(strTo,L"BGRf"))
						spc = cv::COLOR_HSV2BGR;
					else
						hr = E_NOTIMPL;
					}	// else if

				// XYZ
				else if (!WCASECMP(strFmt,L"XYZ") )
					{
					if (	!WCASECMP(strTo,L"B8G8R8") ||
							!WCASECMP(strTo,L"B16G16R16") )
						spc = cv::COLOR_XYZ2BGR;
					else if (!WCASECMP(strTo,L"R8G8B8"))
						spc = cv::COLOR_XYZ2RGB;
					else
						hr = E_NOTIMPL;
					}	// else if

				// Bayer
				else if (!WCASENCMP(strFmt,L"Bayer",5))
					{
					//
					// Apprently OpenCV uses a different naming convention from
					// everyone else.  "Standard" BayerGRBG in Open CV is GB.
					// https://docs.opencv.org/3.4/de/d25/imgproc_color_conversions.html
					// and
					// https://www.mathworks.com/help/images/ref/demosaic.html
					//
					if (bToG)
						spc = (!WCASECMP(&strFmt[5],L"GRBG")) ? cv::COLOR_BayerGB2GRAY :
								(!WCASECMP(&strFmt[5],L"GBRG")) ? cv::COLOR_BayerGR2GRAY :
								(!WCASECMP(&strFmt[5],L"BGGR")) ? cv::COLOR_BayerRG2GRAY :
								(!WCASECMP(&strFmt[5],L"RGGB")) ? cv::COLOR_BayerBG2GRAY : -1;
					else if (!WCASECMP(strTo,L"B8G8R8") || !WCASECMP(strTo,L"B16G16R16"))
						spc = (!WCASECMP(&strFmt[5],L"GRBG")) ? cv::COLOR_BayerGB2BGR :
								(!WCASECMP(&strFmt[5],L"GBRG")) ? cv::COLOR_BayerGR2BGR :
								(!WCASECMP(&strFmt[5],L"BGGR")) ? cv::COLOR_BayerRG2BGR :
								(!WCASECMP(&strFmt[5],L"RGGB")) ? cv::COLOR_BayerBG2BGR : -1;
					else
						hr = E_NOTIMPL;

					// Valid conversion ?
					CCLTRYE ( spc != -1, E_NOTIMPL );
					}	// else if

				// Valid conversion ?
				CCLTRYE ( (spc != -1 || !bAutoCnv), E_NOTIMPL );

				// Perform color space conversion
				if (hr == S_OK)
					{
					if (pMat->isMat() && bAutoCnv)
						cv::cvtColor( *(pMat->mat), *(pMat->mat), spc );
					#ifdef	HAVE_OPENCV_UMAT
					else if (pMat->isUMat() && bAutoCnv)
						cv::cvtColor( *(pMat->umat), *(pMat->umat), spc );
					#endif
					#ifdef	HAVE_OPENCV_CUDA
					else if (pMat->isGPU() && bAutoCnv)
						cv::cuda::cvtColor ( *(pMat->gpumat), *(pMat->gpumat), spc );
					#endif

					// OpenCV does not internally distinguish between BGR and RGB.  This means
					// we have to store a flag indicating the real color format in case it gets
					// downloaded into memory
					if (hr == S_OK && !bToG)
						{
						// Current color format
						hr = pImgUse->store ( adtString(L"ColorFormat"), strTo );
						}	// if
					}	// if
				}	// if

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_WARN, L"Unable to convert from '%s' to '%s' 0x%x",
								(LPCWSTR) strFmt, (LPCWSTR) strTo, hr );
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = E_UNEXPECTED;
			}	// catch

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Format))
		hr = adtValue::toString ( v, strTo );
	else if (_RCP(UseColor))
		bClrUse = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
