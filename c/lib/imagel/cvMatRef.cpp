////////////////////////////////////////////////////////////////////////
//
//									CVMATREF.CPP
//
//				Implementation of the OpenCV matrix container
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"

cvMatRef :: cvMatRef ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"this %p\r\n", this );

	mat		= NULL;
	#ifdef	HAVE_OPENCV_CUDA
	gpumat	= NULL;
	#endif
	#ifdef	HAVE_OPENCV_UMAT
	umat		= NULL;
	#endif

	// So creators do not have to do the initial AddRef
	AddRef();
	}	// cvMatRef

cvMatRef :: cvMatRef ( cv::Mat &_mat ) : cvMatRef()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Copy constructor for the object
	//
	//	PARMAETERS
	//		-	_mat is the initial matrix to use
	//
	////////////////////////////////////////////////////////////////////////

	// Start with given matrix
	mat = new cv::Mat ( _mat );
	}	// cvMatRef

#ifdef	HAVE_OPENCV_UMAT
cvMatRef :: cvMatRef ( cv::UMat &_mat ) : cvMatRef()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Copy constructor for the object
	//
	//	PARMAETERS
	//		-	_mat is the initial matrix to use
	//
	////////////////////////////////////////////////////////////////////////

	// Start with given matrix
	umat = new cv::UMat ( _mat );
	}	// cvMatRef
#endif

#ifdef	HAVE_OPENCV_CUDA
cvMatRef :: cvMatRef ( cv::cuda::GpuMat &_mat ) : cvMatRef()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Copy constructor for the object
	//
	//	PARMAETERS
	//		-	_mat is the initial matrix to use
	//
	////////////////////////////////////////////////////////////////////////

	// Start with given matrix
	gpumat = new cv::cuda::GpuMat ( _mat );
	}	// cvMatRef
#endif

cvMatRef :: ~cvMatRef ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////
//	lprintf ( LOG_DBG, L"this %p mat %p data %p %dx%d size %d\r\n", 
//					this, mat, (mat != NULL) ? mat->data : NULL,
//					(mat != NULL) ? mat->cols : 0,
//					(mat != NULL) ? mat->rows : 0, 
//					(mat != NULL) ? ((U32)(mat->total()*mat->elemSize())) : 0 );
	if (mat != NULL)
		{
		delete mat;
		mat = NULL;
		}	 // if
	#ifdef	HAVE_OPENCV_CUDA
	if (gpumat != NULL)
		{
		delete gpumat;
		gpumat = NULL;
		}	 // if
	#endif
	#ifdef	HAVE_OPENCV_UMAT
	if (umat != NULL)
		{
		delete umat;
		umat = NULL;
		}	 // if
	#endif
	}	// ~cvMatRef

