////////////////////////////////////////////////////////////////////////
//
//									Unary.CPP
//
//				Implementation of the unary operation image node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Unary :: Unary ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg		= NULL;
	iOp		= MATHOP_NOP;
	bReduce	= false;
	iIdx		= 0;
	}	// Unary

HRESULT Unary :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (	pnDesc->load ( adtStringSt(L"Op"), vL ) == S_OK	&& 
				adtValue::type(vL) == VTYPE_STR						&&
				vL.pstr != NULL )
			imageOp ( vL.pstr, &iOp );
		if (	pnDesc->load ( adtStringSt(L"Reduce"), vL ) == S_OK )
			bReduce = vL;
		if (	pnDesc->load ( adtStringSt(L"Index"), vL ) == S_OK )
			iIdx = vL;
		}	// if

	// Detach
	else
		{
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Unary :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		IDictionary	*pImgO	= NULL;
		cvMatRef		*pMat		= NULL;
		cvMatRef		*pMatO	= NULL;

		// State check
		CCLTRYE ( iOp >= MATHOP_ADD, ERROR_INVALID_STATE );

		// Obtain image refence
		CCLTRY	( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Incoming value will receive result.
		// TODO: Seperate receptor for this ?
		CCLTRY	( Prepare::extract ( NULL, v, &pImgO, &pMatO ));

		// Default result is the output image
		CCLTRY ( adtValue::copy ( adtIUnknown(pImgO), vRes ) );

		// Apply
		try
			{
			// Apply operation
			if (hr == S_OK)
				{
				switch (iOp)
					{
					case MATHOP_LOG :
						{
						if (pMat->isMat())
							cv::log ( *(pMat->mat), *(pMatO->mat) );
						#ifdef	HAVE_OPENCV_UMAT
						else if (pMat->isUMat())
							cv::log ( *(pMat->umat), *(pMatO->umat) );
						#endif
						#ifdef	HAVE_OPENCV_CUDA
						else if (pMat->isGPU())
							cv::cuda::log ( *(pMat->gpumat), *(pMatO->gpumat) );
						#endif
						}	// MATHOP_LOG
						break;
					case MATHOP_SUM :
						{
						// If reducing, then the operator will apply to
						// the given dimension index (0 = reduce to single row, 1 = single column)
						cv::Scalar	sclr;
						if (pMat->isMat())
							{
							// Reduction ?
							if (bReduce)
								cv::reduce ( *(pMat->mat), *(pMatO->mat), iIdx, cv::REDUCE_SUM );
							else
								sclr = cv::sum ( *(pMat->mat) );
							}	// if
						#ifdef	HAVE_OPENCV_UMAT
						else if (pMat->isUMat())
							{
							if (bReduce)
								cv::reduce ( *(pMat->umat), *(pMatO->umat), iIdx, cv::REDUCE_SUM );
							else
								sclr = cv::sum ( *(pMat->umat) );
							}	// else if
						#endif
						#ifdef	HAVE_OPENCV_CUDA
						else if (pMat->isGPU())
							{
							if (bReduce)
								cv::cuda::reduce ( *(pMat->gpumat), *(pMatO->gpumat), iIdx, cv::REDUCE_SUM );
							else
								sclr = cv::cuda::sum ( *(pMat->gpumat) );
							}	// else if
						#endif

						// How to handle results for multi-channel images,
						// dictionary ?
						if (!bReduce)
							{
							CCLTRYE ( (pMat->channels() == 1), E_NOTIMPL );
							CCLTRY  ( adtValue::copy ( adtFloat(sclr[0]), vRes ) );
							}	// if

						}	// MATHOP_SUM
						break;

					case MATHOP_NOT :
						if (pMat->isMat())
							cv::bitwise_not ( *(pMat->mat), *(pMatO->mat) );
						#ifdef	HAVE_OPENCV_UMAT
						else if (pMat->isUMat())
							cv::bitwise_not ( *(pMat->umat), *(pMatO->umat) );
						#endif
						#ifdef	HAVE_OPENCV_CUDA
						else if (pMatL->isGPU())
							cv::cuda::bitwise_not ( *(pMat->gpumat), *(pMatO->gpumat) );
						#endif
						break;

					case MATHOP_EXP :
						if (pMat->isMat())
							cv::exp ( *(pMat->mat), *(pMatO->mat) );
						#ifdef	HAVE_OPENCV_UMAT
						else if (pMat->isUMat())
							cv::exp ( *(pMat->umat), *(pMatO->umat) );
						#endif
						#ifdef	HAVE_OPENCV_CUDA
						else if (pMat->isGPU())
							cv::cuda::exp ( *(pMat->gpumat), *(pMatO->gpumat) );
						#endif
						break;

					// Cartesian to polar
					case MATHOP_C2P :
						{
						// This node assumes X and Y are stored in a 2 channel image and
						// the result will be a 2 channel image (mag,phase)
						CCLTRYE ( pMat->channels() == 2, ERROR_INVALID_STATE );
						if (hr == S_OK && pMat->isMat())
							{
							std::vector<cv::Mat> xy(2);
							std::vector<cv::Mat> magp(2);

							// Split the two channels to they can be fed into routine
							cv::split ( *(pMat->mat), xy );

							// Perform conversion
							cv::cartToPolar ( xy[0], xy[1], magp[0], magp[01] );

							// Merge channels together into result
							cv::merge ( magp, *(pMatO->mat) );
							}	// if
						else if (hr == S_OK && pMat->isUMat())
							{
							std::vector<cv::UMat> xy(2);
							std::vector<cv::UMat> magp(2);

							// Split the two channels to they can be fed into routine
							cv::split ( *(pMat->umat), xy );

							// Perform conversion
							cv::cartToPolar ( xy[0], xy[1], magp[0], magp[01] );

							// Merge channels together into result
							cv::merge ( magp, *(pMatO->umat) );
							}	// if

						}	// MATHOP_C2P
						break;

					// Power to cartesian
					case MATHOP_P2C :
						// This node assumes X and Y are stored in a 2 channel image and
						// the result will be a 2 channel image (mag,phase)
						CCLTRYE ( pMat->channels() == 2, ERROR_INVALID_STATE );
						if (pMat->isMat())
							{
							std::vector<cv::Mat> xy(2);
							std::vector<cv::Mat> magp(2);

							// Split the two channels to they can be fed into routine
							cv::split ( *(pMat->mat), magp );

							// Perform conversion
							cv::polarToCart ( magp[0], magp[1], xy[0], xy[1]  );

							// Merge channels together into result
							cv::merge ( xy, *(pMatO->mat) );
							}	// if
						break;

					// Not implemented
					default :
						hr = E_NOTIMPL;
					}	// switch

				}	// if

			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = E_UNEXPECTED;
			}	// catch

		// Debug
		if (hr != S_OK)
			lprintf ( LOG_ERR, L"%s:Fire:Error:hr 0x%x:iOp %d:Img %p:ImgO %p\r\n", 
							(LPCWSTR)strnName, hr, iOp, pImg, pImgO );

		// Clean up
		_RELEASE(pMatO);
		_RELEASE(pImgO);
		_RELEASE(pMat);
		_RELEASE(pImgUse);

		// Result
		if (hr == S_OK)
			_EMT(Fire,vRes);
		else
			_EMT(Error,adtInt(hr) );

		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

////////

HRESULT imageOp ( const WCHAR *wOp, int *piOp )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert a string representation of an operation into its
	//			definition.
	//
	//	PARAMETERS
	//		-	wOp is the string version ("Add", etc)
	//		-	piOp will receive the integer value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Setup
	(*piOp) = MATHOP_NOP;

	// Image specific operations
	if (!WCASENCMP(wOp,L"Sum",3))
		(*piOp) = MATHOP_SUM;

	// Copy with mask
	else if (!WCASENCMP(wOp,L"Copy",4))
		(*piOp) = MATHOP_COPY;

	// Per pixel transform
	else if (!WCASENCMP(wOp,L"Trans",5))
		(*piOp) = MATHOP_XFORM;

	// Default math op
	else
		hr = mathOp(wOp,piOp);

	return hr;
	}	// imageOp
