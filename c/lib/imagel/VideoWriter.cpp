////////////////////////////////////////////////////////////////////////
//
//									VideoWriter.CPP
//
//				Implementation of the video writer node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

VideoWriter :: VideoWriter ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pWr	= NULL;
	iFps	= 10;
	iW		= 320;
	iH		= 240;
	}	// VideoWriter

HRESULT VideoWriter :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"FPS"), vL ) == S_OK)
			iFps = vL;
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			iW = vL;
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			iH = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		onReceive(prClose,adtInt());
		}	// else

	return hr;
	}	// onAttach

HRESULT VideoWriter :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Open for writing
	if (_RCP(Open))
		{
		char	*pc = NULL;

		// State check
		CCLTRYE ( strLoc.length() > 0, ERROR_INVALID_STATE );
		CCLTRYE ( pWr == NULL, ERROR_INVALID_STATE );

		// Create a writer
		CCLTRYE ( (pWr = new cv::VideoWriter()) != NULL, E_OUTOFMEMORY );

		// Open to specified file
		CCLTRY  ( strLoc.toAscii(&pc) );
		CCLTRYE ( pWr->open ( pc, cv::VideoWriter::fourcc('M','P','4','V'),
											30, cv::Size(1920,1080), true ) == true,
						E_UNEXPECTED );

		// Result
		if (hr == S_OK)
			_EMT(Open,strLoc);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pc);
		}	// if

	// Close file
	else if (_RCP(Close))
		{
		// Deleting the object closes the file
		if (pWr != NULL)
			{
			delete pWr;
			pWr = NULL;
			}	// if
		}	// else if

	// Add frame to video
	else if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;

		// State check
		CCLTRYE ( pWr != NULL, ERROR_INVALID_STATE );

		// Obtain image refence
		CCLTRY ( Prepare::extract ( NULL, v, &pImgUse, &pMat ) );

		// Execute 
		if (hr == S_OK && pMat->isMat())
			{
			// Add frame to video
			pWr->write(*(pMat->mat));
			}	// if

		}	// else if

	// State
	else if (_RCP(Location))
		hr = adtValue::toString(v,strLoc);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

