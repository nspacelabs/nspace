////////////////////////////////////////////////////////////////////////
//
//										IMAGEL.H
//
//							Image processing library
//
////////////////////////////////////////////////////////////////////////

#ifndef	IMAGEL_H
#define	IMAGEL_H

// System includes
#include "../../lib/nspcl/nspcl.h"

///////////
// Objects
///////////

// Integer version of image formats
#define	IMGFMT_INV				-1
#define	IMGFMT_U8				0
#define	IMGFMT_S8				1
#define	IMGFMT_U16				2
#define	IMGFMT_S16				3
#define	IMGFMT_F32				4
#define	IMGFMT_F64				5
#define	IMGFMT_F32F32			6
#define	IMGFMT_U8U8				7
#define	IMGFMT_S8S8				8
#define	IMGFMT_R8G8B8			10
#define	IMGFMT_B8G8R8			11
#define	IMGFMT_A8R8G8B8		12
#define	IMGFMT_A8B8G8R8		13
#define	IMGFMT_B8G8R8A8		14
#define	IMGFMT_R8G8B8A8		15
#define	IMGFMT_RGBAF			16
#define	IMGFMT_RGBHF			17
#define	IMGFMT_RGBAHF			18

#define	IMGFMT_B16G16R16		41

#define	IMGFMT_YUV422P16		81

//
// Class - ImageDct.  Convience class for dealing with images stored
//		in a dictionary.
//
// NOTE: Implemented in header to avoid having to link to library
//

class ImageDct
	{
	public :
	ImageDct ( void )
		{
		pDct		= NULL;
		pBits		= NULL;
		pvBits	= NULL;
		iW			= 0;
		iH			= 0;
		iFmt		= 0;	
		iBpp		= 0;
		iCh		= 0;
		}	// ImageDct
	virtual ~ImageDct ( void )
		{
		_RELEASE(pBits);
		_RELEASE(pDct);
		}	// ~ImageDct

	// Utilities
	HRESULT	lock ( IUnknown *punkDct )
		{
		HRESULT			hr		= S_OK;
		adtIUnknown		unkV(punkDct);
		adtValue			vL;
		adtString		strFmt;

		// Container
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDct) );

		// Parameters
		CCLTRY ( pDct->load ( adtString(L"Width"), vL ) );
		CCLOK  ( iW = adtInt(vL); )
		CCLTRY ( pDct->load ( adtString(L"Height"), vL ) );
		CCLOK  ( iH = adtInt(vL); )
		CCLTRY ( pDct->load ( adtString(L"Format"), vL ) );
		CCLTRY ( adtValue::toString ( vL, strFmt ) );

		// Map string format to integer for conveinance
		if (hr == S_OK)
			{
			if (!WCASECMP(strFmt,L"U8"))
				{
				iFmt	= IMGFMT_U8;
				iBpp	= 8;
				iCh	= 1;
				}	// if
			else if (!WCASECMP(strFmt,L"U8U8"))
				{
				iFmt	= IMGFMT_U8U8;
				iBpp	= 8;
				iCh	= 2;
				}	// if
			else if (!WCASECMP(strFmt,L"S8S8"))
				{
				iFmt	= IMGFMT_S8S8;
				iBpp	= 8;
				iCh	= 2;
				}	// if
			else if (!WCASECMP(strFmt,L"S8"))
				{
				iFmt	= IMGFMT_S8;
				iBpp	= 8;
				iCh	= 1;
				}	// if
			else if (!WCASECMP(strFmt,L"U16"))
				{
				iFmt	= IMGFMT_U16;
				iBpp	= 16;
				iCh	= 1;
				}	// if
			else if (!WCASECMP(strFmt,L"S16"))
				{
				iFmt	= IMGFMT_S16;
				iBpp	= 16;
				iCh	= 1;
				}	// if
			else if (!WCASECMP(strFmt,L"F32"))
				{
				iFmt	= IMGFMT_F32;
				iBpp	= 32;
				iCh	= 1;
				}	// if
			else if (!WCASECMP(strFmt,L"F32F32"))
				{
				iFmt	= IMGFMT_F32F32;
				iBpp	= 32;
				iCh	= 2;
				}	// if
			else if (!WCASECMP(strFmt,L"F64"))
				{
				iFmt	= IMGFMT_F64;
				iBpp	= 64;
				iCh	= 1;
				}	// if
			else if (!WCASECMP(strFmt,L"R8G8B8"))
				{
				iFmt	= IMGFMT_R8G8B8;
				iBpp	= 24;
				iCh	= 3;
				}	// if
			else if (!WCASECMP(strFmt,L"B8G8R8"))
				{
				iFmt	= IMGFMT_B8G8R8;
				iBpp	= 24;
				iCh	= 3;
				}	// if
			else if (!WCASECMP(strFmt,L"A8R8G8B8"))
				{
				iFmt	= IMGFMT_A8R8G8B8;
				iBpp	= 32;
				iCh	= 4;
				}	// if
			else if (!WCASECMP(strFmt,L"ABGR32"))
				{
				iFmt	= IMGFMT_A8B8G8R8;
				iBpp	= 32;
				iCh	= 4;
				}	// if
			else if (!WCASECMP(strFmt,L"B8G8R8A8"))
				{
				iFmt	= IMGFMT_B8G8R8A8;
				iBpp	= 32;
				iCh	= 4;
				}	// if
			else if (!WCASECMP(strFmt,L"R8G8B8A8"))
				{
				iFmt	= IMGFMT_R8G8B8A8;
				iBpp	= 32;
				iCh	= 4;
				}	// if
			else if (!WCASECMP(strFmt,L"RGBAf"))
				{
				iFmt	= IMGFMT_RGBAF;
				iBpp	= 32;
				iCh	= 4;
				}	// if
			else if (!WCASECMP(strFmt,L"B16G16R16"))
				{
				iFmt	= IMGFMT_B16G16R16;
				iBpp	= 48;
				iCh	= 3;
				}	// if
			else
				iFmt = IMGFMT_INV;
			}	// if

		// Get bits and lock
		CCLTRY ( pDct->load ( adtString(L"Bits"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IMemoryMapped,&pBits) );
		CCLTRY ( pBits->getInfo ( &pvBits, NULL ) );

		return hr;
		}	// lock

	float		getFloat	( S32 x, S32 y )
		{
		float		fRet	= 0;

		// State check
		if (pDct == NULL || pvBits == NULL)
			return 0;

		// Based on format
		switch (iFmt)
			{
			case	IMGFMT_U8		:
				fRet = *(((U8 *)pvBits) + (y*iW + x));
				break;
			case	IMGFMT_S8		:
				fRet = *(((S8 *)pvBits) + (y*iW + x));
				break;
			case	IMGFMT_U16	:
				fRet = *(((U16 *)pvBits) + (y*iW + x));
				break;
			case	IMGFMT_S16	:
				fRet = *(((S16 *)pvBits) + (y*iW + x));
				break;
			case	IMGFMT_F32	:
				fRet = (*( ((float *)pvBits) + (y*iW + x) ) );
				break;
			case	IMGFMT_F64	:
				fRet = (float)(*( ((double *)pvBits) + (y*iW + x) ) );
				break;

			// For multi-channel data, values are interleaved so 
			// the 'y' coordinate is efftively the channel number offset.
			case	IMGFMT_F32F32	:
				fRet = (*( ((float *)pvBits) + (x*iCh + y) ) );
				break;
			case	IMGFMT_U8U8	:
				fRet = (*( ((U8 *)pvBits) + (x*iCh + y) ) );
				break;
			case	IMGFMT_S8S8	:
				fRet = (*( ((S8 *)pvBits) + (x*iCh + y) ) );
				break;
			}	// switch

		return fRet;
		}
	S32		getInt	( S32 x, S32 y ) { return (S32)getFloat(x,y); }

	// Run-time data
	IDictionary		*pDct;								// Image dictionary
	S32				iW,iH,iFmt,iCh,iBpp;				// Size information
	IMemoryMapped	*pBits;								// Memory block
	void				*pvBits;								// Ptr. to memory
	};

//
// Class - libJpeg.  Object for JPEG images.
//

class libJpeg 
	{
	public :
	libJpeg ( void );										// Constructor
	virtual ~libJpeg ( void );							// Destructor

	// Run-time data
	adtInt	iQ;											// Quality setting

	// Utilities
	HRESULT	construct	( void );					// Construct object
	HRESULT	compress		( IDictionary * );
	HRESULT	compress		( U32, U32, U32, U32, U32, void *,
									IMemoryMapped * );
	HRESULT	decompress	( IDictionary * );
	HRESULT	decompress	( IMemoryMapped *, IMemoryMapped *,
									U32 *, U32 *, U32 *, U32 * );

	private :

	};

//
// Class - libPng.  Object for PNG images.
//

class libPng 
	{
	public :
	libPng ( void );										// Constructor
	virtual ~libPng ( void );							// Destructor

	// Run-time data
	HRESULT			hr_int;								// Parameters
	U8					*pcBitsSrc;							// Decompression bits
	U32				idxSrc,lenSrc;						// Decompression bits

	// Utilities
	HRESULT	construct	( void );					// Construct object
//	HRESULT	compress		( IDictionary * );
//	HRESULT	compress		( U32, U32, U32, U32, IMemoryMapped *,
//									IMemoryMapped * );
	HRESULT	decompress	( IDictionary * );
	HRESULT	decompress	( IMemoryMapped *, IMemoryMapped *,
									U32 *, U32 *, U32 *, U32 * );

	private :

	};

#endif

