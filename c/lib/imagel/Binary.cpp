////////////////////////////////////////////////////////////////////////
//
//									BINARY.CPP
//
//				Implementation of the binary operation image node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Binary :: Binary ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	iOp = MATHOP_ADD;
	adtValue::clear(vL);
	adtValue::clear(vR);
	pSclrR = NULL;
	fAlpha	= 0.0f;
	fBeta		= 0.0f;
	fGamma	= 0.0f;
	bMat		= false;
	}	// Binary

HRESULT Binary :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		pnDesc->load ( adtString(L"Left"), vL );
		if (pnDesc->load ( adtString(L"Right"), vL ) == S_OK)
			onReceive(prRight,vL);
		if (pnDesc->load ( adtString(L"MatrixOp"), vL ) == S_OK)
			bMat = vL;
		if (	pnDesc->load ( adtStringSt(L"Op"), vL ) == S_OK	&& 
				adtValue::type(vL) == VTYPE_STR						&&
				vL.pstr != NULL )
			imageOp ( vL.pstr, &iOp );

		// Weighted operations
		if (	pnDesc->load ( adtStringSt(L"Alpha"), vL ) == S_OK )
				fAlpha = vL;
		if (	pnDesc->load ( adtStringSt(L"Beta"), vL ) == S_OK )
				fBeta = vL;
		if (	pnDesc->load ( adtStringSt(L"Gamma"), vL ) == S_OK )
				fGamma = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		if (pSclrR != NULL)
			{
			delete pSclrR;
			pSclrR = NULL;
			}	// if
		}	// else

	return hr;
	}	// onAttach

HRESULT Binary :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		// State check
		CCLTRYE ( adtValue::empty(vL) == false, ERROR_INVALID_STATE );
		CCLTRYE ( adtValue::empty(vR) == false, ERROR_INVALID_STATE );
		CCLTRYE ( iOp >= MATHOP_ADD, ERROR_INVALID_STATE );

		// Incoming value will receive result.
		// TODO: Seperate receptor for this ?
		CCLTRYE ( adtValue::type(v) == VTYPE_UNK, ERROR_INVALID_STATE );

		// Images for left and right or images for left and scalar for right
		if (	hr == S_OK && 
				adtValue::type(vL) == VTYPE_UNK &&
				!adtValue::empty(vR) )
			{
			IDictionary	*pImgL	= NULL;
			IDictionary	*pImgR	= NULL;
			IDictionary	*pImgO	= NULL;
			cvMatRef		*pMatL	= NULL;
			cvMatRef		*pMatR	= NULL;
			cvMatRef		*pMatO	= NULL;
			bool			bImgR		= false;
			int			chL		= 0;
//			cv::Scalar	*pSclr	= NULL;

			// Image information
			CCLTRY(Prepare::extract ( NULL, vL, &pImgL, &pMatL ));
//			if (hr == S_OK && adtValue::type(vR) == VTYPE_UNK)
			if (hr == S_OK && adtValue::type(vR) == VTYPE_UNK && pSclrR == NULL)
				{
				CCLTRY(Prepare::extract ( NULL, vR, &pImgR, &pMatR ));
				CCLOK ( bImgR = true; )
				}	// if
			CCLTRY(Prepare::extract ( NULL, v, &pImgO, &pMatO ));

			// Debug
//			CCLOK ( image_to_debug ( pMatL, L"Binary", L"c:/temp/binL.png" ); )

			// All images must be of the same type
			if (hr == S_OK)
				hr = ((pMatL->mat != NULL && (pMatR == NULL || pMatR->mat != NULL) && pMatO->mat != NULL)
						#ifdef	HAVE_OPENCV_UMAT
						|| (pMatL->umat != NULL && (pMatR == NULL || pMatR->umat != NULL) && pMatO->umat != NULL)
						#endif
						#ifdef	HAVE_OPENCV_CUDA
						|| (pMatL->gpumat != NULL && (pMatR == NULL || pMatR->gpumat != NULL) && pMatO->gpumat != NULL)
						#endif 
						) ? S_OK : ERROR_INVALID_STATE;

			// For multi-channel scalar operations TODO: Support for > 3 channels ?
//			if (hr == S_OK && !bImgR)
//				hr = ((	pSclr =	(pMatL->channels() == 2) ? new cv::Scalar ( adtFloat(vR), adtFloat(vR) ) :
//										(pMatL->channels() == 3) ? new cv::Scalar ( adtFloat(vR), adtFloat(vR), adtFloat(vR) ) :
//																			new cv::Scalar ( adtFloat(vR) )) != NULL) ? S_OK : E_OUTOFMEMORY;

			// Channel count for scalar multiply
			CCLOK ( chL = pMatL->channels(); )

			// Debug
//			CCLOK ( lprintf ( LOG_DBG, L"%s: iOp %d Left %d/%d Right %d/%d Output %d/%d\r\n",
//										(LPCWSTR)strnName, iOp, pMatL->type(), pMatL->channels(),
//										(bImgR) ? pMatR->type() : 0, (bImgR) ? pMatR->channels() : 0,
//										pMatO->type(), pMatO->channels() ); )
			/*
			adtString strFmt;
			if (image_format ( pMatL, strFmt ) == S_OK)
				lprintf ( LOG_DBG, L"L %s\r\n", (LPCWSTR)strFmt );
			if (image_format ( pMatR, strFmt ) == S_OK)
				lprintf ( LOG_DBG, L"R %s\r\n", (LPCWSTR)strFmt );
			if (image_format ( pMatO, strFmt ) == S_OK)
				lprintf ( LOG_DBG, L"O %s\r\n", (LPCWSTR)strFmt );
			*/

			try
				{
				// Apply operation
				if (hr == S_OK)
					{
					switch (iOp)
						{
						case MATHOP_ADD :
							{
							// NOTE: If alpha, beta or gamma are non-zero then assume graph wants
							// a 'weighted' add.
							bool bW = (fAlpha != 0.0f || fBeta != 0.0f || fGamma != 0.0f);
							if (bImgR)
								{
								if (pMatL->isMat())
									{
									if (!bW)	cv::add ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
									else		cv::addWeighted ( *(pMatL->mat), fAlpha, *(pMatR->mat), fBeta, fGamma, *(pMatO->mat) );
									}	// if
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									{
									if (!bW)	cv::add ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
									else		cv::addWeighted ( *(pMatL->umat), fAlpha, *(pMatR->umat), fBeta, fGamma, *(pMatO->umat) );
									}	// else if
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									{
									if (!bW)	cv::cuda::add ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
									else		cv::cuda::addWeighted ( *(pMatL->gpumat), fAlpha, *(pMatR->gpumat), fBeta, fGamma, *(pMatO->gpumat) );
									}	// else if
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									{
									if (!bW)	cv::add ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
									else		cv::addWeighted ( *(pMatL->mat), fAlpha, *(pSclrR), fBeta, fGamma, *(pMatO->mat) );
									}	// if
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									{
									if (!bW)	cv::add ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
									else		cv::addWeighted ( *(pMatL->umat), fAlpha, *(pSclrR), fBeta, fGamma, *(pMatO->umat) );
									}	// else if
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									{
									if (!bW)	cv::cuda::add ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
									else		cv::cuda::addWeighted ( *(pMatL->gpumat), fAlpha, *(pSclrR), fBeta, fGamma, *(pMatO->gpumat) );
									}	// else if
								#endif
								}	// else
							}	// MATHOP_ADD
							break;
						case MATHOP_SUB :
							if (bImgR)
								{
								if (pMatL->isMat())
									cv::subtract ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::subtract ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::subtract ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									cv::subtract ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::subtract ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::subtract ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
								#endif
								}	// else
							break;
						case MATHOP_MUL :
							if (bImgR)
								{
								if (pMatL->isMat())
									{
									if (bMat)
										*(pMatO->mat) = *(pMatL->mat) * *(pMatR->mat);
									else
										cv::multiply ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
									}	// if
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									{
									if (bMat)
										cv::gemm ( *(pMatL->umat), *(pMatR->umat), 1.0, cv::noArray(), 0.0, *(pMatO->umat) );
									else
										cv::multiply ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
									}	// else if
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									{
									if (bMat)
										cv::cuda::gemm ( *(pMatL->gpumat), *(pMatR->gpumat), 1.0, cv::noArray(), 0.0, *(pMatO->gpumat) );
									else
										cv::cuda::multiply ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
									}	// else if
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									cv::multiply ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::multiply ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::multiply ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
								#endif
								}	// else
							break;
						case MATHOP_DIV :
							if (bImgR)
								{
								if (pMatL->isMat())
									cv::divide ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::divide ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::divide ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									cv::divide ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::divide ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::divide ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
								#endif
								}	// else
							break;
						case MATHOP_POW :
							// Current just supporting image ^ power so right side must be singular scalar.
							if (!bImgR)
								{
								if (pMatL->isMat())
									cv::pow ( *(pMatL->mat), (*(pSclrR))[0], *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::pow ( *(pMatL->umat), (*(pSclrR))[0], *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::pow ( *(pMatL->gpumat), (*(pSclrR))[0], *(pMatO->gpumat) );
								#endif
								}	// if
							else 
								hr = E_NOTIMPL;
							break;

						// Bitwise
						case MATHOP_AND :
							if (bImgR)
								{
								if (pMatL->isMat())
									cv::bitwise_and ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::bitwise_and ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::bitwise_and ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									cv::bitwise_and ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::bitwise_and ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::bitwise_and ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
								#endif
								}	// else
							break;
						case MATHOP_XOR :
							if (bImgR)
								{
								if (pMatL->isMat())
									cv::bitwise_xor ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::bitwise_xor ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::bitwise_xor ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									cv::bitwise_xor ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::bitwise_xor ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::bitwise_xor ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
								#endif
								}	// else
							break;
						case MATHOP_OR :
							if (bImgR)
								{
								if (pMatL->isMat())
									cv::bitwise_or ( *(pMatL->mat), *(pMatR->mat), *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::bitwise_or ( *(pMatL->umat), *(pMatR->umat), *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::bitwise_or ( *(pMatL->gpumat), *(pMatR->gpumat), *(pMatO->gpumat) );
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									cv::bitwise_or ( *(pMatL->mat), *pSclrR, *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::bitwise_or ( *(pMatL->umat), *pSclrR, *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::bitwise_or ( *(pMatL->gpumat), *pSclrR, *(pMatO->gpumat) );
								#endif
								}	// else
							break;
						case MATHOP_LEFT :
							if (bImgR)
								{
								if (pMatL->isMat())
									pMatL->mat->copyTo ( *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									pMatL->umat->copyTo ( *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									pMatL->gpumat->copyTo ( *(pMatO->gpumat) );
								#endif
								}	// if
							break;
						case MATHOP_RIGHT :
							if (bImgR)
								{
								if (pMatR->isMat())
									pMatR->mat->copyTo ( *(pMatO->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatR->isUMat())
									pMatR->umat->copyTo ( *(pMatO->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatR->isGPU())
									pMatR->gpumat->copyTo ( *(pMatO->gpumat) );
								#endif
								}	// if
							break;

						// Comparisons
						case MATHOP_EQ :
						case MATHOP_GT :
						case MATHOP_GE :
						case MATHOP_LT :
						case MATHOP_LE :
						case MATHOP_NE :
							{
							// OpenCv compare operation
							int
							cmpop =	(iOp == MATHOP_EQ) ? cv::CMP_EQ :
										(iOp == MATHOP_GT) ? cv::CMP_GT :
										(iOp == MATHOP_GE) ? cv::CMP_GE :
										(iOp == MATHOP_GT) ? cv::CMP_LT :
										(iOp == MATHOP_GE) ? cv::CMP_LE : cv::CMP_NE;

							// Perform comparison
							// NOTE: Compare functions seem to need their own destination away
							// from source.  If outside graph uses the same destination as one of the
							// sides this won't work.  Output to own matrix then copy result.
							if (bImgR)
								{
								if (pMatL->isMat())
									{
									cv::Mat	matO;
									cv::compare ( *(pMatL->mat), *(pMatR->mat), matO, cmpop );
									matO.copyTo(*(pMatO->mat));
									}	// if
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									{
									cv::UMat	matO;
									cv::compare ( *(pMatL->umat), *(pMatR->umat), matO, cmpop );
									matO.copyTo(*(pMatO->umat));
									}	// else if
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									{
									cv::cuda::GpuMat	matO;
									cv::cuda::compare ( *(pMatL->gpumat), *(pMatR->gpumat), matO, cmpop );
									matO.copyTo(*(pMatO->gpumat));
									}	// else if
								#endif
								}	// if
							else
								{
								if (pMatL->isMat())
									{
									cv::Mat	matO;
									cv::compare ( *(pMatL->mat), *pSclrR, matO, cmpop );
									matO.copyTo(*(pMatO->mat));
									}	// if
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									{
									cv::UMat	matO;
									cv::compare ( *(pMatL->umat), *pSclrR, matO, cmpop );
									matO.copyTo(*(pMatO->umat));
									}	// else if
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									{
									cv::cuda::GpuMat	matO;
									cv::cuda::compare ( *(pMatL->gpumat), *pSclrR, matO, cmpop );
									matO.copyTo(*(pMatO->gpumat));
									}	// else if
								#endif
								}	// else

							}	// MATHOP_XXX
							break;

						// Copy image (left) to output using mask (right)
						case MATHOP_COPY :
							if (bImgR)
								{
								if (pMatL->isMat())
									pMatL->mat->copyTo ( *(pMatO->mat), *(pMatR->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									pMatL->umat->copyTo ( *(pMatO->umat), *(pMatR->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									pMatL->gpumat->copyTo ( *(pMatO->gpumat), *(pMatR->gpumat) );
								#endif
								}	// if
							else 
								hr = E_NOTIMPL;
							break;

						// Per pixel transform
						case MATHOP_XFORM :
							if (bImgR)
								{
								if (pMatL->isMat())
									cv::transform ( *(pMatR->mat), *(pMatO->mat), *(pMatL->mat) );
								#ifdef	HAVE_OPENCV_UMAT
								else if (pMatL->isUMat())
									cv::transform ( *(pMatR->umat), *(pMatO->umat), *(pMatL->umat) );
								#endif
								#ifdef	HAVE_OPENCV_CUDA
								else if (pMatL->isGPU())
									cv::cuda::transform ( *(pMatR->gpumat), *(pMatO->gpumat), *(pMatL->gpumat) );
								#endif
								}	// if
							else
								hr = E_NOTIMPL;
							break;

						/*
						https://docs.opencv.org/2.4/doc/tutorials/gpu/gpu-basics-similarity/gpu-basics-similarity.html
						// Peak signal to noise ratio
						case MATHOP_PSNR :
							// Image to image required
							if (bImgR)
								{
								// CPU
								if (pMatL->isMat())
									{
									// Abs
//									cv::Mat	matO;
//									cv::compare ( *(pMatL->mat), *(pMatR->mat), matO, cmpop );
//									matO.copyTo(*(pMatO->mat));
									}	// if
								else
									hr = E_NOTIMPL;
								}	// if
							else
								hr = E_NOTIMPL;
							break;
							*/

						// Not implemented
						default :
							hr = E_NOTIMPL;
						}	// switch

					}	// if

				}	// try
			catch ( cv::Exception &ex )
				{
				lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = E_UNEXPECTED;
				}	// catch

			// Result
			CCLTRY ( adtValue::copy ( adtIUnknown(pImgO), vRes ) );

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_ERR, L"%s:Fire:Error:hr 0x%x:iOp %d:L %p:R %p O %p:ImgR? %d\r\n", 
								(LPCWSTR)strnName, hr, iOp, pImgL, pImgR, pImgO, (bImgR) ? 1 : 0 );

			// Debug
//			CCLOK ( image_to_debug ( pMatO, L"Binary", L"c:/temp/binO.png" ); )

			// Clean up
//			if (pSclr != NULL)
//				delete pSclr;
			_RELEASE(pMatO);
			_RELEASE(pImgO);
			_RELEASE(pMatR);
			_RELEASE(pImgR);
			_RELEASE(pMatL);
			_RELEASE(pImgL);
			}	// if

		// Not handled/error
		else
			{
			lprintf ( LOG_DBG, L"%s: iOp %d Left? %s Right? %s Out? %s\r\n",
										(LPCWSTR)strnName, iOp,
										(adtValue::empty(vL) == false) ? L"true" : L"false",
										(adtValue::empty(vR) == false) ? L"true" : L"false",
										(adtValue::empty(v) == false) ? L"true" : L"false" );
			hr = E_NOTIMPL;
			}	// else

		// Result
		if (hr == S_OK)
			_EMT(Fire,vRes);
		else
			_EMT(Error,adtInt(hr) );
		}	// else if

	// State
	else if (_RCP(Left))
		adtValue::copy ( v, vL );
	else if (_RCP(Right))
		{
		adtIUnknown unkV(v);
		IList			*pLst	= NULL;
		IIt			*pIt	= NULL;
		U32			sz		= 0;
		adtValue		vRs[4];

		// Cache specified value
		adtValue::copy ( v, vR );

		// Release previous scalar
		if (pSclrR != NULL)
			{
			delete pSclrR;
			pSclrR = NULL;
			}	// if

		// Allow either a single value (list of 1 scalar) or
		// a list of scalers
		if (	adtValue::type(vR) != VTYPE_UNK )
			{
			// Assign
			adtValue::copy ( vR, vRs[0] );
			sz = 1;
			}	// if

		// Object ?
		else if (	(IUnknown *)NULL != unkV				&&
						_QI(unkV,IID_IList,&pLst) == S_OK	&&
						pLst->iterate(&pIt) == S_OK )
			{
			// Number of items in the list
			pLst->size(&sz);

			// Read specified values
			for (U32 i = 0;i < sz && i < 4;++i)
				{
				pIt->read(vRs[i]);
				pIt->next();
				}	// for

			}	// if

		// Create scalar for supported channels
		switch (sz)
			{
			case 1 :
				CCLTRYE ( (pSclrR = new cv::Scalar ( adtFloat(vRs[0]) )) != NULL, E_OUTOFMEMORY );
				break;
			case 2 :
				CCLTRYE ( (pSclrR = new cv::Scalar ( adtFloat(vRs[0]), adtFloat(vRs[1]) )) != NULL, E_OUTOFMEMORY );
				break;
			case 3 :
				CCLTRYE ( (pSclrR = new cv::Scalar (	adtFloat(vRs[0]), adtFloat(vRs[1]), 
																	adtFloat(vRs[2]) )) != NULL, E_OUTOFMEMORY );
				break;
			case 4 :
				CCLTRYE ( (pSclrR = new cv::Scalar (	adtFloat(vRs[0]), adtFloat(vRs[1]),
																	adtFloat(vRs[2]), adtFloat(vRs[3]) )) != NULL, E_OUTOFMEMORY );
				break;
			// pSclR will remain NULL
			}	// switch

		// Clean up
		_RELEASE(pIt);
		_RELEASE(pLst);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

