////////////////////////////////////////////////////////////////////////
//
//									AT.CPP
//
//				Implementation of the pixel access image node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

At :: At ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg	= NULL;
	iX		= 0;
	iY		= 0;
	iCh	= 0;
	}	// At

HRESULT At :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"X"), vL ) == S_OK)
			iX = vL;
		if (pnDesc->load ( adtString(L"Y"), vL ) == S_OK)
			iY = vL;
		if (pnDesc->load ( adtString(L"Channel"), vL ) == S_OK)
			onReceive(prChannel,vL);
		pnDesc->load ( adtString(L"Value"), vAt );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT At :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Load
	if (_RCP(Load))
		{
		IDictionary	*pImgUse = NULL;
		IList			*pvLst	= NULL;
		cvMatRef		*pMat		= NULL;
		bool			bSingle	= false;
		S32			x			= 0;
		S32			y			= 0;
		adtValue		vLd;
		cv::Mat		matAt;

		// Value to use
//		const ADTVALUE *pvUse	= (!adtValue::empty(vAt)) ? &vAt : &v;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Valid range ?
		CCLTRYE (	(iX >= 0 && (S32)iX < pMat->cols()) &&
						(iY >= 0 && (S32)iY < pMat->rows()), ERROR_INVALID_STATE );

		// Starting coordinates
		CCLOK ( x = iX; )
		CCLOK ( y = iY; )

		//
		// Determine if specified value is a list
		//
		if (hr == S_OK)
			{
//			adtIUnknown		unkV(*pvUse);
			adtIUnknown		unkV(vAt);

			// Assume a single value
			bSingle = true;

			// Object ?
			if ((IUnknown *)(NULL) != unkV)
				{
				// Not a single value
				bSingle = false;

				// List ?
				if (	_QI(unkV,IID_IList,&pvLst) != S_OK )
					hr = ERROR_NOT_SUPPORTED;
				}	// if

			}	// if

		// If the source mat is 'umat' or 'gpumat' then a copy of the
		// data must be downloaded for access to the elements
		#ifdef	HAVE_OPENCV_UMAT
		if (hr == S_OK && pMat->isUMat())
			matAt = pMat->umat->getMat ( cv::ACCESS_READ );
		#endif

		// Load value or values from image
		while (hr == S_OK && (bSingle || x < pMat->cols()))
			{
			// Open CV uses exceptions
			try
				{
				// CPU
				if (pMat->isMat())
					{
					// Single channel
					if (pMat->mat->channels() == 1)
						{
						switch (CV_MAT_DEPTH(pMat->mat->type()))
							{
							// 8-bit
							case CV_8U :
								hr = adtValue::copy ( adtInt(pMat->mat->at<U8>(y,x)), vLd );
								break;
							case CV_8S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<S8>(y,x)), vLd );
								break;

							// 16-bit
							case CV_16U :
								hr = adtValue::copy ( adtInt(pMat->mat->at<U16>(y,x)), vLd );
								break;
							case CV_16S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<S16>(y,x)), vLd );
								break;

							// 32-bit
							case CV_32S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<S32>(y,x)), vLd );
								break;
							case CV_32F :
								hr = adtValue::copy ( adtFloat(pMat->mat->at<float>(y,x)), vLd );
								break;
							}	// switch
						}	// if

//								pMat->mat->at<cv::Vec4i>(y,x)[iCh] = adtInt(vSt);

					// 2-channel
					else if (pMat->mat->channels() == 2)
						{
//lprintf ( LOG_DBG, L"Channels %d", CV_MAT_CN(pMat->mat->flags) );
						switch (CV_MAT_DEPTH(pMat->mat->type()))
							{
							// 8-bit
							case CV_8U :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2b>(y,x)[iCh]), vLd );
								break;
							case CV_8S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2b>(y,x)[iCh]), vLd );
								break;

							// 16-bit
							case CV_16U :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2w>(y,x)[iCh]), vLd );
								break;
							case CV_16S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2s>(y,x)[iCh]), vLd );
								break;

							// 32-bit
							case CV_32S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2i>(y,x)[iCh]), vLd );
								break;
							case CV_32F :
								hr = adtValue::copy ( adtFloat(pMat->mat->at<cv::Vec2f>(y,x)[iCh]), vLd );
								break;
							}	// switch
						}	// else if

					// 4-channel
					else if (pMat->mat->channels() == 4)
						{
//lprintf ( LOG_DBG, L"Channels %d", CV_MAT_CN(pMat->mat->flags) );
						switch (CV_MAT_DEPTH(pMat->mat->type()))
							{
							// 8-bit
							case CV_8U :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2b>(y,x)[iCh]), vLd );
								break;
							case CV_8S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2b>(y,x)[iCh]), vLd );
								break;

							// 16-bit
							case CV_16U :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2w>(y,x)[iCh]), vLd );
								break;
							case CV_16S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2s>(y,x)[iCh]), vLd );
								break;

							// 32-bit
							case CV_32S :
								hr = adtValue::copy ( adtInt(pMat->mat->at<cv::Vec2i>(y,x)[iCh]), vLd );
								break;
							case CV_32F :
								hr = adtValue::copy ( adtFloat(pMat->mat->at<cv::Vec2f>(y,x)[iCh]), vLd );
								break;
							}	// switch
						}	// else if

					}	// if

				#ifdef	HAVE_OPENCV_CUDA
				// GPU
				else if (pMat->isGPU())
					{
					// Anything can be done ?
					static bool bFirst = true;
					if (bFirst)
						{
						lprintf ( LOG_ERR, L"Per pixel access for GPU images not available" );
						bFirst = false;
						}	// if

					// Unsupported since image is in GPU memory
					hr = E_NOTIMPL;
					}	// if
				#endif

				// UMAT
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					{
					// Access array
					switch (CV_MAT_DEPTH(matAt.type()))
						{
						// 8-bit
						case CV_8U :
							hr = adtValue::copy ( adtInt(matAt.at<U8>(y,x)), vLd );
							break;
						case CV_8S :
							hr = adtValue::copy ( adtInt(matAt.at<S8>(y,x)), vLd );
							break;

						// 16-bit
						case CV_16U :
							hr = adtValue::copy ( adtInt(matAt.at<U16>(y,x)), vLd );
							break;
						case CV_16S :
							hr = adtValue::copy ( adtInt(matAt.at<S16>(y,x)), vLd );
							break;

						// 32-bit
						case CV_32S :
							hr = adtValue::copy ( adtInt(matAt.at<S32>(y,x)), vLd );
							break;
						case CV_32F :
							hr = adtValue::copy ( adtFloat(matAt.at<float>(y,x)), vLd );
							break;
						}	// switch
					}	// else if
				#endif

				}	// try
			catch ( cv::Exception ex )
				{
				lprintf ( LOG_ERR, L"%s:At::Load:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = E_UNEXPECTED;
				}	// catch

			// Single value done or proceed to next value
			if (bSingle)
				break;
			else
				{
				pvLst->write(vLd);
				++x;
				}	// else
			}	// while

		// Result
		if (hr == S_OK)
			{
			if (bSingle)
				_EMT(Load,vLd);
			else
				_EMT(Load,adtIUnknown(pvLst));
			}	// if
		else
			{
//			lprintf ( LOG_ERR, L"Load failed hr 0x%x\r\n", hr );
			_EMT(Error,adtInt(hr));
			}	// else

		// Clean up
		_RELEASE(pvLst);
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// Store
	else if (_RCP(Store))
		{
		IDictionary	*pImgUse = NULL;
		IDictionary	*pvDct	= NULL;
		IContainer	*pvCnt	= NULL;
		cvMatRef		*pMat		= NULL;
		IIt			*pIt		= NULL;
		bool			bSingle	= false;
		S32			x			= 0;
		S32			nx			= 0;
		S32			y			= 0;
		S32			ny			= 0;
		U32			sz			= 0;
		adtValue		vSt;

		// Value to use
		const ADTVALUE *pvUse	= (!adtValue::empty(vAt)) ? &vAt : &v;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Valid range ? (Y = -1 means append)
		CCLTRYE ( (((S32)iX) >= 0 && ((S32)iY) >= -1), ERROR_INVALID_STATE );

		//
		// Determine if specified value is a dictionary, list or single value
		//
		if (hr == S_OK)
			{
			adtIUnknown		unkV(*pvUse);

			// Assume not a single value
			bSingle = false;

			// Object ?
			if ((IUnknown *)(NULL) != unkV)
				{
				// Dictionary/List ?
				if (	_QI(unkV,IID_IDictionary,&pvDct) == S_OK )
					{
					// Iterate keys/values
					CCLTRY ( pvDct->iterate(&pIt) );

					// Assign as container
					CCLOK ( pvCnt = pvDct; )
					_ADDREF(pvCnt);
					}	// if
				else if (_QI(unkV,IID_IContainer,&pvCnt) == S_OK )
					hr = pvCnt->iterate(&pIt);
				else
					hr = ERROR_NOT_SUPPORTED;
				}	// if

			// Value
			else
				{
				hr			= adtValue::copy ( *pvUse, vSt );
				bSingle	= true;
				}	// else

			}	// if

		// Needed columns and rows
		if (hr == S_OK)
			{
			// Single value
			nx = iX+1;

			// List of values
			if (pvCnt != NULL && pvCnt->size(&sz) == S_OK)
				nx = sz;

			// Single value
			ny = iY+1;

			// Y = -1 means auto-append row for a list
			if (iY == -1 && pvCnt != NULL)
				ny = pMat->rows()+1;
			}	// if

		// Append any needs columns and rows
		if (hr == S_OK)
			{
			// Need columns ?
			if (nx > pMat->cols())
				{
				// Columns to append
				cv::Mat	matCols(pMat->rows(),(nx-pMat->cols()),pMat->mat->type());

				// Zero data, node property ?
				matCols = cv::Scalar::all(0);

				// Append
				cv::hconcat ( *(pMat->mat), matCols, *(pMat->mat) );
				}	// if

			// Need rows ?
			if (ny > pMat->rows())
				{
				// Rows to append
				cv::Mat	matRows((ny-pMat->rows()),pMat->cols(),pMat->mat->type());

				// Zero data, node property ?
				matRows = cv::Scalar::all(0);

				// Append
				cv::vconcat ( *(pMat->mat), matRows, *(pMat->mat) );
				}	// if

			}	// if

		// Starting coordinates
		if (hr == S_OK)
			{
			// List
			if (iY == -1)
				{
				x = 0;
				y = (pMat->rows()-1);
				}	// if
			// Value
			else
				{
				x = iX;
				y = iY;
				}	// else
			}	// if

		// Store value or values into array
		while (hr == S_OK && (bSingle || pIt->read(vSt) == S_OK))
			{
			// Open CV uses exceptions
			try
				{
				// CPU
				if (pMat->isMat())
					{
					// Single channel
					if (pMat->mat->channels() == 1)
						{
						switch (CV_MAT_DEPTH(pMat->mat->type()))
							{
							// 8-bit
							case CV_8U :
								pMat->mat->at<U8>(y,x) = adtInt(vSt);
								break;
							case CV_8S :
								pMat->mat->at<S8>(y,x) = adtInt(vSt);
								break;

							// 16-bit
							case CV_16U :
								pMat->mat->at<U16>(y,x) = adtInt(vSt);
								break;
							case CV_16S :
								pMat->mat->at<S16>(y,x) = adtInt(vSt);
								break;

							// 32-bit
							case CV_32S :
								pMat->mat->at<S32>(y,x) = adtInt(vSt);
								break;
							case CV_32F :
								pMat->mat->at<float>(y,x) = adtFloat(vSt);
								break;
							}	// switch
						}	// if

					// 4 Channel
					else if (pMat->mat->channels() == 4)
						{
						switch (CV_MAT_DEPTH(pMat->mat->type()))
							{
							// 8-bit
							case CV_8U :
//lprintf ( LOG_DBG, L"CV_8U4 %d,%d,%d,0x%x", x, y, (S32)iCh, (U32)adtInt(vSt) );
								pMat->mat->at<cv::Vec4b>(y,x)[iCh] = adtInt(vSt);
								break;
							case CV_8S :
								pMat->mat->at<cv::Vec4b>(y,x)[iCh] = adtInt(vSt);
								break;

							// 16-bit
							case CV_16U :
								pMat->mat->at<cv::Vec4w>(y,x)[iCh] = adtInt(vSt);
								break;
							case CV_16S :
								pMat->mat->at<cv::Vec4s>(y,x)[iCh] = adtInt(vSt);
								break;

							// 32-bit
							case CV_32S :
								pMat->mat->at<cv::Vec4i>(y,x)[iCh] = adtInt(vSt);
								break;
							case CV_32F :
								pMat->mat->at<cv::Vec4f>(y,x)[iCh] = adtFloat(vSt);
								break;
							}	// switch
						}	// else

					}	// else

				// GPU
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					{
					// Anything can be done ?
					static bool bFirst = true;
					if (bFirst)
						{
						lprintf ( LOG_ERR, L"Per pxel access for GPU images not available" );
						bFirst = false;
						}	// if

					// Unsupported since image is in GPU memory
					hr = E_NOTIMPL;
					}	// if
				#endif

				// UMAT
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					{
					// Access array
					cv::Mat	matAt = pMat->umat->getMat( (_RCP(Load)) ? cv::ACCESS_READ : cv::ACCESS_WRITE );
					switch (CV_MAT_DEPTH(matAt.type()))
						{
						// 8-bit
						case CV_8U :
							matAt.at<U8>(y,x) = adtInt(vSt);
							break;
						case CV_8S :
							matAt.at<S8>(y,x) = adtInt(vSt);
							break;

						// 16-bit
						case CV_16U :
							matAt.at<U16>(y,x) = adtInt(vSt);
							break;
						case CV_16S :
							matAt.at<S16>(y,x) = adtInt(vSt);
							break;

						// 32-bit
						case CV_32S :
							matAt.at<S32>(y,x) = adtInt(vSt);
							break;
						case CV_32F :
							matAt.at<float>(y,x) = adtFloat(vSt);
							break;
						}	// switch
					}	// else if
				#endif

				}	// try
			catch ( cv::Exception ex )
				{
				lprintf ( LOG_ERR, L"%s:At::Store:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = E_UNEXPECTED;
				}	// catch

			// Single value done or proceed to next value
			if (bSingle)
				break;
			else
				{
				pIt->next();
				++x;
				}	// else
			}	// while

		// Result
		if (hr == S_OK)
			_EMT(Store,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pIt);
		_RELEASE(pvDct);
		_RELEASE(pvCnt);
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(X))
		iX = v;
	else if (_RCP(Y))
		iY = v;
	else if (_RCP(Channel))
		iCh = v;
	else if (_RCP(Value))
		adtValue::copy ( v, vAt );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

