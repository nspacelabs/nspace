////////////////////////////////////////////////////////////////////////
//
//									Stats.CPP
//
//				Implementation of the image statistics node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Stats :: Stats ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg			= NULL;
	bEnt			= false;
	bBoundRct	= false;
	bSum			= false;
	bCum			= false;
	fMin			= 0.0;
	fMax			= 256.0f;
	iSz			= 256;
	fPerc			= 0.0f;
	}	// Stats

HRESULT Stats :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Entropy"), vL ) == S_OK)
			bEnt = vL;
		if (pnDesc->load ( adtString(L"BoundRect"), vL ) == S_OK)
			bBoundRct = vL;
		if (pnDesc->load ( adtString(L"NonZero"), vL ) == S_OK)
			bNonZero = vL;
		if (pnDesc->load ( adtString(L"Sum"), vL ) == S_OK)
			bSum = vL;
		if (pnDesc->load ( adtString(L"Cumulative"), vL ) == S_OK)
			bCum = vL;
		if (pnDesc->load ( adtString(L"Minimum"), vL ) == S_OK)
			onReceive(prMinimum,vL);
		if (pnDesc->load ( adtString(L"Maximum"), vL ) == S_OK)
			onReceive(prMaximum,vL);
		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			onReceive(prSize,vL);
		if (pnDesc->load ( adtString(L"Percent"), vL ) == S_OK)
			fPerc = vL;
		if (pnDesc->load ( adtString(L"Ranges"), vL ) == S_OK)
			unkRngs = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Stats :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// OpenCV throws exceptions
		try
			{
			// Limits
			if (hr == S_OK)
				{
				cv::Point	ptMin,ptMax;
				double		dMin,dMax;
				adtString	strFmt;
				int			ch = 0;

				// MinMaxLoc does not accept requests for min/max points so filter that out

				// Only makes sense in single channel case
				if ((ch = pMat->channels()) == 1)
					{
					// Values and locations of min and max
					if (pMat->isMat())
						cv::minMaxLoc ( *(pMat->mat), &dMin, &dMax, 
												(ch == 1) ? &ptMin : NULL, (ch == 1) ? &ptMax : NULL );
					#ifdef	HAVE_OPENCV_UMAT
					else if (pMat->isUMat())
						cv::minMaxLoc ( *(pMat->umat), &dMin, &dMax, 
												(ch == 1) ? &ptMin : NULL, (ch == 1) ? &ptMax : NULL );
					#endif
					#ifdef	HAVE_OPENCV_CUDA
					else if (pMat->isGPU())
						cv::cuda::minMaxLoc ( *(pMat->gpumat), &dMin, &dMax, 
														(ch == 1) ? &ptMin : NULL, (ch == 1) ? &ptMax : NULL );
					#endif
					}	// if

				// Extract string version of format
				CCLTRY ( image_format ( pMat, strFmt ) );

				// Result
				CCLTRY ( pImgUse->store ( adtString(L"Width"), adtInt(pMat->cols()) ) );
				CCLTRY ( pImgUse->store ( adtString(L"Height"), adtInt(pMat->rows()) ) );
				CCLTRY ( pImgUse->store ( adtString(L"Format"), strFmt ) );
				CCLTRY ( pImgUse->store ( adtString(L"Channels"), adtInt(ch) ) );
				if (hr == S_OK && ch == 1)
					{
					CCLTRY ( pImgUse->store ( adtString(L"Min"), adtDouble(dMin) ) );
					CCLTRY ( pImgUse->store ( adtString(L"Max"), adtDouble(dMax) ) );
					CCLTRY ( pImgUse->store ( adtString(L"MinX"), adtInt(ptMin.x) ) );
					CCLTRY ( pImgUse->store ( adtString(L"MinY"), adtInt(ptMin.y) ) );
					CCLTRY ( pImgUse->store ( adtString(L"MaxX"), adtInt(ptMax.x) ) );
					CCLTRY ( pImgUse->store ( adtString(L"MaxY"), adtInt(ptMax.y) ) );
					}	// if
				}	// if

			// Local try/catch.  For some reason CUDA supports
			// mean/standard deviation only on 8-bit images.  Errors
			// like this should be noted but no reason to fail the entire section
			// since other statistics could still be useful.
			if (hr == S_OK && (!pMat->isGPU() || pMat->type() == CV_8UC1))
				{
				cv::Scalar mean	= 0;
				cv::Scalar stddev = 0;
				try
					{
					// Perform calculation
					if (pMat->isMat())
						cv::meanStdDev ( *(pMat->mat), mean, stddev );
					#ifdef	HAVE_OPENCV_UMAT
					else if (pMat->isUMat())
						cv::meanStdDev ( *(pMat->umat), mean, stddev );
					#endif
					#ifdef	HAVE_OPENCV_CUDA
					else if (pMat->isGPU())
						{
						// Only support 8-bit unsigned images. One possible solution,
						// get limits, normalize to 0-255 then scale results with previous limits.
						cv::cuda::meanStdDev ( *(pMat->gpumat), mean, stddev );
						}	// else if
					#endif
					// Debug
		//			if (mean[0] > 1000 || mean[0] < -1000)
		//				dbgprintf ( L"Hi\r\n" );

					// Result
					CCLTRY ( pImgUse->store ( adtString(L"Mean"), adtDouble(mean[0]) ) );
					CCLTRY ( pImgUse->store ( adtString(L"StdDev"), adtDouble(stddev[0]) ) );
					}	// try
				catch ( cv::Exception &ex )
					{
					lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
					}	// catch

				}	// if

			// Entropy calculation
			if (hr == S_OK && bEnt == true && pMat->channels() == 1)
				{
				cv::Mat			matHst,matLog;
				float				range[]		= { fMin, fMax };
				const float *	histRange	= { range };
				int				histSize		= iSz;
				float				ent			= 0.0f;

				// Calculate the historgram of the image
				if (pMat->isMat())
					{
					cv::Mat	matHst,matLog;

					// Histogram
					cv::calcHist ( pMat->mat, 1, 0, cv::noArray(), matHst, 1, 
													&histSize, &histRange );

					// Normalize
					matHst /= (double)pMat->mat->total();

					// Ensure no log of zeroes
					cv::add ( matHst, cv::Scalar::all(1e-20), matHst );

					// Compute entropy
					cv::log ( matHst, matLog );
					cv::multiply(matHst,matLog,matHst);
					ent = (float)(-1*cv::sum(matHst).val[0]);
					}	// else
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					{
					std::vector<int>			ch				= { 0 };
					std::vector<int>			histSize		= { (S32)iSz };
					std::vector<float>		histRange	= { fMin, fMax };
					std::vector<cv::UMat>	matLst		= { *(pMat->umat) };
					cv::UMat						matHst,matLog;

					// Only the 'array' version of calcHist is available for UMat
					cv::calcHist ( matLst, ch, cv::UMat(), matHst, histSize, histRange );

					// Normalize
					cv::divide ( matHst, cv::Scalar((double)pMat->umat->total()), matHst );

					// Ensure no log of zeroes
					cv::add ( matHst, cv::Scalar::all(1e-20), matHst );

					// Compute entropy
					cv::log ( matHst, matLog );
					cv::multiply(matHst,matLog,matHst);
					ent = (float)(-1*cv::sum(matHst).val[0]);
					}	// if
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					{
					cv::cuda::GpuMat	gpuHst,gpuHstf,gpuLog,gpu;

					// Histogram
					cv::cuda::calcHist ( *(pMat->gpumat), gpuHst );

					// To floating points for calculation
					gpuHst.convertTo ( gpuHstf, CV_32FC1 );

					// Normalize
					cv::cuda::divide ( gpuHstf, cv::Scalar(pMat->rows()*pMat->cols()), gpuHstf );

					// Ensure no log of zeroes
					cv::cuda::add ( gpuHstf, cv::Scalar::all(1e-20), gpuHstf );

					// Compute entropy
					cv::cuda::log ( gpuHstf, gpuLog );
					cv::cuda::multiply(gpuHstf,gpuLog,gpuHstf);
					ent = (float)(-1*cv::cuda::sum(gpuHstf).val[0]);
					}	// if
				#endif

				// Result
				CCLTRY ( pImgUse->store ( adtString(L"Entropy"), adtDouble(ent) ) );
				}	// if

			// Bounding rectangle
			if (hr == S_OK && bBoundRct == true)
				{
				cv::Rect rct;

				// Calculate bounding rectangle, assumes array of points
				if (pMat->isMat())
					rct = cv::boundingRect ( *(pMat->mat) );
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					rct = cv::boundingRect ( *(pMat->umat) );
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					{
					cv::Mat		matNoGpu;

					// Currently no GPU based version ?
					pMat->gpumat->download ( matNoGpu );

					// Execute
					rct = cv::boundingRect ( matNoGpu );
					}	// if
				#endif

				// Result
				CCLTRY ( pImgUse->store ( adtString(L"Left"), adtInt(rct.x) ) );
				CCLTRY ( pImgUse->store ( adtString(L"Top"), adtInt(rct.y) ) );
				CCLTRY ( pImgUse->store ( adtString(L"Width"), adtInt(rct.width) ) );
				CCLTRY ( pImgUse->store ( adtString(L"Height"), adtInt(rct.height) ) );
				}	// if

			// Pixel sum
			if (hr == S_OK && bSum == true)
				{
				cv::Scalar	sum = 0;

				// Calculate total sum of pixels
				if (pMat->isMat())
					sum = cv::sum(*(pMat->mat));
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					sum = cv::sum(*(pMat->umat));
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					sum = cv::cuda::sum(*(pMat->gpumat));
				#endif

				// Result
				CCLTRY ( pImgUse->store ( adtString(L"Sum"), adtDouble(sum[0]) ) );
				}	// if

			// Non-zero pixels
			if (hr == S_OK && bNonZero == true)
				{
				int nZ = 0;

				// Perform calculation
				if (pMat->isMat())
					nZ = cv::countNonZero ( *(pMat->mat) );
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					nZ = cv::countNonZero ( *(pMat->umat) );
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					nZ = cv::cuda::countNonZero ( *(pMat->gpumat) );
				#endif

				// Result
				CCLTRY ( pImgUse->store ( adtString(L"NonZero"), adtInt(nZ) ) );
				}	 // if

			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = E_UNEXPECTED;
			}	// catch

		// Debug
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"stats failed : 0x%x\r\n", hr );

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// Histogram
	else if (_RCP(Histogram))
		{
		IDictionary		*pImgUse = NULL;
		IDictionary		*pImgHst = NULL;
		cvMatRef			*pMat		= NULL;
		cvMatRef			*pMatHst	= NULL;
//		float				range[]		= { fMin, fMax };
//		const float *	histRange	= { range };
		float				*pfRngs		= NULL;
		int				histSize		= iSz;
		std::vector<float>	
							rngs;

		// Obtain image refence and result image
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );
		CCLTRY ( Prepare::extract ( NULL, v, &pImgHst, &pMatHst ) );

		// List of ranges specified directly ?
		// TODO: Do this once on receiving ranges.
		if (hr == S_OK && (IUnknown *)(NULL) != unkRngs)
			{
			IContainer	*pLst		= NULL;
			IIt			*pIt		= NULL;
			U32			sz,idx	= 0;
			adtValue		vL;

			// Size of range list
			CCLTRY ( _QISAFE(unkRngs,IID_IContainer,&pLst) );
			CCLTRY ( pLst->size(&sz) );

			// Number of ranges has to be size + 1 to cover entire area
			CCLTRYE ( (sz == iSz+1), ERROR_INVALID_STATE );

			// Allocate memory for range array
			CCLTRYE ( (pfRngs = (float *) _ALLOCMEM ( sz*sizeof(float) )) != NULL,
							E_OUTOFMEMORY );

			// Graph can specify a list of non-uniform ranges. Add to list.
			CCLTRY ( pLst->iterate ( &pIt ) );
			while (hr == S_OK && pIt->read ( vL ) == S_OK)
				{
				// Add to range list
				pfRngs[idx++] = adtFloat(vL);

				// Next value
				pIt->next();
				}	// while

			}	// else if

		// Calculate the historgram of the image
		if (hr == S_OK)
			{
			// Vectorized parameters
			try
				{
				// cv::Mat/CPU
				if (pMat->isMat())
					{
					cv::Mat matHst;

					// Histogram (uniform or not)
					if (pfRngs == NULL)
						{
						cv::calcHist ( std::vector<cv::Mat>{*(pMat->mat)}, 
											std::vector<int>{0},
											cv::noArray(),
											matHst,
											std::vector<int>{histSize},
											std::vector<float>{fMin,fMax} );
						}	// if
					else
						{
						cv::calcHist (	pMat->mat, 1, 0, cv::noArray(), matHst, 1,
											&histSize, (const float **) &pfRngs, false );
						}	// else

					// Copy to result
					matHst.copyTo(*(pMatHst->mat));
					}	// if
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					{
					// Histogram (uniform or not)
					if (pfRngs == NULL)
						{
						cv::UMat matHst;

						// Histogram
						cv::calcHist ( std::vector<cv::UMat>{*(pMat->umat)}, 
											std::vector<int>{0},
											cv::noArray(),
											matHst,
											std::vector<int>{histSize},
											std::vector<float>{fMin,fMax} );

						// Copy to result
						matHst.copyTo(*(pMatHst->umat));
						}	// if
					else
						{
						cv::Mat	matImgLcl;
						cv::Mat	matHstLcl;

						// Download image into local mat (slow!)
						pMat->umat->copyTo ( matImgLcl );

						// Non-uniform histogram
						cv::calcHist (	&matImgLcl, 1, 0, cv::noArray(), matHstLcl, 1,
											&histSize, (const float **) &pfRngs, false );

						// Upload result
						matHstLcl.copyTo(*(pMatHst->umat));
						}	// else

					}	// if
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU() && pMatHst->isGPU())
					{
					// NOTE: GPU histograms only work on 8-bit arrays (256 bins) and uniform histograms.
					// Punt to CPU when necessary (slow due to copy down of image)
					if (	(pMat->type() & CV_MAT_DEPTH_MASK) == CV_8U &&
							pfRngs == NULL )
						{
						cv::cuda::GpuMat matHst;

						// Histogram
						cv::cuda::calcHist ( 
							std::vector<cv::cuda::GpuMat>{*(pMat->gpumat)}, 
							cv::noArray(),
							matHst );

						// Copy to result
						matHst.copyTo(*(pMatHst->gpumat));
						}	// if
					else
						{
						cv::Mat	matImg;
						cv::Mat	matHst;

						// Download image into local mat (slow!)
						pMat->gpumat->download(matImg);

						// Histogram (uniform or not)
						if (pfRngs == NULL)
							{
							// Histogram
							cv::calcHist ( std::vector<cv::Mat>{matImg}, 
												std::vector<int>{0},
												cv::noArray(),
												matHst,
												std::vector<int>{histSize},
												std::vector<float>{fMin,fMax} );

							}	// if
						else
							{
							// Non-uniform histogram
							cv::calcHist (	&matImg, 1, 0, cv::noArray(), matHst, 1,
												&histSize, (const float **) &pfRngs, false );
							}	// else

						// Upload result
						pMatHst->gpumat->upload ( matHst );
						}	// else					

					}	// if
				#endif
				else
					hr = E_NOTIMPL;
				}
			catch ( cv::Exception &ex )
				{
				lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = E_UNEXPECTED;
				}	// catch

			// Cumulative ?
			if (bCum)
				{
				// Analysis has to be done locally due to non-OpenCV implementation.
				// Down the histogram into a local matrix
				cv::Mat	matHst;
				if (pMatHst->isMat())
					pMatHst->mat->copyTo ( matHst );
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMatHst->isUMat())
					pMatHst->umat->copyTo ( matHst );
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMatHst->isGPU())
					pMatHst->gpumat->download ( matHst );
				#endif

				// Number of pixels
				U32
				npix = pMat->rows()*pMat->cols();

				// Compute upper and lower bounds of given percentage
				float fL = (1 - (fPerc/100.0f))*npix;
				float fU = (fPerc/100.0f) * npix;

				// Percentile targets
				int	iUat	= 0;
				int	iLat	= 0;

				// Calculate
				for (int i = 1;i < histSize;++i)
					{
					// Accumulate intensities
					matHst.at<float>(i) += matHst.at<float>(i-1);
//lprintf ( LOG_DBG, L"cumsum %d) : %g (%g)\r\n", i, matHst.at<float>(i), matHst.at<float>(i-1) );

					// Percentile search
					if (iLat == 0 && matHst.at<float>(i) > fL)
						iLat = i;
					if (iUat == 0 && matHst.at<float>(i) > fU)
						iUat = i;
					}	// for

				// Percentile wanted ?
				if (fPerc > 0.0f)
					{
					// Upper and lower position of percentile bounds
					CCLTRY ( pImgHst->store ( adtString(L"Lower"), adtInt(iLat) ) );
					CCLTRY ( pImgHst->store ( adtString(L"Upper"), adtInt(iUat) ) );
					}	// if

				// Need to upload results into matrix
				if (pMatHst->isMat())
					matHst.copyTo ( *(pMatHst->mat) );
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMatHst->isUMat())
					matHst.copyTo ( *(pMatHst->umat) );
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMatHst->isGPU())
					pMatHst->gpumat->upload ( matHst );
				#endif
				}	// if (bCum)

			}	// if (hr == S_OK)

		// Debug
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"histogram failed : 0x%x\r\n", hr );

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgHst));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pfRngs);
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		_RELEASE(pMatHst);
		_RELEASE(pImgHst);
		}	// else if

	// Execute debug behaviour (for dev.)
	else if (_RCP(Debug))
		{
		#ifdef 	_WIN32
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		FILE			*fDbg		= NULL;
		adtString	strName;

		// Obtain image refence and result image
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Debug file, use node name
		CCLTRY ( adtValue::copy ( strnName, strName ) );
		CCLTRY ( strName.append ( L".txt" ) );
		CCLTRYE ( _wfopen_s(&fDbg,strName,L"w") == 0, errno );

		// Download pixel values
		cv::Mat	matCpu;
		if (pMat->isMat())
			pMat->mat->copyTo ( matCpu );
		#ifdef	HAVE_OPENCV_UMAT
		else if (pMat->isUMat())
			pMat->umat->copyTo ( matCpu );
		#endif
		#ifdef	HAVE_OPENCV_CUDA
		else if (pMat->isGPU())
			pMat->gpumat->download ( matCpu );
		#endif

		// Dump raw values
		for (int r = 0;hr == S_OK && r < matCpu.rows;++r)
//		for (int c = 0;hr == S_OK && c < matCpu.cols;++c)
			{
//			fwprintf_s ( fDbg, L"%d) ", r );
//			fwprintf_s ( fDbg, L"%d) ", c );
			for (int c = 0;hr == S_OK && c < matCpu.cols;++c)
//			for (int r = 0;hr == S_OK && r < matCpu.rows;++r)
				{
				switch ( matCpu.channels() )
					{
					case 1 :
						{
						switch (CV_MAT_DEPTH(matCpu.type()))
							{
							case CV_32F :
								{
								float		v = matCpu.at<float>(r,c);
								fwprintf_s ( fDbg, L"%g ", v );
								}	// CV_32F
								break;
							}	// switch

						}	// 1
						break;
					case 3 :
						{
						switch (CV_MAT_DEPTH(matCpu.type()))
							{
							case CV_8U :
								{
								cv::Vec3b v = matCpu.at<cv::Vec3b>(r,c);
								fwprintf_s ( fDbg, L"(%d %d %d) ", v[0], v[1], v[2] );
								}	// CV_32F
								break;
							case CV_16U :
								{
								cv::Vec3s	v = matCpu.at<cv::Vec3s>(r,c);
								fwprintf_s ( fDbg, L"(%d %d %d) ", v[0], v[1], v[2] );
								}	// CV_32F
								break;
							case CV_32F :
								{
								cv::Vec3f	v = matCpu.at<cv::Vec3f>(r,c);
								fwprintf_s ( fDbg, L"(%g %g %g) ", v[0], v[1], v[2] );
								}	// CV_32F
								break;
							}	// switch

						}	// 3
						break;
					}	// switch
				}	// for
			fwprintf_s ( fDbg, L"\r" );
			}	// for

		// Clean up
		if (fDbg != NULL)
			fclose(fDbg);
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		#endif
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Minimum))
		fMin = v;
	else if (_RCP(Maximum))
		fMax = v;
	else if (_RCP(Size))
		iSz = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

