////////////////////////////////////////////////////////////////////////
//
//										JPEG.CPP
//
//				Implementation of the JPEG (de)compression utilities
//
//				Copyright (c) nSpace, LLC
//				All right reserved
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

//#ifdef	_WIN32
//extern "C" {
//#define	HAVE_INT32
//#include "../ext/libjpeg/jpeglib.h"
//#include "../ext/libjpeg/jerror.h"
//#include "../ext/libjpeg/jpegint.h"
//}
//#elif		__unix__
#include <jpeglib.h>
#if		__unix__
#include	<sys/time.h>
#endif

// For JPEG library...
#include <setjmp.h>

//
// Structure - CTX_JPEG.	JPEG compression/decompression context.  We
//		want a big enough buffer for compression so that we will most likely
//		not need to resize the destination byte stream.
//
#define	JCOMPRESS_BLKSIZE		0x2000

typedef struct
	{
	IMemoryMapped	*pBitsDst;							// Destination bits
	void				*pvBitsDst;							// Destination buffer
	U32				szDst;								// Destination size
	HRESULT			hr;									// Context result functions
	jmp_buf			ljenv;								// Long jump environment
	bool 				bFinish;								// Inside 'finish' ?
	} CTX_JPEG;

// Prototypes
void		jpeg_error_exit				( j_common_ptr );
boolean	jpeg_fill_input_buffer		( j_decompress_ptr );
void		jpeg_init_destination		( j_compress_ptr );
void		jpeg_init_source				( j_decompress_ptr );
boolean	jpeg_empty_output_buffer	( j_compress_ptr );
void		jpeg_skip_input_data			( j_decompress_ptr, long );
void		jpeg_term_destination		( j_compress_ptr );
void		jpeg_term_source				( j_decompress_ptr );

// Local functions
#ifdef	USE_CUDAJPEG
HRESULT	cudaCompress	(	U32, U32, U32, U32, U32, U32,
									IMemoryMapped *,
									IMemoryMapped * );
HRESULT	cudaDecompress	(	IMemoryMapped *,
									IMemoryMapped *,
									U32 *, U32 *, U32 *, U32 * );
HRESULT	cudaJpegInit	( bool );

static U32	cudaJpegRefCnt	= 0;
#endif

libJpeg :: libJpeg ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	iQ		= 75;

	// Reference counted
	#ifdef	USE_CUDAJPEG
	if (InterlockedIncrement(&cudaJpegRefCnt) == 1)
		cudaJpegInit(true);
	#endif
	}	// libJpeg

libJpeg :: ~libJpeg ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for the object
	//
	////////////////////////////////////////////////////////////////////////

	#ifdef	USE_GPUJPEG
//	if (pcgpu != NULL)
//		{
//		gpujpeg_encoder_destroy((gpujpeg_encoder *)pcgpu);
//		pcgpu = NULL;
//		}	// if
//	if (pdgpu != NULL)
//		{
//		gpujpeg_decoder_destroy((gpujpeg_decoder *)pdgpu);
//		pdgpu = NULL;
//		}	// if
	#endif

	// Reference counted
	#ifdef	USE_CUDAJPEG
	if (InterlockedDecrement(&cudaJpegRefCnt) == 0)
		cudaJpegInit(false);
	#endif
	}	// ~libJpeg

HRESULT libJpeg :: compress ( IDictionary *pDct )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Compresses an uncompressed image 'in-place'.  The results
	//			replace the dictionary state.
	//
	//	PARAMETERS
	//		-	pDct contains the image information.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr				= S_OK;
	IMemoryMapped	*pBitsSrc	= NULL;
	IMemoryMapped	*pBitsDst	= NULL;
	int				iCs			= JCS_GRAYSCALE;
	adtString		strFmt;
	adtIUnknown		unkV;
	adtInt			iWidth,iHeight,iBpp;
	ImageDct			dctImage;

	// Image parameters
	CCLTRY ( dctImage.lock ( pDct ) );

	// Determine input color space for incoming format
	if (hr == S_OK)
		{
		switch (dctImage.iFmt)
			{
			case IMGFMT_U8 :
			case IMGFMT_S8 :
			case IMGFMT_U16 :
			case IMGFMT_S16 :
				iCs = JCS_GRAYSCALE;
				break;
			case IMGFMT_R8G8B8 :
				iCs = JCS_EXT_RGB;
				break;
			case IMGFMT_B8G8R8 :
				iCs = JCS_EXT_BGR;
				break;
			case IMGFMT_A8R8G8B8 :
				iCs = JCS_EXT_ARGB;
				break;
			case IMGFMT_A8B8G8R8 :
				iCs = JCS_EXT_ABGR;
				break;
			case IMGFMT_R8G8B8A8 :
				iCs = JCS_EXT_RGBA;
				break;
			case IMGFMT_B8G8R8A8 :
				iCs = JCS_EXT_BGRA;
				break;
			default :
				hr = E_NOTIMPL;
			}	// switch
		}	// if

	// Create destination memory
	CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pBitsDst ) );

	// Compress
	if (hr == S_OK)
		{
		adtDate dNow,dThen;

		// Debug
		dThen.now();

		// Use JPEG compression on GPU (Make optional/node property) ?
		#ifdef	USE_CUDAJPEG
		hr = cudaCompress (	dctImage.iW, dctImage.iH, dctImage.iBpp, dctImage.iCh, iCs, iQ,
									dctImage.pBits, pBitsDst );

		// If for some reason the library does not support compression of image use fallback
		if (hr != S_OK)
		#endif

		// Default compression
		hr = compress (	dctImage.iW, dctImage.iH, dctImage.iBpp, dctImage.iCh, iCs,
								dctImage.pvBits, pBitsDst );

		// Debug
		dNow.now();
//		lprintf ( LOG_INFO, L"Time to compress %g s (hr 0x%x)\r\n", (dNow-dThen)*SECINDAY, hr );
		}	// if

	// New format
	CCLTRY ( pDct->store ( adtString(L"Format"), adtString(L"JPG") ) );

	// Replace bits
	CCLTRY (	pDct->store ( adtString(L"Bits"), adtIUnknown(pBitsDst) ));

	// Clean up
	_RELEASE(pBitsDst);
	_RELEASE(pBitsSrc);

	return hr;
	}	// compress

HRESULT libJpeg :: compress ( U32 width, U32 height, U32 bpp, U32 ch, U32 cs,
										void *pvBitsSrc, IMemoryMapped *pBitsDst )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Compresses an uncompressed image.
	//
	//	PARAMETERS
	//		-	width,height are the dimensions of the image
	//		-	bpp is the bits per pixel.
	//		-	ch is the number of input color channels (1 for grayscale, etc)
	//		-	cs is the color space (JCS_XXX)
	//		-	pvBitsSrc is the source bit stream
	//		-	pBitsDst is the destination bit stream
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT								hr				= S_OK;
	U8										*pcBitsSrc	= (U8 *)pvBitsSrc;
	U8										*pcRowBits	= NULL;
	U32									szRow			= 0;
	U32									i				= (bpp == 8) ? 0 : 1;
	struct jpeg_compress_struct	cmp;
	struct jpeg_destination_mgr	dst;
	struct jpeg_error_mgr			es;
	JSAMPROW								row_ptr[1];
	CTX_JPEG								ctx;

	// Compression structure
	cmp.err			= jpeg_std_error(&es);
	es.error_exit	= jpeg_error_exit;
	jpeg_create_compress(&cmp);

	// Compressor setup
	cmp.input_components		= (bpp == 8) ? 1 : 3;
	cmp.in_color_space		= (ch == 1) ? JCS_GRAYSCALE : JCS_RGB;
	jpeg_set_defaults ( &cmp );

	// Destination manager
	dst.init_destination		= jpeg_init_destination;
	dst.empty_output_buffer	= jpeg_empty_output_buffer;
	dst.term_destination		= jpeg_term_destination;
	cmp.dest						= &dst;
	jpeg_set_quality ( &cmp, iQ, true );

	// Destination bits
	memset ( &ctx, 0, sizeof(ctx) );
	ctx.pBitsDst		= pBitsDst;
	ctx.hr				= S_OK;
	cmp.client_data	= &ctx;

	// Image parameters
	cmp.image_width	= width;
	cmp.image_height	= height;

	// Initialize the destination manager.
	dst.next_output_byte	= NULL;
	dst.free_in_buffer	= 0;
	szRow						= width*(bpp/8);
	pcRowBits				= pcBitsSrc;
	cmp.input_components	= ch;
	cmp.in_color_space	= (J_COLOR_SPACE)cs;

	// Very ugly, because of the way the JPEG library works we cannot return
	// after an 'jpeg_error_exit'.
	if (hr == S_OK)
		{
		// Install handler
		int sj = setjmp ( (ctx.ljenv) );
		switch ( sj )
			{
			// Zero just means environment saved, continue processing
			case 0 :

				// Compression
				CCLOK		( jpeg_start_compress ( &cmp, TRUE ); )
				CCLTRYE	( (ctx.hr == S_OK), hr );
				while (hr == S_OK && cmp.next_scanline < cmp.image_height)
					{
					// Compress next row
					CCLOK ( row_ptr[0] = pcRowBits; )
					CCLOK ( jpeg_write_scanlines ( &cmp, row_ptr, 1 ); )

					// Next row
					CCLTRYE	( (ctx.hr == S_OK), hr );
					CCLOK		( pcRowBits += szRow; )
					}	// while
				CCLOK ( ctx.bFinish = true; )
				CCLOK ( jpeg_finish_compress ( &cmp ); )

				// Set the final size of the compressed image
//				lprintf ( LOG_INFO, L"Final size : %d", ctx.szDst );
				CCLTRY ( ctx.pBitsDst->setSize ( ctx.szDst ) );

				// Perform long jump ourselves to clean up 'setjmp'
				longjmp ( (ctx.ljenv), 1 );
				break;

			// One means success, done
			case 1 :
				hr = S_OK;
				break;

			// Anything else is an error code
			default :
				hr = sj;
				break;
			}	// switch
		}	// if

	// Clean up
	jpeg_destroy_compress(&cmp);

	return hr;
	}	// compress

HRESULT libJpeg :: construct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		CCLObject
	//
	//	PURPOSE
	//		-	Called when the object is being created.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// GPUJPEG library
	#ifdef	USE_GPUJPEG

	// One-time initialization TODO: Support for other device numbers
	int ret = gpujpeg_init_device(0,GPUJPEG_VERBOSE);
//	gpujpeg_init_device(0,GPUJPEG_VERBOSE);
	if (ret != 0)
		lprintf ( LOG_ERR, L"gpujpeg_init_device %d", ret );

	// Default parameters
//	struct gpujpeg_parameters param;
//	gpujpeg_set_default_parameters(&param);

	// Default image parameters
//	struct gpujpeg_image_parameters param_image;
//	gpujpeg_image_set_default_parameters(&param_image);

	// Create encoder/decoder structures
//	pcgpu = (U8 *) gpujpeg_encoder_create(&param,&param_image);
//	pdgpu = (U8 *) gpujpeg_decoder_create();
	#endif

	return hr;
	}	// construct

HRESULT libJpeg :: decompress ( IDictionary *pDct )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decompresses a compressed image 'in-place'.  The results
	//			replace the dictionary state.
	//
	//	PARAMETERS
	//		-	pDct contain/will receive the image information.  Assumes
	//			'Bits' and 'Format' properties are valid and contains 
	//			compressed image.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr				= S_OK;
	IMemoryMapped	*pBitsSrc	= NULL;
	IMemoryMapped	*pBitsDst	= NULL;
	adtString		strFmt;
	adtIUnknown		unkV;
	adtValue			vL;
	U32				w,h,bpp,ch;

	// State check
	CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

	// Access source bits
	CCLTRY ( pDct->load ( adtString(L"Bits"), vL ) );
	CCLTRY ( _QISAFE((unkV=vL),IID_IMemoryMapped,&pBitsSrc) );

	// Validate format
	CCLTRY ( pDct->load ( adtString(L"Format"), vL ) );
	CCLTRYE( (strFmt = vL).length() > 0, E_UNEXPECTED );
	CCLTRYE( (!WCASECMP(strFmt,L"JPEG") || !WCASECMP(strFmt,L"JPG")), E_NOTIMPL );

	// Create destination memory
	CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pBitsDst ) );

	// Decompress
	if (hr == S_OK)
		{
		adtDate dNow,dThen;

		// Debug
		dThen.now();

		// Use JPEG compression on GPU (Make optional/node property) ?
		#ifdef	USE_CUDAJPEG
		hr = cudaDecompress ( pBitsSrc, pBitsDst, &w, &h, &bpp, &ch );

		// If for some reason the library does not support compression of image use fallback
		if (hr != S_OK)
		#endif

		// Compress
		hr = decompress (	pBitsSrc, pBitsDst, &w, &h, &bpp, &ch );

		// Debug
		dNow.now();
//		lprintf ( LOG_INFO, L"Time to decompress %g s (hr 0x%x)\r\n", (dNow-dThen)*SECINDAY, hr );
		}	// if

	// New format (Assumption!)
	if (hr == S_OK)
		{
		if (bpp == 8 && ch == 1)
			hr = pDct->store ( adtString(L"Format"), adtString(L"U8") );
		else if (bpp == 16 && ch == 1)
			hr = pDct->store ( adtString(L"Format"), adtString(L"U16") );

		// Decompression below is configured to output BGR (if extensions are available)
		else if (bpp == 24 && ch == 3)
			{
			#ifdef JCS_EXTENSIONS
			hr = pDct->store ( adtString(L"Format"), adtString(L"B8G8R8") );
			#else
			hr = pDct->store ( adtString(L"Format"), adtString(L"R8G8B8") );
			#endif
			}	// else if
		else
			hr = E_UNEXPECTED;
		}	// if

	// Image information
	CCLTRY ( pDct->store(adtString(L"Width"),adtInt(w)) );
	CCLTRY ( pDct->store(adtString(L"Height"),adtInt(h)) );
	CCLTRY (	pDct->store ( adtString(L"Bits"), adtIUnknown(pBitsDst) ));

	// Clean up
	_RELEASE(pBitsDst);
	_RELEASE(pBitsSrc);

	return hr;
	}	// decompress

HRESULT libJpeg :: decompress (	IMemoryMapped *pBitsSrc,
											IMemoryMapped *pBitsDst,
											U32 *width, U32 *height, U32 *bpp, U32 *ch )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decompresses an compressed image.
	//
	//	PARAMETERS
	//		-	pBitsSrc is the source bit stream
	//		-	pBitsDst is the destination bit stream
	//		-	width,height,bpp,ch will receive the image parameters
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT								hr				= S_OK;
	U8										*pcBitsSrc	= NULL;
	U8										*pcBitsDst	= NULL;
	U8										*pcRowBits	= NULL;
	U32									szRow			= 0;
	JSAMPROW								row_ptr[1];
	CTX_JPEG								ctx;
	U32									idx,sz;
	struct jpeg_error_mgr			es;
	struct jpeg_decompress_struct ds;
	struct jpeg_source_mgr			ss;

	// Initialize decompression
	ds.err			= jpeg_std_error(&es);
	es.error_exit	= jpeg_error_exit;
	jpeg_create_decompress(&ds);

	// Source manager
	ss.init_source			= jpeg_init_source;
	ss.fill_input_buffer	= jpeg_fill_input_buffer;
	ss.skip_input_data	= jpeg_skip_input_data;
	ss.resync_to_restart	= jpeg_resync_to_restart;
	ss.term_source			= jpeg_term_source;
	ds.src = &ss;

	// Access and size source bits
	CCLTRY( pBitsSrc->getInfo ( (void **) &pcBitsSrc, &sz ) );

	// Initialize the source manager
	CCLOK ( ss.next_input_byte = (JOCTET *) pcBitsSrc; )
	CCLOK ( ss.bytes_in_buffer	= sz; )

	// Context for own routines
	CCLOK ( memset ( &ctx, 0, sizeof(ctx) ); )
	CCLOK ( ctx.hr				= S_OK; )
	CCLOK ( ds.client_data	= &ctx; )

	// Very ugly, because of the way the JPEG library works we cannot return
	// after an 'jpeg_error_exit'.
	if (hr == S_OK)
		{
		// Install handler
		int sj = setjmp ( (ctx.ljenv) );
		switch ( sj )
			{
			// Zero just means environment saved, continue processing
			case 0 :
				// Header
				CCLOK		( jpeg_read_header ( &ds, TRUE ); )

				// Image information
				CCLOK		( *width		= ds.image_width; )
				CCLOK		( *height	= ds.image_height; )
				CCLOK		( *bpp		= ds.num_components*8; )	// Correct ?
				CCLOK		( *ch			= ds.num_components; )		// Correct ?
				//lprintf ( LOG_DBG, L"W %d H %d Bpp %d Ch %d\r\n",
				//				*width, *height, *bpp, *ch );

				// Output to BGR type formats for consistency across library
				// if extended color spaces are supported (libjpeg-turbo)
				#ifdef JCS_EXTENSIONS
				switch (ds.out_color_space)
					{
					// RGB -> BGR
					case JCS_RGB :
						ds.out_color_space = JCS_EXT_BGR;
						break;
					}	// switch
				#endif

				// Select an appropriate 
				// Size and access destination bits.
				CCLOK		( szRow = (*width)*((*bpp)/8); )
				CCLTRY	( pBitsDst->setSize ( (*height)*szRow ) );
				CCLTRY	( pBitsDst->getInfo ( (void **) &pcBitsDst, NULL ) );
				CCLOK		( pcRowBits			= pcBitsDst; )

				// Decompress each row
				CCLOK		( jpeg_start_decompress ( &ds ); )
				CCLTRYE	( (ctx.hr == S_OK), hr );
				for (idx = 0;hr == S_OK && idx < ds.image_height;++idx)
					{
					// Decompress next row
					CCLOK ( row_ptr[0] = pcRowBits; )
					if (hr == S_OK && jpeg_read_scanlines ( &ds, row_ptr, 1 ) != 1)
						{
						lprintf ( LOG_DBG, L"jpeg_read_scanlines failed at index %d\r\n", idx );
						hr = E_UNEXPECTED;
						}	// if

					// Next row
					CCLTRYE	( (ctx.hr == S_OK), hr );
					CCLOK		( pcRowBits += szRow; )
					}	// for
				CCLOK ( ctx.bFinish = true; )
				CCLOK ( jpeg_finish_decompress ( &ds ); )

				// Perform long jump ourselves to clean up 'setjmp'
				longjmp ( (ctx.ljenv), 1 );
				break;

			// One means success, done
			case 1 :
				hr = S_OK;
				break;

			// Two means success with caveat, still need to clean up
			case 2 :
				jpeg_abort_decompress ( &ds );
				break;

			// Anything else is an error code
			default :
				jpeg_abort_decompress ( &ds );
				hr = sj;
				break;
			}	// switch
		}	// if

	// Clean up
	jpeg_destroy_decompress ( &ds );

	return hr;
	}	// decompress

//
// JPEG support routines
//

boolean jpeg_empty_output_buffer ( j_compress_ptr pc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the destination buffer fills up.
	//
	//	PARAMETERS
	//		-	pc is the compression information
	//
	//	RETURN VALUE
	//		TRUE if successsful
	//
	////////////////////////////////////////////////////////////////////////
	CTX_JPEG				*ctx	= (CTX_JPEG *) (pc->client_data);

	// Current block
	if (ctx->hr == S_OK)
		ctx->pvBitsDst = NULL;

	// Another block has been used
	if (ctx->hr == S_OK) ctx->szDst += JCOMPRESS_BLKSIZE;

	// Resize for the next block
	if (ctx->hr == S_OK)
		ctx->hr = ctx->pBitsDst->setSize ( ctx->szDst + JCOMPRESS_BLKSIZE );

	// Acquire ptr. to next block
	if (ctx->hr == S_OK)
		{
		U8 *pcBitsDst = NULL;

		// Information about block
		ctx->hr = ctx->pBitsDst->getInfo( (void **) &pcBitsDst, NULL );
		

		// Ptr. into block
		if (ctx->hr == S_OK)
			ctx->pvBitsDst = &pcBitsDst[ctx->szDst];
		}	// if

	// Reset JPEG info.
	pc->dest->next_output_byte	= (JOCTET *) ctx->pvBitsDst;
	pc->dest->free_in_buffer	= JCOMPRESS_BLKSIZE;

	return (ctx->hr == S_OK);
	}	// jpeg_empty_output_buffer

void jpeg_error_exit ( j_common_ptr pc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when a fatal error is detected by the library.
	//
	//	PARAMETERS
	//		-	pc is the compression information
	//
	////////////////////////////////////////////////////////////////////////
	CTX_JPEG		*ctx	= (CTX_JPEG *) (pc->client_data);

	// This is to handle JPEG images(streams) that do not terminate 
	// propertly.  Only ignore errors during 'jpeg_finish_compress' since
	// theoretically the image is valid at this point.
	if (!ctx->bFinish)
		{
		char			buffer[JMSG_LENGTH_MAX];

		// Error message
		pc->err->format_message ( pc, buffer );
		lprintf ( LOG_ERR, L"%S", buffer );
		}	// if

	// Exit, handle garbage at end of file during 'finish'
	longjmp ( ctx->ljenv, (ctx->bFinish) ? 2 : E_UNEXPECTED );

	}	// jpeg_error_exit

boolean jpeg_fill_input_buffer ( j_decompress_ptr pd )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when library runs out of source data.
	//
	//	PARAMETERS
	//		-	pd is the decompression information
	//
	//	RETURN VALUE
	//		Nonzero if successful
	//
	////////////////////////////////////////////////////////////////////////
	// All data is available at start, error if this function called
	return 0;
	}	// jpeg_fill_input_buffer

void jpeg_init_destination ( j_compress_ptr pc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called by jpeg_start_compress to initialize the destination
	//			buffers.
	//
	//	PARAMETERS
	//		-	pc is the compression information
	//
	////////////////////////////////////////////////////////////////////////
	CTX_JPEG				*ctx	= (CTX_JPEG *) (pc->client_data);

	// Pre-size and access destination bits/context
	if (ctx->hr == S_OK)
		ctx->hr = ctx->pBitsDst->setSize ( JCOMPRESS_BLKSIZE );
	if (ctx->hr == S_OK)
		ctx->hr = ctx->pBitsDst->getInfo ( &(ctx->pvBitsDst), NULL );
	if (ctx->hr == S_OK)
		ctx->szDst = 0;

	// Available output
	pc->dest->next_output_byte	= (JOCTET *) ctx->pvBitsDst;
	pc->dest->free_in_buffer	= JCOMPRESS_BLKSIZE;

	}	// jpeg_init_destination

void jpeg_init_source ( j_decompress_ptr pd )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called by jpeg_start_decompress to initialize the source buffers
	//
	//	PARAMETERS
	//		-	pd is the decompression information
	//
	////////////////////////////////////////////////////////////////////////
	}	// jpeg_init_source

void jpeg_skip_input_data ( j_decompress_ptr pd, long nskip )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to skip over some of the input data.
	//
	//	PARAMETERS
	//		-	pd is the decompression information
	//		-	nskip is the # of bytes to skip
	//
	////////////////////////////////////////////////////////////////////////
	}	// jpeg_skip_input_data

void jpeg_term_destination ( j_compress_ptr pc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called by jpeg_finish_compress to flush buffers.
	//
	//	PARAMETERS
	//		-	pc is the compression information
	//
	////////////////////////////////////////////////////////////////////////
	CTX_JPEG				*ctx	= (CTX_JPEG *) (pc->client_data);

	// Unlock current block
	if (ctx->hr == S_OK)
		ctx->pvBitsDst = NULL;

	// Part of another block has been used
	if (ctx->hr == S_OK)
		ctx->szDst += (U32)(JCOMPRESS_BLKSIZE-pc->dest->free_in_buffer);

	}	// jpeg_term_destination

void jpeg_term_source ( j_decompress_ptr pd )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called by jpeg_finish_decompress to flush buffers.
	//
	//	PARAMETERS
	//		-	pd is the decompression information
	//
	////////////////////////////////////////////////////////////////////////
	}	// jpeg_term_source

