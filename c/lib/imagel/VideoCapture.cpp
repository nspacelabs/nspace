////////////////////////////////////////////////////////////////////////
//
//									VideoCapture.CPP
//
//				Implementation of the video capture node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

VideoCapture :: VideoCapture ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg	= NULL;
	bOpen = false;
	adtValue::copy ( adtInt(0), vSrc );
	iW		= 0;
	iH		= 0;
	iFps	= 0;
	}	// VideoCapture

HRESULT VideoCapture :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Source"), vL ) == S_OK)
			adtValue::copy(vL,vSrc);
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			iW = vL;
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			iH = vL;
		if (pnDesc->load ( adtString(L"FPS"), vL ) == S_OK)
			iFps = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		if (bOpen)
			{
			bOpen = false;
			vc.release();
			}	// if
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT VideoCapture :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Grab video frame
	if (_RCP(Fire))
		{
		cvMatRef	*pMat	= NULL;

		// State check
		CCLTRYE ( bOpen == true && pImg != NULL, ERROR_INVALID_STATE );

		// Create a matrix reference object for the image
		CCLTRYE( (pMat = new cvMatRef()) != NULL, E_OUTOFMEMORY );
		CCLTRYE ( (pMat->mat = new cv::Mat()) != NULL, E_OUTOFMEMORY );

		// This is so awful, there is no way to avoid the buffering
		// OpenCV does with images so all of the images must 'grabbed' before
		// the latest one can be retrieved to ensure the latest image is obtained.
		if (hr == S_OK)
			{
			adtDate dNow,dThen;

			// Continue until grabbed or error
			while (hr == S_OK)
				{
				// Pre-grab
				dThen.now();

				// Grab a frame
				if (!vc.grab())
					hr = E_UNEXPECTED;

				// Post-grab
				dNow.now();

				// If non-zero time, frame is valid
//				lprintf ( LOG_DBG, L"Dt %g\r\n", ((dNow-dThen)*MSECINDAY) );
				if ((dNow-dThen)*MSECINDAY > 2)
					break;
				}	// while

			// Basic idea is to grab buffer until it takes a non-zero amount of time,
			// too quick and it is assumed 'grab' is just grabbing an old image
			// from the buffered list.

			// Grab maximum number of frames.  Apparently calling 'grab' is faster
			// than actually trying to retrieve the frame data.  The rumor seems to
			// be that the OpenCV buffer is 5 frames deep.

			// NOTE: The problem with this approach is if there are no images buffered
			// then the next acqusition takes 5 frames.
//			for (int dly = 0;dly < 5;++dly)

			// Just issue grab for next image
//			vc.grab();
			}	// if

		// Retrieve data for tha latest frame
		CCLTRYE ( vc.retrieve(*(pMat->mat)) == true, E_UNEXPECTED );

		// Result
		CCLTRY(pImg->store ( adtString(L"cvMatRef"), adtIUnknown(*pMat) ) );
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImg));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		}	// if

	// Open
	else if (_RCP(Open))
		{
		// State check
		CCLTRYE ( bOpen == false, ERROR_INVALID_STATE );

		// Open
		if (hr == S_OK)
			{
			try
				{
				// Open device/filename
				if (adtValue::type(vSrc) == VTYPE_I4)
					vc.open(adtInt(vSrc));
				else if (adtValue::type(vSrc) == VTYPE_STR)
					{
					char			*pcSrc = NULL;
					adtString	strSrc(vSrc);

					// Access file
					CCLTRYE(strSrc.length() > 0,E_INVALIDARG);
					CCLTRY (strSrc.toAscii(&pcSrc) );
					CCLOK(vc.open(pcSrc);)

					// Clean up
					_FREEMEM(pcSrc);
					}	// else if
				else
					hr = E_INVALIDARG;

				// Valid device ?
				if (hr == S_OK)
					hr = vc.isOpened() ? S_OK : E_UNEXPECTED;

				// Issue setup
				setup();
				}	// try
			catch ( cv::Exception &ex )
				{
				lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = E_UNEXPECTED;
				}	// catch
			}	// if

		// Result
		if (hr == S_OK)
			{
			bOpen = true;
			_EMT(Open,adtBool(bOpen));
			}	// if
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Close
	else if (_RCP(Close))
		{
		// Shutdown
		if (bOpen)
			{
			bOpen = false;
			vc.release();
			}	// if

		// Result ?
//		_EMT(Open,adtBool(false));
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Source))
		adtValue::copy(v,vSrc);
	else if (_RCP(Width))
		{
		iW = v;
		setup();
		}	// else if
	else if (_RCP(Height))
		{
		iH = v;
		setup();
		}	// else if
	else if (_RCP(Fps))
		{
		iFps = v;
		setup();
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

void VideoCapture :: setup ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Setup video capture device with current parameters.
	//
	////////////////////////////////////////////////////////////////////////

	// Peform setup, need to close ?
	if (iW != 0)
		vc.set(cv::CAP_PROP_FRAME_WIDTH,iW);
	if (iH != 0)
		vc.set(cv::CAP_PROP_FRAME_HEIGHT,iH);
	if (iFps != 0)
		vc.set(cv::CAP_PROP_FPS,iFps);
	#if		(CV_VERSION_MAJOR == 2) || (CV_VERSION_MAJOR == 3)
	vc.set(CV_CAP_PROP_BUFFERSIZE,2);
	#endif
	}	// setup
