////////////////////////////////////////////////////////////////////////
//
//									MOMENTS.CPP
//
//				Implementation of the image moments node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Moments :: Moments ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctRes	= NULL;
	pImg		= NULL;
	}	// Moments

HRESULT Moments :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Create container for results
		CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDctRes ) );

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		_RELEASE(pDctRes);
//		_RELEASE(pKer);
		}	// else

	return hr;
	}	// onAttach

HRESULT Moments :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Calculate
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		cv::Moments	m;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// OpenCV exception handling
		try
			{
			// Calculate
			if (pMat->isMat())
				m = cv::moments ( *(pMat->mat) );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				m = cv::moments ( *(pMat->umat) );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				m = cv::moments ( *(pMat->gpumat) );
			#endif
			else
				hr = E_NOTIMPL;
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = S_FALSE;
			}	// catch

		// Transfer results to dictionary
		if (hr == S_OK)
			{
			// Spatial moments
			CCLTRY ( pDctRes->store ( adtString(L"m00"), adtDouble(m.m00) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m10"), adtDouble(m.m10) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m01"), adtDouble(m.m01) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m20"), adtDouble(m.m20) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m11"), adtDouble(m.m11) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m02"), adtDouble(m.m02) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m30"), adtDouble(m.m30) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m21"), adtDouble(m.m21) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m12"), adtDouble(m.m12) ) );
			CCLTRY ( pDctRes->store ( adtString(L"m03"), adtDouble(m.m03) ) );

			// Central moments
			CCLTRY ( pDctRes->store ( adtString(L"mu20"), adtDouble(m.mu20) ) );
			CCLTRY ( pDctRes->store ( adtString(L"mu11"), adtDouble(m.mu11) ) );
			CCLTRY ( pDctRes->store ( adtString(L"mu02"), adtDouble(m.mu02) ) );
			CCLTRY ( pDctRes->store ( adtString(L"mu30"), adtDouble(m.mu30) ) );
			CCLTRY ( pDctRes->store ( adtString(L"mu21"), adtDouble(m.mu21) ) );
			CCLTRY ( pDctRes->store ( adtString(L"mu12"), adtDouble(m.mu12) ) );
			CCLTRY ( pDctRes->store ( adtString(L"mu03"), adtDouble(m.mu03) ) );

			// Central normalized moments
			CCLTRY ( pDctRes->store ( adtString(L"nu20"), adtDouble(m.nu20) ) );
			CCLTRY ( pDctRes->store ( adtString(L"nu11"), adtDouble(m.nu11) ) );
			CCLTRY ( pDctRes->store ( adtString(L"nu02"), adtDouble(m.nu02) ) );
			CCLTRY ( pDctRes->store ( adtString(L"nu30"), adtDouble(m.nu30) ) );
			CCLTRY ( pDctRes->store ( adtString(L"nu21"), adtDouble(m.nu21) ) );
			CCLTRY ( pDctRes->store ( adtString(L"nu12"), adtDouble(m.nu12) ) );
			CCLTRY ( pDctRes->store ( adtString(L"nu03"), adtDouble(m.nu03) ) );
			}	// if

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDctRes));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

