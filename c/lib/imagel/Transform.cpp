////////////////////////////////////////////////////////////////////////
//
//									TOMOGRAPHY.CPP
//
//				Implementation of the image tomography node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Transform :: Transform ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg		= NULL;
	dScl[0]	= dScl[1]	= 1;
	dTrns[0] = dTrns[1]	= 0;
	dRot[0]	= dRot[1]	= 0;
	}	// Transform

HRESULT Transform :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"TranslateX"), vL ) == S_OK)
			onReceive(prTranslateX,vL);
		if (pnDesc->load ( adtString(L"TranslateY"), vL ) == S_OK)
			onReceive(prTranslateY,vL);
		if (pnDesc->load ( adtString(L"ScaleX"), vL ) == S_OK)
			onReceive(prScaleX,vL);
		if (pnDesc->load ( adtString(L"ScaleY"), vL ) == S_OK)
			onReceive(prScaleY,vL);
		if (pnDesc->load ( adtString(L"Radius"), vL ) == S_OK)
			onReceive(prRadius,vL);
		if (pnDesc->load ( adtString(L"Inverse"), vL ) == S_OK)
			bInv = vL;
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Transform :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Apply transforms
	if (_RCP(Fire) || _RCP(Polar))
		{
		IDictionary	*pImgUse		= NULL;
		cvMatRef		*pMat			= NULL;
		cv::Point2f	ptSrc[3],ptDst[3];

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Transforms (TODO: Rotation,scaling)
		if (hr == S_OK)
			{
			// From point
			ptSrc[0] = ptDst[0] = cv::Point2f(0,0);
			ptSrc[1] = ptDst[1] = cv::Point2f(10,0);
			ptSrc[2] = ptDst[2] = cv::Point2f(0,10);

			// Offset points by specified amount
			ptDst[0].x += (float)dTrns[0];
			ptDst[0].y += (float)dTrns[1];
			ptDst[1].x += (float)dTrns[0];
			ptDst[1].y += (float)dTrns[1];
			ptDst[2].x += (float)dTrns[0];
			ptDst[2].y += (float)dTrns[1];
			}	// if

		// OpenCV uses exceptions
		try
			{
			// Execute
			if (hr == S_OK && pMat->isMat())
				{
				cv::Mat matT,matDst;

				// Apply
				if (_RCP(Polar))
					{
					// warpPolar seems to be introduced in 3.4.2
					#if	( \
								((CV_VERSION_MAJOR == 3) && (CV_VERSION_MINOR == 4) && (CV_VERSION_REVISION >= 2)) || \
								((CV_VERSION_MAJOR == 3) && (CV_VERSION_MINOR > 4)) || \
								((CV_VERSION_MAJOR > 3)) )
					double	radius	= vR;
					int		flags		= cv::WARP_POLAR_LINEAR|cv::WARP_FILL_OUTLIERS;

					// Use scaling for size
					cv::Size	sz((int)dScl[0],(int)dScl[1]);

					// Use translation for center
					cv::Point2f	pt((float)dTrns[0],(float)dTrns[1]);

					// Inverse ?
					if (bInv) flags |= cv::WARP_INVERSE_MAP;

					// Transform
					cv::warpPolar ( *(pMat->mat), matDst, sz, pt, radius, flags );
					#else
					hr = E_NOTIMPL;
					#endif
					}	// if
				else
					{
					// Transform to use
					matT = cv::getAffineTransform(ptSrc,ptDst);

					// Transform
					cv::warpAffine ( *(pMat->mat), matDst, matT, pMat->mat->size() );
					}	// else

				// Copy back to source
				matDst.copyTo(*(pMat->mat));
				}	// if
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				{
				cv::UMat matDst;
				cv::Mat	matT;

				// Transform
				matT = cv::getAffineTransform(ptSrc,ptDst);

				// Apply
				cv::warpAffine ( *(pMat->umat), matDst, matT, pMat->umat->size() );

				// Copy back to source
				matDst.copyTo(*(pMat->umat));
				}	// else if
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				{
				// TODO: CHECK THIS!
				cv::cuda::GpuMat	matDst;
				cv::Mat				matT;

				// Transform
				matT = cv::getAffineTransform(ptSrc,ptDst);

				// Apply
				cv::warpAffine ( *(pMat->gpumat), matDst, matT, pMat->gpumat->size() );

				// Copy back to source
				matDst.copyTo(*(pMat->gpumat));
				}	// if
			#endif
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = S_FALSE;
			}	// catch

		// Result
		if (hr == S_OK)
			{
			if (_RCP(Polar))
				_EMT(Polar,adtIUnknown(pImgUse));
			else
				_EMT(Fire,adtIUnknown(pImgUse));
			}	// if
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// Scale/Rotate/Translate
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(ScaleX))
		dScl[0] = (vD=v);
	else if (_RCP(ScaleY))
		dScl[1] = (vD=v);
	else if (_RCP(RotateX))
		dRot[0] = (vD=v);
	else if (_RCP(RotateY))
		dRot[1] = (vD=v);
	else if (_RCP(TranslateX))
		dTrns[0] = (vD=v);
	else if (_RCP(TranslateY))
		dTrns[1] = (vD=v);
	else if (_RCP(Radius))
		vR = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive


