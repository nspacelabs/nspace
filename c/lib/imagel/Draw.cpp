////////////////////////////////////////////////////////////////////////
//
//									Draw.CPP
//
//				Implementation of the draw shape node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Draw :: Draw ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg		= NULL;
	fR			= fB = fG = 0.0f;
	fA			= 1.0f;
	fX0		= fY0 = fX1 = fY1 = 0.0f;
	iThick	= 1;
	fRad		= 1.0f;
	fAn		= 0.0f;
	fAf		= 0.0f;
	fAt		= 0.0f;
	strShp	= L"Line";
	}	// Draw

HRESULT Draw :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Shape"), vL ) == S_OK)
			adtValue::toString ( vL, strShp );
		if (pnDesc->load ( adtString(L"Red"), vL ) == S_OK)
			fR = vL;
		if (pnDesc->load ( adtString(L"Green"), vL ) == S_OK)
			fG = vL;
		if (pnDesc->load ( adtString(L"Blue"), vL ) == S_OK)
			fB = vL;
		if (pnDesc->load ( adtString(L"Color"), vL ) == S_OK)
			onReceive ( prColor, vL );
		if (pnDesc->load ( adtString(L"Intensity"), vL ) == S_OK)
			onReceive ( prIntensity, vL );
		if (pnDesc->load ( adtString(L"Thickness"), vL ) == S_OK)
			iThick = vL;
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			fW = vL;
		if (pnDesc->load ( adtString(L"Radius"), vL ) == S_OK)
			fRad = vL;
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			fH = vL;
		if (pnDesc->load ( adtString(L"Angle"), vL ) == S_OK)
			fAn = vL;
		if (pnDesc->load ( adtString(L"AngleFrom"), vL ) == S_OK)
			fAf = vL;
		if (pnDesc->load ( adtString(L"AngleTo"), vL ) == S_OK)
			fAt = vL;
		if (pnDesc->load ( adtString(L"X0"), vL ) == S_OK)
			fX0 = vL;
		if (pnDesc->load ( adtString(L"Y0"), vL ) == S_OK)
			fY0 = vL;
		if (pnDesc->load ( adtString(L"X1"), vL ) == S_OK)
			fX1 = vL;
		if (pnDesc->load ( adtString(L"Y1"), vL ) == S_OK)
			fY1 = vL;
		if (pnDesc->load ( adtString(L"Percent"), vL ) == S_OK)
			bPerc = vL;
		if (pnDesc->load ( adtString(L"Value"), vL ) == S_OK)
			onReceive ( prValue, vL );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Draw :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		int			t,thick	= ((t = iThick) > 0) ? (t = iThick) : 1;
		cv::Point2f	pt0 ( fX0, fY0 );
		cv::Point2f	pt1 ( fX1, fY1 );
		cv::Mat		matNoGpu;
		cv::Scalar	clr;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Color selection based on channels
		CCLOK ( clr =	(pMat->channels() == 4) ?	cv::Scalar(fB, fG, fR, fA) :
							(pMat->channels() == 3) ?	cv::Scalar(fB, fG, fR) :
							(pMat->channels() == 2) ?	cv::Scalar(fB, fG) :
																cv::Scalar(fB); )

		// No GPU support for drawing
		// Download the current image into a local matrix for processing
		#ifdef	HAVE_OPENCV_CUDA
		if (hr == S_OK && pMat->isGPU())
			pMat->gpumat->download ( matNoGpu );
		#endif

		// NOTE: Some drawing functions crash on thickness = 0 (line)

		// Percent coordinates ?
		if (hr == S_OK && bPerc)
			{
			pt0.x *= (pMat->cols()/100.0f);	pt1.x *= (pMat->cols()/100.0f);
			pt0.y *= (pMat->rows()/100.0f);	pt1.y *= (pMat->rows()/100.0f);
			}	// if

//		if (iThick == cv::FILLED)
//			lprintf ( LOG_DBG, L"Hi\r\n" );

		// Perform operation
		if (hr == S_OK && !WCASECMP(strShp,L"Line"))
			{
			if (pMat->isMat())
				cv::line ( *(pMat->mat), pt0, pt1, clr, thick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::line ( *(pMat->umat), pt0, pt1, clr, thick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::line ( matNoGpu, pt0, pt1, clr, thick );
			#endif
			}	// if
		else if (hr == S_OK && !WCASECMP(strShp,L"ArrowedLine"))
			{
			if (pMat->isMat())
				cv::arrowedLine ( *(pMat->mat), pt0, pt1, clr, thick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::arrowedLine ( *(pMat->umat), pt0, pt1, clr, thick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::arrowedLine ( matNoGpu, pt0, pt1, clr, thick );
			#endif
			}	// if
		else if (hr == S_OK && !WCASECMP(strShp,L"Ellipse"))
			{
			// Rotated rectangle for which to bound the ellipse
			cv::RotatedRect	rct ( pt0, cv::Size2f(fW,fH), fAn );

			// Perform draw
			if (pMat->isMat())
				cv::ellipse ( (*pMat->mat), rct, clr, thick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::ellipse ( (*pMat->umat), rct, clr, thick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::ellipse ( matNoGpu, rct, clr, thick );
			#endif
			}	// else if
		else if (hr == S_OK && !WCASECMP(strShp,L"Arc"))
			{
			// Perform draw
			if (pMat->isMat())
				cv::ellipse ( (*pMat->mat), pt0, cv::Size(pt1),
									fAn, fAf, fAt, clr, thick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::ellipse ( (*pMat->umat), pt0, cv::Size(pt1),
									fAn, fAf, fAt, clr, thick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::ellipse ( matNoGpu, pt0, cv::Size(pt1),
									fAn, fAf, fAt, clr, thick );
			#endif
			}	// else if
		else if (hr == S_OK && !WCASECMP(strShp,L"Circle"))
			{
			// Radius
			int
			iRad = (int) (	(fRad == -1)	?	-1 : 
								(bPerc)			?	(fRad * (pMat->cols()/100.0f)) :
														(float)fRad );

			// Draw
			if (pMat->isMat())
				cv::circle ( *(pMat->mat), pt0, iRad, clr, iThick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::circle ( *(pMat->umat), pt0, iRad, clr, iThick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::circle ( matNoGpu, pt0, (int)(float)fRad, clr, iThick );
			#endif
			}	// else if
		else if (hr == S_OK && !WCASECMP(strShp,L"Rectangle"))
			{
			if (pMat->isMat())
				cv::rectangle ( *(pMat->mat), pt0, pt1, clr, iThick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::rectangle ( *(pMat->umat), pt0, pt1, clr, iThick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::rectangle ( matNoGpu, pt0, pt1, clr, thick );
			#endif
			}	// else if
		else if (hr == S_OK && !WCASECMP(strShp,L"Text"))
			{
			char			*pcStr = NULL;
			adtString	strV;
			cv::String	str;

			// Convert to openCV string
			CCLTRY ( adtValue::toString ( vValue, strV ) );
			CCLTRY ( strV.toAscii(&pcStr) );
			CCLOK  ( str = pcStr; )

			// Draw text
			if (pMat->isMat())
				cv::putText ( *(pMat->mat), str, pt0, cv::FONT_HERSHEY_SIMPLEX, fW, clr, thick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::putText ( *(pMat->umat), str, pt0, cv::FONT_HERSHEY_SIMPLEX, fW, clr, thick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				cv::putText ( matNoGpu, str, pt0, cv::FONT_HERSHEY_SIMPLEX, fW, clr, thick );
			#endif

			// Clean up
			_FREEMEM(pcStr);
			}	// else if
		else if (hr == S_OK && !WCASECMP(strShp,L"PolyLines"))
			{
			IContainer						*pLst	= NULL;
			IIt								*pIt	= NULL;
			adtIUnknown						unkV(vValue);
//			std::vector<cv::Point2f>	pts;
			std::vector<cv::Point>		pts;
			adtValue							vL;

			// A list of values (X0,Y0,X1,Y1,etc) is required for the drawing
			CCLTRY ( _QISAFE(unkV,IID_IContainer,&pLst) );
			CCLTRY ( pLst->iterate ( &pIt ) );

			// Add the points to the list
			while (hr == S_OK && pIt->read ( vL ) == S_OK)
				{
				adtFloat fX(vL);

				// Y coordinate
				pIt->next();
				hr = pIt->read ( vL );
				pIt->next();

				// Add the point
				CCLOK ( pts.push_back ( cv::Point((int)fX,adtInt(vL)) ); )
				}	// while

			// Draw
			if (pMat->isMat())
				cv::polylines ( *(pMat->mat), pts, false, clr, thick );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				cv::polylines ( *(pMat->umat), pts, false, clr, thick );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			#error	TODO
			else if (pMat->isGPU())
				hr = E_NOTIMPL;
//				cv::line ( matNoGpu, pt0, pt1, clr, thick );
			#endif

			// Clean up
			_RELEASE(pIt);
			_RELEASE(pLst);
			}	// if

		// Upload changes back to GPU
		#ifdef	HAVE_OPENCV_CUDA
		if (hr == S_OK && pMat->isGPU())
			pMat->gpumat->upload ( matNoGpu );
		#endif

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Debug
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"%s : draw '%s' failed : 0x%x\r\n", (LPCWSTR)strnName, (LPCWSTR)strShp, hr );

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Color))
		{
		adtInt	iClr(v);

		// Alternative way to specify a color
		fB = (float)((iClr >> 0) & 0xff);
		fG = (float)((iClr >> 8) & 0xff);
		fR = (float)((iClr >> 16) & 0xff);
		fA = (float)((iClr >> 24) & 0xff);
		}	// if
	else if (_RCP(Intensity))
		{
		adtFloat	fClr(v);

		// Alternative way to specify a color
		fB = fClr;
		fG = fClr;
		fR = fClr;
		fA = 1.0f;
		}	// if
	else if (_RCP(X0))
		fX0 = v;
	else if (_RCP(X1))
		fX1 = v;
	else if (_RCP(Y0))
		fY0 = v;
	else if (_RCP(Y1))
		fY1 = v;
	else if (_RCP(Angle))
		fA = v;
	else if (_RCP(AngleFrom))
		fAf = v;
	else if (_RCP(AngleTo))
		fAt = v;
	else if (_RCP(Height))
		fH = v;
	else if (_RCP(Width))
		fW = v;
	else if (_RCP(Thick))
		iThick = v;
	else if (_RCP(Radius))
		fRad = v;
	else if (_RCP(Value))
		hr = adtValue::copy ( v, vValue );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

