////////////////////////////////////////////////////////////////////////
//
//									Channel.CPP
//
//				ImplementChannelion of the channel access image node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Channel :: Channel ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg	= NULL;
	pImgQ	= NULL;
	pItQ	= NULL;
	}	// Channel

HRESULT Channel :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Image reference queue and iterator
		CCLTRY ( COCREATE ( L"Adt.Queue", IID_IList, &pImgQ ) );
		CCLTRY ( pImgQ->iterate(&pItQ) );

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Rows"), vL ) == S_OK)
			iRows = vL;
		if (pnDesc->load ( adtString(L"Channels"), vL ) == S_OK)
			iChs = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pItQ);
		_RELEASE(pImgQ);
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Channel :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Split
	if (_RCP(Split))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		IThis			*pMatCh	= NULL;
		U32			nch		= 0;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Number of channels in source image
		CCLTRYE ( (nch = pMat->channels()) > 0, E_UNEXPECTED );

		// Clear queue to receive planes
		CCLTRY ( pImgQ->clear() );

		// Split image
		try
			{
			if (hr == S_OK && pMat->isMat())
				{
				std::vector<cv::Mat> chs(pMat->channels());

				// Split
				cv::split ( *(pMat->mat), chs );

				// Add array to queue
				for (U32 i = 0;hr == S_OK && i < nch;++i)
					{
					// Write next channel
					CCLTRYE ( (pMatCh = new cvMatRef ( chs[i] )) != NULL, E_OUTOFMEMORY );
					CCLTRY ( pImgQ->write ( adtIUnknown ( pMatCh ) ) );
					_RELEASE(pMatCh);
					}	// for

				}	// if
			#ifdef	HAVE_OPENCV_UMAT
			else if (hr == S_OK && pMat->isUMat())
				{
				std::vector<cv::UMat> chs(pMat->channels());

				// Split
				cv::split ( *(pMat->umat), chs );

				// Add array to queue
				for (U32 i = 0;hr == S_OK && i < nch;++i)
					{
					// Write next channel
					CCLTRYE ( (pMatCh = new cvMatRef ( chs[i] )) != NULL, E_OUTOFMEMORY );
					CCLTRY ( pImgQ->write ( adtIUnknown ( pMatCh ) ) );
					_RELEASE(pMatCh);
					}	// for

				}	// else if
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (hr == S_OK && pMat->isGPU())
				{
				std::vector<cv::cuda::GpuMat> chs(pMat->channels());

				// Split
				cv::cuda::split ( *(pMat->gpumat), chs );

				// Add array to queue
				for (U32 i = 0;hr == S_OK && i < nch;++i)
					{
					// Write next channel
					CCLTRYE ( (pMatCh = new cvMatRef ( chs[i] )) != NULL, E_OUTOFMEMORY );
					CCLTRY ( pImgQ->write ( adtIUnknown ( pMatCh ) ) );
					_RELEASE(pMatCh);
					}	// for
				}	// else if
			#endif
			else
				hr = E_NOTIMPL;
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = S_FALSE;
			}	// catch

		// Start from beginning of queue
		CCLOK ( pItQ->begin(); )

		// Send out the first/next entry
		if (hr == S_OK)
			onReceive(prNext,v);
		else
			_EMT(End,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// Next image
	else if (_RCP(Next))
		{
		adtValue vL;

		// Next image from queue
		CCLTRY (	pItQ->read ( vL ) );
		if (hr == S_OK)
			{
			IDictionary *pDct = NULL;

			// Wrap matrix reference object in an image dictionary for external use
			CCLTRY ( COCREATE ( L"Adt.Dictionary", IID_IDictionary, &pDct ) );
//			lprintf ( LOG_DBG, L"Split : pDct %p : cvMatRef %p\r\n", pDct, vL.punk );
			CCLTRY ( pDct->store ( adtString(L"cvMatRef"), vL ) );
			CCLTRY ( adtValue::copy ( adtIUnknown(pDct), vL ) );

			// Clean up
			_RELEASE(pDct);
			}	// if

		// Done w/this image
		CCLOK ( pItQ->next(); )

		// Emit result
		if (hr == S_OK)
			_EMT(Next,vL);
		else
			_EMT(End,adtInt(hr));
		
		}	// else if

	// Merge
	else if (_RCP(Merge))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		U32			cnt		= 0;

		// Anything in the queue ?
		CCLTRY ( pImgQ->size ( &cnt ) );
		CCLTRYE ( cnt > 0, ERROR_INVALID_STATE );

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// At the moment, only CPU bound matrices supported
//		CCLTRYE ( pMat->isMat() == true, E_INVALIDARG );

		// Create a list of matrices to merge, then merge into
		// a single matrix and place in image dictionary.
		if (hr == S_OK)// && pMat->isMat())
			{
			std::vector<cv::Mat>		chs(cnt);
			#ifdef	HAVE_OPENCV_UMAT
			std::vector<cv::UMat>	uchs(cnt);
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			std::vector<cv::cuda::GpuMat>	gpuchs(cnt);
			#endif
			adtValue						vL;

			// Assign matrices from queue
			CCLTRY ( pItQ->begin() );
			for (	U32 i = 0; hr == S_OK && i < cnt; ++i)
				{
				IDictionary *pDct = NULL;
				cvMatRef		*pRef	= NULL;
				adtIUnknown	unkV;

				// Read next image dictionary
				CCLTRY ( pItQ->read ( vL ) );
				CCLOK  ( pItQ->next() ; )

				// Obtain image reference
				CCLTRY ( Prepare::extract ( NULL, vL, &pDct, &pRef ) );

				// Properp binding required
//				CCLTRYE( pRef->isMat() == true, E_INVALIDARG );

				// Copy over image
//				lprintf ( LOG_DBG, L"Merge : pDct %p : pRef %p\r\n", pDct, pRef );
				if (hr == S_OK && pRef->isMat())
					chs[i] = *(pRef->mat);
				#ifdef	HAVE_OPENCV_UMAT
				else if (hr == S_OK && pRef->isUMat())
					uchs[i] = *(pRef->umat);
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (hr == S_OK && pRef->isGPU())
					gpuchs[i] = *(pRef->gpumat);
				#endif

				// Clean up
				_RELEASE(pRef);
				_RELEASE(pDct);
				}	// for

			// Perform merge
			if (hr == S_OK && pMat->isMat())
				cv::merge ( chs, *(pMat->mat) );
			#ifdef	HAVE_OPENCV_UMAT
			else if (hr == S_OK && pMat->isUMat())
				cv::merge ( uchs, *(pMat->umat) );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (hr == S_OK && pMat->isGPU())
				cv::cuda::merge ( gpuchs, *(pMat->gpumat) );
			#endif
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Merge,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// Push image onto stack for merging
	else if (_RCP(Push))
		{
		IDictionary	*pDct = NULL;
		adtIUnknown unkV(v);

		// Expecting image dictionary
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDct) );

		// Push image onto queue
		CCLTRY ( pImgQ->write ( v ) );

		// Clean up
		_RELEASE(pDct);
		}	// else if

	// Re-shape the image
	else if (_RCP(Reshape))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		U32			nch		= 0;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// State check
		CCLTRYE ( iRows > 0 || iChs > 0, ERROR_INVALID_STATE );

		// Reshape
		try
			{
			// Execute 
			if (pMat->isMat())
				(*pMat->mat) = pMat->mat->reshape ( iChs, iRows );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				(*pMat->umat) = pMat->umat->reshape ( iChs, iRows );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				(*pMat->gpumat) = pMat->gpumat->reshape ( iChs, iRows );
			#endif
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = S_FALSE;
			}	// catch

		// Result
		if (hr == S_OK)
			_EMT(Reshape,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Rows))
		iRows = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

