////////////////////////////////////////////////////////////////////////
//
//									OPENCV.CPP
//
//		OpenCV implmenetation of needed image processing functionality.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#if		_MSC_VER >= 1900
#else
//#include <opencv2/gpu/gpu.hpp>
#endif
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/features2d/features2d.hpp>

// Globals
using namespace cv;
static bool bInit = false;
static bool bGPU	= false;

// String references
static adtString	strRefWidth(L"Width");
static adtString	strRefHeight(L"Height");
static adtString	strRefBits(L"Bits");
static adtString	strRefFormat(L"Format");
static adtString	strRefStep(L"Step");

HRESULT image_load ( const WCHAR *pwLoc, IDictionary *pImg, bool bGlb )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Load an image from disk.
	//
	//	PARAMETERS
	//		-	pwLoc is the source path/filename.
	//		-	pImg will receive the image data.
	//		-	bGlb is true to put image in global/shared memory, default
	//			is false/heap.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
//	Mat			*pmImg	= NULL;
	char			*paLoc	= NULL;
	adtString	strLoc(pwLoc);

	// Using OpenCV, needs full, valid Windows path
	#ifdef	_WIN32
	CCLOK ( strLoc.replace ( '/', '\\' ); )
	#endif

	// OpenCV needs ASCII
	CCLTRY ( strLoc.toAscii ( &paLoc ) );

	// 'imread' does not seem as stable on OpenCV.
	//	TODO: Replace with own PNG,BMP,etc persistence

	// Convert image into dictionary
	if (hr == S_OK)
		{
		// Read the image
		cv::Mat matRd = cv::imread ( paLoc, cv::IMREAD_UNCHANGED );

		// Something loaded ?
		CCLTRYE ( (matRd.data != NULL), ERROR_NOT_FOUND );

//		cv::UMat mat = cv::imread ( paLoc, CV_LOAD_IMAGE_UNCHANGED ).getUMat(cv::ACCESS_READ);
//		hr = image_from_mat ( &mat, pImg );
//		IplImage		*plImg = NULL;

		// Attempt load.  OpenCV does not really "understand" an alpha channel so allow
		// load to 'correct' incoming image.
		// TODO: Alpha channel support, probably by NOT using OpenCV routines to load/save images
//		CCLTRYE ( (plImg	= cvLoadImage ( paLoc )) != NULL,
//		CCLTRYE ( (plImg	= cvLoadImage ( paLoc, CV_LOAD_IMAGE_UNCHANGED )) != NULL,
//						E_UNEXPECTED );

		// Wrap into matrix
		if (hr == S_OK)
			{
			cvMatRef	matRef;

//			cv::Mat mat ( plImg, false );
//			cv::Mat mat = cv::cvarrToMat(plImg);
//			matRef.mat = &mat;
			matRef.mat = &matRd;
			hr = image_from_mat ( &matRef, pImg, bGlb );
			matRef.mat = NULL;
			}	// if

		// Clean up
//		if (plImg != NULL)
//			cvReleaseImage(&plImg);
		}	// if
	else
		lprintf ( LOG_DBG, L"Unable to load image : 0x%x(%s)\r\n", hr, pwLoc );

	// Clean up
	_FREEMEM(paLoc);

	return hr;
	}	// image_load

HRESULT image_save ( IDictionary *pImg, const WCHAR *pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Save an image to disk.
	//
	//	PARAMETERS
	//		-	pImg contains the image data.
	//		-	pwLoc is the destination path/filename.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	Mat				*pmImg	= NULL;
	char				*paLoc	= NULL;
	adtString		strLoc(pwLoc);
	int				ret		= 0;
	bool				b			= false;
	IMemoryMapped	*pBits	= NULL;
	void *				pvBits	= NULL;

	// Using OpenCV, needs full, valid Windows path
	#ifdef	_WIN32
	CCLOK ( strLoc.replace ( '/', '\\' ); )
	#endif

	// Convert image to OpenCV
	CCLTRY ( image_to_mat ( pImg, &pmImg, &pBits, &pvBits ) );

	// OpenCV needs ASCII
	CCLTRY ( strLoc.toAscii ( &paLoc ) );

	// 'imwrite' does not seem to work, OpenCV is so flaky
	//	TODO: Replace with own PNG,BMP,etc persistence
//	CCLOK ( ret = cvSaveImage ( paLoc, &(IplImage(*pmImg)) ); )
	CCLOK ( b = imwrite ( paLoc, *pmImg ); )
	if (!b)
		lprintf ( LOG_INFO, L"cvSaveImage : %S : b %d errno %d\r\n", 
										paLoc, b, errno );

	// Clean up
	_FREEMEM(paLoc);
	if (pmImg != NULL)
		delete pmImg;
	_RELEASE(pBits);

	return hr;
	}	// image_save

HRESULT image_to_debug ( cvMatRef *pMat, const WCHAR *pwCtx, 
									const WCHAR *pwLoc )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Output/save debug information about an image.
	//
	//	PARAMETERS
	//		-	pMat is the matrix object
	//		-	pwCtx is the context
	//		-	pwLoc is the destination for the image
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr			= S_OK;
	char			*paLoc	= NULL;
	U32			type		= 0;
	double		dMin		= 0.0;
	double		dMax		= 0.0;
	cv::Point	ptMin,ptMax;
	adtString	strLoc(pwLoc);
	cv::Mat		matSave;

	// Values and locations of min and max
	if (pMat->isMat())
		{
		cv::minMaxLoc ( *(pMat->mat), &dMin, &dMax, &ptMin, &ptMax );
		type = pMat->mat->type();
		}	// if
	#ifdef	HAVE_OPENCV_UMAT
	else if (pMat->isUMat())
		{
		cv::minMaxLoc ( *(pMat->umat), &dMin, &dMax, &ptMin, &ptMax );
		type = pMat->umat->type();
		}	// if
	#endif
	#ifdef	HAVE_OPENCV_CUDA
	else if (pMat->isGPU())
		{
		cv::cuda::minMaxLoc ( *(pMat->gpumat), &dMin, &dMax, &ptMin, &ptMax );
		type = pMat->gpumat->type();
		}	// if
	#endif
	dbgprintf ( L"%s (%s) : Min : %g @ (%d,%d) : Max : %g @ (%d,%d) : type %d\r\n",
						pwCtx, 
						(pMat->isGPU()) ? L"GPU" :
						(pMat->isUMat()) ? L"UMAT" : L"CPU",
						dMin, ptMin.x, ptMin.y, dMax, ptMax.x, ptMax.y,
						type );

	// Download
	if (pMat->isMat())
		pMat->mat->copyTo ( matSave );
	#ifdef	HAVE_OPENCV_UMAT
	else if (pMat->isUMat())
		pMat->umat->copyTo ( matSave );
	#endif
	#ifdef	HAVE_OPENCV_CUDA
	else if (pMat->isGPU())
		pMat->gpumat->download ( matSave );
	#endif

	// Some sample point values
	if (matSave.type() == CV_32FC1)
		dbgprintf ( L"%g %g %g %g - %g %g %g %g\r\n",
						matSave.at<float>(0,0),
						matSave.at<float>(0,1),
						matSave.at<float>(0,2),
						matSave.at<float>(0,3),
						matSave.at<float>(1,0),
						matSave.at<float>(1,1),
						matSave.at<float>(1,2),
						matSave.at<float>(1,3) );

	// Conversion for saving to image
	cv::normalize ( matSave, matSave, 0, 255, cv::NORM_MINMAX );
	matSave.convertTo ( matSave, CV_8UC1 );

	// Save to file
	if (strLoc.toAscii ( &paLoc ) == S_OK)
		cv::imwrite ( paLoc, matSave );
	
	// Clean up
	_FREEMEM(paLoc);

	return hr;
	}	// image_to_debug

HRESULT image_format ( cvMatRef *pM, adtString &strF )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Determine string version of format for image.
	//
	//	PARAMETERS
	//		-	pM contains the image data
	//		-	strF will receive the format
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;

	// Assign format, add new formats as needed.
	switch ( pM->channels() )
		{
		case 1:
			// Gray-scale
			switch ( (pM->type() & CV_MAT_DEPTH_MASK) )
				{
				case CV_8U:
					strF = L"U8";
					break;
				case CV_16U:
					strF = L"U16";
					break;
				case CV_16S:
					strF = L"S16";
					break;
				case CV_32F:
					strF = L"F32";
					break;
				default :
					hr = E_NOTIMPL;
				}	// switch
			break;
		case 2:
			switch ( (pM->type() & CV_MAT_DEPTH_MASK) )
				{
				case CV_8U:
					strF = L"U8U8";
					break;
				case CV_8S:
					strF = L"S8S8";
					break;
				case CV_16U:
					strF = L"U16U16";
					break;
				case CV_16S:
					strF = L"S16S16";
					break;
				case CV_32F:
					strF = L"F32F32";
					break;
				default :
					hr = E_NOTIMPL;
				}	// switch
			break;
		case 3 :
			// Color
			switch ( (pM->type() & CV_MAT_DEPTH_MASK) )
				{
				case CV_8U :
					strF = L"B8G8R8";
					break;
				case CV_16S :
					strF = L"S16x3";
					break;
				case CV_16U :
					strF = L"B16G16R16";
					break;
				case CV_32F :
					strF = L"BGRf";
					break;
				default :
					hr = E_NOTIMPL;
				}	// switch
			break;
		case 4 :
			// Color
			switch ( (pM->type() & CV_MAT_DEPTH_MASK) )
				{
				case CV_8U :
					strF = L"B8G8R8A8";
					break;
				case CV_16S :
					strF = L"S16x4";
					break;
				case CV_32F :
					strF = L"BGRAf";
					break;
				default :
					hr = E_NOTIMPL;
				}	// switch
			break;
		default :
			lprintf ( LOG_WARN, L"Unsupported channels %d", pM->channels() );
			hr = E_NOTIMPL;
		}	// switch

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_WARN, L"Failed 0x%x:Channels %d:Type 0x%x", 
						hr, pM->channels(), pM->type() );

	return hr;
	}	// image_format

HRESULT image_format ( const WCHAR *pwFmt, U32 *pcvFmt )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert system string format to OpenCV format.
	//
	//	PARAMETERS
	//		-	pwFmt is the system string version of the format 
	//		-	pcvFmt will receive the OpenCV format
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr	= S_OK;

	// NOTE: Add needed formats over time
	// NOTE: Up to caller to keep track of RGB vs BGR ordering since
	// OpenCV always does BGR.
	if (hr == S_OK && !WCASECMP(pwFmt,L"U16"))
		*pcvFmt = CV_16UC1;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"S16"))
		*pcvFmt = CV_16SC1;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"U8"))
		*pcvFmt = CV_8UC1;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"U8U8"))
		*pcvFmt = CV_8UC2;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"S8S8"))
		*pcvFmt = CV_8SC2;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"S8"))
		*pcvFmt = CV_8SC1;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"F32"))
		*pcvFmt = CV_32FC1;
	else if (hr == S_OK && !WCASECMP(pwFmt,L"F32F32"))
		*pcvFmt = CV_32FC2;
	else if (hr == S_OK && 
				(!WCASECMP(pwFmt,L"R8G8B8") || !WCASECMP(pwFmt,L"B8G8R8")))
		*pcvFmt = CV_8UC3;
	else if (hr == S_OK && 
				(	!WCASECMP(pwFmt,L"R8G8B8A8") || !WCASECMP(pwFmt,L"B8G8R8A8") ||
					!WCASECMP(pwFmt,L"A8R8G8B8") ))
		*pcvFmt = CV_8UC4;
	else if (hr == S_OK && 
				(!WCASECMP(pwFmt,L"R16G16B16") || !WCASECMP(pwFmt,L"B16G16R16")))
		*pcvFmt = CV_16UC3;
	else if (hr == S_OK && 
				(	!WCASECMP(pwFmt,L"R16G16B16A16") || !WCASECMP(pwFmt,L"B16G16R16A16") ||
					!WCASECMP(pwFmt,L"A16R16G16B16") ))
		*pcvFmt = CV_16UC4;
	else if (hr == S_OK && (!WCASECMP(pwFmt,L"BGRf")))
		*pcvFmt = CV_32FC3;
	else if (hr == S_OK && (!WCASECMP(pwFmt,L"BGRAf")))
		*pcvFmt = CV_32FC4;

	// No native support for half floats so store as 16s
	else if (hr == S_OK && 
				(!WCASECMP(pwFmt,L"RGBAhf") || !WCASECMP(pwFmt,L"BGRAhf")))
		*pcvFmt = CV_16SC4;
	else 
		hr = ERROR_NOT_SUPPORTED;

	return hr;
	}	// image_format

HRESULT image_from_mat ( cvMatRef *pMat, IDictionary *pImg, bool bGlb )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Convert an OpenCV mat object to an image dictionary.
	//
	//	PARAMETERS
	//		-	pMat contains the image data
	//		-	pImg will receive the image data
	//		-	bGlb is true to put image in global/shared memory, default
	//			is false/heap.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr			= S_OK;
	IMemoryMapped	*pBits	= NULL;
	void				*pvBits	= NULL;
	bool				bMemCr	= false;
	U32				step		= 0;
	adtString		strFmt;
	cv::Mat			mat;
	adtValue			vL;

	// Must download first
	if (pMat->isMat())
		mat = *(pMat->mat);
	#ifdef	HAVE_OPENCV_UMAT
	else if (pMat->isUMat())
		mat = pMat->umat->getMat(cv::ACCESS_READ);
	#endif
	#ifdef	HAVE_OPENCV_CUDA
	else if (pMat->isGPU())
		pMat->gpumat->download(mat);
	#endif

	// Image information
	CCLTRY ( pImg->store ( strRefWidth, adtInt(mat.cols) ) );
	CCLTRY ( pImg->store ( strRefHeight, adtInt(mat.rows) ) );

	// Compute/use step size
	CCLOK ( step = (U32)(mat.cols*mat.elemSize()); )
	if (hr == S_OK && pImg->load ( strRefStep, vL ) == S_OK)
		step = adtInt(vL);

	// Attempt to recycle and resize an existing memory block
	if (hr == S_OK)
		{
		IPersistId	*pId	= NULL;
		adtValue		vId;
		adtString	strId;

		// Existing bits ?
		CCLTRY ( pImg->load ( strRefBits, vL ) );

		// Check if class Id matches the requested memory type
		CCLTRY ( _QISAFE(adtIUnknown(vL),IID_IPersistId,&pId) );
		CCLTRY ( pId->getPersistId ( vId ) );
		CCLTRY ( adtValue::toString ( vId, strId ) );
		CCLTRYE( (bGlb && !WCASECMP(strId,L"Io.SharedMemory")) ||
					(!bGlb && !WCASECMP(strId,L"Io.MemoryBlock")),E_INVALIDARG);

		// Interface
		CCLTRY ( _QISAFE(adtIUnknown(vL),IID_IMemoryMapped,&pBits) );

		// Size
		CCLTRY ( pBits->setSize ( (U32)(mat.rows*step) ) );

		// If anything failed, memory will have to be created
		bMemCr	= (hr != S_OK);
		hr			= S_OK;

		// Debug
//		lprintf ( LOG_DBG, L"bMemCr %s Size %d pBits %p hr 0x%x\r\n", 
//							(bMemCr) ? L"Create" : L"Reuse", (U32)(mat.rows*step), pBits, hr );

		// Clean up
		_RELEASE(pId);
		}	// if

	// Create and size memory block for image
	if (hr == S_OK && bMemCr)
		{
		// Local memory
		if (!bGlb)
			{
			CCLTRY ( COCREATE ( L"Io.MemoryBlock", IID_IMemoryMapped, &pBits ) );
			}	// if

		// Global memory
		else
			{
			CCLTRY ( COCREATE ( L"Io.SharedMemory", IID_IMemoryMapped, &pBits ) );
			}	// if

		// Bits for image
		CCLTRY ( pImg->store ( strRefBits, adtIUnknown(pBits) ) );

		// Size of region
		CCLTRY ( pBits->setSize ( (U32)(mat.rows*step) ) );
		}	// if

	// Copy bits.  If the source and destination ptrs are the same, then the
	// block was used directly, nothing to copy.
	CCLTRY ( pBits->getInfo ( &pvBits, NULL ) );
	if (hr == S_OK && (pvBits != mat.data))
		{
		// Same step size ?
		if ((mat.cols*mat.elemSize()) == step)
			memcpy ( pvBits, mat.data, mat.total()*mat.elemSize() );
		else
			{
			U8		*pcDstBits	= (U8 *)pvBits;
			U8		*pcSrcBits	= (U8 *)mat.data;

			// Copy a row at a time to handle optional striding properly
			for (S32 row = 0;row < mat.rows;++row)
				{
				// Copy row
				memcpy ( pcDstBits, pcSrcBits, (mat.cols*mat.elemSize()) );
	//			memset ( pcDstBits, 0x80, step );

				// Next row
				pcDstBits += step;
				pcSrcBits += (mat.cols*mat.elemSize());
				}	// for

			}	// else

		}	// if

	// Retreive equivalent system format
	CCLTRY ( image_format ( pMat, strFmt ) );

	// OpenCV does not keep track of color format, override if specified directly (by 'Convert' node)
	if (hr == S_OK && pImg->load ( adtString(L"ColorFormat"), vL ) == S_OK)
		hr = adtValue::toString ( vL, strFmt );

	// Update format
	CCLTRY ( pImg->store ( strRefFormat, strFmt ) );

	// Debug
	if (hr != S_OK)
		lprintf ( LOG_WARN, L"Failed 0x%x", hr );

	// Clean up
	_RELEASE(pBits);

	return hr;
	}	// image_from_mat

HRESULT image_to_mat ( IDictionary *pImg, Mat **ppM,
								IMemoryMapped **ppBits, void **ppvBits )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Wrap an image inside an OpenCV matrix.
	//
	//	PARAMETERS
	//		-	pImg contains the image data.
	//		-	ppM will receive the matrix.
	//		-	ppBits,ppvBits will receive the bits information for unlocking
	//			convenience to caller
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT			hr		= S_OK;
	U32				w		= 0;
	U32				h		= 0;
	U32				cvFmt	= 0;
	U32				step	= cv::Mat::AUTO_STEP;
	adtValue			vL;
	adtString		strFmt;
	adtIUnknown		unkV;

	// Setup
	(*ppM) = NULL;

	// Debug
//	lprintf ( LOG_DBG, L"pImg %p\r\n", pImg );

	// Access image information
	CCLTRY ( pImg->load ( strRefWidth, vL ) );
	CCLOK  ( w = adtInt(vL); )
	CCLTRY ( pImg->load ( strRefHeight, vL ) );
	CCLOK  ( h = adtInt(vL); )
	CCLTRY ( pImg->load ( strRefFormat, vL ) );
	CCLTRYE( (strFmt = vL).length() > 0, E_UNEXPECTED );
	CCLTRY ( pImg->load ( strRefBits, vL ) );
	CCLTRY ( _QISAFE((unkV=vL),IID_IMemoryMapped,ppBits) );
	CCLTRY ( (*ppBits)->getInfo ( ppvBits, NULL ) );

	// Optional step length
	if (hr == S_OK && pImg->load ( strRefStep, vL ) == S_OK)
		step = adtInt(vL);

	// Convert format to OpenCV format
	CCLTRY ( image_format ( strFmt, &cvFmt ) );
 
	// Debug
//	lprintf ( LOG_DBG, L"w %d h %d step %d cvFmt %d format %s *ppvBits %p hr 0x%x\r\n", 
//					w, h, step, cvFmt, (LPCWSTR)strFmt, *ppvBits, hr );

	// Wrap bits
	CCLTRYE ( ((*ppM) = new Mat ( h, w, cvFmt, *ppvBits, step )) != NULL,
												E_OUTOFMEMORY );

	// Debug
//	lprintf ( LOG_DBG, L"w %d h %d step %d cvFmt %d format %s *ppvBits %p hr 0x%x\r\n", 
//					(*ppM)->cols, (*ppM)->rows, step, cvFmt, (LPCWSTR)strFmt, *ppvBits, hr );

	return hr;
	}	// image_to_mat
