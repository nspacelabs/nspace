////////////////////////////////////////////////////////////////////////
//
//									Lookup.CPP
//
//				Implementation of the image table look-up node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

Lookup :: Lookup ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg	= NULL;
	pTbl	= NULL;
	}	// Lookup

HRESULT Lookup :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
//		if (pnDesc->load ( adtString(L"Left"), vL ) == S_OK)
//			iL = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		_RELEASE(pTbl);
		}	// else

	return hr;
	}	// onAttach

HRESULT Lookup :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse		= NULL;
		IDictionary	*pTblUse		= NULL;
		cvMatRef		*pMat			= NULL;
		cvMatRef		*pTblMat		= NULL;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Look-up table
		CCLTRY ( Prepare::extract ( pTbl, adtValue(), &pTblUse, &pTblMat ) );

		// Catch OpenCV exceptions
		try
			{
			// Execute
			if (hr == S_OK && pMat->isMat() && pTblMat->isMat())
				{
				cv::Mat	matDst;

				// Perform look-up, possibly to non-matching depths
				cv::LUT( *(pMat->mat), *(pTblMat->mat), matDst );

				// Copy back to source
				matDst.copyTo(*(pMat->mat));
				}	// if
			#ifdef	HAVE_OPENCV_UMAT
			else if (hr == S_OK && pMat->isUMat() && pTblMat->isUMat())
				{
				cv::UMat	matDst;

				// Perform look-up, possibly to non-matching depths
				cv::LUT( *(pMat->umat), *(pTblMat->umat), matDst );

				// Copy back to source
				matDst.copyTo(*(pMat->umat));
				}	// if
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (hr == S_OK && pMat->isGPU())
				{
				// NOTE: Currently the GPU look-up table is limited
				// to only 8-bit values.  Unfortunately this means
				// download/transform/upload cycle for unsupported formats.
				if (	(pMat->type()		& CV_MAT_DEPTH_MASK) == CV_8U &&
						(pTblMat->type()	& CV_MAT_DEPTH_MASK) == CV_8U)
					{
					// Caching 'pLut' in object is causing heap corruption (?)
					cv::cuda::GpuMat						matDst;
					cv::Ptr<cv::cuda::LookUpTable>	pLut;

					// Need a look-up table for this image
					pLut = cv::cuda::createLookUpTable(*(pTblMat->gpumat));

					// Perform lookup
					pLut->transform ( *(pMat->gpumat), matDst );

					// Copy back to source
					matDst.copyTo(*(pMat->gpumat));
					}	// if

				// SLOW version
				else
					{
					cv::Mat	matSrc,matTbl,matDst;

					// Download image into local mat
					pMat->gpumat->download(matSrc);
					pTblMat->gpumat->download(matTbl);
					
					// Perform look-up, possibly to non-matching depths
					cv::LUT( matSrc, matTbl, matDst );

					// Copy back to source
					pMat->gpumat->upload ( matDst );
					}	// else

				}	// if
			#endif
			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = S_FALSE;
			}	// catch

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pTblMat);
		_RELEASE(pTblUse);
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Table))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pTbl);
		_QISAFE(unkV,IID_IDictionary,&pTbl);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

