////////////////////////////////////////////////////////////////////////
//
//									CREATE.CPP
//
//				Implementation of the image creation node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals
extern bool	bCuda;									// CUDA enabled
extern bool	bUMat;									// UMat/OpenCL enabled

Create :: Create ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg		= NULL;
	strFmt	= L"U8";
	iW			= 128;
	iH			= 128;
	bCPU		= false;
	strType	= L"";
	}	// Create

HRESULT Create :: create ( IDictionary *pDct, U32 w, U32 h, U32 f,
									cvMatRef **ppMat, bool bCpu )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Create an appropiate matrix reference object for the image.
	//
	//	PARAMETERS
	//		-	pDct is an optional dictionary to receive the information
	//		-	w,h are the dimensions of the image
	//		-	f is the OpenCV format
	//		-	ppMat is optional and will receive the referenced matrix object.
	//		-	bCpu is true to create a CPU bound cv::Mat regardless of
	//			the current GPU settings.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr		= S_OK;
	cvMatRef	*pMat	= NULL;

	// Ensure GPU initialization has taken place
	CCLOK ( Prepare::gpuInit(); )

	// Create a matrix based on the GPU mode
	CCLTRYE( (pMat = new cvMatRef()) != NULL, E_OUTOFMEMORY );
	if (bCpu || (!bUMat && !bCuda))
		{
		CCLTRYE ( (pMat->mat = new cv::Mat ( h, w, f )) != NULL,
						E_OUTOFMEMORY );
		CCLOK ( pMat->mat->setTo ( cv::Scalar(0) ); )
		}	// else
	#ifdef	HAVE_OPENCV_UMAT
	if (!bCpu && bUMat)
		{
		CCLTRYE ( (pMat->umat = new cv::UMat ( h, w, f )) != NULL,
						E_OUTOFMEMORY );
		CCLOK ( pMat->umat->setTo ( cv::Scalar(0) ); )
		}	// else if
	#endif
	#ifdef	HAVE_OPENCV_CUDA
	else if (!bCpu && bCuda)
		{
		CCLTRYE ( (pMat->gpumat = new cv::cuda::GpuMat ( h, w, f )) != NULL,
						E_OUTOFMEMORY );
		CCLOK ( pMat->gpumat->setTo ( cv::Scalar(0) ); )
		}	// if
	#endif

	// Store image in image dictionary
	if (hr == S_OK && pDct != NULL)
		hr = pDct->store ( adtString(L"cvMatRef"), adtIUnknown(*pMat) );

	// Result
	if (hr == S_OK && ppMat != NULL)
		{
		(*ppMat) = pMat;
		_ADDREF((*ppMat));
		}	// if

	// Clean up
	_RELEASE(pMat);
	return hr;
	}	// create

HRESULT Create :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Accel"), vL ) == S_OK)
			onReceive(prAccel,vL);
		if (pnDesc->load ( adtString(L"Format"), vL ) == S_OK)
			hr = adtValue::toString ( vL, strFmt );
		if (pnDesc->load ( adtString(L"Width"), vL ) == S_OK)
			iW = vL;
		if (pnDesc->load ( adtString(L"Height"), vL ) == S_OK)
			iH = vL;
		if (pnDesc->load ( adtString(L"CPU"), vL ) == S_OK)
			bCPU = vL;
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			hr = adtValue::toString ( vL, strType );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Create :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		U32			cvFmt;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, NULL ) );

		// DEBUG
//		if (!WCASECMP(strnName,L"CreatePlusTmp"))
//			dbgprintf ( L"Hi\r\n" );

		// Process
		// A 'type' can be specified if a certain type of matrix is to
		// be created, other a blank image is created.
		if (hr == S_OK)
			{
			// Map requested format into OpenCV format
			if (!WCASECMP(strFmt,L"F32"))
				cvFmt = CV_32FC1;
			else if ((!WCASECMP(strFmt,L"R8G8B8") || !WCASECMP(strFmt,L"B8G8R8")))
				cvFmt = CV_8UC3;
			else if ((!WCASECMP(strFmt,L"R8G8B8A8") || !WCASECMP(strFmt,L"B8G8R8A8")))
				cvFmt = CV_8UC4;
			else if ((!WCASECMP(strFmt,L"R16G16B16") || !WCASECMP(strFmt,L"B16G16R16")))
				cvFmt = CV_16UC3;
			else if (!WCASECMP(strFmt,L"U16"))
				cvFmt = CV_16UC1;
			else if (!WCASECMP(strFmt,L"S16"))
				cvFmt = CV_16SC1;
			else if (!WCASECMP(strFmt,L"U8"))
				cvFmt = CV_8UC1;
			else if (!WCASECMP(strFmt,L"S8"))
				cvFmt = CV_8SC1;
			else
				cvFmt = CV_8UC1;

			// Create a blank matrix based on the GPU mode
			CCLTRY ( create ( pImgUse, iW, iH, cvFmt, &pMat, bCPU ) );

			// Gaussian kernel
			if (!WCASECMP(strType,L"Gaussian"))
				{
				cv::Mat	matK;

				// Create a CPU bound kernel
				CCLOK ( matK = cv::getGaussianKernel(iW,0,cvFmt); )

				// Gaussian kernel gives 1D version, create 2D version
				CCLOK ( cv::mulTransposed ( matK, matK, false ); )

				// Copy to blank image
				if (pMat->isMat())
					matK.copyTo ( *(pMat->mat) );
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					matK.copyTo ( *(pMat->umat) );
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					pMat->gpumat->upload ( matK );
				#endif
				}	// if

			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// else if

	// Matrix from stream
	else if (_RCP(Stream))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		IByteStream	*pStm		= NULL;
		U32			fmt		= CV_8UC1;
		U32			h			= 0;
		U32			w			= 0;
		adtIUnknown	unkV(v);
		U64			sz;

		// State check
		CCLTRY ( _QISAFE(unkV,IID_IByteStream, &pStm) );

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, NULL ) );

		// Size of byte stream
		CCLTRY ( pStm->available ( &sz ) );

		// Default is 1xBytes 8bits per element.  Can be changed
		// by specifying other values.
		if (hr == S_OK)
			{
			fmt	= CV_8UC1;
			w		= 1;
			h		= (U32)sz;
			}	// if

		// Allow format to be specified
		if (hr == S_OK && strFmt.length() > 0)
			hr = image_format ( strFmt, &fmt );

		// Non-default values
		if (hr == S_OK && iW != 128)
			w = iW;
		if (hr == S_OK && iH != 128)
			h = iH;

		// Create a 1-D "image" for receive the bytes
		CCLTRY ( create ( pImgUse, w, h, fmt, &pMat, true ) );

		// Compute "real" size of data based on specified dimensions and format
		CCLOK ( sz =	pMat->cols() * 
							pMat->rows() *
							pMat->mat->elemSize(); )

		// Read the data from the stream into the data
		CCLTRY ( pStm->read ( pMat->mat->data, sz, NULL ) );

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		_RELEASE(pStm);
		}	// else if

	// Matrix from list
	else if (_RCP(List) || _RCP(ListByRow))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;
		IContainer	*pLst		= NULL;
		IIt			*pIt		= NULL;
		cv::Mat		mat;
		adtIUnknown unkV(v);
		adtValue		vL;

		// Expecting list of values columns, then rows.
		CCLTRY ( _QISAFE(unkV,IID_IContainer,&pLst) );
		CCLTRY ( pLst->iterate ( &pIt ) );

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, NULL ) );

		// Create a CPU bound 2D floating point "image" to receive entries
		CCLOK ( mat = cv::Mat::zeros ( iH, iW, CV_32FC1 ); )

		// Read the values directly into position
		if (hr == S_OK)
			{
			for (int r = 0, c = 0;r < (S32)iH && c < (S32)iW;)
				{
				// Next value
				if (pIt->read ( vL ) != S_OK)
					break;
				pIt->next();

				// Place into position
				mat.at<float>(r,c) = adtFloat(vL);

				// Increment appropriate index based on order
				if (_RCP(ListByRow) && ++r >= (S32)iH)
					{
					r = 0;
					++c;
					}	// if
				else if (_RCP(List) && ++c >= (S32)iW)
					{
					c = 0;
					++r;
					}	// else if
				}	// for
			}	// for

		// Now create the proper type of matrix for the current flags to
		// receive the initialized matrix
		CCLTRY ( create ( pImgUse, iW, iH, CV_32FC1, &pMat, bCPU ) );

		// Copy matrix to destination
		if (hr == S_OK)
			{
			if (pMat->isMat())
				mat.copyTo ( *(pMat->mat) );
			#ifdef	HAVE_OPENCV_UMAT
			else if (pMat->isUMat())
				mat.copyTo ( *(pMat->umat) );
			#endif
			#ifdef	HAVE_OPENCV_CUDA
			else if (pMat->isGPU())
				pMat->gpumat->upload ( mat );
			#endif
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		_RELEASE(pIt);
		_RELEASE(pLst);
		}	// else if

	// Set the preferred 'global' acceleration preference.
	else if (_RCP(Accel))
		{
		adtString strType(v);

		// Re-initialize with new preference
		Prepare::gpuInit (	!WCASECMP(strType,L"Cuda"), 
									!WCASECMP(strType,L"OpenCL"), true );

		// Result
		_EMT(Accel,adtString(	(bCuda) ? L"Cuda" :
										(bUMat) ? L"OpenCL" : L"CPU" ));
		}	// else if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Format))
		adtValue::toString ( v, strFmt );
	else if (_RCP(Width))
		iW = v;
	else if (_RCP(Height))
		iH = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

