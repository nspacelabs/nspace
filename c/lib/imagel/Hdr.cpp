////////////////////////////////////////////////////////////////////////
//
//									HDR.CPP
//
//				Implementation of the Hdr image node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// CV_VERSION_EPOCH seems to be defined only for < OpenCV 3
#ifndef	CV_VERSION_EPOCH
//#include <opencv2/photo/photo.hpp>

// Globals

Hdr :: Hdr ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg			= NULL;
	strMap		= L"";
	fInten		= 80.0f;
	}	// Hdr

HRESULT Hdr :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Tonemap"), vL ) == S_OK)
			onReceive(prMap,vL);
		if (pnDesc->load ( adtString(L"Intensity"), vL ) == S_OK)
			fInten = vL;
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pImg);
		}	// else

	return hr;
	}	// onAttach

HRESULT Hdr :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, &pMat ) );

		// Map ?
		CCLTRYE ( WCASECMP(strMap,L"Reinhard") == 0, E_INVALIDARG );

		// Execute
		if (hr == S_OK)
			{
			// Open CV uses exceptions
			try
				{
				// Execute 
				if (pMat->isMat())
					{
					// Own destination
					cv::Mat matDst;

					// Tonemap type
					if (tone.empty())
//					cv::Ptr<cv::TonemapReinhard>
						tone = cv::createTonemapReinhard();

					// Setup
//					tone->setIntensity(fInten);

					// Apply
					tone->process(*(pMat->mat),matDst);

					// Result
					if (matDst.rows > 0 && matDst.cols > 0)
						matDst.copyTo ( *(pMat->mat) );
					}	// else
				#ifdef	HAVE_OPENCV_UMAT
				else if (pMat->isUMat())
					{
					// Own destination
					cv::UMat matDst;

					// Tonemap type
					cv::Ptr<cv::TonemapReinhard>
					tone = cv::createTonemapReinhard();

					// Setup
//					tone->setIntensity(fInten);

					// Apply
					tone->process(*(pMat->umat),matDst);

					// Result
					if (matDst.rows > 0 && matDst.cols > 0)
						matDst.copyTo ( *(pMat->umat) );
					}	// else if
				#endif
				#ifdef	HAVE_OPENCV_CUDA
				else if (pMat->isGPU())
					{
					hr = E_NOTIMPL;
					}	// if
				#endif
				}	// try
			catch ( cv::Exception &ex )
				{
				lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
				hr = S_FALSE;
				}	// catch

			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMat);
		_RELEASE(pImgUse);
		}	// if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Map))
		hr = adtValue::toString ( v, strMap );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

#endif
