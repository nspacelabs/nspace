////////////////////////////////////////////////////////////////////////
//
//										cuda_jpeg.CPP
//
//				Implementation of the CUDA JPEG (de)compression utilities
//
//				Based on nVidia jpegNPP sample from the CUDA library
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"

#ifdef	USE_CUDAJPEG

// Cuda
#include <npp.h>
#include <cuda_runtime.h>
#include "cuda_Exceptions.h"
#include "cuda_Endianess.h"

//
// Internal structures
//

struct FrameHeader
{
    unsigned char nSamplePrecision;
    unsigned short nHeight;
    unsigned short nWidth;
    unsigned char nComponents;
    unsigned char aComponentIdentifier[3];
    unsigned char aSamplingFactors[3];
    unsigned char aQuantizationTableSelector[3];
};

struct ScanHeader
{
    unsigned char nComponents;
    unsigned char aComponentSelector[3];
    unsigned char aHuffmanTablesSelector[3];
    unsigned char nSs;
    unsigned char nSe;
    unsigned char nA;
};

struct QuantizationTable
{
    unsigned char nPrecisionAndIdentifier;
    unsigned char aTable[64];
};

struct HuffmanTable
{
    unsigned char nClassAndIdentifier;
    unsigned char aCodes[16];
    unsigned char aTable[256];
};

//
// Structure - refImage.  In order to use the same quantization and huffman 
//		tables for every compression, this structure will store information
//		about the 'reference' image that contains those tables that is decompressed
//		once on initialization.
//		

static struct _refImage
	{
	FrameHeader			oFrameHeader;
	ScanHeader			oScanHeader;
	QuantizationTable	aQuantizationTables[4];
	HuffmanTable		aHuffmanTables[4];
	} refImage;

static bool	bHaveRef	= false;
static bool	bUseRef	= false;
//
// Structure - refCmp/refDcmp.  To avoid allocating/deallocating the same memory for
//		every image, this structure can be used to cache pre-allocated memory for re-use.
//

static struct _refCmp
	{
	// Source RGB image
	Npp8u		*pdSrc;										// Source image memory
	int		srcW,srcH;									// Source memory size
	size_t	srcPitch;									// Source pitch

	// Source YCbCr image
	Npp8u		*pdYuv[3];									// Image memory
	int		yuvW,yuvH;									// Yuv memory size
	size_t	yuvPitch[3];								// Yuv pitch

	// DCT block
	Npp16s	*apdDCT[3];									// DCT for each plane
	int		dctW[3],dctH[3];							// DCT dimensions
	Npp32s	aDCTStep[3];								// DCT pitch

	// Scan output
	Npp8u		*pdScan;										// Scan memory
	Npp32s	nScanSize;									// Size of scan memory

	// Scatch space
	Npp8u		*pJpegEncoder;								// Needed by encoder
	size_t	nEncSize;									// Encoder size

	// Huffman tables
	NppiEncodeHuffmanSpec	*apHuffmanDCTable[3];
	NppiEncodeHuffmanSpec	*apHuffmanACTable[3];

	// Quantization tables
   Npp8u							*pdQuantizationTables;
	} refCmp;												// Compression memory

static struct _refDcmp
	{
	// Source image planes
	Npp8u		*apSrcImage[3];							// Ptr to memory 
	int		srcW[3],srcH[3];							// Source memory size
	Npp32s	aSrcImageStep[3];							// Step sizes

	// DCT block
	Npp16s	*apdDCT[3];									// DCT for each plane (device)
	Npp16s	*aphDCT[3];									// DCT for each plane (host)
	int		dctW[3],dctH[3];							// DCT dimensions
	Npp32s	aDCTStep[3];								// DCT pitch

	// Destination image planes
	Npp8u		*apDstImage;								// Ptr to memory
	int		dstW,dstH;									// Destination memory size
	size_t	aDstImageStep;								// Pitch

	// Quantization tables
   Npp8u		*pdQuantizationTables;
	} refDcmp;												// Decompress memory

// Internal prototypes
int	DivUp							(	int x, int d );
int	nextMarker					(	const unsigned char *pData, int &nPos, int nLength );
void	readFrameHeader			(	const unsigned char *pData, FrameHeader &header );
void	readHuffmanTables			(	const unsigned char *pData, HuffmanTable *pTables );
void	readQuantizationTables	(	const unsigned char *pData, QuantizationTable *pTables );
void	readRestartInterval		(	const unsigned char *pData, int &nRestartInterval );
void	readScanHeader				(	const unsigned char *pData, ScanHeader &header );
void	writeFrameHeader			(	const FrameHeader &header, unsigned char *&pData );
void	writeHuffmanTable			(	const HuffmanTable &table, unsigned char *&pData );
void	writeJFIFTag				(	unsigned char *&pData );
void	writeMarker					(	unsigned char nMarker, unsigned char *&pData );
void	writeQuantizationTable	(	const QuantizationTable &table, unsigned char *&pData );
void	writeScanHeader			(	const ScanHeader &header, unsigned char *&pData );

HRESULT	cudaCompress	( U32 w, U32 h, U32 stride, U32 ch, U32 cs, U32 q,
									IMemoryMapped *pBitsSrc, 
									IMemoryMapped *pBitsDst )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Compresses image to destination buffer.
	//
	//	PARAMETERS
	//		-	w,h is the width and height of the area
	//		-	stride is the size, in bytes, of an entire row.
	//		-	ch is the number of channels
	//		-	cs is the color space
	//		-	q is the quality (TODO, currently uses reference image)
	//		-	pbSrc contains the image bytes
	//		-	pbDst will receive the compress image
	//		-	piDst contains the maximum number of bytes on input, contains
	//			size of compressed image on output.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT						hr						= S_OK;
	FrameHeader					*pFrameHeader		= &(refImage.oFrameHeader);
	ScanHeader					*pScanHeader		= &(refImage.oScanHeader);
	int							nMCUBlocksH			= 0;
	int							nMCUBlocksV			= 0;
	NppiSize						szSrcRoi				= { (int)w, (int)h };
	HuffmanTable				*pHuffmanDCTables = refImage.aHuffmanTables;
	HuffmanTable				*pHuffmanACTables = &refImage.aHuffmanTables[2];
	BYTE							*pbBitsDst			= NULL;
	BYTE							*pDstOutput			= NULL;
	BYTE							*pbSrc				= NULL;
	U32							szSrc,szDst;
	Npp8u							*hpCodesDC[2];
	Npp8u							*hpCodesAC[2];
	Npp8u							*hpTableDC[3];
	Npp8u							*hpTableAC[3];
	NppiDCTState				*pDCTState;
	NppiSize						aDstSize[3];
	size_t						nTempSize;
	Npp32s						nScanSize;
	int							i;
	size_t						nPitch;

	// A reference image is required for compression TODO: Make general
	if (!bHaveRef)
		return S_FALSE;

	// Restrictions during development (6 = JCS_EXT_RGB)
	if (ch != 3 || cs != 6)
		return S_FALSE;

	// Access source bits
	if (pBitsSrc->lock ( 0, 0, (void **) &pbSrc, &szSrc ) != NULL)
		return S_FALSE;

	// Using sample exception handling (change this)
	try
		{
		//
		// Step 1 - Copy source image bits to GPU 
		//

		// Initialize structures
		NPP_CHECK_NPP(nppiDCTInitAlloc(&pDCTState));

		// Allocate a square block of memory, routine will return the actual
		// width, in bytes, of each row that was allocated.
		if (w != refCmp.srcW || h != refCmp.srcH)
			{
			// Need to increase memory
			cudaFree(refCmp.pdSrc);
			NPP_CHECK_CUDA(cudaMallocPitch(&(refCmp.pdSrc), &(refCmp.srcPitch), w*3, h));
			refCmp.srcW = w;
			refCmp.srcH = h;

			// Re-allocating source also means YUV needs to be re-allocated
			cudaFree(refCmp.pdYuv[0]); cudaFree(refCmp.pdYuv[1]); cudaFree(refCmp.pdYuv[2]); 
			NPP_CHECK_CUDA(cudaMallocPitch(&(refCmp.pdYuv[0]), &refCmp.yuvPitch[0], refCmp.srcW, refCmp.srcH));
			NPP_CHECK_CUDA(cudaMallocPitch(&(refCmp.pdYuv[1]), &refCmp.yuvPitch[1], refCmp.srcW/2, refCmp.srcH/2));
			NPP_CHECK_CUDA(cudaMallocPitch(&(refCmp.pdYuv[2]), &refCmp.yuvPitch[2], refCmp.srcW/2, refCmp.srcH/2));
			}	// if

		// Copy source bits to GPU
		NPP_CHECK_CUDA(cudaMemcpy2DAsync(refCmp.pdSrc,refCmp.srcPitch,pbSrc,stride,w*3,h,cudaMemcpyHostToDevice));

		//
		// Step 2 - Convert from RGB to YUV420
		//

		// Convert from RGB to YUV420
		int	iPitchYuv[3] = { (int) refCmp.yuvPitch[0], (int) refCmp.yuvPitch[1], (int) refCmp.yuvPitch[2] };
		NPP_CHECK_NPP(nppiRGBToYUV420_8u_C3P3R(refCmp.pdSrc,(int)refCmp.srcPitch,refCmp.pdYuv,iPitchYuv,szSrcRoi));

		//
		// Step 3 - Prepare destination blocks
		//

		// Calculate the number of vertical and horizontal blocks based on reference sampling factors
		for (i = 0; i < 3; ++i)
			{
			nMCUBlocksV = max(nMCUBlocksV, pFrameHeader->aSamplingFactors[i] & 0x0f );
			nMCUBlocksH = max(nMCUBlocksH, pFrameHeader->aSamplingFactors[i] >> 4 );
			}	// for

		// Size and allocate memory for the DCT and destination image arrays.
		for (i = 0;i < 3;++i)
			{
			// Number blocks per minimum coded unit (MCU)
			NppiSize	oBlocks;
			NppiSize	oBlocksPerMCU = { pFrameHeader->aSamplingFactors[i] >> 4, 
												pFrameHeader->aSamplingFactors[i] & 0x0f };

			// Compute number of blocks
			oBlocks.width	= (int)ceil((w + 7) / 8  *
									static_cast<float>(oBlocksPerMCU.width) / nMCUBlocksH);
			oBlocks.width	= DivUp(oBlocks.width, oBlocksPerMCU.width) * oBlocksPerMCU.width;

			oBlocks.height	= (int)ceil((h + 7) / 8 *
									static_cast<float>(oBlocksPerMCU.height) / nMCUBlocksV);
			oBlocks.height = DivUp(oBlocks.height, oBlocksPerMCU.height) * oBlocksPerMCU.height;

			// Size of source data
			aDstSize[i].width		= oBlocks.width * 8;
			aDstSize[i].height	= oBlocks.height * 8;

			// Allocate memory for the device based DCT data.  (* 64 because 8x8 blocks)
			if (oBlocks.width != refCmp.dctW[i] || oBlocks.height != refCmp.dctH[i])
				{
				cudaFree(refCmp.apdDCT[i]);
				NPP_CHECK_CUDA(cudaMallocPitch(&refCmp.apdDCT[i], &nPitch, oBlocks.width * 64 * sizeof(Npp16s), oBlocks.height));
				refCmp.aDCTStep[i]= static_cast<Npp32s>(nPitch);
				refCmp.dctW[i]		= oBlocks.width;
				refCmp.dctH[i]		= oBlocks.height;
				}	// if

			}	// for

		//
		// Step 4 - Copy quantization table to device
		//

		// Zig-Zag order for an MCU to prioritize the lower frequencies.
		Npp8u aZigzag[] = {
			0,  1,  5,  6, 14, 15, 27, 28,
			2,  4,  7, 13, 16, 26, 29, 42,
			3,  8, 12, 17, 25, 30, 41, 43,
			9, 11, 18, 24, 31, 40, 44, 53,
			10, 19, 23, 32, 39, 45, 52, 54,
			20, 22, 33, 38, 46, 51, 55, 60,
			21, 34, 37, 47, 50, 56, 59, 61,
			35, 36, 48, 49, 57, 58, 62, 63
			};

		// This is only done once due to using reference
		if (refCmp.pdQuantizationTables == NULL)
			{
			// Copy the four quantization tables to device
			NPP_CHECK_CUDA(cudaMalloc(&refCmp.pdQuantizationTables, 64 * 4));
			for (int i = 0; i < 4; ++i)
				{
				Npp8u temp[64];

				// Prepare table in the order required by API
				for(int k = 0 ; k < 32 ; ++ k)
					{
					temp[2 * k + 0] = refImage.aQuantizationTables[i].aTable[aZigzag[k +  0]];
					temp[2 * k + 1] = refImage.aQuantizationTables[i].aTable[aZigzag[k + 32]];
					}

				// Copy to device
				NPP_CHECK_CUDA(cudaMemcpyAsync((unsigned char *)refCmp.pdQuantizationTables + i * 64, temp, 64, cudaMemcpyHostToDevice));          
				}
			}	// if

		//
		// Step 5 - Compress YUV420 planar image
		//

		// DCT, quantize and level shift for all three planes
		for (i = 0;i < 3;++i)
			{
			NPP_CHECK_NPP(nppiDCTQuantFwd8x8LS_JPEG_8u16s_C1R_NEW ( refCmp.pdYuv[i], (int)refCmp.yuvPitch[i],
								refCmp.apdDCT[i], (int)refCmp.aDCTStep[i], 
								refCmp.pdQuantizationTables + pFrameHeader->aQuantizationTableSelector[i] * 64,
								aDstSize[i], pDCTState));
			}	// for

		//
		// Step 6 - Huffman encoding
		//

		// Allocate memory for the destination scan
		nScanSize = w * h * 2;
		nScanSize = nScanSize > (4 << 20) ? nScanSize : (4 << 20);    
		if (nScanSize > refCmp.nScanSize)
			{
			cudaFree(refCmp.pdScan);
			NPP_CHECK_CUDA(cudaMalloc(&refCmp.pdScan, nScanSize));
			refCmp.nScanSize = nScanSize;
			}	// if

		// Allocate temporary storage needed by scan routine
		NPP_CHECK_NPP(nppiEncodeHuffmanGetSize(aDstSize[0], 3, &nTempSize));
		if (nTempSize > refCmp.nEncSize)
			{
			cudaFree(refCmp.pJpegEncoder);
			NPP_CHECK_CUDA(cudaMalloc(&refCmp.pJpegEncoder, nTempSize));
			refCmp.nEncSize = nTempSize;
			}	// if

		// Allocate and initialize the Huffman tables, this should only need to be done
		// the first time since this routine uses the reference tables.
		if (refCmp.apHuffmanDCTable[0] == NULL)
			{
			for (int i = 0; i < 3; ++i)
				{
				nppiEncodeHuffmanSpecInitAlloc_JPEG(pHuffmanDCTables[(pScanHeader->aHuffmanTablesSelector[i] >> 4)].aCodes, 
																	nppiDCTable, &refCmp.apHuffmanDCTable[i]);
				nppiEncodeHuffmanSpecInitAlloc_JPEG(pHuffmanACTables[(pScanHeader->aHuffmanTablesSelector[i] & 0x0f)].aCodes, 
																	nppiACTable, &refCmp.apHuffmanACTable[i]);
				}	// for
			}	// if

		// Generate ptrs to codes and table
		for(int i = 0; i < 2; ++ i)
			{
			hpCodesDC[i] = pHuffmanDCTables[i].aCodes;
			hpCodesAC[i] = pHuffmanACTables[i].aCodes;
			hpTableDC[i] = pHuffmanDCTables[i].aTable;
			hpTableAC[i] = pHuffmanACTables[i].aTable;
			}

		// Perform the Huffman encoding
		Npp32s nScanLength;                
		NPP_CHECK_NPP(nppiEncodeOptimizeHuffmanScan_JPEG_8u16s_P3R(refCmp.apdDCT, refCmp.aDCTStep,
								0, pScanHeader->nSs, pScanHeader->nSe, pScanHeader->nA >> 4, pScanHeader->nA & 0x0f,
								refCmp.pdScan, &nScanLength, hpCodesDC, hpTableDC, hpCodesAC, hpTableAC,
								refCmp.apHuffmanDCTable, refCmp.apHuffmanACTable, aDstSize, refCmp.pJpegEncoder));

		//
		// Step 7 - Write output.
		//
		szDst = 512 + nScanLength;
		if (	pBitsDst->setSize ( szDst ) == S_OK && 
				pBitsDst->lock ( 0, 0, (void **) &pbBitsDst, NULL ) == S_OK)
			{
			pDstOutput = pbBitsDst;
			writeMarker(0x0D8, pDstOutput);
			writeJFIFTag(pDstOutput);
			writeQuantizationTable(refImage.aQuantizationTables[0], pDstOutput);
			writeQuantizationTable(refImage.aQuantizationTables[1], pDstOutput);
			pFrameHeader->nWidth		= w;
			pFrameHeader->nHeight	= h;
			writeFrameHeader(*pFrameHeader, pDstOutput);
			writeHuffmanTable(pHuffmanDCTables[0], pDstOutput);
			writeHuffmanTable(pHuffmanACTables[0], pDstOutput);
			writeHuffmanTable(pHuffmanDCTables[1], pDstOutput);
			writeHuffmanTable(pHuffmanACTables[1], pDstOutput);
			writeScanHeader(*pScanHeader, pDstOutput);
			NPP_CHECK_CUDA(cudaMemcpy(pDstOutput, refCmp.pdScan, nScanLength, cudaMemcpyDeviceToHost));
			pDstOutput += nScanLength;
			writeMarker(0x0D9, pDstOutput);
			szDst = (U32)(pDstOutput-pbBitsDst);
			_UNLOCK(pBitsDst,pBitsDst);
			pBitsDst->setSize ( szDst );
			}	// if
		}	// try
	catch ( npp::Exception &e )
		{
		OutputDebugStringA ( e.toString().c_str() );
		OutputDebugStringA ( "\r\n" );
		hr = S_FALSE;
		}	// catch

	// Clean up
	if (pDCTState != NULL)
		nppiDCTFree(pDCTState);

	return hr;
	}	// cudaCompress

HRESULT	cudaDecompress	(	IMemoryMapped *pBitsSrc,
									IMemoryMapped *pBitsDst,
									U32 *w, U32 *h, U32 *bpp, U32 *ch )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decompresses an compressed image.
	//
	//	PARAMETERS
	//		-	pbSrc is the source data
	//		-	iSrc is the amount of source data
	//		-	pbDst will receive the output
	//		-	iDst is the size of the output buffer
	//		-	w,h will receive the image dimensions
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr						= S_OK;
	int					nPos					= 0;
	int					nMarker				= 0;
	int					nMCUBlocksH			= 0;
	int					nMCUBlocksV			= 0;
	int					nRestartInterval	= -1;
	BYTE					*pbSrc				= NULL;
	BYTE					*pbDst				= NULL;
	HuffmanTable		aHuffmanTables[4];
	HuffmanTable		*pHuffmanDCTables = aHuffmanTables;
	HuffmanTable		*pHuffmanACTables = &aHuffmanTables[2];
	FrameHeader			oFrameHeader;
	ScanHeader			oScanHeader;
	QuantizationTable	aQuantizationTables[4];
	NppiSize				aSrcSize[3];
	NppiDCTState		*pDCTState;
	int					i;
	size_t				nPitch;
	U32					iSrc;

	//
	// Step 1 - Parse incoming stream into actionable structures.
	//

	// Lock source bits
	if (pBitsSrc->lock ( 0, 0, (void **) &pbSrc, &iSrc ) != S_OK)
		return S_FALSE;

	// Initialize structures
	memset (	&oFrameHeader,0,sizeof(FrameHeader));
	memset (	aQuantizationTables, 0, 4 * sizeof(QuantizationTable) );
	memset (	aHuffmanTables,0, 4 * sizeof(HuffmanTable));
	NPP_CHECK_NPP(nppiDCTInitAlloc(&pDCTState));

	// Valid JPEG ?
	nMarker = nextMarker ( pbSrc, nPos, iSrc );
	if (nMarker != 0x0D8)
		{
		OutputDebugString ( L"cudaDecompress::Invalid JPEG image\r\n" );
		return E_INVALIDARG;
		}	// if

	// Continue reading markers until the end
	for (	nMarker = nextMarker ( pbSrc, nPos, iSrc);
			nMarker != -1 && hr == S_OK;
			nMarker = nextMarker ( pbSrc, nPos, iSrc) )
		{
		// Process marker
		switch (nMarker)
			{
			// Start of frame
			case 0x0C0 :
				// Baseline header
            readFrameHeader(pbSrc + nPos, oFrameHeader);

				// Only 3 channel supported
				if (oFrameHeader.nComponents != 3)
					{
					hr = E_NOTIMPL;
					break;
					}	// if

				// Compute channel sizes as stored in the JPEG (8x8 blocks & MCU block layout)
				for (i = 0;i < oFrameHeader.nComponents;++i)
					{
					nMCUBlocksV = max(nMCUBlocksV, oFrameHeader.aSamplingFactors[i] & 0x0f );
					nMCUBlocksH = max(nMCUBlocksH, oFrameHeader.aSamplingFactors[i] >> 4 );
					}	// for

				// Size and allocate memory for the DCT and source image arrays.
				for (i = 0;i < oFrameHeader.nComponents;++i)
					{
					// Number blocks per minimum coded unit (MCU)
					NppiSize	oBlocks;
					NppiSize	oBlocksPerMCU = { oFrameHeader.aSamplingFactors[i] >> 4, oFrameHeader.aSamplingFactors[i] & 0x0f };

					// Compute number of blocks
					oBlocks.width	= (int)ceil((oFrameHeader.nWidth + 7) / 8  *
											static_cast<float>(oBlocksPerMCU.width) / nMCUBlocksH);
					oBlocks.width	= DivUp(oBlocks.width, oBlocksPerMCU.width) * oBlocksPerMCU.width;

					oBlocks.height	= (int)ceil((oFrameHeader.nHeight + 7) / 8 *
											static_cast<float>(oBlocksPerMCU.height) / nMCUBlocksV);
					oBlocks.height = DivUp(oBlocks.height, oBlocksPerMCU.height) * oBlocksPerMCU.height;

					// Size of source data
					aSrcSize[i].width		= oBlocks.width * 8;
					aSrcSize[i].height	= oBlocks.height * 8;

					// Allocate memory for the source image
					if (aSrcSize[i].width != refDcmp.srcW[i] || aSrcSize[i].height != refDcmp.srcH[i])
						{
						cudaFree(refDcmp.apSrcImage[i]);
						NPP_CHECK_CUDA(cudaMallocPitch(&refDcmp.apSrcImage[i], &nPitch, aSrcSize[i].width, aSrcSize[i].height));
						refDcmp.srcW[i]				= aSrcSize[i].width;
						refDcmp.srcH[i]				= aSrcSize[i].height;
						refDcmp.aSrcImageStep[i]	= static_cast<Npp32s>(nPitch);
						}	// if

					// Allocate memory for both the host and device based DCT data.  (* 64 because 8x8 blocks)
					if (oBlocks.width != refDcmp.dctW[i] || oBlocks.height != refDcmp.dctH[i])
						{
						cudaFree(refDcmp.apdDCT[i]);
						NPP_CHECK_CUDA(cudaMallocPitch(&refDcmp.apdDCT[i], &nPitch, oBlocks.width * 64 * sizeof(Npp16s), oBlocks.height));
						refDcmp.dctW[i]		= oBlocks.width;
						refDcmp.dctH[i]		= oBlocks.height;
						refDcmp.aDCTStep[i]	= static_cast<Npp32s>(nPitch);
						cudaFree(refDcmp.aphDCT[i]);
						NPP_CHECK_CUDA(cudaHostAlloc(&refDcmp.aphDCT[i], refDcmp.aDCTStep[i] * oBlocks.height, cudaHostAllocDefault));
						}	// if
					}	// for

				// Width and height information
				*w	= aSrcSize[0].width;
				*h	= aSrcSize[0].height;
				hr = S_OK;
				break;

			// Start of scan
			case 0x0DA :
				{
            // Scan
            readScanHeader(pbSrc + nPos, oScanHeader);
            nPos += 6 + oScanHeader.nComponents * 2;

            int nAfterNextMarkerPos = nPos;
            int nAfterScanMarker = nextMarker(pbSrc, nAfterNextMarkerPos, iSrc);

            if (nRestartInterval > 0)
            {
                while (nAfterScanMarker >= 0x0D0 && nAfterScanMarker <= 0x0D7)
                {
                    // This is a restart marker, go on
                    nAfterScanMarker = nextMarker(pbSrc, nAfterNextMarkerPos, iSrc);
                }
            }

				// Huffman decoding.  Currently it is done on the host.
				NppiDecodeHuffmanSpec *apDecHuffmanDCTable[3];
				NppiDecodeHuffmanSpec *apDecHuffmanACTable[3];

				// Allocate and create huffman table from raw data
				for (i = 0;i < 3;++i)
					{
					nppiDecodeHuffmanSpecInitAllocHost_JPEG(pHuffmanDCTables[(oScanHeader.aHuffmanTablesSelector[i] >> 4)].aCodes, nppiDCTable, &apDecHuffmanDCTable[i]);
					nppiDecodeHuffmanSpecInitAllocHost_JPEG(pHuffmanACTables[(oScanHeader.aHuffmanTablesSelector[i] & 0x0f)].aCodes, nppiACTable, &apDecHuffmanACTable[i]);
					}	// for

				// Perform Huffman decoding of the scan data
				NPP_CHECK_NPP(nppiDecodeHuffmanScanHost_JPEG_8u16s_P3R(pbSrc + nPos, nAfterNextMarkerPos - nPos - 2,
																							nRestartInterval, oScanHeader.nSs, oScanHeader.nSe, 
																							oScanHeader.nA >> 4, oScanHeader.nA & 0x0f,
																							refDcmp.aphDCT, refDcmp.aDCTStep,
																							apDecHuffmanDCTable,
																							apDecHuffmanACTable,
																							aSrcSize));

				// Free hufferman table memory
				for (i = 0;i < 3;++i)
					{
					nppiDecodeHuffmanSpecFreeHost_JPEG(apDecHuffmanDCTable[i]);
					nppiDecodeHuffmanSpecFreeHost_JPEG(apDecHuffmanACTable[i]);
					}	// for

				}	// 0xda
				break;

			// Huffman tables
			case 0x0C4 :
				readHuffmanTables(pbSrc + nPos, aHuffmanTables);
				break;

			// Quantization table
			case 0x0DB :
				readQuantizationTables(pbSrc + nPos, aQuantizationTables);
				break;

			// Restart interval
			case 0x0DD :
            readRestartInterval(pbSrc + nPos, nRestartInterval);
				break;

			// Start of frame 2 (progressive ?)
			case 0x0C2 :
				// Not supported
				hr = ERROR_NOT_SUPPORTED;
				break;

			// End of image
			case 0xd9 :
				break;
			}	// switch

		}	// for

	// Performance
//	LARGE_INTEGER	lFreq,lThen,lNow;
//	QueryPerformanceFrequency ( &lFreq );
//	QueryPerformanceCounter ( &lThen );

	//
	// Step 2 - Peform inverse DCT, de-quantization and level shift.
	//
	if (hr == S_OK)
		{
		// Zig-Zag order for an MCU to prioritize the lower frequencies.
		Npp8u aZigzag[] = {
			0,  1,  5,  6, 14, 15, 27, 28,
			2,  4,  7, 13, 16, 26, 29, 42,
			3,  8, 12, 17, 25, 30, 41, 43,
			9, 11, 18, 24, 31, 40, 44, 53,
			10, 19, 23, 32, 39, 45, 52, 54,
			20, 22, 33, 38, 46, 51, 55, 60,
			21, 34, 37, 47, 50, 56, 59, 61,
			35, 36, 48, 49, 57, 58, 62, 63
			};

		// Copy the four quantization tables to device
		if (refDcmp.pdQuantizationTables == NULL)
			{
			NPP_CHECK_CUDA(cudaMalloc(&refDcmp.pdQuantizationTables, 64 * 4));
			}	// if
		for (int i = 0; i < 4; ++i)
			{
			Npp8u temp[64];

			// Prepare table in the order required by API
			for(int k = 0 ; k < 32 ; ++ k)
				{
				temp[2 * k + 0] = aQuantizationTables[i].aTable[aZigzag[k +  0]];
				temp[2 * k + 1] = aQuantizationTables[i].aTable[aZigzag[k + 32]];
				}

			// Copy to device
			NPP_CHECK_CUDA(cudaMemcpyAsync((unsigned char *)refDcmp.pdQuantizationTables + i * 64, temp, 64, cudaMemcpyHostToDevice));          
			}

		// Copy the Huffman decoded DCT entries to device
		for (int i = 0; i < 3; ++ i)
			{
			NPP_CHECK_CUDA(cudaMemcpyAsync(refDcmp.apdDCT[i], refDcmp.aphDCT[i], refDcmp.aDCTStep[i] * aSrcSize[i].height / 8, cudaMemcpyHostToDevice));
			}

		// Invert DCT, de-quantize and level shift for all three planes
		for (int i = 0; i < 3; ++ i)
			{
			NPP_CHECK_NPP(nppiDCTQuantInv8x8LS_JPEG_16s8u_C1R_NEW(refDcmp.apdDCT[i], refDcmp.aDCTStep[i],
																					refDcmp.apSrcImage[i], refDcmp.aSrcImageStep[i],
																					refDcmp.pdQuantizationTables + oFrameHeader.aQuantizationTableSelector[i] * 64,
																					aSrcSize[i],
																					pDCTState));
			}

		}	// if

	//
	// Step 3 - Convert from YCbCr to RGB for external use and copy bits to host.
	//
	if (hr == S_OK)
		{
		// Allocate device memory for conversion destination
		if (aSrcSize[0].width != refDcmp.dstW || aSrcSize[0].height != refDcmp.dstH)
			{
			cudaFree(refDcmp.apDstImage);
			NPP_CHECK_CUDA(cudaMallocPitch(&refDcmp.apDstImage, &refDcmp.aDstImageStep, aSrcSize[0].width*3, aSrcSize[0].height));
			refDcmp.dstW	= aSrcSize[0].width;
			refDcmp.dstH	= aSrcSize[0].height;
			}	// if

		// 3 channel 8-bit unsigned planar YCbCr to 3 channel 8-bit unsigned packed RGB color conversion.
		// NOTE: This conversion is very slow for some reason on the GPU (> 10 ms), figure out why
		NPP_CHECK_NPP(nppiYUV420ToRGB_8u_P3C3R (	refDcmp.apSrcImage, refDcmp.aSrcImageStep, 
																refDcmp.apDstImage, (int)refDcmp.aDstImageStep, aSrcSize[0] ));

		// Ensure enough room in destination and lock bits
		CCLTRY ( pBitsDst->setSize ( aSrcSize[0].width*aSrcSize[0].height*3 ) );
		CCLTRY ( pBitsDst->lock ( 0, 0, (void **) &pbDst, NULL ) );

		// Copy from device to host memory
		if (hr == S_OK)
			{
			NPP_CHECK_CUDA(cudaMemcpy2D(pbDst, aSrcSize[0].width*3, refDcmp.apDstImage, refDcmp.aDstImageStep, 
									aSrcSize[0].width*3, aSrcSize[0].height, cudaMemcpyDeviceToHost));

			// Fill out addtional information
			*w = aSrcSize[0].width;
			*h = aSrcSize[0].height;
			*bpp = 24;
			*ch  = 3;
			}	// if

		// Clean up
		_UNLOCK(pBitsDst,pbDst);
		}	// if

	// Performance
//	QueryPerformanceCounter ( &lNow );
//	double
//	dt = ((lNow.QuadPart-lThen.QuadPart) * 1.0) / lFreq.QuadPart;
//	lprintf ( LOG_INFO, L"Time to decompress %g ms\r\n", dt*1000.0 );

	// Clean up
	if (pDCTState != NULL)
		nppiDCTFree(pDCTState);

	// Need reference data ?  Unfortunately this has to be checked every time, is there
	// a way around this ?
	if (!bHaveRef && bUseRef)
		{
		// Copy over reference information for the just-decompressed image.  This assumes
		// the first image to be decompressed by this library contains the tables to use
		// for all future compressions.
		memcpy ( &(refImage.oFrameHeader), &oFrameHeader, sizeof(oFrameHeader) );
		memcpy ( &(refImage.oScanHeader), &oScanHeader, sizeof(oScanHeader) );
		memcpy ( refImage.aQuantizationTables, aQuantizationTables, sizeof(aQuantizationTables) );
		memcpy ( refImage.aHuffmanTables, aHuffmanTables, sizeof(aHuffmanTables) );

		// Now have reference
		bHaveRef = true;
		}	// if

	return hr;
	}	// cudaDecompress

HRESULT cudaJpegInit ( bool bInit )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Perform startup/shutdown activity.
	//
	//	PARAMETERS
	//		-	bInit is true to initialize, false to uninitialize
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Initialize
	if (bInit)
		{
		// Default to first device, this may need to change in the future 
		// for dual GPU implementations.
		cudaSetDevice(0);

		// This used to be the way to force one-time initialization in the
		// CUDA library, may not be relevant anymore but harmless.
		cudaFree(0);

		// Initialize global structures
		memset ( &refImage, 0, sizeof(refImage) );
		memset ( &refCmp, 0, sizeof(refCmp) );

		// Load/decompress image to use as a 'reference' image. The huffman table
		// and quantization tables will be used for every future compression.
		// This allows a remote device to ignore the tables and hard code the
		// desired tables in the firmware and also makes compression faster.
		#ifdef	_WIN32
/*
		BY_HANDLE_FILE_INFORMATION	hfi;
		HANDLE hFile = CreateFile ( L"xrhs.jpg", GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, 
												NULL, OPEN_EXISTING, 0, NULL );
		if (	hFile != INVALID_HANDLE_VALUE &&
				GetFileInformationByHandle ( hFile, &hfi ) == TRUE )
			{
			BYTE	*pbSrc = NULL;
			BYTE	*pbDst = NULL;
			int	iDst,iSrc,w,h;
			DWORD	nx;

			// Allocate memory for bits
			pbDst = new BYTE[(iDst = 2048*2048*4)];
			pbSrc = new BYTE[(iSrc = hfi.nFileSizeLow)];

			// Read in the data and decompress
			if (	pbDst != NULL && pbSrc != NULL &&
					ReadFile ( hFile, pbSrc, iSrc, &nx, NULL ) == TRUE )
				cudaDecompress ( pbSrc, nx, pbDst, iDst, &w, &h );

			// Clean up
			if (pbSrc != NULL)
				delete[] pbSrc;
			if (pbDst != NULL)
				delete[] pbDst;
			}	// if

		// Clean up
		if (hFile != INVALID_HANDLE_VALUE)
			CloseHandle(hFile);
*/		#endif
		}	// if

	// Uninitialize
	else
		{
		// Compression
		cudaFree(refCmp.pdSrc);
		cudaFree(refCmp.pdYuv[0]);
		cudaFree(refCmp.pdYuv[1]);
		cudaFree(refCmp.pdYuv[2]);
		cudaFree(refCmp.apdDCT[0]);
		cudaFree(refCmp.apdDCT[1]);
		cudaFree(refCmp.apdDCT[2]);
		cudaFree(refCmp.pdScan);
		cudaFree(refCmp.pJpegEncoder);
		cudaFree(refCmp.pdQuantizationTables);

		// Free huffman table ptrs.
		for (int i = 0; i < 3; ++i)
			{
			nppiEncodeHuffmanSpecFree_JPEG(refCmp.apHuffmanDCTable[i]);
			nppiEncodeHuffmanSpecFree_JPEG(refCmp.apHuffmanACTable[i]);
			}	// for
		memset ( &refCmp, 0, sizeof(refCmp) );

		// Decompression
		cudaFree(refDcmp.apDstImage);
		cudaFree(refDcmp.pdQuantizationTables);
		for (int i = 0;i < 3;++i)
			{
			cudaFree(refDcmp.apSrcImage[i]);
			cudaFreeHost(refDcmp.aphDCT[i]);
			cudaFree(refDcmp.apdDCT[i]);
			}	// for
		memset ( &refDcmp, 0, sizeof(refDcmp) );

		}	// else

	return S_OK;
	}	// cudaJpegInit

//
// Helper routines
//

int DivUp(int x, int d)
{
    return (x + d - 1) / d;
}

template<typename T>
T readAndAdvance(const unsigned char *&pData)
{
    T nElement = readBigEndian<T>(pData);
    pData += sizeof(T);
    return nElement;
}

template<typename T>
void writeAndAdvance(unsigned char *&pData, T nElement)
{
    writeBigEndian<T>(pData, nElement);
    pData += sizeof(T);
}

int nextMarker(const unsigned char *pData, int &nPos, int nLength)
{
    unsigned char c = pData[nPos++];

    do
    {
        while (c != 0xffu && nPos < nLength)
        {
            c =  pData[nPos++];
        }

        if (nPos >= nLength)
            return -1;

        c =  pData[nPos++];
    }
    while (c == 0 || c == 0x0ffu);

    return c;
}

void readFrameHeader(const unsigned char *pData, FrameHeader &header)
{
    readAndAdvance<unsigned short>(pData);
    header.nSamplePrecision = readAndAdvance<unsigned char>(pData);
    header.nHeight = readAndAdvance<unsigned short>(pData);
    header.nWidth = readAndAdvance<unsigned short>(pData);
    header.nComponents = readAndAdvance<unsigned char>(pData);

    for (int c=0; c<header.nComponents; ++c)
    {
        header.aComponentIdentifier[c] = readAndAdvance<unsigned char>(pData);
        header.aSamplingFactors[c] = readAndAdvance<unsigned char>(pData);
        header.aQuantizationTableSelector[c] = readAndAdvance<unsigned char>(pData);
    }

}

void readHuffmanTables(const unsigned char *pData, HuffmanTable *pTables)
{
    unsigned short nLength = readAndAdvance<unsigned short>(pData) - 2;

    while (nLength > 0)
    {
        unsigned char nClassAndIdentifier = readAndAdvance<unsigned char>(pData);
        int nClass = nClassAndIdentifier >> 4; // AC or DC
        int nIdentifier = nClassAndIdentifier & 0x0f;
        int nIdx = nClass * 2 + nIdentifier;
        pTables[nIdx].nClassAndIdentifier = nClassAndIdentifier;

        // Number of Codes for Bit Lengths [1..16]
        int nCodeCount = 0;

        for (int i = 0; i < 16; ++i)
        {
            pTables[nIdx].aCodes[i] = readAndAdvance<unsigned char>(pData);
            nCodeCount += pTables[nIdx].aCodes[i];
        }

        memcpy(pTables[nIdx].aTable, pData, nCodeCount);
        pData += nCodeCount;

        nLength -= (17 + nCodeCount);
    }
}

void readQuantizationTables(const unsigned char *pData, QuantizationTable *pTables)
{
    unsigned short nLength = readAndAdvance<unsigned short>(pData) - 2;

    while (nLength > 0)
    {
        unsigned char nPrecisionAndIdentifier = readAndAdvance<unsigned char>(pData);
        int nIdentifier = nPrecisionAndIdentifier & 0x0f;

        pTables[nIdentifier].nPrecisionAndIdentifier = nPrecisionAndIdentifier;
        memcpy(pTables[nIdentifier].aTable, pData, 64);
        pData += 64;

        nLength -= 65;
    }
}

void readRestartInterval(const unsigned char *pData, int &nRestartInterval)
{
    readAndAdvance<unsigned short>(pData);
    nRestartInterval = readAndAdvance<unsigned short>(pData);
}

void readScanHeader(const unsigned char *pData, ScanHeader &header)
{
    readAndAdvance<unsigned short>(pData);

    header.nComponents = readAndAdvance<unsigned char>(pData);

    for (int c=0; c<header.nComponents; ++c)
    {
        header.aComponentSelector[c] = readAndAdvance<unsigned char>(pData);
        header.aHuffmanTablesSelector[c] = readAndAdvance<unsigned char>(pData);
    }

    header.nSs = readAndAdvance<unsigned char>(pData);
    header.nSe = readAndAdvance<unsigned char>(pData);
    header.nA = readAndAdvance<unsigned char>(pData);
}

void writeMarker(unsigned char nMarker, unsigned char *&pData)
{
    *pData++ = 0x0ff;
    *pData++ = nMarker;
}

void writeJFIFTag(unsigned char *&pData)
{
    const char JFIF_TAG[] =
    {
        0x4a, 0x46, 0x49, 0x46, 0x00,
        0x01, 0x02,
        0x00,
        0x00, 0x01, 0x00, 0x01,
        0x00, 0x00
    };

    writeMarker(0x0e0, pData);
    writeAndAdvance<unsigned short>(pData, sizeof(JFIF_TAG) + sizeof(unsigned short));
    memcpy(pData, JFIF_TAG, sizeof(JFIF_TAG));
    pData += sizeof(JFIF_TAG);
}

void writeQuantizationTable(const QuantizationTable &table, unsigned char *&pData)
{
    writeMarker(0x0DB, pData);
    writeAndAdvance<unsigned short>(pData, sizeof(QuantizationTable) + 2);
    memcpy(pData, &table, sizeof(QuantizationTable));
    pData += sizeof(QuantizationTable);
}

void writeFrameHeader(const FrameHeader &header, unsigned char *&pData)
{
    unsigned char aTemp[128];
    unsigned char *pTemp = aTemp;

    writeAndAdvance<unsigned char>(pTemp, header.nSamplePrecision);
    writeAndAdvance<unsigned short>(pTemp, header.nHeight);
    writeAndAdvance<unsigned short>(pTemp, header.nWidth);
    writeAndAdvance<unsigned char>(pTemp, header.nComponents);

    for (int c=0; c<header.nComponents; ++c)
    {
        writeAndAdvance<unsigned char>(pTemp,header.aComponentIdentifier[c]);
        writeAndAdvance<unsigned char>(pTemp,header.aSamplingFactors[c]);
        writeAndAdvance<unsigned char>(pTemp,header.aQuantizationTableSelector[c]);
    }

    unsigned short nLength = (unsigned short)(pTemp - aTemp);

    writeMarker(0x0C0, pData);
    writeAndAdvance<unsigned short>(pData, nLength + 2);
    memcpy(pData, aTemp, nLength);
    pData += nLength;
}

void writeScanHeader(const ScanHeader &header, unsigned char *&pData)
{
    unsigned char aTemp[128];
    unsigned char *pTemp = aTemp;

    writeAndAdvance<unsigned char>(pTemp, header.nComponents);

    for (int c=0; c<header.nComponents; ++c)
    {
        writeAndAdvance<unsigned char>(pTemp,header.aComponentSelector[c]);
        writeAndAdvance<unsigned char>(pTemp,header.aHuffmanTablesSelector[c]);
    }

    writeAndAdvance<unsigned char>(pTemp,  header.nSs);
    writeAndAdvance<unsigned char>(pTemp,  header.nSe);
    writeAndAdvance<unsigned char>(pTemp,  header.nA);

    unsigned short nLength = (unsigned short)(pTemp - aTemp);

    writeMarker(0x0DA, pData);
    writeAndAdvance<unsigned short>(pData, nLength + 2);
    memcpy(pData, aTemp, nLength);
    pData += nLength;
}

void writeHuffmanTable(const HuffmanTable &table, unsigned char *&pData)
{
    writeMarker(0x0C4, pData);

    // Number of Codes for Bit Lengths [1..16]
    int nCodeCount = 0;

    for (int i = 0; i < 16; ++i)
    {
        nCodeCount += table.aCodes[i];
    }

    writeAndAdvance<unsigned short>(pData, 17 + nCodeCount + 2);
    memcpy(pData, &table, 17 + nCodeCount);
    pData += 17 + nCodeCount;
}

#endif

/*
#include "Endianess.h"


#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#define NPP_ASSERT(C) 
//do {if (!(C)) throw npp::Exception(#C " assertion faild!", __FILE__, __LINE__);} while(false)

#define NPP_CHECK_CUDA(S) do {cudaError_t eCUDAResult; \
        eCUDAResult = S; \
        if (eCUDAResult != cudaSuccess) lprintf ( LOG_INFO, L"NPP_CHECK_CUDA - eCUDAResult = %d)", eCUDAResult); \
        NPP_ASSERT(eCUDAResult == cudaSuccess);} while (false)

    /// Macro for checking error return code for NPP calls.
#define NPP_CHECK_NPP(S) do {NppStatus eStatusNPP; \
        eStatusNPP = S; \
        if (eStatusNPP != NPP_SUCCESS) lprintf ( LOG_INFO, L"NPP_CHECK_NPP - eStatusNPP = %d", eStatusNPP ); \
        NPP_ASSERT(eStatusNPP == NPP_SUCCESS);} while (false)

//
// Routines copied from sample
//

struct FrameHeader
{
    unsigned char nSamplePrecision;
    unsigned short nHeight;
    unsigned short nWidth;
    unsigned char nComponents;
    unsigned char aComponentIdentifier[3];
    unsigned char aSamplingFactors[3];
    unsigned char aQuantizationTableSelector[3];
};

struct ScanHeader
{
    unsigned char nComponents;
    unsigned char aComponentSelector[3];
    unsigned char aHuffmanTablesSelector[3];
    unsigned char nSs;
    unsigned char nSe;
    unsigned char nA;
};

struct QuantizationTable
{
    unsigned char nPrecisionAndIdentifier;
    unsigned char aTable[64];
};

struct HuffmanTable
{
    unsigned char nClassAndIdentifier;
    unsigned char aCodes[16];
    unsigned char aTable[256];
};

enum teComponentSampling
{
	YCbCr_444,
	YCbCr_440,
	YCbCr_422,
	YCbCr_420,
	YCbCr_411,
	YCbCr_410,
	YCbCr_UNKNOWN
};

int DivUp(int x, int d)
{
    return (x + d - 1) / d;
}

template<typename T>
T readAndAdvance(const unsigned char *&pData)
{
    T nElement = readBigEndian<T>(pData);
    pData += sizeof(T);
    return nElement;
}

template<typename T>
void writeAndAdvance(unsigned char *&pData, T nElement)
{
    writeBigEndian<T>(pData, nElement);
    pData += sizeof(T);
}


int nextMarker(const unsigned char *pData, int &nPos, int nLength)
{
    unsigned char c = pData[nPos++];

    do
    {
        while (c != 0xffu && nPos < nLength)
        {
            c =  pData[nPos++];
        }

        if (nPos >= nLength)
            return -1;

        c =  pData[nPos++];
    }
    while (c == 0 || c == 0x0ffu);

    return c;
}

void writeMarker(unsigned char nMarker, unsigned char *&pData)
{
    *pData++ = 0x0ff;
    *pData++ = nMarker;
}

void writeJFIFTag(unsigned char *&pData)
{
    const char JFIF_TAG[] =
    {
        0x4a, 0x46, 0x49, 0x46, 0x00,
        0x01, 0x02,
        0x00,
        0x00, 0x01, 0x00, 0x01,
        0x00, 0x00
    };

    writeMarker(0x0e0, pData);
    writeAndAdvance<unsigned short>(pData, sizeof(JFIF_TAG) + sizeof(unsigned short));
    memcpy(pData, JFIF_TAG, sizeof(JFIF_TAG));
    pData += sizeof(JFIF_TAG);
}

void readFrameHeader(const unsigned char *pData, FrameHeader &header)
{
    readAndAdvance<unsigned short>(pData);
    header.nSamplePrecision = readAndAdvance<unsigned char>(pData);
    header.nHeight = readAndAdvance<unsigned short>(pData);
    header.nWidth = readAndAdvance<unsigned short>(pData);
    header.nComponents = readAndAdvance<unsigned char>(pData);

    for (int c=0; c<header.nComponents; ++c)
    {
        header.aComponentIdentifier[c] = readAndAdvance<unsigned char>(pData);
        header.aSamplingFactors[c] = readAndAdvance<unsigned char>(pData);
        header.aQuantizationTableSelector[c] = readAndAdvance<unsigned char>(pData);
    }

}

void writeFrameHeader(const FrameHeader &header, unsigned char *&pData)
{
    unsigned char aTemp[128];
    unsigned char *pTemp = aTemp;

    writeAndAdvance<unsigned char>(pTemp, header.nSamplePrecision);
    writeAndAdvance<unsigned short>(pTemp, header.nHeight);
    writeAndAdvance<unsigned short>(pTemp, header.nWidth);
    writeAndAdvance<unsigned char>(pTemp, header.nComponents);

    for (int c=0; c<header.nComponents; ++c)
    {
        writeAndAdvance<unsigned char>(pTemp,header.aComponentIdentifier[c]);
        writeAndAdvance<unsigned char>(pTemp,header.aSamplingFactors[c]);
        writeAndAdvance<unsigned char>(pTemp,header.aQuantizationTableSelector[c]);
    }

    unsigned short nLength = (unsigned short)(pTemp - aTemp);

    writeMarker(0x0C0, pData);
    writeAndAdvance<unsigned short>(pData, nLength + 2);
    memcpy(pData, aTemp, nLength);
    pData += nLength;
}


void readScanHeader(const unsigned char *pData, ScanHeader &header)
{
    readAndAdvance<unsigned short>(pData);

    header.nComponents = readAndAdvance<unsigned char>(pData);

    for (int c=0; c<header.nComponents; ++c)
    {
        header.aComponentSelector[c] = readAndAdvance<unsigned char>(pData);
        header.aHuffmanTablesSelector[c] = readAndAdvance<unsigned char>(pData);
    }

    header.nSs = readAndAdvance<unsigned char>(pData);
    header.nSe = readAndAdvance<unsigned char>(pData);
    header.nA = readAndAdvance<unsigned char>(pData);
}


void writeScanHeader(const ScanHeader &header, unsigned char *&pData)
{
    unsigned char aTemp[128];
    unsigned char *pTemp = aTemp;

    writeAndAdvance<unsigned char>(pTemp, header.nComponents);

    for (int c=0; c<header.nComponents; ++c)
    {
        writeAndAdvance<unsigned char>(pTemp,header.aComponentSelector[c]);
        writeAndAdvance<unsigned char>(pTemp,header.aHuffmanTablesSelector[c]);
    }

    writeAndAdvance<unsigned char>(pTemp,  header.nSs);
    writeAndAdvance<unsigned char>(pTemp,  header.nSe);
    writeAndAdvance<unsigned char>(pTemp,  header.nA);

    unsigned short nLength = (unsigned short)(pTemp - aTemp);

    writeMarker(0x0DA, pData);
    writeAndAdvance<unsigned short>(pData, nLength + 2);
    memcpy(pData, aTemp, nLength);
    pData += nLength;
}


void readQuantizationTables(const unsigned char *pData, QuantizationTable *pTables)
{
    unsigned short nLength = readAndAdvance<unsigned short>(pData) - 2;

    while (nLength > 0)
    {
        unsigned char nPrecisionAndIdentifier = readAndAdvance<unsigned char>(pData);
        int nIdentifier = nPrecisionAndIdentifier & 0x0f;

        pTables[nIdentifier].nPrecisionAndIdentifier = nPrecisionAndIdentifier;
        memcpy(pTables[nIdentifier].aTable, pData, 64);
        pData += 64;

        nLength -= 65;
    }
}

void writeQuantizationTable(const QuantizationTable &table, unsigned char *&pData)
{
    writeMarker(0x0DB, pData);
    writeAndAdvance<unsigned short>(pData, sizeof(QuantizationTable) + 2);
    memcpy(pData, &table, sizeof(QuantizationTable));
    pData += sizeof(QuantizationTable);
}

void readHuffmanTables(const unsigned char *pData, HuffmanTable *pTables)
{
    unsigned short nLength = readAndAdvance<unsigned short>(pData) - 2;

    while (nLength > 0)
    {
        unsigned char nClassAndIdentifier = readAndAdvance<unsigned char>(pData);
        int nClass = nClassAndIdentifier >> 4; // AC or DC
        int nIdentifier = nClassAndIdentifier & 0x0f;
        int nIdx = nClass * 2 + nIdentifier;
        pTables[nIdx].nClassAndIdentifier = nClassAndIdentifier;

        // Number of Codes for Bit Lengths [1..16]
        int nCodeCount = 0;

        for (int i = 0; i < 16; ++i)
        {
            pTables[nIdx].aCodes[i] = readAndAdvance<unsigned char>(pData);
            nCodeCount += pTables[nIdx].aCodes[i];
        }

        memcpy(pTables[nIdx].aTable, pData, nCodeCount);
        pData += nCodeCount;

        nLength -= (17 + nCodeCount);
    }
}

void writeHuffmanTable(const HuffmanTable &table, unsigned char *&pData)
{
    writeMarker(0x0C4, pData);

    // Number of Codes for Bit Lengths [1..16]
    int nCodeCount = 0;

    for (int i = 0; i < 16; ++i)
    {
        nCodeCount += table.aCodes[i];
    }

    writeAndAdvance<unsigned short>(pData, 17 + nCodeCount + 2);
    memcpy(pData, &table, 17 + nCodeCount);
    pData += 17 + nCodeCount;
}


void readRestartInterval(const unsigned char *pData, int &nRestartInterval)
{
    readAndAdvance<unsigned short>(pData);
    nRestartInterval = readAndAdvance<unsigned short>(pData);
}

bool printfNPPinfo(int argc, char *argv[], int cudaVerMajor, int cudaVerMinor)
{
    const NppLibraryVersion *libVer   = nppGetLibVersion();

    lprintf(LOG_INFO,L"NPP Library Version %d.%d.%d\n", libVer->major, libVer->minor, libVer->build);

    int driverVersion, runtimeVersion;
    cudaDriverGetVersion(&driverVersion);
    cudaRuntimeGetVersion(&runtimeVersion);

    printf("  CUDA Driver  Version: %d.%d\n", driverVersion/1000, (driverVersion%100)/10);
    printf("  CUDA Runtime Version: %d.%d\n", runtimeVersion/1000, (runtimeVersion%100)/10);

//    bool bVal = checkCudaCapabilities(cudaVerMajor, cudaVerMinor);
 //   return bVal;
	return true;
}

//
// Library functions
//

HRESULT cudadecompress (	IMemoryMapped *pBitsSrc,
									IMemoryMapped *pBitsDst,
									U32 *width, U32 *height, U32 *bpp, U32 *ch )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Decompresses a compressed image.
	//
	//	PARAMETERS
	//		-	pBitsSrc is the source bit stream
	//		-	pBitsDst is the destination bit stream
	//		-	width,height,bpp,ch will receive the image parameters
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr					= S_OK;
	U8				*pcBitsSrc		= NULL;
	U8				*pcBitsDst		= NULL;
   float			nScaleFactor	= 1.0f;;
	U32			szSrc;

	// Access and size source bits
	if ((hr = pBitsSrc->lock ( 0, 0, (void **) &pcBitsSrc, &szSrc )) != S_OK)
		return hr;

	NppiDCTState *pDCTState;
	NPP_CHECK_NPP(nppiDCTInitAlloc(&pDCTState));

	// Logic
	int nPos = 0;
	int nMarker = nextMarker(pcBitsSrc, nPos, szSrc);
	if (nMarker != 0x0D8)
		{
		lprintf ( LOG_INFO, L"Invalid Jpeg Image" );
		hr = E_INVALIDARG;
		}

	nMarker = nextMarker(pcBitsSrc, nPos, szSrc);

	// Parsing and Huffman Decoding (on host)
	FrameHeader oFrameHeader;
	QuantizationTable aQuantizationTables[4];
	Npp8u *pdQuantizationTables;
	cudaMalloc(&pdQuantizationTables, 64 * 4);

	HuffmanTable aHuffmanTables[4];
	HuffmanTable *pHuffmanDCTables = aHuffmanTables;
	HuffmanTable *pHuffmanACTables = &aHuffmanTables[2];
	ScanHeader oScanHeader;
	memset(&oFrameHeader,0,sizeof(FrameHeader));
	memset(aQuantizationTables,0, 4 * sizeof(QuantizationTable));
	memset(aHuffmanTables,0, 4 * sizeof(HuffmanTable));
	int nMCUBlocksH = 0;
	int nMCUBlocksV = 0;
	NppiSize aSrcActualSize[3];
	teComponentSampling eComponentSampling = YCbCr_UNKNOWN;
	
	int nRestartInterval = -1;

	NppiSize aSrcSize[3];
	Npp16s *aphDCT[3] = {0,0,0};
	Npp16s *apdDCT[3] = {0,0,0};
	Npp32s aDCTStep[3];

	Npp8u *apSrcImage[3] = {0,0,0};
	Npp32s aSrcImageStep[3];

	while (nMarker != -1)
		{
		if (nMarker == 0x0D8)
			{
			// Embedded Thumbnail, skip it
			int nNextMarker = nextMarker(pcBitsSrc, nPos, szSrc);

			while (nNextMarker != -1 && nNextMarker != 0x0D9)
				{
				nNextMarker = nextMarker(pcBitsSrc, nPos, szSrc);
				}
			}	// if

		if (nMarker == 0x0DD)
			{
			readRestartInterval(pcBitsSrc + nPos, nRestartInterval);
			}

		if ((nMarker == 0x0C0) | (nMarker == 0x0C2))
			{
			//Assert Baseline for this Sample
			//Note: NPP does support progressive jpegs for both encode and decode
			if (nMarker != 0x0C0)
				{
				lprintf ( LOG_INFO, L"The sample does only support baseline JPEG images" );
				hr = E_INVALIDARG;
				}

			// Baseline or Progressive Frame Header
			readFrameHeader(pcBitsSrc + nPos, oFrameHeader);
			lprintf ( LOG_INFO, L"Image Size %d x %d x %d",
							oFrameHeader.nWidth, oFrameHeader.nHeight, static_cast<int>(oFrameHeader.nComponents) );

			// Return information
			*width	= oFrameHeader.nWidth;
			*height	= oFrameHeader.nHeight;
			*bpp		= 24;
			*ch		= 3;

			//Assert 3-Channel Image for this Sample
			if (oFrameHeader.nComponents != 3)
				{
				lprintf ( LOG_INFO, L"The sample does only support color JPEG images" );
				hr = E_INVALIDARG;
				}

			// Compute channel sizes as stored in the JPEG (8x8 blocks & MCU block layout)
			for (int i = 0; i < oFrameHeader.nComponents; ++ i)
				{
				nMCUBlocksV = max(nMCUBlocksV, oFrameHeader.aSamplingFactors[i] & 0x0f );
				nMCUBlocksH = max(nMCUBlocksH, oFrameHeader.aSamplingFactors[i] >> 4 );
				}

			for (int i = 0; i < oFrameHeader.nComponents; ++ i)
				{
				NppiSize oBlocks;
				NppiSize oBlocksPerMCU = {oFrameHeader.aSamplingFactors[i] >> 4, oFrameHeader.aSamplingFactors[i] & 0x0f};

				aSrcActualSize[i].width = DivUp(oFrameHeader.nWidth * oBlocksPerMCU.width, nMCUBlocksH);
				aSrcActualSize[i].height = DivUp(oFrameHeader.nHeight * oBlocksPerMCU.height, nMCUBlocksV);
				
				oBlocks.width = (int)ceil((oFrameHeader.nWidth + 7) / 8  *
										static_cast<float>(oBlocksPerMCU.width) / nMCUBlocksH);
				oBlocks.width = DivUp(oBlocks.width, oBlocksPerMCU.width) * oBlocksPerMCU.width;

				oBlocks.height = (int)ceil((oFrameHeader.nHeight + 7) / 8 *
											static_cast<float>(oBlocksPerMCU.height) / nMCUBlocksV);
				oBlocks.height = DivUp(oBlocks.height, oBlocksPerMCU.height) * oBlocksPerMCU.height;

				aSrcSize[i].width = oBlocks.width * 8;
				aSrcSize[i].height = oBlocks.height * 8;

				// Allocate Memory
				size_t nPitch;
				NPP_CHECK_CUDA(cudaMallocPitch(&apdDCT[i], &nPitch, oBlocks.width * 64 * sizeof(Npp16s), oBlocks.height));
				aDCTStep[i] = static_cast<Npp32s>(nPitch);

				NPP_CHECK_CUDA(cudaMallocPitch(&apSrcImage[i], &nPitch, aSrcSize[i].width, aSrcSize[i].height));
				aSrcImageStep[i] = static_cast<Npp32s>(nPitch);

				NPP_CHECK_CUDA(cudaHostAlloc(&aphDCT[i], aDCTStep[i] * oBlocks.height, cudaHostAllocDefault));
				}
			}	// if

		if (nMarker == 0x0DB)
			{
			// Quantization Tables
			readQuantizationTables(pcBitsSrc + nPos, aQuantizationTables);
			}

		if (nMarker == 0x0C4)
			{
			// Huffman Tables
			readHuffmanTables(pcBitsSrc + nPos, aHuffmanTables);
			}

		if (nMarker == 0x0DA)
			{
			// Scan
			readScanHeader(pcBitsSrc + nPos, oScanHeader);
			nPos += 6 + oScanHeader.nComponents * 2;

			int nAfterNextMarkerPos = nPos;
			int nAfterScanMarker = nextMarker(pcBitsSrc, nAfterNextMarkerPos, szSrc);

			if (nRestartInterval > 0)
				{
					while (nAfterScanMarker >= 0x0D0 && nAfterScanMarker <= 0x0D7)
						{
						// This is a restart marker, go on
						nAfterScanMarker = nextMarker(pcBitsSrc, nAfterNextMarkerPos, szSrc);
						}
				}	// if

			NppiDecodeHuffmanSpec *apDecHuffmanDCTable[3];
			NppiDecodeHuffmanSpec *apDecHuffmanACTable[3];

			for (int i = 0; i < 3; ++i)
				{
				nppiDecodeHuffmanSpecInitAllocHost_JPEG(pHuffmanDCTables[(oScanHeader.aHuffmanTablesSelector[i] >> 4)].aCodes, nppiDCTable, &apDecHuffmanDCTable[i]);
				nppiDecodeHuffmanSpecInitAllocHost_JPEG(pHuffmanACTables[(oScanHeader.aHuffmanTablesSelector[i] & 0x0f)].aCodes, nppiACTable, &apDecHuffmanACTable[i]);
				}

			NPP_CHECK_NPP(nppiDecodeHuffmanScanHost_JPEG_8u16s_P3R(pcBitsSrc + nPos, nAfterNextMarkerPos - nPos - 2,
                                             nRestartInterval, oScanHeader.nSs, oScanHeader.nSe, 
                                             oScanHeader.nA >> 4, oScanHeader.nA & 0x0f,
                                             aphDCT, aDCTStep,
                                             apDecHuffmanDCTable,
                                             apDecHuffmanACTable,
                                             aSrcSize));

			for (int i = 0; i < 3; ++i)
				{
				nppiDecodeHuffmanSpecFreeHost_JPEG(apDecHuffmanDCTable[i]);
				nppiDecodeHuffmanSpecFreeHost_JPEG(apDecHuffmanACTable[i]);
				}
			}	// if

		nMarker = nextMarker(pcBitsSrc, nPos, szSrc);
		}	// while


	// Copy DCT coefficients and Quantization Tables from host to device 
	Npp8u aZigzag[] = {
	0,  1,  5,  6, 14, 15, 27, 28,
	2,  4,  7, 13, 16, 26, 29, 42,
	3,  8, 12, 17, 25, 30, 41, 43,
	9, 11, 18, 24, 31, 40, 44, 53,
	10, 19, 23, 32, 39, 45, 52, 54,
	20, 22, 33, 38, 46, 51, 55, 60,
	21, 34, 37, 47, 50, 56, 59, 61,
	35, 36, 48, 49, 57, 58, 62, 63
	};

	for (int i = 0; i < 4; ++i)
		{
		Npp8u temp[64];

		for(int k = 0 ; k < 32 ; ++ k)
			{
			temp[2 * k + 0] = aQuantizationTables[i].aTable[aZigzag[k +  0]];
			temp[2 * k + 1] = aQuantizationTables[i].aTable[aZigzag[k + 32]];
			}
		NPP_CHECK_CUDA(cudaMemcpyAsync((unsigned char *)pdQuantizationTables + i * 64, temp, 64, cudaMemcpyHostToDevice));          
		}
        
	for (int i = 0; i < 3; ++ i)
		{
		NPP_CHECK_CUDA(cudaMemcpyAsync(apdDCT[i], aphDCT[i], aDCTStep[i] * aSrcSize[i].height / 8, cudaMemcpyHostToDevice));
		}

	// Inverse DCT
	for (int i = 0; i < 3; ++ i)
		{
		NPP_CHECK_NPP(nppiDCTQuantInv8x8LS_JPEG_16s8u_C1R_NEW(apdDCT[i], aDCTStep[i],
                                       apSrcImage[i], aSrcImageStep[i],
                                       pdQuantizationTables + oFrameHeader.aQuantizationTableSelector[i] * 64,
                                       aSrcSize[i],
                                       pDCTState));
		}

	// Convert to RGB

	// Allocate cuda memory for destination image
	Npp8u		*apRgb	= NULL;
	size_t	nPitch	= 0;
	U32		szDst		= 0;
	NppiSize	szRgb;

	NPP_CHECK_CUDA(cudaMallocPitch(&apRgb, &nPitch, *(width)*3, *height));

	// Perform YUV planar data to packed RGB data
	szRgb.width		= *width;
	szRgb.height	= *height;
	NPP_CHECK_NPP(nppiYUVToBGR_8u_P3C3R	( apSrcImage, aSrcImageStep[0], 
						apRgb, nPitch, szRgb));

	// Allocate and access memory for destination
	CCLTRY(pBitsDst->setSize ( (*width)*(*height)*3 ) );
	CCLTRY(pBitsDst->lock ( 0, 0, (void **) &pcBitsDst, &szDst ) );
	if (hr == S_OK)
		{
		NPP_CHECK_CUDA(cudaMemcpy(pcBitsDst, apRgb, szDst, cudaMemcpyDeviceToHost));
		}	// if

	// Free resources
	cudaFree(apRgb);
	cudaFree(pdQuantizationTables);
	nppiDCTFree(pDCTState);
	for (int i = 0; i < 3; ++i)
		{
		cudaFree(apdDCT[i]);
		cudaFreeHost(aphDCT[i]);
		cudaFree(apSrcImage[i]);
		}

	// Clean up
	_UNLOCK(pBitsSrc,pcBitsSrc);

	return hr;
	}	// cudaecompress
*/
