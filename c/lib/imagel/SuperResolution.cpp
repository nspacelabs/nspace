////////////////////////////////////////////////////////////////////////
//
//									SuperResolution.cpp
//
//				Implementation of the image normalization node.
//
////////////////////////////////////////////////////////////////////////

#include "imagel_.h"
#include <stdio.h>

// Globals

#ifdef	HAVE_OPENCV_CONTRIB
SuperResolution :: SuperResolution ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pImg		= NULL;
	pImgs		= NULL;
	pImgsIt	= NULL;
	pDctN		= NULL;
	pRefN		= NULL;
	strType	= L"DualTVL1";
	}	// SuperResolution

HRESULT SuperResolution :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue		vL;

		// Defaults (optional)
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			adtValue::toString ( vL, strType );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDctN);
		_RELEASE(pRefN);
		_RELEASE(pImg);
		_RELEASE(pImgs);
		}	// else

	return hr;
	}	// onAttach

HRESULT SuperResolution :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Execute
	if (_RCP(Fire))
		{
		IDictionary	*pImgUse = NULL;
		cvMatRef		*pMat		= NULL;

		// State check
		CCLTRYE	( pImgsIt != NULL, ERROR_INVALID_STATE );

		// Obtain image refence
		CCLTRY ( Prepare::extract ( pImg, v, &pImgUse, NULL ) );

		// OpenCV uses exceptions
		try
			{
			// Super resolution existing
			if (hr == S_OK && super.empty())
				{
	//			bool	bGPU		= pMat->isGPU();
				cv::Ptr<cv::superres::DenseOpticalFlowExt>
						of;

				// Super resolution object
	//			if (bGPU)
	//				super = cv::superres::createSuperResolution_BTVL1_CUDA();
	//			else
					super = cv::superres::createSuperResolution_BTVL1();

				// Assign optical flow type
				if (!WCASECMP(strType,L"DualTVL1"))
	//				of = (bGPU) ?	cv::superres::createOptFlow_DualTVL1_CUDA() :
					of = cv::superres::createOptFlow_DualTVL1();
				else
					hr = E_INVALIDARG;

				// Assign to object
				CCLOK ( super->setOpticalFlow(of); )
				}	// if

			// Parameters
			CCLOK ( super->setScale(2); )
			CCLOK ( super->setIterations(2); )
			CCLOK ( super->setTemporalAreaRadius(2); )

			// Generate super resolution from list of images
			if (hr == S_OK)
				{
	/*
				std::vector<cv::Mat> chs(cnt);
				IIt						*pIt	= NULL;
				adtValue					vL;

				// Add images from list to OpenCV vector
				cnt = 0;
				CCLTRY ( pImgs->iterate ( &pIt ) );
				while (hr == S_OK && pIt->read ( vL ) == S_OK)
					{
					IDictionary *pDct = NULL;
					cvMatRef		*pRef	= NULL;

					// Obtain image reference
					CCLTRY ( Prepare::extract ( NULL, vL, &pDct, &pRef ) );

					// Add to list
					CCLOK ( chs[cnt++] = *(pRef->mat); )

					// Next image
					_RELEASE(pRef);
					_RELEASE(pDct);
					pIt->next();
					}	// while
	*/
				// Set list and run
				cv::Mat res;
				super->setInput(this);
				super->nextFrame(res);
//				super->nextFrame(res);
//				super->nextFrame(res);
//				super->nextFrame(res);
				lprintf ( LOG_DBG, L"Hi\r\n" );

				// Create a matrix reference object for the image
				CCLTRYE( (pMat = new cvMatRef()) != NULL, E_OUTOFMEMORY );
				CCLTRYE ( (pMat->mat = new cv::Mat()) != NULL, E_OUTOFMEMORY );
				CCLOK  ( res.copyTo ( *(pMat->mat) ); )
				CCLTRY(pImgUse->store ( adtString(L"cvMatRef"), adtIUnknown(*pMat) ) );
				_RELEASE(pMat);

				// Clean up
				_RELEASE(pDctN);
				_RELEASE(pRefN);
				}	// if

			}	// try
		catch ( cv::Exception &ex )
			{
			lprintf ( LOG_ERR, L"%s:Exception:%S", (LPCWSTR)strnName, ex.err.c_str() );
			hr = E_UNEXPECTED;
			}	// catch

		// Results
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pImgUse));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pImgUse);
		}	// if

	// State
	else if (_RCP(Image))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImg);
		_QISAFE(unkV,IID_IDictionary,&pImg);
		}	// else if
	else if (_RCP(Images))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pImgs);
		_RELEASE(pImgsIt);
		CCLTRY(_QISAFE(unkV,IID_IContainer,&pImgs));
		CCLTRY(pImgs->iterate(&pImgsIt));
		}	// else if
	else if (_RCP(Type))
		hr = adtValue::toString ( v, strType );
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

//
// FrameSource members
//

void SuperResolution :: nextFrame ( cv::OutputArray frame )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to obtain the next frame in the list
	//
	//	PARAMETERS
	//		-	frame will receive the next frame in the list
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr = S_OK;
	adtValue		vL;

	// Previous image
	_RELEASE(pDctN);
	_RELEASE(pRefN);

	// Extract 
	CCLTRY ( pImgsIt->read ( vL ) );

	// Obtain image reference
	CCLTRY ( Prepare::extract ( NULL, vL, &pDctN, &pRefN ) );

	// Frame
	if (hr == S_OK)
		{
		if 		(pRefN->isMat())	pRefN->mat->copyTo(frame);
		else if	(pRefN->isUMat())	pRefN->umat->copyTo(frame);
		}	// if

	// Next image
	CCLOK ( pImgsIt->next(); )

	}	// nextFrame

void SuperResolution :: reset ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Reset list to beginning
	//
	////////////////////////////////////////////////////////////////////////

	// Previous image
	_RELEASE(pDctN);
	_RELEASE(pRefN);

	// Reset iteration
	if (pImgsIt != NULL)
		pImgsIt->begin();
	}	// nextFrame


#endif

