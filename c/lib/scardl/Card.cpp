////////////////////////////////////////////////////////////////////////
//
//									Card.CPP
//
//			Implementation of the smart card reader node
//
////////////////////////////////////////////////////////////////////////

#define INITGUID
#include "scardl_.h"

#define MAX_APDU_SIZE 255

Card :: Card ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	hSC		= NULL;
	hCard		= NULL;
	strName	= L"";
	dwProt	= 0;
	pStm		= NULL;
	}	// Card

HRESULT Card :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		// Clean up
		if (hCard != NULL)
			{
			SCardDisconnect(hCard,SCARD_LEAVE_CARD);
			hCard = NULL;
			}	// if
		if (hSC != NULL)
			{
			SCardReleaseContext(hSC);
			hSC = NULL;
			}	// if
		_RELEASE(pStm);
		}	// else

	return hr;
	}	// onAttach

HRESULT Card :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Connect
	if (_RCP(Connect))
		{
		LONG		lRet;

		// State
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );
		CCLTRYE ( hCard == NULL, ERROR_INVALID_STATE );

		// Create a new context for node
		CCLTRYE ( (lRet = SCardEstablishContext ( SCARD_SCOPE_USER, NULL, 
						NULL, &hSC )) == SCARD_S_SUCCESS, lRet );

		// Connect to the card
		CCLTRYE ( (lRet = SCardConnect ( hSC, strName, SCARD_SHARE_SHARED,
						SCARD_PROTOCOL_Tx, &hCard, &dwProt ))
						== SCARD_S_SUCCESS, lRet );

		// Result
		if (hr == S_OK)
			_EMT(Connect,adtLong((U64)hCard));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Disconnect
	else if (_RCP(Disconnect))
		{
		// State
		CCLTRYE ( hCard != NULL, ERROR_INVALID_STATE );

		// Closing down
		_EMT(Disconnect,adtLong((U64)hCard));

		// Clean up
		if (hCard != NULL)
			{
			SCardDisconnect(hCard,SCARD_LEAVE_CARD);
			hCard = NULL;
			}	// if
		if (hSC != NULL)
			{
			SCardReleaseContext(hSC);
			hSC = NULL;
			}	// if

		}	// else if

	// Perform transmit/read
	else if (_RCP(Transmit))
		{
		DWORD dwRx = MAX_APDU_SIZE;
		BYTE	bRx[MAX_APDU_SIZE],bTx[MAX_APDU_SIZE];
		U64	nTx;
		LONG	lRet;

		// State check
		CCLTRYE ( hCard != NULL, ERROR_INVALID_STATE );
		CCLTRYE  ( pStm != NULL, ERROR_INVALID_STATE );

		// Read outgoing data
		CCLTRY ( pStm->read ( bTx, MAX_APDU_SIZE, &nTx ) );

		// Perform transaction
		CCLTRYE ( (lRet = SCardTransmit ( hCard, SCARD_PCI_T1, bTx, 
						(DWORD)nTx, NULL, bRx, &dwRx )) == SCARD_S_SUCCESS, lRet );

		// Append response to stream
		CCLTRY ( pStm->write ( bRx, dwRx, NULL ) );

/*
		// TEST
		if (hr == S_OK)
			{
			U8		bytes[] = { 0xff, 0xca, 0x00, 0x00, 0x00 };
			U8		rx[255];
			DWORD dwRx = sizeof(rx);

			lRet = SCardTransmit ( hCard, SCARD_PCI_T1, bytes, 5, NULL,
											rx, &dwRx );
			lprintf ( LOG_DBG, L"Bytes %d : 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x",
									dwRx, rx[0], rx[1], rx[2], rx[3], rx[4], rx[5] );
			}	// if
*/

		// Result
		if (hr == S_OK)
			_EMT(Transmit,adtIUnknown(pStm));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else if (_RCP(Name))
		adtValue::toString(v,strName);
	else if (_RCP(Stream))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pStm);
		_QISAFE(unkV,IID_IByteStream,&pStm);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

