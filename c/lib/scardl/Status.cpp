////////////////////////////////////////////////////////////////////////
//
//								STATUS.CPP
//
//		Implementation of the smart card status monitoring node.
//
////////////////////////////////////////////////////////////////////////

#include "scardl_.h"

Status :: Status ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pThrd		= NULL;
	bRun		= false;
	hSC		= NULL;
	strRdrs	= NULL;
	pRdrSts	= NULL;
	nRdrs		= 0;
	nRdrStrs	= 0;
	bRdrUp	= false;
	}	// Status

HRESULT Status :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		// Shutdown worker thread if necessary
		onReceive(prStop,adtInt());
		}	// else

	return hr;
	}	// onAttach

HRESULT Status :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Start polling
	if (_RCP(Start))
		{
		// State check
		CCLTRYE ( pThrd == NULL, ERROR_INVALID_STATE );

		// Start asynchronous polling
		CCLOK(bRun = true;)
		CCLTRY(COCREATE(L"Sys.Thread", IID_IThread, &pThrd ));
		CCLTRY(pThrd->threadStart ( this, 5000 ));
		}	// if

	// Stop polling
	else if (_RCP(Stop))
		{
		// Shutdown worker thread if necessary
		if (pThrd != NULL)
			{
			pThrd->threadStop(10000);
			_RELEASE(pThrd);
			}	// if
		}	// else if

	// Add reader string to list
	else if (_RCP(Add))
		{
		adtValue vL;

		// State check
		CCLTRYE ( strName.length() > 0, ERROR_INVALID_STATE );

		// Add reader string to list
		if (hr == S_OK)
			{
			// Re-size string ptr list
			CCLTRYE ( ((strRdrs = (adtString **) 
						_REALLOCMEM(strRdrs,(nRdrStrs+1)*sizeof(adtString *))) != NULL), 
						E_OUTOFMEMORY );

			// Add string
			if (hr == S_OK)
				{
				// Thread safety
				csRdrs.enter();

				// Add string for name
				strRdrs[nRdrStrs] = new adtString(strName);
				strRdrs[nRdrStrs]->at();

				// List was updated
				++nRdrStrs;
				bRdrUp = true;

				// Thread safety
				csRdrs.leave();
				}	// if

			}	// if

		}	// else if

	// State
	else if (_RCP(Name))
		adtValue::toString(v,strName);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

HRESULT Status :: tickAbort ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' should abort.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	bRun = false;
	return S_OK;
	}	// tickAbort

HRESULT Status :: tick ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Perform one 'tick's worth of work.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr			= S_OK;
	LONG		lRet;

	// Reader list updated ?
	if (hr == S_OK && bRdrUp)
		{
		// Thread safety
		csRdrs.enter();

		// Processed
		bRdrUp = false;

		// Resize the list by one to make room for new reader name.
		CCLTRYE ( ((pRdrSts = (SCARD_READERSTATE *) 
					_REALLOCMEM(pRdrSts,nRdrStrs*sizeof(SCARD_READERSTATE))) != NULL), 
					E_OUTOFMEMORY );

		// Add reader to list
		for (nRdrs = 0;hr == S_OK && nRdrs < nRdrStrs;++nRdrs)
			{
			// Initialize structure
			memset ( &pRdrSts[nRdrs], 0, sizeof(SCARD_READERSTATE) );
			pRdrSts[nRdrs].dwCurrentState = SCARD_STATE_UNAWARE;
			pRdrSts[nRdrs].szReader			= *(strRdrs[nRdrs]);
			}	// for

		// Thread safety
		csRdrs.leave();
		}	// if

	// Monitor changes
	CCLOK ( lRet = SCardGetStatusChange ( hSC, 1000, pRdrSts, nRdrs ); )
//		CCLTRYE ( lRet == SCARD_S_SUCCESS || lRet == SCARD_E_TIMEOUT, lRet );

	if (lRet != SCARD_S_SUCCESS && lRet != SCARD_E_TIMEOUT)
		{
		// If the last reader is disconnected, in invalidates the context,
		// attempt restart
		if (lRet == SCARD_E_SERVICE_STOPPED || lRet == SCARD_E_NO_SERVICE)
			{
			// Clean up
			SCardReleaseContext(hSC);
			hSC = NULL;

			// Restart
			tickBegin();

			// Restarted
			_EMT(State,adtString(L"Restart"));
			}	// if

		// 
		else
			{
			lprintf ( LOG_DBG, L"Status change return : 0x%x", lRet );
			hr = lRet;
			}	// else
		}	// if

	// Process any changes
	if (lRet == SCARD_S_SUCCESS)
		{
		// Look for changes
		for (U32 i = 0;hr == S_OK && i < nRdrs;++i)
			{
			// Change ?
//			lprintf ( LOG_DBG, L"%s : 0x%x (0x%x)", 
//				pRdrSts[i].szReader, pRdrSts[i].dwEventState, pRdrSts[i].dwCurrentState );
			if (	pRdrSts[i].dwEventState & SCARD_STATE_CHANGED )
				{
//				lprintf ( LOG_DBG, L"%s : 0x%x (0x%x)", 
//					pRdrSts[i].szReader, pRdrSts[i].dwEventState, pRdrSts[i].dwCurrentState );

				// Mask changes
				DWORD dwChange =  (pRdrSts[i].dwCurrentState ^ pRdrSts[i].dwEventState);

				// Emit appropriately changed states
				_EMT(Name,adtString(pRdrSts[i].szReader));
				if (	(pRdrSts[i].dwEventState & SCARD_STATE_EMPTY) &&
						!(pRdrSts[i].dwCurrentState & SCARD_STATE_EMPTY) )
					_EMT(State,adtString(L"Empty"));
				if (	(pRdrSts[i].dwEventState & SCARD_STATE_PRESENT) &&
						!(pRdrSts[i].dwCurrentState & SCARD_STATE_PRESENT) )
					_EMT(State,adtString(L"Present"));

				// Global reader status
				if (i == 0)
					{
					if (	(pRdrSts[i].dwEventState & 0x10000) &&
							!(pRdrSts[i].dwCurrentState & 0x10000) )
						_EMT(State,adtString(L"Arrive"));
					if (	!(pRdrSts[i].dwEventState & 0x10000) &&
							(pRdrSts[i].dwCurrentState & 0x10000) )
						_EMT(State,adtString(L"Depart"));
					}	// if

				// Update state to current
				pRdrSts[i].dwCurrentState = (pRdrSts[i].dwEventState & (~SCARD_STATE_CHANGED));
				}	// if

			}	// for
		}	// if

	// Continue ?
	CCLTRYE ( bRun == true, S_FALSE );

	return hr;
	}	// tick

HRESULT Status :: tickBegin ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that it should get ready to 'tick'.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;
	LONG		lRet;

	// Create a new context for node
	CCLTRYE ( (lRet = SCardEstablishContext ( SCARD_SCOPE_USER, NULL, 
					NULL, &hSC )) == SCARD_S_SUCCESS, lRet );

	// Add string for overall smart card monitoring
	CCLTRYE ( ((strRdrs = (adtString **) 
				_ALLOCMEM(1*sizeof(adtString *))) != NULL), 
				E_OUTOFMEMORY );
	if (hr == S_OK)
		{
		// String for global status
		strRdrs[0] = new adtString(L"\\\\?PnP?\\Notification");
		strRdrs[0]->at();
		nRdrStrs = 1;

		// Update immediately
		bRdrUp = true;
		}	// if


	return hr;
	}	// tickBegin

HRESULT Status :: tickEnd ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		ITickable
	//
	//	PURPOSE
	//		-	Notifies the object that 'ticking' is to stop.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	for (U32 i = 0;i < nRdrStrs;++i)
		if (strRdrs[i] != NULL)
			delete strRdrs[i];
	_FREEMEM(strRdrs);
	_FREEMEM(pRdrSts);
	nRdrs = 0;
	if (hSC != NULL)
		{
		SCardReleaseContext(hSC);
		hSC = NULL;
		}	// if

	return S_OK;
	}	// tickEnd

