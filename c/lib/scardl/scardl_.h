////////////////////////////////////////////////////////////////////////
//
//										SCARDL_.H
//
//				Implementation include file for the smart card library
//
////////////////////////////////////////////////////////////////////////

#ifndef	SCARDL__H
#define	SCARDL__H

// Includes
#include "scardl.h"
#ifdef	_WIN32
#include <winscard.h>
#endif

///////////
// Objects
///////////

/////////
// Nodes
/////////

//
// Class - Card.  Card I/O function node.
//

class Card :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Card ( void );											// Constructor

	// Run-time data
	SCARDCONTEXT	hSC;									// Smart card context
	SCARDHANDLE		hCard;								// Smart card handle
	adtString		strName;								// Reader name
	DWORD				dwProt;								// Selected protocol
	IByteStream		*pStm;								// Input/output stream

	// CCL
	CCL_OBJECT_BEGIN(Card)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Connect)
	DECLARE_CON(Disconnect)
	DECLARE_RCP(Name)
	DECLARE_RCP(Stream)
	DECLARE_CON(Transmit)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Connect)
		DEFINE_CON(Disconnect)
		DEFINE_CON(Transmit)
		DEFINE_RCP(Name)
		DEFINE_RCP(Stream)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Enum.  Enumerate/find smart card readers in system.
//

class Enum :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Enum ( void );											// Constructor

	// Run-time data
	SCARDCONTEXT	hSC;									// Smart card context
	LPTSTR			szReaders,szRdrEnum;				// Reader string list

	// CCL
	CCL_OBJECT_BEGIN(Enum)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(First)
	DECLARE_CON(Next)
	DECLARE_EMT(End)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(First)
		DEFINE_CON(Next)
		DEFINE_EMT(End)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Status.  Statusing monitoring node for smart cards.
//

class Status :
	public CCLObject,										// Base class
	public Behaviour,										// Interface
	public ITickable										// Interface
	{
	public :
	Status ( void );										// Constructor

	// Run-time data
	IThread				*pThrd;							// Asynchronous read thread
	bool					bRun;								// Async should run ?
	adtString			strName;							// Reader name
	SCARDCONTEXT		hSC;								// Smart card context
	SCARD_READERSTATE	*pRdrSts;						// Reader states
	U32					nRdrs;							// Reader state count
	adtString			**strRdrs;						// Name array
	U32					nRdrStrs;						// Reader string count
	bool					bRdrUp;							// Reader list update
	sysCS					csRdrs;							// Reader list protection

	// 'ITickable' members
	STDMETHOD(tick)		( void );
	STDMETHOD(tickAbort)	( void );
	STDMETHOD(tickBegin)	( void );
	STDMETHOD(tickEnd)	( void );

	// CCL
	CCL_OBJECT_BEGIN(Status)
		CCL_INTF(IBehaviour)
		CCL_INTF(ITickable)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Add)
	DECLARE_RCP(Remove)
	DECLARE_RCP(Start)
	DECLARE_RCP(Stop)
	DECLARE_EMT(Error)
	DECLARE_EMT(Fire)
	DECLARE_CON(Name)
	DECLARE_EMT(State)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Add)
		DEFINE_RCP(Remove)
		DEFINE_RCP(Start)
		DEFINE_RCP(Stop)
		DEFINE_EMT(Error)
		DEFINE_EMT(Fire)
		DEFINE_CON(Name)
		DEFINE_EMT(State)
	END_BEHAVIOUR_NOTIFY()
	};

#endif
