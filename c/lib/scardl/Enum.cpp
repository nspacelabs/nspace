////////////////////////////////////////////////////////////////////////
//
//									Enum.CPP
//
//			Implementation of the smart card reader enumeration node
//
////////////////////////////////////////////////////////////////////////

#include "scardl_.h"

Enum :: Enum ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////

	// Setup
	hSC			= NULL;
	szReaders	= NULL;
	}	// Enum

HRESULT Enum :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		}	// if

	// Detach
	else
		{
		// Clean up
		if (szReaders != NULL)
			{
			SCardFreeMemory(hSC,szReaders);
			szReaders = NULL;
			}	// if
		if (hSC != NULL)
			{
			SCardReleaseContext(hSC);
			hSC = NULL;
			}	// if
		}	// else

	return hr;
	}	// onAttach

HRESULT Enum :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Enumerate
	if (_RCP(First))
		{
		DWORD		cchReaders	= SCARD_AUTOALLOCATE;
		LONG		lRet;
	
		// Need context ?
		if (hr == S_OK && hSC == NULL)
			{
			// Create a new context for node
			CCLTRYE ( (lRet = SCardEstablishContext ( SCARD_SCOPE_USER, NULL, 
							NULL, &hSC )) == SCARD_S_SUCCESS, lRet );
			}	// if

		// State check
		CCLTRYE ( hSC != NULL, E_UNEXPECTED );
		if (hr == S_OK)
			{
			// Retrieve list of readers
			lRet = SCardListReaders ( hSC, NULL, (LPTSTR)&szReaders,
						&cchReaders );
			szRdrEnum	= szReaders;

			// Result
			if (lRet == SCARD_E_NO_READERS_AVAILABLE)
				lprintf ( LOG_DBG, L"No smart card readers found" );
			else if (lRet != SCARD_S_SUCCESS)
				lprintf ( LOG_DBG, L"Smart card enumeration failed : %d", lRet );
			}	// if

		// Emit next card
		CCLOK ( onReceive ( prNext, v ); )
		}	// if

	// Next card
	else if (_RCP(Next))
		{
		adtString	strRdr;

		// State check
		CCLTRYE ( szRdrEnum != NULL, ERROR_INVALID_STATE );

		// Next reader and own copy
		CCLOK ( strRdr = szRdrEnum; )
		CCLOK ( strRdr.at(); )

		// Advance to next name
		if (hr == S_OK && *szRdrEnum != '\0')
			szRdrEnum = szRdrEnum + wcslen(szRdrEnum) + 1;

		// Emit next card or signal end
		if (strRdr.length() > 0)
			_EMT(Next,strRdr);
		else
			_EMT(End,adtInt(0));
		}	// else if

	// State
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

