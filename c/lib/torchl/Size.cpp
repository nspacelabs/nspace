////////////////////////////////////////////////////////////////////////
//
//									Size.CPP
//
//				Implementation of the Size tensor node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Size :: Size ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	}	// Size

HRESULT Size :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
//			onReceive(prSize,vL);

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Size :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Retrieve size
	if (_RCP(Fire))
		{
		refTorch	*pRef	= NULL;
//		adtInt		iDim(v);
		adtValue		vL;
		adtLong		lSz;

		// The dimension for which to retrieve the size can
		// be sent in with the request.

		// Extract tensor
		CCLTRY ( refTorch::from ( adtIUnknown(pDct), L"Tensor", &pRef ) );

		// Obtain size of dimension
		CCLOK ( lSz = pRef->tensor->size(0); )

		// Clean up
		_RELEASE(pRef);

		// Result
		if (hr == S_OK)
			_EMT(Fire,lSz);
		else
			_EMT(Error,adtInt(hr));

		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

