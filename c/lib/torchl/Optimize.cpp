////////////////////////////////////////////////////////////////////////
//
//									Optimize.CPP
//
//				Implementation of the network optimization node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Optimize :: Optimize ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	strNet	= L"Net";									// Default
	}	// Optimize

HRESULT Optimize :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			onReceive(prType,vL);
		if (pnDesc->load ( adtString(L"Net"), vL ) == S_OK)
			hr = adtValue::toString ( vL, strNet );

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Optimize :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Optimize/step
	if (_RCP(Fire))
		{
		refTorch	*pRefOpt	= NULL;

		// Extract optimizer
		CCLTRY ( refTorch::from ( pDct, strnName, &pRefOpt ) );

		// Step optimizer
		CCLOK ( pRefOpt->opt->step(); )

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		
		// Clean up
		_RELEASE(pRefOpt);
		}	// else if

	// Reset optimizer state
	else if (_RCP(Reset))
		{
		refTorch		*pRefOpt	= NULL;
		refTorch		*pRefNet	= NULL;
		adtValue		vL;
		adtIUnknown	unkV;

		// Extract needed net
		CCLTRY ( refTorch::from ( pDct, strNet, &pRefNet ) );

		// Optimizer present yet ?
		if (hr == S_OK && pDct->load ( strnName, vL ) != S_OK)
			{
			// New reference object
			CCLTRYE ( (pRefOpt = new refTorch()) != NULL, E_OUTOFMEMORY );

			// Create optimizer with networking parameters
			if (!WCASECMP(strType,L"SGD"))
				{
				CCLTRYE ( (pRefOpt->opt = std::make_shared<torch::optim::SGD>
								(pRefNet->module->parameters(),0.5f))!= nullptr, E_OUTOFMEMORY );
				}	// if
			else if (!WCASECMP(strType,L"Adam"))
				{
				CCLTRYE ( (pRefOpt->opt = std::make_shared<torch::optim::Adam>
								(pRefNet->module->parameters(),0.5f))!= nullptr, E_OUTOFMEMORY );
				}	// if
			else
				hr = E_NOTIMPL;

			// Store in context
			CCLTRY ( refTorch::to ( pDct, strnName, pRefOpt ) );
			}	// if
		else
			{
			// Exists, extract reference
			CCLTRY ( refTorch::from ( pDct, strnName, &pRefOpt ) );
			}	// else

		// Reset optimizer
		CCLOK ( pRefOpt->opt->zero_grad(); )

		// Clean up
		_RELEASE(pRefOpt);
		_RELEASE(pRefNet);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Type))
		hr = adtValue::toString(v,strType);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

