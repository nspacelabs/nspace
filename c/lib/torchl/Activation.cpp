////////////////////////////////////////////////////////////////////////
//
//									Activation.CPP
//
//				Implementation of the activations node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Activation :: Activation ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	}	// Activation

HRESULT Activation :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
//			onReceive(prSize,vL);

		}	// if

	// Detach
	else
		{
		}	// else

	return hr;
	}	// onAttach

HRESULT Activation :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Activation
	if (_RCP(Relu) || _RCP(SoftmaxLog))
		{
		refTorch	*pRef	= NULL;

		// Extract tensor reference
		CCLTRY ( refTorch::from(v,L"Tensor",&pRef) );
		if (hr == S_OK)
			{
			// Apply activation function
			if (_RCP(Relu))
				*(pRef->tensor) = torch::relu(*(pRef->tensor));
			else if (_RCP(SoftmaxLog))
				*(pRef->tensor) = torch::log_softmax(*(pRef->tensor),1);
			}	// if

		// Clean up
		_RELEASE(pRef);

		// Result
		if (hr == S_OK)
			_EMT(Fire,v);
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

