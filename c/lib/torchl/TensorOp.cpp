////////////////////////////////////////////////////////////////////////
//
//									TensorOp.CPP
//
//					Implementation of the tensor operatios node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

TensorOp :: TensorOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	}	// TensorOp

HRESULT TensorOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			onReceive(prSize,vL);

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT TensorOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Create tensor of zeroes of 'list size's
	if (_RCP(Zeroes))
		{
		refTorch	*pRef	= NULL;

		// State check
		CCLTRYE ( pDct != NULL,			ERROR_INVALID_STATE );
		CCLTRYE ( (vSz.size() > 0),	ERROR_INVALID_STATE );

		// Create a reference for the tensor
		CCLTRYE( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE( (pRef->tensor = std::make_shared<torch::Tensor>())
						!= nullptr, E_OUTOFMEMORY );

		// Allocate for zeroes
//		CCLOK ( *(pRef->tensor) = torch::zeros(vSz,torch::kF32); )

		// Store reference in context
		CCLTRY ( pRef->to ( adtIUnknown(pDct), L"Tensor", pRef ) );

		// Clean up
		_RELEASE(pRef);

		// Result
		if (hr == S_OK)
			_EMT(Zeroes,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Tensor from data
	else if (_RCP(From))
		{
		refTorch					*pRef		= NULL;
		IContainer				*pLsts	= NULL;
		IContainer				*pLst		= NULL;
		IIt						*pLstsIt	= NULL;
		IIt						*pIt		= NULL;
		U32						nl			= 0;
		U32						nv			= 0;
		U32						i,j;
		float						*pfdata	= NULL;
		adtIUnknown				unkV(v);
		adtValue					vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Reset dimension state

		// Currently supports a list of lists.  List counts must be the same.
		// This will probably have to expand over time.
		CCLTRY ( _QISAFE(unkV,IID_IContainer,&pLsts) );
		CCLTRY ( pLsts->size ( &nl ) );

		// Obtain the length of the first list
		CCLTRY ( pLsts->iterate ( &pLstsIt ) );
		CCLTRY ( pLstsIt->read ( vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IContainer,&pLst) );
		CCLTRY ( pLst->size ( &nv ) );
		_RELEASE(pLst);

		// Create a new tensor with the required dimensions.
		// For feeding lists of values into a model, libtorch does it
		// by concatenating everything into a big list.
		// Order matters for dimensions.
		CCLTRYE( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE( (pRef->tensor = std::make_shared<torch::Tensor>())
						!= nullptr, E_OUTOFMEMORY );
		CCLOK ( *(pRef->tensor) = torch::zeros({nv,nl},torch::kF32); )

		// Directly inject values from lists into tensor
		CCLTRYE ( (pfdata = pRef->tensor->data_ptr<float>()) != NULL,
						E_UNEXPECTED );
		CCLOK ( pLstsIt->begin(); )
		for (i = 0;hr == S_OK && i < nl && pLstsIt->read ( vL ) == S_OK;++i,pLstsIt->next())
			{
			// Iterate the raw values in the current list
			CCLTRY ( _QISAFE((unkV=vL),IID_IContainer,&pLst) );
			CCLTRY ( pLst->iterate ( &pIt ) );
			for (j = 0;hr == S_OK && j < nv && pIt->read ( vL ) == S_OK;++j,pIt->next())
				// Torch tensors are row-major
				pfdata[j*nl+i] = adtFloat(vL);

				// Left here to show the wrong way, column-major
//				pfdata[i*nv+j] = adtFloat(vL);

			// Clean up
			_RELEASE(pIt);
			_RELEASE(pLst);
			}	// for

		// Store reference in context
		CCLTRY ( refTorch::to ( pDct, L"Tensor", pRef ) );

		// Result
		if (hr == S_OK)
			_EMT(From,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRef);
		_RELEASE(pLstsIt);
		_RELEASE(pLsts);
/*
		refTorch					*pRef		= NULL;
		IContainer				*pLst		= NULL;
		IIt						*pIt		= NULL;
		U32						nv			= 0;
		float						*pfdata	= NULL;
		U32						j;
		adtIUnknown				unkV(v);
		std::vector<int64_t>	nDim;
		adtValue					vL;

		// Create a tensor from a single list of values.

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Obtain the length of the list and iterate the value
		CCLTRY ( _QISAFE(unkV,IID_IContainer,&pLst) );
		CCLTRY ( pLst->size ( &nv ) );
		CCLTRY ( pLst->iterate ( &pIt ) );
		_RELEASE(pLst);

		// Create a new tensor with the required dimensions
		CCLOK ( nDim.push_back ( nv ); )
		CCLTRYE( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE( (pRef->tensor = std::make_shared<torch::Tensor>())
						!= nullptr, E_OUTOFMEMORY );
		CCLOK ( *(pRef->tensor) = torch::zeros(nDim,torch::kF32); )

		// Directly inject values from lists into tensor
		CCLTRYE ( (pfdata = pRef->tensor->data_ptr<float>()) != NULL,
						E_UNEXPECTED );
		for (j = 0;hr == S_OK && j < nv && pIt->read ( vL ) == S_OK;++j,pIt->next())
			pfdata[j] = adtFloat(vL);

		// Store reference in context
		CCLTRY ( pRef->to ( adtIUnknown(pDct), L"Tensor", pRef ) );

		// Result
		if (hr == S_OK)
			_EMT(From,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pIt);
		_RELEASE(pRef);
*/

		}	// else if

	// Tensor to data
	else if (_RCP(To))
		{
		refTorch				*pRef		= NULL;
		IList					*pLsts	= NULL;
		IList					*pLst		= NULL;
		c10::IntArrayRef	aSz;
		U32					i,j;

		// Obtain tensor reference
		CCLTRY ( refTorch::from ( pDct, L"Tensor", &pRef ) );

		// Obtain the size/dimensionality
		CCLOK ( aSz = pRef->tensor->sizes(); )

		// Currently limited to 2D, update if necessary
		CCLTRYE ( aSz.size() == 2, E_NOTIMPL );

		// Create master list
		CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLsts ) );

		// Transfer elements from tensor to lists
		for (i = 0;hr == S_OK && i < aSz[1];++i)
			{
			// Create and add sublist
			CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLst ) );
			CCLTRY ( pLsts->write ( adtIUnknown(pLst) ) );

			// Transfer elements from tensor to list
			for (j = 0;hr == S_OK && j < aSz[0];++j)
				pLst->write ( adtFloat((*(pRef->tensor))[j][i].item<float>()) );

			// Clean up
			_RELEASE(pLst);
			}	// for

		// Result
		if (hr == S_OK)
			_EMT(To,adtIUnknown(pLsts));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pLsts);
		_RELEASE(pRef);
		}	// else if


	// Clone current tensor into provided dictionary
	else if (_RCP(Clone))
		{
		refTorch		*pRef		= NULL;
		refTorch		*pRefDst	= NULL;
		IDictionary	*pDctDst	= NULL;
		adtIUnknown	unkV(v);

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Extract this tensor
		CCLTRY ( refTorch::from ( pDct, L"Tensor", &pRef ) );

		// Target dictionary
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDctDst) );

		// Create new tensor
		CCLTRYE( (pRefDst = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE( (pRefDst->tensor = std::make_shared<torch::Tensor>())
						!= nullptr, E_OUTOFMEMORY );

		// Create the clone the 'proper' way
		CCLOK ( *(pRefDst->tensor) = (*(pRef->tensor)).detach().clone(); )

		// Store in provided dictionary
		CCLTRY (	refTorch::to ( pDctDst, L"Tensor", pRefDst ) );

		// Result
		if (hr == S_OK)
			_EMT(Clone,v);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRefDst);
		_RELEASE(pDctDst);
		_RELEASE(pRef);
		}	// else if

	// Size
	else if (_RCP(Size))
		{
		IContainer	*pCnt	= NULL;
		IIt			*pIt	= NULL;
		adtIUnknown	unkV(v);
		adtValue		vL;

		// Expecting a list of sizes for each dimension
		CCLOK  ( vSz.clear(); )
		CCLTRY ( _QISAFE(unkV,IID_IContainer,&pCnt) );
		CCLTRY ( pCnt->iterate ( &pIt ) );
		while (hr == S_OK && pIt->read ( vL ) == S_OK)
			{
			// Add size to list
			vSz.push_back(adtLong(vL));

			// Next size
			pIt->next();
			}	// while

		// Clean up
		_RELEASE(pIt);
		_RELEASE(pCnt);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

