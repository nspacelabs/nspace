////////////////////////////////////////////////////////////////////////
//
//									NET.CPP
//
//				Implementation of the net node.
//
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Net :: Net ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	}	// Net

HRESULT Net :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
//		adtValue vL;

		// Options
//		if (pnDesc->load ( adtString(L"Name"), vL ) == S_OK)
//			adtValue::toString ( vL, strName );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Net :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Add net
	if (_RCP(Add))
		{
		refTorch	*pRef	= NULL;
		adtValue	vL;

		// State check
		CCLTRYE (	pDct != NULL && 
						pDct->load ( adtString(L"Net"), vL ) != S_OK,
						ERROR_INVALID_STATE );

		// Create an empty module to use as container for future entries.
		CCLTRYE	( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE ( (pRef->module = std::make_shared<torch::nn::Module>()) 
						!= nullptr, E_OUTOFMEMORY );
		CCLTRY	( refTorch::to ( pDct, strnName, pRef ) );

		// Result
		if (hr != S_OK)
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRef);
		}	// else if

	// Fire. 'Fire' for a net is the 'forward' call.  The model
	// will be applied in the graph.
	else if (_RCP(Fire))
		{
		// Forward the signal with the data
		_EMT(Fire,v);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

