////////////////////////////////////////////////////////////////////////
//
//									DataLoader.CPP
//
//		Implementation of the predefined dataset loader node.
//
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

DataLoader :: DataLoader ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pIt		= NULL;
	iBatchSz = 64;
	}	// DataLoader

HRESULT DataLoader :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Name"), vL ) == S_OK)
			onReceive(prName,vL);

		// Debug
//		torch::Tensor	tensor = torch::eye(3);
//		std::stringstream bfr;
//		bfr << tensor << std::endl;
//		lprintf ( LOG_DBG, L"%S\r\n", bfr.str().c_str() );
//		lprintf ( LOG_DBG, L"Is cuda available : %s\r\n",
//						(torch::cuda::is_available()) ? L"true" : L"false" );
		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pIt);
		}	// else

	return hr;
	}	// onAttach

HRESULT DataLoader :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// First/next batch
	if (_RCP(First))
		{
		char	*pcLoc	= NULL;
		IList	*pLst		= NULL;

		// TODO: Optimize
		_RELEASE(pIt);

		// Need updating ?
		if (pIt == NULL)
			{
			// Libtorch uses exceptions.
			try
				{
				adtString	strLocLcl(strLoc);

				// ASCII version of location required by API
				CCLTRY ( strLocLcl.toAscii(&pcLoc) );

				// No trailing slashes for API
				if (pcLoc[strlen(pcLoc)-1] == '/')
					pcLoc[strlen(pcLoc)-1] = '\0';

				// Create list
				CCLTRY ( COCREATE ( L"Adt.List", IID_IList, &pLst ) );

				// Generate list
				if (hr == S_OK)
					{
					// Try to persist the C++ template stuff over multiple iterations
					// just isn't worth it so load all of the batches and place
					// into own list for iteration.

					// Data set access.  This logic will need to change to make
					// it more general if more datasets are to be supported.
					// Normalize data to -1 to +1 from +0 to +1.  Stack
					// tensors along first dimension.
					auto dataset = torch::data::datasets::MNIST(pcLoc)
									.map(torch::data::transforms::Normalize<>(0.5,0.5))
									.map(torch::data::transforms::Stack<>());
 
					// Create a data-loader for the dataset
					auto dataldr = torch::data::make_data_loader ( std::move(dataset),
							torch::data::DataLoaderOptions().batch_size(iBatchSz) );

					// Load batches of data and add to list
					int dbg = 0;
					for (auto& batch : *dataldr)
						{
						IDictionary	*pDctT	= NULL;
						refTorch		*pRef		= NULL;

						// Writing alternating data and targets inside dictionary tensors

						// Training images
						CCLTRY	( COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctT));
						CCLTRYE	( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
						CCLOK		( *(pRef->tensor) = batch.data; )
						CCLTRY	( refTorch::to(pDctT,L"Tensor",pRef) );
						CCLTRY	( pLst->write(adtIUnknown(pDctT)) );
						_RELEASE(pRef);
						_RELEASE(pDctT);

						// Target images
						CCLTRY	( COCREATE(L"Adt.Dictionary",IID_IDictionary,&pDctT));
						CCLTRYE	( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
						CCLOK		( *(pRef->tensor) = batch.target; )
						CCLTRY	( refTorch::to(pDctT,L"Tensor",pRef) );
						CCLTRY	( pLst->write(adtIUnknown(pDctT)) );
						_RELEASE(pRef);
						_RELEASE(pDctT);

						// Debug
	//					if (dbg++ >= 100)
	//						break;
						}	// for
					}	// if

				// Create iterator for list
				CCLTRY ( pLst->iterate ( &pIt ) );

				// Debug
				U32 cnt = 0;
				CCLTRY(pLst->size(&cnt));
				lprintf ( LOG_DBG, L"Image list count %d (0x%x)\r\n", cnt, hr );
				}	// try
			catch ( const c10::Error &ex )
				{
				lprintf ( LOG_DBG, L"Exception: %S\r\n", ex.msg().c_str() );
				hr = E_FAIL;
				}	// catch

			}	// if

		// Ensure iterator is reset
		CCLTRYE ( pIt != NULL, E_UNEXPECTED );
		CCLOK ( pIt->begin(); )

		// Clean up
		_RELEASE(pLst);
		_FREEMEM(pcLoc);

		// Proceed to next image
		if (hr == S_OK)
			hr = onReceive(prNext,v);
		else
			_EMT(End,adtInt());
		}	// if
	else if (_RCP(Next))
		{
		adtValue		vL;

		// State check
		CCLTRYE ( pIt != NULL, ERROR_INVALID_STATE );

		// Obtain next set
		CCLTRY ( pIt->read ( vL ) );

		// For next iteration
		CCLOK ( pIt->next(); )

		// Result
		if (hr == S_OK)
			_EMT(Next,vL);
		else
			_EMT(End,adtInt());

		}	// else if

	// State
	else if (_RCP(Name))
		hr = adtValue::toString(v,strName);
	else if (_RCP(Location))
		{
		hr		= adtValue::toString(v,strLoc);
		_RELEASE(pIt);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

