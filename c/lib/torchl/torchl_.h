////////////////////////////////////////////////////////////////////////
//
//										TORCHL_.H
//
//			Implementation include file for Libtorch library
//
////////////////////////////////////////////////////////////////////////

#ifndef	TORCHL__H
#define	TORCHL__H

// Includes
#undef	NOMINMAX
#define	NOMINMAX
#include	"torchl.h"

// 3rd party

// Reduce some warning in the include files
#pragma warning( disable : 4275)
#pragma warning( disable : 4251)
#pragma warning( disable : 4624)
#pragma warning( disable : 4244)
#include <torch/torch.h>

// Forwards
class Net;

/////////
// Nodes
/////////

//
// Class - Activation.  Activations node
// 

class Activation :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Activation ( void );									// Constructor

	// Run-time data

	// CCL
	CCL_OBJECT_BEGIN(Activation)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_EMT(Fire)
	DECLARE_RCP(Relu)
	DECLARE_RCP(SoftmaxLog)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_EMT(Fire)
		DEFINE_RCP(Relu)
		DEFINE_RCP(SoftmaxLog)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - DataLoader.  Predefined dataset loader/iterator.
//

class DataLoader :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	DataLoader ( void );									// Constructor

	// Run-time data
	adtString		strName;								// Dataset name
	adtString		strLoc;								// Dataset location
	IIt				*pIt;									// Image iterator
	adtInt			iBatchSz;							// Batch size

	// CCL
	CCL_OBJECT_BEGIN(DataLoader)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(First)
	DECLARE_CON(Next)
	DECLARE_EMT(End)
	DECLARE_RCP(Name)
	DECLARE_RCP(Location)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(First)
		DEFINE_CON(Next)
		DEFINE_EMT(End)
		DEFINE_RCP(Name)
		DEFINE_RCP(Location)
	END_BEHAVIOUR_NOTIFY()
	};
/*
//
// Class - Dataset.  Provides a custom data set for provided data.
//

class Dataset :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Dataset ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context
	IContainer		*pSrc;								// Data source			

	// CCL
	CCL_OBJECT_BEGIN(Dataset)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_CON(Open)
	DECLARE_RCP(Source)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_CON(Open)
		DEFINE_RCP(Source)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};
*/
//
// Class - Dropout.  Dropouts node
// 

class Dropout :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Dropout ( void );										// Constructor

	// Run-time data
	adtFloat			fP;									// Probability of element to be zeroed
	adtBool			bIn;									// Inplace ?

	// CCL
	CCL_OBJECT_BEGIN(Dropout)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	BEGIN_BEHAVIOUR()
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Linear.  Linear neural layer
// 

class Linear :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Linear ( void );										// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context
	adtString		strNet;								// Net name
	adtInt			szIn,szOut;							// Inputs/outputs
	adtBool			bBias;								// Bias term ?

	// CCL
	CCL_OBJECT_BEGIN(Linear)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Add)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	DECLARE_RCP(Inputs)
	DECLARE_RCP(Outputs)
	DECLARE_RCP(Name)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Add)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
		DEFINE_RCP(Inputs)
		DEFINE_RCP(Outputs)
		DEFINE_RCP(Name)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Loss.  Tensor loss functions.
// 

class Loss :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Loss ( void );											// Constructor

	// Run-time data
	IDictionary				*pDctPre;					// Prediction tensor context
	IDictionary				*pDctTgt;					// Target tensor context
	IDictionary				*pDctLss;					// Loss tensor context
	torch::nn::MSELoss	mse;							// MSE loss object
	torch::nn::NLLLoss	nll;							// NLL loss object

	// CCL
	CCL_OBJECT_BEGIN(Loss)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Prediction)
	DECLARE_RCP(Target)
	DECLARE_CON(Loss)
	DECLARE_RCP(Mse)
	DECLARE_RCP(Nll)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Prediction)
		DEFINE_RCP(Target)
		DEFINE_CON(Loss)
		DEFINE_RCP(Mse)
		DEFINE_RCP(Nll)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Net.  Neural network module.
// 

class Net :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Net ( void );											// Constructor

	// Run-time data
	IDictionary	*pDct;									// Context

	// CCL
	CCL_OBJECT_BEGIN(Net)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Add)
	DECLARE_CON(Fire)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Add)
		DEFINE_CON(Fire)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Optimize.  Optimizers implementation.
//

class Optimize :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Optimize ( void );									// Constructor

	// Run-time data
	IDictionary		*pDct;								// Optimizer context
	adtString		strType;								// Optimizer type
	adtString		strNet;								// Network

	// CCL
	CCL_OBJECT_BEGIN(Optimize)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Fire)
	DECLARE_RCP(Reset)
	DECLARE_RCP(Type)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Fire)
		DEFINE_RCP(Reset)
		DEFINE_RCP(Type)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Reshape.  Reshape tensor node.
// 

class Reshape :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Reshape ( void );										// Constructor

	// Run-time data
	IDictionary				*pDct;						// Tensor context
	std::vector<int64_t>	vSz;							// Size vector

	// CCL
	CCL_OBJECT_BEGIN(Reshape)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	DECLARE_RCP(Size)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
		DEFINE_RCP(Size)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - Size.  Size operations node
// 

class Size :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Size ( void );											// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context

	// CCL
	CCL_OBJECT_BEGIN(Size)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_EMT(Error)
	DECLARE_CON(Fire)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_EMT(Error)
		DEFINE_CON(Fire)
	END_BEHAVIOUR_NOTIFY()
	};

//
// Class - TensorOp.  Tensor operations node.
// 

class TensorOp :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	TensorOp ( void );									// Constructor

	// Run-time data
	IDictionary				*pDct;						// Tensor context
	std::vector<int64_t>	vSz;							// Size vector

	// CCL
	CCL_OBJECT_BEGIN(TensorOp)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Connections
	DECLARE_RCP(Dictionary)
	DECLARE_CON(Clone)
	DECLARE_EMT(Error)
	DECLARE_CON(From)
	DECLARE_RCP(Size)
	DECLARE_CON(To)
	DECLARE_CON(Zeroes)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_CON(Clone)
		DEFINE_EMT(Error)
		DEFINE_CON(From)
		DEFINE_RCP(Size)
		DEFINE_CON(To)
		DEFINE_CON(Zeroes)
	END_BEHAVIOUR_NOTIFY()
	};

///////////
// Objects
///////////

//
// Class - refTorch.  Object reference container.
//

class refTorch :
	public CCLObject,										// Base class
	public IThis											// Interface
	{
	public :
	refTorch ( void ) { AddRef(); }					// Constructor
//	virtual ~refTorch ( void )
//		{ lprintf ( LOG_DBG, L"~refTorch : net %p\r\n", net ); }

	// Run-time data
	std::shared_ptr<torch::nn::Module>		module;	// Module
	std::shared_ptr<torch::nn::LinearImpl>	linear;	// Layer
	std::shared_ptr<torch::Tensor>			tensor;	// Tensor
//	std::shared_ptr<torch::optim::SGD>		sgd;		// Optimizer
//	std::shared_ptr<torch::optim::Adam>		adam;		// Optimizer
	std::shared_ptr<torch::optim::Optimizer>	opt;	// Optimizer

	// Utilities
	static HRESULT from ( IDictionary *pD, const WCHAR *pcN, refTorch **ppRef )
		{
		HRESULT		hr			= S_OK;
		IThis			*pThis	= NULL;
		adtIUnknown	unkV;
		adtValue		vL;

		// Load from dictionary
		CCLTRYE( pD != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pD->load ( adtString(pcN), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IThis,&pThis) );
		CCLTRY ( pThis->getThis ( (void **) ppRef ) );

		// Leave reference count on pThis/refTorch for caller
		return hr;
		}	// from
	static HRESULT from ( const ADTVALUE &v, const WCHAR *pcN, refTorch **ppRef )
		{
		HRESULT		hr			= S_OK;
		IDictionary	*pDct		= NULL;
		adtIUnknown	unkV(v);

		// Obtain dictionary and extract
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDct) );
		CCLTRY ( from ( pDct, pcN, ppRef ) );
		_RELEASE(pDct);

		return hr;
		}	// from
	static HRESULT to ( IDictionary *pDct, const WCHAR *pcN, refTorch *pRef )
		{
		HRESULT		hr			= S_OK;
		IThis			*pThis	= pRef;

		// Store object in dictionary under name
		CCLTRY(pDct->store(adtString(pcN),adtIUnknown(pThis)));

		return hr;
		}	// to
	static HRESULT to ( const ADTVALUE &v, const WCHAR *pcN, refTorch *pRef )
		{
		HRESULT		hr			= S_OK;
		IDictionary	*pDct		= NULL;
		adtIUnknown	unkV(v);

		// Obtain dictionary and store
		CCLTRY ( _QISAFE(unkV,IID_IDictionary,&pDct) );
		CCLTRY ( to ( pDct, pcN, pRef ) );
		_RELEASE(pDct);

		return hr;
		}	// to

	// 'IThis' members
	STDMETHOD(getThis)	( void **ppv )
		{ *ppv = (refTorch *)this; return S_OK; }

	// CCL
	CCL_OBJECT_BEGIN_INT(refTorch)
		CCL_INTF(IThis)
	CCL_OBJECT_END()
	};

/*
//
// Class - refTensor.  Tensor reference.
//

class refTensor :
	public CCLObject,										// Base class
	public IThis											// Interface
	{
	public :
	refTensor ( void ) { AddRef(); }					// Constructor

	// Run-time data
	torch::Tensor		tensor;							// Tensor

	// Extract/store from/to dictionary
	static HRESULT from ( const ADTVALUE &v, refTensor **ppRef )
		{
		HRESULT		hr			= S_OK;
		IDictionary	*pDct		= NULL;
		IThis			*pThis	= NULL;
		adtIUnknown	unkV(v);
		adtValue		vL;

		// Retrieve from predefined key
		CCLTRY ( _QISAFE((unkV=vL),IID_IDictionary,&pDct) );
		CCLTRY ( pDct->load ( adtString(L"refTensor"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IThis,&pThis) );
		CCLTRY ( pThis->getThis ( (void **) ppRef ) );
		_RELEASE(pDct);

		// Leave reference count for caller
		return hr;
		}	// from
	static HRESULT to ( const ADTVALUE &v, refTensor *pRef )
		{
		HRESULT		hr			= S_OK;
		IDictionary	*pDct		= NULL;
		IThis			*pThis	= pRef;
		adtIUnknown	unkV(v);
		adtValue		vL;

		// Store to predefined key
		CCLTRY ( _QISAFE((unkV=vL),IID_IDictionary,&pDct) );
		CCLTRY ( pDct->store ( adtString(L"refTensor"), adtIUnknown(pThis) ) );
		_RELEASE(pDct);

		return hr;
		}	// from

	// 'IThis' members
	STDMETHOD(getThis)	( void **ppv )
		{ *ppv = (refTensor *)this; return S_OK; }

	// Operators
	inline operator at::Tensor &() { return tensor; }
	inline refTensor & operator= ( at::Tensor &t )
		{ tensor = t; return *this; }

	// CCL
	CCL_OBJECT_BEGIN_INT(refTensor)
		CCL_INTF(IThis)
	CCL_OBJECT_END()
	};

//
// Class - datasetRef.  Custom dataset reference.
//

class datasetRef :
	public CCLObject,										// Base class
	public IThis,											// Interface
	public torch::data::datasets::Dataset<datasetRef>
	{
	public :
	datasetRef ( IContainer * ) { AddRef(); }		// Constructor

	// Dataset members
	

	// Extract/store from/to dictionary
	static HRESULT from ( const ADTVALUE &v, datasetRef **ppRef )
		{
		HRESULT		hr			= S_OK;
		IDictionary	*pDct		= NULL;
		IThis			*pThis	= NULL;
		adtIUnknown	unkV(v);
		adtValue		vL;

		// Retrieve from predefined key
		CCLTRY ( _QISAFE((unkV=vL),IID_IDictionary,&pDct) );
		CCLTRY ( pDct->load ( adtString(L"datasetRef"), vL ) );
		CCLTRY ( _QISAFE((unkV=vL),IID_IThis,&pThis) );
		CCLTRY ( pThis->getThis ( (void **) ppRef ) );
		_RELEASE(pDct);

		// Leave reference count for caller
		return hr;
		}	// from
	static HRESULT to ( const ADTVALUE &v, datasetRef *pRef )
		{
		HRESULT		hr			= S_OK;
		IDictionary	*pDct		= NULL;
		IThis			*pThis	= pRef;
		adtIUnknown	unkV(v);
		adtValue		vL;

		// Store to predefined key
		CCLTRY ( _QISAFE((unkV=vL),IID_IDictionary,&pDct) );
		CCLTRY ( pDct->store ( adtString(L"datasetRef"), adtIUnknown(pThis) ) );
		_RELEASE(pDct);

		return hr;
		}	// from

	// 'IThis' members
	STDMETHOD(getThis)	( void **ppv )
		{ *ppv = (datasetRef *)this; return S_OK; }

	// CCL
	CCL_OBJECT_BEGIN_INT(datasetRef)
		CCL_INTF(IThis)
	CCL_OBJECT_END()
	};

//
// Class - torchNet.  Network module for neural network
//

class torchNet : public torch::nn::Module
	{
	public :
	torchNet ( Net *pO )									// Constructor
		{ pOwner = pO; _ADDREF(pOwner);
			linear = register_module ( "fc1", torch::nn::Linear(784,63)); }
	virtual ~torchNet ( void )							
		{ _RELEASE(pOwner); }

	// Run-time data
	Net						*pOwner;						// Ptr. to owner node
	torch::nn::Linear		linear{nullptr};			// Linear module
	torch::nn::Linear		tst{nullptr};				// Linear module

	};
*/


#endif
