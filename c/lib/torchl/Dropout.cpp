////////////////////////////////////////////////////////////////////////
//
//									Dropout.CPP
//
//				Implementation of the dropouts node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Dropout :: Dropout ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	fP		= 0.5f;
	bIn	= false;
	}	// Dropout

HRESULT Dropout :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Probability"), vL ) == S_OK)
			fP = vL;
		if (pnDesc->load ( adtString(L"Inplace"), vL ) == S_OK)
			bIn = vL;

		}	// if

	// Detach
	else
		{
		// Shutdown
		}	// else

	return hr;
	}	// onAttach

HRESULT Dropout :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Dropout
	if (_RCP(Fire))
		{
		refTorch	*pRef	= NULL;

		// Extract tensor reference
		CCLTRY ( refTorch::from(v,L"Tensor",&pRef) );
		if (hr == S_OK)
			{
			// Apply dropout function
			if (_RCP(Fire))
				*(pRef->tensor) = torch::dropout(*(pRef->tensor),fP,bIn);
			}	// if

		// Clean up
		_RELEASE(pRef);

		// Result
		if (hr == S_OK)
			_EMT(Fire,v);
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// State
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

