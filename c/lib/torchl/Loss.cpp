////////////////////////////////////////////////////////////////////////
//
//									Loss.CPP
//
//				Implementation of the tensor loss node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Loss :: Loss ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDctPre	= NULL;
	pDctTgt	= NULL;
	pDctLss	= NULL;
	}	// Loss

HRESULT Loss :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
//		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
//			onReceive(prSize,vL);

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDctPre);
		_RELEASE(pDctTgt);
		_RELEASE(pDctLss);
		}	// else

	return hr;
	}	// onAttach

HRESULT Loss :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Loss
	if (_RCP(Nll) || _RCP(Mse))
		{
		refTorch	*pRefPre	= NULL;
		refTorch	*pRefTgt	= NULL;
		refTorch	*pRefLss	= NULL;
		adtValue	vL;

		// State check
		CCLTRYE ( pDctPre != NULL && pDctTgt != NULL && pDctLss != NULL,
						ERROR_INVALID_STATE );

		// Extract tensors
		CCLTRY ( refTorch::from ( adtIUnknown(pDctPre), L"Tensor", &pRefPre ) );
		CCLTRY ( refTorch::from ( adtIUnknown(pDctTgt), L"Tensor", &pRefTgt ) );

		// Create a new loss reference tensor
		CCLTRYE ( (pRefLss = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE( (pRefLss->tensor = std::make_shared<torch::Tensor>())
						!= nullptr, E_OUTOFMEMORY );
		CCLTRY ( refTorch::to ( pDctLss, L"Tensor", pRefLss ) );

		// Compute loss
		if (hr == S_OK)
			{
			// Compute loss
			if (_RCP(Nll))
				*(pRefLss->tensor) = nll ( *(pRefPre->tensor), *(pRefTgt->tensor) );
			else if (_RCP(Mse))
				*(pRefLss->tensor) = mse ( *(pRefPre->tensor), *(pRefTgt->tensor) );
			else
				hr = ERROR_NOT_SUPPORTED;
			}	// if

		// Compute gradients of the loss (NOTE: Does this belong somehwere else ?)
		CCLOK ( pRefLss->tensor->backward(); )

		// Metrics
		if (hr == S_OK)
			{
			auto y_true_mean = torch::mean(*(pRefTgt->tensor));
			auto y_pre_mean = torch::mean(*(pRefPre->tensor));
			auto ss_tot = torch::sum(torch::pow(*(pRefTgt->tensor) - y_true_mean, 2));
			auto ss_res = torch::sum(torch::pow(*(pRefTgt->tensor) - *(pRefPre->tensor), 2));
			auto rsq = (1 - ss_res / ss_tot);
			lprintf ( LOG_DBG, L"Loss %g Mean %g (tgt),%g (pre) rsq %g\r\n",		
							pRefLss->tensor->item<float>(), 
							y_true_mean.item<float>(), 
							y_pre_mean.item<float>(), 
							rsq.item<float>() );
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Loss,adtIUnknown(pDctLss));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRefPre);
		_RELEASE(pRefTgt);
		_RELEASE(pRefLss);
		}	// else if

	// State
	else if (_RCP(Prediction))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctPre);
		_QISAFE(unkV,IID_IDictionary,&pDctPre);
		}	// else if
	else if (_RCP(Target))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctTgt);
		_QISAFE(unkV,IID_IDictionary,&pDctTgt);
		}	// else if
	else if (_RCP(Loss))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDctLss);
		_QISAFE(unkV,IID_IDictionary,&pDctLss);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

