////////////////////////////////////////////////////////////////////////
//
//									Reshape.CPP
//
//				Implementation of the reshape tensor node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Reshape :: Reshape ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct	= NULL;
	}	// Reshape

HRESULT Reshape :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Size"), vL ) == S_OK)
			onReceive(prSize,vL);

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Reshape :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Reshape
	if (_RCP(Fire))
		{
		refTorch	*pRef	= NULL;
		adtValue		vL;

		// Extract tensor
		CCLTRY ( refTorch::from ( adtIUnknown(pDct), L"Tensor", &pRef ) );

		// Valid sizes ? 
		CCLTRYE ( vSz.size() > 0, ERROR_INVALID_STATE );

		// Resize the tensor
		if (hr == S_OK)
			{
			c10::IntArrayRef ar(vSz);
			*(pRef->tensor) = pRef->tensor->reshape(ar);
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Fire,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRef);
		}	// else if

	// Size
	else if (_RCP(Size))
		{
		IContainer	*pCnt	= NULL;
		IIt			*pIt	= NULL;
		adtIUnknown	unkV(v);
		adtValue		vL;

		// Setup
		vSz.clear();

		// Iterate and place sizes in vector array
		CCLTRY ( _QISAFE(unkV,IID_IContainer,&pCnt) );
		CCLTRY ( pCnt->iterate(&pIt));
		while (hr == S_OK && pIt->read ( vL ) == S_OK)
			{
			// Add to list
			vSz.push_back(adtLong(vL));
			pIt->next();
			}	// while

		// Clean up
		_RELEASE(pIt);
		_RELEASE(pCnt);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

