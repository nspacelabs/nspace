////////////////////////////////////////////////////////////////////////
//
//									Linear.CPP
//
//				Implementation of the linear module node
// 
////////////////////////////////////////////////////////////////////////

#include "torchl_.h"

Linear :: Linear ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for the object
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	strNet	= L"Net";									// Default
	}	// Linear

HRESULT Linear :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Net"), vL ) == S_OK)
			hr = adtValue::toString(vL,strNet);
		if (pnDesc->load ( adtString(L"Inputs"), vL ) == S_OK)
			onReceive(prInputs,vL);
		if (pnDesc->load ( adtString(L"Outputs"), vL ) == S_OK)
			onReceive(prOutputs,vL);
		if (pnDesc->load ( adtString(L"Bias"), vL ) == S_OK)
			bBias = vL;

		}	// if

	// Detach
	else
		{
		// Shutdown
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Linear :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Add layer
	if (_RCP(Add))
		{
		refTorch		*pRef		= NULL;
		refTorch		*pRefNet	= NULL;
		char			*pcName	= NULL;
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Net reference object
		CCLTRY ( refTorch::from(pDct,strNet,&pRefNet) );

		// Name should not already exist
		CCLTRYE ( pDct->load ( strnName, vL ) != S_OK, ERROR_INVALID_STATE );

		// Valid sizes ?
		CCLTRYE ( szIn > 0 && szOut > 0, ERROR_INVALID_STATE );

		// Create and register new module
		CCLTRY	( strnName.toAscii ( &pcName ) );
		CCLTRYE	( (pRef = new refTorch()) != NULL, E_OUTOFMEMORY );
		CCLTRYE	( (pRef->linear = std::make_shared<torch::nn::LinearImpl>
						(szIn,szOut)) != nullptr, E_OUTOFMEMORY );
		CCLOK		( pRefNet->module->register_module ( pcName, pRef->linear ); )

		// Store in context under name
		CCLTRY	( refTorch::to ( adtIUnknown(pDct), strnName, pRef ) );

		// Result
		if (hr == S_OK)
			_EMT(Add,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_FREEMEM(pcName);
		_RELEASE(pRef);
		_RELEASE(pRefNet);
		}	// else if

	// Apply data to layer
	else if (_RCP(Fire))
		{
		IDictionary	*pDctT	= NULL;
		refTorch		*pRefT	= NULL;
		refTorch		*pRef		= NULL;
		adtIUnknown	unkV(v);

		// Extract layer
		CCLTRY ( refTorch::from ( pDct, strnName, &pRef ) );

		// Extract tensor
		CCLTRY ( refTorch::from ( unkV, L"Tensor", &pRefT ) );

		// Apply
		CCLOK ( *(pRefT->tensor) = pRef->linear->forward(*(pRefT->tensor)); )

		// Result
		if (hr == S_OK)
			_EMT(Fire,v);
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pRef);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown	unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Inputs))
		szIn = v;
	else if (_RCP(Outputs))
		szOut = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

