////////////////////////////////////////////////////////////////////////
//
//								Graphics.CPP
//
//			Implementation of the Cuda graphics usage node
//
////////////////////////////////////////////////////////////////////////

#define	INITGUID
#include "cudal_.h"

Graphics :: Graphics ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	pDctRes	= NULL;
	}	// Graphics

HRESULT Graphics :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			onReceive(prType,vL);
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDctRes);
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT Graphics :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Register the graphics resource
	if (_RCP(Register))
		{
		cudaGraphicsResource_t	pRes	= NULL;
		cudaError					cr		= cudaSuccess;
		adtValue						vTex;

		// State check
		CCLTRYE ( pDct != NULL && pDctRes != NULL, ERROR_INVALID_STATE );
		CCLTRY ( pDctRes->load ( adtString(L"Texture"), vTex ) );

		// Graphics based on type
		lprintf ( LOG_DBG, L"Register:strType %s pDctRes %p\r\n",
						(LPCWSTR)strType, pDctRes );
		if (hr == S_OK && !WCASECMP(strType,L"Direct3D"))
			{
			ID3D11Resource	*pdx	= NULL;
			adtIUnknown		unkV;

			// Direct X texture resource
			CCLTRY ( _QISAFE((unkV=vTex),IID_ID3D11Resource,&pdx) );

			// Allow Cuda to have access to the resource
			CCLTRYE ( (cr = cudaGraphicsD3D11RegisterResource(&pRes,pdx,
							cudaGraphicsRegisterFlagsNone)) == cudaSuccess,
							cr );

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Unable to Graphics DirectX resource %p (0x%x)\r\n",
											pdx, cr );			

			// Clean up
			_RELEASE(pdx);
			}	// if
		else if (hr == S_OK && !WCASECMP(strType,L"OpenGL"))
			{
			adtLong	lRes(vTex);
			GLuint	gl = (GLuint)(U64)lRes;//lRes;

			// Allow Cuda to have access to the resource
			CCLTRYE ( (cr = cudaGraphicsGLRegisterImage(&pRes,gl,GL_TEXTURE_2D,
							cudaGraphicsRegisterFlagsNone)) == cudaSuccess,
							cr );

			// Debug
			if (hr != S_OK)
				lprintf ( LOG_DBG, L"Unable to Graphics OpenGL resource %d (0x%x)\r\n",
											gl, cr );			
			}	// if
		else
			hr = E_NOTIMPL;

		// Results
		CCLTRY(pDct->store ( adtString(L"Resource"), adtLong((U64)pRes) ) );
		if (hr == S_OK)
			_EMT(Register,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		}	// if

	// Deregister graphics resource
	else if (_RCP(Unregister))
		{
		adtValue		vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// About to close
		CCLOK ( _EMT(Unregister,adtIUnknown(pDct)); )

		// Clean up resources
		if (hr == S_OK && pDct->load ( adtString(L"Resource"), vL ) == S_OK)
			{
			cudaGraphicsResource_t	pRes	= (cudaGraphicsResource_t) (U64)adtLong(vL);

			// Clean up
			cudaGraphicsUnregisterResource(pRes);
			pDct->remove ( adtString(L"Resource") );
			}	// if
		}	// else if

	// Map graphics resource
	else if (_RCP(Map))
		{
		cudaError					cr		= cudaSuccess;
		cudaGraphicsResource_t	pRes	= NULL;
		cudaStream_t				pStm	= NULL;
		cudaArray_t					pArr	= NULL;
		unsigned int				flags	= 0;
		adtValue						vL;
		cudaChannelFormatDesc	dsc;
		cudaExtent					ext;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		memset ( &ext, 0, sizeof(ext) );
		memset ( &dsc, 0, sizeof(dsc) );

		// Obtain cuda resource
		CCLTRY ( pDct->load ( adtString(L"Resource"), vL ) );
		CCLTRYE( (pRes = (cudaGraphicsResource_t)(U64)adtLong(vL)) != 0, E_UNEXPECTED );

		// Create a stream to assist in concurrency
		CUDATRY(cudaStreamCreate(&pStm));
		CCLTRY(pDct->store(adtString(L"Stream"),adtLong((U64)pStm)));

		// Allow CUDA access to the resource
		CUDATRY(cudaGraphicsMapResources(1,&pRes,pStm));

		// Grab access to resource array.  Textures have subresources, not a direct device ptr.
		CUDATRY(cudaGraphicsSubResourceGetMappedArray(&pArr,pRes,0,0));
		CCLTRY(pDct->store(adtString(L"Array"),adtLong((U64)pArr)));

		// Query/store information about the array for external use
		CUDATRY(cudaArrayGetInfo(&dsc,&ext,&flags,pArr));
		CCLTRY(pDct->store(adtString(L"Width"),adtLong(ext.width)));
		CCLTRY(pDct->store(adtString(L"Height"),adtLong(ext.height)));
		CCLTRY(pDct->store(adtString(L"X"),adtInt(dsc.x)));
		CCLTRY(pDct->store(adtString(L"Y"),adtInt(dsc.y)));
		CCLTRY(pDct->store(adtString(L"Z"),adtInt(dsc.z)));
		CCLTRY(pDct->store(adtString(L"W"),adtInt(dsc.w)));

		// The 'depth' field does not seem to be filled in the way this routine
		// could use it so update for self only.  These will add to 24/32 bits they seem
		// to be the channel sizes.
		if (cr == cudaSuccess)
			ext.depth	= (dsc.x+dsc.y+dsc.z+dsc.w)/8;
		CCLTRY(pDct->store(adtString(L"Depth"),adtLong(ext.depth)));
		CCLTRY(pDct->store(adtString(L"Size"),adtLong(ext.width*ext.depth*ext.height)));

		// Debug
		if (hr != S_OK)
			lprintf ( LOG_DBG, L"Unable to map resource %p (0x%x)\r\n", pRes, cr );			

		// Result
		if (hr == S_OK)
			_EMT(Map,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// else if

	// Unmap graphics resource
	else if (_RCP(Unmap))
		{
		cudaGraphicsResource_t	pRes	= NULL;
		cudaStream_t				pStm	= NULL;
		adtValue						vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Resources
		if (hr == S_OK && pDct->load ( adtString(L"Resource"), vL ) == S_OK)
			pRes	= (cudaGraphicsResource_t) (U64)adtLong(vL);
		if (hr == S_OK && pDct->load ( adtString(L"Stream"), vL ) == S_OK)
			pStm	= (cudaStream_t) (U64)adtLong(vL);
		
		// No need for map
		if (pRes != NULL && pStm != NULL)
			{
			// Ensure nothing is going on with the resource
			cudaDeviceSynchronize();

			// NOTE: For whatever reason a delay is needed to avoid a sometimes crash from
			// calling cudaGraphicsUnmapResources.
			Sleep(500);

			// Free it
			cudaGraphicsUnmapResources(1,&pRes,pStm);
			}	// if
		if (pStm != NULL)
			cudaStreamDestroy(pStm);

		// Clean up
		CCLOK ( pDct->remove ( adtString(L"Stream") ); )
		CCLOK ( pDct->remove ( adtString(L"Array") ); )
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Resource))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctRes);
		_QISAFE(unkV,IID_IDictionary,&pDctRes);
		}	// else if
	else if (_RCP(Type))
		hr = adtValue::toString(v,strType);
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive

