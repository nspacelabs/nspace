////////////////////////////////////////////////////////////////////////
//
//										CUDAL.H
//
//								Cuda (nVidia) node library
//
////////////////////////////////////////////////////////////////////////

#ifndef	CUDAL_H
#define	CUDAL_H

// System includes
#include "../nspcl/nspcl.h"

// Cuda
#include <cuda_runtime.h>
#ifdef	_WIN32
#include <cuda_d3d11_interop.h>
#endif
#include <cuda_gl_interop.h>

// Callback function
typedef void (__stdcall *captureCB) ( void );

//
// Class - cudaCapture.  Texture capture object via CUDA.
//

class cudaCapture
	{
	public :
	cudaCapture ( void );								// Constructor
	virtual ~cudaCapture ( void );					// Destructor

	// Run-time data
	bool							bDx;						// Direct X ? (F = OpenGL)
	U64							lRes;						// Resource
	cudaGraphicsResource_t	pRes;						// Resource 
	cudaArray_t					pArr;						// Array structure for texture
	cudaStream_t				pStm;						// Synchronization stream
	cudaChannelFormatDesc	dscTgt;					// Target descriptor
	cudaExtent					extTgt;					// Target extent
	void							*pvMem;					// Host memory
	captureCB					cb;						// Latest callback
	adtString					strId;					// Memory Id
	HANDLE						hMap;						// Shared memory
	void							*pvMap;					// Shared memory

	// Utilities
	HRESULT capture		( captureCB );				// Capture texture data
	HRESULT setCapture	( bool, U64, 				// Set capture resource
									const WCHAR * );

	private :
	};

#endif

