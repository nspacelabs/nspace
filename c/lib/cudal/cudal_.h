////////////////////////////////////////////////////////////////////////
//
//										CUDAL_.H
//
//		Implementation include file for the CUDA node library
//
////////////////////////////////////////////////////////////////////////

#ifndef	CUDAL__H
#define	CUDAL__H

#include "cudal.h"

// Definitions
#define	CUDATRY(a)				if (cr == cudaSuccess) cr = a

///////////
// Objects
///////////

//
//!	\brief A block of Cuda memory supporting the memory mapped interface.
//!	\nodetag Memory CUDA
//

class MemoryBlock :
	public CCLObject,										// Base class
	public IMemoryMapped									// Interface
	{
	public :
	MemoryBlock ( void *, U32, const WCHAR * );	// Constructor

	// 'IMemoryMapped' members
	STDMETHOD(getInfo)	( void **, U32 * );
	STDMETHOD(setInfo)	( void *, U32 );
	STDMETHOD(setSize)	( U32 );
	STDMETHOD(stream)		( IByteStream ** );

	// CCL
	CCL_OBJECT_BEGIN_INT(MemoryBlock)
		CCL_INTF(IMemoryMapped)
	CCL_OBJECT_END()
	virtual void 		destruct		( void );		// Destruct object

	private :

	// Run-time data
	U8				*pcBlk;									// Memory block
	U32			szBlk;									// Size of memory block
	adtString	strType;									// Memory type
	};

/////////
// Nodes
/////////

//
// Class - Graphics.  Cuda graphics manipulation node.
//

class Graphics :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	Graphics ( void );									// Constructor

	// Run-time data
	IDictionary		*pDct;								// Context
	IDictionary		*pDctRes;							// Resource dictionary
	adtString		strType;								// Resource type

	// CCL
	CCL_OBJECT_BEGIN(Graphics)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Devices
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Resource)
	DECLARE_CON(Register)
	DECLARE_CON(Unregister)
	DECLARE_CON(Map)
	DECLARE_CON(Unmap)
	DECLARE_EMT(Error)
	DECLARE_RCP(Type)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Resource)
		DEFINE_CON(Register)
		DEFINE_CON(Unregister)
		DEFINE_CON(Map)
		DEFINE_CON(Unmap)
		DEFINE_EMT(Error)
		DEFINE_RCP(Type)
	END_BEHAVIOUR_NOTIFY()
	
	private :

	};

//
// Class - MemoryOp.  Cuda MemoryOp manipulation node.
//

class MemoryOp :
	public CCLObject,										// Base class
	public Behaviour										// Interface
	{
	public :
	MemoryOp ( void );									// Constructor

	// Run-time data
	IDictionary		*pDct;								// Memory Context
	IDictionary		*pDctArr;							// Array context
	adtString		strType;								// Memory type
	adtInt			iSz;									// Active size

	// CCL
	CCL_OBJECT_BEGIN(MemoryOp)
		CCL_INTF(IBehaviour)
	CCL_OBJECT_END()

	// Devices
	DECLARE_RCP(Dictionary)
	DECLARE_RCP(Array)
	DECLARE_RCP(Size)
	DECLARE_RCP(Type)
	DECLARE_CON(Alloc)
	DECLARE_CON(Free)
	DECLARE_CON(Copy)
	DECLARE_EMT(Error)
	BEGIN_BEHAVIOUR()
		DEFINE_RCP(Dictionary)
		DEFINE_RCP(Array)
		DEFINE_RCP(Size)
		DEFINE_RCP(Type)
		DEFINE_CON(Alloc)
		DEFINE_CON(Free)
		DEFINE_CON(Copy)
		DEFINE_EMT(Error)
	END_BEHAVIOUR_NOTIFY()
	
	private :

	};

#endif
