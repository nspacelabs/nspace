////////////////////////////////////////////////////////////////////////
//
//									capture.cpp
//
//				Implementation of the cuda texture capture object
//
////////////////////////////////////////////////////////////////////////

#include "cudal_.h"

// Protottpes
void CUDART_CB cudaUpdateCB ( cudaStream_t stream, cudaError_t status, 
										void *userData );

cudaCapture :: cudaCapture ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	////////////////////////////////////////////////////////////////////////
	bDx	= true;
	lRes	= 0;
	pRes	= NULL;
	pArr	= NULL;
	pStm	= NULL;
	pvMem	= NULL;
	cb		= NULL;
	hMap	= NULL;
	pvMap = NULL;
	memset ( &dscTgt, 0, sizeof(dscTgt) );
	}	// cudaCapture

cudaCapture :: ~cudaCapture ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Destructor for object.
	//
	////////////////////////////////////////////////////////////////////////
	setCapture(false,0,NULL);
	}	// ~cudaCapture

HRESULT cudaCapture :: capture ( captureCB _cb )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to set the capture texture.
	//
	//	PARAMETERS
	//		-	_cb is the callback to use when capture/copy is complete
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	cudaError	cr		= cudaSuccess;

	// State check
	if (pArr == NULL || pvMem == NULL)
		return ERROR_INVALID_STATE ;

	// Synchronous copy
	CUDATRY(cudaMemcpy2DFromArray ( pvMem, extTgt.width*extTgt.depth, 
					pArr, 0, 0, extTgt.width*extTgt.depth, extTgt.height,
					cudaMemcpyDeviceToHost ));

	// Now host memory can be copied to shared location
	if (cr == cudaSuccess && pvMap != NULL)
		{
		memcpy ( pvMap, pvMem, extTgt.width*extTgt.depth*extTgt.height );
//		memset ( pvMap, 0x80, extTgt.width*extTgt.depth*extTgt.height );
		}	// if
	if (cr != cudaSuccess)
		lprintf ( LOG_DBG, L"cr 0x%x pvMap %p Size %d\r\n",
						cr, pvMap, extTgt.width*extTgt.depth*extTgt.height );

	// Initiate a copy from the GPU to the local buffer.  Perform
	// asynchronously to avoid holding up calling thread. 
//	CUDATRY(cudaMemcpy2DFromArrayAsync ( pvMem, extTgt.width*extTgt.depth, 
//					pArr, 0, 0, extTgt.width*extTgt.depth, extTgt.height,
//					cudaMemcpyDeviceToHost, pStm ));

	// Add callback for when copy is complete to make this call
	// asynchronous/non-blocking.
//	cb = _cb;
//	CUDATRY(cudaStreamAddCallback(pStm,cudaUpdateCB,this,0));

	return (cr == cudaSuccess) ? S_OK : S_FALSE;
	}	// capture

HRESULT cudaCapture :: setCapture ( bool _bDx, U64 _lRes, const WCHAR *pwId )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called to set the capture texture.
	//
	//	PARAMETERS
	//		-	_bDx is true for DirectX, false for OpenGL.
	//		-	_lRes is the resource depending on DirectX or OpenGL
	//		-	pwId is the Id of the shared memory object
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT		hr	= S_OK;
	cudaError	cr	= cudaSuccess;

	// Debug
	lprintf ( LOG_DBG, L"_bDx %d _lRes 0x%x\r\n", _bDx, _lRes );

	// Attach
	if (_lRes != 0)
		{
		// Previous state
		setCapture(false,0,NULL);

		// Save
		bDx	= _bDx;
		lRes	= _lRes;
		strId = pwId;

		//
		// Access the renderer specific resource
		//

		// DirectX
		#ifdef	_WIN32
		if (bDx)
			{
			/*
			ID3D11Resource					*pDxRes	= (ID3D11Resource *)lRes;
			ID3D11Texture2D				*pTex		= NULL;
			D3D11_RESOURCE_DIMENSION	dim;
			pDxRes->GetType(&dim);
			if (pDxRes->QueryInterface(IID_ID3D11Texture2D,(void **)&pTex) == S_OK)
				{
				D3D11_TEXTURE2D_DESC	dsc;
				pTex->GetDesc(&dsc);
				pTex->Release();
				}	// if
			*/
			cr = cudaGraphicsD3D11RegisterResource(&pRes, 
					(ID3D11Resource *)lRes,
					cudaGraphicsRegisterFlagsNone);
			}	// if

		// OpenGL
		else
		#endif
			cr = cudaGraphicsGLRegisterImage(&pRes,
					((GLuint)(long long int)(lRes)),
					GL_TEXTURE_2D,cudaGraphicsRegisterFlagsNone);

		if (cr != cudaSuccess)
			lprintf ( LOG_DBG, L"Register Resource failed %d\r\n", cr );
		lprintf ( LOG_DBG, L"Register cr 0x%x\r\n", cr );

		//
		// Prepare usage of new resource
		//
		if (cr == cudaSuccess)
			{
			unsigned int	flags	= 0;

			// Create a stream to assist in concurrency
			CUDATRY(cudaStreamCreate(&pStm));

			// Allow CUDA access to the resource
			CUDATRY(cudaGraphicsMapResources(1,&pRes,pStm));

			// Grab access to resource array.  Textures have subresources, not a direct device ptr.
			CUDATRY(cudaGraphicsSubResourceGetMappedArray(&pArr,pRes,0,0));

			// Data will be copied from the provided resource into a device based array for own use
			CUDATRY(cudaArrayGetInfo(&dscTgt,&extTgt,&flags,pArr));

			// The 'depth' field does not seem to be filled in the way this routine
			// could use it so udpate for self only.  These will add to 24/32 bits they seem
			// to be the channel sizes.
			if (cr == cudaSuccess)
				extTgt.depth	= (dscTgt.x+dscTgt.y+dscTgt.z+dscTgt.w)/8;

			// Allocate host memory for array target (use this instead of 'malloc' for faster copies
			// due to paging, etc)
			CUDATRY(cudaMallocHost(&pvMem,extTgt.width*extTgt.depth*extTgt.height));

			lprintf ( LOG_DBG, L"Prepare cr 0x%x\r\n", cr );

			// Success ?
			hr = (cr == cudaSuccess) ? S_OK : cr;
			}	// if

		// If successful, it is time to access the shared memory that will receive
		// a copy of the captured image.
		lprintf ( LOG_DBG, L"hr 0x%x strId %s\r\n", hr, (LPCWSTR)strId );
		if (hr == S_OK)
			{
			HRESULT hr = S_OK;

			// Attempt to open an existing map
			CCLTRYE( (hMap = OpenFileMapping ( FILE_MAP_READ|FILE_MAP_WRITE,
							FALSE, strId )) != NULL, GetLastError() );
			lprintf ( LOG_DBG, L"hr 0x%x hMap 0x%x\r\n", hr, hMap );

			// Obtain ptr. to region
			CCLTRYE ( (pvMap = MapViewOfFile ( hMap,
							FILE_MAP_READ|FILE_MAP_WRITE, 
							0, 0, 0 )) != NULL, GetLastError() );

			lprintf ( LOG_DBG, L"Map 0x%x %p 0x%x\r\n", hMap, pvMap, hr );
			}	// if

		}	// if

	// Detach
	else
		{
		//
		// Free existing target
		//
		if (pRes != NULL)
			{
			// Ensure nothing is going on with the resource
			::cudaDeviceSynchronize();

			// NOTE: For whatever reason a delay is needed to avoid a sometimes crash from
			// calling cudaGraphicsUnmapResources.
			Sleep(500);

			// Unbind resource
			cudaGraphicsUnmapResources(1,&pRes,pStm);
			cudaGraphicsUnregisterResource(pRes);
			pRes = NULL;

			// Synchronization stream
			if (pStm != NULL)
				{
				cudaStreamDestroy(pStm);
				pStm = NULL;
				}	// if

			// Free up own memory location
			cudaFreeHost(pvMem);
			pvMem	= NULL;
			}	// if

		// Clean up
		if (pvMap != NULL)
			{
			UnmapViewOfFile(pvMap);
			pvMap = NULL;
			}	// if
		if (hMap != NULL)
			{
			CloseHandle(hMap);
			hMap = NULL;
			}	// if
		}	// else

	// Debug
	lprintf ( LOG_DBG, L"cr 0x%x\r\n", cr );

	return hr;
	}	// setCapture

void CUDART_CB cudaUpdateCB ( cudaStream_t stream, cudaError_t status, 
										void *userData )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	CUDA callback function for when the asynchronous copy has
	//			completed.
	//
	//	PARAMETERS
	//		-	stream is the stream for the callback
	//		-	status is the result of the transaction
	//		-	userData will be the ptr. to the buffer
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	cudaCapture *pThis = (cudaCapture *)userData;

	// Callback
//	lprintf ( LOG_DBG, L"pThis %p\r\n", pThis );
	if (pThis != NULL && pThis->cb != NULL)
		(pThis->cb)();

	}	// cudaUpdateCB
