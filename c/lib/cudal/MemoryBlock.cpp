////////////////////////////////////////////////////////////////////////
//
//									MemoryBlock.CPP
//
//					Implementation of the CUDA memory block object
//
////////////////////////////////////////////////////////////////////////

#include "cudal_.h"
#include <stdio.h>

// Debug

MemoryBlock :: MemoryBlock ( void *pvMem, U32 sz, const WCHAR *pwType )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Constructor for the object.
	//
	////////////////////////////////////////////////////////////////////////
	pcBlk		= (U8 *)pvMem;
	szBlk		= sz;
	strType	= pwType;
	}	// MemoryBlock

void MemoryBlock :: destruct ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the object is being destroyed.
	//
	////////////////////////////////////////////////////////////////////////

	// Clean up
	if (pcBlk != NULL)
		{
		if (!WCASECMP(strType,L"Host"))
			cudaFreeHost(pcBlk);
		pcBlk = NULL;
		}	// if

	}	// destruct

HRESULT MemoryBlock :: getInfo ( void **ppv, U32 *puSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Retrieve pointer and/or the size of the region.
	//
	//	PARAMETERS
	//		-	ppv will receive the ptr.
	//		-	puSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT				hr				= S_OK;

	// Pointer
	if (ppv != NULL)
		{
		// Error on NULL ptr.
		CCLTRYE	( (pcBlk != NULL), E_OUTOFMEMORY );

		// Result
		*ppv = (hr == S_OK) ? pcBlk : NULL;
		}	// if

	// Size
	if (puSz != NULL)	
		*puSz = (hr == S_OK) ? szBlk : 0;

	return hr;
	}	// getInfo

HRESULT MemoryBlock :: setInfo ( void *pv, U32 uSz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Set static information about the memory block, can be
	//			called only one time.
	//
	//	PARAMETERS
	//		-	pv is the ptr. to the memory location
	//		-	uSz is the size of the block
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Not supported
	return E_NOTIMPL;
	}	// setInfo

HRESULT MemoryBlock :: setSize ( U32 sz )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Specifies what size the memory block should be sized too.
	//
	//	PARAMETERS
	//		-	sz is the new size
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////

	// Not resizable
	return E_NOTIMPL;
	}	// setSize

HRESULT MemoryBlock :: stream ( IByteStream **ppStm )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	OVERLOAD
	//	FROM		IMemoryMapped
	//
	//	PURPOSE
	//		-	Creates a byte stream in front of the memory block.
	//
	//	PARAMETERS
	//		-	ppStm is the new stream.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	return E_NOTIMPL;
	}	// stream
