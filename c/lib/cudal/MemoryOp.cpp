////////////////////////////////////////////////////////////////////////
//
//								MemoryOp.CPP
//
//			Implementation of the Cuda memory operations node
//
////////////////////////////////////////////////////////////////////////

#include "cudal_.h"

// Copy callback
//void CUDART_CB cudaCopyCB ( cudaStream_t, cudaError_t, void * );

MemoryOp :: MemoryOp ( void )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Constructor for object.
	//
	//	PARAMETERS
	//		-	hInst is the application instance
	//
	////////////////////////////////////////////////////////////////////////
	pDct		= NULL;
	pDctArr	= NULL;
	}	// MemoryOp

HRESULT MemoryOp :: onAttach ( bool bAttach )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when this behaviour is assigned to a node
	//
	//	PARAMETERS
	//		-	bAttach is true for attachment, false for detachment.
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Attach
	if (bAttach)
		{
		adtValue vL;

		// Defaults
		if (pnDesc->load ( adtString(L"Type"), vL ) == S_OK)
			onReceive(prType,vL);
		}	// if

	// Detach
	else
		{
		// Clean up
		_RELEASE(pDctArr);
		_RELEASE(pDct);
		}	// else

	return hr;
	}	// onAttach

HRESULT MemoryOp :: onReceive ( IReceptor *pr, const ADTVALUE &v )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	The node has received a value on the specified receptor.
	//
	//	PARAMETERS
	//		-	pr is the receptor
	//		-	v is the value
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	HRESULT	hr = S_OK;

	// Allocate a type of memory
	if (_RCP(Alloc))
		{
		void *pvMem = NULL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRYE ( iSz > 0, ERROR_INVALID_STATE );

		// Allocate memory of the specified type
		if (hr == S_OK && !WCASECMP(strType,L"Host"))
			{
			cudaError	cr		= cudaSuccess;
			
			// Allocate
			CCLTRYE ( (cr = cudaMallocHost(&pvMem,iSz)) 
							== cudaSuccess, cr );
			}	// if
		else
			hr = E_NOTIMPL;

		// Wrap in a memory block
		if (hr == S_OK)
			{
			MemoryBlock		*pBlk = NULL;
			IMemoryMapped	*pMap	= NULL;

			// Construct allocated memory block
			CCLTRYE ( (pBlk = new MemoryBlock ( pvMem, iSz, strType ))
							!= NULL, E_OUTOFMEMORY );
			CCLOK ( pBlk->AddRef(); )
			CCLTRY( pBlk->construct() );

			// Save in dictionary
			CCLTRY ( pDct->store ( adtString(L"Memory"), 
							adtIUnknown ( (pMap = pBlk) ) ) );

			// Clean up
			_RELEASE(pBlk);
			}	// if

		// Result
		if (hr == S_OK)
			_EMT(Alloc,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));
		}	// if

	// Free memory
	else if (_RCP(Free))
		{
		adtValue vL;

		// State check
		CCLTRYE ( pDct != NULL, ERROR_INVALID_STATE );

		// Remove object to free
		CCLOK ( pDct->remove ( adtString(L"Memory") ); )
		}	// if

	// Perform an synchronous 2D copy from/to memory from/to an array
	else if (_RCP(Copy))
		{
		cudaError		cr			= cudaSuccess;
		cudaArray_t		pArr		= NULL;
		cudaStream_t	pStm		= NULL;
		cudaMemcpyKind	kind		= cudaMemcpyDefault;
		IMemoryMapped	*pMap		= NULL;
		void				*pvMem	= NULL;
		U32				szMap;
		adtValue			vL;
		adtIUnknown		unkV;
		adtInt			iW,iH,iD;

		// Memory block
		CCLTRYE	( pDct != NULL, ERROR_INVALID_STATE );
		CCLTRY	( pDct->load ( adtString(L"Memory"), vL ) );
		CCLTRY	( _QISAFE((unkV=vL),IID_IMemoryMapped,&pMap) );
		CCLTRY	( pMap->getInfo ( &pvMem, &szMap ) );

		// Array check/access
		CCLTRYE	( pDctArr != NULL, ERROR_INVALID_STATE );
		CCLTRY	( pDctArr->load ( adtString(L"Array"), vL ) );
		CCLTRYE	( (pArr = (cudaArray_t) (U64) adtLong(vL)) != NULL, E_UNEXPECTED );
		CCLTRY	( pDctArr->load ( adtString(L"Stream"), vL ) );
		CCLTRYE	( (pStm = (cudaStream_t) (U64) adtLong(vL)) != NULL, E_UNEXPECTED );

		// Dimensions
		CCLTRY	( pDctArr->load ( adtString(L"Width"), vL ) );
		CCLOK		( iW = vL; )
		CCLTRY	( pDctArr->load ( adtString(L"Height"), vL ) );
		CCLOK		( iH = vL; )
		CCLTRY	( pDctArr->load ( adtString(L"Depth"), vL ) );
		CCLOK		( iD = vL; )

		// State check
		CCLTRYE	( (szMap >= iW*iH*iD), ERROR_OUTOFMEMORY );

		// Memory copy type
		CCLOK		( kind = (!WCASECMP(strType,L"DeviceToHost")) ? cudaMemcpyDeviceToHost :
																					cudaMemcpyDefault; )

		// Require explicit type
		CCLTRYE	( kind != cudaMemcpyDefault, E_NOTIMPL );

		// Type in this case specifies the copy type
		CCLTRYE ( (cr = cudaMemcpy2DFromArray ( pvMem, iW*iD, 
						pArr, 0, 0, iW*iD, iH, kind )) == cudaSuccess, cr );

		// Result
		if (hr == S_OK)
			_EMT(Copy,adtIUnknown(pDct));
		else
			_EMT(Error,adtInt(hr));

		// Clean up
		_RELEASE(pMap);
		}	// else if

	// State
	else if (_RCP(Dictionary))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDct);
		_QISAFE(unkV,IID_IDictionary,&pDct);
		}	// else if
	else if (_RCP(Array))
		{
		adtIUnknown unkV(v);
		_RELEASE(pDctArr);
		_QISAFE(unkV,IID_IDictionary,&pDctArr);
		}	// else if
	else if (_RCP(Type))
		hr = adtValue::toString(v,strType);
	else if (_RCP(Size))
		iSz = v;
	else
		hr = ERROR_NO_MATCH;

	return hr;
	}	// receive
/*
void CUDART_CB cudaCopyCB (	cudaStream_t stream, cudaError_t status, 
										void *userData )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	CUDA callback function for when the asynchronous copy has
	//			completed.
	//
	//	PARAMETERS
	//		-	stream is the stream for the callback
	//		-	status is the result of the transaction
	//		-	userData will be the ptr. to the buffer
	//
	//	RETURN VALUE
	//		S_OK if successful
	//
	////////////////////////////////////////////////////////////////////////
	MemoryOp *pThis = (MemoryOp *)userData;

	// Emit result
	pThis->peOnCopy2DAsync->receive(pThis,L"Value",adtInt(status));

	}	// cudaCopyCB
*/
