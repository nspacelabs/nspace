// Test.cpp : Defines the entry point for the console application.
//


#ifdef _WIN32

#include "../../lib/nshl/nshl_.h"

class IKeySink;

class adtValueTst
	{
	public :
	adtValueTst ( void );
	adtValueTst ( int i );
	adtValueTst ( const WCHAR *pw );
	adtValueTst ( double d );
	adtValueTst ( float f );

	float toFloat ( void );
	};

class IKeySource
	{
	public :
	STDMETHOD(listen)		( const adtValueTst &, IKeySink *, const adtValueTst &, bool )	PURE;
	STDMETHOD(load)		( const adtValueTst &, adtValueTst & )									PURE;
	STDMETHOD(store)		( const adtValueTst &, const adtValueTst & )							PURE;
	};

class IKeySink
	{
	public :
	STDMETHOD(attach)		( IKeySource *, bool )													PURE; 
	STDMETHOD(stored)		( IKeySource *, const adtValueTst &, const adtValueTst & )	PURE; 
	};

class IEmitter;
class IReceptorEx
	{
	STDMETHOD(receive)		( const adtValueTst & );
	};

class IEmitter
	{
	STDMETHOD(connect)		( IReceptorEx * );
	STDMETHOD(disconnect)	( IReceptorEx * );
	STDMETHOD(emit)			( const adtValueTst & );
	};

class LocationEx : public IReceptorEx, public IEmitter
	{
	// 'IReceptorEx' members
	STDMETHOD(receive)		( const adtValueTst & );

	// 'iEmitter' members
	STDMETHOD(connect)		( IReceptorEx * );
	STDMETHOD(disconnect)	( IReceptorEx * );
	STDMETHOD(emit)			( const adtValueTst & );

	};

class Location : public IKeySource, public IKeySink
	{
	public :

	// 'IKeySource' members
	STDMETHOD(listen)		( const adtValueTst &, IKeySink *, const adtValueTst &, bool );
	STDMETHOD(load)		( const adtValueTst &, adtValueTst & );
	STDMETHOD(store)		( const adtValueTst &, const adtValueTst & );

	// 'IKeySink' members
	STDMETHOD(attach)		( IKeySource *, bool ); 
	STDMETHOD(stored)		( IKeySource *, const adtValueTst &, const adtValueTst & ); 

	};

class Node1 : public IKeySink
	{
	public :
	// 'IKeySink' members
	STDMETHOD(attach)		( IKeySource *, bool ); 
	STDMETHOD(stored)		( IKeySource *, const adtValueTst &, const adtValueTst & ); 
	};

HRESULT Location :: store ( const adtValueTst &k, const adtValueTst &v )
	{
	Node1		node1;
	node1.stored(this,k,v);
	return S_FALSE;
	}	// store

int WINAPI wWinMain(_In_ HINSTANCE hInst, _In_opt_ HINSTANCE hPrevInst,
								_In_ LPWSTR lpCmdLine, _In_ int nCmdShow )
	{
/*
	Location		loc;
	adtValueTst	vL;
	Node1			node;
	float			fVal;

	loc.store(L"YouCanByteMeNow",2);
	loc.load(1.234,vL);
	fVal = vL.toFloat();
	loc.listen(L"Fire",&node,L"Left",true);
//	loc.onValue("Fire",12);
*/
/*
	// Attempt access
	HANDLE hFile = CreateFile ( L"\\\\.\\COM6", GENERIC_READ|GENERIC_WRITE, 
						FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING,
						0, NULL );
	if (hFile != INVALID_HANDLE_VALUE)
		{
		COMMTIMEOUTS	cto;
		BYTE	pkt[5] = { 0xaa, 0x00, 0x07, 0x01, 0x00 };
		BYTE	rsp[20];
		DWORD	dx;
		DCB	dcb;

		// Current settings
		memset ( &dcb, 0, sizeof(dcb) );
		dcb.DCBlength = sizeof(dcb);
		GetCommState ( hFile, &dcb );
		dcb.BaudRate	= CBR_115200;
		dcb.ByteSize	= 8;
		dcb.StopBits	= 0;
		dcb.Parity		= NOPARITY;
		SetCommState ( hFile, &dcb );

		// Set communication timeouts TODO: Node properties
		memset ( &cto, 0, sizeof(cto) );
		cto.ReadIntervalTimeout			= 20;
		cto.ReadTotalTimeoutConstant	= 1000;
		cto.WriteTotalTimeoutConstant	= 1000;
		SetCommTimeouts ( hFile, &cto );

		PurgeComm ( hFile, PURGE_RXABORT|PURGE_RXCLEAR|PURGE_TXABORT|PURGE_TXCLEAR );

		if (WriteFile ( hFile, pkt, sizeof(pkt), &dx, NULL ))
			{
while (1)
{
			if (ReadFile ( hFile, rsp, sizeof(rsp), &dx, NULL ))
				{
				if (dx == 0)
					OutputDebugString ( L"Zero!\r\n");
				else
					OutputDebugString ( L"Not zero!\r\n");
				}	// if
}
			}	// if

		CloseHandle(hFile);
		}	// if
*/

	return 0;
	}	// main

//
// Node
//

HRESULT Node1::attach ( IKeySource *ps, bool ba )
	{
	return S_FALSE;
	}

HRESULT Node1::stored ( IKeySource *, const adtValueTst &, const adtValueTst & )
	{
	return S_FALSE;
	}

//
// Location
//

HRESULT Location::attach ( IKeySource *ps, bool ba )
	{
	return S_FALSE;
	}

HRESULT Location::listen ( const adtValueTst &kf, IKeySink *ps, const adtValueTst &kt, bool bl )
	{
	return S_FALSE;
	}

HRESULT Location::load	( const adtValueTst &k, adtValueTst &v )
	{
	return S_FALSE;
	}

HRESULT Location::stored ( IKeySource *, const adtValueTst &, const adtValueTst & )
	{
	return S_FALSE;
	}

//
// adtValueTst
//

adtValueTst :: adtValueTst ( void )
	{
	}

adtValueTst :: adtValueTst ( int i )
	{
	}

adtValueTst :: adtValueTst ( const WCHAR *pw )
	{
	}

adtValueTst :: adtValueTst ( double d )
	{
	}

adtValueTst :: adtValueTst ( float f )
	{
	}

float adtValueTst :: toFloat ( void )
	{
	return 0;
	}

#else

#include "../../lib/adtl/adtl_.h"

int main ( int argc, char *argv[] )
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Entry point into program.
	//
	//	PARAMETERS
	//		-	argc,argv are the command line parameters
	//
	//	RETURN VALUE
	//		Exit code
	//
	////////////////////////////////////////////////////////////////////////
	adtString str(L"YouCanByteMeNow");
	return 0;
	}	// main

#endif
