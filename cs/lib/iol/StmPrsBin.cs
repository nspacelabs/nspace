﻿////////////////////////////////////////////////////////////////////////
//
//									StmPrsBin.cs
//
//				C# version of nSpace value binary parser.
//
////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Collections;
using System.Collections.Generic;

namespace nSpace
	{
	class StmPrsBin
		{
		// Value types
		public const int VTYPE_EMPTY	= 0;
		public const int VTYPE_I4		= 3;
		public const int VTYPE_I8		= 20;
		public const int VTYPE_R4		= 4;
		public const int VTYPE_R8		= 5;
		public const int VTYPE_DATE	= 7;
		public const int VTYPE_STR		= 8;
		public const int VTYPE_BOOL	= 11;
		public const int VTYPE_UNK		= 13;

		static public System.Object load ( byte[] bfr, ref int idx )
			{
			////////////////////////////////////////////////////////////////////////
			//
			//	PURPOSE
			//		-	Load the nSpace value from the byte array
			//
			//	PARAMETERS
			//		-	bfr contains the information
			//		-	idx contains the current index into the buffer
			//
			//	RETURN VALUE
			//		Loaded object
			//
			////////////////////////////////////////////////////////////////////////
			object	ret	= null;
			short		vt		= 0;

			// Begin marker
			vt = load16(bfr,ref idx);
			if (vt != 0x3141)
				{
				Debug.WriteLine("Invalid begin marker : " + vt );
				return null;
				}	// if

			// Value type
			vt = load16(bfr,ref idx);

			// Process value
			switch (vt)
				{
				case VTYPE_I4 :
					ret = load32(bfr,ref idx);
					break;
				case VTYPE_I8 :
					ret = load64(bfr,ref idx);
					break;
				case VTYPE_R4 :
					Array.Reverse(bfr,idx,4);
					ret = BitConverter.ToSingle(bfr,idx);
					idx += 4;
					break;
				case VTYPE_R8 :
					Array.Reverse(bfr,idx,8);
					ret = BitConverter.ToDouble(bfr,idx);
					idx += 8;
					break;
				case VTYPE_DATE :
					Array.Reverse(bfr,idx,8);
					ret = BitConverter.ToDouble(bfr,idx);
					idx += 8;
					break;
				case VTYPE_STR :
					ret = loadStr(bfr,ref idx);
					break;
				case VTYPE_BOOL :
					ret = load16(bfr,ref idx);
					ret = (Convert.ToInt16(ret) == 0) ? false : true;
					break;
				case VTYPE_UNK :
					{
					// Object Id
					string
					id = loadStr(bfr,ref idx).ToLower();

					// List
					if (id.Equals("adt.list"))
						{
						// Read list count
						int	
						cnt = load32(bfr,ref idx);

						// Create list to hold object
						List<System.Object>
						lst = new List<System.Object>();

						// Load list values
						for (int i = 0;i < cnt;++i)
							lst.Add(load(bfr,ref idx));
//							lst[i] = load(bfr,ref idx);

						// Result
						ret = lst;
						}	// if

					// Dictionary
					else if (id.Equals("adt.dictionary"))
						{
						// Read count
						int	
						cnt = load32(bfr,ref idx);

						// Incoming dictionary
						Dictionary<System.Object,System.Object>
						dct = new Dictionary<System.Object,System.Object>(cnt);

						// Read key/value pairs into dictionary
						for (int i = 0;i < cnt;++i)
							dct[load(bfr,ref idx)] = load(bfr,ref idx);

						// Result
						ret = dct;
						}	// else if

					// Byte stream
					else if (id.Equals("io.stmmemory"))
						{
						Debug.WriteLine("Not implemented:"+id);
						}	// else if

					// Memory block
					else if (id.Equals("io.memoryblock"))
						{
						Debug.WriteLine("Not implemented:"+id);
						}	// else if

					else
						Debug.WriteLine("Unknown object:"+id);
					}	// VTYPE_UNK
					break;
				case VTYPE_EMPTY:
					// Empty values are ok
					break;
				default :
					Debug.WriteLine("StmPrsBin::load:Unhandled value type:" + vt);
					break;
				}	// switch

			// End marker
			vt = load16(bfr,ref idx);
			if (vt != 0x5926)
				{
				Debug.WriteLine("Invalid end marker : " + vt );
				return null;
				}	// if

			return ret;
			}	// load

		// Load functions
		static short load16 ( byte[] bfr, ref int idx )
			{
			short 
			ret = IPAddress.HostToNetworkOrder(BitConverter.ToInt16(bfr,idx));
			idx += 2;
			return ret;
			}	// load16
		static int load32 ( byte[] bfr, ref int idx )
			{
			int
			ret = IPAddress.HostToNetworkOrder(BitConverter.ToInt32(bfr,idx));
			idx += 4;
			return ret;
			}	// load32
		static long load64 ( byte[] bfr, ref int idx )
			{
			long 
			ret = IPAddress.HostToNetworkOrder(BitConverter.ToInt64(bfr,idx));
			idx += 8;
			return ret;
			}	// load16
		static string loadStr ( byte[] bfr, ref int idx )
			{
			string ret = null;

			// Obtain length of string
			int
			len = load32(bfr,ref idx);

			// Size of source 'WCHAR' characters
			short
			szw = load16(bfr,ref idx);

			// Read string characters
			switch (szw)
				{
				// 2 bytes per character, e.g. Windows Unicode
				case 2:
					ret = System.Text.Encoding.Unicode.GetString(bfr,idx,2*len);
					idx += 2*len;
					break;
				}	// switch

			return ret;
			}	// loadStr

		}	// StmPrsBin

	}	// nSpace
