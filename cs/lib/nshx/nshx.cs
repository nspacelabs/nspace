﻿////////////////////////////////////////////////////////////////////////
//
//									NSHX.CS
//
//	Contains the C# bindings to the external C API to the nSpace client.
//
////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace nSpace
	{
	class nSpaceClient
		{
		// nSpace external C API
		[DllImport("NSHX")]
		public static extern int n_Close		( ref ulong client );
		[DllImport("NSHX")]
		public static extern int n_Listen	( ulong client, string path, ushort bListen );
		[DllImport("NSHX")]
		public static extern int n_Load		( ulong client, string path );
		[DllImport("NSHX")]
		public static extern int n_Open		( ref ulong client, string cmdLine, ushort sharem, ushort direct );
		[DllImport("NSHX")]
		public static extern int n_Resolve	( string path, StringBuilder pathAbs, int size );
		[DllImport("NSHX")]
//		public static extern int n_Received	( ulong client, StringBuilder root, StringBuilder location, int size, [MarshalAs(UnmanagedType.Struct)] ref object var );
		public static extern int n_Received	( ulong client, StringBuilder root, StringBuilder location, int strsz, out IntPtr data, out int datasz );
		[DllImport("NSHX")]
		public static extern int n_Store		( ulong client, string path, [MarshalAs(UnmanagedType.Struct)] object var );

		// Platform specific shared memory for IPC
		[DllImport("NSHX")]
		public static extern ulong n_shm_open	( string name, int length );
		[DllImport("NSHX")]
		public static extern int n_shm_copy_to	( ulong fd, [In] byte[] data, int length );
		[DllImport("NSHX")]
		public static extern ulong n_shm_close	( ulong fd );

		//
		// Testing interface to C DLL
		//
		public static void Test()
			{
			ulong				client	= 0;
			System.Object	var		= null;
			StringBuilder	strRoot	= new StringBuilder(1024);
			StringBuilder	strLoc	= new StringBuilder(1024);

			// Testing, directory for 'nshx.dll'
			Directory.SetCurrentDirectory("C:\\dev\\nSpace\\c\\build\\Debug");

			// Perform tests on the namespace
			nSpaceClient.n_Open(ref client,"{ Namespace CSharp }", 0, 1);
			nSpaceClient.n_Load(client,"/app/auto/default/Initialize/OnFire/Value");
//			nSpaceClient.n_Received(client, strRoot, strLoc, strRoot.Capacity, ref var );
			var = "YouCanByteMeNow!!";
			nSpaceClient.n_Store(client, "/app/auto/default/Initialize/Fire", var);
			nSpaceClient.n_Listen(client, "/app/auto/default/Initialize/OnFire", 1);
//			nSpaceClient.n_Received(client, strRoot, strLoc, strRoot.Capacity, ref var );
			System.Diagnostics.Debug.WriteLine(var);		// Should be previously stored value
			nSpaceClient.n_Close(ref client);
			}  // Test

		}  //  nSpaceClient

	}	// nSpace

