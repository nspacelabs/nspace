////////////////////////////////////////////////////////////////////////
//
//									NSPACE.JS
//
//		Unified nSpace script to bind existing HTML elements to nSpace web 
//		sockets server.
//
////////////////////////////////////////////////////////////////////////

// Create new instance of this object immediately
new function ()
	{
	var	ws			= null;								// WebSocket object
	var	connType	= 0;									// Connection type
	var	nElems	= {};									// Bounded element states

	nSpace_bind =
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Object to bind HTML elements on page to nSpace server.
		//
		////////////////////////////////////////////////////////////////////////

		init : function() 
			{ 
			////////////////////////////////////////////////////////////////////////
			//
			//	PURPOSE
			//		-	Initialize state of the binding object
			//
			////////////////////////////////////////////////////////////////////////

			// Parser
			nSpaceXML.init();

			// URL for hosting document
			loc	= window.location;

			// Generate websocket string
//			url	= "ws://"+loc.host+"/nspace/";

			// Try and support multiple scenarios.
//			boolean bConn = false;

			// Initialize connection state
			ws			= null;
			connType = 0;

			// Proceed with first connection type
			this.nextConn();


			},

		nextConn : function()
			{
			////////////////////////////////////////////////////////////////////////
			//
			//	PURPOSE
			//		-	Called to attempt the next connection metholodgy
			//
			////////////////////////////////////////////////////////////////////////

			// URL for hosting document
			loc	= window.location;

			//
			// 1) Normal - Loading script through webpage from a server that
			//					supports websockets
			//
			if (connType === 0)
				{
				// A valid hostname is required
				if (loc.hostname.length > 0)
					{
					// Attempt to connect to web socket server via http
					ws = new WebSocket("ws://"+loc.hostname);

					// Options and events
					ws.binaryType	= "arraybuffer";
					ws.onopen		= this.onOpen;
					ws.onmessage	= this.onMessage;	
					ws.onerror		= this.onError;	
					}	// if
				else
					++connType;
				}	// if

			//
			// 2) Script was loaded through web page but server does not support
			// web sockets, attempt to connect to server directly on known port.
			//
			if (connType === 1)
				{
				// A valid hostname is required
				if (loc.hostname.length > 0)
					{
					// Attempt direct connection
					ws = new WebSocket("ws://"+loc.hostname+":50001/");

					// Options and events
					ws.binaryType	= "arraybuffer";
					ws.onopen		= this.onOpen;
					ws.onmessage	= this.onMessage;	
					ws.onerror		= this.onError;	
					}	// if
				else
					++connType;
				}	// if

			// 3) Script was loaded directly (e.g., file://...).  Try
			// and connect directly to local web socket server.  Useful
			// during development.
			if (connType === 2)
				{
				// No hostname ?
				if (loc.hostname.length < 1)
					{
					// Attempt connection
					ws = new WebSocket("ws://127.0.0.1:50001/");

					// DEBUG, direct connect to debug system with static page
//					ws = new WebSocket("ws://192.168.1.253:50001/");
//					ws = new WebSocket("ws://192.168.1.252:50001/");
//					ws = new WebSocket("ws://192.168.1.205:50001/");

					// Options and events
					ws.binaryType	= "arraybuffer";
					ws.onopen		= this.onOpen;
					ws.onmessage	= this.onMessage;		
					ws.onerror		= this.onError;	
					}	// if
				else
					++connType;
				}	// if

			// No more tries
			if (connType > 2)
				{
				console.log("Connection attempts have failed" );
				}	// else

			},	// nextConn

		onError : function(event)
			{
			////////////////////////////////////////////////////////////////////////
			//
			//	PURPOSE
			//		-	Callback when a WebSocket detects an error.
			//
			//	PARAMETERS
			//		-	event contains the event details
			//
			////////////////////////////////////////////////////////////////////////

			// Trying to connect ?
			if (connType < 3)
				{
				++connType;
				nSpace_bind.nextConn();
				}	// if

			},	// onError

		onMessage : function(event)
			{
			////////////////////////////////////////////////////////////////////////
			//
			//	PURPOSE
			//		-	Callback when a WebSocket message is received.
			//
			//	PARAMETERS
			//		-	event contains the message
			//
			////////////////////////////////////////////////////////////////////////
			var msg		= null;
//			var elem		= null;
			var loc		= null;
			var type		= null;
			var value	= null;
			var tag		= null;
			var node		= null;

			// Type of received message
			if (typeof event.data === "string")
				{
				// Extract nSpace value from incoming XML
				parser	= new DOMParser();
				xmlDoc	= parser.parseFromString(event.data,"text/xml");
				msg		= nSpaceXML.load ( xmlDoc.documentElement );

				// Debug
//				console.log(msg);
				}	// if

			// Binary
			else if (event.data instanceof ArrayBuffer)
				{
				var bfr	= new DataView(event.data);
				var idx	= new Uint32Array(1);

				// DEBUG
				if (event.data.byteLength === 0)
					{
					console.log("Received zero length buffer!");
					return;
					}	// if

				// Extract nSpace value from  ArrayBuffer
//				console.log("ArrayBuffer:"+event.data);
				msg		= nSpaceBin.load ( bfr, idx );

				// Debug
//				console.log(msg);
				}	// else if

			// Sanity check
			if (msg === null || !("Root" in msg) || !("Location" in msg))
				{
				console.log("onMessage:Missing fields:"+msg);
				return;
				}	// if

			// Generate full path to element.  
			var locf = (msg["Root"]+msg["Location"]).toLowerCase();
//			console.log(locf);

			// THIS COULD BE SLOW although generally there are not enough unique element
			// paths on a page to matter.  Now that the full path to the value has to be
			// used (root/location are arbitrary) search for elements that
			// have a path that matches the begining of the full location.
			for (var path in nElems)
				{
				// Does key match beginning of path ?
				if (!locf.startsWith(path))
					continue;

				// Process all elements associated with path
				for (var e = 0, n = nElems[path].length; e < n; ++e)
					{
					// Next element
					elem = nElems[path][e];
					if (elem === null)
						return;
//					console.log ( "Element : " + elem + "WxH:"+elem.width+"x"+elem.height );

					// To make it easier to process string
					tag	= elem.tagName.toLowerCase();
					type	= elem["type"];
					value	= msg["Value"];

					//
					// Process value based on element type
					//
					console.log("Path:"+locf+":Tag:"+tag+":Type:"+type+":Value:"+value);

					//
					// Is element a node ?
					// In order to support both interface states and nodes directly
					// set a flag for the nodes, only nodes will have a 'top-level' descriptor.
					// 
					if (locf === path+"ondescriptor/value")
						elem.attributes["data-nisnode"] = true;
					node = (elem.attributes["data-nisnode"] === true) ? true : false;

					// Non-type specific

					// Enable
					if (locf.endsWith("element/enable/onfire/value"))
						elem.disabled = !value;

					// Button
					else if (tag === "button")
						{
//elem.disable = false;

						//
						// File state
						//
	/*
						if (locf.endsWith("execute/onfire/value"))
							{
							// Mode specified ?
							if (!("nfileMode" in elem))
								continue;


							// On hold since prompting for a file apparently cannot occur
							// outside the main event loop

							// Execute based on mode
							if (elem["nfileMode"] == "save")
								{
								// Prompt for file selection
								var event	= new MouseEvent ('click', { view: window, bubbles: true, cancelable: true } );
								var input	= document.createElement('input');
								input.type	= 'file';
								var c = !input.dispatchEvent(event);
								alert(c);
	//							input.click();
								alert ( input.files[0] );
								}	// if

							}	// if
						else if (loc === "mode")
							elem["nfileMode"] = value.toLowerCase();
						else if (loc === "content")
							elem["nfileContent"] = value;
						else if (loc === "extension")
							elem["nfileExtension"] = value;
						else if (loc === "title")
							elem["nfileTitle"] = value;
	*/
						}	// if

					// Radio
					// Radio buttons currently used as direct indicies into a list state
					else if (tag === "input" && type === "radio")
						{
						// Value directly from node
						if (node && locf.endsWith("onfire/value"))
							{
							// Set state of radio button if matching text
							// Default to spanning label ?
							if (	elem.hasAttribute("data-nval") &&
									elem.getAttribute("data-nval").toLowerCase() === value.toLowerCase() )
								elem.parentElement.MaterialRadio.check();
							else
								elem.parentElement.MaterialRadio.uncheck();
							}	// if

						}	// else if

					// Checkbox
					else if (tag === "input" && type === "checkbox")
						{
						// Activate or support direct value connection
						if (	(node && locf.endsWith("onfire/value")) ||
								(!node && locf.endsWith("activate/onfire/value")) )
							{
							// Default usage
							elem.checked = value;

							// MDL support
							// This is to handle the absolutely horrible way that MDL does this,
							// simply setting 'checked' do not update the UI
							if (elem.parentElement.hasOwnProperty("MaterialSwitch"))
								{
								if (value === true)
									elem.parentElement.MaterialSwitch.on();
								else
									elem.parentElement.MaterialSwitch.off();
								}	// if
							else if (elem.parentElement.hasOwnProperty("MaterialCheckbox"))
								{
								if (value === true)
									elem.parentElement.MaterialCheckbox.check();
								else
									elem.parentElement.MaterialCheckbox.uncheck();
								}	// if
							}	// if
						}	// else if

					// Range / Slider
					else if (tag === "input" && type === "range")
						{
						// Value
						if (	(node && locf.endsWith("onfire/value")) ||
								(!node && locf.endsWith("element/default/onfire/value")) )
							{
							// MDL support
							if (elem.hasOwnProperty("MaterialSlider"))
								elem.MaterialSlider.change(value);
							else
								elem.value	= value;
							}	// if

						// Limits
						else if (	(node && locf.endsWith("ondescriptor/value")) ||
										(!node && locf.endsWith("element/default/ondescriptor/value")) )
							{
							if (value.hasOwnProperty("Minimum"))
								elem.min = value["Minimum"];
							if (value.hasOwnProperty("Maximum"))
								elem.max = value["Maximum"];
							}	// else if

						}	// else if

					// List box
					else if (tag === "select")
						{
						// Current list
						if (	(node && locf.endsWith("ondescriptor/value")) ||
								(!node && locf.endsWith("list/onfire/value")) )
							{
							var str;

							// Remove existing elements
							while (elem.length > 0)
								elem.remove(0);

							// If dealing with a node, the 'allowed' list specified
							// which strings are valid inputs.
							if (node)
								value = value["Allowed"];

							// Add strings to list
							for (idx in value)
								{
								var option = document.createElement("option");
								option.text = value[idx];
								elem.add(option);
								}	// for

							// If selected index was valid, re-select
							if (elem.hasOwnProperty("nselectedIndex"))
								elem.selectedIndex = elem.nselectedIndex;

							// Explicit set of 'not selected' required since browsers seems
							// to auto-select on add
							else
								elem.selectedIndex = -1;
							}	// if

						// Default value is the selected index
						else if (locf.endsWith("element/default/onfire/value"))
							{
							// Store in own property as well in case list is updated
							elem.nselectedIndex	= parseInt(value) - 1;
							elem.selectedIndex	= elem.nselectedIndex;
							}	// else if

						// Node selection via latest value
						else if (node && locf.endsWith("onfire/value"))
							{
							elem.value = value;
							}	// else if

						}	// else if

					// Text/edit box
					else if (tag === "input" && type === "text")
						{
						// Value
						if (	(node && locf.endsWith("onfire/value")) ||
								(!node && locf.endsWith("element/default/onfire/value")) )
							{
							// Decimal places for floating point
							if (elem.hasAttribute("data-ndec"))
								elem.value = parseFloat(value).toFixed(parseInt(elem.getAttribute("data-ndec")));
							// Default
							else
								elem.value= value;
							}	// if
						}	// else if

					// Image
					else if (tag === "canvas")
						{
						// A default value for images is simply a dictionary
						// containing the image information
						if (	(node && locf.endsWith("onfire/value")) ||
								(!node && locf.endsWith("element/default/onfire/value")) )
							{
							// The data for an image is a dictionary with its parameters
							if (value.hasOwnProperty("Bits"))
								{
								// Access image information
								var bits		= new Uint8Array(value["Bits"]);

								// Context for rendering to the canvas
								var ctx	= elem.getContext("2d");

								// Create a new image object for rendering
								var img				= new Image();

								// This is necessary since the canvas element dimensions
								// may not be available at load/draw time.
								img["drawWidth"]	= elem.width;
								img["drawHeight"]	= elem.height;

								// In case blob gets created
								var blb	= null;

								// Handle required formats
								if (value["Format"] === "JPEG" || value["Format"] === "JPG")
									{
									// Create a blob for the image bits
									blb = new Blob([bits], { type : 'image/jpeg' } );
									}	// if
								else if (value["Format"] === "PNG")
									{
									// Create a blob for the image bits
									blb = new Blob([bits], { type : 'image/png' } );
									}	// if

								// NOTE: Special case of raw pixel data.  Does not URL/blob logic below.
								else if (value["Format"] === "R8G8B8" || value["Format"] === "R8G8B8A8" || 
											value["Format"] === "B8G8R8" || value["Format"] === "B8G8R8A8" ||
											value["Format"].toLowerCase() === "u8x2")
									{
									// Size of image
									var width	= value["Width"];
									var height	= value["Height"];

									// Byte order
									var bRGB		= (value["Format"] === "R8G8B8");
									var bRGBA	= (value["Format"] === "R8G8B8A8");
									var bBGR		= (value["Format"] === "B8G8R8");
									var bBGRA	= (value["Format"] === "B8G8R8A8");
									var bGray8	= (value["Format"].toLowerCase() === "u8x2");

									// Must handle re-sizing to shape.  Create memory canvas and draw
									// entire image to it, then draw resized into target canvas
									var canvasMem	= document.createElement("canvas");
									var ctxMem		= canvasMem.getContext("2d");

									// Ensure memory canvas matches size of element for later drawing
									canvasMem.width	= width;
									canvasMem.height	= height;

									// Get image data directly from canvas and write raw pixel data to it.
									var imageD	= ctxMem.createImageData(width,height);
									var data		= imageD.data;
									var len		= bits.length;
									for (var y = 0,srcidx = 0,dstidx = 0;y < height;++y)
										for (var x = 0;x < width;++x)
											{
											if (bBGR || bBGRA)
												{
												data[dstidx++]	= bits[srcidx+2];
												data[dstidx++]	= bits[srcidx+1];
												data[dstidx++]	= bits[srcidx+0];
												srcidx += (bRGB) ? 3 : 4;
												}	// if
											else if (bRGB || bRGBA)
												{
												data[dstidx++]	= bits[srcidx++];
												data[dstidx++]	= bits[srcidx++];
												data[dstidx++]	= bits[srcidx++];
												if (bRGBA) ++srcidx;
												}	// else
											else if (bGray8)
												{
												data[dstidx++]	= bits[srcidx];
												data[dstidx++]	= bits[srcidx];
												data[dstidx++]	= bits[srcidx++];
												}	// else if
											data[dstidx++] = 0xff;
											}	// for

									// Write pixel data directly to memory canvas
									ctxMem.putImageData(imageD,0,0);

									// Draw to target canvas
									ctx.drawImage(canvasMem,0,0,elem.width,elem.height);
									}	// if

								// Raw RGB
		//						else if (value["Format"] == "R8G8B8")
		//							{
		//							// Create a blob for the image bits
		//							blb = new Blob([bits], { type : 'image/x-rgb' } );
		//							}	// if

								// If valid blob, render it to canvas
								if (blb !== null)
									{
									// URL for blob
									var url	= URL.createObjectURL(blb);

									// Image will be drawn when URL is loaded
									img.onerror = function ()
										{
										alert("Eeek!");
										};
									img.onload = function ()
										{
										// TODO: Resize/aspect ratio options ?
										ctx.drawImage(img,0,0,img["drawWidth"],img["drawHeight"]);
										URL.revokeObjectURL(url);
										};	// onload

									// Assign URL to image for rendering
									img.src = url;
									}	// if
								}	// if
							}	// if

							// Raw pixel data
							/*
							elem.width	= width;
							elem.height	= height;
								var imageD	= ctx.createImageData(width,height);
								var data		= imageD.data;
								var len		= bits.length;
								for (var y = 0,srcidx = 0,dstidx = 0;y < height;++y)
									for (var x = 0;x < width;++x)
										{
										data[dstidx++]	= bits[srcidx];
										data[dstidx++]	= bits[srcidx];
										data[dstidx++]	= bits[srcidx++];
										data[dstidx++] = 0xff;
										}	// for
								ctx.putImageData(imageD,0,0);
							*/

						}	// else if

					// Default handler for what is assumed to be generic text
					// (paragraph, div, headers, etc)
					else 
						{
						// Value
//						if (loc === "element/default")
//							elem.textContent = value;
						}	// else if

	//				else
	//					{
	//					console.log ( "Unhandled type for message : " + tag + ":" + type );
	//					}	// else

					}	// for

				}	// for

			},

		onOpen : function ()
			{
			////////////////////////////////////////////////////////////////////////
			//
			//	PURPOSE
			//		-	Callback when a WebSocket connection is made.
			//
			////////////////////////////////////////////////////////////////////////
			var root		= "";
			var elems	= null;
			var path		= null;

			// Enumerate nSpace elements and bind them to their paths.
 
			// Enumerate all of the elements on the page
			elems = document.getElementsByTagName("*");

			// Find every element that specifies and nSpace path attribute.
			for (var i = 0, n = elems.length;i < n;++i)
				{
				// A 'root' path can be specified so that subsequent elements
				// can specify relative paths
				if (elems[i].getAttribute("data-nroot") !== null)
					root = elems[i].getAttribute("data-nroot");

				// nSpace path specified ?
				if (elems[i].getAttribute("data-nloc") !== null)
					{
					// Generate full bind path if element path is relative
					path = elems[i].getAttribute("data-nloc");
					if (path[0] !== '/')
						path = root + path;

					// Lowercase for all paths to avoid case issues
					path = path.toLowerCase();

					// Assign full path for quick reference
					console.log("Path : "+path);
					elems[i].attributes["data-nabs"] = path;

					// Does path need a dictionary ?
					if (typeof nElems[path] === "undefined")
						nElems[path] = new Array();
					
					// Associate the path with the HTML element
					nElems[path].push(elems[i]);

					// Set-up events
					elems[i].addEventListener("click",onClickn);
					elems[i].addEventListener("change",onChangen);
					elems[i].addEventListener("input",onInputn);

					// Send listen request for path
//					console.log ( "tagName:"+elems[i].tagName+":listen:"+path );
					listen(path);
					}	// if

			}	// for

			},

		}	// nSpace_bind

	var listen = function(srcLoc)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Issue a 'listen' request for the specified path
		//
		//	PARAMETERS
		//		-	srcLoc is the location to listen to
		//
		////////////////////////////////////////////////////////////////////////
		var req = 
			{
			Verb: "Listen",
			};

		// Location for listening
		req["From"] = srcLoc;

		// Send request
		if (ws !== null)
			ws.send(nSpaceXML.save(req));
		};	// listen

	//
	// Events
	//

	var onChangen = function(event)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Called when the an option changes.
		//
		//	PARAMETERS
		//		-	event contains the event information
		//
		////////////////////////////////////////////////////////////////////////
		var elem	= event.srcElement;
		var type = elem["type"];
		var dct  = {};
		var xml	= null;
		var send = true;

		// Store template
		dct["Verb"]		= "Store";
		dct["To"]		= elem.attributes["data-nabs"];

		// Support interface states and nodes
		if (elem.attributes["data-nisnode"] === true)
			dct["To"] += "Fire/Value";
		else
			dct["To"] += "Element/Default/Fire/Value";

		// Select
		if (type === "select-one")
			{
			// To support direct link to nodes, it is assumed a list of strings
			// are specified in an 'allowed' list.  If node, send selected string.
			if (elem.attributes["data-nisnode"] === true)
				{
				dct["Value"]	= elem.value;
				}	// if

			// Selected index, indexes in nSpace are 1-based
			else
				dct["Value"]	= elem.selectedIndex+1;
			}	// if

		// Range
		else if (type === "range")
			dct["Value"]	= elem.value;

		// Text/edit
		else if (type === "text")
			dct["Value"]	= elem.value;

		// Unhandled type
		else
			send = false;

		// Transmit store
		if (send === true)
			{
			// Debug
//			console.log("onChangen:");
//			console.log(dct);

			// Convert to XML
			xml = nSpaceXML.save(dct);

			// Transmit
			ws.send ( xml );
			}	// if

		};	// onChangen

	var onClickn = function(event)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Called when an element is 'clicked'.
		//
		//	PARAMETERS
		//		-	event contains the event information
		//
		////////////////////////////////////////////////////////////////////////
		var elem	= event.srcElement;
		var dct  = {};
		var xml	= null;
		var send = true;

		// When using tool kits (e.g. MDL), sometimes the parent element is
		// the needed nSpace elemnt, check for nSpace location
		if (elem.attributes["data-nabs"] === undefined)
			elem = elem.parentElement;
		if (elem.attributes["data-nabs"] === undefined)
			return;

		// Store template
		dct["Verb"]		= "Store";
		dct["To"]		= elem.attributes["data-nabs"];

		// Support interface states and nodes
		if (elem.attributes["data-nisnode"] === true)
			dct["To"] += "Fire/Value";
		else
			dct["To"] += "Activate/Fire/Value";

		// Button
		var type = elem["type"];
		if (type === "button" || type === "submit")
			{
			// For a button, value does not matter
			dct["Value"]		= 0;
			}	// if

		// Checkbox
		else if (type === "checkbox")
			{
			// Checked state
			dct["Value"]	= (elem.checked === true) ? true : false;
			}	// else

		// Radio
		else if (type === "radio" && elem.hasAttribute("data-nval"))
			dct["Value"]	= elem.getAttribute("data-nval");

		// Unhandled type
		else
			{
//			console.log ( "Unhandled type for click : " + type );
			send = false;
			}	// else

		// Transmit store
		if (send === true)
			{
			// Debug
//			console.log("onClickn:");
//			console.log(dct);

			// Convert to XML
			xml = nSpaceXML.save(dct);

			// Transmit
			ws.send ( xml );
			}	// if
		};	// onClickn

	var onInputn = function(event)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Called when input is available on an  element
		//
		//	PARAMETERS
		//		-	event contains the event information
		//
		////////////////////////////////////////////////////////////////////////
		var elem	= event.srcElement;
		var type = elem["type"];
		var dct  = {};
		var xml	= null;
		var send = true;

		// Store template
		dct["Verb"]		= "Store";
		dct["To"]		= elem.attributes["data-nabs"];

		// Support interface states and nodes
		if (elem.attributes["data-nisnode"] === true)
			dct["To"] += "Fire/Value";
		else
			dct["To"] += "Element/Default/Fire/Value";

		// Range/slider
		if (type === "range")
			dct["Value"]	= event.srcElement.value;

		// Unhandled type
		else
			{
//			console.log ( "Unhandled type for input : " + type );
			send = false;
			}	// else

		// Transmit store
		if (send === true)
			{
			// Debug
//			console.log("onInputn:");
//			console.log(dct);

			// Convert to XML
			xml = nSpaceXML.save(dct);

			// Transmit
			ws.send ( xml );
			}	// if
		};	// onInputn

	};	// function

// Execute main on document loaded
document.onreadystatechange = function ()
	{
	////////////////////////////////////////////////////////////////////////
	//
	//	PURPOSE
	//		-	Called when the documents ready state has changed.
	//
	/////////////	///////////////////////////////////////////////////////////

	// Document ready ?
	if (document.readyState === "complete")
		nSpace_bind.init();		
	};	// onreadystatechange

////////////////////////////////////////////////////////////////////////
//
//									NSPACE_BIN.JS
//
//							nSpace binary value parser.
//
////////////////////////////////////////////////////////////////////////

//
// Class - nSpaceBin. Object to parse nSpace values from/to array buffers.
//
 
var nSpaceBin =
	{
	init : function() 
		{ 
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Initialize state of the object
		//
		////////////////////////////////////////////////////////////////////////
		},

	load : function(bfr,idx)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Load a value from an array buffer
		//
		//	PARAMETERS
		//		-	bfr is the byte array
		//		-	idx is the index into buffer
		//
		// RETURN VALUE
		//		Converted value
		//
		////////////////////////////////////////////////////////////////////////
		var ret	= null;

		// Value begin
		if (nSpaceBin.load16(bfr,idx) !== 0x3141)
			return ret;

		// Value type
		var type = nSpaceBin.load16(bfr,idx);
		switch (type)
			{
			// String
			case 0x8 :
				// Read string value
				ret = nSpaceBin.loadStr(bfr,idx);
				break;

			// Numbers
			case 3 :
				ret = nSpaceBin.load32(bfr,idx);
				break;
			case 11 :
				ret = (nSpaceBin.load16(bfr,idx) !== 0) ? true : false;
				break;
			case 20 :
				ret = nSpaceBin.load64(bfr,idx);
				break;
			case 4 :
				ret	= bfr.getFloat32(idx[0]);
				idx[0] += 4;
				break;
			case 5 :
			case 7 :
				// Dates are doubles representing number of days since Jan 1, 2001
				// TODO: Convert to javascript data
				ret		= bfr.getFloat64(idx[0]);
				idx[0] += 8;
				break;

			// Object
			case 0xd :
				// Read object Id string
				var id = nSpaceBin.loadStr(bfr,idx).toLowerCase();

				// Dictionary
				if (id === "adt.dictionary")
					{
					// Empty dictionary
					ret = {};

					// Number of key/value pairs
					var cnt = nSpaceBin.load32(bfr,idx);

					// Read pairs
					for (var i = 0;i < cnt;++i)
						{
						var key	= nSpaceBin.load(bfr,idx);
						var val	= nSpaceBin.load(bfr,idx);
						ret[key]	= val;
						}	// for

					}	// if

				// List
				else if (id === "adt.list")
					{
					// Empty list
					ret = [];

					// Number of values
					cnt = nSpaceBin.load32(bfr,idx);

					// Read values
					for (i = 0;i < cnt;++i)
						{
						val	= nSpaceBin.load(bfr,idx);
						ret.push(val);
						}	// for
					}	// else if

				// Memory block or memory stream
				else if (id === "io.memoryblock" || id === 'io.stmmemory')
					{
					// Size of region
					var sz = nSpaceBin.load32(bfr,idx);

					// Read in block of memory
					ret		= bfr.buffer.slice(idx[0],idx[0]+sz);
					idx[0]	+= sz;
					}	// else if

				// Unknown
				else
					{
					console.log("Unhanlded object type : " + id );
					}	// else
				break;

			// Empty values ok
			case 0 :
				// Empty
				ret = null;
				break;

			default :
				console.log ( "nSpaceBin:Unhandled type:"+type );
			}	// switch

		// Value end
		if (nSpaceBin.load16(bfr,idx) !== 0x5926)
			return null;

		return ret;
		},	// load

	load16 : function(bfr,idx)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Load a 16-bit value from the buffer
		//
		//	PARAMETERS
		//		-	bfr is the array buffer
		//		-	idx is the index from which to read
		//
		// RETURN VALUE
		//		Value
		//
		////////////////////////////////////////////////////////////////////////
		ret	= bfr.getInt16(idx[0]);
		idx[0] += 2;
		return ret;
		},	// load16

	load32 : function(bfr,idx)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Load a 32-bit value from the buffer
		//
		//	PARAMETERS
		//		-	bfr is the array buffer
		//		-	idx is the index from which to read
		//
		// RETURN VALUE
		//		Value
		//
		////////////////////////////////////////////////////////////////////////
		ret	= bfr.getInt32(idx[0]);
		idx[0] += 4;

		return ret;
		},	// load32

	load64 : function(bfr,idx)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Load a 64-bit value from the buffer
		//
		//	PARAMETERS
		//		-	bfr is the array buffer
		//		-	idx is the index from which to read
		//
		// RETURN VALUE
		//		Value
		//
		////////////////////////////////////////////////////////////////////////

		// Javascript cannot handle 64-bit integers.  Down convert to 32-bit,
		// this works for the current usage, this will need to change if truly
		// > 32-bit size is necessary.  Ignore upper 32-bits for now.
		idx[0] += 4;
		ret	= bfr.getInt32(idx[0]);
		idx[0] += 4;

		return ret;
		},	// load64

	loadStr : function(bfr,idx)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Load a string from the buffer
		//
		//	PARAMETERS
		//		-	bfr is the array buffer
		//		-	idx is the index from which to read
		//
		// RETURN VALUE
		//		String
		//
		////////////////////////////////////////////////////////////////////////
		var ret = null;

		// Length of string
		var len = nSpaceBin.load32(bfr,idx);

		// Size of WCHAR in remote system
		var szw = nSpaceBin.load16(bfr,idx);

		// Read string bytes
		switch (szw)
			{
			// 2 bytes per char (Windows)
			case 2 :
				ret = "";
				for (var i = 0;i < len;++i)
					{
					var c = nSpaceBin.load16(bfr,idx);
					c = ( ((c >> 8) & 0xff) | ((c << 8) & 0xff00) );
					ret += String.fromCharCode(c);
					}	// for
				break;
			// 4 bytes per char (Linux)
			case 4 :
				ret = "";
				for (i = 0;i < len;++i)
					{
					c = nSpaceBin.load32(bfr,idx);
					c = ( ((c >> 24) & 0xff) | ((c >> 8) & 0xff00) );
					ret += String.fromCharCode(c);
					}	// for
				break;

			default :
				console.log("Unhandled WCHAR size:"+szw);
				break;
			}	// switch

		return ret;
		},	// loadStr

	save : function(value)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Save value to array buffer
		//
		//	PARAMETERS
		//		-	value is the value to save
		//
		// RETURN VALUE
		//		Array buffer
		//
		////////////////////////////////////////////////////////////////////////
		var ret = null;

		return ret;
		}	// load

	}	// nSpaceBin
	

////////////////////////////////////////////////////////////////////////
//
//									NSPACE_XML.JS
//
//							nSpace XML value parser.
//
////////////////////////////////////////////////////////////////////////

//
// Class - nSpaceXML. Object to parse nSpace values from/to XML strings.
//
 
var nSpaceXML =
	{
	init : function() 
		{ 
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Initialize state of the object
		//
		////////////////////////////////////////////////////////////////////////
		},

	load : function(elem)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Load a value from an XML document
		//
		//	PARAMETERS
		//		-	elem is the root element of the XML document
		//
		// RETURN VALUE
		//		Converted value
		//
		////////////////////////////////////////////////////////////////////////
		var ret = null;

		// Type
		if (elem.nodeName === "Dictionary")
			{
			var key = null;
			var val = null;

			// Empty dictionary
			var dct = {};

			// Load key/value pairs into dictionary
			for (var i = 0;i < elem.childNodes.length;i+=2)
				if (	((key = nSpaceXML.load(elem.childNodes[i+0])) !== null) &&
						((val = nSpaceXML.load(elem.childNodes[i+1])) !== null) )
					dct[key] = val;

			// Use as return value
			ret = dct;
			}	// if
		else if (elem.nodeName === "List")
			{
			val = null;

			// Empty list
			var lst = [];

			// Load child values into list
			for (i = 0;i < elem.childNodes.length;++i)
				if ((val = nSpaceXML.load(elem.childNodes[i])) !== null)
					lst.push ( val );

			// Use as return value
			ret = lst;
			}	// else if

		// Single value
		else if (elem.nodeName === "Value")
			{
			val = null;

			// Type specified ?
			var type = (elem.attributes.length > 0) ?
							elem.attributes["Type"].nodeValue : null;

			// It is possible to receive an 'empty' value in which
			// case there is not 'firstChild'.
			if (	(elem.firstChild !== null) &&
					(val = elem.firstChild.nodeValue) !== null)
				{
				// Default
				if (type === null)
					type = "string";
				else 
					type = type.toLowerCase();

				// Convert type
				if (type === "float" || type === "double")
					val = parseFloat(val);
				else if (type === "int" || type === "long")
					val = parseInt(val);
				else if (type === "boolean")
					val = Boolean(val.toLowerCase() === "true");
				}	// if

			// Use as return value
			ret = val;
			}	// else if

		return ret;
		},	// load

	save : function(value)
		{
		////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		-	Save value to XML string
		//
		//	PARAMETERS
		//		-	value is the value to save
		//
		// RETURN VALUE
		//		XML string
		//
		////////////////////////////////////////////////////////////////////////
		var ret = null;
		var dct = {};
		var lst = [];

		// Dictionary
		if (value instanceof Object)
			{
			// Begin dictionary
			ret = "<Dictionary>"

			// Append values
			for (var key in value)
				{
				// Key then value
				ret += nSpaceXML.save(key);
				ret += nSpaceXML.save(value[key]);
				}	// for

			// End dictionary
			ret += "</Dictionary>";
			}	// if

		// List
		else if (value instanceof Array)
			{
			console.log("Array");
			}	// else if

		// Value
		else
			{
			// Begin value
			ret = "<Value";

			// String
//			console.log(typeof value);
			if (typeof value === "string")
				ret += ">"+value.toString();

			// Number
			else if (typeof value === "number")
				{
				// Integer
				if (typeof value === "number")
					{
					ret += " Type=\"Integer\">";
					ret += value.toString();
					}	// else if
				else
					{
					ret += " Type=\"Float\">";
					ret += value.toString();
					}	// else

				}	// else if

			// Boolean
			else if (typeof value === "boolean")
				{
				ret += " Type=\"Boolean\">";
				ret += value.toString();
				}	// else if

			// Default
			else
				ret += ">"+value.toString();

			// End value
			ret += "</Value>";
			}	// else

		return ret;
		}	// load

	}	// nSpaceXML
